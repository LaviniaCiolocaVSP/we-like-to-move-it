export class Definitions {
  //OUTPUT BEHAVIOR OPTIONS
  static readonly LATCH: string = 'Latch';
  static readonly FOLLOW: string = 'Follow';
  static readonly UNLATCH: string = 'Unlatch';
  static readonly PULSE: string = 'Pulse';

  static readonly DOOR_FORCED_OPEN_ST: string = 'Door Forced Open';
  static readonly DOOR_AJAR_ST: string = 'Door Ajar';
  static readonly DOOR_AJAR_CLEARED_ST: string = 'Door Ajar Cleared';
  static readonly INVALID_PINS_ST: string = 'Too Many Invalid PINs';
  static readonly DOOR_SCHED_OVER_BY_KEYPAD_ST: string = 'Unlock Schedule Overridden by Keypad';
  static readonly DOOR_SCHED_RESTORED_BY_KEYPAD_ST: string = 'Unlock Schedule Restored by Keypad';
  static readonly DOOR_LOCKED_BY_TIMER_ST: string = 'Door Locked By Timer';
  static readonly DOOR_UNLOCKED_BY_TIMER_ST: string = 'Door Unlocked By Timer';
  static readonly CREDENTIAL_NO_PERMISSION_ST: string = 'Failed Access';
  static readonly CREDENTIAL_PRESENTED_ST: string = 'Valid Credential Presented';
  static readonly EXIT_REX_ST: string = 'Exit Requested';
}