import {Component, OnDestroy, OnInit} from '@angular/core';
import {AccountService} from '../../../services/account.service';
import {AccountSummary} from '../../../models/AccountSummary';
import {Router} from '@angular/router';
import {saveAs} from 'node_modules/file-saver';
import {AccountDashboardSummary} from '../../../models/AccountDashboardSummary';

class ArrayOfAccountSummary extends Array<AccountSummary> {
}

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  accountDetails: Array<AccountSummary>;
  contentLoaded: boolean;
  accountSummary: AccountDashboardSummary;

  constructor(private router: Router,
              private accountService: AccountService) {
    this.contentLoaded = false;
  }

  ngOnInit() {
    this.loadAccountDetails();
  }


  ngOnDestroy(): void {
  }

  onBubbleClick(item: AccountSummary) {
    this.router.navigate([item.type, this.accountSummary.accountId]);
  }

  async loadCurrentAccountSummary() {
    this.accountService.getCurrentDatabaseAccountDashboardSummary()
        .then(result => {
          this.accountSummary = result;
          this.accountDetails = this.getAccountSummary();
        });
  }

  getAccountSummary(): ArrayOfAccountSummary {
    const accountDetails: Array<AccountSummary> = new Array<AccountSummary>();

    accountDetails.push({type: 'doors', value: this.accountSummary.doorsCount});
    accountDetails.push({type: 'devices', value: this.accountSummary.devicesCount});
    accountDetails.push({type: 'users', value: this.accountSummary.usersCount});
    accountDetails.push({type: 'credentials', value: this.accountSummary.credentialsCount});
    accountDetails.push({type: 'schedules', value: this.accountSummary.schedulesCount});
    accountDetails.push({type: 'holidays', value: this.accountSummary.holidaysCount});
    accountDetails.push({type: 'groups', value: this.accountSummary.groupsCount});
    accountDetails.push({type: 'panels', value: this.accountSummary.panelsCount});
    return accountDetails;
  }

  loadAccountDetails() {
    this.contentLoaded = false;
    this.loadCurrentAccountSummary();
    this.contentLoaded = true;
  }

  private saveToFileSystem(response) {
    const jsonContent = JSON.stringify(response, null, ' ');

    const blob = new Blob([jsonContent], {type: 'application/json'});

    console.log('Response.name: ' + response.name);

    const filename = this.getFileName(response.name);
    console.log('fileName: ' + filename);

    saveAs(blob, filename);
  }

  getFileName(responseName: string): string {
    const date = new Date();
    let fileName = 'RPS-Backup-' + date.getFullYear();
    const currentMonth: number = date.getMonth() + 1;
    let month: string = currentMonth + '';
    let day: string = date.getDate() + '';

    if (currentMonth < 10) {
      month = '0' + currentMonth;
    }
    if (date.getDate() < 10) {
      day = '0' + date.getDate();
    }

    fileName += '_' + month + '_' + day + '-' + responseName;

    return fileName;
  }

}
