import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule,
  CalendarModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  MultiSelectModule,
  RadioButtonModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {BoardsComponent} from './container/boards.component';
import {EditDoorBoardComponent} from './editBoard/edit-door-board/edit-door-board.component';
import {EditIoBoardComponent} from './editBoard/edit-io-board/edit-io-board.component';

@NgModule({
  declarations: [BoardsComponent, EditDoorBoardComponent, EditIoBoardComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    MultiSelectModule,
    RadioButtonModule
  ]
})
export class BoardsModule {
}
