import {Component, Input, OnInit} from '@angular/core';
import {DoorBoardForm} from '../../../../models/DoorBoardForm';
import {BoardsService} from '../../../../services/boards.service';
import {SelectItem} from 'primeng/api';

@Component({
  selector: 'app-edit-door-board',
  templateUrl: './edit-door-board.component.html',
  styleUrls: ['./edit-door-board.component.scss']
})
export class EditDoorBoardComponent implements OnInit {

  doorBoardForm: DoorBoardForm;

  @Input() panelOid: number;
  @Input() boardTypeId: number;
  @Input() newBoard: number;
  @Input() boardNumber: number;

  stateOptionsNormallyClosedOpen: SelectItem[] = [
    {
      label: 'Normally Closed', value: '0'
    }, {
      label: 'Normally Open', value: '1'
    }
  ];
  stateOptionsNormalEnergized: SelectItem[] = [
    {
      label: 'Normal', value: '0'
    }, {
      label: 'Energized', value: '1'
    }
  ];


  selectedDoor1RexStateOption: SelectItem;
  selectedDoor1DoorContactStateOption: SelectItem;
  selectedDoor1DoorLockStateOption: SelectItem;
  selectedDoor1AuxRelay1StateOption: SelectItem;
  selectedDoor1AuxInput1StateOption: SelectItem;
  selectedDoor1AuxInput2StateOption: SelectItem;
  selectedDoor1AuxRelay2StateOption: SelectItem;

  selectedDoor2RexStateOption: SelectItem;
  selectedDoor2DoorContactStateOption: SelectItem;
  selectedDoor2DoorLockStateOption: SelectItem;
  selectedDoor2AuxRelay1StateOption: SelectItem;
  selectedDoor2AuxInput1StateOption: SelectItem;
  selectedDoor2AuxInput2StateOption: SelectItem;
  selectedDoor2AuxRelay2StateOption: SelectItem;

  constructor(private boardsService: BoardsService) {
  }

  ngOnInit() {
    console.log(this.panelOid);
    console.log(this.boardTypeId);
    console.log(this.newBoard);
    console.log(this.boardNumber);

    this.boardsService.getDoorBoardPoints(this.panelOid, this.boardTypeId, this.newBoard, this.boardNumber).then(
      (result) => {
        this.doorBoardForm = result;

        this.selectedDoor1RexStateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door1Rex.state];
        this.selectedDoor1DoorContactStateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door1DoorContact.state];
        this.selectedDoor1DoorLockStateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door1DoorLock.state];
        this.selectedDoor1AuxRelay1StateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door1AuxRelay1.state];
        this.selectedDoor1AuxInput1StateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door1AuxInput1.state];
        this.selectedDoor1AuxInput2StateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door1AuxInput2.state];
        this.selectedDoor1AuxRelay2StateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door1AuxRelay2.state];

        this.selectedDoor2RexStateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door2Rex.state];
        this.selectedDoor2DoorContactStateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door2DoorContact.state];
        this.selectedDoor2DoorLockStateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door2DoorLock.state];
        this.selectedDoor2AuxRelay1StateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door2AuxRelay1.state];
        this.selectedDoor2AuxInput1StateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door2AuxInput1.state];
        this.selectedDoor2AuxInput2StateOption = this.stateOptionsNormallyClosedOpen[this.doorBoardForm.door2AuxInput2.state];
        this.selectedDoor2AuxRelay2StateOption = this.stateOptionsNormalEnergized[this.doorBoardForm.door2AuxRelay2.state];

      }
    );
  }

}
