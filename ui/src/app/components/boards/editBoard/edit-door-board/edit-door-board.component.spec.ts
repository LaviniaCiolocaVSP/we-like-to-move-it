import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditDoorBoardComponent } from './edit-door-board.component';

describe('EditDoorBoardComponent', () => {
  let component: EditDoorBoardComponent;
  let fixture: ComponentFixture<EditDoorBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditDoorBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditDoorBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
