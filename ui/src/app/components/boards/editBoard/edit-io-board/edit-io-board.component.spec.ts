import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditIoBoardComponent } from './edit-io-board.component';

describe('EditIoBoardComponent', () => {
  let component: EditIoBoardComponent;
  let fixture: ComponentFixture<EditIoBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditIoBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditIoBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
