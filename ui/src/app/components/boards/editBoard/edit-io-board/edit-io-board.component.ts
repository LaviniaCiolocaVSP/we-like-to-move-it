import {Component, Input, OnInit} from '@angular/core';
import {SelectItem} from 'primeng/api';
import {BoardsService} from '../../../../services/boards.service';
import {IOBoardForm} from '../../../../models/IOBoardForm';

@Component({
  selector: 'app-edit-io-board',
  templateUrl: './edit-io-board.component.html',
  styleUrls: ['./edit-io-board.component.scss']
})
export class EditIoBoardComponent implements OnInit {

  ioBoardForm: IOBoardForm;

  @Input() panelOid: number;
  @Input() boardTypeId: number;
  @Input() newBoard: number;
  @Input() boardNumber: number;

  stateOptionsNormallyClosedOpen: SelectItem[] = [
    {
      label: 'Normally Closed', value: '0'
    }, {
      label: 'Normally Open', value: '1'
    }
  ];
  stateOptionsNormalEnergized: SelectItem[] = [
    {
      label: 'Normal', value: '0'
    }, {
      label: 'Energized', value: '1'
    }
  ];


  selectedInput1StateOption: SelectItem;
  selectedInput2StateOption: SelectItem;
  selectedInput3StateOption: SelectItem;
  selectedInput4StateOption: SelectItem;
  selectedInput6StateOption: SelectItem;
  selectedInput7StateOption: SelectItem;
  selectedInput5StateOption: SelectItem;
  selectedInput8StateOption: SelectItem;

  selectedOutput1StateOption: SelectItem;
  selectedOutput2StateOption: SelectItem;
  selectedOutput3StateOption: SelectItem;
  selectedOutput4StateOption: SelectItem;
  selectedOutput6StateOption: SelectItem;
  selectedOutput7StateOption: SelectItem;
  selectedOutput5StateOption: SelectItem;
  selectedOutput8StateOption: SelectItem;

  constructor(private boardsService: BoardsService) {
    console.log('adgsfsf');
  }

  ngOnInit() {
    console.log(this.panelOid);
    console.log(this.boardTypeId);
    console.log(this.newBoard);
    console.log(this.boardNumber);

    this.boardsService.getIOBoardPoints(this.panelOid, this.boardTypeId, this.newBoard, this.boardNumber).then(
      (result) => {
        this.ioBoardForm = result;

        this.selectedInput1StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input1.state];
        this.selectedInput2StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input2.state];
        this.selectedInput3StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input3.state];
        this.selectedInput4StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input4.state];
        this.selectedInput5StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input5.state];
        this.selectedInput6StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input6.state];
        this.selectedInput7StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input7.state];
        this.selectedInput8StateOption = this.stateOptionsNormallyClosedOpen[this.ioBoardForm.input7.state];

        this.selectedOutput1StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output1.state];
        this.selectedOutput2StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output2.state];
        this.selectedOutput3StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output3.state];
        this.selectedOutput4StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output4.state];
        this.selectedOutput5StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output5.state];
        this.selectedOutput6StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output6.state];
        this.selectedOutput7StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output7.state];
        this.selectedOutput8StateOption = this.stateOptionsNormalEnergized[this.ioBoardForm.output7.state];

      }
    );
  }

}
