import {Component, OnInit, QueryList, ViewChildren} from '@angular/core';
import {Board} from '../../../models/Board';
import {ActivatedRoute, Router} from '@angular/router';
import {BoardsService} from '../../../services/boards.service';
import {BoardType} from '../../../models/BoardType';
import {PanelsService} from '../../../services/panels.service';
import {SelectItem} from 'primeng/primeng';
import {BoardTypesEnum} from '../../../models/BoardTypesEnum';
import {EditDoorBoardComponent} from '../editBoard/edit-door-board/edit-door-board.component';
import {ProgIoPoint} from '../../../models/ProgIoPoint';
import {IoPointTemplate} from '../../../models/IoPointTemplate';
import {EditIoBoardComponent} from '../editBoard/edit-io-board/edit-io-board.component';

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss']
})
export class BoardsComponent implements OnInit {

  BoardTypesEnum: BoardTypesEnum = new BoardTypesEnum();
  boards: Board[];
  cols: any[];
  contentLoaded: boolean;
  panelOid: number;
  currentPanel;
  currentAccountId: number;

  selectedBoards: Board[];

  boardTypes: BoardType[];
  selectedBoardType: BoardType;
  boardNumbersAvailable: Array<number>;

  availableBoardNumbers: SelectItem[] = [];
  selectedBoardNumber: SelectItem;

  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;

  newBoard: Board = new Board();

  addBoardFlag = 1;
  editBoardFlag = 0;

  @ViewChildren(EditDoorBoardComponent) private editDoorBoardComponent: QueryList<EditDoorBoardComponent>;
  @ViewChildren(EditIoBoardComponent) private editIOBoardComponent: QueryList<EditIoBoardComponent>;

  constructor(private router: Router, private activateRoute: ActivatedRoute, private boardService: BoardsService,
              private panelService: PanelsService) {
    this.contentLoaded = false;
    this.initCols();
  }

  initCols() {
    this.cols = [
      {field: 'boardLocation', header: 'Board location'},
      {field: 'boardType', header: 'Board type'},
      {field: 'boardNumber', header: 'Board number'},
    ];
  }

  ngOnInit() {
    this.currentAccountId = this.activateRoute.snapshot.params['id'];
    this.panelOid = this.activateRoute.snapshot.params['panelOid'];

    this.panelService.getPanelByObjectId(this.panelOid)
      .then(result => {
        this.currentPanel = result;
      })
      .catch(error => console.error(error));

    this.loadBoards()
      .then()
      .catch(error => console.error(error));
  }

  async loadBoards() {
    this.contentLoaded = false;
    this.selectedBoards = [];

    this.boardService.getBoardsForPanel(this.panelOid)
      .then((result) => {
        this.boards = result;
        this.contentLoaded = true;
      });
  }

  navigateBack() {
    this.router.navigate(['panels', this.currentAccountId]);
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  buildSelectedBoardIds(): number[] {
    let boardIds: number[] = [];

    this.selectedBoards.map(row => {
      boardIds.push(row.objectId);
    });

    return boardIds;
  }

  deleteBoards() {
    let boardIds: number[] = this.buildSelectedBoardIds();

    this.boardService.deleteBoards(boardIds)
      .then(result => {
        this.closeDeleteDialog();
        this.loadBoards();
      })
      .catch(error => console.error(error));
  }

  showDialogToAdd() {
    this.prepareDialogToAdd()
      .then()
      .catch(error => console.error(error));
  }

  async prepareDialogToAdd() {
    this.newBoard = new Board();

    this.boardTypes = await this.boardService.getBoardTypesForPanelType(this.panelOid);
    this.boardNumbersAvailable = await this.boardService.getAvailableBoardNumbers(this.panelOid);

    for (const b of this.boardNumbersAvailable) {
      let selectItem = {
        label: b.toString(), value: b
      };

      this.availableBoardNumbers.push(selectItem);
    }
    if (this.boardNumbersAvailable.length > 0)
      this.selectedBoardNumber = this.availableBoardNumbers[0];
    // console.log(this.availableBoardNumbers);
    // console.log(this.boardNumbersAvailable);
    this.selectedBoardType = this.boardTypes[0];

    this.displayAddDialog = true;
  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }

  addBoard() {
    this.newBoard.panelOid = this.panelOid;
    this.newBoard.boardTypeId = this.selectedBoardType.boardTypeId;
    this.newBoard.boardNumber = this.selectedBoardNumber.value;

    switch (this.selectedBoardType.boardTypeId) {
      case this.BoardTypesEnum.DOOR_BOARD:
      case this.BoardTypesEnum.ACS6000_BOARD:
      case this.BoardTypesEnum.ACS300_BOARD:
        this.addDoorBoard();
        break;
      case this.BoardTypesEnum.IO_BOARD:
        this.addIOBoard();
        break;
    }
  }


  addDoorBoard() {

    let progIoPoints: ProgIoPoint[] = this.setAllProgIoPointsDoorBoard();
    this.newBoard.progIoPoints = progIoPoints;
    //console.log(this.newBoard);

    this.boardService.addBoard(this.newBoard)
      .then((result) => {
        //  console.log(result);
        this.closeAddDialog();
        this.loadBoards()
          .then()
          .catch(error => console.error(error));
      })
      .catch(error => console.error(error));
  }

  private setAllProgIoPointsDoorBoard() {
    let progIoPoints: ProgIoPoint[] = [];
    const firstDoorBoardComponent = this.editDoorBoardComponent.first;
    const doorBoardForm = firstDoorBoardComponent.doorBoardForm;
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1Rex, firstDoorBoardComponent.selectedDoor1RexStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2Rex, firstDoorBoardComponent.selectedDoor2RexStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1DoorContact, firstDoorBoardComponent.selectedDoor1DoorContactStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2DoorContact, firstDoorBoardComponent.selectedDoor2DoorContactStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1DoorLock, firstDoorBoardComponent.selectedDoor1DoorLockStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2DoorLock, firstDoorBoardComponent.selectedDoor2DoorLockStateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1Reader, doorBoardForm.door1Reader.state));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2Reader, doorBoardForm.door2Reader.state));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1AuxRelay1, firstDoorBoardComponent.selectedDoor1AuxRelay1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2AuxRelay1, firstDoorBoardComponent.selectedDoor2AuxRelay1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1AuxRelay2, firstDoorBoardComponent.selectedDoor1AuxRelay2StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2AuxRelay2, firstDoorBoardComponent.selectedDoor2AuxRelay2StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1AuxInput1, firstDoorBoardComponent.selectedDoor1AuxInput1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2AuxInput1, firstDoorBoardComponent.selectedDoor2AuxInput1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door1AuxInput2, firstDoorBoardComponent.selectedDoor1AuxInput2StateOption.value));
    progIoPoints.push(this.setProgIoPoint(doorBoardForm.door2AuxInput2, firstDoorBoardComponent.selectedDoor2AuxInput2StateOption.value));
    return progIoPoints;
  }

  private setProgIoPoint(ioPointTemplate: IoPointTemplate, state) {
    const progIoPoint: ProgIoPoint = new ProgIoPoint();
    // console.log(ioPointTemplate);
    //console.log(this.selectedBoardNumber);
    if (this.selectedBoardNumber !== undefined)
      progIoPoint.boardNumber = this.selectedBoardNumber.value;
    else progIoPoint.boardNumber = this.newBoard.boardNumber;
    progIoPoint.name = ioPointTemplate.defaultLabel;
    progIoPoint.eol = ioPointTemplate.eol;
    progIoPoint.inuse = 0;
    progIoPoint.pointAddress = ioPointTemplate.pointAddress;
    progIoPoint.state = state;
    progIoPoint.type = ioPointTemplate.type;
    return progIoPoint;
  }


  addIOBoard() {

    let progIoPoints: ProgIoPoint[] = this.setAllProgIoPointsIOBoard();
    this.newBoard.progIoPoints = progIoPoints;
    //console.log(this.newBoard);

    this.boardService.addBoard(this.newBoard)
      .then((result) => {
        //  console.log(result);
        this.closeAddDialog();
        this.loadBoards()
          .then()
          .catch(error => console.error(error));
      })
      .catch(error => console.error(error));
  }

  private setAllProgIoPointsIOBoard() {
    let progIoPoints: ProgIoPoint[] = [];
    const firstIOBoardComponent = this.editIOBoardComponent.first;
    const ioBoardForm = firstIOBoardComponent.ioBoardForm;
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input1, firstIOBoardComponent.selectedInput1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input2, firstIOBoardComponent.selectedInput2StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input3, firstIOBoardComponent.selectedInput3StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input4, firstIOBoardComponent.selectedInput4StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input5, firstIOBoardComponent.selectedInput5StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input6, firstIOBoardComponent.selectedInput6StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input7, firstIOBoardComponent.selectedInput7StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.input8, firstIOBoardComponent.selectedInput8StateOption.value));

    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output1, firstIOBoardComponent.selectedOutput1StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output2, firstIOBoardComponent.selectedOutput2StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output3, firstIOBoardComponent.selectedOutput3StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output4, firstIOBoardComponent.selectedOutput4StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output5, firstIOBoardComponent.selectedOutput5StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output6, firstIOBoardComponent.selectedOutput6StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output7, firstIOBoardComponent.selectedOutput7StateOption.value));
    progIoPoints.push(this.setProgIoPoint(ioBoardForm.output8, firstIOBoardComponent.selectedOutput8StateOption.value));
    return progIoPoints;
  }


  showDialogToEdit() {
    this.prepareDialogToEdit()
      .then()
      .catch(error => console.error(error));
  }

  async prepareDialogToEdit() {
    this.newBoard = this.selectedBoards[0];
    this.newBoard.panelOid = this.panelOid;

    this.boardTypes = await this.boardService.getBoardTypes();
    let selectedBoardTypeIndex = this.boardTypes.findIndex(
      boardType => boardType.boardTypeId === this.newBoard.boardTypeId);
    this.selectedBoardType = this.boardTypes[selectedBoardTypeIndex];

    this.displayEditDialog = true;
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  saveBoardChanges() {
    //this.newBoard.boardTypeId = this.selectedBoardType.boardTypeId;
    //console.log(this.editDoorBoardComponent);

    switch (this.selectedBoardType.boardTypeId) {
      case this.BoardTypesEnum.DOOR_BOARD:
      case this.BoardTypesEnum.ACS6000_BOARD:
      case this.BoardTypesEnum.ACS300_BOARD:
        this.editDoorBoard();
        break;
      case this.BoardTypesEnum.IO_BOARD:
        this.editIOBoard();
        break;
      default:
        this.editBoardDefault();
    }


  }

  editBoardDefault() {
    this.boardService.updateBoard(this.newBoard)
      .then(result => {
        this.loadBoards();
        this.closeEditDialog();
      })
      .catch(error => console.error(error));
  }

  editDoorBoard() {
    let progIoPoints: ProgIoPoint[] = this.setAllProgIoPointsDoorBoard();
    this.newBoard.progIoPoints = progIoPoints;
    //console.log(this.newBoard);

    this.boardService.updateBoard(this.newBoard)
      .then((result) => {
        //  console.log(result);
        this.closeEditDialog();
        this.loadBoards()
          .then()
          .catch(error => console.error(error));
      })
      .catch(error => console.error(error));
  }

  editIOBoard() {
    let progIoPoints: ProgIoPoint[] = this.setAllProgIoPointsIOBoard();
    this.newBoard.progIoPoints = progIoPoints;
    //console.log(this.newBoard);

    this.boardService.updateBoard(this.newBoard)
      .then((result) => {
        //  console.log(result);
        this.closeEditDialog();
        this.loadBoards()
          .then()
          .catch(error => console.error(error));
      })
      .catch(error => console.error(error));
  }
}
