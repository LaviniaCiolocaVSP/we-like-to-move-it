import {Component, OnInit} from '@angular/core';
import {PanelsService} from '../../../services/panels.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Panel} from '../../../models/Panel';
import {PanelType} from "../../../models/PanelType";

@Component({
  selector: 'app-panels',
  templateUrl: './panels.component.html',
  styleUrls: ['./panels.component.scss']
})
export class PanelsComponent implements OnInit {
  panelsList: Panel[];
  cols: any[];
  contentLoaded: boolean;
  currentAccountId: number;

  selectedPanels: Panel[];
  panelTypes: PanelType[];
  selectedPanelType: PanelType;

  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;

  newPanel: Panel = new Panel();

  DEFAULT_LOG_PERIOD: number = 60;
  DEFAULT_ANTIPASSBACK_RESET_INTERVAL: number = 0;
  DEFAULT_NETWORK_ID: number = 2;

  constructor(private router: Router, private activateRoute: ActivatedRoute, private panelsService: PanelsService) {
    this.contentLoaded = false;
    this.initCols();
  }

  initCols() {
    this.cols = [
      {field: 'name', header: 'Panel name'},
      {field: 'panelCP', header: 'Panel CP'},
      {field: 'panelType', header: 'Model'},
      {field: 'firmwareVersion', header: 'Firmware version'},
      // {field: 'buildVersion', header: 'Build version'},
      // {field: 'ipAddress', header: 'IP address'},
      // {field: 'lastPanelHeartbeat', header: 'Last panel heartbeat'},
      {field: 'logPeriod', header: 'Log period'}
    ];
  }

  ngOnInit() {
    this.currentAccountId = this.activateRoute.snapshot.params['id'];
    this.loadPanelList()
        .then()
        .catch(error => console.error(error));
  }

  async loadPanelList() {
    this.contentLoaded = false;
    this.selectedPanels = [];

    this.panelsService.getPanelsForAccount(this.currentAccountId)
        .then((result) => {
          this.panelsList = result;
          this.contentLoaded = true;
        });
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }


  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  buildSelectedPanelIds(): number[] {
    let panelIds: number[] = [];

    this.selectedPanels.map(row => {
      panelIds.push(row.brainId);
    });

    return panelIds;
  }

  deletePanels() {
    let panelIds: number[] = this.buildSelectedPanelIds();

    this.panelsService.deletePanels(panelIds)
        .then(result => {
          this.closeDeleteDialog();
          this.loadPanelList();
        })
        .catch(error => console.error(error));
  }

  showDialogToAdd() {
    this.prepareDialogToAdd()
        .then()
        .catch(error => console.error(error));
  }

  async prepareDialogToAdd() {
    this.newPanel = new Panel();

    this.panelTypes = await this.panelsService.getPanelTypes();
    this.selectedPanelType = this.panelTypes[0];

    this.newPanel.logPeriod = this.DEFAULT_LOG_PERIOD;
    this.newPanel.antipassBackResetInterval = this.DEFAULT_ANTIPASSBACK_RESET_INTERVAL;
    this.newPanel.networkId = this.DEFAULT_NETWORK_ID;

    this.displayAddDialog = true;
  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }

  addPanel() {
    this.newPanel.accountId = this.currentAccountId;
    this.newPanel.panelTypeId = this.selectedPanelType.paneTypeId;

    this.panelsService.addPanel(this.newPanel)
        .then((result) => {
          console.log(result);
          this.closeAddDialog();
          this.loadPanelList()
              .then()
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  showDialogToEdit() {
    this.prepareDialogToEdit()
        .then()
        .catch(error => console.error(error));
  }

  async prepareDialogToEdit() {
    this.newPanel = this.selectedPanels[0];

    this.panelTypes = await this.panelsService.getPanelTypes();
    let selectedPanelTypeIndex = this.panelTypes.findIndex(
      panelType => panelType.paneTypeId === this.newPanel.panelTypeId);
    this.selectedPanelType = this.panelTypes[selectedPanelTypeIndex];

    this.displayEditDialog = true;
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  savePanelChanges() {
    this.newPanel.panelTypeId = this.selectedPanelType.paneTypeId;

    this.panelsService.updatePanel(this.newPanel)
        .then(result => {
          this.loadPanelList();
          this.closeEditDialog();
        })
        .catch(error => console.error(error));
  }

  navigateToBoards(panel: Panel): void {
    this.router.navigate(['boards/' + this.currentAccountId + '/' + panel.objectId]);
  }

}
