import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Group} from '../../../models/Group';
import {GroupsService} from '../../../services/groups.service';
import {SelectItem} from 'primeng/api';
import {Site} from '../../../models/Site';
import {SitesService} from '../../../services/sites.service';
import {GroupAntipassback} from '../../../models/GroupAntipassback';
import {GroupPermission} from "../../../models/GroupPermission";
import {ScheduleSummary} from "../../../models/ScheduleSummary";
import {ScheduleService} from "../../../services/schedule.service";


@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {
  groupsList: Array<Group>;
  selectedGroups: Group[] = [];
  currentAccountId: number;
  contentLoaded: boolean;
  panelLog: any;

  newGroup: Group = new Group();

  displayAddDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayEditDialog: boolean = false;

  ampmOptions: SelectItem[] = [];
  selectedAMPM: SelectItem;
  hours: SelectItem[] = [];
  selectedHour: SelectItem;
  minutes: SelectItem[] = [];
  selectedMinute: SelectItem;

  availableSites: Site[];
  selectedSite: Site;

  cols: any[];
  permissionCols: any[];

  groupPermissions: GroupPermission[];
  schedulesForSelectedSite: ScheduleSummary[];

  showLog: boolean = false;
  loading: boolean = false;

  selectedGroupAntipassbackImmunity = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }

  constructor(
    private groupsService: GroupsService,
    private siteService: SitesService,
    private scheduleService: ScheduleService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.contentLoaded = false;
  }

  async loadInitialData() {
    this.cols = [
      {field: 'groupId', header: 'Group ID'},
      {field: 'name', header: 'Group name'},
      {field: 'description', header: 'Description'},
      {field: 'numberOfUsers', header: 'Users'}
    ];

    this.permissionCols = [
      {field: 'siteName', header: 'Site'},
      {field: 'deviceName', header: 'Device name'},
      {field: 'deviceType', header: 'Device type'},
      {field: 'selectedSchedule', header: 'Schedule'}
    ];

    this.ampmOptions = [{label: 'AM', value: 1}, {label: 'PM', value: 2}];
    this.selectedAMPM = this.ampmOptions[0];

    this.hours.push({label: '', value: -1});
    for (var i = 1; i <= 12; i++) {
      let hourString: string = i + '';
      this.hours.push({label: hourString, value: i});
    }

    this.minutes.push({label: '', value: -1});
    for (var i = 0; i <= 59; i++) {
      let minuteString: string = '';
      if (i <= 9)
        minuteString += '0';
      minuteString += i;

      this.minutes.push({label: minuteString, value: i});
    }

    this.selectedHour = this.hours[0];
    this.selectedMinute = this.minutes[0];

    this.availableSites = await this.siteService.getSitesForAccountToEditGroup(this.currentAccountId);
  }

  ngOnInit() {
    this.currentAccountId = this.activatedRoute.snapshot.params['id'];
    this.loadGroupData();
    this.loadInitialData();
  }

  loadGroupData() {
    this.contentLoaded = false;
    this.groupsService.getGroups(this.currentAccountId)
        .then(result => {
          this.groupsList = result;
        })
        .catch(error => console.error(error));
    ;
    this.contentLoaded = true;
    this.selectedGroups = [];

    this.newGroup = new Group();
    const groupAntipassback: GroupAntipassback = new GroupAntipassback();
    groupAntipassback.immuneToAntipassback = false;
    this.newGroup.uiGroupAntipassbackDTO = groupAntipassback;

    console.log('newGroup set:');
    console.log(this.newGroup);
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  async showDialogToAdd() {
    this.displayAddDialog = true;
    this.newGroup = new Group();
  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }


  async showDialogToEdit() {
    let selectedSiteIndex = this.availableSites.findIndex(site => site.siteName === 'Current privileges');
    console.log('Selected site index ' + selectedSiteIndex);
    this.selectedSite = this.availableSites[selectedSiteIndex];

    console.log('Available sites');
    console.log(this.availableSites);
    this.selectedSite = this.availableSites[0];
    console.log('Selected site');
    console.log(this.selectedSite);

    this.newGroup = this.selectedGroups[0];
    console.log('Group to edit');
    console.log(this.newGroup);

    this.selectedGroupAntipassbackImmunity = this.newGroup.uiGroupAntipassbackDTO.immuneToAntipassback;

    this.updateAntipassbackResetTimeFieldsForSelectedGroup();

    this.updateGroupPermissions();

    this.displayEditDialog = true;
  }

  updateGroupPermissions() {
    console.log('Updating group permission for selected site: ' + this.selectedSite.siteObjectId + ', group: ' + this.newGroup.objectId);

    this.groupsService.getGroupPermissions(this.currentAccountId, this.newGroup.objectId, this.selectedSite.siteObjectId)
        .then(result => {
          this.groupPermissions = result;
        })
        .catch(error => console.error(error));
    console.log(this.groupPermissions);
  }

  updateAntipassbackResetTimeFieldsForSelectedGroup() {
    let antipassbackResetTime = this.newGroup.uiGroupAntipassbackDTO.resetTime;
    console.log('Antipassback reset time: ' + antipassbackResetTime);
    if (antipassbackResetTime == null || antipassbackResetTime === undefined || antipassbackResetTime < 0) {
      this.selectedAMPM = this.ampmOptions[0];
      this.selectedHour = this.hours[0];
      this.selectedMinute = this.minutes[0];
    } else {
      //antipassbackResetTime 0 in the database should be shown as 12:00 AM in UI
      if (antipassbackResetTime === 0) {
        antipassbackResetTime += (12 * 60);
        console.log('Antipassback reset time: ' + antipassbackResetTime);
      }
      let antipassbackHours = Math.floor(antipassbackResetTime / 60);
      let antipassbackMinutes = antipassbackResetTime % 60;
      console.log('antipassbackMinutes' + antipassbackMinutes);

      if (antipassbackHours > 12) {
        this.selectedAMPM = this.ampmOptions[1];
        antipassbackHours -= 12;
      } else
        this.selectedAMPM = this.ampmOptions[0];

      let selectedHourIndex = this.hours.findIndex(hour => hour.value === antipassbackHours);
      this.selectedHour = this.hours[selectedHourIndex];

      let selectedMinuteIndex = this.minutes.findIndex(minute => minute.value === antipassbackMinutes);
      console.log('selectedMinuteIndex ' + selectedMinuteIndex);
      this.selectedMinute = this.minutes[selectedMinuteIndex];
      console.log(this.minutes);
    }
  }

  closeEditDialog() {
    console.log('Groups list after delete:');
    console.log(this.groupsList);
    this.displayEditDialog = false;
  }


  async addGroup() {
    this.newGroup.accountId = this.currentAccountId;

    console.log('Adding group');
    console.log(this.newGroup);

    await this.groupsService.addGroup(this.newGroup);
    this.closeAddDialog();
    this.loadGroupData();
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  deleteGroups() {
    const groupObjectIds: number[] = [];
    this.selectedGroups.map(row => {
      groupObjectIds.push(row.objectId);
    });

    this.groupsService.deleteGroups(groupObjectIds)
        .then(result => {
          this.closeDeleteDialog();
          this.loadGroupData();
        })
        .catch(error => console.error(error));
  }

  computeResetTimeForSelectedGroup(): number {
    let resetTime: number = 0;

    if (this.selectedHour.value !== -1 && this.selectedMinute.value !== -1) {
      let hours: number = this.selectedHour.value;
      if (this.selectedAMPM.label === 'PM' && hours < 12)
        hours += 12;

      resetTime = 60 * hours + this.selectedMinute.value;
    } else {
      //-1 represents not selected values
      resetTime = -1;
    }

    return resetTime;
  }

  async saveGroupChanges() {
    // this.newGroup.securityGroupTypeId = this.selectedGroupType.id;
    this.newGroup.uiGroupAntipassbackDTO.resetTime = this.computeResetTimeForSelectedGroup();
    this.newGroup.uiGroupAntipassbackDTO.immuneToAntipassback = this.selectedGroupAntipassbackImmunity;

    console.log('Updating group...');
    console.log(this.newGroup);
    console.log(this.groupPermissions);

    await this.groupsService.updateGroup(this.newGroup, this.groupPermissions);
    this.closeEditDialog();
    this.loadGroupData();
  }
}
