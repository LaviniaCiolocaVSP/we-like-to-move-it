import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GroupsComponent} from './container/groups.component';
import {
  ButtonModule, CheckboxModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  SharedModule
} from 'primeng/primeng';
import {HttpClientModule} from '@angular/common/http';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [GroupsComponent],
  imports: [
    CommonModule,
    TableModule,
    HttpClientModule,
    DataTableModule,
    SharedModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    CheckboxModule,
    ButtonModule,
    DropdownModule
  ]
})
export class GroupsModule {
}
