import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './container/home.component';
import {UploadComponent} from '../upload/upload.component';
import {HttpClientModule} from '@angular/common/http';
import {AccountDownloadComponent} from '../account-download/account-download.component';
import {AppRoutingModule} from '../../app-routing.module';
import {AccountService} from '../../services/account.service';
import {FormUploadModule} from '../form-upload/form-upload.module';
import {ButtonModule, ProgressBarModule, ProgressSpinnerModule} from 'primeng/primeng';
import {FileUploadModule} from 'primeng/fileupload';

@NgModule({
  declarations: [
    HomeComponent,
    UploadComponent,
    AccountDownloadComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    AppRoutingModule,
    FormUploadModule,
    ProgressBarModule,
    ProgressSpinnerModule,
    ButtonModule, FileUploadModule
  ],
  exports: [
    UploadComponent
  ],
  providers: [
    AccountService
  ]
})
export class HomeModule {
}
