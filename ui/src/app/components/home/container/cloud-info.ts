export interface CloudInfo {
  name: string;
  active: boolean;
}
