import {Component, OnInit, ViewChild} from '@angular/core';
import {AccountService} from '../../../services/account.service';
import {Account} from '../../../models/Account';
import {Router} from '@angular/router';
import {SessionService} from '../../../services/session.service';
import {saveAs} from 'node_modules/file-saver';
import {JsonFilesService} from '../../../services/json-files.service';
import {BrivoAccount} from '../../../models/BrivoAccount';
import {DatabaseConnection} from '../../../models/DatabaseConnection';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  @ViewChild('fileUpload') fileUpload: any;
  title = 'Definitely NOT Reference Panel Server';
  databaseConnection: DatabaseConnection = null;

  currentAction: string = null;
  currentActionCurrentAccount: string = null;

  uploadedJSONFiles: Account[];
  selectedAccount: Account;

  currentDatabaseAccount: BrivoAccount;
  accountIsLoaded = true;

  displayProgress: boolean = false;
  loadingError: boolean = false;

  constructor(private sessionService: SessionService, private router: Router, private accountService: AccountService,
              private jsonFilesService: JsonFilesService) {
  }

  async loadUploadedJSONFiles() {
    this.databaseConnection = await this.accountService.getDatabaseConnectionName();

    this.jsonFilesService.getJSONFiles()
      .then(result => {
        this.uploadedJSONFiles = result;

        this.uploadedJSONFiles.forEach((accountItem) => {
          accountItem.accountName = accountItem.fileName;
        });
      });
  }

  refreshUploadedJSONFiles(): void {
    this.loadUploadedJSONFiles()
      .catch(error => console.error(error));
  }

  async loadCurrentAccount() {
    this.accountIsLoaded = false;
    this.currentDatabaseAccount = await this.accountService.getCurrentDatabaseAccount();
    this.accountIsLoaded = true;
  }

  accountIsSelected(account: Account) {
    const selectedAccountName = this.selectedAccount ? this.selectedAccount.accountName : undefined;
    return selectedAccountName ? selectedAccountName === account.accountName : false;
  }

  ngOnInit() {
    this.loadUploadedJSONFiles()
      .then()
      .catch(error => console.error(error));
    this.loadCurrentAccount()
      .then()
      .catch(error => console.error(error));
  }

  goToDashboard() {
    this.router.navigate(['dashboard']);
  }

  setActive(account: Account) {
    this.selectedAccount = account;
  }

  loadAndStartUsingSelectedCloud(): void {
    console.log('playing audio');
    this.playAudio();
    console.log('playing audio');

    this.currentAction = `Loading and starting to use the account '${this.selectedAccount.accountName}'...`;
    this.displayProgress = true;
    this.loadingError = false;
    this.accountService.selectAccount(this.selectedAccount.accountName)
      .then(response => {
        console.log(response);

        this.currentAction = `The account '${this.selectedAccount.accountName}' was successfully loaded`;

        //this.sessionService.updateOfComponent(1);

        this.loadCurrentAccount()
          .then((response) => {
            console.log('account loaded');
            this.displayProgress = false;
          })
          .catch(error => {
            console.log('error on loading account');
            console.error(error);
            this.displayProgress = false;
            this.loadingError = true;
            this.currentAction = `There was a problem trying to load account ${this.selectedAccount.accountName}`;
          });
      })
      .catch((err) => {
        this.currentAction = `There was a problem trying to load account ${this.selectedAccount.accountName}`;
        console.log('error on select account');
        console.log(err);
        this.displayProgress = false;
        this.loadingError = true;
        this.currentDatabaseAccount = null;
      });
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "assets/audio/move-it.mov";
    audio.load();
    audio.play();
  }

  async deleteSelectedCloud() {
    this.currentAction = `Deleting the account '${this.selectedAccount.accountName}'...`;
    const message = await this.jsonFilesService.deleteAccount(this.selectedAccount.accountName);
    console.log(message);
    this.refreshUploadedJSONFiles();
    this.currentAction = null;
    this.selectedAccount = null;
  }

  exportCurrentDatabaseAccount() {
    if (this.currentDatabaseAccount != null) {
      this.currentActionCurrentAccount = 'Downloading current account...';
      this.accountService.downloadCurrentAccount(this.currentDatabaseAccount.accountId)
        .then(response => {
          this.saveToFileSystem(response);
        })
        .catch(error => console.error(error));
    } else {
      this.currentActionCurrentAccount = 'No account to download...';
    }
  }

  downloadSelectedConfig() {
    this.currentAction = `Downloading the config for the account '${this.selectedAccount.accountName}'...`;
    this.jsonFilesService
      .download(this.selectedAccount.accountName)
      .then(response => {
        this.saveToFileSystem(response);
      })
      .catch(error => console.error(error));
  }

  private saveToFileSystem(response) {
    const jsonContent = JSON.stringify(response, null, ' ');

    const blob = new Blob([jsonContent], {type: 'application/json'});

    const filename = this.getFileName(response.name);
    console.log('filename: ' + filename);
    saveAs(blob, filename);

    this.currentActionCurrentAccount = 'The current account was successfully downloaded!';
  }

  getFileName(responseName: string): string {
    const date = new Date();
    let fileName = 'RPS-' + date.getFullYear();
    const currentMonth: number = date.getMonth() + 1;
    let month: string = currentMonth + '';
    let day: string = date.getDate() + '';

    if (currentMonth < 10) {
      month = '0' + currentMonth;
    }
    if (date.getDate() < 10) {
      day = '0' + date.getDate();
    }

    fileName += '_' + month + '_' + day + '-' + responseName;

    return fileName;
  }

  onUpload(event, form) {
    console.log('playing audio 1');
    this.playAudio();
    console.log('playing audio');

    this.currentAction = 'Uploading the selected file';
    this.displayProgress = true;
    this.loadingError = false;
    console.log(event.files[0]);
    this.jsonFilesService
      .createAccountFromFile(event.files[0])
      .then(result => {

        this.displayProgress = false;
        this.currentAction = 'The account definition file was successfully uploaded!';
        form.clear();
        this.fileUpload.clear();
        this.refreshUploadedJSONFiles();

        this.selectedAccount = new Account();
        let fileNameWithExtension = event.files[0].name;
        let fileNameWithoutExtension = fileNameWithExtension.slice(0, -5);

        this.selectedAccount.accountName = fileNameWithoutExtension;

        this.currentAction = `Loading and starting to use the account '${this.selectedAccount.accountName}'...`;
        this.displayProgress = true;
        this.loadingError = false;
        this.accountService.selectAccount(this.selectedAccount.accountName)
            .then(response => {
              console.log(response);

              this.currentAction = `The account '${this.selectedAccount.accountName}' was successfully loaded`;

              this.loadCurrentAccount()
                  .then((response) => {
                    console.log('account loaded');
                    this.displayProgress = false;
                  })
                  .catch(error => {
                    console.log('error on loading account');
                    console.error(error);
                    this.displayProgress = false;
                    this.loadingError = true;
                    this.currentAction = `There was a problem trying to load account ${this.selectedAccount.accountName}`;
                  });
            })
            .catch((err) => {
              this.currentAction = `There was a problem trying to load account ${this.selectedAccount.accountName}`;
              console.log('error on select account');
              console.log(err);
              this.displayProgress = false;
              this.loadingError = true;
              this.currentDatabaseAccount = null;
            });
      })
      .catch((error) => {
        console.log(error);
        this.displayProgress = false;
        this.currentAction = 'There was a problem trying to upload the file';
        this.loadingError = true;
        form.clear();
        this.fileUpload.clear();
      });
  }

}
