import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {AccountService} from '../../services/account.service';
import {saveAs} from 'node_modules/file-saver';
import {SessionService} from '../../services/session.service';
import {Subscription} from 'rxjs';
import {BrivoAccount} from '../../models/BrivoAccount';

@Component({
  selector: 'account-download',
  templateUrl: './account-download.component.html',
  styleUrls: ['./account-download.component.scss']
})
export class AccountDownloadComponent implements OnInit {

  brivoAccounts: BrivoAccount[];
  selectedBrivoAccount: BrivoAccount;
  pageSize = 100;
  pageIndex = 0;

  listLoaded = true;
  subscription: Subscription;
  downloadMessage: string;
  @ViewChild('filterValue') input: ElementRef;
  private loadingMessage = 'Loading Brivo accounts list...';

  constructor(private accountService: AccountService, private sessionService: SessionService) {
  }

  ngOnInit() {
    this.loadBrivoAccounts()
        .then()
        .catch(error => console.error(error));

    this.subscription = this.sessionService.communicationItem.subscribe(
      item => {
        this.loadBrivoAccounts()
            .then()
            .catch(error => console.error(error));
      })
    ;
  }

  public clear() {
    this.input.nativeElement.value = '';
  }

  async loadBrivoAccounts() {
    this.listLoaded = false;
    const filterName = this.input.nativeElement.value;
    this.pageIndex = 0;


    await this.accountService.getAccountsFromBrivo(filterName, this.pageIndex, this.pageSize)
              .then(response => {
                this.brivoAccounts = response;
              });

    this.listLoaded = true;
    this.selectedBrivoAccount = this.brivoAccounts[0];
  }

  async onScroll(event: any) {

    if (event.target.offsetHeight + event.target.scrollTop === event.target.scrollHeight) {
      console.log('scrolling');
      const filterName = this.input.nativeElement.value;
      this.pageIndex++;
      const brivoAuxList = await this.accountService.getAccountsFromBrivo(filterName, this.pageIndex, this.pageSize);
      this.brivoAccounts = this.brivoAccounts.concat(brivoAuxList);
    }
  }

  setActive(account: BrivoAccount) {
    this.selectedBrivoAccount = account;
  }

  accountIsSelected(account: BrivoAccount) {
    return account === this.selectedBrivoAccount;
  }

  playAudio(){
    let audio = new Audio();
    audio.src = "assets/audio/i-like-to-move-it-short.mov";
    audio.load();
    audio.play();
  }

  downloadBrivoAccount() {
    this.playAudio();
    this.downloadMessage = 'The account selected will download now...';
    this.accountService.downloadBrivoAccount(this.selectedBrivoAccount.accountId)
        .then(response => {
          this.saveToFileSystem(response);
          this.downloadMessage = 'The account was successfully downloaded!';
        })
        .catch(error => {
          console.error(error);
          this.downloadMessage = error;
        });
  }

  private saveToFileSystem(response) {
    const jsonContent = JSON.stringify(response, null, ' ');

    const blob = new Blob([jsonContent], {type: "application/json"});

    const filename: string = this.getFileName(response.name);
    saveAs(blob, filename);
  }


  getFileName(responseName: string): string {
    const date = new Date();
    let fileName = 'RPS-' + date.getFullYear();
    const currentMonth: number = date.getMonth() + 1;
    let month: string = currentMonth + '';
    let day: string = date.getDate() + '';

    if (currentMonth < 10) {
      month = '0' + currentMonth;
    }
    if (date.getDate() < 10) {
      day = '0' + date.getDate();
    }

    fileName += '_' + month + '_' + day + '-' + responseName;

    return fileName;
  }
}
