import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Door} from '../../../models/Door';
import {DevicesService} from '../../../services/devices.service';
import {DoorData} from '../../../models/DoorData';
import {Panel} from '../../../models/Panel';
import {PanelsService} from '../../../services/panels.service';
import {Board} from '../../../models/Board';
import {BoardsService} from '../../../services/boards.service';
import {Schedule} from '../../../models/Schedule';
import {ScheduleService} from '../../../services/schedule.service';
import {BoardNode} from '../../../models/BoardNode';
import {Site} from "../../../models/Site";
import {SitesService} from "../../../services/sites.service";

@Component({
  selector: 'app-doors',
  templateUrl: './doors.component.html',
  styleUrls: ['./doors.component.scss']
})
export class DoorsComponent implements OnInit {
  cols: any[];

  contentLoaded: boolean;
  currentAccountId: number;
  panelLog: any;

  doorsList: Array<Door>;
  selectedDoors: Door[] = [];
  selectedDoorForDetails: Door;
  newDoor: Door;

  sitesList: Site[];
  selectedSite: Site;

  panelList: Panel[] = [];
  selectedPanel: Panel;

  boardsForPanel: Board[] = [];
  selectedBoard: Board;
  nodesForBoard: BoardNode[] = [];
  selectedNode: BoardNode;

  schedulesList: Schedule[];
  selectedDeviceUnlockSchedule: Schedule;
  selectedTwoFactorSchedule: Schedule;
  selectedCardRequiredSchedule: Schedule;

  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;
  displayDetailsDialog: boolean = false;

  DEFAULT_DOOR_AJAR_THRESHOLD: number = 120;
  DEFAULT_INVALID_PINS_THRESHOLD: number = 3;
  DEFAULT_INVALID_PINS_SHUTOUT: number = 120;
  DEFAULT_PASSTHROUGH_PERIOD: number = 5;
  DEFAULT_TWO_FACTOR_INTERVAL: number = 3;
  DEFAULT_ALARM_SHUNT_DURATION: number = 1;
  CHECKBOX_ENABLED_VALUE: boolean = true;
  CHECKBOX_DISABLED_VALUE: boolean = false;
  DEFAULT_CONTROL_FROM_BROWSER = false;

  INGRESS: number = 1;
  EGRESS: number = 2;
  NEITHER: number = 0;

  showLog: boolean = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }

  constructor(private doorsService: DevicesService, private activeRoute: ActivatedRoute, private router: Router,
              private panelService: PanelsService, private boardService: BoardsService,
              private scheduleService: ScheduleService, private siteService: SitesService) {
    this.contentLoaded = false;
    this.initCols();
    this.initSelectedDoorForDetails();
    this.initNewDoor();
  }

  initNewDoor() {
    this.newDoor = new Door();
    this.newDoor.doorData = new DoorData();
  }

  initCols() {
    this.cols = [
      {field: 'deviceObjectId', header: 'Device OID'},
      {field: 'siteName', header: 'Site'},
      {field: 'panelSerialNumber', header: 'Panel ID'},
      {field: 'name', header: 'Door name'},
      {field: 'boardDescription', header: 'Board'},
      {field: 'boardNumber', header: 'Board number'},
      {field: 'nodeNumber', header: 'Node'}
    ];
  }

  initSelectedDoorForDetails() {
    this.selectedDoorForDetails = new Door();
    this.selectedDoorForDetails.doorData = new DoorData();
  }

  ngOnInit() {
    this.currentAccountId = this.activeRoute.snapshot.params['id'];
    this.loadDoors()
        .then()
        .catch(error => console.error(error));
  }

  async deleteDoors() {
    const doorIds = this.selectedDoors.map(item => item.deviceId);

    this.selectedPanel = await this.panelService.getPanelByBrainId(this.selectedDoors[0].brainId);
    console.log(this.selectedPanel);

    this.doorsService.deleteDoors(doorIds)
        .then(result => {
          this.closeDeleteDialog();
          this.loadDoors()
              .then()
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  async loadDoors() {
    this.selectedDoors = [];
    this.contentLoaded = false;
    this.doorsList = await this.doorsService.getDoorsForAccount(this.currentAccountId);
    this.contentLoaded = true;
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  showDialogToAdd() {
    this.siteService.getSitesForAccountWithNone(this.currentAccountId)
        .then(result => {
          this.sitesList = result;
        })
        .catch(error => console.error(error));

    this.panelService.getPanelsForAccountWithNone(this.currentAccountId)
        .then(result => {
          this.panelList = result;
        })
        .catch(error => console.error(error));

    this.scheduleService.getSchedulesForAccountWithNone(this.currentAccountId)
        .then(result => {
          this.schedulesList = result;
        })
        .catch(error => console.error(error));

    this.initNewDoor();

    this.newDoor.doorData.notifyOnAjar = this.CHECKBOX_ENABLED_VALUE;
    this.newDoor.doorData.doorAjar = this.DEFAULT_DOOR_AJAR_THRESHOLD;
    this.newDoor.doorData.maxInvalid = this.DEFAULT_INVALID_PINS_THRESHOLD;
    this.newDoor.doorData.invalidShutout = this.DEFAULT_INVALID_PINS_SHUTOUT;
    this.newDoor.doorData.passthrough = this.DEFAULT_PASSTHROUGH_PERIOD;
    this.newDoor.twoFactorInterval = this.DEFAULT_TWO_FACTOR_INTERVAL;
    this.newDoor.doorData.useAlarmShunt = this.CHECKBOX_ENABLED_VALUE;
    this.newDoor.doorData.alarmDelay = this.DEFAULT_ALARM_SHUNT_DURATION;
    this.newDoor.doorData.notifyOnForced = this.CHECKBOX_DISABLED_VALUE;
    this.newDoor.doorData.rexFiresDoor = this.CHECKBOX_DISABLED_VALUE;
    this.newDoor.inOut = this.NEITHER;
    this.newDoor.controlFromBrowser = this.DEFAULT_CONTROL_FROM_BROWSER;

    this.selectedSite = undefined;
    this.selectedPanel = undefined;
    this.selectedBoard = undefined;
    this.selectedNode = undefined;
    this.selectedDeviceUnlockSchedule = undefined;
    this.selectedCardRequiredSchedule = undefined;
    this.selectedTwoFactorSchedule = undefined;

    this.displayAddDialog = true;
  }

  panelSelectionChanged() {
    this.boardService.getBoardsForPanelWithNone(this.selectedPanel.objectId)
        .then(result => {
          this.boardsForPanel = result;
        })
        .catch(error => console.error(error));
    this.selectedBoard = undefined;
    this.selectedNode = undefined;
  }

  boardSelectionChanged() {
    this.boardService.getNodesForBoardWithNone(this.selectedBoard.objectId)
        .then(result => {
          this.nodesForBoard = result;
        })
        .catch(error => console.error(error));

    this.selectedNode = undefined;
  }

  async showDialogToEdit() {
    this.newDoor = this.selectedDoors[0];
    console.log(this.newDoor);

    this.schedulesList = await this.scheduleService.getSchedulesForAccountWithNone(this.currentAccountId);
    console.log(this.schedulesList);

    this.selectedPanel = await this.panelService.getPanelByBrainId(this.newDoor.brainId);
    console.log(this.selectedPanel);

    let selectedUnlockScheduleIndex = this.schedulesList.findIndex(schedule => schedule.scheduleId === this.newDoor.doorUnlockScheduleId);
    if (selectedUnlockScheduleIndex < 0)
      selectedUnlockScheduleIndex = 0;
    this.selectedDeviceUnlockSchedule = this.schedulesList[selectedUnlockScheduleIndex];
    console.log(selectedUnlockScheduleIndex);

    let cardRequiredScheduleIndex = this.schedulesList.findIndex(schedule => schedule.scheduleId === this.newDoor.cardRequiredScheduleId);
    if (cardRequiredScheduleIndex < 0)
      cardRequiredScheduleIndex = 0;
    this.selectedCardRequiredSchedule = this.schedulesList[cardRequiredScheduleIndex];
    console.log(cardRequiredScheduleIndex);

    let twoFactorScheduleIndex = this.schedulesList.findIndex(schedule => schedule.scheduleId === this.newDoor.twoFactorScheduleId);
    if (twoFactorScheduleIndex < 0)
      twoFactorScheduleIndex = 0;
    this.selectedTwoFactorSchedule = this.schedulesList[twoFactorScheduleIndex];
    console.log(twoFactorScheduleIndex);

    if (this.newDoor.maxRexExtension == -1)
      this.newDoor.maxRexExtension = undefined;

    this.displayEditDialog = true;
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  saveDoorChanges() {
    this.newDoor.twoFactorScheduleId = this.selectedTwoFactorSchedule.scheduleId;
    this.newDoor.cardRequiredScheduleId = this.selectedCardRequiredSchedule.scheduleId;
    this.newDoor.doorUnlockScheduleId = this.selectedDeviceUnlockSchedule.scheduleId;

    this.doorsService.updateDoor(this.newDoor)
        .then(result => {
          this.loadDoors();
          this.closeEditDialog();
        })
        .catch(error => console.error(error));
  }

  showDoorDetails(door: Door) {
    this.selectedDoorForDetails = door;

    this.displayDetailsDialog = true;
  }

  closeDetailsDialog() {
    this.displayDetailsDialog = false;
  }

  closeAddDialog() {
    // this.selectedSite = undefined;
    // this.selectedPanel = undefined;
    // this.selectedBoard = undefined;
    // this.selectedNode = undefined;

    this.displayAddDialog = false;
  }

  addDoor() {
    this.newDoor.siteId = this.selectedSite.siteId;
    this.newDoor.accountId = this.currentAccountId;
    this.newDoor.boardNumber = this.selectedBoard.boardNumber;
    this.newDoor.nodeNumber = this.selectedNode.nodeNumber;
    this.newDoor.brainId = this.selectedPanel.brainId;

    if (this.selectedDeviceUnlockSchedule != undefined && this.selectedDeviceUnlockSchedule.scheduleId > 0)
      this.newDoor.doorUnlockScheduleId = this.selectedDeviceUnlockSchedule.scheduleId;

    if (this.selectedCardRequiredSchedule != undefined)
      this.newDoor.cardRequiredScheduleId = this.selectedCardRequiredSchedule.scheduleId;
    else
      this.newDoor.cardRequiredScheduleId = 0;

    if (this.selectedTwoFactorSchedule != undefined)
      this.newDoor.twoFactorScheduleId = this.selectedTwoFactorSchedule.scheduleId;
    else
      this.newDoor.twoFactorScheduleId = 0;

    console.log('Adding door');
    console.log(this.newDoor);
    this.doorsService.addDoor(this.newDoor)
        .then((result) => {
          console.log(result);
          this.closeAddDialog();
          this.loadDoors()
              .then()
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }
}
