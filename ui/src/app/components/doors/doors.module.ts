import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule,
  CalendarModule,
  CheckboxModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  MultiSelectModule, RadioButtonModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {DoorsComponent} from "./container/doors.component";

@NgModule({
  declarations: [DoorsComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    MultiSelectModule,
    CheckboxModule,
    RadioButtonModule
  ]
})
export class DoorsModule {
}
