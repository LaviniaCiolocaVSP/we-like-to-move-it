import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HolidaysComponent} from './container/holidays.component';
import {
  ButtonModule,
  CalendarModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  MultiSelectModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';


@NgModule({
  declarations: [HolidaysComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    MultiSelectModule
  ]
})
export class HolidaysModule {
}
