import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Holiday} from '../../../models/Holiday';
import {HolidayService} from '../../../services/holiday.service';
import {GroupsService} from '../../../services/groups.service';
import {Group} from '../../../models/Group';
import {Schedule} from 'src/app/models/Schedule';
import {ScheduleService} from '../../../services/schedule.service';

@Component({
  selector: 'app-holidays',
  templateUrl: './holidays.component.html',
  styleUrls: ['./holidays.component.scss']
})
export class HolidaysComponent implements OnInit {
  holidayList: Array<Holiday>;
  selectedHolidays: Holiday[] = [];

  currentAccountId: number;
  contentLoaded: boolean;
  panelLog: any;

  groupList: Group[] = [];
  selectedGroup: Group;
  newHoliday: Holiday = new Holiday();
  displayAddDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayEditDialog: boolean = false;
  displayDetailsDialog: boolean = false;

  cols: any[];
  schedulesCols: any[];

  schedules: Schedule[] = [];
  selectedSchedules: Schedule[] = [];
  holidaySchedules: Schedule[] = [];

  showLog: boolean = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }


  constructor(
    private holidayService: HolidayService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private groupsService: GroupsService,
    private scheduleService: ScheduleService
  ) {
    this.contentLoaded = false;
    this.cols = [
      {field: 'name', header: 'Name'},
      {field: 'from', header: 'Start date'},
      {field: 'to', header: 'End date'},
      {field: 'site', header: 'Site'}
    ];
    this.schedulesCols = [
      {field: 'scheduleId', header: 'Schedule Id'},
      {field: 'scheduleName', header: 'Name'},
      {field: 'description', header: 'Description'},
      {field: 'siteName', header: 'Site'}
    ];

  }

  ngOnInit() {
    this.currentAccountId = this.activatedRoute.snapshot.params['id'];
    this.loadHolidayData();
  }

  async loadHolidayData() {
    this.contentLoaded = false;
    this.holidayList = await this.holidayService.getHolidays(this.currentAccountId);
    this.holidayList.map(row => {
      row.isEditable = false;
    });
    this.contentLoaded = true;
    this.selectedHolidays = [];
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  async showDialogToAdd() {
    this.newHoliday = new Holiday();
    this.selectedSchedules = [];

    let today = new Date();
    this.newHoliday.from = new Date(today.getFullYear(), today.getMonth(), today.getDay(), 0, 0, 0, 0);
    this.newHoliday.to = new Date(today.getFullYear(), today.getMonth(), today.getDay(), 23, 59, 59, 0);

    this.groupList = await this.groupsService.getDeviceGroupsWithNone(this.currentAccountId);
    this.selectedGroup = this.groupList[0];

    this.schedules = await this.scheduleService.getSchedules(this.currentAccountId);

    this.displayAddDialog = true;
  }

  addHoliday() {
    this.newHoliday.accountId = this.currentAccountId;
    this.newHoliday.siteId = this.selectedGroup.objectId;

    let dateFrom: Date = new Date(this.newHoliday.from.getTime() - (this.newHoliday.from.getTimezoneOffset() * 60000));
    let dateTo: Date = new Date(this.newHoliday.to.getTime() - (this.newHoliday.to.getTimezoneOffset() * 60000));
    /*    this.newHoliday.from = dateFrom;
        this.newHoliday.to = dateTo;*/

    this.newHoliday.scheduleDTOSet = this.selectedSchedules;
    this.holidayService.addHoliday(this.newHoliday, dateFrom, dateTo).then((result) => {
      this.closeAddDialog();
      this.loadHolidayData();
    });

  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  deleteHolidays() {
    let holiday_ids: number[] = [];
    this.selectedHolidays.map(row => {
      holiday_ids.push(row.holidayId);
    });
    console.log(holiday_ids);

    this.holidayService.deleteHolidays(holiday_ids).then((result) => {
      console.log(result);
      this.closeDeleteDialog();
      this.loadHolidayData();

    });
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  async showDialogToEdit() {
    this.displayEditDialog = true;
    this.newHoliday = this.selectedHolidays[0];
    let dateFrom: Date = new Date(this.selectedHolidays[0].from);
    let dateTo: Date = new Date(this.selectedHolidays[0].to);
    this.newHoliday.from = dateFrom;
    this.newHoliday.to = dateTo;
    console.log(this.newHoliday);

    this.groupList = await this.groupsService.getDeviceGroupsWithNone(this.currentAccountId);
    let selectedGroupIndex = this.groupList.findIndex(site => site.objectId === this.newHoliday.siteId);
    this.selectedGroup = this.groupList[selectedGroupIndex];

    this.schedules = await this.scheduleService.getSchedules(this.currentAccountId);
    this.selectedSchedules = await this.scheduleService.getSchedulesForHoliday(this.selectedHolidays[0].holidayId);
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  saveHolidaysChanges() {
    let dateFrom: Date = new Date(this.newHoliday.from.getTime() - (this.newHoliday.from.getTimezoneOffset() * 60000));
    let dateTo: Date = new Date(this.newHoliday.to.getTime() - (this.newHoliday.to.getTimezoneOffset() * 60000));

    this.newHoliday.siteId = this.selectedGroup.objectId;
    this.newHoliday.scheduleDTOSet = this.selectedSchedules;
    this.holidayService.updateHoliday(this.newHoliday, dateFrom, dateTo).then((result) => {
      console.log(result);
      this.closeEditDialog();
      this.loadHolidayData();
      this.newHoliday = new Holiday();
    });
  }

  async showDialogDetails(holiday: Holiday) {
    this.newHoliday = holiday;
    this.holidaySchedules = await this.scheduleService.getSchedulesForHoliday(holiday.holidayId);
    this.displayDetailsDialog = true;
    console.log(this.holidaySchedules);
  }

  closeDetailsDialog() {
    this.displayDetailsDialog = false;
  }
}
