import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  ButtonModule,
  CalendarModule,
  CheckboxModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  MultiSelectModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {DevicesComponent} from "./container/devices.component";
import { SwitchComponent } from './templates/switch/switch.component';
import { TimerComponent } from './templates/timer/timer.component';
import { EventComponent } from './templates/event/event.component';
import { ValidCredentialComponent } from './templates/valid-credential/valid-credential.component';

@NgModule({
  declarations: [DevicesComponent, SwitchComponent, TimerComponent, EventComponent, ValidCredentialComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    MultiSelectModule,
    CheckboxModule
  ]
})
export class DevicesModule {
}
