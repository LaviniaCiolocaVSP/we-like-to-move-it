import {Component, OnInit, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Device} from '../../../models/Device';
import {DevicesService} from '../../../services/devices.service';
import {Panel} from "../../../models/Panel";
import {PanelsService} from "../../../services/panels.service";
import {SelectItem} from "primeng/api";
import {Site} from "../../../models/Site";
import {SitesService} from "../../../services/sites.service";
import {SwitchComponent} from "../templates/switch/switch.component";
import {TimerComponent} from "../templates/timer/timer.component";
import {EventComponent} from "../templates/event/event.component";
import {ValidCredentialComponent} from "../templates/valid-credential/valid-credential.component";

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.scss']
})
export class DevicesComponent implements OnInit {
  devicesList: Array<Device>;
  selectedDevices: Device[] = [];

  newDevice: Device;
  selectedDeviceForDetails: Device;

  contentLoaded: boolean;
  currentAccountId: number;

  panelList: Panel[] = [];
  selectedPanel: Panel;

  sitesList: Site[];
  selectedSite: Site;

  displayDeleteDialog: boolean = false;
  displayAddDialog: boolean = false;
  displayEditDialog: boolean = false;
  displayDetailsDialog: boolean = false;

  selectedDeviceType: SelectItem;
  deviceTypes: SelectItem[];
  private cols: any[];

  ADD_DEVICE_ACTION = 'ADD';
  EDIT_DEVICE_ACTION = 'EDIT';

  SWITCH: string = 'SWITCH';
  TIMER: string = 'TIMER';
  EVENT_TRACK: string = 'EVENT';
  VALID_CREDENTIAL: string = 'WIEGAND';

  isAddDeviceButtonDisabled: boolean = false;

  @ViewChild(SwitchComponent) private switchComponent: SwitchComponent;
  @ViewChild(TimerComponent) private timerComponent: TimerComponent;
  @ViewChild(EventComponent) private eventComponent: EventComponent;
  @ViewChild(ValidCredentialComponent) private validCredentialComponent: ValidCredentialComponent;

  showLog: boolean = false;

  constructor(private devicesService: DevicesService, private panelService: PanelsService,
              private activeRoute: ActivatedRoute, private router: Router, private siteService: SitesService) {
    this.contentLoaded = false;
    this.initCols();
    this.initSelectedDeviceForDetails();
    this.initDeviceTypes();
    console.log('Device types');
    console.log(this.deviceTypes);
    this.newDevice = new Device();
  }

  initDeviceTypes() {
    this.deviceTypes = [
      {label: '', value: 'NONE'},
      {label: this.SWITCH, value: this.SWITCH},
      {label: this.TIMER, value: this.TIMER},
      {label: 'VALID CREDENTIAL', value: this.VALID_CREDENTIAL},
      {label: 'EVENT TRACK', value: this.EVENT_TRACK}
    ];
  }

  initCols() {
    this.cols = [
      {field: 'deviceObjectId', header: 'Device OID'},
      {field: 'siteName', header: 'Site'},
      {field: 'panelSerialNumber', header: 'Panel ID'},
      {field: 'name', header: 'Device name'},
      {field: 'deviceType', header: 'Device Type'},
    ];
  }

  initSelectedDeviceForDetails() {
    this.selectedDeviceForDetails = new Device();
    // this.selectedDeviceForDetails.deviceData = new DeviceData();
  }

  ngOnInit() {
    this.currentAccountId = this.activeRoute.snapshot.params['id'];
    this.loadDevices()
        .then()
        .catch(error => console.error(error));
  }


  hideShowLog() {
    this.showLog = !this.showLog;
  }

  async deleteDevices() {
    const deviceIds = this.selectedDevices.map(item => item.deviceId);

    this.selectedPanel = await this.panelService.getPanelByBrainId(this.selectedDevices[0].brainId);
    console.log(this.selectedPanel);

    this.devicesService.deleteDevices(deviceIds)
        .then(result => {
          this.closeDeleteDialog();
          this.loadDevices()
              .then()
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  async loadDevices() {
    this.selectedDevices = [];
    this.contentLoaded = false;
    this.devicesList = await this.devicesService.getDevicesForAccount(this.currentAccountId);
    this.contentLoaded = true;
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  showDialogToAdd() {
    this.siteService.getSitesForAccountWithNone(this.currentAccountId)
        .then(result => {
          this.sitesList = result;
        })
        .catch(error => console.error(error));

    this.panelService.getPanelsForAccountWithNone(this.currentAccountId)
        .then(result => {
          this.panelList = result;
        })
        .catch(error => console.error(error));

    this.selectedSite = undefined;
    this.selectedPanel = undefined;
    this.selectedDeviceType = undefined;
    this.newDevice = new Device();
    this.isAddDeviceButtonDisabled = false;

    this.displayAddDialog = true;
  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }

  addDevice() {
    console.log("Adding device");
    console.log(this.selectedDeviceType);
    console.log(this.selectedDeviceType.value);

    switch (this.selectedDeviceType.value) {
      case this.SWITCH:
        this.addSwitch();
        break;

      case this.TIMER:
        this.addTimer();
        break;

      case this.EVENT_TRACK:
        this.addEventTrack();
        break;

      case this.VALID_CREDENTIAL:
        this.addValidCredential();
        break;

      default:
        console.log('There is no add method associated to the selected device type');
        break;
    }
  }

  addSwitch() {
    console.log('Adding switch');
    this.switchComponent.setSelectedFieldsOnSwitch();
    let switchDevice = this.switchComponent.switchForm;

    switchDevice.name = this.newDevice.name;
    switchDevice.siteId = this.selectedSite.siteId;
    switchDevice.brainId = this.selectedPanel.brainId;

    console.log('SWITCH to be added');
    console.log(switchDevice);

    this.devicesService.addSwitch(switchDevice)
        .then(result => {
          this.loadDevices()
              .then(response => {
                this.closeAddDialog();
              })
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));

  }

  addTimer() {
    console.log('Adding timer');
    this.timerComponent.setSelectedFieldsOnTimer();
    let timerDevice = this.timerComponent.timerForm;

    timerDevice.name = this.newDevice.name;
    timerDevice.siteId = this.selectedSite.siteId;
    timerDevice.brainId = this.selectedPanel.brainId;

    console.log('TIMER to be added');
    console.log(timerDevice);

    this.devicesService.addTimer(timerDevice)
        .then(result => {
          this.loadDevices()
              .then(response => {
                this.closeAddDialog();
              })
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  addEventTrack() {
    //check for doors - if there are no doors for panel - no event track can be added
    console.log('Adding event track');
    this.eventComponent.setSelectedFieldsOnEventTrack();
    let eventTrackDevice = this.eventComponent.eventTrackForm;

    eventTrackDevice.name = this.newDevice.name;
    eventTrackDevice.siteId = this.selectedSite.siteId;
    eventTrackDevice.brainId = this.selectedPanel.brainId;

    console.log('EVENT TRACK to be added');
    console.log(eventTrackDevice);

    this.devicesService.addEventTrack(eventTrackDevice)
        .then(result => {
          this.loadDevices()
          this.loadDevices()
              .then(response => {
                this.closeAddDialog();
              })
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  addValidCredential() {
    console.log('Adding valid credential...');
    this.validCredentialComponent.setSelectedFieldsOnValidCredential();
    let validCredentialDevice = this.validCredentialComponent.validCredentialForm;

    validCredentialDevice.name = this.newDevice.name;
    validCredentialDevice.siteId = this.selectedSite.siteId;
    validCredentialDevice.brainId = this.selectedPanel.brainId;

    console.log('Valid credential to be added');
    console.log(validCredentialDevice);

    this.devicesService.addValidCredential(validCredentialDevice)
        .then(result => {
          this.loadDevices()
              .then(response => {
                this.closeAddDialog();
              })
              .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
  }

  async showDialogToEdit() {
    this.newDevice = this.selectedDevices[0];

    this.selectedPanel = await this.panelService.getPanelByBrainId(this.newDevice.brainId);
    console.log(this.selectedPanel);

    this.displayEditDialog = true;
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  saveDeviceChanges() {
    console.log('Saving device changes');

    switch (this.newDevice.deviceType) {
      case this.SWITCH:
        this.saveSwitchChanges()
            .then(result => {
              this.closeEditDialog();
              this.selectedDevices = [];
            });
        break;

      case this.TIMER:
        this.saveTimerChanges()
            .then(result => {
              this.closeEditDialog();
              this.selectedDevices = [];
            });
        break;

      case this.EVENT_TRACK:
        this.saveEventTrackChanges()
            .then(result => {
              this.closeEditDialog();
              this.selectedDevices = [];
            });
        break;

      case this.VALID_CREDENTIAL:
        this.saveValidCredentialChanges()
            .then(result => {
              this.closeEditDialog();
              this.selectedDevices = [];
            });
        break;

      default:
        console.log('There is no update method associated to the device type');
        break;
    }
  }

  async saveSwitchChanges() {
    console.log('Saving switch changes');

    this.switchComponent.setSelectedFieldsOnSwitch();
    let switchDevice = this.switchComponent.switchForm;

    switchDevice.name = this.newDevice.name;

    await this.devicesService.updateSwitch(switchDevice);
  }

  async saveTimerChanges() {
    console.log('Saving timer changes');

    this.timerComponent.setSelectedFieldsOnTimer();
    let timerDevice = this.timerComponent.timerForm;

    timerDevice.name = this.newDevice.name;

    await this.devicesService.updateTimer(timerDevice);
  }

  async saveEventTrackChanges() {
    console.log('Saving event track changes');

    this.eventComponent.setSelectedFieldsOnEventTrack();
    let eventDevice = this.eventComponent.eventTrackForm;

    eventDevice.name = this.newDevice.name;

    await this.devicesService.updateEventTrack(eventDevice);
  }

  async saveValidCredentialChanges() {
    console.log('Saving valid credential changes');

    this.validCredentialComponent.setSelectedFieldsOnValidCredential();
    const validCredentialDevice = this.validCredentialComponent.validCredentialForm;

    validCredentialDevice.name = this.newDevice.name;

    console.log(validCredentialDevice);
    await this.devicesService.updateValidCredential(validCredentialDevice);
  }

  showDeviceDetails(device: Device) {
    this.selectedDeviceForDetails = device;
    console.log('DOOR');
    console.log(this.selectedDeviceForDetails);

    this.displayDetailsDialog = true;
  }

  closeDetailsDialog() {
    this.displayDetailsDialog = false;
  }

  receiveEventFromValidCredentialChild(event) {
    console.log('Received event from valid credential child: usableReaders exist = ' + event);

    if (event === true)
      this.isAddDeviceButtonDisabled = false;
    else
      this.isAddDeviceButtonDisabled = true;

    console.log('isAddDeviceButtonDisabled: ' + this.isAddDeviceButtonDisabled);
  }

  receiveEventFromEventTrackChild(event) {
    console.log('Received event from event track child: doorsExist = ' + event);

    if (event === true)
      this.isAddDeviceButtonDisabled = false;
    else
      this.isAddDeviceButtonDisabled = true;

    console.log('isAddDeviceButtonDisabled: ' + this.isAddDeviceButtonDisabled);
  }
}
