import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectItem} from "primeng/api";
import {Schedule} from "../../../../models/Schedule";
import {DevicesService} from "../../../../services/devices.service";
import {ScheduleService} from "../../../../services/schedule.service";
import {Definitions} from "../../../../constants/Definitions";
import {EventTrackForm} from "../../../../models/EventTrackForm";
import {Door} from "../../../../models/Door";

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss']
})
export class EventComponent implements OnInit {

  eventTrackForm: EventTrackForm;

  @Input() panelOid: number;
  @Input() currentAccountId: number;
  @Input() siteObjectId: number;
  @Input() action: string;
  @Input() deviceId: number;

  //tell parent if there are no doors
  @Output() sendEventToDevices = new EventEmitter<boolean>();

  outputBehaviors: SelectItem[];
  selectedOutputBehavior: SelectItem;

  eventTypes: SelectItem[];
  selectedEventType: SelectItem;

  scheduleList: Schedule[];
  selectedSchedule: Schedule;

  disengageDelayDisabled: boolean = false;
  disengageMessageDisplayed: boolean = false;

  doorsOfSelectedPanel: Door[];
  selectedDoor: Door;

  outputBehaviorDurationLabel: string;

  constructor(private deviceService: DevicesService, private scheduleService: ScheduleService) {
  }

  async ngOnInit() {
    await this.initCommonFields();

    console.log('ACTION: ' + this.action);
    if (this.action === 'ADD') {
      await this.prepareEventToAdd();

      const doorsExist: boolean = this.doorsOfSelectedPanel !== undefined && this.doorsOfSelectedPanel.length > 0;
      console.log('doorsExist ' + doorsExist);
      console.log(this.sendEventToDevices);
      this.sendEventToDevices.emit(doorsExist);
    }
    else if (this.action === 'EDIT')
      await this.prepareEventToEdit();
    else
      console.log('Unknown action for EVENT device type');

  }

  async initCommonFields() {
    this.initEventTypes();
    this.initOutputBehaviors();

    this.scheduleList = await this.scheduleService.getSchedulesForAccountBySiteWithNone(this.currentAccountId, this.siteObjectId);
    this.selectedSchedule = this.scheduleList[0];

    await this.initDoorsForPanel(this.panelOid);
  }

  initOutputBehaviors() {
    this.outputBehaviors = this.deviceService.getOutputBehaviors();

    if (this.selectedEventType.label != Definitions.DOOR_AJAR_ST) {
      let followBehaviorIndex = this.outputBehaviors.findIndex(behavior => behavior.label === Definitions.FOLLOW);

      console.log('FOLLOW BEHAVIOR INDEX');
      console.log(followBehaviorIndex);
      this.outputBehaviors = this.outputBehaviors.filter(behavior => behavior.label != Definitions.FOLLOW);
      console.log('AFTER SLICE');
      console.log(this.outputBehaviors);
    }

    this.selectedOutputBehavior = this.outputBehaviors[0];
    this.updateOutputBehaviorDurationLabel();
  }

  initEventTypes() {
    this.eventTypes = [
      {label: Definitions.DOOR_FORCED_OPEN_ST, value: '11'},
      {label: Definitions.DOOR_AJAR_ST, value: '19'},
      {label: Definitions.DOOR_AJAR_CLEARED_ST, value: '20'},
      {label: Definitions.INVALID_PINS_ST, value: '32'},
      {label: Definitions.DOOR_SCHED_OVER_BY_KEYPAD_ST, value: '52'},
      {label: Definitions.DOOR_SCHED_RESTORED_BY_KEYPAD_ST, value: '53'},
      {label: Definitions.DOOR_LOCKED_BY_TIMER_ST, value: '10'},
      {label: Definitions.DOOR_UNLOCKED_BY_TIMER_ST, value: '9'},
      {label: Definitions.CREDENTIAL_NO_PERMISSION_ST, value: '5'},
      {label: Definitions.CREDENTIAL_PRESENTED_ST, value: '0'},
      {label: Definitions.EXIT_REX_ST, value: '6'}
    ];

    // console.log(Definitions.FOLLOW);
    // console.log(Definitions.DOOR_FORCED_OPEN_ST);
    // console.log(this.eventTypes);
    this.selectedEventType = this.eventTypes[0];
  }

  async initDoorsForPanel(panelOid: number) {
    this.doorsOfSelectedPanel = await this.deviceService.getDoorsForPanel(this.panelOid);
    this.selectedDoor = this.doorsOfSelectedPanel[0];
  }

  outputBehaviorSelectionChanged() {
    console.log('New output behavior selected');
    console.log(this.selectedOutputBehavior);

    this.updateDisengageDelayDisabled();
    this.updateDisengageMessageDisplayed();
    this.updateOutputBehaviorDurationLabel();
  }

  eventTypeSelectionChanged() {
    console.log('Event type selection changed');
    console.log(this.selectedEventType);

    this.initOutputBehaviors();
    this.updateDisengageMessageDisplayed();
  }

  updateDisengageDelayDisabled() {
    if (this.selectedOutputBehavior.label === Definitions.LATCH || this.selectedOutputBehavior.label === Definitions.UNLATCH) {
      this.disengageDelayDisabled = true;
      this.eventTrackForm.disengageDelay = 0;
    } else
      this.disengageDelayDisabled = false;
  }

  updateOutputBehaviorDurationLabel() {
    if (this.selectedOutputBehavior.label === Definitions.FOLLOW)
      this.outputBehaviorDurationLabel = 'Disengage delay';

    else if (this.selectedOutputBehavior.label === Definitions.PULSE)
      this.outputBehaviorDurationLabel = 'Pulse duration';

    else
      this.outputBehaviorDurationLabel = 'Duration';
  }

  updateDisengageMessageDisplayed() {
    if (this.selectedEventType.label === Definitions.DOOR_AJAR_ST && this.selectedOutputBehavior.label === Definitions.FOLLOW)
      this.disengageMessageDisplayed = true;
    else
      this.disengageMessageDisplayed = false;
  }

  async prepareEventToAdd() {
    console.log('Preparing event to add...');

    this.eventTrackForm = await this.deviceService.getEventTrackTemplateByPanelOid(this.panelOid);

    this.eventTrackForm.panelOid = this.panelOid;
    this.eventTrackForm.accountId = this.currentAccountId;
    this.eventTrackForm.outputs = [];
  }

  async prepareEventToEdit() {
    console.log('Preparing event with device id ' + this.deviceId + ' to edit');

    this.eventTrackForm = await this.deviceService.getEventTrackByDeviceId(this.deviceId);
    console.log(this.eventTrackForm);
    console.log(this.scheduleList);

    this.eventTrackForm.panelOid = this.panelOid;
    this.eventTrackForm.accountId = this.currentAccountId;
    this.eventTrackForm.deviceId = this.deviceId;

    let selectedEventTypeIndex = this.eventTypes.findIndex(eventType => Number(eventType.value) == this.eventTrackForm.eventTypeId);
    console.log('SELECTED EVENT TYPE INDEX');
    console.log(selectedEventTypeIndex);
    if (selectedEventTypeIndex == -1)
      this.selectedEventType = this.eventTypes[0];
    else
      this.selectedEventType = this.eventTypes[selectedEventTypeIndex];

    //reinitialize output behaviors based on the selected event type
    this.initOutputBehaviors();

    console.log(this.outputBehaviors);
    let selectedBehaviorIndex = this.outputBehaviors.findIndex(behavior => Number(behavior.value) === this.eventTrackForm.behavior);
    console.log('SELECTED BEHAVIOR INDEX');
    console.log(selectedBehaviorIndex);

    console.log(this.selectedOutputBehavior);
    if (selectedBehaviorIndex == -1)
      this.selectedOutputBehavior = this.outputBehaviors[0];
    else
      this.selectedOutputBehavior = this.outputBehaviors[selectedBehaviorIndex];
    this.updateDisengageDelayDisabled();
    this.updateDisengageMessageDisplayed();
    this.updateOutputBehaviorDurationLabel();

    let selectedScheduleIndex = this.scheduleList.findIndex(schedule => schedule.scheduleId === this.eventTrackForm.scheduleId);
    console.log('SELECTED schedule INDEX');
    console.log(selectedScheduleIndex);
    console.log(this.scheduleList);
    if (selectedScheduleIndex == -1)
      this.selectedSchedule = this.scheduleList[0];
    else
      this.selectedSchedule = this.scheduleList[selectedScheduleIndex];

    let selectedDoorIndex = this.doorsOfSelectedPanel.findIndex(door => door.deviceObjectId === this.eventTrackForm.doorId);
    console.log('SELECTED door INDEX');
    console.log(selectedDoorIndex);
    if (selectedDoorIndex == -1)
      this.selectedDoor = this.doorsOfSelectedPanel[0];
    else
      this.selectedDoor = this.doorsOfSelectedPanel[selectedDoorIndex];

  }

  setSelectedFieldsOnEventTrack() {
    this.eventTrackForm.behavior = Number(this.selectedOutputBehavior.value);
    this.eventTrackForm.scheduleId = this.selectedSchedule.scheduleId;
    this.eventTrackForm.doorId = this.selectedDoor.deviceObjectId;
    this.eventTrackForm.eventTypeId = Number(this.selectedEventType.value);
  }
}
