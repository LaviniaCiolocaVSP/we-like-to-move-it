import {Component, Input, OnInit} from '@angular/core';
import {SelectItem} from "primeng/api";
import {Schedule} from "../../../../models/Schedule";
import {DevicesService} from "../../../../services/devices.service";
import {ScheduleService} from "../../../../services/schedule.service";
import {TimerForm} from "../../../../models/TimerForm";
import {Definitions} from "../../../../constants/Definitions";

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.scss']
})
export class TimerComponent implements OnInit {


  timerForm: TimerForm;

  @Input() panelOid: number;
  @Input() currentAccountId: number;
  @Input() siteObjectId: number;
  @Input() action: string;
  @Input() deviceId: number;

  outputBehaviors: SelectItem[];
  selectedOutputBehavior: SelectItem;

  scheduleList: Schedule[];
  selectedSchedule: Schedule;

  disengageDelayDisabled: boolean = false;

  constructor(private deviceService: DevicesService, private scheduleService: ScheduleService) {
  }

  async ngOnInit() {
    await this.initCommonFields();

    console.log('ACTION: ' + this.action);
    if (this.action === 'ADD')
      await this.prepareTimerToAdd();
    else if (this.action === 'EDIT')
      await this.prepareTimerToEdit();
    else
      console.log('Unknown action for TIMER device type');

  }

  async initCommonFields() {
    this.initOutputBehaviors();
    this.selectedOutputBehavior = this.outputBehaviors[0];

    this.scheduleList = await this.scheduleService.getSchedulesForAccountBySiteWithNone(this.currentAccountId, this.siteObjectId);
    this.selectedSchedule = this.scheduleList[0];
  }

  outputBehaviorSelectionChanged() {
    console.log('New output behavior selected');
    console.log(this.selectedOutputBehavior);

    this.updateDisengageDelayDisabled();
  }

  updateDisengageDelayDisabled() {
    if (this.selectedOutputBehavior.label === Definitions.LATCH || this.selectedOutputBehavior.label === Definitions.UNLATCH) {
      this.disengageDelayDisabled = true;
      this.timerForm.disengageDelay = 0;
    } else
      this.disengageDelayDisabled = false;
  }

  async prepareTimerToAdd() {
    console.log('Preparing timer to add...');

    this.timerForm = await this.deviceService.getTimerTemplateByPanelOid(this.panelOid);

    this.timerForm.panelOid = this.panelOid;
    this.timerForm.accountId = this.currentAccountId;
    this.timerForm.outputs = [];
  }

  async prepareTimerToEdit() {
    console.log('Preparing timer with device id ' + this.deviceId + ' to edit');

    this.timerForm = await this.deviceService.getTimerByDeviceId(this.deviceId);
    console.log(this.timerForm);
    console.log(this.scheduleList);

    this.timerForm.panelOid = this.panelOid;
    this.timerForm.accountId = this.currentAccountId;
    this.timerForm.deviceId = this.deviceId;

    let selectedBehaviorIndex = this.outputBehaviors.findIndex(behavior => Number(behavior.value) === this.timerForm.behavior);
    console.log('SELECTED BEHAVIOR INDEX');
    console.log(selectedBehaviorIndex);

    console.log(this.selectedOutputBehavior);
    if (selectedBehaviorIndex == -1)
      this.selectedOutputBehavior = this.outputBehaviors[0];
    else
      this.selectedOutputBehavior = this.outputBehaviors[selectedBehaviorIndex];
    this.updateDisengageDelayDisabled();

    let selectedScheduleIndex = this.scheduleList.findIndex(schedule => schedule.scheduleId === this.timerForm.scheduleId);
    console.log('SELECTED schedule INDEX');
    console.log(selectedScheduleIndex);
    console.log(this.scheduleList);
    if (selectedScheduleIndex == -1)
      this.selectedSchedule = this.scheduleList[0];
    else
      this.selectedSchedule = this.scheduleList[selectedScheduleIndex];

  }

  initOutputBehaviors() {
    this.outputBehaviors = this.deviceService.getOutputBehaviors();

    //Follow is the default output behavior for TIMER device
    this.selectedOutputBehavior = this.outputBehaviors[0];
  }

  setSelectedFieldsOnTimer() {
    this.timerForm.behavior = Number(this.selectedOutputBehavior.value);
    this.timerForm.scheduleId = this.selectedSchedule.scheduleId;
  }

}
