import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectItem} from "primeng/api";
import {Schedule} from "../../../../models/Schedule";
import {DevicesService} from "../../../../services/devices.service";
import {ScheduleService} from "../../../../services/schedule.service";
import {Definitions} from "../../../../constants/Definitions";
import {ValidCredentialForm} from "../../../../models/ValidCredentialForm";

@Component({
  selector: 'app-valid-credential',
  templateUrl: './valid-credential.component.html',
  styleUrls: ['./valid-credential.component.scss']
})
export class ValidCredentialComponent implements OnInit {

  @Input() panelOid: number;
  @Input() currentAccountId: number;
  @Input() siteObjectId: number;
  @Input() action: string;
  @Input() deviceId: number;

  //tell parent if there are no usable readers
  @Output() sendEventToDevices = new EventEmitter<boolean>();

  outputBehaviors: SelectItem[];
  selectedOutputBehavior: SelectItem;

  scheduleList: Schedule[];
  selectedCardRequiredSchedule: Schedule;
  selectedTwoFactorSchedule: Schedule;

  disengageDelayDisabled: boolean = false;

  validCredentialForm: ValidCredentialForm;

  constructor(private deviceService: DevicesService, private scheduleService: ScheduleService) {
  }

  ngOnInit() {
    this.initCommonFields();

    console.log('ACTION: ' + this.action);
    if (this.action === 'ADD') {
      this.prepareValidCredentialToAdd()
          .then(result => {
            const usableReadersExist: boolean = this.validCredentialForm.availableReaders !== undefined && this.validCredentialForm.availableReaders.length > 0;
            console.log('usableReadersExist ' + usableReadersExist);
            console.log(this.sendEventToDevices);
            this.sendEventToDevices.emit(usableReadersExist);
          });
    } else if (this.action === 'EDIT')
      this.prepareValidCredentialToEdit();
    else
      console.log('Unknown action for VALID CREDENTIAL device type');
  }

  initCommonFields() {
    console.log('Init common fields');
    this.initOutputBehaviors();
    console.log('Successfully init output behaviors');

    this.scheduleService.getSchedulesForAccountBySiteWithNone(this.currentAccountId, this.siteObjectId)
        .then(result => {
          this.scheduleList = result;
          console.log('Schedules:');
          console.log(this.scheduleList);

          this.selectedTwoFactorSchedule = this.scheduleList[0];
          this.selectedCardRequiredSchedule = this.scheduleList[0];
        })
        .catch(error => console.error(error));
  }

  outputBehaviorSelectionChanged() {
    console.log('New output behavior selected');
    console.log(this.selectedOutputBehavior);

    this.updateDisengageDelayDisabled();
  }

  updateDisengageDelayDisabled() {
    if (this.selectedOutputBehavior.label === Definitions.LATCH || this.selectedOutputBehavior.label === Definitions.UNLATCH) {
      this.disengageDelayDisabled = true;
      this.validCredentialForm.disengageDelay = 0;
    } else
      this.disengageDelayDisabled = false;
  }

  async prepareValidCredentialToAdd() {
    console.log('Preparing valid credential to add...');

    this.validCredentialForm = await this.deviceService.getValidCredentialTemplateByPanelOid(this.panelOid);
    
    this.validCredentialForm.panelOid = this.panelOid;
    this.validCredentialForm.accountId = this.currentAccountId;
    this.validCredentialForm.selectedReader = this.validCredentialForm.availableReaders[0];
    this.validCredentialForm.outputs = [];

    console.log('Successfully got valid credential template');
  }

  prepareValidCredentialToEdit() {
    console.log('Preparing valid credential with device id ' + this.deviceId + ' to edit');

    this.deviceService.getValidCredentialByDeviceId(this.deviceId)
        .then(response => {
          this.validCredentialForm = response;

          this.validCredentialForm.panelOid = this.panelOid;
          this.validCredentialForm.accountId = this.currentAccountId;
          this.validCredentialForm.deviceId = this.deviceId;

          const selectedBehaviorIndex = this.outputBehaviors.findIndex(behavior => Number(behavior.value) === this.validCredentialForm.behavior);
          console.log('SELECTED BEHAVIOR INDEX');
          console.log(selectedBehaviorIndex);
          console.log(this.selectedOutputBehavior);

          if (selectedBehaviorIndex === -1)
            this.selectedOutputBehavior = this.outputBehaviors[0];
          else
            this.selectedOutputBehavior = this.outputBehaviors[selectedBehaviorIndex];
          this.updateDisengageDelayDisabled();

          const selectedTwoFactorScheduleIndex = this.scheduleList.findIndex(schedule => schedule.scheduleId === this.validCredentialForm.twoFactorScheduleId);
          console.log('SELECTED TWO FACTOR SCHEDULE INDEX');
          console.log(selectedTwoFactorScheduleIndex);
          console.log(this.scheduleList);

          if (selectedTwoFactorScheduleIndex === -1)
            this.selectedTwoFactorSchedule = this.scheduleList[0];
          else
            this.selectedTwoFactorSchedule = this.scheduleList[selectedTwoFactorScheduleIndex];

          const selectedCardRequiredScheduleIndex = this.scheduleList.findIndex(schedule => schedule.scheduleId === this.validCredentialForm.cardRequiredScheduleId);
          console.log('SELECTED CARD REQUIRED SCHEDULE INDEX');
          console.log(selectedCardRequiredScheduleIndex);

          if (selectedCardRequiredScheduleIndex === -1)
            this.selectedCardRequiredSchedule = this.scheduleList[0];
          else
            this.selectedCardRequiredSchedule = this.scheduleList[selectedCardRequiredScheduleIndex];
        })
        .catch(error => console.error(error));
  }

  initOutputBehaviors() {
    this.outputBehaviors = this.deviceService.getOutputBehaviors();
    //remove 'FOLLOW' behavior for valid credential
    this.outputBehaviors = this.outputBehaviors.slice(1);

    this.selectedOutputBehavior = this.outputBehaviors[2];
  }

  setSelectedFieldsOnValidCredential() {
    this.validCredentialForm.behavior = Number(this.selectedOutputBehavior.value);
    this.validCredentialForm.cardRequiredScheduleId = this.selectedCardRequiredSchedule.scheduleId;
    this.validCredentialForm.twoFactorScheduleId = this.selectedTwoFactorSchedule.scheduleId;
  }


}
