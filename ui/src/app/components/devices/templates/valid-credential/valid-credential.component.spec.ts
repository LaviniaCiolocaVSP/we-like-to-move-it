import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValidCredentialComponent } from './valid-credential.component';

describe('ValidCredentialComponent', () => {
  let component: ValidCredentialComponent;
  let fixture: ComponentFixture<ValidCredentialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidCredentialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValidCredentialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
