import {Component, Input, OnInit} from '@angular/core';
import {SwitchForm} from "../../../../models/SwitchForm";
import {DevicesService} from "../../../../services/devices.service";
import {SelectItem} from "primeng/api";
import {ScheduleService} from "../../../../services/schedule.service";
import {Schedule} from "../../../../models/Schedule";
import {Definitions} from "../../../../constants/Definitions";

@Component({
  selector: 'app-switch',
  templateUrl: './switch.component.html',
  styleUrls: ['./switch.component.scss']
})
export class SwitchComponent implements OnInit {

  switchForm: SwitchForm;

  @Input() panelOid: number;
  @Input() currentAccountId: number;
  @Input() siteObjectId: number;
  @Input() action: string;
  @Input() deviceId: number;

  disengageDelayDisabled: boolean = false;

  outputBehaviors: SelectItem[];
  selectedOutputBehavior: SelectItem;

  scheduleList: Schedule[];
  selectedSchedule: Schedule;

  PROGIO_SCHEDULE_TYPE: number = 3;

  constructor(private deviceService: DevicesService, private scheduleService: ScheduleService) {
  }

  async ngOnInit() {
    await this.initCommonFields();

    console.log('ACTION: ' + this.action);
    if (this.action === 'ADD')
      await this.prepareSwitchToAdd();
    else if (this.action === 'EDIT')
      await this.prepareSwitchToEdit();
    else
      console.log('Unknown action for SWITCH device type');

  }

  async initCommonFields() {
    this.initOutputBehaviors();
    this.selectedOutputBehavior = this.outputBehaviors[0];

    this.scheduleList = await this.scheduleService.getSchedulesForAccountBySiteWithNone(this.currentAccountId, this.siteObjectId);
    this.selectedSchedule = this.scheduleList[0];
  }

  async prepareSwitchToAdd() {
    console.log('Preparing switch to add...');

    this.switchForm = await this.deviceService.getSwitchTemplateByPanelOid(this.panelOid);

    this.switchForm.panelOid = this.panelOid;
    this.switchForm.accountId = this.currentAccountId;
    this.switchForm.input = this.switchForm.availableInputs[0];
    this.switchForm.outputs = [];
  }

  async prepareSwitchToEdit() {
    console.log('Preparing switch with device id ' + this.deviceId + ' to edit');

    this.switchForm = await this.deviceService.getSwitchByDeviceId(this.deviceId);
    console.log(this.switchForm);
    console.log(this.scheduleList);

    this.switchForm.panelOid = this.panelOid;
    this.switchForm.accountId = this.currentAccountId;
    this.switchForm.deviceId = this.deviceId;

    let selectedBehaviorIndex = this.outputBehaviors.findIndex(behavior => Number(behavior.value) === this.switchForm.behavior);
    console.log('SELECTED BEHAVIOR INDEX');
    console.log(selectedBehaviorIndex);

    console.log(this.selectedOutputBehavior);
    if (selectedBehaviorIndex == -1)
      this.selectedOutputBehavior = this.outputBehaviors[0];
    else
      this.selectedOutputBehavior = this.outputBehaviors[selectedBehaviorIndex];

    this.updateDisengageDelayDisabled();

    let selectedScheduleIndex = this.scheduleList.findIndex(schedule => schedule.scheduleId === this.switchForm.scheduleId);
    console.log('SELECTED schedule INDEX');
    console.log(selectedScheduleIndex);
    console.log(this.scheduleList);
    if (selectedScheduleIndex == -1)
      this.selectedSchedule = this.scheduleList[0];
    else
      this.selectedSchedule = this.scheduleList[selectedScheduleIndex];

  }

  initOutputBehaviors() {
    this.outputBehaviors = this.deviceService.getOutputBehaviors();

    //Follow is the default output behavior for SWITCH device
    this.selectedOutputBehavior = this.outputBehaviors[0];
  }

  setSelectedFieldsOnSwitch() {
    this.switchForm.behavior = Number(this.selectedOutputBehavior.value);
    this.switchForm.scheduleId = this.selectedSchedule.scheduleId;
  }


  outputBehaviorSelectionChanged() {
    console.log('New output behavior selected');
    console.log(this.selectedOutputBehavior);

    this.updateDisengageDelayDisabled();
  }

  updateDisengageDelayDisabled() {
    if (this.selectedOutputBehavior.label === Definitions.LATCH || this.selectedOutputBehavior.label === Definitions.UNLATCH) {
      this.disengageDelayDisabled = true;
      this.switchForm.disengageDelay = 0;
    } else
      this.disengageDelayDisabled = false;
  }
}
