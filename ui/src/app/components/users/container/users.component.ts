import {Component, OnInit} from '@angular/core';
import {UsersService} from '../../../services/users.service';
import {User} from '../../../models/User';
import {ActivatedRoute, Router} from '@angular/router';
import {GroupsService} from '../../../services/groups.service';
import {GroupSummary} from '../../../models/GroupSummary';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  usersList: Array<User>;
  selectedUsers: User[] = [];

  contentLoaded: boolean;

  groupsList: GroupSummary[] = [];
  selectedGroups: GroupSummary[] = [];

  newUser: User = new User();
  displayAddDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayEditDialog: boolean = false;
  displayDetailsDialog: boolean = false;
  editModeOn: boolean = false;
  currentAccountId: number;

  private cols: any[];

  showLog: boolean = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }


  constructor(
    private activeRoute: ActivatedRoute,
    private usersService: UsersService,
    private groupsService: GroupsService,
    private router: Router
  ) {
    this.contentLoaded = false;
    this.cols = [
      {field: 'userId', header: 'User ID'},
      {field: 'firstName', header: 'First name'},
      {field: 'lastName', header: 'Last name'},
      {field: 'groupsString', header: 'Groups'},
    ];
  }

  ngOnInit() {
    this.loadUserData();
  }

  async loadUserData() {
    this.contentLoaded = false;
    this.currentAccountId = this.activeRoute.snapshot.params['id'];
    console.log('Getting users');
    this.usersList = await this.usersService.getUsers(this.currentAccountId);
    console.log('Users retrieved');
    this.createGroupsAsStringForAllUsers();

    this.groupsList = await this.groupsService.getGroupsSummary(this.currentAccountId);

    this.contentLoaded = true;
    this.selectedUsers = [];
    this.selectedGroups = [];
  }

  createGroupsAsStringForAllUsers() {
    this.usersList.forEach(user => {
      let userGroupsString = '';

      if (user.uiSecurityGroup.length > 0) {
        user.uiSecurityGroup.forEach(group => {
          userGroupsString += group.name + ' ## ';
        });

        userGroupsString = userGroupsString.slice(0, userGroupsString.length - 4);
        console.log(userGroupsString);
      }

      user.groupsString = userGroupsString;
    });
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  showDialogToAdd() {
    this.displayAddDialog = true;
    this.newUser = new User();

    this.selectedGroups = [];
  }

  addUser() {
    this.newUser.accountId = this.currentAccountId;
    this.newUser.userTypeId = 1;

    console.log(this.selectedGroups);
    this.newUser.uiSecurityGroup = this.selectedGroups;

    console.log(this.newUser);

    this.usersService.addUser(this.newUser).then((result) => {
      console.log(result);
      this.closeAddDialog();
      this.loadUserData();
    });

  }

  closeAddDialog() {
    this.displayAddDialog = false;
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  deleteUsers() {
    let users_id: number[] = [];
    this.selectedUsers.map(row => {
      users_id.push(row.objectId);
    });
    console.log(users_id);

    this.usersService.deleteUsers(users_id).then((result) => {
      console.log(result);
      this.closeDeleteDialog();
      this.loadUserData();
    });
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  saveUserChanges() {
    this.newUser = this.selectedUsers[0];
    this.newUser.uiSecurityGroup = this.selectedGroups;

    console.log(this.newUser);

    this.usersService.updateUser(this.newUser).then((result) => {
      console.log(result);
      this.closeEditDialog();
      this.loadUserData();
    });
  }

  async showDialogToEdit() {
    this.newUser = this.selectedUsers[0];
    console.log(this.newUser);

    this.selectedGroups = this.newUser.uiSecurityGroup;
    console.log('EDIT');
    console.log(this.groupsList);
    console.log(this.selectedGroups);
    this.displayEditDialog = true;
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  showDialogDetails(user: User) {
    this.newUser = user;
    this.displayDetailsDialog = true;
  }

}
