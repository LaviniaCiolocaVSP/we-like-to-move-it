import {Component, OnInit} from '@angular/core';
import {SessionService} from '../../../services/session.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Schedule} from '../../../models/Schedule';
import {ScheduleService} from '../../../services/schedule.service';
import {Group} from '../../../models/Group';
import {GroupsService} from '../../../services/groups.service';
import {Day} from 'src/app/models/Day';
import {ScheduleDataS} from '../../../models/ScheduleDataS';
import {SelectItem} from "primeng/api";
import {Holiday} from "../../../models/Holiday";
import {HolidayService} from "../../../services/holiday.service";

@Component({
  selector: 'app-scheduledetails',
  templateUrl: './scheduleDetails.component.html',
  styleUrls: ['./scheduleDetails.component.scss']
})

export class ScheduleDetailsComponent implements OnInit {
  private static MINUTES_IN_HOUR = 60;
  private static MINUTES_IN_DAY = 60 * 24;

  schedule: Schedule;

  currentAccountId: number;
  currentScheduleId: number;

  contentLoaded: boolean;
  panelLog: any;

  groupList: Group[] = [];
  selectedGroup: Group;

  userGroupList: Group[] = [];
  selectedUserGroup: Group;

  newSchedule: Schedule = new Schedule();
  displayEditDialog: boolean = false;

  weekDays: Day[] = [];
  weekDaysUI: Day[] = [];

  selectedWeekDay: Day;
  scheduleDataList: ScheduleDataS[] = [];
  scheduleData: ScheduleDataS = new ScheduleDataS();

  selectable: boolean = true;
  removable: boolean = true;

  ampm: SelectItem[] = [];
  startAMPM: SelectItem;
  stopAMPM: SelectItem;

  cols: any[];

  holidays: Holiday[] = [];
  selectedHolidays: Holiday[] = [];

  constructor(
    private sessionService: SessionService,
    private scheduleService: ScheduleService,
    private router: Router,
    private groupsService: GroupsService,
    private activatedRoute: ActivatedRoute,
    private holidayService: HolidayService
  ) {
    this.contentLoaded = false;

    this.cols = [
      {field: 'site', header: 'Site'},
      {field: 'name', header: 'Name'},
      {field: 'from', header: 'Start date'},
      {field: 'to', header: 'End date'}

    ];

    this.weekDays = [{dayId: 1, dayName: 'Monday'},
      {dayId: 2, dayName: 'Tuesday'},
      {dayId: 3, dayName: 'Wednesday'},
      {dayId: 4, dayName: 'Thursday'},
      {dayId: 5, dayName: 'Friday'},
      {dayId: 6, dayName: 'Saturday'},
      {dayId: 0, dayName: 'Sunday'},
      {dayId: 7, dayName: 'Holiday'},];

    this.weekDaysUI = [
      {dayId: 0, dayName: 'Sunday'},
      {dayId: 1, dayName: 'Monday'},
      {dayId: 2, dayName: 'Tuesday'},
      {dayId: 3, dayName: 'Wednesday'},
      {dayId: 4, dayName: 'Thursday'},
      {dayId: 5, dayName: 'Friday'},
      {dayId: 6, dayName: 'Saturday'},
      {dayId: 7, dayName: 'Holiday'},
    ];
    this.ampm = [{label: 'AM', value: 1}, {label: 'PM', value: 2}];
  }

  ngOnInit() {
    this.currentAccountId = this.activatedRoute.snapshot.params['accountId'];
    this.currentScheduleId = this.activatedRoute.snapshot.params['scheduleId'];
    this.loadScheduleData();
  }

  async loadScheduleData() {
    this.contentLoaded = false;
    this.schedule = await this.scheduleService.getSchedule(this.currentScheduleId);
    this.contentLoaded = true;
  }

  navigateBack() {
    this.router.navigate(['schedules', this.currentAccountId]);
  }

  addScheduleDataToList() {
    let startArray = this.scheduleData.start.split(':');
    let stopArray = this.scheduleData.stop.split(':');
    let hoursStart = parseInt(startArray[0], 10);
    let hoursStop = parseInt(stopArray[0], 10);
    let minutesStart = parseInt(startArray[1], 10);
    let minutesStop = parseInt(stopArray[1], 10);

    if (this.scheduleData.start[0] == '0')
      this.scheduleData.start = this.scheduleData.start.substring(1);
    this.scheduleData.start += this.startAMPM.label;

    if (this.scheduleData.stop[0] == '0')
      this.scheduleData.stop = this.scheduleData.stop.substring(1);
    this.scheduleData.stop += this.stopAMPM.label;

    if (this.startAMPM.value == 1) //AM
      if (hoursStart == 12) {
        hoursStart = 0;
      }
    if (this.stopAMPM.value == 1) {
      if (hoursStop == 12)
        hoursStop = 0;
    }

    if (this.startAMPM.value == 2) {
      if (hoursStart != 12 && hoursStart < 12) {
        hoursStart = hoursStart + 12;
      }
    }
    if (this.stopAMPM.value == 2) {
      if (hoursStop != 12 && hoursStop < 12) {
        hoursStop = hoursStop + 12;
      }
    }
    console.log(this.selectedWeekDay.dayId + ' * ' + ScheduleDetailsComponent.MINUTES_IN_DAY + ' +  ' + hoursStart + ' * ' + ScheduleDetailsComponent.MINUTES_IN_HOUR + ' + ' + minutesStart);
    console.log(this.selectedWeekDay.dayId + ' * ' + ScheduleDetailsComponent.MINUTES_IN_DAY + ' +  ' + hoursStop + ' * ' + ScheduleDetailsComponent.MINUTES_IN_HOUR + ' + ' + minutesStop);

    this.scheduleData.startTime = this.selectedWeekDay.dayId * ScheduleDetailsComponent.MINUTES_IN_DAY + hoursStart * ScheduleDetailsComponent.MINUTES_IN_HOUR + minutesStart;
    this.scheduleData.stopTime = this.selectedWeekDay.dayId * ScheduleDetailsComponent.MINUTES_IN_DAY + hoursStop * ScheduleDetailsComponent.MINUTES_IN_HOUR + minutesStop;

    this.scheduleData.dayName = this.selectedWeekDay.dayName;
    this.scheduleData.dayId = this.selectedWeekDay.dayId;

    this.scheduleData.scheduleId = this.currentAccountId;
    this.scheduleDataList.push(this.scheduleData);
    console.log(this.scheduleDataList);

    this.selectedWeekDay = {dayId: 1, dayName: 'Monday'};
    this.scheduleData = new ScheduleDataS();
    this.startAMPM = {label: 'AM', value: 1};
    this.stopAMPM = {label: 'AM', value: 1};
  }


  remove(scheduleDataS: ScheduleDataS) {
    const index = this.scheduleDataList.indexOf(scheduleDataS);

    if (index >= 0) {
      this.scheduleDataList.splice(index, 1);
    }
  }

  async showDialogToEdit() {
    this.groupList = await this.groupsService.getDeviceGroupsWithNone(this.currentAccountId);
    let selectedSiteIndex = this.groupList.findIndex(site => site.objectId === this.schedule.siteId);
    this.selectedGroup = this.groupList[selectedSiteIndex];

    this.userGroupList = await this.groupsService.getUsersGroupsWithNone(this.currentAccountId);
    let selectedUserGroupIndex = this.userGroupList.findIndex(userGroup => userGroup.objectId === this.schedule.groupId);
    this.selectedUserGroup = this.userGroupList[selectedUserGroupIndex];

    this.holidays = await this.holidayService.getHolidays(this.currentAccountId);
    this.selectedHolidays = this.schedule.holidayDTOSet;

    this.newSchedule = this.schedule;
    this.displayEditDialog = true;
    this.scheduleDataList = this.schedule.scheduleData;

    this.selectedWeekDay = {dayId: 1, dayName: 'Monday'};
    this.startAMPM = {label: 'AM', value: 1};
    this.stopAMPM = {label: 'AM', value: 1};
  }

  closeEditDialog() {
    this.displayEditDialog = false;
  }

  editSchedule() {
    this.newSchedule.siteId = this.selectedGroup.objectId;
    this.newSchedule.scheduleData = this.scheduleDataList;
    this.newSchedule.groupId = this.selectedUserGroup.objectId;

    this.newSchedule.holidayDTOSet = this.selectedHolidays;
    this.scheduleService.updateSchedule(this.newSchedule)
        .then((result) => {
          console.log(result);
          this.closeEditDialog();
          this.loadScheduleData();
        });
  }
}
