import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Schedule} from '../../../models/Schedule';
import {ScheduleService} from '../../../services/schedule.service';
import {Group} from '../../../models/Group';
import {GroupsService} from '../../../services/groups.service';
import {Day} from 'src/app/models/Day';
import {ScheduleDataS} from '../../../models/ScheduleDataS';
import {Holiday} from '../../../models/Holiday';
import {SelectItem} from 'primeng/api';
import {HolidayService} from '../../../services/holiday.service';

@Component({
  selector: 'app-schedules',
  templateUrl: './schedules.component.html',
  styleUrls: ['./schedules.component.scss']
})

export class SchedulesComponent implements OnInit {
  private static MINUTES_IN_HOUR = 60;
  private static MINUTES_IN_DAY = 60 * 24;

  scheduleList: Array<Schedule>;
  selectedSchedules: Schedule[] = [];

  currentAccountId: number;
  contentLoaded: boolean;
  panelLog: any;

  groupList: Group[] = [];
  selectedGroup: Group;

  userGroupList: Group[] = [];
  selectedUserGroup: Group;

  newSchedule: Schedule = new Schedule();
  displayAddDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayEditDialog: boolean = false;
  weekDays: Day[] = [];
  selectedWeekDay: Day;
  scheduleDataList: ScheduleDataS[] = [];
  scheduleData: ScheduleDataS = new ScheduleDataS();
  selectable: boolean = true;
  removable: boolean = true;
  ampm: SelectItem[] = [];
  startAMPM: SelectItem;
  stopAMPM: SelectItem;
  holidays: Holiday[] = [];
  selectedHolidays: Holiday[] = [];
  private cols: any[];

  showLog: boolean = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }


  constructor(
    private scheduleService: ScheduleService,
    private router: Router,
    private groupsService: GroupsService,
    private holidayService: HolidayService,
    private activatedRoute: ActivatedRoute
  ) {
    this.contentLoaded = false;
    this.cols = [
      {field: 'scheduleId', header: 'Schedule Id'},
      {field: 'scheduleName', header: 'Name'},
      {field: 'description', header: 'Description'},
      {field: 'siteName', header: 'Site'}
    ];

    this.weekDays = [{dayId: 1, dayName: 'Monday'},
      {dayId: 2, dayName: 'Tuesday'},
      {dayId: 3, dayName: 'Wednesday'},
      {dayId: 4, dayName: 'Thursday'},
      {dayId: 5, dayName: 'Friday'},
      {dayId: 6, dayName: 'Saturday'},
      {dayId: 0, dayName: 'Sunday'},
      {dayId: 7, dayName: 'Holiday'}];

    this.ampm = [{label: 'AM', value: 1}, {label: 'PM', value: 2}];
  }

  ngOnInit() {
    this.currentAccountId = this.activatedRoute.snapshot.params['id'];
    this.loadScheduleData();
  }

  async loadScheduleData() {
    this.contentLoaded = false;
    this.scheduleList = await this.scheduleService.getSchedules(this.currentAccountId);
    this.contentLoaded = true;
    this.selectedSchedules = [];
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  async showDialogToAdd() {
    this.newSchedule = new Schedule();

    this.groupList = await this.groupsService.getDeviceGroupsWithNone(this.currentAccountId);
    this.selectedGroup = this.groupList[0];

    this.userGroupList = await this.groupsService.getUsersGroupsWithNone(this.currentAccountId);
    this.selectedUserGroup = this.userGroupList[0];

    this.holidays = await this.holidayService.getHolidays(this.currentAccountId);

    this.selectedWeekDay = {dayId: 0, dayName: 'Monday'};
    this.startAMPM = {label: 'AM', value: 1};
    this.stopAMPM = {label: 'AM', value: 1};

    this.displayAddDialog = true;
  }

  addSchedule() {
    this.newSchedule.accountId = this.currentAccountId;
    this.newSchedule.groupId = this.selectedUserGroup.objectId;
    this.newSchedule.siteId = this.selectedGroup.objectId;
    this.newSchedule.scheduleData = this.scheduleDataList;
    console.log(this.newSchedule);

    this.newSchedule.holidayDTOSet = this.selectedHolidays;
    this.scheduleService.addSchedule(this.newSchedule).then((result) => {
      console.log(result);
      this.closeAddDialog();
      this.loadScheduleData();
    });

  }

  closeAddDialog() {
    this.displayAddDialog = false;
    this.scheduleDataList = [];
    this.selectedHolidays = [];
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  deleteSchedules() {
    let schedule_ids: number[] = [];
    this.selectedSchedules.map(row => {
      schedule_ids.push(row.scheduleId);
    });
    console.log(schedule_ids);

    this.scheduleService.deleteSchedules(schedule_ids).then((result) => {
      console.log(result);
      this.closeDeleteDialog();
      this.loadScheduleData();
    });
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }

  addScheduleDataToList() {

    console.log(this.scheduleData);

    let startArray = this.scheduleData.start.split(':');
    let stopArray = this.scheduleData.stop.split(':');
    let hoursStart = parseInt(startArray[0], 10);
    let hoursStop = parseInt(stopArray[0], 10);
    let minutesStart = parseInt(startArray[1], 10);
    let minutesStop = parseInt(stopArray[1], 10);

    if (this.scheduleData.start[0] == '0')
      this.scheduleData.start = this.scheduleData.start.substring(1);
    this.scheduleData.start += this.startAMPM.label;

    if (this.scheduleData.stop[0] == '0')
      this.scheduleData.stop = this.scheduleData.stop.substring(1);
    this.scheduleData.stop += this.stopAMPM.label;

    if (this.startAMPM.value == 1) //AM
      if (hoursStart == 12) {
        hoursStart = 0;
      }
    if (this.stopAMPM.value == 1) {
      if (hoursStop == 12)
        hoursStop = 0;
    }

    if (this.startAMPM.value == 2) {
      if (hoursStart != 12 && hoursStart < 12) {
        hoursStart = hoursStart + 12;
      }
    }
    if (this.stopAMPM.value == 2) {
      if (hoursStop != 12 && hoursStop < 12) {
        hoursStop = hoursStop + 12;
      }
    }
    console.log(this.selectedWeekDay.dayId + ' * ' + SchedulesComponent.MINUTES_IN_DAY + ' +  ' + hoursStart + ' * ' + SchedulesComponent.MINUTES_IN_HOUR + ' + ' + minutesStart);
    console.log(this.selectedWeekDay.dayId + ' * ' + SchedulesComponent.MINUTES_IN_DAY + ' +  ' + hoursStop + ' * ' + SchedulesComponent.MINUTES_IN_HOUR + ' + ' + minutesStop);


    this.scheduleData.startTime = this.selectedWeekDay.dayId * SchedulesComponent.MINUTES_IN_DAY + hoursStart * SchedulesComponent.MINUTES_IN_HOUR + minutesStart;
    this.scheduleData.stopTime = this.selectedWeekDay.dayId * SchedulesComponent.MINUTES_IN_DAY + hoursStop * SchedulesComponent.MINUTES_IN_HOUR + minutesStop;

    this.scheduleData.dayName = this.selectedWeekDay.dayName;
    this.scheduleData.dayId = this.selectedWeekDay.dayId;

    this.scheduleData.scheduleId = this.currentAccountId;
    this.scheduleDataList.push(this.scheduleData);
    console.log(this.scheduleDataList);

    this.selectedWeekDay = {dayId: 1, dayName: 'Monday'};
    this.scheduleData = new ScheduleDataS();
    this.startAMPM = {label: 'AM', value: 1};
    this.stopAMPM = {label: 'AM', value: 1};
  }

  remove(scheduleDataS: ScheduleDataS) {
    const index = this.scheduleDataList.indexOf(scheduleDataS);
    if (index >= 0) {
      this.scheduleDataList.splice(index, 1);
    }
  }

  navigateToDetails(schedule: Schedule): void {
    this.router.navigate(['scheduleDetails/' + this.currentAccountId + '/' + schedule.scheduleId]);
  }
}
