import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SchedulesComponent} from './container/schedules.component';
import {
  ButtonModule,
  CalendarModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputMaskModule,
  InputTextModule,
  MultiSelectModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {MatChipsModule} from '@angular/material/chips';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ScheduleDetailsComponent} from './details/scheduleDetails.component';


@NgModule({
  declarations: [SchedulesComponent, ScheduleDetailsComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    CalendarModule,
    DropdownModule,
    MatChipsModule,
    MatIconModule,
    MatFormFieldModule,
    InputMaskModule,
    MultiSelectModule
  ]
})
export class SchedulesModule {
}
