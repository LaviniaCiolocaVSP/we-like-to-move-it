import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Credential} from '../../../models/Credential';
import {CredentialService} from '../../../services/credential.service';
import {CredentialFormat} from '../../../models/CredentialFormat';
import {UsersService} from '../../../services/users.service';
import {UserSummary} from '../../../models/UserSummary';

@Component({
  selector: 'app-credentials',
  templateUrl: './credentials.component.html',
  styleUrls: ['./credentials.component.scss']
})
export class CredentialsComponent implements OnInit {
  credentialsList: Array<Credential>;
  selectedCredentials: Credential[];
  cols: any[];
  panelLog: any;

  credentialFormatsList: CredentialFormat[];
  selectedCredentialFormat: CredentialFormat;
  newCredential: Credential = new Credential();

  availableUsers: UserSummary[];
  selectedOwner: UserSummary;

  currentAccountId: number;

  contentLoaded: boolean;

  displayAddDialog: boolean = false;
  displayDeleteDialog: boolean = false;
  displayEditDialog: boolean = false;

  showLog: boolean = false;
  requiredFields: boolean = false;

  hideShowLog() {
    this.showLog = !this.showLog;
  }

  constructor(private credentialsService: CredentialService,
              private userService: UsersService, private router: Router,
              private activatedRoute: ActivatedRoute) {
    this.contentLoaded = false;
    this.userService = userService;
    this.initCols();
  }

  ngOnInit() {
    this.currentAccountId = this.activatedRoute.snapshot.params['id'];
    this.loadCredentials();
  }

  initCols() {
    this.cols = [
      {field: 'credentialType', header: 'SharedCredential Type'},
      {field: 'referenceId', header: 'Reference'},
      {field: 'facility_code', header: 'Facility Code'},
      {field: 'credential', header: 'Hex Value'},
      {field: 'numBits', header: 'Number of Bits'},
      {field: 'ownerFullName', header: 'Owner'},
      {field: 'enableOn', header: 'Effective Date'},
      {field: 'expires', header: 'Effective To'}
    ];
  }

  async loadCredentials() {
    this.contentLoaded = false;
    this.selectedCredentials = [];
    this.credentialsList = await this.credentialsService.getCredentialsForAccount(this.currentAccountId);
    this.contentLoaded = true;
  }

  navigateBack() {
    this.router.navigate(['dashboard']);
  }

  showDialogToAdd() {
    this.prepareDialogToAdd()
      .then()
      .catch(error => console.error(error));
  }

  async prepareDialogToAdd() {
    this.requiredFields = false;
    this.newCredential = new Credential();
    this.newCredential.numBits = 32;

    let today = new Date();
    this.newCredential.enableOn = new Date(today.getFullYear(), today.getMonth(), today.getDay(), 0, 0, 0, 0);
    this.newCredential.expires = new Date(today.getFullYear(), today.getMonth(), today.getDay(), 23, 59, 59, 0);

    this.availableUsers = await this.userService.getUsersSummary(this.currentAccountId);
    this.selectedOwner = this.availableUsers[0];

    this.credentialFormatsList = await this.credentialsService.getCredentialFormatsForAccount();
    this.selectedCredentialFormat = this.credentialFormatsList[0];

    this.displayAddDialog = true;
  }

  closeAddDialog() {
    this.requiredFields = false;
    this.displayAddDialog = false;
  }

  addCredential() {
    this.requiredFields = false;
    if (this.selectedCredentialFormat.credentialFormatId === 100) {

      this.newCredential.ownerObjectId = this.selectedOwner.objectId;
      this.newCredential.credentialTypeId = this.selectedCredentialFormat.credentialFormatId;
      this.newCredential.accountId = this.currentAccountId;
      if (this.newCredential.facility_code === '' || this.newCredential.referenceId === '') {
        this.requiredFields = true;
      } else {
        this.credentialsService.addCredential26Bit(this.newCredential)
          .then((result) => {
            console.log(result);
            this.closeAddDialog();
            this.loadCredentials()
              .then()
              .catch(error => console.error(error));
          })
          .catch(error => console.error(error));

      }
    } else {
      this.newCredential.accountId = this.currentAccountId;
      this.newCredential.credentialTypeId = this.selectedCredentialFormat.credentialFormatId;
      this.newCredential.ownerObjectId = this.selectedOwner.objectId;

      let enableOn: Date = new Date(this.newCredential.enableOn.getTime() - (this.newCredential.enableOn.getTimezoneOffset() * 60000));
      let expires: Date = new Date(this.newCredential.expires.getTime() - (this.newCredential.expires.getTimezoneOffset() * 60000));
      this.newCredential.enableOn = enableOn;
      this.newCredential.expires = expires;

      /*  this.newCredential.credential = this.newCredential.credential;
        this.newCredential.numBits = this.newCredential.numBits;
        this.newCredential.referenceId = this.newCredential.referenceId;*/

      this.credentialsService.addCredential(this.newCredential)
        .then((result) => {
          console.log(result);
          this.closeAddDialog();
          this.loadCredentials()
            .then()
            .catch(error => console.error(error));
        })
        .catch(error => console.error(error));
    }
  }

  showDialogToEdit() {
    this.prepareDialogToEdit()
      .then()
      .catch(error => console.error(error));
  }

  async prepareDialogToEdit() {
    this.requiredFields = false;
    console.log(this.newCredential);
    this.newCredential = this.copyCredential(this.selectedCredentials[0]);

    this.availableUsers = await this.userService.getUsersSummary(this.currentAccountId);
    const selectedUserIndex = this.availableUsers.findIndex(user => user.objectId === this.newCredential.ownerObjectId);
    this.selectedOwner = this.availableUsers[selectedUserIndex];

    this.credentialFormatsList = await this.credentialsService.getCredentialFormatsForAccount();
    const selectedCredentialFormatIndex = this.credentialFormatsList.findIndex(
      credentialFormat => credentialFormat.credentialFormatId === this.newCredential.credentialTypeId);
    this.selectedCredentialFormat = this.credentialFormatsList[selectedCredentialFormatIndex];

    this.displayEditDialog = true;

  }

  copyCredential(credential: Credential): Credential {
    const duplicateCredential: Credential = new Credential();

    duplicateCredential.accountId = credential.accountId;
    duplicateCredential.credentialId = credential.credentialId;
    duplicateCredential.referenceId = credential.referenceId;
    duplicateCredential.credential = credential.credential;
    duplicateCredential.numBits = credential.numBits;
    duplicateCredential.ownerObjectId = credential.ownerObjectId;
    duplicateCredential.ownerFullName = credential.ownerFullName;
    duplicateCredential.credentialTypeId = credential.credentialTypeId;
    duplicateCredential.credentialType = credential.credentialType;
    duplicateCredential.facility_code = credential.facility_code;

    if (credential.expires != null) {
      const expires: Date = new Date(credential.expires);
      duplicateCredential.expires = expires;
    }
    if (credential.enableOn != null) {
      const enableOn: Date = new Date(credential.enableOn);
      duplicateCredential.enableOn = enableOn;
    }
    return duplicateCredential;
  }

  closeEditDialog() {
    this.requiredFields = false;
    this.displayEditDialog = false;
  }

  saveCredentialChanges() {
    this.requiredFields = false;
    if (this.selectedCredentialFormat.credentialFormatId === 100) {

      console.log(this.newCredential.enableOn);
      console.log(this.newCredential.expires);
      const enableOn: Date = new Date(this.newCredential.enableOn.getTime() - (this.newCredential.enableOn.getTimezoneOffset() * 60000));
      this.newCredential.enableOn = enableOn;
      if (this.newCredential.expires !== null) {
        const expires: Date = new Date(this.newCredential.expires.getTime() - (this.newCredential.expires.getTimezoneOffset() * 60000));
        this.newCredential.expires = expires;
      }

      this.newCredential.ownerObjectId = this.selectedOwner.objectId;

      if (this.newCredential.facility_code === '' || this.newCredential.referenceId === '') {
        this.requiredFields = true;
      } else {
        this.credentialsService.updateCredential26Bit(this.newCredential)
          .then(result => {
            this.loadCredentials();
            this.closeEditDialog();
          })
          .catch(error => console.error(error));

      }
    } else {
      const enableOn: Date = new Date(this.newCredential.enableOn.getTime() - (this.newCredential.enableOn.getTimezoneOffset() * 60000));
      const expires: Date = new Date(this.newCredential.expires.getTime() - (this.newCredential.expires.getTimezoneOffset() * 60000));
      this.newCredential.enableOn = enableOn;
      this.newCredential.expires = expires;

      this.newCredential.ownerObjectId = this.selectedOwner.objectId;
      this.newCredential.credentialTypeId = this.selectedCredentialFormat.credentialFormatId;

      this.credentialsService.updateCredential(this.newCredential)
        .then(result => {
          this.loadCredentials();
          this.closeEditDialog();
        })
        .catch(error => console.error(error));
    }
  }

  showDialogToDelete() {
    this.displayDeleteDialog = true;
  }

  deleteCredentials() {
    const credentialIds: number[] = this.buildSelectedCredentialIds();

    this.credentialsService.deleteCredentials(credentialIds)
      .then(result => {
        this.closeDeleteDialog();
        this.loadCredentials();
      })
      .catch(error => console.error(error));
  }

  buildSelectedCredentialIds(): number[] {
    let credentialIds: number[] = [];

    this.selectedCredentials.map(row => {
      credentialIds.push(row.credentialId);
    });

    console.log(credentialIds);

    return credentialIds;
  }

  closeDeleteDialog() {
    this.displayDeleteDialog = false;
  }
}
