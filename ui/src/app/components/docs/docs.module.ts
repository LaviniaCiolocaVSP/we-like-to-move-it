import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DocsComponent} from './container/docs.component';
import {
  ButtonModule,
  DataTableModule,
  DialogModule,
  DropdownModule,
  InputTextModule,
  MultiSelectModule,
  SharedModule
} from 'primeng/primeng';
import {TableModule} from 'primeng/table';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FormsModule} from '@angular/forms';
import {MatSelectModule} from "@angular/material";


@NgModule({
  declarations: [DocsComponent],
  imports: [
    CommonModule,
    DataTableModule,
    SharedModule,
    TableModule,
    BrowserAnimationsModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    TableModule,
    ButtonModule,
    DropdownModule,
    MultiSelectModule,
    MatSelectModule,
    FormsModule
  ]
})
export class DocsModule {
}
