import { Component, OnInit } from '@angular/core';
import {DocsService} from "../../../services/docs.service";
import {saveAs} from 'node_modules/file-saver';

@Component({
  selector: 'app-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss']
})
export class DocsComponent implements OnInit {

  docList: any[];

  constructor(private docsService: DocsService) { }

  ngOnInit() {
    this.docsService.getAvailableDocs().then(response => {
        this.docList = response;
      }).catch(error => {
        console.log("Error: " + error);
      });
  }

  downloadSelectedConfig(docName: string) {
    this.docsService
      .downloadDoc(docName)
      .then(response => {
        this.saveToFileSystem(response, docName);
      })
      .catch(error => console.error(error));
  }

  private saveToFileSystem(response, docName) {
    const jsonContent = JSON.stringify(response, null, ' ');

    const blob = new Blob([response], {type: "application/pdf"});

    saveAs(blob, docName);
  }
}
