import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HttpClientModule} from '@angular/common/http';
import {FormUploadComponent} from './container/form-upload.component';
import {AlertModule} from '../alert/alert.module';

@NgModule({
  declarations: [
    FormUploadComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule, AlertModule
  ],
  exports: [
    FormUploadComponent
  ]
})
export class FormUploadModule {
}
