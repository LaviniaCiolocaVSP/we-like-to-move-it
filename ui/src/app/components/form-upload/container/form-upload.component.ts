import {Component, EventEmitter, Input, Output} from '@angular/core';
import {AccountService} from '../../../services/account.service';
import {JsonFilesService} from '../../../services/json-files.service';
import {AlertService} from '../../../services/alert.service';

export type UploadType = 'account' | 'firmware';

@Component({
  selector: 'form-upload',
  templateUrl: './form-upload.component.html',
  styleUrls: ['./form-upload.component.scss']
})
export class FormUploadComponent {

  @Input('uploadType') uploadType: UploadType;

  selectedFiles: FileList = null;
  selectedFileName: string;

  @Output() loadAccountsEvent = new EventEmitter();
  @Output() loadFirmwareEvent = new EventEmitter();

  uploadedFile: File;
  progress: { percentage: number } = {percentage: 0};

  uploaded: boolean;
  uploadedMessage: string;

  constructor(private accountService: AccountService, private jsonFilesService: JsonFilesService,
              private alertService: AlertService) {
    this.alertService.clearAlert();
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
    if (this.selectedFiles.item(0) != null)
      this.selectedFileName = this.selectedFiles.item(0).name;
  }

  uploadFileForUploadType(): void {
    if (this.uploadType === 'account') {
      this.uploadAccountJSONFile();
    }
  }

  uploadAccountJSONFile(): void {
    this.progress.percentage = 0;
    this.alertService.clearAlert();
    this.uploadedMessage = '';
    this.uploadedFile = this.selectedFiles.item(0);
    if (this.uploadedFile.type !== 'application/json') {
      this.alertService.warning(
        'File type is not JSON');
      this.selectedFiles = undefined;
      this.selectedFileName = '';
    } else {
      this.jsonFilesService
        .createAccountFromFile(this.uploadedFile)
        .then(result => {
          this.uploaded = true;
          this.uploadedMessage = 'The account definition file was successfully uploaded!';
          this.selectedFiles = undefined;

          this.loadAccountsEvent.next();
        });
    }
  }

}
