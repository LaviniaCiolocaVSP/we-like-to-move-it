import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {InfoService} from "../../services/info.service";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  title = 'Account Migrator';
  version: string;

  constructor(
    private router: Router,
    private infoService: InfoService) {
  }

  ngOnInit() {
    this.infoService.getInfo().then(result => {
      this.version = result.version;
    });
  }

}
