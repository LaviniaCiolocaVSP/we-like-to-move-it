import {ScheduleSummary} from "./ScheduleSummary";

export class GroupPermission {
  deviceName: string;
  deviceId: number;
  deviceObjectId: number;
  scheduleId: number;
  securityActionId: number;
  deviceType: string;
  siteName: string;
  selectedSchedule: ScheduleSummary;
  availableSchedules: ScheduleSummary[];
}