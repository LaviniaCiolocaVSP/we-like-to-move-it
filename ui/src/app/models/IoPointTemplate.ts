export class IoPointTemplate {
  boardType: number;
  pointAddress: number;
  silkscreen: string;
  defaultLabel: string;
  type: number;
  eol: number;
  state: number;
}