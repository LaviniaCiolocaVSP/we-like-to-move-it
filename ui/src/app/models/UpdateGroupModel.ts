import {Group} from "./Group";
import {GroupPermission} from "./GroupPermission";

export class UpdateGroupModel {
  group: Group;
  groupPermissions: GroupPermission[];
}