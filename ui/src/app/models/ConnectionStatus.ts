export enum ConnectionStatus {
  Loading,
  Connected,
  Disconnected
}
