export class DevicePhysicalPoint {
  name: string;
  boardNumber: number;
  pointAddress: number;
  description: string;
  type: number;
}