import {TimerForm} from "./TimerForm";
import {DevicePhysicalPoint} from "./DevicePhysicalPoint";

export class ValidCredentialForm extends TimerForm {
  availableReaders: DevicePhysicalPoint[];
  selectedReader: DevicePhysicalPoint;
}
