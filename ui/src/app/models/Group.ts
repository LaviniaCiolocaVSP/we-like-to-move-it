import {GroupAntipassback} from "./GroupAntipassback";

export class Group {
  groupId: number;
  name: string;
  accountId: number;
  description: string;
  objectId: number;
  uiGroupAntipassbackDTO: GroupAntipassback;
  keypadUnlockHoldPrivileges: boolean;
  numberOfUsers: number;
}
