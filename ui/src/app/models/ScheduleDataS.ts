export class ScheduleDataS {
  scheduleId: number;

  start: string;
  stop: string;

  dayId: number;
  dayName: string;

  startTime: number;
  stopTime: number;

  startAMPM: number;
  stopAMPM: number;

}
