export class Event {
  eventId: number;
  panelObjectId: number;
  eventTime: Date;
  eventType: string;
  eventData: string;
  electronicSerialNumber: string;
}
