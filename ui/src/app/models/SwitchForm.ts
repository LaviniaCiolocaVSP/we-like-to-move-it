import {DevicePhysicalPoint} from "./DevicePhysicalPoint";
import {TimerForm} from "./TimerForm";

export class SwitchForm extends TimerForm {
  availableInputs: DevicePhysicalPoint[];
  input: DevicePhysicalPoint;
}