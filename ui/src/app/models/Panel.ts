export class Panel {
  brainId: number;
  objectId: number;
  panelCP: string;
  firmwareVersion: string;
  lastPanelHeartbeat: Date;
  panelType: string;
  panelTypeId: number;
  name: string;
  accountId: number;
  logPeriod: number;
  antipassBackResetInterval: number;
  networkId: number;
  password: string;
  timezone: string;
  buildVersion: string;
  ipAddress: string;
}
