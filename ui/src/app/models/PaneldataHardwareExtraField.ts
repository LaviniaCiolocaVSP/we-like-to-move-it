export class PaneldataHardwareExtraField {
  rootJsonTag: string;
  matchField: string;
  existingTags: string;
  tagToBeAddedOrUpdated: string;
  originalJsonText: string;
  id: number;
  values: string;
}
