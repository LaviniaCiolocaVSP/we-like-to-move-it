export class Device {
  accountId: number;
  brainId: number;
  deviceId: number;
  panelSerialNumber: string;
  panelName: string;
  name: string;
  isLockedDown: number;
  created: Date;
  deleted: number;
  twoFactorScheduleId: number;
  twoFactorInterval: number;
  cardRequiredScheduleId: number;
  siteId: number;
  siteObjectId: number;
  siteName: string;
  deviceType: string;
  deviceTypeId: number;
  panelOid: number;
  deviceObjectId: number;
}
