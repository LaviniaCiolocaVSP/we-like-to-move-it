export class Info {
  version: string;
  releaseDate: string;
  changeLog: Array<string>;
  previousVersions: Array<Info>;
}