export class Credential {
  credentialId: number;
  credentialType: string;
  credentialTypeId: number;
  ownerFullName: string;
  ownerObjectId: number;
  enableOn: Date = null;
  expires: Date = null;
  referenceId: string = '';
  credential: string;
  numBits: number;
  accountId: number;
  facility_code: string = '';
}
