export class ProgIoPoint {
  name: string;
  boardNumber: number;
  pointAddress: number;
  type: number;
  eol: number;
  state: number;
  inuse: number;
}