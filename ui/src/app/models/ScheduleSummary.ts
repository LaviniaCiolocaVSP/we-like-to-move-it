export class ScheduleSummary {
  scheduleId: number;
  scheduleName: string;
  siteId: number;
  siteName: string;
}