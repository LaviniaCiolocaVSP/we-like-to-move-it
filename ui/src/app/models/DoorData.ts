export class DoorData {
  notifyOnAjar: boolean;
  doorAjar: number;
  maxInvalid: number;
  invalidShutout: number;
  passthrough: number;
  notifyOnForced: boolean;
  rexFiresDoor: boolean;
  useAlarmShunt: boolean;
  alarmDelay: number;
}