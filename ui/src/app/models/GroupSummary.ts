export class GroupSummary {
  groupId: number;
  name: string;
  groupObjectId: number;
}