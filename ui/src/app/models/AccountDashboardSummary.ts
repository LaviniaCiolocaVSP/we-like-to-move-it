export class AccountDashboardSummary {
  accountId: number;
  accountName: string;
  panelsCount: number;
  doorsCount: number;
  devicesCount: number;
  holidaysCount: number;
  schedulesCount: number;
  groupsCount: number;
  usersCount: number;
  credentialsCount: number;
}
