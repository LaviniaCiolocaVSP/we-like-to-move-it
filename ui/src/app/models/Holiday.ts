import {Schedule} from './Schedule';

export class Holiday {
  holidayId: number;
  name: string;
  from: Date;
  to: Date;
  accountId: number;
  siteId: number;
  cadmHolidayId: number;
  isEditable: boolean;
  site: string;
  scheduleDTOSet: Schedule[];
}
