import {ScheduleDataS} from './ScheduleDataS';
import {Holiday} from './Holiday';

export class Schedule {
  scheduleId: number;
  scheduleName: string;
  description: string;
  scheduleTypeId: number;
  scheduleType: string;
  accountId: number;
  cadmScheduleId: number;
  siteId: number;
  siteName: string;
  isEditable: boolean;
  groupId: number;
  groupName: string;
  gracePeriod: number = 0;
  scheduleData: ScheduleDataS[];
  scheduleDataArray: string[];
  holidayDTOSet: Holiday[];

}
