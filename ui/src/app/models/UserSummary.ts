export class UserSummary {
  objectId: number;
  firstName: string;
  lastName: string;
  fullName: string;
}
