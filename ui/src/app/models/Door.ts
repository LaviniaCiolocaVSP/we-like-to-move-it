import {DoorData} from "./DoorData";

export class Door {
  accountId: number;
  deviceId: number;
  deviceObjectId: number;
  name: string;
  brainId: number;
  panelSerialNumber: string;
  boardDescription: string;
  boardNumber: number;
  nodeNumber: number;
  doorData: DoorData;
  maxRexExtension: number;
  doorUnlockScheduleName: string;
  doorUnlockScheduleId: number;
  twoFactorInterval: number;
  twoFactorSchedule: string;
  twoFactorScheduleId: number;
  cardRequiredSchedule: string;
  cardRequiredScheduleId: number;
  panelId: number;
  inOut: number;
  siteId: number;
  siteName: string;
  controlFromBrowser: boolean;
}
