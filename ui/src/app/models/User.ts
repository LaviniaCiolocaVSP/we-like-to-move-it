import {GroupSummary} from "./GroupSummary";

export class User {
  userId: number;
  objectId: number;
  accountId: number;
  userTypeId: number;
  firstName: string;
  lastName: string;
  isEditable: boolean;
  uiSecurityGroup: GroupSummary[];
  groupsString: string;
}
