import {IoPointTemplate} from './IoPointTemplate';

export class IOBoardForm {
  input1: IoPointTemplate;
  input2: IoPointTemplate;
  input3: IoPointTemplate;
  input4: IoPointTemplate;
  input5: IoPointTemplate;
  input6: IoPointTemplate;
  input7: IoPointTemplate;
  input8: IoPointTemplate;

  output1: IoPointTemplate;
  output2: IoPointTemplate;
  output3: IoPointTemplate;
  output4: IoPointTemplate;
  output5: IoPointTemplate;
  output6: IoPointTemplate;
  output7: IoPointTemplate;
  output8: IoPointTemplate;

  failSafeStateAllowed: boolean;
}