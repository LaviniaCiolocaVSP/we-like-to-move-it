import {IoPointTemplate} from './IoPointTemplate';

export class DoorBoardForm {
  door1Rex: IoPointTemplate;
  door1DoorContact: IoPointTemplate;
  door1DoorLock: IoPointTemplate;
  door1AuxRelay1: IoPointTemplate;
  door1AuxRelay2: IoPointTemplate;
  door1AuxInput1: IoPointTemplate;
  door1AuxInput2: IoPointTemplate;
  door1Reader: IoPointTemplate;

  door2Rex: IoPointTemplate;
  door2DoorContact: IoPointTemplate;
  door2DoorLock: IoPointTemplate;
  door2AuxRelay1: IoPointTemplate;
  door2AuxRelay2: IoPointTemplate;
  door2AuxInput1: IoPointTemplate;
  door2AuxInput2: IoPointTemplate;
  door2Reader: IoPointTemplate;

  failSafeStateAllowed: boolean;
}