import {ProgIoPoint} from './ProgIoPoint';

export class Board {
  boardLocation: string;
  boardType: string;
  boardTypeId: number;
  boardNumber: number;
  objectId: number;
  panelOid: number;
  boardDescription: string;
  progIoPoints: ProgIoPoint[];
}
