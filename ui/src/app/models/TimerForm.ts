import {DevicePhysicalPoint} from "./DevicePhysicalPoint";

export class TimerForm {
  availableOutputs: DevicePhysicalPoint[];
  disengageDelay: number;
  reportEngage: boolean;
  reportDisengage: boolean;
  engageMessage: string;
  disengageMessage: string;
  behavior: number;

  brainId: number;
  panelOid: number;
  panelSerialNumber: string;
  deviceId: number;
  name: string;
  created: Date;
  twoFactorScheduleId: number;
  twoFactorInterval: number;
  cardRequiredScheduleId: number;
  siteId: number;
  siteName: number;
  deviceType: string;
  deviceTypeId: number;
  accountId: number;

  outputs: DevicePhysicalPoint[];
  scheduleId: number;
}