import {Panel} from './Panel';
import {Device} from './Device';
import {Holiday} from './Holiday';
import {Schedule} from './Schedule';
import {Group} from './Group';
import {User} from './User';
import {Credential} from './Credential';

export class Account {
  fileName: string;
  accountId: number;
  accountName: string;
  panels: Panel[];
  doors: Device[];
  devices: Device[];
  holidays: Holiday[];
  schedules: Schedule[];
  groups: Group[];
  users: User[];
  credentials: Credential[];
}
