export class BoardTypesEnum {
  NULL_STATE = -1;
  DOOR_BOARD = 1;
  IO_BOARD = 2;
  EDGE_BOARD = 3;
  IPDC1_BOARD = 4;
  IPDC2_BOARD = 5;
  SALTO_ROUTER = 6;
  IPAC = 7;
  ALLEGION_GATEWAY = 8;
  MR52_BOARD = 9;
  EP4502_BOARD = 10;
  EP1502_BOARD = 11;
  ACS300_BOARD = 12;
  ACS6000_BOARD = 13;
}
