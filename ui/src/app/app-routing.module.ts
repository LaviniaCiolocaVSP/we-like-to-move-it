import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/container/home.component';
import {DoorsComponent} from './components/doors/container/doors.component';
import {PanelsComponent} from './components/panels/container/panels.component';
import {DashboardComponent} from './components/dashboard/container/dashboard.component';
import {UsersComponent} from './components/users/container/users.component';
import {GroupsComponent} from './components/groups/container/groups.component';
import {CredentialsComponent} from './components/credentials/container/credentials.component';
import {HolidaysComponent} from './components/holidays/container/holidays.component';
import {SchedulesComponent} from './components/schedules/container/schedules.component';
import {ScheduleDetailsComponent} from './components/schedules/details/scheduleDetails.component';

import {BoardsComponent} from './components/boards/container/boards.component';
import {DevicesComponent} from './components/devices/container/devices.component';
import {EditDoorBoardComponent} from './components/boards/editBoard/edit-door-board/edit-door-board.component';
import {DocsComponent} from './components/docs/container/docs.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'doors/:id', component: DoorsComponent},
  {path: 'devices/:id', component: DevicesComponent},
  {path: 'panels/:id', component: PanelsComponent},
  {path: 'dashboard', component: DashboardComponent},
  {path: 'users/:id', component: UsersComponent},
  {path: 'boards/:id/:panelOid', component: BoardsComponent},
  {path: 'doorBoard', component: EditDoorBoardComponent},
  {path: 'groups/:id', component: GroupsComponent},
  {path: 'credentials/:id', component: CredentialsComponent},
  {path: 'holidays/:id', component: HolidaysComponent},
  {path: 'schedules/:id', component: SchedulesComponent},
  {path: 'scheduleDetails/:accountId/:scheduleId', component: ScheduleDetailsComponent},
  {path: 'docs', component: DocsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
