import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HomeModule} from './components/home/home.module';
import {DoorsModule} from './components/doors/doors.module';
import {PanelsModule} from './components/panels/panels.module';
import {MenuComponent} from './components/menu/menu.component';
import {DashboardModule} from './components/dashboard/dashboard.module';
import {DropdownModule} from 'primeng/primeng';

import {AccountService} from './services/account.service';
import {CommonService} from './services/common.service';
import {UsersModule} from './components/users/users.module';
import {GroupsModule} from './components/groups/groups.module';
import {CredentialsModule} from './components/credentials/credentials.module';
import {HolidaysModule} from './components/holidays/holidays.module';
import {SchedulesModule} from './components/schedules/schedules.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {BoardsModule} from './components/boards/boards.module';
import {DocsModule} from './components/docs/docs.module';
import {DevicesModule} from './components/devices/devices.module';
import {RouterModule} from '@angular/router';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HomeModule,
    DoorsModule,
    DevicesModule,
    PanelsModule,
    DashboardModule,
    UsersModule,
    GroupsModule,
    CredentialsModule,
    DropdownModule,
    HolidaysModule,
    SchedulesModule,
    BrowserAnimationsModule,
    BoardsModule,
    DocsModule,
    RouterModule
  ],
  providers: [AccountService, CommonService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
