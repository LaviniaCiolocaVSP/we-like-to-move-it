import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommonService} from './common.service';
import {Door} from '../models/Door';
import {Device} from "../models/Device";
import {SwitchForm} from "../models/SwitchForm";
import {SelectItem} from "primeng/api";
import {TimerForm} from "../models/TimerForm";
import {Definitions} from "../constants/Definitions";
import {EventTrackForm} from "../models/EventTrackForm";
import {ValidCredentialForm} from "../models/ValidCredentialForm";

class ArrayOfDoors extends Array<Door> {
}

class ArrayOfDevices extends Array<Device> {
}

@Injectable({
  providedIn: 'root'
})
export class DevicesService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'device';
  }

  getDoorsForAccount(accountId: number): Promise<ArrayOfDoors> {
    return this.http
               .get<ArrayOfDoors>(`${this.httpAd}${this.entity}/doors/${accountId}`)
               .toPromise();
  }

  getDoorsForPanel(panelOid: number): Promise<ArrayOfDoors> {
    return this.http
               .get<ArrayOfDoors>(`${this.httpAd}${this.entity}/doors/ofPanel/${panelOid}`)
               .toPromise();
  }

  deleteDoors(doorIds: number[]): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(doorIds)
    };

    return this.http
               .delete(`${this.httpAd}${this.entity}/doors`, options)
               .toPromise();
  }

  getDevicesForAccount(accountId: number): Promise<ArrayOfDevices> {
    return this.http
               .get<ArrayOfDevices>(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }

  deleteDevices(doorIds: number[]): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(doorIds)
    };

    return this.http
               .delete(`${this.httpAd}${this.entity}/devices`, options)
               .toPromise();
  }

  addDoor(door: Door): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/door`, door)
               .toPromise();
  }

  updateDoor(door: Door): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}/door`, door)
               .toPromise();
  }

  getSwitchTemplateByPanelOid(panelOid: number): Promise<SwitchForm> {
    return this.http
               .get<SwitchForm>(`${this.httpAd}${this.entity}/switchTemplate/${panelOid}`)
               .toPromise();
  }

  getSwitchByDeviceId(deviceId: number): Promise<SwitchForm> {
    return this.http
               .get<SwitchForm>(`${this.httpAd}${this.entity}/switch/${deviceId}`)
               .toPromise();
  }

  getTimerTemplateByPanelOid(panelOid: number): Promise<TimerForm> {
    return this.http
               .get<TimerForm>(`${this.httpAd}${this.entity}/timerTemplate/${panelOid}`)
               .toPromise();
  }

  getTimerByDeviceId(deviceId: number): Promise<TimerForm> {
    return this.http
               .get<TimerForm>(`${this.httpAd}${this.entity}/timer/${deviceId}`)
               .toPromise();
  }

  getEventTrackTemplateByPanelOid(panelOid: number): Promise<EventTrackForm> {
    return this.http
               .get<EventTrackForm>(`${this.httpAd}${this.entity}/eventTrackTemplate/${panelOid}`)
               .toPromise();
  }

  getValidCredentialTemplateByPanelOid(panelOid: number): Promise<ValidCredentialForm> {
    return this.http
               .get<ValidCredentialForm>(`${this.httpAd}${this.entity}/validCredentialTemplate/${panelOid}`)
               .toPromise();
  }

  getEventTrackByDeviceId(deviceId: number): Promise<EventTrackForm> {
    return this.http
               .get<EventTrackForm>(`${this.httpAd}${this.entity}/eventTrack/${deviceId}`)
               .toPromise();
  }

  addEventTrack(eventTrackDevice: EventTrackForm): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/eventTrack`, eventTrackDevice)
               .toPromise();
  }

  updateEventTrack(eventTrackDevice: EventTrackForm): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}/eventTrack`, eventTrackDevice)
               .toPromise();
  }

  addSwitch(switchDevice: SwitchForm): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/switch`, switchDevice)
               .toPromise();
  }

  updateSwitch(switchDevice: SwitchForm): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}/switch`, switchDevice)
               .toPromise();
  }

  addTimer(timerDevice: TimerForm): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/timer`, timerDevice)
               .toPromise();
  }

  updateTimer(timerDevice: TimerForm): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}/timer`, timerDevice)
               .toPromise();
  }

  addValidCredential(validCredentialDevice: ValidCredentialForm): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/validCredential`, validCredentialDevice)
               .toPromise();
  }

  updateValidCredential(validCredentialDevice: ValidCredentialForm): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}/validCredential`, validCredentialDevice)
               .toPromise();
  }

  getValidCredentialByDeviceId(deviceId: number): Promise<ValidCredentialForm> {
    return this.http
               .get<ValidCredentialForm>(`${this.httpAd}${this.entity}/validCredential/${deviceId}`)
               .toPromise();
  }

  getOutputBehaviors(): SelectItem[] {
    return [
      {label: Definitions.FOLLOW, value: '2'},
      {label: Definitions.LATCH, value: '0'},
      {label: Definitions.UNLATCH, value: '1'},
      {label: Definitions.PULSE, value: '3'}];
  }
}
