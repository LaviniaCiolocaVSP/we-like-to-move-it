import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class SessionService {

  private communicationBetweenComponents = new BehaviorSubject<number>(0);
  communicationItem = this.communicationBetweenComponents.asObservable();

  /* The number received as a parameter is not used in our case, we just want to update a component */
  updateOfComponent(number) {
    this.communicationBetweenComponents.next(number);
  }
}
