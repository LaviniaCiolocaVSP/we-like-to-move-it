import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Holiday} from '../models/Holiday';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class HolidayService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'holiday';
  }

  getHolidays(accountId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }

  addHoliday(holiday: Holiday, dateFrom: Date, dateTo: Date): Promise<any> {
    holiday.from = dateFrom;
    holiday.to = dateTo;
    console.log(holiday);
    return this.http
               .post(`${this.httpAd}${this.entity}`, holiday)
               .toPromise();
  }

  deleteHolidays(holidayIds: number[]) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(holidayIds)
    };

    return this.http
               .delete(`${this.httpAd}${this.entity}`, options)
               .toPromise();
  }

  updateHoliday(holiday: Holiday, dateFrom: Date, dateTo: Date) {
    holiday.from = dateFrom;
    holiday.to = dateTo;
    console.log(holiday);
    return this.http
               .put(`${this.httpAd}${this.entity}`, holiday)
               .toPromise();
  }
}
