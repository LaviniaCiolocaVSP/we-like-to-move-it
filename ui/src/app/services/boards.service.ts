import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommonService} from './common.service';
import {Board} from '../models/Board';
import {BoardType} from '../models/BoardType';
import {BoardNode} from '../models/BoardNode';
import {DoorBoardForm} from '../models/DoorBoardForm';
import {IOBoardForm} from '../models/IOBoardForm';

class ArrayOfBoards extends Array<Board> {
}

class ArrayOfBoardTypes extends Array<BoardType> {
}

class ArrayOfNodes extends Array<BoardNode> {
}

class ArrayOfNumbers extends Array<number> {

}


@Injectable({
  providedIn: 'root'
})
export class BoardsService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'board';
  }

  getBoardsForPanel(panelOid: number): Promise<ArrayOfBoards> {
    return this.http
      .get<ArrayOfBoards>(`${this.httpAd}${this.entity}/${panelOid}`)
      .toPromise();
  }

  async getNodesForBoard(boardObjectId: number): Promise<ArrayOfNodes> {
    return this.http
      .get<ArrayOfNodes>(`${this.httpAd}${this.entity}/nodes/${boardObjectId}`)
      .toPromise();
  }

  async getNodesForBoardWithNone(boardObjectId: number): Promise<ArrayOfNodes> {
    let noNode = new BoardNode();
    noNode.nodeDescription = '';
    noNode.nodeNumber = 0;

    let nodes = await this.getNodesForBoard(boardObjectId);

    let allNodes = [noNode, ...nodes];

    return allNodes;
  }

  async getBoardsForPanelWithNone(panelOid: number): Promise<ArrayOfBoards> {
    let noBoard = new Board();
    noBoard.boardDescription = '';
    noBoard.objectId = 0;

    let boards = await this.getBoardsForPanel(panelOid);

    let allBoards = [noBoard, ...boards];

    return allBoards;
  }

  addBoard(board: Board): Promise<any> {
    return this.http
      .post(`${this.httpAd}${this.entity}`, board)
      .toPromise();
  }

  updateBoard(board: Board): Promise<any> {
    return this.http
      .put(`${this.httpAd}${this.entity}`, board)
      .toPromise();
  }

  getBoardTypes(): Promise<ArrayOfBoardTypes> {
    return this.http
      .get<ArrayOfBoardTypes>(`${this.httpAd}${this.entity}/boardTypes`)
      .toPromise();
  }

  getBoardTypesForPanelType(panelOid: number): Promise<ArrayOfBoardTypes> {
    return this.http
      .get<ArrayOfBoardTypes>(`${this.httpAd}${this.entity}/boardTypes/${panelOid}`)
      .toPromise();
  }

  getAvailableBoardNumbers(panelOid: number): Promise<ArrayOfNumbers> {
    return this.http
      .get<ArrayOfNumbers>(`${this.httpAd}${this.entity}/availableBoardNumbers/${panelOid}`)
      .toPromise();
  }


  deleteBoards(boardIds: number[]): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(boardIds)
    };

    return this.http
      .delete(`${this.httpAd}${this.entity}`, options)
      .toPromise();
  }

  getDoorBoardPoints(panelOid: number, boardTypeId: number, newBoard: number, boardNumber: number): Promise<DoorBoardForm> {
    console.log(newBoard);
    if (newBoard === 1) {
      return this.http
        .get<DoorBoardForm>(`${this.httpAd}${this.entity}/doorBoardTemplatePoints/${panelOid}/${boardTypeId}`)
        .toPromise();
    } else {
      return this.http
        .get<DoorBoardForm>(`${this.httpAd}${this.entity}/doorBoardProgIoPoints/${panelOid}/${boardNumber}`)
        .toPromise();
    }
  }

  getIOBoardPoints(panelOid: number, boardTypeId: number, newBoard: number, boardNumber: number): Promise<IOBoardForm> {
    console.log(newBoard);
    if (newBoard === 1) {
      return this.http
        .get<IOBoardForm>(`${this.httpAd}${this.entity}/ioBoardTemplatePoints/${panelOid}/${boardTypeId}`)
        .toPromise();
    } else {
      return this.http
        .get<IOBoardForm>(`${this.httpAd}${this.entity}/ioBoardProgIoPoints/${panelOid}/${boardNumber}`)
        .toPromise();
    }
  }
}
