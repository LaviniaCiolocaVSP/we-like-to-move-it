import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  protected entity: string = '';
  protected httpAd: string = 'http://localhost:8080/';

  constructor(protected http: HttpClient) {
  }
}
