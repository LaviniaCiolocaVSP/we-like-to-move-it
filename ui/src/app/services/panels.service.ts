import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommonService} from './common.service';
import {Panel} from "../models/Panel";
import {PanelType} from "../models/PanelType";

class ArrayOfPanels extends Array<Panel> {
}

class ArrayOfPanelTypes extends Array<PanelType> {

}

@Injectable({
  providedIn: 'root'
})
export class PanelsService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'panel';
  }

  getPanelsForAccount(accountId: number): Promise<ArrayOfPanels> {
    return this.http
               .get<ArrayOfPanels>(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }

  async getPanelsForAccountWithNone(accountId: number): Promise<ArrayOfPanels> {
    let noPanel = new Panel();
    noPanel.brainId = 0;
    noPanel.name = '';

    let panels = await this.getPanelsForAccount(accountId);

    let allPanels = [noPanel, ...panels];

    return allPanels;
  }

  getPanelByBrainId(brainId: number): Promise<Panel> {
    return this.http
               .get<Panel>(`${this.httpAd}${this.entity}/get/${brainId}`)
               .toPromise();
  }

  getPanelBySerialNumber(panelSerialNumber: string): Promise<Panel> {
    return this.http
               .get<Panel>(`${this.httpAd}${this.entity}/get/serialNumber/${panelSerialNumber}`)
               .toPromise();
  }

  getPanelByObjectId(objectId: number): Promise<Panel> {
    return this.http
               .get<Panel>(`${this.httpAd}${this.entity}/get/object/${objectId}`)
               .toPromise();
  }

  getPanelTypes(): Promise<ArrayOfPanelTypes> {
    return this.http
               .get<ArrayOfPanelTypes>(`${this.httpAd}${this.entity}/panelTypes`)
               .toPromise();
  }

  deletePanels(panelIds: number[]): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(panelIds)
    };

    return this.http
               .delete(`${this.httpAd}${this.entity}`, options)
               .toPromise();
  }

  addPanel(panel: Panel): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}`, panel)
               .toPromise();
  }

  updatePanel(panel: Panel): Promise<any> {
    return this.http
               .put(`${this.httpAd}${this.entity}`, panel)
               .toPromise();
  }
}
