import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/User';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends CommonService {

  private url: string;

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'user';

    this.url = `${this.httpAd}${this.entity}`;
  }

  getUsers(accountId: number): Promise<any> {
    return this.http
      .get(`${this.url}/${accountId}`)
      .toPromise();
  }

  getUsersSummary(accountId: number): Promise<any> {
    return this.http
      .get(`${this.url}/summary/${accountId}`)
      .toPromise();
  }

  addUser(user: User): Promise<any> {
    return this.http
      .post(`${this.url}/add`, user)
      .toPromise();
  }

  deleteUsers(users_id: number[]) {
    return this.http
      .post(`${this.url}/delete`, users_id)
      .toPromise();
  }

  updateUser(user: User) {
    return this.http
      .post(`${this.url}/update`, user)
      .toPromise();
  }

  getUsersForGroup(groupId: number): Promise<any> {
    return this.http
      .get(`${this.url}/group/${groupId}`)
      .toPromise();
  }
}
