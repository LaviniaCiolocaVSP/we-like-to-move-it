import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommonService} from './common.service';
import {Credential} from '../models/Credential';

class ArrayOfCredentials extends Array<Credential> {
}

@Injectable({
  providedIn: 'root'
})
export class CredentialsService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'credential';
  }

  getCredentialsForAccount(accountId: number): Promise<ArrayOfCredentials> {
    return this.http
               .get<ArrayOfCredentials>(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }
}
