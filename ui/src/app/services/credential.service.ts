import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CommonService} from './common.service';
import {Credential} from '../models/Credential';
import {CredentialFormat} from '../models/CredentialFormat';

class ArrayOfCredentials extends Array<Credential> {
}

class ArrayOfCredentialFormats extends Array<CredentialFormat> {
}

@Injectable({
  providedIn: 'root'
})
export class CredentialService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'credential';
  }

  getCredentialsForAccount(accountId: number): Promise<ArrayOfCredentials> {
    return this.http
      .get<ArrayOfCredentials>(`${this.httpAd}${this.entity}/${accountId}`)
      .toPromise();
  }

  getCredentialsForPanel(panelOid: number): Promise<ArrayOfCredentials> {
    return this.http
      .get<ArrayOfCredentials>(`${this.httpAd}${this.entity}/panel/${panelOid}`)
      .toPromise();
  }

  getCredentialFormatsForAccount(): Promise<ArrayOfCredentialFormats> {
    return this.http
      .get<ArrayOfCredentialFormats>(`${this.httpAd}${this.entity}/credentialFormats`)
      .toPromise();
  }

  addCredential(credential: Credential): Promise<any> {
    return this.http
      .post(`${this.httpAd}${this.entity}`, credential)
      .toPromise();
  }

  addCredential26Bit(credential: Credential): Promise<any> {
    return this.http
      .post(`${this.httpAd}${this.entity}/26Bit`, credential)
      .toPromise();
  }

  deleteCredentials(credentialIds: number[]): Promise<any> {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(credentialIds)
    };

    return this.http
      .delete(`${this.httpAd}${this.entity}`, options)
      .toPromise();
  }

  updateCredential(credential: Credential): Promise<any> {
    return this.http
      .put(`${this.httpAd}${this.entity}`, credential)
      .toPromise();
  }

  updateCredential26Bit(credential: Credential): Promise<any> {
    return this.http
      .put(`${this.httpAd}${this.entity}/26Bit`, credential)
      .toPromise();
  }
}
