import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {CommonService} from "./common.service";

@Injectable({
  providedIn: 'root'
})
export class DocsService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'docs';
  }

  downloadDoc(docName: string): Promise<any> {
    return this.http
      .get(`${this.httpAd}${this.entity}/download/${docName}`, {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/pdf')
          .set('Accept', 'application/pdf'),
        responseType: 'blob'
      })
      .toPromise();
  }

  getAvailableDocs(): Promise<any> {
    return this.http
      .get(`${this.httpAd}${this.entity}`)
      .toPromise();
  }
}
