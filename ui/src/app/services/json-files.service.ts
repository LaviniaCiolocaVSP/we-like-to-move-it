import {Injectable} from '@angular/core';
import {CommonService} from "./common.service";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Account} from "../models/Account";
import {ResponseMessage} from '../models/messages/ResponseMessage';

@Injectable({
  providedIn: 'root'
})
export class JsonFilesService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'json';
  }

  getJSONFiles(): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}`)
               .toPromise();
  }

  createAccountFromFile(file: File): Promise<any> {
    const formData: FormData = new FormData();
    formData.append('file', file);

    const url = `${this.httpAd}${this.entity}`;
    return this.http
               .post(url, formData)
               .toPromise();
  }

  deleteAccount(fileName: string): Promise<ResponseMessage> {
    const url = `${this.httpAd}${this.entity}`;
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: fileName,
    };
    return this.http
               .delete<ResponseMessage>(url, options)
               .toPromise();
  }

  download(accountName: string): Promise<Account> {
    return this.http
               .get<Account>(`${this.httpAd}${this.entity}/download/${accountName}`)
               .toPromise();
  }
}
