import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Schedule} from '../models/Schedule';
import {CommonService} from './common.service';

@Injectable({
  providedIn: 'root'
})
export class ScheduleService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'schedule';
  }

  getSchedulesForAccountBySite(accountId: number, siteObjectId: number): Promise<any> {
    console.log('SITE');
    console.log(siteObjectId);
    return this.http.get(`${this.httpAd}${this.entity}/bysite/${accountId}/${siteObjectId}`).toPromise();
  }

  async getSchedulesForAccountBySiteWithNone(accountId: number, siteObjectId: number): Promise<any> {
    console.log('Getting schedules with none...');
    let noSchedule = new Schedule();
    noSchedule.scheduleId = 0;
    noSchedule.scheduleName = 'NONE';

    let schedules = await this.getSchedulesForAccountBySite(accountId, siteObjectId);

    const allSchedules = [noSchedule, ...schedules];

    console.log('With none');
    console.log(allSchedules);

    return allSchedules;
  }

  getSchedules(accountId: number): Promise<any> {
    return this.http.get(`${this.httpAd}${this.entity}/${accountId}`).toPromise();
  }

  async getSchedulesForAccountWithNone(accountId: number): Promise<any> {
    let noSchedule = new Schedule();
    noSchedule.scheduleId = 0;
    noSchedule.scheduleName = 'NONE';

    let schedules = await this.getSchedules(accountId);

    let allSchedules = [noSchedule, ...schedules];

    return allSchedules;
  }

  addSchedule(schedule: Schedule): Promise<any> {
    return this.http.post(`${this.httpAd}${this.entity}`, schedule).toPromise();
  }

  deleteSchedules(scheduleIds: number[]) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(scheduleIds)
    };

    return this.http
               .delete(`${this.httpAd}${this.entity}`, options)
               .toPromise();
  }

  updateSchedule(schedule: Schedule) {
    return this.http.put(`${this.httpAd}${this.entity}`, schedule).toPromise();
  }

  getSchedule(scheduleId: number): Promise<any> {
    return this.http.get(`${this.httpAd}${this.entity}/get/${scheduleId}`).toPromise();
  }

  getSchedulesForHoliday(holidayId: number): Promise<any> {
    return this.http.get(`${this.httpAd}${this.entity}/get/holiday/${holidayId}`).toPromise();
  }
}
