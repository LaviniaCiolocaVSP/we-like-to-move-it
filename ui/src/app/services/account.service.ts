import {Injectable} from '@angular/core';
import {CommonService} from './common.service';
import {HttpClient} from '@angular/common/http';
import {Account} from '../models/Account';
import {BrivoAccount} from '../models/BrivoAccount';
import {AccountDashboardSummary} from '../models/AccountDashboardSummary';
import {DatabaseConnection} from '../models/DatabaseConnection';

class ArrayOfAccounts extends Array<BrivoAccount> {
}

@Injectable({
  providedIn: 'root'
})
export class AccountService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'account';
  }

  getAccountsFromBrivo(nameFilter: string, pageIndex: number, pageSize: number): Promise<ArrayOfAccounts> {
    const requestParams = `?nameFilter=${nameFilter}&pageIndex=${pageIndex}&pageSize=${pageSize}`;
    return this.http
               .get<ArrayOfAccounts>(`${this.httpAd}${this.entity}${requestParams}`)
               .toPromise();
  }

  getCurrentDatabaseAccount(): Promise<BrivoAccount> {
    return this.http
               .get<BrivoAccount>(`${this.httpAd}${this.entity}/current`)
               .toPromise();
  }

  getCurrentDatabaseAccountDashboardSummary(): Promise<AccountDashboardSummary> {
    return this.http
               .get<AccountDashboardSummary>(`${this.httpAd}${this.entity}/currentDashboardSummary`)
               .toPromise();
  }

  downloadBrivoAccount(accountId: number): Promise<Account> {
    return this.http
               .get<Account>(`${this.httpAd}${this.entity}/download/brivo/${accountId}`)
               .toPromise();
  }

  downloadCurrentAccount(accountId: number): Promise<Account> {
    return this.http
               .get<Account>(`${this.httpAd}${this.entity}/download/current/${accountId}`)
               .toPromise();
  }

  selectAccount(accountName: string): Promise<Account> {
    return this.http
               .put<Account>(`${this.httpAd}${this.entity}/current?accountName=${accountName}`, {})
               .toPromise();
  }

  getDatabaseConnectionName(): Promise<DatabaseConnection> {
    return this.http
               .get<DatabaseConnection>(`${this.httpAd}${this.entity}/databaseConnectionName`, {})
               .toPromise();
  }
}
