import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CommonService} from './common.service';
import {Info} from "../models/Info";

@Injectable({
  providedIn: 'root'
})
export class InfoService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
  }

  getInfo(): Promise<Info> {
    return this.http.get<Info>(`${this.httpAd}info`).toPromise();
  }
}
