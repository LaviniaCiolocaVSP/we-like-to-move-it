import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CommonService} from './common.service';
import {Site} from "../models/Site";

class ArrayOfSites extends Array<Site> {
}

@Injectable({
  providedIn: 'root'
})
export class SitesService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'site';
  }

  getSitesForAccount(accountId: number): Promise<ArrayOfSites> {
    return this.http
               .get<ArrayOfSites>(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }

  getSitesForAccountToEditGroup(accountId: number): Promise<ArrayOfSites> {
    return this.http
               .get<ArrayOfSites>(`${this.httpAd}${this.entity}/addAllSitesAndCurrentPrivileges/${accountId}`)
               .toPromise();
  }

  async getSitesForAccountWithNone(accountId: number): Promise<ArrayOfSites> {
    let noSite = new Site();
    noSite.siteId = 0;
    noSite.siteName = '';

    let sites = await this.getSitesForAccount(accountId);

    let allSites = [noSite, ...sites];

    return allSites;
  }
}
