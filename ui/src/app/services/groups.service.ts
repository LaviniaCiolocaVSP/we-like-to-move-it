import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Group} from '../models/Group';
import {CommonService} from './common.service';
import {GroupPermission} from "../models/GroupPermission";
import {UpdateGroupModel} from "../models/UpdateGroupModel";

@Injectable({
  providedIn: 'root'
})
export class GroupsService extends CommonService {

  constructor(http: HttpClient) {
    super(http);
    this.entity = 'group';
  }

  getGroups(accountId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/${accountId}`)
               .toPromise();
  }

  getGroupsSummary(accountId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/summary/${accountId}`)
               .toPromise();
  }

  async getGroupPermissions(accountId: number, groupId: number, siteId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/groupPermissions/${accountId}/${groupId}/${siteId}`)
               .toPromise();
  }

  getDeviceGroups(accountId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/devices/${accountId}`)
               .toPromise();
  }

  async getDeviceGroupsWithNone(accountId: number): Promise<any> {
    let universalGroup = new Group();
    universalGroup.name = 'Universal';
    universalGroup.description = 'Universal';
    universalGroup.objectId = 0;

    let availableDeviceGroups = await this.getDeviceGroups(accountId);

    let allDeviceGroups = [universalGroup, ...availableDeviceGroups];

    return allDeviceGroups;
  }

  getUsersGroups(accountId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/users/${accountId}`)
               .toPromise();
  }

  async getUsersGroupsWithNone(accountId: number): Promise<any> {
    let noneGroup = new Group();
    noneGroup.name = 'None';
    noneGroup.description = 'None';
    noneGroup.objectId = 0;

    let availableUserGroups = await this.getUsersGroups(accountId);

    let allUserGroups = [noneGroup, ...availableUserGroups];

    return allUserGroups;
  }

  addGroup(group: Group): Promise<any> {
    return this.http
               .post(`${this.httpAd}${this.entity}/add`, group)
               .toPromise();
  }

  deleteGroups(groupObjectIds: number[]) {
    const options = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
      body: JSON.stringify(groupObjectIds)
    };


    return this.http
               .delete(`${this.httpAd}${this.entity}`, options)
               .toPromise();
  }

  updateGroup(group: Group, groupPermissions: GroupPermission[]) {
    const updateGroupModel: UpdateGroupModel = new UpdateGroupModel();
    updateGroupModel.group = group;
    updateGroupModel.groupPermissions = groupPermissions;

    console.log('UpdateGroupModel');
    console.log(updateGroupModel);

    return this.http
               .put(`${this.httpAd}${this.entity}`, updateGroupModel)
               .toPromise();
  }

  getUsersGroupsForDevice(deviceId: number): Promise<any> {
    return this.http
               .get(`${this.httpAd}${this.entity}/users/device/${deviceId}`)
               .toPromise();
  }

}
