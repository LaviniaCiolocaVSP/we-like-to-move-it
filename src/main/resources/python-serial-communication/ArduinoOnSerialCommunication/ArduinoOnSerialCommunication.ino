/*
 Wiegand Simulator!
*/

#define READYFORINPUTSTRING ">"
#define READER_1_DATA0_PIN 2
#define READER_1_DATA1_PIN 3

#define READER_2_DATA0_PIN 4
#define READER_2_DATA1_PIN 5

#define READER_3_DATA0_PIN 6
#define READER_3_DATA1_PIN 7

#define READER_4_DATA0_PIN 8
#define READER_4_DATA1_PIN 9

#define READER_5_DATA0_PIN 10
#define READER_5_DATA1_PIN 11

#define DOOR_PIN 12

#define PULSE_WIDTH_MICROSECONDS 20
#define INTER_PULSE_DELAY_MILLISECONDS 10

int analogPin = A0;
int analog = 0;
int analogReadCounter = 0;
int relayStatus = 0;
int lastRelayStatus = 0;

// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);

  pinMode(A0, INPUT_PULLUP);

  pinMode(READER_1_DATA0_PIN, OUTPUT);
  pinMode(READER_1_DATA1_PIN, OUTPUT);

  pinMode(READER_2_DATA0_PIN, OUTPUT);
  pinMode(READER_2_DATA1_PIN, OUTPUT);

  pinMode(READER_3_DATA0_PIN, OUTPUT);
  pinMode(READER_3_DATA1_PIN, OUTPUT);

  pinMode(READER_4_DATA0_PIN, OUTPUT);
  pinMode(READER_4_DATA1_PIN, OUTPUT);

  pinMode(READER_5_DATA0_PIN, OUTPUT);
  pinMode(READER_5_DATA1_PIN, OUTPUT);

  pinMode(DOOR_PIN, OUTPUT);

  digitalWrite(READER_1_DATA0_PIN, HIGH);
  digitalWrite(READER_1_DATA1_PIN, HIGH);

  digitalWrite(READER_2_DATA0_PIN, HIGH);
  digitalWrite(READER_2_DATA1_PIN, HIGH);

  digitalWrite(READER_3_DATA0_PIN, HIGH);
  digitalWrite(READER_3_DATA1_PIN, HIGH);

  digitalWrite(READER_4_DATA0_PIN, HIGH);
  digitalWrite(READER_4_DATA1_PIN, HIGH);

  digitalWrite(READER_5_DATA0_PIN, HIGH);
  digitalWrite(READER_5_DATA1_PIN, HIGH);

  digitalWrite(DOOR_PIN, LOW);

  //Serial.println(READYFORINPUTSTRING);
}

int sendWiegandBitString(char readerNumber, String bitString) {
  int data0Pin = READER_1_DATA0_PIN;
  int data1Pin = READER_1_DATA1_PIN;

  switch(readerNumber) {
   case '2'  :
      data0Pin = READER_2_DATA0_PIN;
      data1Pin = READER_2_DATA1_PIN;
      break;
   case '3'  :
      data0Pin = READER_3_DATA0_PIN;
      data1Pin = READER_3_DATA1_PIN;
      break;
   case '4'  :
      data0Pin = READER_4_DATA0_PIN;
      data1Pin = READER_4_DATA1_PIN;
      break;
   case '5'  :
      data0Pin = READER_5_DATA0_PIN;
      data1Pin = READER_5_DATA1_PIN;
      break;
  }

  for(int i = 0; i < bitString.length(); i++) {
    if(bitString[i] == '0') {
      digitalWrite(data0Pin, LOW);
      delayMicroseconds(PULSE_WIDTH_MICROSECONDS);
      digitalWrite(data0Pin, HIGH);
    } else {
      digitalWrite(data1Pin, LOW);
      delayMicroseconds(PULSE_WIDTH_MICROSECONDS);
      digitalWrite(data1Pin, HIGH);
    }

    delay(INTER_PULSE_DELAY_MILLISECONDS);
  }

}

// the loop routine runs over and over again forever:
void loop() {
  int analogReadValue = analogRead(A0);

  if (analogReadCounter == 10000) {
    analogReadCounter = 0;

    // Serial.println(analogReadValue);

    // Value is 20 for switch set to DRY
    if (analogRead(A0) < 20) {
      relayStatus = 1;
    } else {
      relayStatus = 0;
    }
    if (lastRelayStatus == 0 && relayStatus == 1) {
      Serial.println("RELAY OPEN");
    }
    if (lastRelayStatus == 1 && relayStatus == 0) {
      Serial.println("RELAY CLOSED");
    }
  } else {
    analogReadCounter++;
  }

  while (Serial.available() > 0 ) {

   String str = Serial.readString();
   //Serial.print("Received from serial: ");
   Serial.println(str);

   str.trim(); // trims all the whitespace to make sure the comparison works
   if (str == "DOOR_FORCED_OPEN") {

      Serial.println("Forcing door open");
      digitalWrite(DOOR_PIN, HIGH);
      delay(2000);
      digitalWrite(DOOR_PIN, LOW);
      delay(10000);
      //Serial.println(READYFORINPUTSTRING);

   } else if (str == "DOOR_AJAR_CLOSED") {

      Serial.println("++++Setting door pin to high+++");
      digitalWrite(DOOR_PIN, HIGH);

      delay(2000);

      int val = digitalRead(DOOR_PIN);
      Serial.print("+++val: ");
      Serial.print(val);
   } else if (str == "DOOR_AJAR_OPEN") {

      Serial.println("----Setting door pin to low----");
      digitalWrite(DOOR_PIN, LOW);

      delay(2000);

      int val = digitalRead(DOOR_PIN);
      Serial.print("---val: ");
      Serial.print(val);

   } else {

     // Serial.println("Sending credentials");

     int dollarIndex = str.indexOf('$');
     int semiColonIndex = str.indexOf(';');

     if(dollarIndex == -1 || semiColonIndex == -1) {
        // Serial.println("Wrong format! Input needs to be [reader number]$[bitstring];\nExample: 1$111001010111001011;");
     } else {
        sendWiegandBitString(str.substring(0, dollarIndex)[0],str.substring(dollarIndex + 1, semiColonIndex));
     }
   }
  }

  lastRelayStatus = relayStatus;
}