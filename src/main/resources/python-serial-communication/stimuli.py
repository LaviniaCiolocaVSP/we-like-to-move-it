import asyncio
import datetime
import random
import websockets
import serial_asyncio
import os
import re
import multiprocessing as mp
import sys
import time

import socket
import threading
from http.server import BaseHTTPRequestHandler,HTTPServer, test, SimpleHTTPRequestHandler
from urllib.parse import urlparse, urlsplit, parse_qs

fromArduinoQueue = asyncio.Queue()
toArduinoQueue = asyncio.Queue()

fromPanelQueue = asyncio.Queue()
toPanelQueue = asyncio.Queue()


async def producer_handler(websocket, path, queue):
    print('---------------------producer_handler: START---------------------')

    while True:
        print('    wait for message...')
        message = await queue.get()
        print('    message received, will be sent on websocket')
        await websocket.send(message)
        print(f'    message is: {message}')

    print('---------------------producer_handler: END-----------------------')

async def consumer_handler(websocket, path, queue):
    print('---------------------consumer_handler: START---------------------')

    while True:
        print('    wait for message...')
        message = await websocket.recv()
        print('    message received, will be sent toArduinoQueue')
        queue.put_nowait(message)

    print('---------------------consumer_handler: END-----------------------')

async def webSocketConnectionPanel(websocket, path):
    print('---------------------webSocketConnection: START---------------------')

    global toPanelQueue
    global fromPanelQueue

    consumer_task = asyncio.ensure_future(consumer_handler(websocket, path, toPanelQueue))
    producer_task = asyncio.ensure_future(producer_handler(websocket, path, fromPanelQueue))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task],
        return_when = asyncio.FIRST_COMPLETED,
    )
    for task in pending:
        task.cancel()
    print('---------------------webSocketConnection: END-----------------------')

async def webSocketConnectionArduino(websocket, path):
    print('---------------------webSocketConnection: START---------------------')

    global fromArduinoQueue
    global toArduinoQueue

    consumer_task = asyncio.ensure_future(consumer_handler(websocket, path, toArduinoQueue))
    producer_task = asyncio.ensure_future(producer_handler(websocket, path, fromArduinoQueue))
    done, pending = await asyncio.wait(
        [consumer_task, producer_task ],
        return_when = asyncio.FIRST_COMPLETED,
    )
    for task in pending:
        task.cancel()
    print('---------------------webSocketConnection: END-----------------------')



class SerialConnection(asyncio.Protocol):
    print('---------------------SerialConnection: START---------------------')
    def __init__(self, fromQ, toQ):
        super().__init__()
        self.receivedString = ""
        self.fromQueue = fromQ
        self.toQueue = toQ

    def connection_made(self, transport):
        self.transport = transport
        print('port opened', transport)
        self.sendTask = asyncio.ensure_future(self.send())

    def data_received(self, data):
        print('    -----------------SerialConnection: data_received: START-----------------')
        print(f'    data received: {data}')
        print(f'    data decoded: {data.decode("ascii")}')
        thestr = data.decode("ascii").replace("\r","") #\r is really messing the splits up so remove it
        if thestr.find("\n")>-1:
            splitstrings = thestr.split("\n")
            # first send the string we know we have (the first split)
            self.fromQueue.put_nowait(self.receivedString+splitstrings[0]);
            # and then we might have more strings in the split (not the first and not the last)
            for middlestring in splitstrings[1:-1]:
                self.fromQueue.put_nowait(middlestring);
                print(f'    Inside FOR put_nowait the middlestring: {middlestring}')
            # now we start over with the last string in the split which is the one without a line break
            self.receivedString = splitstrings[-1]
            print(f'    Inside IF the self.receivedString is: {self.receivedString}')
        else:
            print(f'    Inside ELSE - thestr is: {thestr}')
            self.receivedString += thestr
            #self.fromQueue.put_nowait(self.receivedString)
            print(f'    Inside ELSE the self.receivedString is: {self.receivedString}')

    def connection_lost(self, exc):
        print('-----------------SerialConnection: connection_lost-----------------')
        self.sendTask.cancel()

        print('Trying to reconnect with {} device...'.format(self.deviceIdentifier))
        self.reconnect_handler()

    def close_connection(self):
        print('-----------------SerialConnection: closing_connection-----------------')
        self.transport.close()
        self.sendTask.cancel()

    def reconnect_handler(self):
        deviceConnected = False

        while not deviceConnected:
            deviceConnected = openSocketCommunication(self.__class__)
            if not deviceConnected:
                print('Waiting 2 seconds to reconnect to {}...'.format(self.deviceIdentifier))
                time.sleep(2)


        print('Device connected')


    async def send(self):
        try:
            while True:
                message = await self.toQueue.get()
                message += '\n'
                print(f'From radu2 websocket to Arduino: {message}')
                bytelist = message.encode('ascii', 'ignore')
                self.transport.serial.write(bytelist);
        finally:
            print("Exiting serial send");

class PanelSerialConnection(SerialConnection):
    deviceIdentifier = 'Brivo'
    socketPort = 5678
    connectionHandler = webSocketConnectionPanel
    def __init__(self):
        global fromPanelQueue
        global toPanelQueue
        super().__init__(fromPanelQueue, toPanelQueue)

class ArduinoSerialConnection(SerialConnection):
    deviceIdentifier = 'Arduino'
    socketPort = 5677
    connectionHandler = webSocketConnectionArduino
    def __init__(self):
        global fromArduinoQueue
        global toArduinoQueue
        super().__init__(fromArduinoQueue, toArduinoQueue)


def main():
    eventLoop = asyncio.get_event_loop()

    openSocketCommunication(PanelSerialConnection)
    openSocketCommunication(ArduinoSerialConnection)

    try:
        eventLoop.run_forever()
    except KeyboardInterrupt:
        pass

    eventLoop.close()

def openSocketCommunication(serialConnectionClass):
    global global_event_loop
    if global_event_loop == None:
        global_event_loop = asyncio.get_event_loop()
    serialCommonPath = '/dev/'
    deviceFound = False
    eventLoopRestart = False

    deviceList = getConnectedDevices()
    for info, mountPoint in deviceList:
        if serialConnectionClass.deviceIdentifier in info:
            fullPath = serialCommonPath + mountPoint
            deviceFound = True
            break

    if not deviceFound:
        return deviceFound

    createWebSocketConnection(fullPath, serialConnectionClass.socketPort, serialConnectionClass, serialConnectionClass.connectionHandler)

    return deviceFound

def createWebSocketConnection(serialPath, socketPort, serialConnectionClass, webSocketConnectionClass):
    global panelServer
    global arduinoServer
    global global_event_loop
    global arduinoConn
    global panelConn

    if serialConnectionClass.deviceIdentifier == 'Brivo':
        try:
            print('look here')
            if panelServer.__class__.__name__ == 'Task':
                panelServer = panelServer.result()
                panelServer.close()
            else:
                panelServer.close()
            asyncio.ensure_future(panelServer.wait_closed())
            panelConn.close_connection()
        except:
            pass

    if serialConnectionClass.deviceIdentifier == 'Arduino':
        try:
            arduinoServer.close()
            asyncio.ensure_future(arduinoServer.wait_closed())
        except:
            pass

    coro = serial_asyncio.create_serial_connection(global_event_loop, serialConnectionClass, serialPath)
    start_server = websockets.serve(ws_handler = webSocketConnectionClass, host = '127.0.0.1', port = socketPort, loop = global_event_loop)

    if global_event_loop.is_running():
        asyncio.ensure_future(coro, loop=global_event_loop)
        serialConn = None
        server = asyncio.ensure_future(start_server, loop=global_event_loop)
    else:
        _, serialConn = global_event_loop.run_until_complete(coro)
        server = global_event_loop.run_until_complete(start_server)
    print("Socket available on port: {}".format(socketPort))

    if serialConnectionClass.deviceIdentifier == 'Brivo':
        if serialConn != None:
            panelConn = serialConn
        panelServer = server

    if serialConnectionClass.deviceIdentifier == 'Arduino':
        arduinoServer = server

class myHandler(BaseHTTPRequestHandler):

    def end_headers (self):
        self.send_header('Access-Control-Allow-Origin', '*')
        BaseHTTPRequestHandler.end_headers(self)

    #Handler for the GET requests
    def do_GET(self):
        if self.path == '/getCurrentPort':
            self.getCurrentPort()
        else:
            self.changePort()

        return

    def getCurrentPort(self):
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write(str(PanelSerialConnection.socketPort).encode())

    def changePort(self):
        responseMessage = 'Port changed successfully!'
        query = urlsplit(self.path).query
        params = parse_qs(query)
        new_port = int(params['port'][0])
        self.send_response(200)
        self.send_header('Content-type','text/html')
        self.end_headers()
        self.wfile.write(responseMessage.encode())

        PanelSerialConnection.socketPort = new_port
        status = openSocketCommunication(PanelSerialConnection)
        print("Port change without issues")




def getCommandContent(command):
    f = os.popen(command)
    message = f.read()
    return message

def getConnectedDevices():
    devicesListCommand = 'ls /dev/ | grep tty.usb*'
    devicesInfoCommand = 'system_profiler SPUSBDataType'
    locationIdString = 'Location ID'

    allDevicesList = []

    content = getCommandContent(devicesListCommand)
    for line in content.split('\n'):
        if len(line) > 0:
            deviceNumber = re.findall(r'\d+', line)[0][0:-1]
            deviceMountPoint = line
            deviceInfo = getCommandContent(devicesInfoCommand).split('\n')
            for idx in range(len(deviceInfo)):
                if locationIdString in deviceInfo[idx]:
                    deviceInfoId = deviceInfo[idx][25:29]
                    if deviceInfoId == deviceNumber:
                        manufacturerInfo = deviceInfo[idx - 1][24:]
                        allDevicesList.append((manufacturerInfo, deviceMountPoint))
    return allDevicesList

def createMiniHttpServer():
    server = HTTPServer(('', 1337), myHandler)
    print('Started httpserver on port {}'.format(1337))

    thread = threading.Thread(target = server.serve_forever)
    thread.daemon = True

    try:
        thread.start()
    except KeyboardInterrupt:
        server.shutdown()
        sys.exit(0)



if __name__ == '__main__':

    #Create a web server and define the handler to manage the
        #incoming request
    global global_event_loop
    global_event_loop = None
    createMiniHttpServer()
    deviceList = getConnectedDevices()
    print(deviceList)
    main()
