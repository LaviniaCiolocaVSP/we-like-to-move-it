from flask import Flask, render_template
from flask_socketio import SocketIO
import asyncio
import serial_asyncio
import serial

app = Flask(__name__)
app.config['SECRET_KEY'] = 'vnkdjnfjknfl1232#'
socketio = SocketIO(app)

fromArduinoQueue = asyncio.Queue()
serialPath = 'COM13'

arduino = serial.Serial("COM12", 9600)

@app.route('/')
def sessions():
    return render_template('index.html')

def messageReceived(methods=['GET', 'POST']):
    print('message was received!!!')

@socketio.on('foo')
def handle_my_custom_event(json, methods=['GET', 'POST']):
    print('received my event: ' + str(json))
    socketio.emit('my response', json, callback=messageReceived)

@socketio.on('arduino')
def handle_my_arduino_event(json, methods=['GET', 'POST']):
    print('arduino event: ' + str(json))
    value = arduino.readline()
    print(value.rstrip())
	
async def producer_handler():
	print('---------------------producer_handler: START---------------------')

	while True:
		print('    wait for message...')
		message = await fromArduinoQueue.get()
		print('    message received, will be sent on websocket')
		print(f'    message is: {message}')

	print('---------------------producer_handler: END-----------------------')

class SerialConnection(asyncio.Protocol):
	print('---------------------SerialConnection: START---------------------')
	def __init__(self):
		super().__init__()
		self.receivedString = ""

	def connection_made(self, transport):
		self.transport = transport
		print('port opened', transport)
		self.sendTask = asyncio.ensure_future(self.send())

	def data_received(self, data):
		global fromArduinoQueue
		print('    -----------------SerialConnection: data_received: START-----------------')
		print(f'    data received: {data}')
		print(f'    data decoded: {data.decode("ascii")}')
		thestr = data.decode("ascii").replace("\r","") #\r is really messing the splits up so remove it
		if thestr.find("\n")>-1:
			splitstrings = thestr.split("\n")
			# first send the string we know we have (the first split)
			fromArduinoQueue.put_nowait(self.receivedString+splitstrings[0]);
			# and then we might have more strings in the split (not the first and not the last)
			for middlestring in splitstrings[1:-1]:
				fromArduinoQueue.put_nowait(middlestring);
				print(f'    Inside FOR put_nowait the middlestring: {middlestring}')
			# now we start over with the last string in the split which is the one without a line break
			self.receivedString = splitstrings[-1]
			print(f'    Inside IF the self.receivedString is: {self.receivedString}')
		else:
			print(f'    Inside ELSE - thestr is: {thestr}')
			self.receivedString += thestr
			#fromArduinoQueue.put_nowait(self.receivedString)
			print(f'    Inside ELSE the self.receivedString is: {self.receivedString}')

	def connection_lost(self, exc):
		print('    -----------------SerialConnection: connection_lost-----------------')
		self.sendTask.cancel()

	async def send(self):
		try:
			while True:
				message = await toArduinoQueue.get()
				message += '\n'
				print(f'From radu2 websocket to Arduino: {message}')
				bytelist = message.encode('ascii', 'ignore')
				self.transport.serial.write(bytelist);
		finally:
			print("Exiting serial send");

if __name__ == '__main__':
    socketio.run(app, debug=True)

