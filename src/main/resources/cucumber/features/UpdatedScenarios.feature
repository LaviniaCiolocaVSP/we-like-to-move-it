Feature: Updated features

  Scenario: Send pulse message to panel
    Given the following panel serial number "STB-4M-YYQ6F"
    When a pulse request message is sent with parameters
      | administratorId | doorName      | boardNo | nodeNo |
      | 789             | 6000e-NJ DR-1 | 1       | 1      |
    Then I check that the Arduino confirms the pulse command

  Scenario: Update firmware
    Given the following panel serial number "STB-4M-YYQ6F"
    And the file "6-brivo.6.0.0.1-q03.tar.gz" is uploaded
    When I send an update firmware request
    And wait maximum "5" minutes for the panel to restart
    Then I check in the latest panel log that the firmware version is "6.0.0.1" and the build version is "abc"

  Scenario: Create 26Bit type credential
    Given the following panel serial number "STB-4M-YYQ6F"
    And the user identified uniquely by the name "Robert Mueller" exists
    When I send a request to create a 26 bit type credential the card number "6325" and facility code "23"
    Then the credential should be added to the panel

  Scenario: Create 26Bit type credential with dates
    Given the following panel serial number "STB-4M-YYQ6F"
    And the user identified uniquely by the name "Robert Mueller" exists
    When I send a request to create a 26 bit type credential the card number "6325" and facility code "23" that will be enabled on "" and it will expire on ""
    Then the credential should be added to the panel

  Scenario: Delete 26Bit type credential
    Given the following panel serial number "STB-4M-YYQ6F"
    And that a 26 bit type credential with the card number "6325" and facility code "23" exists
    When I send a request to delete the credential
    Then the credential should be deleted from the panel

  Scenario: Send unknown credential to simulator
    Given the following panel serial number "STB-4M-YYQ6F"
    And the simulator connected to the reader with number "1"
    And the card number "44" and facility code "44" does not exist on the panel
    And I present the credential "1" number of times
    And I wait "5" seconds between presenting the credential
    When I send a 26 bit type credential to simulator with the card number "44" and facility code "44"
    Then I verify that the panel reports unknown card and the door remains locked

  Scenario: Send known credential to simulator
    Given the following panel serial number "STB-4M-YYQ6F"
    And the simulator connected to the reader with number "1"
    And the card number "44" and facility code "44" exists on the panel
    And I present the credential "1" number of times
    And I wait "5" seconds between presenting the credential
    When I send a 26 bit type credential to simulator with the card number "44" and facility code "44"
    Then I verify that the panel confirms that the door is unlocked by reading the relay status from the Arduino

  Scenario: Present no permission to door credential to the panel
    Given the following panel serial number "STB-4M-YYQ6F"
    And the simulator connected to the reader with number "1"
    And the card number "44" and facility code "44" exists on the panel
    And I present the credential "1" number of times
    And I wait "5" seconds between presenting the credential
    When I send a 26 bit type credential to simulator with the card number "44" and facility code "44"
    Then I verify that the panel confirms that the credential has no permission and the door remains locked

  Scenario: Present no permission to door credential to the panel
    Given the following panel serial number "STB-4M-YYQ6F"
    And the simulator connected to the reader with number "1"
    And the card number "44" and facility code "44" exists on the panel
    And I present the credential "1" number of times
    And I wait "5" seconds between presenting the credential
    When I send a 26 bit type credential to simulator with the card number "44" and facility code "44"
    Then I verify that the panel confirms that the credential is outside date range and the door remains locked

  Scenario: Create reusable schedule without holidays
    Given the following panel serial number "STB-4M-YYQ6F"
    And group "VIPs" exist in the current account
    When I create a schedule with the name "Custom schedule", assigned to the site "Seattle"
    And having the enabling group "VIPs" and grace period "0"
    And the schedule blocks are "Monday 10:00AM-12:00AM,Tuesday 12:00AM-10:12PM"
    Then the schedule is created and added to the selected panel

  Scenario: Create holiday
    Given the following panel serial number "STB-4M-YYQ6F"
    When I create a holiday with the name "MyHoliday", assigned to the site "Seattle"
    And having the start date "06-01-2019 12:00 AM" and the end date "06-06-2019 08:00 AM"
    And it is assigned to the following schedules ""
    Then the holiday is created and added to the selected panel

  Scenario: Door forced open
    Given the following panel serial number "STB-4M-YYQ6F"
    When I simulate with Arduino a DOOR_FORCED_OPEN event on door
      | administratorId | doorName      | boardNo | nodeNo |
      | 789             | 6000e-NJ DR-1 | 1       | 1      |
    And the door has the Use-request-to-exit property enabled
    Then the event was received and exists in the events log

  Scenario: Door Ajar
    Given the following panel serial number "STB-4M-YYQ6F"
    When I simulate with Arduino the closing of the following door
      | administratorId | doorName      | boardNo | nodeNo |
      | 789             | 6000e-NJ DR-1 | 1       | 1      |
    And the Door ajar enabled property for that door is true
    And I pulse the door
    And I simulate with Arduino the opening of the door and wait the amount specified in the Door ajar threshold property
    Then the DOOR_AJAR_SET event was received and exists in the events log