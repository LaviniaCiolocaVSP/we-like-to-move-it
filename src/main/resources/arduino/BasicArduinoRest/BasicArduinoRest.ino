// Load Wi-Fi library
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>

// Replace with your network credentials
const char* ssid     = "Elysian_rds";
const char* password = "googlechrome";

// Set web server port number to 80
WiFiServer server(80);

// Variable to store the HTTP request
String header;

// Variable to store the parameter sent by the request
String queryParams;

int indexStart;

int analogReadCounter = 0;

int relayStatus = 0;
int lastRelayStatus = 0;

String RSP_IP = "http://192.168.1.107:8080";

// Variable to store the outgoing HTTP request
HTTPClient http;

#define READER_1_DATA0_PIN 16
#define READER_1_DATA1_PIN 5
#define READER_2_DATA0_PIN 4
#define READER_2_DATA1_PIN 0
#define READER_3_DATA0_PIN 2
#define READER_3_DATA1_PIN 14
#define READER_4_DATA0_PIN 12
#define READER_4_DATA1_PIN 13

#define PULSE_WIDTH_MICROSECONDS 20
#define INTER_PULSE_DELAY_MILLISECONDS 10



int sendWiegandBitString(char readerNumber, String bitString){
  int data0Pin = READER_1_DATA0_PIN;
  int data1Pin = READER_1_DATA1_PIN;
  switch(readerNumber) {
   case '2'  :
      data0Pin = READER_2_DATA0_PIN;
      data1Pin = READER_2_DATA1_PIN;
      break;
   case '3'  :
      data0Pin = READER_3_DATA0_PIN;
      data1Pin = READER_3_DATA1_PIN;
      break;
   case '4'  :
      data0Pin = READER_4_DATA0_PIN;
      data1Pin = READER_4_DATA1_PIN;
      break;
  }
  for(int i=0;i<bitString.length();i++){
    if(bitString[i]=='0'){
      digitalWrite(data0Pin, LOW);
      delayMicroseconds(PULSE_WIDTH_MICROSECONDS);
      digitalWrite(data0Pin, HIGH);
    } else {
      digitalWrite(data1Pin, LOW);
      delayMicroseconds(PULSE_WIDTH_MICROSECONDS);
      digitalWrite(data1Pin, HIGH);
    }
    delay(INTER_PULSE_DELAY_MILLISECONDS);
  }
  Serial.println("Sent credential to panel");
}


void setup() {
  Serial.begin(115200);

  pinMode(A0, INPUT_PULLUP);
  pinMode(READER_1_DATA0_PIN, OUTPUT);
  pinMode(READER_1_DATA1_PIN, OUTPUT);
  pinMode(READER_2_DATA0_PIN, OUTPUT);
  pinMode(READER_2_DATA1_PIN, OUTPUT);
  pinMode(READER_3_DATA0_PIN, OUTPUT);
  pinMode(READER_3_DATA1_PIN, OUTPUT);
  pinMode(READER_4_DATA0_PIN, OUTPUT);
  pinMode(READER_4_DATA1_PIN, OUTPUT);
  
  digitalWrite(READER_1_DATA0_PIN, HIGH);
  digitalWrite(READER_1_DATA1_PIN, HIGH);
  digitalWrite(READER_2_DATA0_PIN, HIGH);
  digitalWrite(READER_2_DATA1_PIN, HIGH);
  digitalWrite(READER_3_DATA0_PIN, HIGH);
  digitalWrite(READER_3_DATA1_PIN, HIGH);
  digitalWrite(READER_4_DATA0_PIN, HIGH);
  digitalWrite(READER_4_DATA1_PIN, HIGH);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("\nConnecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);

  // STATIC ADDRESS ASSIGNED 192.168.1.199

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  // Print local IP address and start web server
  Serial.println("");
  Serial.println("WiFi connected.");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
  server.begin();
}

void loop(){
  
  if (analogReadCounter == 10000) {
    analogReadCounter = 0;
    if (analogRead(A0) > 1000) {
      relayStatus = 1;
    } else {
      relayStatus = 0;
    }
    if (lastRelayStatus == 0 && relayStatus == 1) {
      Serial.println("RELAY OPEN");
       // IP ADDRESS SUBJECT TO CHANGE!!!!
      http.begin(RSP_IP + "/relayOpen" + "/1");
      int httpCode = http.POST("");
      String payload = http.getString();
      http.end();
    }
    if (lastRelayStatus == 1 && relayStatus == 0) {
      Serial.println("RELAY CLOSED");
      http.begin(RSP_IP + "/relayClosed" + "/1");
      int httpCode = http.POST("");
      String payload = http.getString();
      http.end();
    }
  } else {
    analogReadCounter++;
  }
  
  WiFiClient client = server.available();   // Listen for incoming clients
  if (client) {                             // If a new client connects,
    Serial.println("New Client.");          // print a message out in the serial port
    String currentLine = "";                // make a String to hold incoming data from the client
    while (client.connected()) {            // loop while the client's connected
      if (client.available()) {             // if there's bytes to read from the client,
        char c = client.read();             // read a byte, then
        Serial.write(c);                    // print it out the serial monitor
        header += c;
        if (c == '\n') {                    // if the byte is a newline character
          // if the current line is blank, you got two newline characters in a row.
          // that's the end of the client HTTP request, so send a response:
          if (currentLine.length() == 0) {
            // HTTP headers always start with a response code (e.g. HTTP/1.1 200 OK)
            // and a content-type so the client knows what's coming, then a blank line

            // turns the GPIOs on and off
            if (header.indexOf("POST /sendCredential") >= 0) {

              indexStart = header.indexOf("?string=");
              indexStart += 8; // to skip param name
              char ch = header[indexStart];
              while (ch != ' ') {
                queryParams += ch;
                indexStart++;
                ch = header[indexStart];
              }
              Serial.println("QUERYPARAMS: " + queryParams);

              int dollarIndex = queryParams.indexOf('$');
              int semiColonIndex = queryParams.indexOf(';');

              // Example of request: http://192.168.1.199:80/sendCredential?string=1$10011011100000000001101110;
              if(dollarIndex == -1 || semiColonIndex == -1){
                Serial.println("Wrong format! Input needs to be [reader number]$[bitstring];\nExample: 1$10011011100000000001101110;");
              } else {
                
                char readerNumber = queryParams.substring(0, dollarIndex)[0];
                String credential = queryParams.substring(dollarIndex + 1, semiColonIndex);

                sendWiegandBitString(readerNumber, credential);
                
              }
              
              client.println("HTTP/1.1 200 OK");
              client.println("Content-type:text/html");
              client.println("Connection: close");
              client.println();

              queryParams = "";

            } else {
              client.println("HTTP/1.1 404 NOT FOUND");
              client.println("Content-type:text/html");
              client.println("Connection: close");
              client.println();
            }
                       
            client.println("");
            // Break out of the while loop
            break;
          } else { // if you got a newline, then clear currentLine
            currentLine = "";
          }
        } else if (c != '\r') {  // if you got anything else but a carriage return character,
          currentLine += c;      // add it to the end of the currentLine
        }
      }
    }
    // Clear the header variable
    header = "";
    // Close the connection
    client.stop();
    Serial.println("Client disconnected.");
    Serial.println("");
  }

  lastRelayStatus = relayStatus;
}
