BEGIN TRANSACTION;

delete from brivo20.device_schedule;
delete from brivo20.object_permission;

delete from brivo20.schedule_exception_data;
delete from brivo20.schedule_data;
delete from brivo20.schedule_holiday_map;
delete from brivo20.notif_rule;
delete from brivo20.tes_resident;
delete from brivo20.schedule;
delete from brivo20.holiday;

delete from brivo20.credential_field_value;
delete from brivo20.credential_field;
delete from brivo20.access_credential;
delete from brivo20.mobile_invitation;
delete from brivo20.oauth_service_token;
delete from brivo20.login_session;
delete from brivo20.users;

delete from brivo20.board_data;
delete from brivo20.board_property;
delete from brivo20.board;
delete from brivo20.door_data;
delete from brivo20.device_property;
delete from brivo20.door_data_antipassback;
delete from brivo20.camera;
delete from brivo20.video_camera_device_mapping;
delete from brivo20.ipac_data;
delete from brivo20.device;
delete from brivo20.address;

delete from brivo20.camera_param;
delete from brivo20.camera_group_ovr_camera;
delete from brivo20.camera_motion_status;
delete from brivo20.camera_migration_history;
delete from brivo20.ovr_camera;
delete from brivo20.video_camera_parameter_value;
delete from brivo20.video_camera;
delete from brivo20.video_provider_property_value;
delete from brivo20.smartvue_nvr_registration;
delete from brivo20.video_provider;
delete from brivo20.security_group_member;
delete from brivo20.security_group_antipassback;
delete from brivo20.security_group;

delete from brivo20.swap_history;
delete from brivo20.brain_connection_status;
delete from brivo20.prog_device_puts;
delete from brivo20.brain_state;
delete from brivo20.prog_io_points;
delete from brivo20.rs485_settings;
delete from brivo20.brain;


delete from brivo20.report_configuration;
delete from brivo20.feature_override;
delete from brivo20.oauth_application;
delete from brivo20.application_account;
delete from brivo20.oauth_access_token;
delete from brivo20.oauth_client_details;
delete from brivo20.application_key;
delete from brivo20.application;
delete from brivo20.account_settings_value;
delete from brivo20.security_policy_param;
delete from brivo20.object_permission_template;
delete from brivo20.tes_directory;
delete from brivo20.event_sub_criteria;
delete from brivo20.event_subscription;
delete from brivo20.account_api_permission;
delete from brivo20.custom_field_value;
delete from brivo20.badge_text_item;
delete from brivo20.badge_barcode_item;
delete from brivo20.custom_field_definition;
delete from brivo20.account;

delete from brivo20.brain_lock;
delete from brivo20.video_provider_property_value;
delete from brivo20.video_provider;
delete from brivo20.object_property;
delete from brivo20.object;

COMMIT;
