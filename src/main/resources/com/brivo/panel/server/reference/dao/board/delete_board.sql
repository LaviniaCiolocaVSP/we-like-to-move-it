BEGIN TRANSACTION;

DELETE FROM brivo20.board_property
WHERE board_object_id = :boardObjectId;

DELETE FROM brivo20.prog_io_points
WHERE panel_oid = :brainObjectId
AND board_number = :boardNumber;

DELETE FROM brivo20.board
WHERE object_id = :boardObjectId;

DELETE FROM brivo20.object
WHERE object_id = :boardObjectId;

COMMIT;