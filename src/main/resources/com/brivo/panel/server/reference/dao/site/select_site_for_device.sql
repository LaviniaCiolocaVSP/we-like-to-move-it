SELECT DISTINCT g.security_group_id AS siteID
FROM security_group g,
     security_group_member m
WHERE g.security_group_id = m.security_group_id
  AND m.object_id = :memberOID
  AND g.security_group_type_id = 1
  AND g.deleted = 0