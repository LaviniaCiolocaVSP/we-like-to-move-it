UPDATE site_occupant
SET is_present    = 0,
    exited        = :exited,
    exit_door_oid = :exitDoorId
WHERE user_oid = :userId
  AND site_oid = :siteId
