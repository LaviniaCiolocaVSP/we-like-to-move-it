INSERT INTO site_occupant
(user_oid,
 site_oid,
 is_present,
 entered,
 entry_door_oid)
VALUES (:userId,
        :siteId,
        1,
        :entered,
        :entryDoorId)
