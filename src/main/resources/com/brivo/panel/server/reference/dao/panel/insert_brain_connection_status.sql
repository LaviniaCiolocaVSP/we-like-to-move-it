INSERT INTO brain_connection_status
(
	brain_id, 
	message_queue_name, 
	connect_time, 
	version, 
	ip_address
)
SELECT brain_id, 
	:messageQueueName, 
	CURRENT_TIMESTAMP, 
	1, 
	:ipAddress
FROM brain bn 
WHERE bn.object_id = :panelObjId
