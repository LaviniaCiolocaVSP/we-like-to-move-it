SELECT owner_object_id,
       last_owner_object_id,
       disabled,
       reference_id,
       enable_on,
       expires
FROM brain b
       JOIN access_credential ac ON ac.account_id = b.account_id
       JOIN access_credential_type act ON ac.access_credential_type_id = act.access_credential_type_id
WHERE b.object_id = :objectID
  AND ac.credential = :credentialValue
  AND (ac.num_bits = :credentialBits
  OR act.num_bits = :credentialBits)