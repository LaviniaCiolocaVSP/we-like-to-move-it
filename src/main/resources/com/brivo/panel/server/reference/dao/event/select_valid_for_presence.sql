SELECT SUM(COALESCE(d.is_ingress, 0)) AS totalIngress,
       SUM(COALESCE(d.is_egress, 0))  AS totalEgress
FROM device d,
     security_group g,
     security_group_member m
WHERE d.object_id = m.object_id
  AND g.security_group_id = m.security_group_id
  AND d.deleted = 0
  AND g.security_group_type_id != 0
  AND g.security_group_id IN (
  SELECT security_group_id
  FROM security_group_member
  WHERE object_id = :objectId
)