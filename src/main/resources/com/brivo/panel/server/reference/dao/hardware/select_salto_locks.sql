SELECT 
    d.object_id as deviceOID,
    dd.node_num as lockID,
    dd.passthrough as passthrough,
    ds.schedule_id as scheduleID,
    dd.enable_privacy_mode as enablePrivacyMode,
    dd.cache_cred_time_limit as lockCacheCredentialTimeLimit,
    lockdown.object_id as lockedDown
FROM 
    brain b
JOIN 
    device d ON b.brain_id = d.brain_id
JOIN 
    door_data dd ON d.object_id = dd.device_oid
LEFT OUTER JOIN 
    device_schedule ds ON d.device_id = ds.device_id
LEFT OUTER JOIN
    (
    SELECT 
        DISTINCT op.object_id
    FROM 
        object_permission op
    JOIN 
        security_group sg ON op.actor_object_id = sg.object_id
    JOIN 
        device d ON d.object_id = op.object_id
    JOIN
        brain b ON b.brain_id = d.brain_id
    WHERE 
        b.object_id = :panelObjectId
        AND d.device_type_id = 9
        AND sg.disabled != 1
        AND sg.lockdown = 1
    ) lockdown ON d.object_id = lockdown.object_id
WHERE 
    b.object_id = :panelObjectId
    AND d.device_type_id = 9
    AND d.deleted = 0