SELECT DISTINCT b.object_id
FROM device d
JOIN object_permission op ON op.object_id = d.object_id
JOIN security_group sg ON sg.object_id = op.actor_object_id
JOIN security_group_member sgm ON sgm.security_group_id = sg.security_group_id
JOIN users u ON u.object_id = sgm.object_id
JOIN brain b on d.brain_id = b.brain_id
WHERE u.object_id in(:userObjectIds)
AND u.user_type_id  = 1