SELECT access_credential_id
FROM mobile_invitation mi
WHERE user_object_id = :objectId
  AND redeemed = 1
ORDER BY redemption_date DESC