select
    b.object_id as board_object_id,
    bp1.value as mac_address,
    bp2.value as port_number
from
    board b
join 
    board_property bp1 on (b.object_id = bp1.board_object_id 
                           and bp1.name_key = 'MAC Address')
join 
    board_property bp2 on (b.object_id = bp2.board_object_id 
                           and bp2.name_key = 'Service Port')
where 
    b.panel_oid = :panelObjectId