SELECT brain_id,
	port,
	operation_mode,
	baud_rate,
	error_detection_method
FROM rs485_settings
WHERE brain_id = :brainId