SELECT owner_object_id,
       last_owner_object_id,
       disabled,
       reference_id,
       enable_on,
       expires
FROM brain b,
     access_credential ac
WHERE b.object_id = :objectID
  AND ac.account_id = b.account_id
  AND ac.credential = :credentialValue
  AND ac.access_credential_type_id < 100