SELECT b.brain_id,
  b.object_id,
  b.account_id,
  b.network_id,
  b.electronic_serial_number,
  b.firmware_version,
  b.time_zone,
  b.panel_changed,
  b.persons_changed,
  b.schedules_changed,
  b.brain_type_id,
  b.is_registered,
  b.log_period
FROM brain b
JOIN brain_type bt ON b.brain_type_id = bt.brain_type_id
WHERE bt.firmware_protocol = 6000
AND b.account_id = :accountId