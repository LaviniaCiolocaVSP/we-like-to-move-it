SELECT s.schedule_id AS scheduleId,
  s.account_id AS accountId,
  s.schedule_type_id AS scheduleTypeId,
  s.enabling_group_id AS enablingGroupId,
  s.enabling_grace_period AS enablingGracePeriod
FROM schedule s
WHERE s.account_id = :accountId
  