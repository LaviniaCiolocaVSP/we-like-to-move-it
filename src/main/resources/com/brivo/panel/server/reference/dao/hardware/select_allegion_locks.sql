select 
    d.object_id as deviceId,
    dd.node_num as lockId,
    ds.schedule_id as unlockScheduleId,
    dd.passthrough as passthroughDelay,
    dd.door_ajar as doorAjarDuration,
    dd.notify_on_ajar as reportDoorAjar,
    dd.enable_privacy_mode as enablePrivacyMode,
    lockdown.object_id as lockedDown
from
    device d 
join 
    door_data dd on d.object_id = dd.device_oid
join
    brain br on d.BRAIN_ID = br.brain_id 
join
    board b on b.panel_oid = br.object_id
left outer join
    device_schedule ds on d.device_id = ds.device_id
left outer join
    (
    select 
        distinct op.object_id
    from 
        object_permission op
    join 
        security_group sg on op.actor_object_id = sg.object_id
    join 
        device d on d.object_id = op.object_id
    join
        brain b on b.brain_id = d.brain_id
    where 
        b.object_id = :panelObjectId
        and d.device_type_id = 11
        and sg.disabled != 1
        and sg.lockdown = 1
    ) lockdown on d.object_id = lockdown.object_id
where 
    d.deleted = 0
and
    d.device_type_id = 11
and 
    b.object_id = :boardOid
and
    dd.board_num = :boardNum