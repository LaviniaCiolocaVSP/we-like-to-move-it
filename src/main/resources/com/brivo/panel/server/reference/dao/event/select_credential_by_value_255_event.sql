SELECT owner_object_id,
       last_owner_object_id,
       disabled,
       reference_id,
       enable_on,
       expires
FROM brain b,
     access_credential ac,
     access_credential_type act
WHERE b.object_id = :panelObjectId
  AND ac.account_id = b.account_id
  AND ac.access_credential_type_id = act.access_credential_type_id
  AND ac.credential = :credentialValue
  AND ac.created < act.new_card_engine_date