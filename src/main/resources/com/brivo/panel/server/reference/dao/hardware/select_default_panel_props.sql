SELECT d.property_type_id AS typeID,
	d.id AS name,
	COALESCE(p.value,d.value) AS value
FROM default_panel_prop d
LEFT OUTER JOIN (
	SELECT *
    FROM object_property
    WHERE object_id = :objectID) p ON d.id = p.id
WHERE d.brain_type_id = :brainTypeID
AND d.network_id = :networkID
