UPDATE 
    brain
SET 
   persons_changed = CURRENT_TIMESTAMP
WHERE
    object_id in (:brainIds) 