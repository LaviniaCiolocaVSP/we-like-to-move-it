SELECT br.object_id AS boardObjectID,
	br.board_number AS boardNum,
	bp.value AS gatewayId
FROM board br
JOIN board_property bp ON br.object_id = bp.board_object_id
JOIN brain brn ON br.panel_oid = brn.object_id
WHERE br.board_type = 8
AND brn.object_id = :panelOid