SELECT COUNT(*) AS count
FROM (
	SELECT DISTINCT ac.owner_object_id AS ownerOID,
	    ac.access_credential_type_id AS accessCredentialTypeID,
	    ac.enable_on AS enableOn,
	    ac.expires AS expires,
	    ac.credential AS credential,
	    ac.access_credential_id AS accessCredentialID,
	    COALESCE(act.num_bits, ac.num_bits) AS numBits,
	    (ac.created - act.new_card_engine_date) AS createdWith255Magic
	FROM access_credential ac
	JOIN access_credential_type act ON ac.access_credential_type_id = act.access_credential_type_id
	WHERE owner_object_id IN (
    	SELECT DISTINCT ac.owner_object_id
    	FROM access_credential ac
    	JOIN security_group_member sgm ON ac.owner_object_id = sgm.object_id
    	JOIN users u ON sgm.object_id = u.object_id
    	JOIN security_group sg ON sgm.security_group_id = sg.security_group_id
    	JOIN object_permission op ON sg.object_id = op.actor_object_id
    	JOIN device d ON op.object_id = d.object_id
    	JOIN brain b ON d.brain_id = b.brain_id
    	WHERE ac.disabled != 1
        AND ac.enable_on is not null
        AND u.deactivated IS NULL
        AND u.disabled = 0
        AND sg.disabled = 0
        AND d.deleted != 1
        AND b.electronic_serial_number = :panelSerialNumber
    )
) a