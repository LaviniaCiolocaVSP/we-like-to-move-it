UPDATE brain_connection_status bcs
SET message_queue_name = :messageQueueName, 
	connect_time = CURRENT_TIMESTAMP, 
	version = version + 1, 
	ip_address = :ipAddress
WHERE bcs.brain_id = (
	SELECT brain_id 
	FROM brain bn 
	WHERE bn.object_id = :panelObjId
)