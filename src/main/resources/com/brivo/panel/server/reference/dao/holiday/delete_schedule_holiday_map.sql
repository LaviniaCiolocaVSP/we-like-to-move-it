BEGIN TRANSACTION;


DELETE FROM brivo20.schedule_holiday_map
WHERE holiday_oid = :holidayId;

COMMIT;