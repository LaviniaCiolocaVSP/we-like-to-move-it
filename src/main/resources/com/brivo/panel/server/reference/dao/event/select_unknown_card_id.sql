SELECT reference_id
FROM brain b,
     access_credential ac,
     access_credential_type act
WHERE b.object_id = :panelObjectId
  AND ac.account_id = b.account_id
  AND ac.access_credential_type_id = act.access_credential_type_id
  AND ac.access_credential_id = :credentialId
 