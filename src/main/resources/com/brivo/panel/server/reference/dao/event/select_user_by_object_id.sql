select first_name,
       last_name,
       disabled,
       deactivated
from users u
where u.object_id = :userObjectId
