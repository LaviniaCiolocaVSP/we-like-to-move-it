SELECT start_time AS startTime,
    stop_time AS stopTime
FROM schedule_data
WHERE schedule_id = :scheduleId
ORDER BY start_time ASC