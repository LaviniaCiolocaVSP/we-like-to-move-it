BEGIN TRANSACTION;

DELETE FROM brivo20.prog_device_puts
WHERE device_oid = :deviceObjectId;

DELETE FROM brivo20.device_schedule
WHERE device_id = :deviceId;

--DELETE FROM brivo20.prog_device_data
--WHERE device_oid = :deviceObjectId;

DELETE FROM brivo20.object_permission
WHERE object_id = :deviceObjectId;

UPDATE brivo20.device
SET deleted = 1
WHERE device_id = :deviceId;

COMMIT;