SELECT schedule_id AS scheduleID,
    security_action_id AS securityActionID,
    actor_object_id AS actorOID,
    0 AS immunity
FROM object_permission
WHERE object_id = :deviceOID
AND security_action_id = 2006
AND object_id = (
	CASE WHEN 0 = (
	    SELECT count(*)
		    FROM object_permission
		    WHERE object_id = :deviceOID
		  	AND security_action_id = 2004
		   	AND actor_object_id IN (
		        SELECT object_id
		        FROM security_group
		        WHERE lockdown = 1
		      	AND disabled != 1
		       	AND security_group_type_id = 2
		   	)
		) THEN :deviceOID ELSE 0 END)
UNION
/* get all perms where a group is the actor, group may be deleted */
SELECT op.schedule_id AS scheduleID,
    op.security_action_id AS securityActionID,
    op.actor_object_id AS actorOID,
    COALESCE(sga.immunity, 0) AS immunity
FROM object_permission op
JOIN security_group sg ON op.actor_object_id = sg.object_id
JOIN security_group_antipassback sga ON sg.security_group_id = sga.security_group_id
WHERE op.object_id = :deviceOID
AND op.security_action_id in (2004, 5231)
AND sg.disabled != 1
AND sg.lockdown != 1
UNION
/* get all perms where root group is actor, which means everything, so we include here */
SELECT schedule_id AS scheduleID,
    security_action_id AS securityActionID,
    actor_object_id AS actorOID,
    0 AS immunity
FROM object_permission
WHERE object_id = (
    SELECT object_id
    FROM security_group
    WHERE security_group_type_id = 0
  	AND lockdown != 1
 	AND account_id = (
        SELECT account_id
        FROM brain
        WHERE electronic_serial_number = :panelSerialNumber
   	)
)
AND security_action_id = 2005