UPDATE brain_connection_status
SET message_queue_name = null,
	connect_time = null
WHERE message_queue_name = :messageQueueName