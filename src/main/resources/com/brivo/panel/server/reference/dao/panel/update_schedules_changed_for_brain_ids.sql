UPDATE 
    brain
SET 
   schedules_changed = CURRENT_TIMESTAMP
WHERE
    object_id in (:brainIds) 