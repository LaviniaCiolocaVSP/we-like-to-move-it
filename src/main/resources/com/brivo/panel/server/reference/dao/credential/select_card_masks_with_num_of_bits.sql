SELECT cf.format AS card_mask,
	act.num_bits AS mask_length
FROM credential_field cf
JOIN access_credential_type act ON cf.access_credential_type_id = act.access_credential_type_id
WHERE cf.credential_field_type_id = 3
AND act.num_bits = :numOfBits 
    