SELECT d.account_id                       AS account_id,
       u.object_id                        AS object_id,
       u.first_name || ' ' || u.last_name AS full_name,
       d.object_id                        AS device_oid,
       o.object_type_id                   AS object_type_id,
       d.name                             AS device_name,
       sg.object_id                       AS site_oid,
       sg.name                            AS security_group_name,
       d.device_type_id                   AS device_type_id,
       ''                                 AS engage_msg,
       ''                                 AS disengage_msg
FROM users u,
     device d,
     object o,
     security_group sg,
     security_group_member sgm
WHERE o.object_id = :deviceObjectId
  AND u.object_id = :userObjectId
  AND d.object_id = o.object_id
  AND sgm.object_id = o.object_id
  AND sg.security_group_id = sgm.security_group_id
  AND sg.disabled = 0
  AND sg.security_group_type_id != 0
  AND sg.security_group_id = :securityGroupId