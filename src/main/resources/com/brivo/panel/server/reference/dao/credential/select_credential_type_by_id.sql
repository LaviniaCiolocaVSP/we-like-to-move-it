SELECT access_credential_type_id,
	name,
	description,
	num_bits,
	format,
	new_card_engine_date
FROM access_credential_type
WHERE access_credential_type_id = :credentialTypeId