SELECT
  ipt.silkscreen AS name,
  ipt.point_address AS ioPointAddress
FROM IO_POINT_TEMPLATE ipt
  JOIN board ON ipt.BOARD_TYPE = board.BOARD_TYPE
  JOIN DOOR_DATA dd ON board.BOARD_NUMBER = dd.BOARD_NUM
  JOIN device d ON d.OBJECT_ID = dd.DEVICE_OID
  JOIN brain ON brain.BRAIN_ID = d.BRAIN_ID
WHERE board.PANEL_OID = brain.OBJECT_ID
      AND d.OBJECT_ID = :deviceOid