SELECT credential_field_id,
  credential_field_type_id,
  ordering,
  name AS field_name,
  xml_name,
  for_reference,
  display,
  format AS format_value,
  bcd_length
FROM credential_field
WHERE access_credential_type_id = :access_credential_type_id
ORDER BY ordering