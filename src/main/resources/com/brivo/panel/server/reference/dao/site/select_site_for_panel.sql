SELECT DISTINCT g.security_group_id AS siteID
FROM security_group g,
     security_group_member m,
     device d,
     brain b
WHERE g.security_group_id = m.security_group_id
  AND m.object_id = d.object_id
  AND b.brain_id = d.brain_id
  AND b.object_id = :panelOID
  AND g.security_group_type_id = 1
  AND g.deleted = 0
  AND d.deleted = 0
