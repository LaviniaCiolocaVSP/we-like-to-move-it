BEGIN TRANSACTION;

DELETE FROM brivo20.elevator_floor_map
WHERE panel_oid = :brainObjectId;

DELETE FROM brivo20.rs485_settings
WHERE brain_id = :brainId;

DELETE FROM brivo20.brain_state
WHERE object_id = :brainObjectId;

DELETE FROM brivo20.brain
WHERE brain_id = :brainId;

DELETE FROM brivo20.object_property
WHERE object_id = :brainObjectId;

DELETE FROM brivo20.object
WHERE object_id = :brainObjectId;

COMMIT;