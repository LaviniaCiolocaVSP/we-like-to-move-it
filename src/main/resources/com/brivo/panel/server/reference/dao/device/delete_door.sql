BEGIN TRANSACTION;

--DELETE FROM brivo20.door_data
--WHERE device_oid = :deviceObjectId;

UPDATE brivo20.prog_io_points
SET inuse = 0
WHERE panel_oid = :brainObjectId
AND board_number = :boardNumber
AND point_address in (:pointAddresses);

--DELETE FROM brivo20.door_data_antipassback
--WHERE device_oid = :deviceObjectId;

--DELETE FROM brivo20.device_property
--WHERE device_id = :deviceId;

DELETE FROM brivo20.device_schedule
WHERE device_id = :deviceId;

DELETE FROM brivo20.object_permission
WHERE object_id = :deviceObjectId;

UPDATE brivo20.device
SET deleted = 1
WHERE device_id = :deviceId;

--DELETE FROM brivo20.object
--WHERE object_id = :deviceObjectId;

COMMIT;