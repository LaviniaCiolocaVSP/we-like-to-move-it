SELECT 
    dd.device_oid as deviceOID,
    dd.board_num as boardNumber,
    dd.node_num as nodeNumber,
    dd.use_alarm_shunt as useAlarmShunt,
    dd.schedule_enabled as scheduleEnabled,
    dd.max_invalid as maxInvalid,
    dd.invalid_shutout as invalidShutout,
    dd.passthrough as passthrough,
    dd.alarm_delay as alarmDelay,
    dd.door_ajar as doorAjar,
    dd.invalid_cred_win as invalidCredWin,
    dd.notify_on_ajar as notifyOnAjar,
    dd.notify_on_forced as notifyOnForced,
    dd.rex_fires_door as rexFiresDoor,
    maxRexExtension.value as maxRexExtension,
    d.two_factor_schedule_id as twoFactorScheduleID,
    d.two_factor_interval as twoFactorInterval,
    d.card_required_schedule_id as cardRequiredScheduleID,
    coalesce(dda.zone,-1) as ddazone,
    dda.alt_reader_panel_oid as altOID,
    dda.alt_reader_board_num as altBoard,
    dda.alt_reader_point_address as altAddress,
    coalesce(dda.alt_zone,-1) as altZone,
    b.antipassback_reset_interval as resetInterval,
    dd.use_lock_on_open as useLockOnOpen,
    dd.lock_on_open_delay as lockOnOpenDelay,
    dd.debounce_period as debouncePeriod,
    ds.schedule_id as unlockScheduleId,
    lockdown.object_id as lockedDown,
    --added by Laura on 17.07.2019 - to allow testing of RequestDeviceStatus PCS message
    d.enable_live_control as enableLiveControl
FROM 
    brain b
JOIN 
    device d ON b.brain_id = d.brain_id
JOIN 
    door_data dd ON d.object_id = dd.device_oid
LEFT OUTER JOIN 
    door_data_antipassback dda ON dd.device_oid = dda.device_oid
LEFT OUTER JOIN
    device_schedule ds ON d.device_id = ds.device_id
LEFT OUTER JOIN
    device_property maxRexExtension ON (d.device_id = maxRexExtension.device_id
                                        AND maxRexExtension.id = 'MAX_REX_EXTENSION')
LEFT OUTER JOIN
    (
    SELECT 
        DISTINCT op.object_id
    FROM 
        object_permission op
    JOIN 
        security_group sg ON op.actor_object_id = sg.object_id
    JOIN 
        device d ON d.object_id = op.object_id
    JOIN
        brain b ON b.brain_id = d.brain_id
    WHERE 
        b.object_id = :panelObjectId
        AND d.device_type_id = 1
        AND sg.disabled != 1
        AND sg.lockdown = 1
    ) lockdown ON d.object_id = lockdown.object_id
WHERE 
    b.object_id = :panelObjectId
    AND d.device_type_id = 1
    AND d.deleted != 1