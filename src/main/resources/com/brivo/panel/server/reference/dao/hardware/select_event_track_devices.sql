SELECT d.object_id AS deviceOID,
    pdd.door_id AS doorID,
    pdd.event_type AS eventTypeID,
    pdd.notify_flag AS notifyFlag,
    ds.schedule_id AS scheduleID
FROM brain b
JOIN device d ON b.brain_id = d.brain_id
JOIN prog_device_data pdd ON d.object_id = pdd.device_oid
JOIN device_schedule ds ON d.device_id = ds.device_id
WHERE b.electronic_serial_number = :panelSerialNumber
AND d.deleted != 1
AND d.device_type_id = 6