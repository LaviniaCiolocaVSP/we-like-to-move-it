UPDATE 
    brain
SET 
   panel_changed = CURRENT_TIMESTAMP,
   persons_changed = CURRENT_TIMESTAMP,
   schedules_changed = CURRENT_TIMESTAMP
WHERE
    object_id in (:panelObjectIds)
