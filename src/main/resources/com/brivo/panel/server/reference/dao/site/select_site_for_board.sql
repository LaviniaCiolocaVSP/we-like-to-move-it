SELECT DISTINCT sg.security_group_id AS siteID
FROM security_group sg
       JOIN security_group_member sgm ON sgm.security_group_id = sg.security_group_id
       JOIN device d ON d.object_id = sgm.object_id
       JOIN brain bn ON d.brain_id = bn.brain_id
       JOIN board bd ON bd.panel_oid = bn.object_id
       JOIN door_data dd ON (dd.device_oid = d.object_id AND dd.board_num = bd.board_number)
WHERE bd.object_id = :boardOID
  AND sg.deleted = 0
  AND sg.security_group_type_id = 1
  AND d.deleted = 0