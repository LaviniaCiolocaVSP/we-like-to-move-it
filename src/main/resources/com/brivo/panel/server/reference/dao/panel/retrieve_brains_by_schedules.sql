SELECT DISTINCT
    b.object_id
FROM 
    device d
JOIN
    object_permission op on d.object_id = op.object_id
JOIN
    brain b on d.brain_id = b.brain_id
WHERE
    op.schedule_id in (:scheduleIds)
AND
     op.security_action_id = 2004