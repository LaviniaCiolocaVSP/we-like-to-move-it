BEGIN TRANSACTION;

DELETE FROM brivo20.schedule_data
WHERE schedule_id = :scheduleId;

DELETE FROM brivo20.schedule_holiday_map
WHERE schedule_oid = :scheduleId;

COMMIT;