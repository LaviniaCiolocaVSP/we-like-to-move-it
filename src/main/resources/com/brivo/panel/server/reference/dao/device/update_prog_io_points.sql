BEGIN TRANSACTION;

UPDATE brivo20.prog_io_points
SET inuse = :inuse
WHERE panel_oid = :panelOid
AND board_number = :boardNumber
AND point_address = :pointAddress;

COMMIT;