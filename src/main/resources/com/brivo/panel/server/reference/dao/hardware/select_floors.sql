select 
    efm.floor_oid as deviceOID,
    efm.elevator_oid as elevatorOID,
    efm.board_number as boardNumber,
    efm.point_address as pointAddress,
    ds.schedule_id as unlockScheduleId,
    lockdown.object_id as lockedDown
from 
    device d
join
    elevator_floor_map efm on d.object_id = efm.floor_oid
left outer join
    device_schedule ds on d.device_id = ds.device_id
left outer join
    (
    select 
        distinct op.object_id
    from 
        object_permission op
    join 
        security_group sg on op.actor_object_id = sg.object_id
    join 
        device d on d.object_id = op.object_id
    join
        brain b on b.brain_id = d.brain_id
    where 
        b.object_id = :panelObjectId
        and d.device_type_id = 7
        and sg.disabled != 1
        and sg.lockdown = 1
    ) lockdown on d.object_id = lockdown.object_id    
WHERE 
    efm.panel_oid = :panelObjectId
    and d.deleted=0