SELECT DISTINCT
    b.object_id
FROM 
    device d
JOIN 
    device_schedule ds on ds.device_id = d.device_id
JOIN
    brain b on d.brain_id = b.brain_id
WHERE 
    schedule_id in (:scheduleIds)