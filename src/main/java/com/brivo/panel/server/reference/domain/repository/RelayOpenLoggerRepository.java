package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.RelaysOpenLogger;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface RelayOpenLoggerRepository extends CrudRepository<RelaysOpenLogger, Long> {

    @Query(
            value = "SELECT coalesce(max(rol.relays_open_logger_id),0) FROM brivo20.relays_open_logger rol",
            nativeQuery = true
    )
    Optional<Long> getMaxRelaysOpenLoggerId();

    @Query(
            value = "SELECT * FROM brivo20.relays_open_logger " +
                    "WHERE relays_open_logger_id > :start_id AND relays_open_logger_id <= :end_id",
            nativeQuery = true
    )
    Optional<List<RelaysOpenLogger>> getAllRecordsBetween(@Param("start_id") long startId,
                                                          @Param("end_id") long end_id);
}
