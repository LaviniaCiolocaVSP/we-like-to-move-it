package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "proximity_property", schema = "brivo20", catalog = "onair")
public class ProximityProperty {
    private long proximityId;
    private long valueTypeId;
    private String value;
    private Timestamp updated;
    private Proximity proximityByProximityId;
    private ValueType valueTypeByValueTypeId;

    @Id
    @Basic
    @Column(name = "proximity_id", nullable = false)
    public long getProximityId() {
        return proximityId;
    }

    public void setProximityId(long proximityId) {
        this.proximityId = proximityId;
    }

    @Basic
    @Column(name = "value_type_id", nullable = false)
    public long getValueTypeId() {
        return valueTypeId;
    }

    public void setValueTypeId(long valueTypeId) {
        this.valueTypeId = valueTypeId;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 256)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProximityProperty that = (ProximityProperty) o;

        if (proximityId != that.proximityId) {
            return false;
        }
        if (valueTypeId != that.valueTypeId) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (proximityId ^ (proximityId >>> 32));
        result = 31 * result + (int) (valueTypeId ^ (valueTypeId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "proximity_id", referencedColumnName = "proximity_id", nullable = false, insertable = false, updatable = false)
    public Proximity getProximityByProximityId() {
        return proximityByProximityId;
    }

    public void setProximityByProximityId(Proximity proximityByProximityId) {
        this.proximityByProximityId = proximityByProximityId;
    }

    @ManyToOne
    @JoinColumn(name = "value_type_id", referencedColumnName = "value_type_id", nullable = false, insertable = false, updatable = false)
    public ValueType getValueTypeByValueTypeId() {
        return valueTypeByValueTypeId;
    }

    public void setValueTypeByValueTypeId(ValueType valueTypeByValueTypeId) {
        this.valueTypeByValueTypeId = valueTypeByValueTypeId;
    }
}
