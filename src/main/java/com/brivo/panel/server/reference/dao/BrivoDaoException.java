package com.brivo.panel.server.reference.dao;

public class BrivoDaoException extends RuntimeException {
    private static final long serialVersionUID = 6582587721685163944L;
    protected Exception nested = null;

    public BrivoDaoException() {
        super();
    }

    public BrivoDaoException(String exceptionInformation) {
        super(exceptionInformation);
    }

    public BrivoDaoException(String msg, Exception e) {
        super(msg);
        nested = e;
        super.initCause(e);
    }

    public Exception getNestedException() {
        return nested;
    }

    public String toString() {
        return super.toString() + " Nested exception is " + nested;
    }
}
