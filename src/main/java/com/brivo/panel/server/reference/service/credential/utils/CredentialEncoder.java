package com.brivo.panel.server.reference.service.credential.utils;

import com.brivo.panel.server.reference.domain.entities.credential.Credential;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFormat;

public interface CredentialEncoder
{
    /**
     * Encodes a {@link Credential} into a {@code String} based on the format
     * specified by {@code toEncode}.
     * 
     * @param toEncode
     *            The {@link Credential} to be encoded. This credential must be
     *            fully populated, as information on how to encode the
     *            credential is taken from the format of the credential to be
     *            encoded. {@link Credential getFormat()}.
     *            <p>
     *            This method assumes that the credential's format has already
     *            been properly validated
     *            </p>
     * 
     * 
     * 
     */
    public void encode(Credential toEncode);
    /**
     * Attempts to decode the {@code encodedCredential} string into a
     * {@link Credential} using the given {@code credentialFormat}
     * 
     * @param encodedCredential
     *            a hex encoded string representing the bytes from a reader
     * @param credentialFormat
     *            a fully populated {@link CredentialFormat}.
     * @return a {@link Credential} object with
     *         {@link Credential getFieldValues()} set according to
     *         that format if successful. If the credential could not be decoded
     *         into this format, {@code null}.
     */
    public Credential decode(String encodedCredential, CredentialFormat credentialFormat);

}
