package com.brivo.panel.server.reference.dao.user;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum UserQuery implements FileQuery {
    DELETE_USER;

    private String query;

    UserQuery() {
        this.query = QueryUtils.getQueryText(UserQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}