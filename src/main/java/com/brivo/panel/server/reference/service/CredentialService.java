package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.credential.CredentialDao;
import com.brivo.panel.server.reference.model.credential.CardMask;
import com.brivo.panel.server.reference.model.credential.Credential;
import com.brivo.panel.server.reference.model.credential.CredentialData;
import com.brivo.panel.server.reference.model.credential.CredentialUsage;
import com.brivo.panel.server.reference.model.hardware.Panel;
import org.bouncycastle.util.encoders.Hex;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Service class for handling credential data.
 */
@Service
public class CredentialService {
    private static final String MESSAGE_DIGEST_ALGORITHM_SHA = "SHA";

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialService.class);
    @Autowired
    CredentialDao credentialDao;  // Package level protection for UT access
    @Value("${credential.page.size}")
    private Integer credentialPageSize;

    /**
     * Returns the maximum number of credentials allowed as defined
     * by configuration property maximum.number.credentials.
     * <p>
     * Note : If more credentials exist than were requested by the limit, the used
     * credentials from cassandra are given priority.
     *
     * @param panel The panel.
     * @return CredentialData.
     */
    @BrivoTransactional
    public CredentialData getAllCredentials(Panel panel, Long offset, int pageSize) {
        LOGGER.trace(">>> GetAllCredentials");

        CredentialData data = new CredentialData();

        data.setLastUpdate(panel.getPersonsChanged());

        List<Credential> regularCredentialList = this.credentialDao.getAllCredentials(panel, offset, pageSize);

        // Calculate and set count value.
        Long credentialCount = this.credentialDao.getCredentialCount(panel);
        data.setCount(credentialCount);

        List<Credential> prioritizedCredentialList;

        // If more credentials exist than what were requested by the limit,
        // we must fetch the used credentials from Cassandra and give them priority.
        if (credentialCount > regularCredentialList.size()) {
            List<CredentialUsage> credentialUsageList = getCredentialsUsed(panel.getAccountId());
            List<Credential> cassandraCredentialsForPanel = getCredentials(panel, credentialUsageList);
            prioritizedCredentialList = prioritizeCredentialData(regularCredentialList, cassandraCredentialsForPanel);
        } else {
            prioritizedCredentialList = regularCredentialList;
            // metricRegistry.meter("paneldata.credential.get_credentials_all_postgres").mark();
        }

        adjustCredentials(prioritizedCredentialList);

        data.setCredentials(prioritizedCredentialList);

        data.setOffset(0L);
        data.setPageSize(prioritizedCredentialList.size());

        LOGGER.trace("<<< GetAllCredentials");

        return data;
    }

    /**
     * Return credential data in a paged manner.
     *
     * @param panel    The panel.
     * @param offset   Offset in credentials to start after.
     * @param pageSize Number of credentials to return.
     * @return CredentialData.
     */
    @BrivoTransactional
    public CredentialData getCredentialData(Panel panel, Long offset, Integer pageSize) {
        LOGGER.trace(">>> GetCredentialData");

        CredentialData data = new CredentialData();

        data.setLastUpdate(panel.getPersonsChanged());

        List<Credential> credentialList = getPageCredentials(panel, offset, pageSize);

        adjustCredentials(credentialList);

        data.setCredentials(credentialList);

        data.setOffset(offset);
        data.setPageSize(pageSize);

        // Calculate and set count value.
        Long credentialCount = this.credentialDao.getCredentialCount(panel);
        data.setCount(credentialCount);

        LOGGER.trace("<<< GetCredentialData");

        return data;
    }

    /**
     * Gets the Credential objects for the Account-specific credentials from Cassandra.
     *
     * @param accountId
     * @return List of corresponding Credential objects.
     */
    private List<CredentialUsage> getCredentialsUsed(long accountId) {
        LOGGER.trace(">>> getCredentialsUsed");

        List<CredentialUsage> credentialUsageList = new ArrayList<>();
        // List<CredentialUsage> credentialUsageList = this.cassandraDao.getCredentialsUsed(accountId);

        LOGGER.trace("<<< getCredentialsUsed");

        return credentialUsageList;
    }

    /**
     * Gets the Credential objects for the corresponding CredentialUsage objects obtained from Cassandra specific to
     * the given panel.
     * <p>
     * Note : The returned list may be much smaller than the input list because some of the credentials and devices
     * may have been removed in OnAir.  Also, the CredentialUsage objects obtained from Cassandra are not panel specific
     * (they are Account specific), so filtering is done to return just the credentials from Cassandra for the specified panel.
     *
     * @param panel The associated panel.
     * @return List of corresponding Credential objects.
     * <p>
     * Package level protection for access by UTs
     */
    List<Credential> getCredentials(Panel panel, List<CredentialUsage> credentialUsageList) {
        return this.credentialDao.getCredentials(panel, credentialUsageList);
    }

    /**
     * Returns a list of credentials the same size as the regular list, but gives priority to the Cassandra credentials
     * over any regular credentials.  Which regular credentials get replaced is completely arbitrary.
     *
     * @param regularCredentialList        This is also limited by the configuration property maximum.number.credentials
     * @param cassandraCredentialsForPanel This is also limited by the configuration property maximum.number.credentials
     *                                     and we also know that these credentials also exist in the database.
     * @return List<Credential> : Prioritized list of credentials.  For performance, this may be a modified version of the
     * given cassandraCredentialsForPanel list with additional credentials from the regular list
     * added to it.
     */
    private List<Credential> prioritizeCredentialData(List<Credential> regularCredentialList,
                                                      List<Credential> cassandraCredentialsForPanel) {
        LOGGER.trace(">>> prioritizeCredentialData");

        if (LOGGER.isInfoEnabled() ) {
            LOGGER.info("\nregularCredentialList.size() : {}",
                    regularCredentialList.size());
            if (cassandraCredentialsForPanel != null) {
                LOGGER.info("\ncassandraCredentialsForPanel.size() : {}",
                        cassandraCredentialsForPanel.size());
            } else {
                LOGGER.info("\ncassandraCredentialsForPanel : {}",
                        cassandraCredentialsForPanel);
            }
        }

        List<Credential> prioritizedList;

        if (cassandraCredentialsForPanel == null || cassandraCredentialsForPanel.size() == 0) {
            prioritizedList = regularCredentialList;
            // metricRegistry.meter("paneldata.credential.get_credentials_all_postgres").mark();

            // If they are the same size, we know we can just return the cassandra credentials that exist in the DB.
        } else if (cassandraCredentialsForPanel.size() == regularCredentialList.size()) {
            prioritizedList = cassandraCredentialsForPanel;
            // metricRegistry.meter("paneldata.credential.get_credentials_all_cassandra").mark();


            // If we reach here, we know that we have more regular credentials than cassandra credentials,
            // so our returned prioritized list will be a combination of both kinds of credentials,
            // but all of the cassandra credentials will be included.
        } else {

            prioritizedList = cassandraCredentialsForPanel;

            int numberOfCredentialsToReturn = regularCredentialList.size();
            Set<Long> prioritzedCredentialsSet = new HashSet<>();

            for (int i = 0; i < cassandraCredentialsForPanel.size(); ++i) {
                Credential credential = cassandraCredentialsForPanel.get(i);
                prioritzedCredentialsSet.add(credential.getCredentialId());
            }

            int numOfCassandraCredentialsForPanel = prioritzedCredentialsSet.size();
            int numRegularCredentialsToInclude = numberOfCredentialsToReturn - numOfCassandraCredentialsForPanel;

            // Note : The check for i<numberOfCredentialsToReturn is not needed, but is added here to prevent an infinite loop
            // from ever happening.
            for (int i = 0; i < numberOfCredentialsToReturn && numRegularCredentialsToInclude > 0; ++i) {
                Credential regularCredential = regularCredentialList.get(i);

                // If the set does not already contain this credential, add it to the list of credentials to be returned
                if (prioritzedCredentialsSet.add(regularCredential.getCredentialId())) {
                    prioritizedList.add(regularCredential);
                    numRegularCredentialsToInclude--;
                }
            }

            // metricRegistry.meter("paneldata.credential.get_credentials_prioritized_mix").mark();
        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("\nprioritizedList.size() : {}", prioritizedList.size());
        }

        LOGGER.trace("<<< prioritizeCredentialData");
        return prioritizedList;
    }

    /**
     * Loop through the allCredentials and make any needed business adjustments. These
     * include masking off facility codes for old credentials, and hashing credentials values.
     */
    private void adjustCredentials(List<Credential> allCredentials) {
        LOGGER.trace(">>> AdjustCredentials");

        for (Credential cred : allCredentials) {
            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("CredentialId [{}], userId [{}].", cred.getCredentialId(), cred.getUserId());
            }

            if (cred.getNumberBits() == 26 && cred.getCreatedWith255Magic()) {
                cred.setValue(checkFor255FacilityCode(cred.getValue()));
            }

            cred.setValue(hashCredential(cred.getValue()));
        }

        LOGGER.trace("<<< AdjustCredentials");
    }

    /**
     * Mask off Facility Code. Copied from old Panel Server.
     * <p>
     * As Rudy explains this we used to allow cards to be put in where the facility
     * code should be ignored. At one point we stopped allowing this. We record this
     * date in the access_credential_type new_card_engine_date. If the card is old
     * enough and of a specific type then we mask off the facility code.
     *
     * @param value The credential value.
     * @return The value with the facility code masked off.
     */
    private String checkFor255FacilityCode(String value) {
        String credentialValue = value;
        BigInteger TwoFiftyFive = new BigInteger("1FE0000", 16);
        BigInteger credValue = new BigInteger(value, 16);
        BigInteger maskedValue = credValue.and(TwoFiftyFive);

        if (maskedValue.equals(TwoFiftyFive)) {
            credentialValue = credValue.and(new BigInteger("1FFFE", 16)).toString(16);
        }

        return credentialValue;
    }

    /**
     * Hash the input string using a SHA hash.
     */
    String hashCredential(String value) {
        try {
            MessageDigest digest = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM_SHA);
            byte[] hashed = digest.digest(value.getBytes());
            return Hex.toHexString(hashed);
        } catch (NoSuchAlgorithmException e) {
            LOGGER.error("Could not get SHA message digest.", e);
        }
        return null;
    }

    /**
     * Get the card masks for this panel.
     *
     * @return List of CardMask.
     */
    List<CardMask> getCardMasks() {
        List<CardMask> masks = credentialDao.getCardMasks();
        masks.add(new CardMask(26, "1fffe"));
        return masks;
    }

    /**
     * Get the card masks for this credential.
     *
     * @param numOfBits
     * @return List of CardMask.
     */
    public List<CardMask> getCardMasks(Integer numOfBits) {
        List<CardMask> masks = credentialDao.getCardMasks(numOfBits);
        return masks;
    }

    /**
     * Get one page of credentials for this panel.
     *
     * @param panel
     * @return
     */
    private List<Credential> getPageCredentials(Panel panel, Long offset, Integer pageSize) {
        return credentialDao.getPageCredentials(panel, offset, pageSize);
    }

    public Integer getCredentialPageSize() {
        return credentialPageSize;
    }

}