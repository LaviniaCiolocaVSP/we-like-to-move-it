package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Set;

public class UiUserDTO implements Serializable {
    private long objectId;
    private Long userId;
    private long accountId;
    private String username;
    private String firstName;
    private String lastName;
    private long userTypeId;
    private Set<UiGroupSummaryDTO> uiSecurityGroup;

    public UiUserDTO() {

    }

    public UiUserDTO(long objectId, Long userId, String username, String firstName, String lastName, long accountId,
                     long userTypeId) {
        this.objectId = objectId;
        this.accountId = accountId;
        this.userId = userId;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userTypeId = userTypeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(long userTypeId) {
        this.userTypeId = userTypeId;
    }

    public Set<UiGroupSummaryDTO> getUiSecurityGroup() {
        return uiSecurityGroup;
    }

    public void setUiSecurityGroup(final Set<UiGroupSummaryDTO> uiSecurityGroup) {
        this.uiSecurityGroup = uiSecurityGroup;
    }
}
