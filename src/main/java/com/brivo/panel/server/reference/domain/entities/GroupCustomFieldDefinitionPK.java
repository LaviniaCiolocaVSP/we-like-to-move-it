package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class GroupCustomFieldDefinitionPK implements Serializable {
    private long groupCustomFieldDefinitionId;
    private long accountId;

    @Column(name = "group_custom_field_definition_id", nullable = false)
    @Id
    public long getGroupCustomFieldDefinitionId() {
        return groupCustomFieldDefinitionId;
    }

    public void setGroupCustomFieldDefinitionId(long groupCustomFieldDefinitionId) {
        this.groupCustomFieldDefinitionId = groupCustomFieldDefinitionId;
    }

    @Column(name = "account_id", nullable = false)
    @Id
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GroupCustomFieldDefinitionPK that = (GroupCustomFieldDefinitionPK) o;

        if (groupCustomFieldDefinitionId != that.groupCustomFieldDefinitionId) {
            return false;
        }
        return accountId == that.accountId;
    }

    @Override
    public int hashCode() {
        int result = (int) (groupCustomFieldDefinitionId ^ (groupCustomFieldDefinitionId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        return result;
    }
}
