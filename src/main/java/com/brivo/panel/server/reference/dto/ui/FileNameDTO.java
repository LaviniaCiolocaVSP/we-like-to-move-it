package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class FileNameDTO implements Serializable {
    private final String fileName;

    public FileNameDTO(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }
}
