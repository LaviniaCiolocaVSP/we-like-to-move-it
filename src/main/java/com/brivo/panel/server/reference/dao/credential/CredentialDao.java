package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.domain.entities.credential.CredentialField;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFormat;
import com.brivo.panel.server.reference.dto.CredentialFieldDTO;
import com.brivo.panel.server.reference.model.credential.CardMask;
import com.brivo.panel.server.reference.model.credential.Credential;
import com.brivo.panel.server.reference.model.credential.CredentialUsage;
import com.brivo.panel.server.reference.model.hardware.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DAO class for credential data.
 */
@Component
public class CredentialDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialDao.class);

    @Value("${maximum.number.credentials}")
    private Integer maximumNumberCredentials;

    private NamedParameterJdbcTemplate template;

    private CardMaskExtractor cardMaskExtractor;

    private CredentialExtractor credentialExtractor;

    private CredentialFieldRowExtractor credentialFieldRowExtractor;

    private CredentialFormatRowExtractor credentialFormatRowExtractor;

    /**
     * Get Card Masks.
     *
     * @return List of CardMask.
     */
    public List<CardMask> getCardMasks() {
        LOGGER.trace(">>> getCardMasks");

        String query = CredentialQuery.SELECT_CARD_MASKS.getQuery();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]: \n{}", CredentialQuery.SELECT_CARD_MASKS.name(), query);
        }

        Map<String, Object> paramMap = new HashMap<>();

        List<CardMask> list = template.query(query, paramMap, cardMaskExtractor);

        LOGGER.trace("<<< getCardMasks");

        return list;
    }

    /**
     * Get Card Masks.
     *
     * @return List of CardMask.
     */
    public List<CardMask> getCardMasks(Integer numOfBits) {
        LOGGER.trace(">>> getCardMasks");

        String query = CredentialQuery.SELECT_CARD_MASKS_WITH_NUM_OF_BITS.getQuery();

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]: \n{}", CredentialQuery.SELECT_CARD_MASKS_WITH_NUM_OF_BITS.name(), query);
        }

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("numOfBits", numOfBits);
        List<CardMask> list = template.query(query, paramMap, cardMaskExtractor);

        LOGGER.trace("<<< getCardMasks");

        return list;
    }

    /**
     * Get all credentials.
     *
     * @param panel The associated panel.
     * @return List of Credential.
     */
    public List<Credential> getAllCredentials(Panel panel, Long offset, int pageSize) {
        LOGGER.trace(">>> GetAllCredentials");

        String query = CredentialQuery.SELECT_CREDENTIALS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());
        paramMap.put("maxNumCredentials", maximumNumberCredentials);
        paramMap.put("offset", offset);
        paramMap.put("pageSize", pageSize);
        
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]:\n Params [{}] \n{}", CredentialQuery.SELECT_CREDENTIALS.name(), paramMap, query);
        }

        List<Credential> credentials = template.query(query, paramMap, credentialExtractor);

        LOGGER.trace("<<< GetAllCredentials");

        return credentials;
    }

    /**
     * Get one page of credentials.
     * <p>
     * The query that uses the access_credential_use table is SELECT_CREDENTIALS_PAGE_USE.
     *
     * @param panel    The associated panel.
     * @param offset   Offset to fetch after.
     * @param pageSize Number rows to return.
     * @return List of Credential.
     */
    public List<Credential> getPageCredentials(Panel panel, Long offset, Integer pageSize) {
        LOGGER.trace(">>> GetPageCredentials");

        String query = CredentialQuery.SELECT_CREDENTIALS_PAGE.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());
        paramMap.put("offset", offset);
        paramMap.put("pageSize", pageSize);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]:\n Params [{}] \n{}", CredentialQuery.SELECT_CREDENTIALS_PAGE.name(), paramMap, query);
        }

        List<Credential> credentials = template.query(query, paramMap, credentialExtractor);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(
                    "Query [{}] returned [{}] rows.",
                    CredentialQuery.SELECT_CREDENTIALS_PAGE.name(), credentials.size());

            LOGGER.trace("<<< GetPageCredentials");
        }

        return credentials;
    }

    /**
     * Return the number of credentials available for this panel.
     *
     * @param panel The Panel.
     * @return The count.
     */
    public Long getCredentialCount(Panel panel) {
        LOGGER.trace(">>> GetCredentialCount");

        String query = CredentialQuery.SELECT_CREDENTIALS_COUNT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]:\n Params [{}] \n{}", CredentialQuery.SELECT_CREDENTIALS_COUNT.name(), paramMap, query);
        }

        Long count = template.queryForObject(query, paramMap, Long.class);

        LOGGER.trace("<<< GetCredentialCount");

        return count;
    }

    /**
     * Gets the Credential objects for the corresponding CredentialUsage objects obtained from Cassandra specific to
     * the given panel.
     * <p>
     * Note : The returned list may be much smaller than the input list because some of the credentials and devices
     * may have been removed in OnAir.  Also, the CredentialUsage objects obtained from Cassandra are not panel specific
     * (they are Account specific), so filtering is done to return just the credentials from Cassandra for the specified panel.
     *
     * @param panel The associated panel.
     * @return List of corresponding Credential objects.
     */
    public List<Credential> getCredentials(Panel panel, List<CredentialUsage> credentialUsageList) {
        LOGGER.trace(">>> getCredentials(List<CredentialUsage> credentialUsageList)");

        if (credentialUsageList == null || credentialUsageList.size() == 0) {
            return null;
        }

        List<Credential> credentialList = null;

        // Note : A temporary table will automatically get deleted when the SQL transaction ends.
        // Note: The "ON COMMIT DROP" option is specified when the temp table is created.
        // Also, the temporary table is only visible to this connection, so there is not a concurrency issue when this method is called by
        // multiple users at the same time even though the name of the temporary table is the same.  Each caller will have
        // their own unique temporary table used for their connection only.
        // Foreign key constraints on not allowed on a temporary table, but in this case we would not want to have them on CredentialId
        // and DeviceId anyway because the data in Cassandra will exist even after the corresponding foreign keys are deleted in postgres.
        // For example, a credential or a door can be removed in OnAir but it will remain in the Cassamdra table actor_last_credential_use.
        String create_temp_table_statement = "CREATE TEMPORARY TABLE Cassandra_Credential_Usage (CredentialId BigInt NOT NULL, DeviceId BigInt NOT NULL,"
                + " UserId BigInt NOT NULL, PRIMARY KEY (CredentialId, DeviceId, UserId)) ON COMMIT DROP";

        String insertSql =
                "insert into Cassandra_Credential_Usage " +
                        "  (CredentialId, DeviceId, UserId) " +
                        "values " +
                        "  (:credentialId, :deviceId, :userId)";

        this.template.getJdbcOperations().execute(create_temp_table_statement);

        Map<String, Object>[] paramMapArray = new Map[credentialUsageList.size()];
        Map<String, Object> paramMap = null;

        int index = 0;
        for (CredentialUsage credentialUsage : credentialUsageList) {
            paramMap = new HashMap<>();
            paramMap.put("credentialId", credentialUsage.getCredentialId());
            paramMap.put("deviceId", credentialUsage.getDeviceId());
            paramMap.put("userId", credentialUsage.getUserId());
            paramMapArray[index] = paramMap;
            index++;
        }

        this.template.batchUpdate(insertSql, paramMapArray);

        String query = CredentialQuery.SELECT_USED_CREDENTIALS.getQuery();

        paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());
        paramMap.put("maxNumCredentials", maximumNumberCredentials);

        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("Query [{}]:\n Params [{}] \n{}", CredentialQuery.SELECT_USED_CREDENTIALS.name(), paramMap, query);
        }

        credentialList = template.query(query, paramMap, credentialExtractor);

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("Number of used credentials fetched: {}", (credentialList != null ? credentialList.size() :
                    0));
        }

        LOGGER.trace("<<< getCredentials(List<CredentialUsage> credentialUsageList)");

        return credentialList;
    }

    @Autowired
    public void setTemplate(NamedParameterJdbcTemplate template) {
        this.template = template;
    }

    @Autowired
    public void setCardMaskExtractor(CardMaskExtractor cardMaskExtractor) {
        this.cardMaskExtractor = cardMaskExtractor;
    }

    @Autowired
    public void setCredentialExtractor(CredentialExtractor credentialExtractor) {
        this.credentialExtractor = credentialExtractor;
    }

    @Autowired
    public void setCredentialFieldRowExtractor(CredentialFieldRowExtractor credentialFieldRowExtractor) {
        this.credentialFieldRowExtractor = credentialFieldRowExtractor;
    }

    @Autowired
    public void setCredentialFormatRowExtractor(CredentialFormatRowExtractor credentialFormatRowExtractor) {
        this.credentialFormatRowExtractor = credentialFormatRowExtractor;
    }

    public void setMaximumNumberCredentials(Integer maximumNumberCredentials) {
        this.maximumNumberCredentials = maximumNumberCredentials;
    }

    public void deleteCredentialById(Long credentialId) {
        LOGGER.info("Deleting credential with id {}", credentialId);

        String query = CredentialQuery.DELETE_CREDENTIAL.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("accessCredentialId", credentialId);

        template.update(query, paramMap);
    }

    public CredentialFormat retrieveCredentialFormat(Long access_credential_type_id) {

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("access_credential_type_id", access_credential_type_id);
        List<CredentialFormat> format_list = template.query(CredentialQuery.RETRIEVE_CREDENTIAL_FORMAT.getQuery(), paramMap, credentialFormatRowExtractor);

        CredentialFormat format = format_list.get(0);

        if (format != null) {

            List<CredentialField> formatFields = template.query(
                    CredentialQuery.RETRIEVE_CREDENTIAL_FORMAT_FIELDS.getQuery(),
                    paramMap, credentialFieldRowExtractor);

            format.setFields(formatFields);
        }
        return format;
    }

}
