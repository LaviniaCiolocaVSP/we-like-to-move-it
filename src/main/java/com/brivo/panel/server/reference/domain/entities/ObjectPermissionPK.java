package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ObjectPermissionPK implements Serializable {
    private long securityActionId;
    private long actorObjectId;
    private long objectId;

    @Column(name = "security_action_id", nullable = false)
    @Id
    public long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Column(name = "actor_object_id", nullable = false)
    @Id
    public long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Column(name = "object_id", nullable = false)
    @Id
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObjectPermissionPK that = (ObjectPermissionPK) o;

        if (securityActionId != that.securityActionId) return false;
        if (actorObjectId != that.actorObjectId) return false;
        if (objectId != that.objectId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityActionId ^ (securityActionId >>> 32));
        result = 31 * result + (int) (actorObjectId ^ (actorObjectId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }
}
