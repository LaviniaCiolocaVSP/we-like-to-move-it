package com.brivo.panel.server.reference.model.message;

public class DownstreamMessage {
    private DownstreamMessageType messageType;

    public DownstreamMessage(DownstreamMessageType messageType) {
        this.messageType = messageType;
    }

    public DownstreamMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(final DownstreamMessageType messageType) {
        this.messageType = messageType;
    }

}
