package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class Schedule {
    private Long scheduleId;
    private List<ScheduleBlock> scheduleBlocks;
    private List<ScheduleBlock> holidayBlocks;
    private List<Long> holidayIds;
    private EnablingGroup enablingGroup;
    private ScheduleThreatSettings threatLevel;
    private List<ScheduleExceptionBlock> exceptionBlocks;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public List<ScheduleBlock> getScheduleBlocks() {
        return scheduleBlocks;
    }

    public void setScheduleBlocks(List<ScheduleBlock> scheduleBlocks) {
        this.scheduleBlocks = scheduleBlocks;
    }

    public List<ScheduleBlock> getHolidayBlocks() {
        return holidayBlocks;
    }

    public void setHolidayBlocks(List<ScheduleBlock> holidayBlocks) {
        this.holidayBlocks = holidayBlocks;
    }

    public List<Long> getHolidayIds() {
        return holidayIds;
    }

    public void setHolidayIds(List<Long> holidayIds) {
        this.holidayIds = holidayIds;
    }

    public EnablingGroup getEnablingGroup() {
        return enablingGroup;
    }

    public void setEnablingGroup(EnablingGroup enablingGroup) {
        this.enablingGroup = enablingGroup;
    }

    public ScheduleThreatSettings getThreatLevel() {
        return threatLevel;
    }

    public void setThreatLevel(ScheduleThreatSettings threatLevel) {
        this.threatLevel = threatLevel;
    }

    public List<ScheduleExceptionBlock> getExceptionBlocks() {
        return exceptionBlocks;
    }

    public void setExceptionBlocks(List<ScheduleExceptionBlock> exceptionBlocks) {
        this.exceptionBlocks = exceptionBlocks;
    }

}
