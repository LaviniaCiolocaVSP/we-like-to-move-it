package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "camera_migration", schema = "brivo20", catalog = "onair")
public class CameraMigration {
    private long cameraMigrationId;
    private Timestamp migrationDate;
    private String migrationName;
    private String migrationStatus;
    private long destinationAvhsServerId;
    private Timestamp created;
    private String notes;
    private Collection<CameraMigrationCredential> cameraMigrationCredentialsByCameraMigrationId;
    private Collection<CameraMigrationHistory> cameraMigrationHistoriesByCameraMigrationId;

    @Id
    @Column(name = "camera_migration_id", nullable = false)
    public long getCameraMigrationId() {
        return cameraMigrationId;
    }

    public void setCameraMigrationId(long cameraMigrationId) {
        this.cameraMigrationId = cameraMigrationId;
    }

    @Basic
    @Column(name = "migration_date", nullable = true)
    public Timestamp getMigrationDate() {
        return migrationDate;
    }

    public void setMigrationDate(Timestamp migrationDate) {
        this.migrationDate = migrationDate;
    }

    @Basic
    @Column(name = "migration_name", nullable = false, length = 60)
    public String getMigrationName() {
        return migrationName;
    }

    public void setMigrationName(String migrationName) {
        this.migrationName = migrationName;
    }

    @Basic
    @Column(name = "migration_status", nullable = false, length = 60)
    public String getMigrationStatus() {
        return migrationStatus;
    }

    public void setMigrationStatus(String migrationStatus) {
        this.migrationStatus = migrationStatus;
    }

    @Basic
    @Column(name = "destination_avhs_server_id", nullable = false)
    public long getDestinationAvhsServerId() {
        return destinationAvhsServerId;
    }

    public void setDestinationAvhsServerId(long destinationAvhsServerId) {
        this.destinationAvhsServerId = destinationAvhsServerId;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "notes", nullable = false, length = 500)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraMigration that = (CameraMigration) o;

        if (cameraMigrationId != that.cameraMigrationId) {
            return false;
        }
        if (destinationAvhsServerId != that.destinationAvhsServerId) {
            return false;
        }
        if (migrationDate != null ? !migrationDate.equals(that.migrationDate) : that.migrationDate != null) {
            return false;
        }
        if (migrationName != null ? !migrationName.equals(that.migrationName) : that.migrationName != null) {
            return false;
        }
        if (migrationStatus != null ? !migrationStatus.equals(that.migrationStatus) : that.migrationStatus != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return notes != null ? notes.equals(that.notes) : that.notes == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraMigrationId ^ (cameraMigrationId >>> 32));
        result = 31 * result + (migrationDate != null ? migrationDate.hashCode() : 0);
        result = 31 * result + (migrationName != null ? migrationName.hashCode() : 0);
        result = 31 * result + (migrationStatus != null ? migrationStatus.hashCode() : 0);
        result = 31 * result + (int) (destinationAvhsServerId ^ (destinationAvhsServerId >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "cameraMigrationByCameraMigrationId")
    public Collection<CameraMigrationCredential> getCameraMigrationCredentialsByCameraMigrationId() {
        return cameraMigrationCredentialsByCameraMigrationId;
    }

    public void setCameraMigrationCredentialsByCameraMigrationId(Collection<CameraMigrationCredential> cameraMigrationCredentialsByCameraMigrationId) {
        this.cameraMigrationCredentialsByCameraMigrationId = cameraMigrationCredentialsByCameraMigrationId;
    }

    @OneToMany(mappedBy = "cameraMigrationByCameraMigrationId")
    public Collection<CameraMigrationHistory> getCameraMigrationHistoriesByCameraMigrationId() {
        return cameraMigrationHistoriesByCameraMigrationId;
    }

    public void setCameraMigrationHistoriesByCameraMigrationId(Collection<CameraMigrationHistory> cameraMigrationHistoriesByCameraMigrationId) {
        this.cameraMigrationHistoriesByCameraMigrationId = cameraMigrationHistoriesByCameraMigrationId;
    }
}
