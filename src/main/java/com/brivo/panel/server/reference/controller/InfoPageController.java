package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.ui.UiInfoDTO;
import com.brivo.panel.server.reference.service.FileService;
import com.brivo.panel.server.reference.service.InfoPageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class InfoPageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfoPageController.class);

    private final InfoPageService infoPageService;

    @Autowired
    public InfoPageController(final InfoPageService infoPageService) {
        this.infoPageService = infoPageService;
    }

    @GetMapping(
            path = "/info",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public UiInfoDTO getInfo() {
        LOGGER.info("Reading info file");
        return infoPageService.getInfoFileContents();
    }
}
