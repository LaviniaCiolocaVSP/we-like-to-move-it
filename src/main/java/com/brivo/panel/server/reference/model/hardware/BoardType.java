package com.brivo.panel.server.reference.model.hardware;

/**
 * See Board_Type table.
 *
 * @author brandon
 */
public enum BoardType {
    DOOR_BOARD(1),
    IO_BOARD(2),
    EDGE_BOARD(3),
    IPDC1_BOARD(4),
    IPDC2_BOARD(5),
    SALTO_ROUTER(6),
    IPAC(7),
    ALLEGION_GATEWAY(8);

    private int typeId;

    BoardType(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeId() {
        return this.typeId;
    }
}
