package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "custom_field", schema = "brivo20", catalog = "onair")
@IdClass(CustomFieldPK.class)
public class CustomField {
    private long accountId;
    private long fieldId;
    private String fieldLabel;
    private Account accountByAccountId;

    @Id
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Id
    @Column(name = "field_id", nullable = false)
    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    @Basic
    @Column(name = "field_label", nullable = true, length = 30)
    public String getFieldLabel() {
        return fieldLabel;
    }

    public void setFieldLabel(String fieldLabel) {
        this.fieldLabel = fieldLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomField that = (CustomField) o;

        if (accountId != that.accountId) {
            return false;
        }
        if (fieldId != that.fieldId) {
            return false;
        }
        return fieldLabel != null ? fieldLabel.equals(that.fieldLabel) : that.fieldLabel == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (fieldId ^ (fieldId >>> 32));
        result = 31 * result + (fieldLabel != null ? fieldLabel.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
