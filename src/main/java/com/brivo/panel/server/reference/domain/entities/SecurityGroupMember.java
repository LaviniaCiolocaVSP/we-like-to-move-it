package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "security_group_member", schema = "brivo20", catalog = "onair")
@IdClass(SecurityGroupMemberPK.class)
public class SecurityGroupMember implements Serializable {
    private long securityGroupId;
    private long objectId;
    private SecurityGroup securityGroupBySecurityGroupId;
    private BrivoObject objectByBrivoObjectId;

    @Id
    @Column(name = "security_group_id", nullable = false)
    public long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityGroupMember that = (SecurityGroupMember) o;

        if (securityGroupId != that.securityGroupId) {
            return false;
        }
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityGroupId ^ (securityGroupId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "security_group_id", referencedColumnName = "security_group_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityGroup getSecurityGroupBySecurityGroupId() {
        return securityGroupBySecurityGroupId;
    }

    public void setSecurityGroupBySecurityGroupId(SecurityGroup securityGroupBySecurityGroupId) {
        this.securityGroupBySecurityGroupId = securityGroupBySecurityGroupId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }
}
