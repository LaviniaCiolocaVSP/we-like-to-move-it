package com.brivo.panel.server.reference.model.credential;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;

public class Credential {
    private Long credentialId;
    private Long userId;
    private Instant active;
    private Instant expires;
    private String value;
    private CredentialType credentialType;
    private Integer numberBits;
    private Instant created;
    private Boolean createdWith255Magic;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Long credentialId) {
        this.credentialId = credentialId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Instant getActive() {
        return active;
    }

    public void setActive(Instant active) {
        this.active = active;
    }

    public Instant getExpires() {
        return expires;
    }

    public void setExpires(Instant expires) {
        this.expires = expires;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public CredentialType getCredentialType() {
        return credentialType;
    }

    public void setCredentialType(CredentialType credentialType) {
        this.credentialType = credentialType;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Integer getNumberBits() {
        return numberBits;
    }

    public void setNumberBits(Integer numberBits) {
        this.numberBits = numberBits;
    }

    @JsonIgnore
    public Boolean getCreatedWith255Magic() {
        return createdWith255Magic;
    }

    public void setCreatedWith255Magic(Boolean createdWith255Magic) {
        this.createdWith255Magic = createdWith255Magic;
    }
}
