package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "video_camera", schema = "brivo20", catalog = "onair")
public class VideoCamera {
    private long videoCameraId;
    private long objectId;
    private long accountId;
    private String name;
    private long videoProviderId;
    private long securityGroupId;
    private Timestamp created;
    private Timestamp updated;
    private Long deleted;
    private long videoCameraModelId;
    private Long videoSubscriptionId;
    private Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByVideoCameraId;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    private VideoProvider videoProviderByVideoProviderId;
    private SecurityGroup securityGroupBySecurityGroupId;
    private VideoCameraModel videoCameraModelByVideoCameraModelId;
    private VideoSubscription videoSubscriptionByVideoSubscriptionId;
    private Collection<VideoCameraDeviceMapping> videoCameraDeviceMappingsByVideoCameraId;
    private Collection<VideoCameraParameterValue> videoCameraParameterValuesByVideoCameraId;

    @Id
    @Column(name = "video_camera_id", nullable = false)
    public long getVideoCameraId() {
        return videoCameraId;
    }

    public void setVideoCameraId(long videoCameraId) {
        this.videoCameraId = videoCameraId;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "video_provider_id", nullable = false)
    public long getVideoProviderId() {
        return videoProviderId;
    }

    public void setVideoProviderId(long videoProviderId) {
        this.videoProviderId = videoProviderId;
    }

    @Basic
    @Column(name = "security_group_id", nullable = false)
    public long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "video_camera_model_id", nullable = false)
    public long getVideoCameraModelId() {
        return videoCameraModelId;
    }

    public void setVideoCameraModelId(long videoCameraModelId) {
        this.videoCameraModelId = videoCameraModelId;
    }

    @Basic
    @Column(name = "video_subscription_id", nullable = true)
    public Long getVideoSubscriptionId() {
        return videoSubscriptionId;
    }

    public void setVideoSubscriptionId(Long videoSubscriptionId) {
        this.videoSubscriptionId = videoSubscriptionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoCamera that = (VideoCamera) o;

        if (videoCameraId != that.videoCameraId) {
            return false;
        }
        if (objectId != that.objectId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (videoProviderId != that.videoProviderId) {
            return false;
        }
        if (securityGroupId != that.securityGroupId) {
            return false;
        }
        if (videoCameraModelId != that.videoCameraModelId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) {
            return false;
        }
        return videoSubscriptionId != null ? videoSubscriptionId.equals(that.videoSubscriptionId) : that.videoSubscriptionId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoCameraId ^ (videoCameraId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (videoProviderId ^ (videoProviderId >>> 32));
        result = 31 * result + (int) (securityGroupId ^ (securityGroupId >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        result = 31 * result + (int) (videoCameraModelId ^ (videoCameraModelId >>> 32));
        result = 31 * result + (videoSubscriptionId != null ? videoSubscriptionId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "videoCameraByVideoCameraId")
    public Collection<CameraGroupOvrCamera> getCameraGroupOvrCamerasByVideoCameraId() {
        return cameraGroupOvrCamerasByVideoCameraId;
    }

    public void setCameraGroupOvrCamerasByVideoCameraId(Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByVideoCameraId) {
        this.cameraGroupOvrCamerasByVideoCameraId = cameraGroupOvrCamerasByVideoCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "video_provider_id", referencedColumnName = "video_provider_id", nullable = false, insertable = false, updatable = false)
    public VideoProvider getVideoProviderByVideoProviderId() {
        return videoProviderByVideoProviderId;
    }

    public void setVideoProviderByVideoProviderId(VideoProvider videoProviderByVideoProviderId) {
        this.videoProviderByVideoProviderId = videoProviderByVideoProviderId;
    }

    @ManyToOne
    @JoinColumn(name = "security_group_id", referencedColumnName = "security_group_id", nullable = false, insertable = false, updatable = false)
    public SecurityGroup getSecurityGroupBySecurityGroupId() {
        return securityGroupBySecurityGroupId;
    }

    public void setSecurityGroupBySecurityGroupId(SecurityGroup securityGroupBySecurityGroupId) {
        this.securityGroupBySecurityGroupId = securityGroupBySecurityGroupId;
    }

    @ManyToOne
    @JoinColumn(name = "video_camera_model_id", referencedColumnName = "video_camera_model_id", nullable = false, insertable = false, updatable = false)
    public VideoCameraModel getVideoCameraModelByVideoCameraModelId() {
        return videoCameraModelByVideoCameraModelId;
    }

    public void setVideoCameraModelByVideoCameraModelId(VideoCameraModel videoCameraModelByVideoCameraModelId) {
        this.videoCameraModelByVideoCameraModelId = videoCameraModelByVideoCameraModelId;
    }

    @ManyToOne
    @JoinColumn(name = "video_subscription_id", referencedColumnName = "video_subscription_id", insertable = false, updatable = false)
    public VideoSubscription getVideoSubscriptionByVideoSubscriptionId() {
        return videoSubscriptionByVideoSubscriptionId;
    }

    public void setVideoSubscriptionByVideoSubscriptionId(VideoSubscription videoSubscriptionByVideoSubscriptionId) {
        this.videoSubscriptionByVideoSubscriptionId = videoSubscriptionByVideoSubscriptionId;
    }

    @OneToMany(mappedBy = "videoCameraByVideoCameraId")
    public Collection<VideoCameraDeviceMapping> getVideoCameraDeviceMappingsByVideoCameraId() {
        return videoCameraDeviceMappingsByVideoCameraId;
    }

    public void setVideoCameraDeviceMappingsByVideoCameraId(Collection<VideoCameraDeviceMapping> videoCameraDeviceMappingsByVideoCameraId) {
        this.videoCameraDeviceMappingsByVideoCameraId = videoCameraDeviceMappingsByVideoCameraId;
    }

    @OneToMany(mappedBy = "videoCameraByVideoCameraId")
    public Collection<VideoCameraParameterValue> getVideoCameraParameterValuesByVideoCameraId() {
        return videoCameraParameterValuesByVideoCameraId;
    }

    public void setVideoCameraParameterValuesByVideoCameraId(Collection<VideoCameraParameterValue> videoCameraParameterValuesByVideoCameraId) {
        this.videoCameraParameterValuesByVideoCameraId = videoCameraParameterValuesByVideoCameraId;
    }
}
