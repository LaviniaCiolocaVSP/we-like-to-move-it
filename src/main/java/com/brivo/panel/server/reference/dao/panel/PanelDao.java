package com.brivo.panel.server.reference.dao.panel;

import com.brivo.panel.server.reference.model.hardware.Panel;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class PanelDao {

    private final NamedParameterJdbcTemplate jdbcTemplate;
    private final PanelRowMapper panelRowMapper;

    @Autowired
    public PanelDao(final NamedParameterJdbcTemplate jdbcTemplate, final PanelRowMapper panelRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.panelRowMapper = panelRowMapper;
    }

    /**
     * Get panel by electronic serial number.
     */
    public Panel getPanelByElectronicSerialNumber(String panelElectronicSerialNumber) {
        String query = PanelQuery.SELECT_PANEL_BY_SERIAL.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panelElectronicSerialNumber);

        List<Panel> panels = jdbcTemplate.query(query, paramMap, panelRowMapper);

        return Iterables.getFirst(panels, null);
    }

    public Panel getPanelByDeviceObjectId(Long deviceObjectId) {
        String query = PanelQuery.SELECT_PANEL_BY_DEVICE.getQuery();

        Map<String, Object> paramMap = Collections.singletonMap("deviceObjectId", deviceObjectId);

        List<Panel> list = jdbcTemplate.query(query, paramMap, panelRowMapper);

        return Iterables.getFirst(list, null);
    }

    public Long getDeviceObjectIdByDeviceId(Long deviceId) {
        String query = PanelQuery.SELECT_DEVICE_OBJECT_ID_BY_DEVICE_ID.getQuery();

        Map<String, Object> paramMap = Collections.singletonMap("deviceId", deviceId);

        return jdbcTemplate.queryForObject(query, paramMap, Long.class);
    }

    public void deletePanelById(final Long brainId, final Long brainObjectId) {
        String query = PanelQuery.DELETE_PANEL.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brainId", brainId);
        paramMap.put("brainObjectId", brainObjectId);

        jdbcTemplate.update(query, paramMap);
    }
}
