package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "credential_mask", schema = "brivo20", catalog = "onair")
@IdClass(CredentialMaskPK.class)
public class CredentialMask {
    private long accessCredentialType;
    private String mask;
    private long useOrder;
    private AccessCredentialType accessCredentialTypeByAccessCredentialType;

    @Id
    @Column(name = "access_credential_type", nullable = false)
    public long getAccessCredentialType() {
        return accessCredentialType;
    }

    public void setAccessCredentialType(long accessCredentialType) {
        this.accessCredentialType = accessCredentialType;
    }

    @Basic
    @Column(name = "mask", nullable = false, length = 255)
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }

    @Id
    @Column(name = "use_order", nullable = false)
    public long getUseOrder() {
        return useOrder;
    }

    public void setUseOrder(long useOrder) {
        this.useOrder = useOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialMask that = (CredentialMask) o;

        if (accessCredentialType != that.accessCredentialType) {
            return false;
        }
        if (useOrder != that.useOrder) {
            return false;
        }
        return mask != null ? mask.equals(that.mask) : that.mask == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accessCredentialType ^ (accessCredentialType >>> 32));
        result = 31 * result + (mask != null ? mask.hashCode() : 0);
        result = 31 * result + (int) (useOrder ^ (useOrder >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "access_credential_type", referencedColumnName = "access_credential_type_id", nullable = false,
            insertable = false, updatable = false)
    public AccessCredentialType getAccessCredentialTypeByAccessCredentialType() {
        return accessCredentialTypeByAccessCredentialType;
    }

    public void setAccessCredentialTypeByAccessCredentialType(AccessCredentialType accessCredentialTypeByAccessCredentialType) {
        this.accessCredentialTypeByAccessCredentialType = accessCredentialTypeByAccessCredentialType;
    }
}
