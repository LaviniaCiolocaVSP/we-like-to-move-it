package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "tes_directory", schema = "brivo20", catalog = "onair")
public class TesDirectory {
    private long directoryId;
    private long objectId;
    private long accountId;
    private String name;
    private long codeLength;
    private Timestamp created;
    private Timestamp updated;
    private short deleted;
    private Collection<IpacData> ipacDataByDirectoryId;
    private Account accountByAccountId;
    private Collection<TesResident> tesResidentsByDirectoryId;

    @Id
    @Column(name = "directory_id", nullable = false)
    public long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(long directoryId) {
        this.directoryId = directoryId;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "code_length", nullable = false)
    public long getCodeLength() {
        return codeLength;
    }

    public void setCodeLength(long codeLength) {
        this.codeLength = codeLength;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TesDirectory that = (TesDirectory) o;

        if (directoryId != that.directoryId) {
            return false;
        }
        if (objectId != that.objectId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (codeLength != that.codeLength) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (directoryId ^ (directoryId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (codeLength ^ (codeLength >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) deleted;
        return result;
    }

    @OneToMany(mappedBy = "tesDirectoryByTesDirectoryId")
    public Collection<IpacData> getIpacDataByDirectoryId() {
        return ipacDataByDirectoryId;
    }

    public void setIpacDataByDirectoryId(Collection<IpacData> ipacDataByDirectoryId) {
        this.ipacDataByDirectoryId = ipacDataByDirectoryId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "tesDirectoryByDirectoryId")
    public Collection<TesResident> getTesResidentsByDirectoryId() {
        return tesResidentsByDirectoryId;
    }

    public void setTesResidentsByDirectoryId(Collection<TesResident> tesResidentsByDirectoryId) {
        this.tesResidentsByDirectoryId = tesResidentsByDirectoryId;
    }
}
