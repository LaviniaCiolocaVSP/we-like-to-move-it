package com.brivo.panel.server.reference.service.account;

import com.brivo.panel.server.reference.domain.repository.AccountTypeRepository;
import com.brivo.panel.server.reference.domain.repository.BrainStateRepository;
import com.brivo.panel.server.reference.domain.repository.DevicePropertyRepository;
import com.brivo.panel.server.reference.domain.repository.DoorDataAntipassbackRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupRepository;
import com.brivo.panel.server.reference.service.util.FolderUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.time.LocalDateTime;

@Component
public abstract class AbstractAccountCRUDComponent {

    public static final Long NOT_AVAILABLE_LONG_VALUE = -1L;
    public static final String NOT_AVAILABLE = "N/A";
    public static final LocalDateTime NOT_AVAILABLE_DATE = LocalDateTime.of(1000, 10, 10, 10, 10, 10);
    public static final short NOT_AVAILABLE_SHORT_VALUE = -1;
    public static final String MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY = "MAX_REX_EXTENSION";
    public static final String BLUETOOTH_READER_ID_DEVICE_TYPE_PROPERTY = "BLUETOOTH_READER_ID";
    static final String PATH_SEPARATOR = File.separator;
    @Autowired
    AccountTypeRepository accountTypeRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ObjectPermissionRepository objectPermissionRepository;

    @Autowired
    PanelRepository panelRepository;

    @Autowired
    DoorDataAntipassbackRepository doorDataAntipassbackRepository;

    @Autowired
    DevicePropertyRepository devicePropertyRepository;

    @Autowired
    BrainStateRepository brainStateRepository;

    @Autowired
    SecurityGroupRepository securityGroupRepository;

    @Value("${upload-folder.root}")
    String uploadRootFolder;

    @Value("${upload-folder.accounts}")
    String accountsFolder;

    String accountsUploadFolder;

    @PostConstruct
    public void initialize() {
        accountsUploadFolder = uploadRootFolder + PATH_SEPARATOR + accountsFolder;
        FolderUtils.checkFolderExistence(accountsUploadFolder, "accounts upload");
    }
}
