package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "board_data", schema = "brivo20", catalog = "onair")
public class BoardData {
    private long boardId;
    private String boardType;
    private String hardwareSerial;
    private String hardwareRevision;
    private String firmwareVersion;
    private Long brainId;
    private String freescaleVersion;
    private String hardwareType;
    private short finished;
    private Timestamp created;
    private Timestamp updated;
    private String modem;
    private String mfgClientVersion;
    private String mfgServerVersion;
    private Long manufacturerId;
    private Manufacturer manufacturerByManufacturerId;

    @Id
    @Column(name = "board_id", nullable = false)
    public long getBoardId() {
        return boardId;
    }

    public void setBoardId(long boardId) {
        this.boardId = boardId;
    }

    @Basic
    @Column(name = "board_type", nullable = false, length = 10)
    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    @Basic
    @Column(name = "hardware_serial", nullable = false, length = 30)
    public String getHardwareSerial() {
        return hardwareSerial;
    }

    public void setHardwareSerial(String hardwareSerial) {
        this.hardwareSerial = hardwareSerial;
    }

    @Basic
    @Column(name = "hardware_revision", nullable = true, length = 10)
    public String getHardwareRevision() {
        return hardwareRevision;
    }

    public void setHardwareRevision(String hardwareRevision) {
        this.hardwareRevision = hardwareRevision;
    }

    @Basic
    @Column(name = "firmware_version", nullable = true, length = 10)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Basic
    @Column(name = "brain_id", nullable = true)
    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "freescale_version", nullable = true, length = 10)
    public String getFreescaleVersion() {
        return freescaleVersion;
    }

    public void setFreescaleVersion(String freescaleVersion) {
        this.freescaleVersion = freescaleVersion;
    }

    @Basic
    @Column(name = "hardware_type", nullable = true, length = 10)
    public String getHardwareType() {
        return hardwareType;
    }

    public void setHardwareType(String hardwareType) {
        this.hardwareType = hardwareType;
    }

    @Basic
    @Column(name = "finished", nullable = false)
    public short getFinished() {
        return finished;
    }

    public void setFinished(short finished) {
        this.finished = finished;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "modem", nullable = true, length = 64)
    public String getModem() {
        return modem;
    }

    public void setModem(String modem) {
        this.modem = modem;
    }

    @Basic
    @Column(name = "mfg_client_version", nullable = true, length = 10)
    public String getMfgClientVersion() {
        return mfgClientVersion;
    }

    public void setMfgClientVersion(String mfgClientVersion) {
        this.mfgClientVersion = mfgClientVersion;
    }

    @Basic
    @Column(name = "mfg_server_version", nullable = true, length = 10)
    public String getMfgServerVersion() {
        return mfgServerVersion;
    }

    public void setMfgServerVersion(String mfgServerVersion) {
        this.mfgServerVersion = mfgServerVersion;
    }

    @Basic
    @Column(name = "manufacturer_id", nullable = true)
    public Long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardData boardData = (BoardData) o;

        if (boardId != boardData.boardId) {
            return false;
        }
        if (finished != boardData.finished) {
            return false;
        }
        if (boardType != null ? !boardType.equals(boardData.boardType) : boardData.boardType != null) {
            return false;
        }
        if (hardwareSerial != null ? !hardwareSerial.equals(boardData.hardwareSerial) : boardData.hardwareSerial != null) {
            return false;
        }
        if (hardwareRevision != null ? !hardwareRevision.equals(boardData.hardwareRevision) : boardData.hardwareRevision != null) {
            return false;
        }
        if (firmwareVersion != null ? !firmwareVersion.equals(boardData.firmwareVersion) : boardData.firmwareVersion != null) {
            return false;
        }
        if (brainId != null ? !brainId.equals(boardData.brainId) : boardData.brainId != null) {
            return false;
        }
        if (freescaleVersion != null ? !freescaleVersion.equals(boardData.freescaleVersion) : boardData.freescaleVersion != null) {
            return false;
        }
        if (hardwareType != null ? !hardwareType.equals(boardData.hardwareType) : boardData.hardwareType != null) {
            return false;
        }
        if (created != null ? !created.equals(boardData.created) : boardData.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(boardData.updated) : boardData.updated != null) {
            return false;
        }
        if (modem != null ? !modem.equals(boardData.modem) : boardData.modem != null) {
            return false;
        }
        if (mfgClientVersion != null ? !mfgClientVersion.equals(boardData.mfgClientVersion) : boardData.mfgClientVersion != null) {
            return false;
        }
        if (mfgServerVersion != null ? !mfgServerVersion.equals(boardData.mfgServerVersion) : boardData.mfgServerVersion != null) {
            return false;
        }
        return manufacturerId != null ? manufacturerId.equals(boardData.manufacturerId) : boardData.manufacturerId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (boardId ^ (boardId >>> 32));
        result = 31 * result + (boardType != null ? boardType.hashCode() : 0);
        result = 31 * result + (hardwareSerial != null ? hardwareSerial.hashCode() : 0);
        result = 31 * result + (hardwareRevision != null ? hardwareRevision.hashCode() : 0);
        result = 31 * result + (firmwareVersion != null ? firmwareVersion.hashCode() : 0);
        result = 31 * result + (brainId != null ? brainId.hashCode() : 0);
        result = 31 * result + (freescaleVersion != null ? freescaleVersion.hashCode() : 0);
        result = 31 * result + (hardwareType != null ? hardwareType.hashCode() : 0);
        result = 31 * result + (int) finished;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (modem != null ? modem.hashCode() : 0);
        result = 31 * result + (mfgClientVersion != null ? mfgClientVersion.hashCode() : 0);
        result = 31 * result + (mfgServerVersion != null ? mfgServerVersion.hashCode() : 0);
        result = 31 * result + (manufacturerId != null ? manufacturerId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "manufacturer_id", referencedColumnName = "manufacturer_id", insertable = false, updatable = false)
    public Manufacturer getManufacturerByManufacturerId() {
        return manufacturerByManufacturerId;
    }

    public void setManufacturerByManufacturerId(Manufacturer manufacturerByManufacturerId) {
        this.manufacturerByManufacturerId = manufacturerByManufacturerId;
    }
}
