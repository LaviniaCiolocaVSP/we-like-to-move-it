package com.brivo.panel.server.reference.model.usergroup;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.List;

public class UserGroup {
    private Long userGroupId;
    private List<Long> userIds;
    private Integer antiPassbackResetTime;
    private Boolean immuneToAntipassback;
    private Instant active;
    private Instant expires;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getUserGroupId() {
        return userGroupId;
    }

    public void setUserGroupId(Long userGroupId) {
        this.userGroupId = userGroupId;
    }

    public List<Long> getUserIds() {
        return userIds;
    }

    public void setUserIds(List<Long> userIds) {
        this.userIds = userIds;
    }

    public Integer getAntiPassbackResetTime() {
        return antiPassbackResetTime;
    }

    public void setAntiPassbackResetTime(Integer antiPassbackResetTime) {
        this.antiPassbackResetTime = antiPassbackResetTime;
    }

    public Instant getActive() {
        return active;
    }

    public void setActive(Instant active) {
        this.active = active;
    }

    public Instant getExpires() {
        return expires;
    }

    public void setExpires(Instant expires) {
        this.expires = expires;
    }

    public Boolean getImmuneToAntipassback() {
        return immuneToAntipassback;
    }

    public void setImmuneToAntipassback(Boolean immuneToAntipassback) {
        this.immuneToAntipassback = immuneToAntipassback;
    }
}
