package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface AccountRepository extends CrudRepository<Account, Long> {

    Optional<Account> findByName(final String name);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO(acc.accountId, acc.name) " +
                    "FROM Account acc " +
                    "ORDER BY acc.accountId ASC"
    )
    Page<UiAccountSummaryDTO> getCurrentAccount(final Pageable limit);

    @Query(
            value = "SELECT MIN(brand_id) " +
                    "FROM brivo20.brand ",
            nativeQuery = true
    )
    Long getMinBrandId();
}
