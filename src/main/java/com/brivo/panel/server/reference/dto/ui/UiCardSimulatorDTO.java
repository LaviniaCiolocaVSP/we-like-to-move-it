package com.brivo.panel.server.reference.dto.ui;

public class UiCardSimulatorDTO {

    private String panelSerialNo;
    private String IPAddress;
    private Integer readerNumber;
    private String facilityCode;
    private String cardNumber;
    private Integer iterationCount;
    private String binaryValue;
    private Integer delayTime;

    public UiCardSimulatorDTO() {

    }

    public UiCardSimulatorDTO(final String panelSerialNo, final String IPAddress, final Integer readerNumber, final String facilityCode, final String cardNumber, final Integer iterationCount, final String binaryValue, final Integer delayTime) {
        this.panelSerialNo = panelSerialNo;
        this.IPAddress = IPAddress;
        this.readerNumber = readerNumber;
        this.facilityCode = facilityCode;
        this.cardNumber = cardNumber;
        this.iterationCount = iterationCount;
        this.binaryValue = binaryValue;
        this.delayTime = delayTime;
    }

    public String getPanelSerialNo() {
        return panelSerialNo;
    }

    public void setPanelSerialNo(final String panelSerialNo) {
        this.panelSerialNo = panelSerialNo;
    }

    public String getIPAddress() {
        return IPAddress;
    }

    public void setIPAddress(final String IPAddress) {
        this.IPAddress = IPAddress;
    }

    public Integer getReaderNumber() {
        return readerNumber;
    }

    public void setReaderNumber(final Integer readerNumber) {
        this.readerNumber = readerNumber;
    }

    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(final String facilityCode) {
        this.facilityCode = facilityCode;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(final String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public Integer getIterationCount() {
        return iterationCount;
    }

    public void setIterationCount(final Integer iterationCount) {
        this.iterationCount = iterationCount;
    }

    public String getBinaryValue() {
        return binaryValue;
    }

    public void setBinaryValue(final String binaryValue) {
        this.binaryValue = binaryValue;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(final Integer delayTime) {
        this.delayTime = delayTime;
    }
}
