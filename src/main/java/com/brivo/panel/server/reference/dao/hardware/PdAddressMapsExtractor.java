package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.PdAddressMap;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class PdAddressMapsExtractor implements ResultSetExtractor<List<PdAddressMap>> {

    final int NODE_ONE_IO_POINT_ADDRESS = 10;
    final int NODE_TWO_IO_POINT_ADDRESS = 23;

    @Override
    public List<PdAddressMap> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<PdAddressMap> pdAddresses = new ArrayList<PdAddressMap>();
        while (rs.next()) {

            int osdpPdAddress = rs.getInt("pdAddress");
            if (!rs.wasNull()) {
                PdAddressMap map = new PdAddressMap();
                map.setBoardAddress(rs.getInt("boardAddress"));
                map.setPdAddress(osdpPdAddress);
                if (rs.getInt("nodeNum") == 1) {
                    map.setPointAddress(NODE_ONE_IO_POINT_ADDRESS);
                } else {
                    map.setPointAddress(NODE_TWO_IO_POINT_ADDRESS);
                }
                pdAddresses.add(map);
            }

        }

        return pdAddresses;
    }

}
