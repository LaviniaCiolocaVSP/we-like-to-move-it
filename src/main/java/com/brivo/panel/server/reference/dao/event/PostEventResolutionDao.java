package com.brivo.panel.server.reference.dao.event;

import com.brivo.panel.server.reference.model.event.SiteOccupant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class PostEventResolutionDao {
    private static final ValidateSiteOccupancyResultSetExtractor VALIDATE_SITE_OCCUPANCY_EXTRACTOR = new ValidateSiteOccupancyResultSetExtractor();
    private static final SiteOccupantResultSetExtractor SITE_OCCUPANT_EXTRACTOR = new SiteOccupantResultSetExtractor();
    Logger logger = LoggerFactory.getLogger(PostEventResolutionDao.class);
    @Autowired
    NamedParameterJdbcTemplate template;

    /**
     * Update the unknown_card_log table.
     *//*
    public int updateUnknownCredential(SecurityLogEvent event)
    {
        logger.trace(">>> UpdateUnknownCredential");

        String query = EventQuery.UPDATE_UNKNOWN_CREDENTIAL.getQuery();

        Map<String,Object> paramMap = new HashMap<>();
        paramMap.put("credentialValue", event.getEventData().getUnknownWeigand());
        paramMap.put("credentialBits", event.getEventData().getCredentialBitLength());
        paramMap.put("occurred", event.getOccurred());
        paramMap.put("objectID", event.getObjectId());
        
        logger.trace("Query ["
                + EventQuery.UPDATE_UNKNOWN_CREDENTIAL.name()
                + "]:\n Params [" + paramMap + "] \n" + query);
        
        int rows = template.update(query, paramMap);
        
        logger.trace("<<< UpdateUnknownCredential");
        
        return rows;
    }
*/
    public Boolean isValidForSiteOccupant(Long deviceObjectId) {
        logger.trace(">>> GetValidForPresence");

        String query = EventQuery.SELECT_VALID_FOR_PRESENCE.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectId", deviceObjectId);

        logger.trace("Query ["
                + EventQuery.SELECT_VALID_FOR_PRESENCE.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        return template.query(query, paramMap, VALIDATE_SITE_OCCUPANCY_EXTRACTOR);
    }

    public SiteOccupant getSiteOccupantDoorInfo(Long deviceObjectId) {
        String query = EventQuery.SELECT_SITE_OCCUPANT_DOOR_INFO.getQuery();
        Map<String, Long> params = Collections.singletonMap("deviceObjectId", deviceObjectId);

        return template.query(query, params, SITE_OCCUPANT_EXTRACTOR);
    }

    /**
     * Upsert Site Occupant.
     * <p>
     * Run update_site_occupant_for_presence.sql
     */
    public void upsertSiteOccupantForIngress(SiteOccupant so) {
        logger.trace(">>> UpdateSiteOccupant");

        String updateQuery = EventQuery.UPDATE_SITE_OCCUPANT_FOR_INGRESS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("siteId", so.getSiteId());
        paramMap.put("entered", Date.from(so.getEntered()));
        paramMap.put("entryDoorId", so.getEntryDoorId());
        paramMap.put("userId", so.getUserId());

        logger.trace("Query ["
                + EventQuery.UPDATE_SITE_OCCUPANT_FOR_INGRESS.name()
                + "]:\n Params [" + paramMap + "] \n" + updateQuery);

        int rows = template.update(updateQuery, paramMap);

        if (rows == 0) {
            String insertQuery = EventQuery.INSERT_SITE_OCCUPANT_FOR_INGRESS.getQuery();

            logger.trace("Query ["
                    + EventQuery.INSERT_SITE_OCCUPANT_FOR_INGRESS.name()
                    + "]:\n Params [" + paramMap + "] \n" + insertQuery);

            template.update(insertQuery, paramMap);
        }

        logger.trace("<<< UpdateSiteOccupant");

    }

    public void updateSiteOccupantForEgress(SiteOccupant siteOccupant) {
        String updateQuery = EventQuery.UPDATE_SITE_OCCUPANT_FOR_EGRESS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("siteId", siteOccupant.getSiteId());
        paramMap.put("exited", Date.from(siteOccupant.getExited()));
        paramMap.put("exitDoorId", siteOccupant.getExitDoorId());
        paramMap.put("userId", siteOccupant.getUserId());

        template.update(updateQuery, paramMap);

        logger.trace("Query ["
                + EventQuery.UPDATE_SITE_OCCUPANT_FOR_EGRESS.name()
                + "]:\n Params [" + paramMap + "] \n" + updateQuery);

    }

    private static class ValidateSiteOccupancyResultSetExtractor implements ResultSetExtractor<Boolean> {

        @Override
        public Boolean extractData(ResultSet rs) throws SQLException, DataAccessException {
            Boolean validForSiteOccupancy = false;
            if (rs.next()) {
                int ingress = rs.getInt("totalIngress");
                int egress = rs.getInt("totalEgress");

                validForSiteOccupancy = (ingress > 0 && egress > 0);
            }

            return validForSiteOccupancy;
        }
    }

    private static class SiteOccupantResultSetExtractor implements ResultSetExtractor<SiteOccupant> {

        @Override
        public SiteOccupant extractData(ResultSet rs) throws SQLException, DataAccessException {
            SiteOccupant siteOccupant = null;
            if (rs.next()) {
                siteOccupant = new SiteOccupant();
                siteOccupant.setDeviceId(rs.getLong("deviceObjectId"));
                siteOccupant.setSiteId(rs.getLong("siteObjectId"));
                siteOccupant.setIsIngress(1 == rs.getInt("is_ingress"));
                siteOccupant.setIsEgress(1 == rs.getInt("is_egress"));
            }

            return siteOccupant;
        }
    }

}

