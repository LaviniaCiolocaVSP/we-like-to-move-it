package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "user_print_status", schema = "brivo20", catalog = "onair")
public class UserPrintStatus implements Serializable {
    private long userPrintStatusId;
    private long userObjectId;
    private long printStatusId;
    private Timestamp updated;
    private long badgeTemplateId;
    private Long badgePrintJobId;
    //    private Collection<Users> reportSelectByReportId;
    private PrintStatus printStatusByPrintStatusId;
    private BadgeTemplate badgeTemplateByBadgeTemplateId;
    private BadgePrintJob badgePrintJobByBadgePrintJobId;

    @Id
    @Column(name = "user_print_status_id", nullable = false)
    public long getUserPrintStatusId() {
        return userPrintStatusId;
    }

    public void setUserPrintStatusId(long userPrintStatusId) {
        this.userPrintStatusId = userPrintStatusId;
    }

    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Column(name = "print_status_id", nullable = false)
    public long getPrintStatusId() {
        return printStatusId;
    }

    public void setPrintStatusId(long printStatusId) {
        this.printStatusId = printStatusId;
    }

    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Column(name = "badge_template_id", nullable = false)
    public long getBadgeTemplateId() {
        return badgeTemplateId;
    }

    public void setBadgeTemplateId(long badgeTemplateId) {
        this.badgeTemplateId = badgeTemplateId;
    }

    @Column(name = "badge_print_job_id", nullable = true)
    public Long getBadgePrintJobId() {
        return badgePrintJobId;
    }

    public void setBadgePrintJobId(Long badgePrintJobId) {
        this.badgePrintJobId = badgePrintJobId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserPrintStatus that = (UserPrintStatus) o;

        if (userPrintStatusId != that.userPrintStatusId) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        if (printStatusId != that.printStatusId) {
            return false;
        }
        if (badgeTemplateId != that.badgeTemplateId) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        return badgePrintJobId != null ? badgePrintJobId.equals(that.badgePrintJobId) : that.badgePrintJobId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userPrintStatusId ^ (userPrintStatusId >>> 32));
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (int) (printStatusId ^ (printStatusId >>> 32));
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (badgeTemplateId ^ (badgeTemplateId >>> 32));
        result = 31 * result + (badgePrintJobId != null ? badgePrintJobId.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "userPrintStatusByObjectId")
//    public Collection<Users> getReportSelectByReportId() {
//        return reportSelectByReportId;
//    }
//
//    public void setReportSelectByReportId(Collection<Users> reportSelectByReportId) {
//        this.reportSelectByReportId = reportSelectByReportId;
//    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "print_status_id", referencedColumnName = "print_status_id", nullable = false, insertable = false,
            updatable = false)
    public PrintStatus getPrintStatusByPrintStatusId() {
        return printStatusByPrintStatusId;
    }

    public void setPrintStatusByPrintStatusId(PrintStatus printStatusByPrintStatusId) {
        this.printStatusByPrintStatusId = printStatusByPrintStatusId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "badge_template_id", referencedColumnName = "badge_template_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeTemplate getBadgeTemplateByBadgeTemplateId() {
        return badgeTemplateByBadgeTemplateId;
    }

    public void setBadgeTemplateByBadgeTemplateId(BadgeTemplate badgeTemplateByBadgeTemplateId) {
        this.badgeTemplateByBadgeTemplateId = badgeTemplateByBadgeTemplateId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "badge_print_job_id", referencedColumnName = "badge_print_job_id", insertable = false,
            updatable = false)
    public BadgePrintJob getBadgePrintJobByBadgePrintJobId() {
        return badgePrintJobByBadgePrintJobId;
    }

    public void setBadgePrintJobByBadgePrintJobId(BadgePrintJob badgePrintJobByBadgePrintJobId) {
        this.badgePrintJobByBadgePrintJobId = badgePrintJobByBadgePrintJobId;
    }
}
