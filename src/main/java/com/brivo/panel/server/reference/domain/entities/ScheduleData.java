package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schedule_data", schema = "brivo20", catalog = "onair")
@IdClass(ScheduleDataPK.class)
public class ScheduleData {
    private long scheduleId;
    private long startTime;
    private long stopTime;
    private Schedule scheduleByScheduleId;

    @Id
    @Column(name = "schedule_id", nullable = false, insertable = false, updatable = false)
    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Id
    @Column(name = "start_time", nullable = false)
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Id
    @Column(name = "stop_time", nullable = false)
    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleData that = (ScheduleData) o;

        if (scheduleId != that.scheduleId) {
            return false;
        }
        if (startTime != that.startTime) {
            return false;
        }
        return stopTime == that.stopTime;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleId ^ (scheduleId >>> 32));
        result = 31 * result + (int) (startTime ^ (startTime >>> 32));
        result = 31 * result + (int) (stopTime ^ (stopTime >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", nullable = false, insertable = false, updatable = false)
    public Schedule getScheduleByScheduleId() {
        return scheduleByScheduleId;
    }

    public void setScheduleByScheduleId(Schedule scheduleByScheduleId) {
        this.scheduleByScheduleId = scheduleByScheduleId;
    }

    @Override
    public String toString() {
        return "ScheduleData{" +
                "scheduleId=" + scheduleId +
                ", startTime=" + startTime +
                ", stopTime=" + stopTime +
                ", scheduleByScheduleId=" + scheduleByScheduleId +
                '}';
    }
}
