package com.brivo.panel.server.reference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

@SpringBootApplication(
        exclude = {
                DataSourceAutoConfiguration.class,
                HibernateJpaAutoConfiguration.class
        }
)
@PropertySource("classpath:application.version")
public class PanelReferenceServerApplication {

    private static final Logger LOGGER = LoggerFactory.getLogger(PanelReferenceServerApplication.class);

    private static final String SPRING_PROFILES_ACTIVE_PROPERTY = "spring.profiles.active";

    public static void main(String[] args) {
        final SpringApplication app = buildApp(args);
        app.run(args);
    }

    private static SpringApplication buildApp(final String[] args) {
        final SpringApplication app = new SpringApplication(PanelReferenceServerApplication.class);
        app.setBannerMode(Banner.Mode.OFF);
        app.setRegisterShutdownHook(true);
        app.setHeadless(true);
        app.setLogStartupInfo(true);
        setProfile(app, args);
        return app;
    }

    private static void setProfile(final SpringApplication app, final String[] args) {
        final SimpleCommandLinePropertySource commandLinePropertySource = new SimpleCommandLinePropertySource(args);
        if (!commandLinePropertySource.containsProperty(SPRING_PROFILES_ACTIVE_PROPERTY)) {
            app.setAdditionalProfiles(RunProfile.DEV_MAC);
        }
    }

    @Bean
    protected ServletContextListener listener() {
        return new ServletContextListener() {
            @Override
            public void contextInitialized(ServletContextEvent sce) {
                LOGGER.trace("ServletContext initialized");
            }

            @Override
            public void contextDestroyed(ServletContextEvent sce) {
                LOGGER.trace("ServletContext destroyed");
            }
        };
    }
}
