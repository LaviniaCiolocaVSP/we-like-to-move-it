package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class ValidCredentialDevice {
    private Long deviceId;
    private Integer inputBoardAddress;
    private Integer inputPointAddress;
    private Long inputBoardId;

    private Boolean reportEngage;
    private List<ProgrammableDeviceOutput> outputs;
    private List<EnterPermission> enterPermissions;
    private TwoFactorCredentialSettings twoFactorCredential;
    private Long cardRequiredScheduleId;
    private AntipassbackSettings antipassback;
    private Boolean reportLiveStatus;
    private KeypadCommand keypadCommand;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getInputBoardAddress() {
        return inputBoardAddress;
    }

    public void setInputBoardAddress(Integer inputBoardAddress) {
        this.inputBoardAddress = inputBoardAddress;
    }

    public Integer getInputPointAddress() {
        return inputPointAddress;
    }

    public void setInputPointAddress(Integer inputPointAddress) {
        this.inputPointAddress = inputPointAddress;
    }

    public Long getInputBoardId() {
        return inputBoardId;
    }

    public void setInputBoardId(Long inputBoardId) {
        this.inputBoardId = inputBoardId;
    }

    public Boolean getReportEngage() {
        return reportEngage;
    }

    public void setReportEngage(Boolean reportEngage) {
        this.reportEngage = reportEngage;
    }

    public List<ProgrammableDeviceOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<ProgrammableDeviceOutput> outputs) {
        this.outputs = outputs;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public TwoFactorCredentialSettings getTwoFactorCredential() {
        return twoFactorCredential;
    }

    public void setTwoFactorCredential(
            TwoFactorCredentialSettings twoFactorCredential) {
        this.twoFactorCredential = twoFactorCredential;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    public AntipassbackSettings getAntipassback() {
        return antipassback;
    }

    public void setAntipassback(AntipassbackSettings antipassback) {
        this.antipassback = antipassback;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

    public KeypadCommand getKeypadCommands() {
        return keypadCommand;
    }

    public void setKeypadCommands(KeypadCommand keypadCommand) {
        this.keypadCommand = keypadCommand;
    }
}
