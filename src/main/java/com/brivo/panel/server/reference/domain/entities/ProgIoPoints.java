package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prog_io_points", schema = "brivo20", catalog = "onair")
@IdClass(ProgIoPointsPK.class)
public class ProgIoPoints {
    private String name;
    private long panelOid;
    private long boardNumber;
    private short pointAddress;
    private long type;
    private Short eol;
    private Short state;
    private Short inuse;
    private Board board;
    private Brain brainByObjectId;
    
    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    @Id
    @Column(name = "panel_oid", nullable = false, insertable = false, updatable = false)
    public long getPanelOid() {
        return panelOid;
    }
    
    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }
    
    @Id
    @Column(name = "board_number", nullable = false)
    public long getBoardNumber() {
        return boardNumber;
    }
    
    public void setBoardNumber(long boardNumber) {
        this.boardNumber = boardNumber;
    }
    
    @Id
    @Column(name = "point_address", nullable = false)
    public short getPointAddress() {
        return pointAddress;
    }
    
    public void setPointAddress(short pointAddress) {
        this.pointAddress = pointAddress;
    }
    
    @Basic
    @Column(name = "type", nullable = false)
    public long getType() {
        return type;
    }
    
    public void setType(long type) {
        this.type = type;
    }
    
    @Basic
    @Column(name = "eol", nullable = true)
    public Short getEol() {
        return eol;
    }
    
    public void setEol(Short eol) {
        this.eol = eol;
    }
    
    @Basic
    @Column(name = "state", nullable = true)
    public Short getState() {
        return state;
    }
    
    public void setState(Short state) {
        this.state = state;
    }
    
    @Basic
    @Column(name = "inuse", nullable = true)
    public Short getInuse() {
        return inuse;
    }
    
    public void setInuse(Short inuse) {
        this.inuse = inuse;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        
        ProgIoPoints that = (ProgIoPoints) o;
        
        if (panelOid != that.panelOid) {
            return false;
        }
        if (boardNumber != that.boardNumber) {
            return false;
        }
        if (pointAddress != that.pointAddress) {
            return false;
        }
        if (type != that.type) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (eol != null ? !eol.equals(that.eol) : that.eol != null) {
            return false;
        }
        if (state != null ? !state.equals(that.state) : that.state != null) {
            return false;
        }
        return inuse != null ? inuse.equals(that.inuse) : that.inuse == null;
    }
    
    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (panelOid ^ (panelOid >>> 32));
        result = 31 * result + (int) (boardNumber ^ (boardNumber >>> 32));
        result = 31 * result + (int) pointAddress;
        result = 31 * result + (int) (type ^ (type >>> 32));
        result = 31 * result + (eol != null ? eol.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (inuse != null ? inuse.hashCode() : 0);
        return result;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Board.class)
    @JoinColumns({
                         @JoinColumn(name = "panel_oid", referencedColumnName = "panel_oid", nullable = false, insertable = false, updatable = false),
                         @JoinColumn(name = "board_number", referencedColumnName = "board_number", nullable = false, insertable = false, updatable = false)
                 })
    public Board getBoard() {
        return board;
    }
    
    public void setBoard(Board board) {
        this.board = board;
    }
    
    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "panel_oid", referencedColumnName = "object_id", insertable = false, updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }
    
    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }
}
