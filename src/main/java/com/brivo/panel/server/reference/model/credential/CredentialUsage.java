package com.brivo.panel.server.reference.model.credential;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Date;

public class CredentialUsage {
    private Long credentialId;
    private Long userId;
    private Long deviceId;
    private Date occurred;

    public CredentialUsage(Long credentialId,
                           Long userId,
                           Long deviceId,
                           Date occurred) {
        this.credentialId = credentialId;
        this.userId = userId;
        this.deviceId = deviceId;
        this.occurred = occurred;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Long credentialId) {
        this.credentialId = credentialId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Date getOccurred() {
        return occurred;
    }

    public void setOccurred(Date occurred) {
        this.occurred = occurred;
    }
}
