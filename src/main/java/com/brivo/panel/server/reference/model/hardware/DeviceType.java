package com.brivo.panel.server.reference.model.hardware;

/**
 * See the Device_Type table.
 *
 * @author brandon
 */
public enum DeviceType {
    DOOR_KEYPAD(1),
    AUX_RELAY(2),
    TIMER(3),
    WIEGAND(4),
    SWITCH(5),
    EVENT(6),
    FLOOR(7),
    ELEVATOR(8),
    SALTO_LOCK(9),
    IPAC(10);

    private int typeId;

    DeviceType(int typeId) {
        this.typeId = typeId;
    }

    public int getTypeId() {
        return this.typeId;
    }
}
