package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ElevatorFloorMapDTO {
    private final Long elevatorOid;
    private final Long floorOid;
    private final Long boardNumber;
    private final Short pointAddress;

    @JsonCreator
    public ElevatorFloorMapDTO(@JsonProperty("elevatorOid") final Long elevatorOid,
                               @JsonProperty("floorOid") final Long floorOid,
                               @JsonProperty("boardNumber") final Long boardNumber,
                               @JsonProperty("pointAddress") final Short pointAddress) {
        this.elevatorOid = elevatorOid;
        this.floorOid = floorOid;
        this.boardNumber = boardNumber;
        this.pointAddress = pointAddress;
    }

    public Long getElevatorOid() {
        return elevatorOid;
    }

    public Long getFloorOid() {
        return floorOid;
    }

    public Long getBoardNumber() {
        return boardNumber;
    }

    public Short getPointAddress() {
        return pointAddress;
    }
}
