package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class DeviceStatusEvent extends Event {
    private Long upTime;
    private Long freeDisk;
    private Long usedDisk;
    private Long freeMemory;
    private Long usedMemory;
    private Long loadMinute;
    private Long loadFiveMinutes;
    private Long loadFifteenMinutes;

    @JsonCreator
    public DeviceStatusEvent(@JsonProperty("eventType") final EventType eventType,
                             @JsonProperty("eventTime") final Instant eventTime,
                             @JsonProperty("upTime") final Long upTime,
                             @JsonProperty("freeDisk") final Long freeDisk,
                             @JsonProperty("usedDisk") final Long usedDisk,
                             @JsonProperty("freeMemory") final Long freeMemory,
                             @JsonProperty("usedMemory") final Long usedMemory,
                             @JsonProperty("loadMinute") final Long loadMinute,
                             @JsonProperty("loadFiveMinutes") final Long loadFiveMinutes,
                             @JsonProperty("loadFifteenMinutes") final Long loadFifteenMinutes) {
        super(eventType, eventTime);
        this.upTime = upTime;
        this.freeDisk = freeDisk;
        this.usedDisk = usedDisk;
        this.freeMemory = freeMemory;
        this.usedMemory = usedMemory;
        this.loadMinute = loadMinute;
        this.loadFiveMinutes = loadFiveMinutes;
        this.loadFifteenMinutes = loadFifteenMinutes;
    }

    public Long getUpTime() {
        return upTime;
    }

    public void setUpTime(Long upTime) {
        this.upTime = upTime;
    }

    public Long getFreeDisk() {
        return freeDisk;
    }

    public void setFreeDisk(Long freeDisk) {
        this.freeDisk = freeDisk;
    }

    public Long getUsedDisk() {
        return usedDisk;
    }

    public void setUsedDisk(Long usedDisk) {
        this.usedDisk = usedDisk;
    }

    public Long getFreeMemory() {
        return freeMemory;
    }

    public void setFreeMemory(Long freeMemory) {
        this.freeMemory = freeMemory;
    }

    public Long getUsedMemory() {
        return usedMemory;
    }

    public void setUsedMemory(Long usedMemory) {
        this.usedMemory = usedMemory;
    }

    public Long getLoadMinute() {
        return loadMinute;
    }

    public void setLoadMinute(Long loadMinute) {
        this.loadMinute = loadMinute;
    }

    public Long getLoadFiveMinutes() {
        return loadFiveMinutes;
    }

    public void setLoadFiveMinutes(Long loadFiveMinutes) {
        this.loadFiveMinutes = loadFiveMinutes;
    }

    public Long getLoadFifteenMinutes() {
        return loadFifteenMinutes;
    }

    public void setLoadFifteenMinutes(Long loadFifteenMinutes) {
        this.loadFifteenMinutes = loadFifteenMinutes;
    }
}
