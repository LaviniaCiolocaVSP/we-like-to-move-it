package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.AntipassbackSettings;
import com.brivo.panel.server.reference.model.hardware.Door;
import com.brivo.panel.server.reference.model.hardware.LockOnOpenSettings;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.hardware.TwoFactorCredentialSettings;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Door Extractor.
 *
 * @author brandon
 * @see Gen5DeviceBuilder.progDevicesData().
 * @see https://brivosys.atlassian.net/wiki/display/ws/PanelDataDownstream
 * #PanelDataDownstream-ACS5000Doordevice
 */
public class DoorExtractor implements ResultSetExtractor<List<Door>> {
    private Panel panel;

    public DoorExtractor(Panel panel) {
        this.panel = panel;
    }

    @Override
    public List<Door> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<Door> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceOID = rs.getLong("deviceOID");
            Integer boardAddress = rs.getInt("boardNumber");
            Integer nodeNumber = rs.getInt("nodeNumber");

            Integer useAlarmShuntInt = rs.getInt("useAlarmShunt");
            Boolean useAlarmShunt = false;
            if (useAlarmShuntInt != null && useAlarmShuntInt == 1) {
                useAlarmShunt = true;
            }

            Integer scheduleEnabledInt = rs.getInt("scheduleEnabled");
            Boolean scheduleEnabled = false;
            if (scheduleEnabledInt != null && scheduleEnabledInt == 1) {
                scheduleEnabled = true;
            }

            Integer maxInvalidPin = rs.getInt("maxInvalid");
            Integer invalidShutout = rs.getInt("invalidShutout");
            Integer passthrough = rs.getInt("passthrough");
            Integer alarmDelay = rs.getInt("alarmDelay");
            Integer doorAjar = rs.getInt("doorAjar");
            Integer invalidCredWin = rs.getInt("invalidCredWin");

            Integer notifyOnAjarInt = rs.getInt("notifyOnAjar");
            Boolean notifyOnAjar = false;
            if (notifyOnAjarInt != null && notifyOnAjarInt == 1) {
                notifyOnAjar = true;
            }

            Integer notifyOnForcedInt = rs.getInt("notifyOnForced");
            Boolean notifyOnForced = false;
            if (notifyOnForcedInt != null && notifyOnForcedInt == 1) {
                notifyOnForced = true;
            }

            Integer rexFiresDoorInt = rs.getInt("rexFiresDoor");
            Boolean rexFiresDoor = false;
            if (rexFiresDoorInt != null && rexFiresDoorInt == 1) {
                rexFiresDoor = true;
            }

            Integer maxRexExtension = rs.getInt("maxRexExtension");
            Long twoFactorScheduleId = rs.getLong("twoFactorScheduleID");
            Integer twoFactorInterval = rs.getInt("twoFactorInterval");
            Long cardRequiredScheduleId = rs.getLong("cardRequiredScheduleID");
            Integer antiPassbackZone = rs.getInt("ddazone");
            Integer antiPassbackAltZone = rs.getInt("altZone");
            Integer antiPassbackBoardAddress = rs.getInt("altBoard");
            Integer antiPassbackPointAddress = rs.getInt("altAddress");
            Integer antiPassbackResetInteval = rs.getInt("resetInterval");

            Integer useLockOnOpenInt = rs.getInt("useLockOnOpen");
            Boolean useLockOnOpen = false;
            if (useLockOnOpenInt != null && useLockOnOpenInt == 1) {
                useLockOnOpen = true;
            }

            //added by Laura on 17.07.2019 to support RequestDeviceStatus message for doors
            Integer enableLiveControlInt = rs.getInt("enableLiveControl");

            Integer lockOnOpenDelay = rs.getInt("lockOnOpenDelay");
            Integer debouncePeriod = rs.getInt("debouncePeriod");

            Door door = new Door();
            door.setBoardNumber(boardAddress);
            door.setDeviceId(deviceOID);
            door.setNodeNumber(nodeNumber);
            door.setScheduleEnabled(scheduleEnabled);
            door.setMaxPinAttempts(maxInvalidPin);
            door.setMaxPinAttemptsLockDuration(invalidShutout);
            door.setPassThroughDelay(passthrough);
            door.setUseAlarmShuntRelayBehavior(useAlarmShunt);
            door.setAlarmShuntDelay(alarmDelay);
            door.setDoorAjarDuration(doorAjar);
            door.setInvalidPinResetDuration(invalidCredWin);
            door.setReportDoorAjar(notifyOnAjar);
            door.setReportDoorForced(notifyOnForced);
            door.setRexBehavior(rexFiresDoor);
            door.setMaxRexTriggerExtension(maxRexExtension);

            door.setEnableLiveControl(enableLiveControlInt);

            // Set two factor credential settings.
            TwoFactorCredentialSettings twoFactor = new TwoFactorCredentialSettings();
            twoFactor.setScheduleId(twoFactorScheduleId);
            twoFactor.setInterval(twoFactorInterval);
            door.setTwoFactorCredential(twoFactor);

            door.setCardRequiredScheduleId(cardRequiredScheduleId);

            // Set antipassback settings.
            AntipassbackSettings antipassback = new AntipassbackSettings();
            Boolean inputReaderUsesAntipassback = (antiPassbackZone >= 0);
            Boolean alternateReaderUsesAntipassback = (antiPassbackAltZone >= 0);
            antipassback.setAlternateReaderBoardAddress(antiPassbackBoardAddress);
            antipassback.setAlternateReaderPointAddress(antiPassbackPointAddress);
            antipassback.setInputReaderUsesAntipassback(inputReaderUsesAntipassback);
            antipassback.setAlternateReaderUsesAntipassback(alternateReaderUsesAntipassback);
            antipassback.setInputReaderZone(antiPassbackZone);
            antipassback.setAlternateReaderZone(antiPassbackAltZone);
            antipassback.setResetInterval(antiPassbackResetInteval);
            door.setAntipassback(antipassback);

            door.setDebouncePeriod(debouncePeriod);

            // Set lock on open settings.
            LockOnOpenSettings lockOnOpen = new LockOnOpenSettings();
            lockOnOpen.setUseLockOnOpen(useLockOnOpen);
            lockOnOpen.setDelay(lockOnOpenDelay);
            door.setLockOnOpen(lockOnOpen);

            long unlockScheduleId = 0;
            rs.getLong("lockedDown");
            if (rs.wasNull()) {
                unlockScheduleId = rs.getLong("unlockScheduleId");
            }
            door.setUnlockScheduleId(unlockScheduleId);

            list.add(door);
        }

        return list;
    }

}
