package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "brain_connection_status", schema = "brivo20", catalog = "onair")
public class BrainConnectionStatus {
    private long brainId;
    private String messageQueueName;
    private Timestamp connectTime;
    private long version;
    private String ipAddress;
    private Brain brainByBrainId;

    @Id
    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "message_queue_name", nullable = true, length = 255)
    public String getMessageQueueName() {
        return messageQueueName;
    }

    public void setMessageQueueName(String messageQueueName) {
        this.messageQueueName = messageQueueName;
    }

    @Basic
    @Column(name = "connect_time", nullable = true)
    public Timestamp getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(Timestamp connectTime) {
        this.connectTime = connectTime;
    }

    @Basic
    @Column(name = "version", nullable = false)
    public long getVersion() {
        return version;
    }

    public void setVersion(long version) {
        this.version = version;
    }

    @Basic
    @Column(name = "ip_address", nullable = true, length = 20)
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrainConnectionStatus that = (BrainConnectionStatus) o;

        if (brainId != that.brainId) {
            return false;
        }
        if (version != that.version) {
            return false;
        }
        if (messageQueueName != null ? !messageQueueName.equals(that.messageQueueName) : that.messageQueueName != null) {
            return false;
        }
        if (connectTime != null ? !connectTime.equals(that.connectTime) : that.connectTime != null) {
            return false;
        }
        return ipAddress != null ? ipAddress.equals(that.ipAddress) : that.ipAddress == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainId ^ (brainId >>> 32));
        result = 31 * result + (messageQueueName != null ? messageQueueName.hashCode() : 0);
        result = 31 * result + (connectTime != null ? connectTime.hashCode() : 0);
        result = 31 * result + (int) (version ^ (version >>> 32));
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false)
    public Brain getBrainByBrainId() {
        return brainByBrainId;
    }

    public void setBrainByBrainId(Brain brainByBrainId) {
        this.brainByBrainId = brainByBrainId;
    }
}
