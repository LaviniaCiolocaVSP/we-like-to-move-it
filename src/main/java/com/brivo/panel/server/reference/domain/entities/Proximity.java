package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class Proximity {
    private long proximityId;
    private long proximityTypeId;
    private long accountId;
    private String name;
    private ProximityType proximityTypeByProximityTypeId;
    private Account accountByAccountId;
    private Collection<ProximityProperty> proximityPropertiesByProximityId;
    private Collection<SiteProximity> siteProximitiesByProximityId;

    @Id
    @Column(name = "proximity_id", nullable = false)
    public long getProximityId() {
        return proximityId;
    }

    public void setProximityId(long proximityId) {
        this.proximityId = proximityId;
    }

    @Basic
    @Column(name = "proximity_type_id", nullable = false)
    public long getProximityTypeId() {
        return proximityTypeId;
    }

    public void setProximityTypeId(long proximityTypeId) {
        this.proximityTypeId = proximityTypeId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Proximity proximity = (Proximity) o;

        if (proximityId != proximity.proximityId) {
            return false;
        }
        if (proximityTypeId != proximity.proximityTypeId) {
            return false;
        }
        if (accountId != proximity.accountId) {
            return false;
        }
        return name != null ? name.equals(proximity.name) : proximity.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (proximityId ^ (proximityId >>> 32));
        result = 31 * result + (int) (proximityTypeId ^ (proximityTypeId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "proximity_type_id", referencedColumnName = "proximity_type_id", nullable = false,
            insertable = false, updatable = false)
    public ProximityType getProximityTypeByProximityTypeId() {
        return proximityTypeByProximityTypeId;
    }

    public void setProximityTypeByProximityTypeId(ProximityType proximityTypeByProximityTypeId) {
        this.proximityTypeByProximityTypeId = proximityTypeByProximityTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false,
            updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "proximityByProximityId")
    public Collection<ProximityProperty> getProximityPropertiesByProximityId() {
        return proximityPropertiesByProximityId;
    }

    public void setProximityPropertiesByProximityId(Collection<ProximityProperty> proximityPropertiesByProximityId) {
        this.proximityPropertiesByProximityId = proximityPropertiesByProximityId;
    }

    @OneToMany(mappedBy = "proximityByProximityId")
    public Collection<SiteProximity> getSiteProximitiesByProximityId() {
        return siteProximitiesByProximityId;
    }

    public void setSiteProximitiesByProximityId(Collection<SiteProximity> siteProximitiesByProximityId) {
        this.siteProximitiesByProximityId = siteProximitiesByProximityId;
    }
}
