package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Brain;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface PanelRepository extends CrudRepository<Brain, Long> {

    Optional<Brain> findByElectronicSerialNumber(@Param("serialNumber") final String serialNumber);

    Optional<List<Brain>> findByAccountId(@Param("accountId") long accountId);

    Optional<Brain> findByObjectId(@Param("objectId") long objectId);

    @Query(
            value = "SELECT coalesce(max(b.brain_id),0) FROM brivo20.brain b",
            nativeQuery = true
    )
    Optional<Long> getMaxBrainId();

    @Query(
            value = "select count(*) from brain b where b.account_id = :account_id",
            nativeQuery = true
    )
    long getBrainCountForAccount(@Param("account_id") long accountId);

    @Query(
            value = "select brain_type_id from brain b where b.object_id = :panel_oid",
            nativeQuery = true
    )
    long getBrainTypeIdForPanelOid(@Param("panel_oid") Long panel_oid);

    @Query(
            value = "select firmware_version from brain b where b.object_id = :panel_oid",
            nativeQuery = true
    )
    String getFirmwareVersionByObjectId(@Param("panel_oid") Long panel_oid);

    @Query(
            value = "UPDATE brivo20.brain b " +
                    "SET panel_changed = :now, persons_changed = :now, schedules_changed = :now " +
                    "WHERE brain_id = :brainId",
            nativeQuery = true
    )
    @Modifying
    void updateBrainToFlush(@Param("brainId") Long brainId, @Param("now") LocalDateTime now);

    @Query(
            value = "select min(brain_type_id) from brain b where b.account_id = :accountId",
            nativeQuery = true
    )
    long getMinBrainTypeIdForAccount(@Param("accountId") Long accountId);

    @Query(
            value = "UPDATE brivo20.brain b " +
                    "SET firmware_version = :firmwareVersion " +
                    "WHERE electronic_serial_number = :serialNumber",
            nativeQuery = true
    )
    @Modifying
    void updateFirmwareVersionOnPanel(@Param("serialNumber") String serialNumber,
                                      @Param("firmwareVersion") String firmwareVersion);

    @Query(
            value = "select electronic_serial_number from brain b limit 1",
            nativeQuery = true
    )
    Optional<String> findAValidPanelSerialNumber();

    @Query(
            value = "UPDATE brivo20.brain b " +
                    "SET panel_changed = :currentTime ",
            nativeQuery = true
    )
    @Modifying
    void updatePanelChangedForAllPanels(@Param("currentTime") LocalDateTime currentTime);


}
