package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

@SuppressWarnings({"unused", "unchecked"})
public class HolidayDTO extends AbstractDTO {

    private final Long holidayId;
    private final String name;
    private final Long siteId;
    private final LocalDateTime from;
    private final LocalDateTime to;
    private final Long cadmHolidayId;

    @JsonCreator
    public HolidayDTO(@JsonProperty("id") final Long holidayId, @JsonProperty("name") final String name,
                      @JsonProperty("siteId") final Long siteId, @JsonProperty("from") final LocalDateTime from,
                      @JsonProperty("to") final LocalDateTime to, @JsonProperty("cadmHolidayId") final Long cadmHolidayId) {
        this.holidayId = holidayId;
        this.name = name;
        this.siteId = siteId;
        this.from = from;
        this.to = to;
        this.cadmHolidayId = cadmHolidayId;
    }

    @Override
    public Long getId() {
        return holidayId;
    }

    public String getName() {
        return name;
    }

    public Long getSiteId() {
        return siteId;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public Long getCadmHolidayId() {
        return cadmHolidayId;
    }

}
