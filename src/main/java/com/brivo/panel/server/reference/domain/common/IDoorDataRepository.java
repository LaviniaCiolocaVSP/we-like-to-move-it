package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.DoorData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface IDoorDataRepository extends CrudRepository<DoorData, Long> {

    @Query(
            value = "SELECT dd.* " +
                    "FROM brivo20.door_data dd " +
                    "LEFT JOIN brivo20.device d ON dd.device_oid = d.object_id " +
                    "WHERE d.object_id = :objectId",
            nativeQuery = true
    )
    Optional<DoorData> findByObjectId(@Param("objectId") long objectId);
}