package com.brivo.panel.server.reference.domain.entities;

import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public enum BoardTypes {
    NULL_STATE(-1L, "Unknown Board"),
    DOOR_BOARD(1L, "Door Control Board"),
    IO_BOARD(2L, "I/O Board"),
    EDGE_BOARD(3L, "Edge Board"),
    IPDC1_BOARD(4L, "IPDC 1-Door Board"),
    IPDC2_BOARD(5L, "IPDC 2-Doors Board"),
    SALTO_ROUTER(6L, "Salto Wireless Router"),
    IPAC(7L, "IPAC Board"),
    ALLEGION_GATEWAY(8L, "Allegion Gateway"),
    MR52_BOARD(9L, "MR52 Door Board"),
    EP4502_BOARD(10L, "EP4502 Door Board"),
    EP1502_BOARD(11L, "EP1502 Door Board"),
    ACS300_BOARD(12L, "ACS300 Door Board"),
    ACS6000_BOARD(13L, "ACS6000 Door Board");
    
    private static final Map<Long, BoardTypes> BY_TYPE_ID;
    
    static {
        final Map<Long, BoardTypes> byTypeID = new HashMap<>();
        for (BoardTypes bt : BoardTypes.values()) {
            byTypeID.put(bt.boardTypeId, bt);
        }
        
        BY_TYPE_ID = Collections.unmodifiableMap(byTypeID);
    }
    
    private Long boardTypeId;
    private String description;
    
    BoardTypes(Long boardTypeId) {
        this.boardTypeId = boardTypeId;
    }
    
    BoardTypes(Long boardTypeId, String description) {
        this.description = description;
        this.boardTypeId = boardTypeId;
    }
    
    public static BoardTypes valueOf(Long boardTypeId) {
        final BoardTypes ot = BY_TYPE_ID.get(boardTypeId);
        return ot;
    }
    
    public Long getBoardTypeId() {
        return boardTypeId;
    }
    
    public String getDescription() {
        return description;
    }
    
    public static List<BoardTypes> getAllBoardTypesIds() {
        List<BoardTypes> states = BY_TYPE_ID.entrySet().stream()
                                            .filter(x -> x.getValue() != NULL_STATE)
                                            .map(x -> x.getValue())
                                            .collect(Collectors.toList());
        
        return states;
    }
    
    public static List<BoardTypes> getAvailableTypes(PanelType panelType) {
        List<BoardTypes> boardTypes = new ArrayList<BoardTypes>();
        
        if ((panelType == PanelType.IRIS_5_0) ||
            (panelType == PanelType.IRIS_5_0_CDMA) ||
            (panelType == PanelType.IRIS_5_0_GSM)) {
            boardTypes.add(DOOR_BOARD);
            boardTypes.add(IO_BOARD);
            boardTypes.add(SALTO_ROUTER);
            boardTypes.add(ALLEGION_GATEWAY);
        } else if (panelType == PanelType.ACS6000) {
            boardTypes.add(ACS6000_BOARD);
            boardTypes.add(IO_BOARD);
            boardTypes.add(SALTO_ROUTER);
            boardTypes.add(ALLEGION_GATEWAY);
        } else if (panelType == PanelType.ACS300) {
            boardTypes.add(ALLEGION_GATEWAY);
            boardTypes.add(SALTO_ROUTER);
            boardTypes.add(ACS300_BOARD);
        } else if (panelType.equals(PanelType.EDGE)) {
            boardTypes.add(EDGE_BOARD);
        } else if (panelType.equals(PanelType.IPDC1)) {
            boardTypes.add(IPDC1_BOARD);
        } else if (panelType.equals(PanelType.IPDC2)) {
            boardTypes.add(IPDC2_BOARD);
        } else if ((panelType.equals(PanelType.IPAC_STANDALONE)) ||
                   (panelType.equals(PanelType.IPAC_SERVER_APARATO)) ||
                   (panelType.equals(PanelType.IPAC_CLOUD))) {
            boardTypes.add(IPAC);
        } else if (panelType.equals(PanelType.MERCURY_EP1502) ||
                   panelType.equals(PanelType.MERCURY_EP4502)) {
            boardTypes.add(MR52_BOARD);
        }
        
        return boardTypes;
    }
    
    public static List<BoardTypes> getWirelessBoardTypess() {
        return Lists.newArrayList(BoardTypes.SALTO_ROUTER, BoardTypes.ALLEGION_GATEWAY);
    }
    
    public static List<Long> getWirelessBoardStates() {
        
        return Lists.newArrayList(BoardTypes.SALTO_ROUTER.getBoardTypeId(), BoardTypes.ALLEGION_GATEWAY.getBoardTypeId());
        
    }
    
    public static boolean isWirelessRouter(Long state) {
        if (state != null) {
            return getWirelessBoardStates().contains(state);
        }
        
        return false;
    }
    
    public boolean isWirelessRouter() {
        return isWirelessRouter(this.boardTypeId);
    }
    
    public boolean matchesBoardTypesId(Integer boardTypeId) {
        return Objects.equal(this.boardTypeId, boardTypeId);
    }
    
}
