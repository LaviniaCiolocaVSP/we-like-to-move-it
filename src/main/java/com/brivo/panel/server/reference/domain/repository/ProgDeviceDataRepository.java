package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IProgDeviceDataRepository;
import com.brivo.panel.server.reference.domain.entities.ProgDeviceData;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProgDeviceDataRepository extends IProgDeviceDataRepository {

    @Query(
            value = "DELETE FROM brivo20.prog_device_data pdd " +
                    "WHERE pdd.device_oid = :deviceOid",
            nativeQuery = true
    )
    @Modifying
    void deleteByDeviceOid(@Param("deviceOid") Long deviceOid);

    @Query(
            value = "UPDATE brivo20.prog_device_data " +
                    "SET notify_flag = :notifyFlag, notify_diseng = :notifyDiseng, engage_msg = :engageMsg, disengage_msg = :disengageMsg, " +
                    "door_id = :doorId, event_type = :eventType " +
                    "WHERE device_oid = :deviceOid",
            nativeQuery = true
    )
    @Modifying
    void updateProgDeviceDataByDeviceOid(@Param("deviceOid") Long deviceOid, @Param("notifyFlag") Short notifyFlag,
                                         @Param("notifyDiseng") Short notifyDiseng, @Param("engageMsg") String engageMsg,
                                         @Param("disengageMsg") String disengageMsg, @Param("doorId") Long doorId,
                                         @Param("eventType") Short eventType);
}
