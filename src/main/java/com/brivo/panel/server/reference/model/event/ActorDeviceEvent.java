package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class ActorDeviceEvent extends Event {
    private Long deviceId;
    private Long userId;

    @JsonCreator
    public ActorDeviceEvent(@JsonProperty("eventType") final EventType eventType,
                            @JsonProperty("eventTime") final Instant eventTime,
                            @JsonProperty("deviceId") final Long deviceId,
                            @JsonProperty("userId") final Long userId) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
