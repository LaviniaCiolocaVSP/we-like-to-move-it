package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "brain_ip_address", schema = "brivo20", catalog = "onair")
public class BrainIpAddress {
    private Long brainIpAddressId;
    private Long brainId;
    private String electronicSerialNumber;
    private String ipAddress;
    private Timestamp updateDate;
    private Brain brainByObjectId;

    @Id
    @Column(name = "brain_ip_address_id", nullable = false)
    public Long getBrainIpAddressId() {
        return brainIpAddressId;
    }

    public void setBrainIpAddressId(Long brainIpAddressId) {
        this.brainIpAddressId = brainIpAddressId;
    }

    @Column(name = "brain_id", nullable = false)
    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    @Column(name = "electronic_serial_number", nullable = false)
    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    @Column(name = "ip_address", nullable = false)
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Column(name = "update_date", nullable = false)
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Brain.class)
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false,
            updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainIpAddress that = (BrainIpAddress) o;
        return Objects.equals(brainIpAddressId, that.brainIpAddressId) &&
                Objects.equals(brainId, that.brainId) &&
                Objects.equals(electronicSerialNumber, that.electronicSerialNumber) &&
                Objects.equals(ipAddress, that.ipAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brainIpAddressId, brainId, electronicSerialNumber, ipAddress);
    }

    @Override
    public String toString() {
        return "BrainIpAddress{" +
                "brainIpAddressId=" + brainIpAddressId +
                ", brainId=" + brainId +
                ", electronicSerialNumber='" + electronicSerialNumber + '\'' +
                ", ipAddress='" + ipAddress + '\'' +
                ", updateDate=" + updateDate +
                '}';
    }
}
