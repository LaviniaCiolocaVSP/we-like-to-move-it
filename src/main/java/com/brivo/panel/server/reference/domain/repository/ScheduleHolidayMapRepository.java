package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.ScheduleHolidayMap;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleHolidayMapRepository extends CrudRepository<ScheduleHolidayMap, Long> {
}
