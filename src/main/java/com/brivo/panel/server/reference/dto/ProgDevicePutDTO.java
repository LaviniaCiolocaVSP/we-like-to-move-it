package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProgDevicePutDTO {
    private final Long panelOid;
    private final Long boardNumber;
    private final Long pointAddress;
    private final Long behavior;
    private final Long argument;

    @JsonCreator
    public ProgDevicePutDTO(@JsonProperty("panelOid") final Long panelOid,
                            @JsonProperty("boardNumber") final Long boardNumber,
                            @JsonProperty("pointAddress") final Long pointAddress,
                            @JsonProperty("behavior") final Long behavior,
                            @JsonProperty("argument") final Long argument) {
        this.panelOid = panelOid;
        this.boardNumber = boardNumber;
        this.pointAddress = pointAddress;
        this.behavior = behavior;
        this.argument = argument;
    }

    public Long getPanelOid() {
        return panelOid;
    }

    public Long getBoardNumber() {
        return boardNumber;
    }

    public Long getPointAddress() {
        return pointAddress;
    }

    public Long getBehavior() {
        return behavior;
    }

    public Long getArgument() {
        return argument;
    }
}
