package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "dealer_account_v", schema = "brivo20", catalog = "onair")
public class DealerAccountV {
    private Long dealerAccountId;
    private String dealerName;
    private String dealerDescription;
    private String dealerAccountNumber;
    private Timestamp dealerCreated;
    private Timestamp dealerRegistered;
    private Timestamp dealerUpdated;
    private Timestamp dealerActivated;
    private Timestamp dealerClosed;
    private String dealerAccountStatus;
    private Long dealerOpsFlag;
    private Long customerAccountId;
    private String customerName;
    private Long customerBrandId;
    private String customerDescription;
    private String customerAccountNumber;
    private Timestamp customerCreated;
    private Timestamp customerRegistered;
    private Timestamp customerUpdated;
    private Timestamp customerActivated;
    private Timestamp customerClosed;
    private String customerAccountStatus;
    private String customerDealerRefNum;
    private Long customerOpsFlag;

    @Id
    @Basic
    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Basic
    @Column(name = "dealer_name", nullable = true, length = 32)
    public String getDealerName() {
        return dealerName;
    }

    public void setDealerName(String dealerName) {
        this.dealerName = dealerName;
    }

    @Basic
    @Column(name = "dealer_description", nullable = true, length = 256)
    public String getDealerDescription() {
        return dealerDescription;
    }

    public void setDealerDescription(String dealerDescription) {
        this.dealerDescription = dealerDescription;
    }

    @Basic
    @Column(name = "dealer_account_number", nullable = true, length = 10)
    public String getDealerAccountNumber() {
        return dealerAccountNumber;
    }

    public void setDealerAccountNumber(String dealerAccountNumber) {
        this.dealerAccountNumber = dealerAccountNumber;
    }

    @Basic
    @Column(name = "dealer_created", nullable = true)
    public Timestamp getDealerCreated() {
        return dealerCreated;
    }

    public void setDealerCreated(Timestamp dealerCreated) {
        this.dealerCreated = dealerCreated;
    }

    @Basic
    @Column(name = "dealer_registered", nullable = true)
    public Timestamp getDealerRegistered() {
        return dealerRegistered;
    }

    public void setDealerRegistered(Timestamp dealerRegistered) {
        this.dealerRegistered = dealerRegistered;
    }

    @Basic
    @Column(name = "dealer_updated", nullable = true)
    public Timestamp getDealerUpdated() {
        return dealerUpdated;
    }

    public void setDealerUpdated(Timestamp dealerUpdated) {
        this.dealerUpdated = dealerUpdated;
    }

    @Basic
    @Column(name = "dealer_activated", nullable = true)
    public Timestamp getDealerActivated() {
        return dealerActivated;
    }

    public void setDealerActivated(Timestamp dealerActivated) {
        this.dealerActivated = dealerActivated;
    }

    @Basic
    @Column(name = "dealer_closed", nullable = true)
    public Timestamp getDealerClosed() {
        return dealerClosed;
    }

    public void setDealerClosed(Timestamp dealerClosed) {
        this.dealerClosed = dealerClosed;
    }

    @Basic
    @Column(name = "dealer_account_status", nullable = true, length = 32)
    public String getDealerAccountStatus() {
        return dealerAccountStatus;
    }

    public void setDealerAccountStatus(String dealerAccountStatus) {
        this.dealerAccountStatus = dealerAccountStatus;
    }

    @Basic
    @Column(name = "dealer_ops_flag", nullable = true)
    public Long getDealerOpsFlag() {
        return dealerOpsFlag;
    }

    public void setDealerOpsFlag(Long dealerOpsFlag) {
        this.dealerOpsFlag = dealerOpsFlag;
    }

    @Basic
    @Column(name = "customer_account_id", nullable = true)
    public Long getCustomerAccountId() {
        return customerAccountId;
    }

    public void setCustomerAccountId(Long customerAccountId) {
        this.customerAccountId = customerAccountId;
    }

    @Basic
    @Column(name = "customer_name", nullable = true, length = 32)
    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Basic
    @Column(name = "customer_brand_id", nullable = true)
    public Long getCustomerBrandId() {
        return customerBrandId;
    }

    public void setCustomerBrandId(Long customerBrandId) {
        this.customerBrandId = customerBrandId;
    }

    @Basic
    @Column(name = "customer_description", nullable = true, length = 256)
    public String getCustomerDescription() {
        return customerDescription;
    }

    public void setCustomerDescription(String customerDescription) {
        this.customerDescription = customerDescription;
    }

    @Basic
    @Column(name = "customer_account_number", nullable = true, length = 10)
    public String getCustomerAccountNumber() {
        return customerAccountNumber;
    }

    public void setCustomerAccountNumber(String customerAccountNumber) {
        this.customerAccountNumber = customerAccountNumber;
    }

    @Basic
    @Column(name = "customer_created", nullable = true)
    public Timestamp getCustomerCreated() {
        return customerCreated;
    }

    public void setCustomerCreated(Timestamp customerCreated) {
        this.customerCreated = customerCreated;
    }

    @Basic
    @Column(name = "customer_registered", nullable = true)
    public Timestamp getCustomerRegistered() {
        return customerRegistered;
    }

    public void setCustomerRegistered(Timestamp customerRegistered) {
        this.customerRegistered = customerRegistered;
    }

    @Basic
    @Column(name = "customer_updated", nullable = true)
    public Timestamp getCustomerUpdated() {
        return customerUpdated;
    }

    public void setCustomerUpdated(Timestamp customerUpdated) {
        this.customerUpdated = customerUpdated;
    }

    @Basic
    @Column(name = "customer_activated", nullable = true)
    public Timestamp getCustomerActivated() {
        return customerActivated;
    }

    public void setCustomerActivated(Timestamp customerActivated) {
        this.customerActivated = customerActivated;
    }

    @Basic
    @Column(name = "customer_closed", nullable = true)
    public Timestamp getCustomerClosed() {
        return customerClosed;
    }

    public void setCustomerClosed(Timestamp customerClosed) {
        this.customerClosed = customerClosed;
    }

    @Basic
    @Column(name = "customer_account_status", nullable = true, length = 32)
    public String getCustomerAccountStatus() {
        return customerAccountStatus;
    }

    public void setCustomerAccountStatus(String customerAccountStatus) {
        this.customerAccountStatus = customerAccountStatus;
    }

    @Basic
    @Column(name = "customer_dealer_ref_num", nullable = true, length = 50)
    public String getCustomerDealerRefNum() {
        return customerDealerRefNum;
    }

    public void setCustomerDealerRefNum(String customerDealerRefNum) {
        this.customerDealerRefNum = customerDealerRefNum;
    }

    @Basic
    @Column(name = "customer_ops_flag", nullable = true)
    public Long getCustomerOpsFlag() {
        return customerOpsFlag;
    }

    public void setCustomerOpsFlag(Long customerOpsFlag) {
        this.customerOpsFlag = customerOpsFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DealerAccountV that = (DealerAccountV) o;

        if (dealerAccountId != null ? !dealerAccountId.equals(that.dealerAccountId) : that.dealerAccountId != null) {
            return false;
        }
        if (dealerName != null ? !dealerName.equals(that.dealerName) : that.dealerName != null) {
            return false;
        }
        if (dealerDescription != null ? !dealerDescription.equals(that.dealerDescription) : that.dealerDescription != null) {
            return false;
        }
        if (dealerAccountNumber != null ? !dealerAccountNumber.equals(that.dealerAccountNumber) : that.dealerAccountNumber != null) {
            return false;
        }
        if (dealerCreated != null ? !dealerCreated.equals(that.dealerCreated) : that.dealerCreated != null) {
            return false;
        }
        if (dealerRegistered != null ? !dealerRegistered.equals(that.dealerRegistered) : that.dealerRegistered != null) {
            return false;
        }
        if (dealerUpdated != null ? !dealerUpdated.equals(that.dealerUpdated) : that.dealerUpdated != null) {
            return false;
        }
        if (dealerActivated != null ? !dealerActivated.equals(that.dealerActivated) : that.dealerActivated != null) {
            return false;
        }
        if (dealerClosed != null ? !dealerClosed.equals(that.dealerClosed) : that.dealerClosed != null) {
            return false;
        }
        if (dealerAccountStatus != null ? !dealerAccountStatus.equals(that.dealerAccountStatus) : that.dealerAccountStatus != null) {
            return false;
        }
        if (dealerOpsFlag != null ? !dealerOpsFlag.equals(that.dealerOpsFlag) : that.dealerOpsFlag != null) {
            return false;
        }
        if (customerAccountId != null ? !customerAccountId.equals(that.customerAccountId) : that.customerAccountId != null) {
            return false;
        }
        if (customerName != null ? !customerName.equals(that.customerName) : that.customerName != null) {
            return false;
        }
        if (customerBrandId != null ? !customerBrandId.equals(that.customerBrandId) : that.customerBrandId != null) {
            return false;
        }
        if (customerDescription != null ? !customerDescription.equals(that.customerDescription) : that.customerDescription != null) {
            return false;
        }
        if (customerAccountNumber != null ? !customerAccountNumber.equals(that.customerAccountNumber) : that.customerAccountNumber != null) {
            return false;
        }
        if (customerCreated != null ? !customerCreated.equals(that.customerCreated) : that.customerCreated != null) {
            return false;
        }
        if (customerRegistered != null ? !customerRegistered.equals(that.customerRegistered) : that.customerRegistered != null) {
            return false;
        }
        if (customerUpdated != null ? !customerUpdated.equals(that.customerUpdated) : that.customerUpdated != null) {
            return false;
        }
        if (customerActivated != null ? !customerActivated.equals(that.customerActivated) : that.customerActivated != null) {
            return false;
        }
        if (customerClosed != null ? !customerClosed.equals(that.customerClosed) : that.customerClosed != null) {
            return false;
        }
        if (customerAccountStatus != null ? !customerAccountStatus.equals(that.customerAccountStatus) : that.customerAccountStatus != null) {
            return false;
        }
        if (customerDealerRefNum != null ? !customerDealerRefNum.equals(that.customerDealerRefNum) : that.customerDealerRefNum != null) {
            return false;
        }
        return customerOpsFlag != null ? customerOpsFlag.equals(that.customerOpsFlag) : that.customerOpsFlag == null;
    }

    @Override
    public int hashCode() {
        int result = dealerAccountId != null ? dealerAccountId.hashCode() : 0;
        result = 31 * result + (dealerName != null ? dealerName.hashCode() : 0);
        result = 31 * result + (dealerDescription != null ? dealerDescription.hashCode() : 0);
        result = 31 * result + (dealerAccountNumber != null ? dealerAccountNumber.hashCode() : 0);
        result = 31 * result + (dealerCreated != null ? dealerCreated.hashCode() : 0);
        result = 31 * result + (dealerRegistered != null ? dealerRegistered.hashCode() : 0);
        result = 31 * result + (dealerUpdated != null ? dealerUpdated.hashCode() : 0);
        result = 31 * result + (dealerActivated != null ? dealerActivated.hashCode() : 0);
        result = 31 * result + (dealerClosed != null ? dealerClosed.hashCode() : 0);
        result = 31 * result + (dealerAccountStatus != null ? dealerAccountStatus.hashCode() : 0);
        result = 31 * result + (dealerOpsFlag != null ? dealerOpsFlag.hashCode() : 0);
        result = 31 * result + (customerAccountId != null ? customerAccountId.hashCode() : 0);
        result = 31 * result + (customerName != null ? customerName.hashCode() : 0);
        result = 31 * result + (customerBrandId != null ? customerBrandId.hashCode() : 0);
        result = 31 * result + (customerDescription != null ? customerDescription.hashCode() : 0);
        result = 31 * result + (customerAccountNumber != null ? customerAccountNumber.hashCode() : 0);
        result = 31 * result + (customerCreated != null ? customerCreated.hashCode() : 0);
        result = 31 * result + (customerRegistered != null ? customerRegistered.hashCode() : 0);
        result = 31 * result + (customerUpdated != null ? customerUpdated.hashCode() : 0);
        result = 31 * result + (customerActivated != null ? customerActivated.hashCode() : 0);
        result = 31 * result + (customerClosed != null ? customerClosed.hashCode() : 0);
        result = 31 * result + (customerAccountStatus != null ? customerAccountStatus.hashCode() : 0);
        result = 31 * result + (customerDealerRefNum != null ? customerDealerRefNum.hashCode() : 0);
        result = 31 * result + (customerOpsFlag != null ? customerOpsFlag.hashCode() : 0);
        return result;
    }
}
