package com.brivo.panel.server.reference.config.brivo.accounts;

import com.brivo.panel.server.reference.config.AbstractDataSourceConfig;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@EnableTransactionManagement
@EnableJpaRepositories(
        entityManagerFactoryRef = "brivoAccountsEntityManagerFactory",
        transactionManagerRef = "brivoAccountsTransactionManager",
        basePackages = "com.brivo.panel.server.reference.domain.brivo"
)
public abstract class AbstractBrivoAccountsDataSourceConfig extends AbstractDataSourceConfig {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractBrivoAccountsDataSourceConfig.class);
    public static String DATABASE_CONNECTION_NAME;

    @Value("${brivo.accounts.datasource.url}")
    private String databaseUrl;

    @Value("${brivo.accounts.datasource.username}")
    private String username;

    @Value("${brivo.accounts.datasource.password}")
    private String password;

    @Value("${brivo.accounts.datasource.driverClassName}")
    private String driverClassName;

    @Value("${brivo.accounts.datasource.hibernate.dialect}")
    private String hibernateDialect;

    @Autowired
    private DataSource rpsDataSource;

    @Bean("brivoAccountsEntityManager")
    public EntityManager brivoAccountsEntityManager() {
        return brivoAccountsEntityManagerFactory().createEntityManager();
    }

    @Bean("brivoAccountsEntityManagerFactory")
    public EntityManagerFactory brivoAccountsEntityManagerFactory() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.hibernateDialect", hibernateDialect);

        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(getUsedDataSource());
        emf.setJpaVendorAdapter(jpaVendorAdapter);
        emf.setPackagesToScan(ENTITIES_PACKAGE);
        emf.setPersistenceUnitName("brivoAccountsPersistenceUnit");
        emf.setJpaProperties(properties);
        emf.afterPropertiesSet();

        return emf.getObject();
    }

    private DataSource getUsedDataSource() {
        LOGGER.debug("Verifying the access to the Brivo accounts database ['{}']...", databaseUrl);
        try {
            final DataSource brivoAccountsDataSource = brivoAccountsDataSource();
            LOGGER.debug("The Brivo accounts database ['{}'] is accessible, using it", databaseUrl);

            DATABASE_CONNECTION_NAME = "Brivo DB";

            return brivoAccountsDataSource;
        } catch (final Exception ex) {
            LOGGER.warn("The Brivo accounts database ['{}'] is not accessible, using the RPS database ['{}'] instead",
                    databaseUrl, ((HikariDataSource) rpsDataSource).getJdbcUrl());

            DATABASE_CONNECTION_NAME = "RPS Local DB";

            return rpsDataSource;
        }
    }

    private DataSource brivoAccountsDataSource() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setPoolName("brivo-accounts-panel-server-connection-pool");
        hikariConfig.setMaximumPoolSize(4);
        hikariConfig.setMinimumIdle(0);
        hikariConfig.setConnectionTimeout(10000);   // 10 seconds
        hikariConfig.setIdleTimeout(30_000);        // 30 seconds
        hikariConfig.setMaxLifetime(60_000);        // 1 minute
        hikariConfig.setJdbcUrl(databaseUrl);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);
        hikariConfig.setSchema(DEFAULT_SCHEMA_NAME);
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setConnectionTestQuery("SELECT 1");
        hikariConfig.setInitializationFailTimeout(2);

        return new HikariDataSource(hikariConfig);
    }

    @Bean("brivoAccountsTransactionManager")
    public PlatformTransactionManager brivoAccountsTransactionManager() {
        return new JpaTransactionManager(brivoAccountsEntityManagerFactory());
    }
}
