package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CustomFieldPK implements Serializable {
    private long accountId;
    private long fieldId;

    @Column(name = "account_id", nullable = false)
    @Id
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "field_id", nullable = false)
    @Id
    public long getFieldId() {
        return fieldId;
    }

    public void setFieldId(long fieldId) {
        this.fieldId = fieldId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomFieldPK that = (CustomFieldPK) o;

        if (accountId != that.accountId) {
            return false;
        }
        return fieldId == that.fieldId;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (fieldId ^ (fieldId >>> 32));
        return result;
    }
}
