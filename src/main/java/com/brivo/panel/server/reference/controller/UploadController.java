package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.service.CloudService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class UploadController {

    private final CloudService cloudService;

    @Autowired
    public UploadController(final CloudService cloudService) {
        this.cloudService = cloudService;
    }

    @PostMapping("/post")
    public ResponseEntity<String> uploadFile(@RequestParam("file") MultipartFile file) {
        return ResponseEntity.ok(cloudService.upload(file));
    }
}
