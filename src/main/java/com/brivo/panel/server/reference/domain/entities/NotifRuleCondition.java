package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "notif_rule_condition", schema = "brivo20", catalog = "onair")
public class NotifRuleCondition {
    private long notifRuleObjectId;
    private long securityActionId;
    private Long actorObjectId;
    private Long acteeObjectId;
    private NotifRule notifRuleByNotifRuleObjectId;
    private SecurityAction securityActionBySecurityActionId;
    private BrivoObject objectByActorBrivoObjectId;
    private BrivoObject objectByActeeBrivoObjectId;

    @Id
    @Basic
    @Column(name = "notif_rule_object_id", nullable = false)
    public long getNotifRuleObjectId() {
        return notifRuleObjectId;
    }

    public void setNotifRuleObjectId(long notifRuleObjectId) {
        this.notifRuleObjectId = notifRuleObjectId;
    }

    @Basic
    @Column(name = "security_action_id", nullable = false)
    public long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "actee_object_id", nullable = true)
    public Long getActeeObjectId() {
        return acteeObjectId;
    }

    public void setActeeObjectId(Long acteeObjectId) {
        this.acteeObjectId = acteeObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotifRuleCondition that = (NotifRuleCondition) o;

        if (notifRuleObjectId != that.notifRuleObjectId) {
            return false;
        }
        if (securityActionId != that.securityActionId) {
            return false;
        }
        if (actorObjectId != null ? !actorObjectId.equals(that.actorObjectId) : that.actorObjectId != null) {
            return false;
        }
        return acteeObjectId != null ? acteeObjectId.equals(that.acteeObjectId) : that.acteeObjectId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (notifRuleObjectId ^ (notifRuleObjectId >>> 32));
        result = 31 * result + (int) (securityActionId ^ (securityActionId >>> 32));
        result = 31 * result + (actorObjectId != null ? actorObjectId.hashCode() : 0);
        result = 31 * result + (acteeObjectId != null ? acteeObjectId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "notif_rule_object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public NotifRule getNotifRuleByNotifRuleObjectId() {
        return notifRuleByNotifRuleObjectId;
    }

    public void setNotifRuleByNotifRuleObjectId(NotifRule notifRuleByNotifRuleObjectId) {
        this.notifRuleByNotifRuleObjectId = notifRuleByNotifRuleObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "security_action_id", referencedColumnName = "security_action_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityAction getSecurityActionBySecurityActionId() {
        return securityActionBySecurityActionId;
    }

    public void setSecurityActionBySecurityActionId(SecurityAction securityActionBySecurityActionId) {
        this.securityActionBySecurityActionId = securityActionBySecurityActionId;
    }

    @ManyToOne
    @JoinColumn(name = "actor_object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByActorBrivoObjectId() {
        return objectByActorBrivoObjectId;
    }

    public void setObjectByActorBrivoObjectId(BrivoObject objectByActorBrivoObjectId) {
        this.objectByActorBrivoObjectId = objectByActorBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "actee_object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByActeeBrivoObjectId() {
        return objectByActeeBrivoObjectId;
    }

    public void setObjectByActeeBrivoObjectId(BrivoObject objectByActeeBrivoObjectId) {
        this.objectByActeeBrivoObjectId = objectByActeeBrivoObjectId;
    }
}
