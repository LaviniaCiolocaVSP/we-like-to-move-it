package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.ScheduleType;
import org.springframework.data.repository.CrudRepository;

public interface ScheduleTypeRepository extends CrudRepository<ScheduleType, Long> {

}
