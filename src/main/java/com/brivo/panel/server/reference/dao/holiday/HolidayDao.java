package com.brivo.panel.server.reference.dao.holiday;

import com.brivo.panel.server.reference.dao.BrivoDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class HolidayDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(HolidayDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteHolidayById(Long holidayId) {
        LOGGER.info("Deleting holiday with id {}", holidayId);

        String query = HolidayQuery.DELETE_HOLIDAY.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("holidayId", holidayId);

        jdbcTemplate.update(query, paramMap);
    }

    public void deleteHolidayScheduleMapById(Long holidayId) {
        LOGGER.info("Deleting holiday with id {}", holidayId);

        String query = HolidayQuery.DELETE_SCHEDULE_HOLIDAY_MAP.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("holidayId", holidayId);

        jdbcTemplate.update(query, paramMap);
    }
}
