package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import java.sql.Timestamp;

//@Entity
//@Table(name = "adt_town_lookup_mat_v", schema = "brivo20", catalog = "onair")
public class AdtTownLookupMatV {
    private String physicalAddress;
    private String adtTownCode;
    private String adtCustNumber;
    private String adtJobNumber;
    private Long accountId;
    private Timestamp created;
    private Timestamp updated;

    @Basic
    @Column(name = "physical_address", nullable = true, length = 128)
    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @Basic
    @Column(name = "adt_town_code", nullable = true, length = 3)
    public String getAdtTownCode() {
        return adtTownCode;
    }

    public void setAdtTownCode(String adtTownCode) {
        this.adtTownCode = adtTownCode;
    }

    @Basic
    @Column(name = "adt_cust_number", nullable = true, length = 5)
    public String getAdtCustNumber() {
        return adtCustNumber;
    }

    public void setAdtCustNumber(String adtCustNumber) {
        this.adtCustNumber = adtCustNumber;
    }

    @Basic
    @Column(name = "adt_job_number", nullable = true, length = 4)
    public String getAdtJobNumber() {
        return adtJobNumber;
    }

    public void setAdtJobNumber(String adtJobNumber) {
        this.adtJobNumber = adtJobNumber;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AdtTownLookupMatV that = (AdtTownLookupMatV) o;

        if (physicalAddress != null ? !physicalAddress.equals(that.physicalAddress) : that.physicalAddress != null) {
            return false;
        }
        if (adtTownCode != null ? !adtTownCode.equals(that.adtTownCode) : that.adtTownCode != null) {
            return false;
        }
        if (adtCustNumber != null ? !adtCustNumber.equals(that.adtCustNumber) : that.adtCustNumber != null) {
            return false;
        }
        if (adtJobNumber != null ? !adtJobNumber.equals(that.adtJobNumber) : that.adtJobNumber != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = physicalAddress != null ? physicalAddress.hashCode() : 0;
        result = 31 * result + (adtTownCode != null ? adtTownCode.hashCode() : 0);
        result = 31 * result + (adtCustNumber != null ? adtCustNumber.hashCode() : 0);
        result = 31 * result + (adtJobNumber != null ? adtJobNumber.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }
}
