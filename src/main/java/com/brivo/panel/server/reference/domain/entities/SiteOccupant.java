package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "site_occupant", schema = "brivo20", catalog = "onair")
public class SiteOccupant {
    private long userOid;
    private long siteOid;
    private short isPresent;
    private Timestamp entered;
    private Timestamp exited;
    private long entryDoorOid;
    private Long exitDoorOid;

    @Id
    @Basic
    @Column(name = "user_oid", nullable = false)
    public long getUserOid() {
        return userOid;
    }

    public void setUserOid(long userOid) {
        this.userOid = userOid;
    }

    @Basic
    @Column(name = "site_oid", nullable = false)
    public long getSiteOid() {
        return siteOid;
    }

    public void setSiteOid(long siteOid) {
        this.siteOid = siteOid;
    }

    @Basic
    @Column(name = "is_present", nullable = false)
    public short getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(short isPresent) {
        this.isPresent = isPresent;
    }

    @Basic
    @Column(name = "entered", nullable = false)
    public Timestamp getEntered() {
        return entered;
    }

    public void setEntered(Timestamp entered) {
        this.entered = entered;
    }

    @Basic
    @Column(name = "exited", nullable = true)
    public Timestamp getExited() {
        return exited;
    }

    public void setExited(Timestamp exited) {
        this.exited = exited;
    }

    @Basic
    @Column(name = "entry_door_oid", nullable = false)
    public long getEntryDoorOid() {
        return entryDoorOid;
    }

    public void setEntryDoorOid(long entryDoorOid) {
        this.entryDoorOid = entryDoorOid;
    }

    @Basic
    @Column(name = "exit_door_oid", nullable = true)
    public Long getExitDoorOid() {
        return exitDoorOid;
    }

    public void setExitDoorOid(Long exitDoorOid) {
        this.exitDoorOid = exitDoorOid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SiteOccupant that = (SiteOccupant) o;

        if (userOid != that.userOid) {
            return false;
        }
        if (siteOid != that.siteOid) {
            return false;
        }
        if (isPresent != that.isPresent) {
            return false;
        }
        if (entryDoorOid != that.entryDoorOid) {
            return false;
        }
        if (entered != null ? !entered.equals(that.entered) : that.entered != null) {
            return false;
        }
        if (exited != null ? !exited.equals(that.exited) : that.exited != null) {
            return false;
        }
        return exitDoorOid != null ? exitDoorOid.equals(that.exitDoorOid) : that.exitDoorOid == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userOid ^ (userOid >>> 32));
        result = 31 * result + (int) (siteOid ^ (siteOid >>> 32));
        result = 31 * result + (int) isPresent;
        result = 31 * result + (entered != null ? entered.hashCode() : 0);
        result = 31 * result + (exited != null ? exited.hashCode() : 0);
        result = 31 * result + (int) (entryDoorOid ^ (entryDoorOid >>> 32));
        result = 31 * result + (exitDoorOid != null ? exitDoorOid.hashCode() : 0);
        return result;
    }
}
