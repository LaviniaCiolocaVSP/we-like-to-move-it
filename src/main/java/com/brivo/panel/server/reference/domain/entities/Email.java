package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Email {
    private long emailId;
    private Long ownerObjectId;
    private String value;
    private String type;
    private long notifyBrivoInfo;
    private Timestamp created;
    private Timestamp updated;
    private Long languageId;
    private long bounceCount;
    private Timestamp lastBounce;
    //private BrivoObject objectByOwnerBrivoObjectId;

    @Id
    @Column(name = "email_id", nullable = false)
    public long getEmailId() {
        return emailId;
    }

    public void setEmailId(long emailId) {
        this.emailId = emailId;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = true)
    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Basic
    @Column(name = "value", nullable = true, length = 512)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "type", nullable = true, length = 32)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "notify_brivo_info", nullable = false)
    public long getNotifyBrivoInfo() {
        return notifyBrivoInfo;
    }

    public void setNotifyBrivoInfo(long notifyBrivoInfo) {
        this.notifyBrivoInfo = notifyBrivoInfo;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "language_id", nullable = true)
    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    @Basic
    @Column(name = "bounce_count", nullable = false)
    public long getBounceCount() {
        return bounceCount;
    }

    public void setBounceCount(long bounceCount) {
        this.bounceCount = bounceCount;
    }

    @Basic
    @Column(name = "last_bounce", nullable = true)
    public Timestamp getLastBounce() {
        return lastBounce;
    }

    public void setLastBounce(Timestamp lastBounce) {
        this.lastBounce = lastBounce;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Email email = (Email) o;

        if (emailId != email.emailId) {
            return false;
        }
        if (notifyBrivoInfo != email.notifyBrivoInfo) {
            return false;
        }
        if (bounceCount != email.bounceCount) {
            return false;
        }
        if (ownerObjectId != null ? !ownerObjectId.equals(email.ownerObjectId) : email.ownerObjectId != null) {
            return false;
        }
        if (value != null ? !value.equals(email.value) : email.value != null) {
            return false;
        }
        if (type != null ? !type.equals(email.type) : email.type != null) {
            return false;
        }
        if (created != null ? !created.equals(email.created) : email.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(email.updated) : email.updated != null) {
            return false;
        }
        if (languageId != null ? !languageId.equals(email.languageId) : email.languageId != null) {
            return false;
        }
        return lastBounce != null ? lastBounce.equals(email.lastBounce) : email.lastBounce == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (emailId ^ (emailId >>> 32));
        result = 31 * result + (ownerObjectId != null ? ownerObjectId.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (int) (notifyBrivoInfo ^ (notifyBrivoInfo >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (languageId != null ? languageId.hashCode() : 0);
        result = 31 * result + (int) (bounceCount ^ (bounceCount >>> 32));
        result = 31 * result + (lastBounce != null ? lastBounce.hashCode() : 0);
        return result;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id")
    public BrivoObject getObjectByOwnerBrivoObjectId() {
        return objectByOwnerBrivoObjectId;
    }

    public void setObjectByOwnerBrivoObjectId(BrivoObject objectByOwnerBrivoObjectId) {
        this.objectByOwnerBrivoObjectId = objectByOwnerBrivoObjectId;
    }
    */
}
