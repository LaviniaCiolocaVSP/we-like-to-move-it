package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CustomFieldDefinitionPK implements Serializable {
    private long customFieldId;
    private long accountId;

    @Column(name = "custom_field_id", nullable = false)
    @Id
    public long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Column(name = "account_id", nullable = false)
    @Id
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomFieldDefinitionPK that = (CustomFieldDefinitionPK) o;

        if (customFieldId != that.customFieldId) {
            return false;
        }
        return accountId == that.accountId;
    }

    @Override
    public int hashCode() {
        int result = (int) (customFieldId ^ (customFieldId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        return result;
    }
}
