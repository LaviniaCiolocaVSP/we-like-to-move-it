package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class PanelEventData {
    private List<ActorDeviceEvent> actorDeviceEvents;
    private List<AlarmEvent> alarmEvents;
    private List<BoardEvent> boardEvents;
    private List<DeviceEvent> deviceEvents;
    private List<DeviceStatusEvent> deviceStatusEvents;
    private List<CredentialEvent> credentialEvents;
    private List<PanelReportEvent> panelReportEvents;
    private List<SaltoNodeEvent> saltoNodeEvents;
    private List<SaltoRouterHelloEvent> saltoRouterHelloEvents;
    private List<ActorScheduleEvent> actorScheduleEvents;
    private List<StartupEvent> startUpEvents;
    private List<WireEvent> wireEvents;
    private List<DeviceActionEvent> deviceActionEvents;
    private List<EntranceEvent> entranceEvents;
    private List<UnauthorizedIpAccessEvent> unauthorizedIpAccessEvents;

    @JsonCreator
    public PanelEventData(@JsonProperty("ActorDeviceEvents") final List<ActorDeviceEvent> actorDeviceEvents,
                          @JsonProperty("AlarmEvents") final List<AlarmEvent> alarmEvents,
                          @JsonProperty("BoardEvents") final List<BoardEvent> boardEvents,
                          @JsonProperty("DeviceEvents") final List<DeviceEvent> deviceEvents,
                          @JsonProperty("DeviceStatusEvents") final List<DeviceStatusEvent> deviceStatusEvents,
                          @JsonProperty("CredentialEvents") final List<CredentialEvent> credentialEvents,
                          @JsonProperty("PanelReportEvents") final List<PanelReportEvent> panelReportEvents,
                          @JsonProperty("SaltoNodeEvents") final List<SaltoNodeEvent> saltoNodeEvents,
                          @JsonProperty("SaltoRouterHelloEvents") final List<SaltoRouterHelloEvent> saltoRouterHelloEvents,
                          @JsonProperty("ActorScheduleEvents") final List<ActorScheduleEvent> actorScheduleEvents,
                          @JsonProperty("StartUpEvents") final List<StartupEvent> startUpEvents,
                          @JsonProperty("WireEvents") final List<WireEvent> wireEvents,
                          @JsonProperty("DeviceActionEvents") final List<DeviceActionEvent> deviceActionEvents,
                          @JsonProperty("EntranceEvents") final List<EntranceEvent> entranceEvents,
                          @JsonProperty("UnauthorizedIpAccessEvents") final List<UnauthorizedIpAccessEvent> unauthorizedIpAccessEvents) {
        this.actorDeviceEvents = actorDeviceEvents;
        this.alarmEvents = alarmEvents;
        this.boardEvents = boardEvents;
        this.deviceEvents = deviceEvents;
        this.deviceStatusEvents = deviceStatusEvents;
        this.credentialEvents = credentialEvents;
        this.panelReportEvents = panelReportEvents;
        this.saltoNodeEvents = saltoNodeEvents;
        this.saltoRouterHelloEvents = saltoRouterHelloEvents;
        this.actorScheduleEvents = actorScheduleEvents;
        this.startUpEvents = startUpEvents;
        this.wireEvents = wireEvents;
        this.deviceActionEvents = deviceActionEvents;
        this.entranceEvents = entranceEvents;
        this.unauthorizedIpAccessEvents = unauthorizedIpAccessEvents;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public List<ActorDeviceEvent> getActorDeviceEvents() {
        return actorDeviceEvents;
    }

    public void setActorDeviceEvents(List<ActorDeviceEvent> actorDeviceEvents) {
        this.actorDeviceEvents = actorDeviceEvents;
    }

    public List<AlarmEvent> getAlarmEvents() {
        return alarmEvents;
    }

    public void setAlarmEvents(List<AlarmEvent> alarmEvents) {
        this.alarmEvents = alarmEvents;
    }

    public List<BoardEvent> getBoardEvents() {
        return boardEvents;
    }

    public void setBoardEvents(List<BoardEvent> boardEvents) {
        this.boardEvents = boardEvents;
    }

    public List<DeviceEvent> getDeviceEvents() {
        return deviceEvents;
    }

    public void setDeviceEvents(List<DeviceEvent> deviceEvents) {
        this.deviceEvents = deviceEvents;
    }

    public List<DeviceStatusEvent> getDeviceStatusEvents() {
        return deviceStatusEvents;
    }

    public void setDeviceStatusEvents(List<DeviceStatusEvent> deviceStatusEvents) {
        this.deviceStatusEvents = deviceStatusEvents;
    }

    public List<CredentialEvent> getCredentialEvents() {
        return credentialEvents;
    }

    public void setCredentialEvents(
            List<CredentialEvent> invalidCredentialEvents) {
        this.credentialEvents = invalidCredentialEvents;
    }

    public List<PanelReportEvent> getPanelReportEvents() {
        return panelReportEvents;
    }

    public void setPanelReportEvents(List<PanelReportEvent> panelReportEvents) {
        this.panelReportEvents = panelReportEvents;
    }

    public List<SaltoNodeEvent> getSaltoNodeEvents() {
        return saltoNodeEvents;
    }

    public void setSaltoNodeEvents(List<SaltoNodeEvent> saltoNodeEvents) {
        this.saltoNodeEvents = saltoNodeEvents;
    }

    public List<SaltoRouterHelloEvent> getSaltoRouterHelloEvents() {
        return saltoRouterHelloEvents;
    }

    public void setSaltoRouterHelloEvents(
            List<SaltoRouterHelloEvent> saltoRouterHelloEvents) {
        this.saltoRouterHelloEvents = saltoRouterHelloEvents;
    }

    public List<ActorScheduleEvent> getActorScheduleEvents() {
        return actorScheduleEvents;
    }

    public void setActorScheduleEvents(
            List<ActorScheduleEvent> scheduleActivatedEvents) {
        this.actorScheduleEvents = scheduleActivatedEvents;
    }


    public List<StartupEvent> getStartUpEvents() {
        return startUpEvents;
    }

    public void setStartUpEvents(List<StartupEvent> startUpEvents) {
        this.startUpEvents = startUpEvents;
    }

    public List<WireEvent> getWireEvents() {
        return wireEvents;
    }

    public void setWireEvents(List<WireEvent> wireEvents) {
        this.wireEvents = wireEvents;
    }

    public List<DeviceActionEvent> getDeviceActionEvents() {
        return deviceActionEvents;
    }

    public void setDeviceActionEvents(List<DeviceActionEvent> deviceActionEvents) {
        this.deviceActionEvents = deviceActionEvents;
    }

    public List<EntranceEvent> getEntranceEvents() {
        return entranceEvents;
    }

    public void setEntranceEvents(List<EntranceEvent> entranceEvents) {
        this.entranceEvents = entranceEvents;
    }

    public List<UnauthorizedIpAccessEvent> getUnauthorizedIpAccessEvents() {
        return unauthorizedIpAccessEvents;
    }

    public void setUnauthorizedIpAccessEvents(List<UnauthorizedIpAccessEvent> unauthorizedIpAccessEvents) {
        this.unauthorizedIpAccessEvents = unauthorizedIpAccessEvents;
    }

}
