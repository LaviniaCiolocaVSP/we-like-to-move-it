package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "credential_field_type", schema = "brivo20", catalog = "onair")
public class CredentialFieldType {
    private long credentialFieldTypeId;
    private String name;
    private Collection<CredentialField> credentialFieldsByCredentialFieldTypeId;

    @Id
    @Column(name = "credential_field_type_id", nullable = false)
    public long getCredentialFieldTypeId() {
        return credentialFieldTypeId;
    }

    public void setCredentialFieldTypeId(long credentialFieldTypeId) {
        this.credentialFieldTypeId = credentialFieldTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialFieldType that = (CredentialFieldType) o;

        if (credentialFieldTypeId != that.credentialFieldTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (credentialFieldTypeId ^ (credentialFieldTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "credentialFieldTypeByCredentialFieldTypeId")
    public Collection<CredentialField> getCredentialFieldsByCredentialFieldTypeId() {
        return credentialFieldsByCredentialFieldTypeId;
    }

    public void setCredentialFieldsByCredentialFieldTypeId(Collection<CredentialField> credentialFieldsByCredentialFieldTypeId) {
        this.credentialFieldsByCredentialFieldTypeId = credentialFieldsByCredentialFieldTypeId;
    }
}
