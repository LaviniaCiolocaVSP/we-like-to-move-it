package com.brivo.panel.server.reference.dao.site;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum SiteQuery implements FileQuery {
    SELECT_SITE_FOR_BOARD,
    SELECT_SITE_FOR_DEVICE,
    SELECT_SITE_FOR_PANEL;

    private String query;

    SiteQuery() {
        this.query = QueryUtils.getQueryText(SiteQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
