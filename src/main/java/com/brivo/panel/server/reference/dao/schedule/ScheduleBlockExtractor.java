package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.model.schedule.ScheduleBlock;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Schedule Blook Extractor.
 *
 * @author brandon
 */
@Component
public class ScheduleBlockExtractor
        implements ResultSetExtractor<List<ScheduleBlock>> {
    @Override
    public List<ScheduleBlock> extractData(ResultSet rs)
            throws SQLException, DataAccessException {
        List<ScheduleBlock> list = new ArrayList<>();

        while (rs.next()) {
            ScheduleBlock block = new ScheduleBlock();

            Integer start = rs.getInt("startTime");
            Integer stop = rs.getInt("stopTime");

            block.setStartMinute(start);
            block.setEndMinute(stop);

            list.add(block);
        }

        return list;
    }
}
