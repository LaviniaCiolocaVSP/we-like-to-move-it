package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DoorDataDTO {

    private final Long board;
    private final Long node;
    private final Long osdpPdAddress;
    private final Short doorAjarEnabled;
    private final Long doorAjarThreshold;
    private final Long invalidPINsThreshold;
    private final Long invalidPINsShutdown;
    private final Long passThroughPeriod;
    private final Long debouncePeriod;
    private final Short useLockOnOpen;
    private final Long lockOnOpenDelay;
    private final Short useRequestToExit;
    private final Short rexUnlock;
    private final Short useAlarmShunt;
    private final Long alarmDelay;
    private final Short scheduleEnabled;
    private final Short cacheCredLimitTime;
    private final Short enablePrivacyMode;
    private final Long invalidCredWin;

    @JsonCreator
    public DoorDataDTO(@JsonProperty("board") final Long board, @JsonProperty("node") final Long node,
                       @JsonProperty("osdpPdAddress") final Long osdpPdAddress,
                       @JsonProperty("doorAjarEnabled") final Short doorAjarEnabled,
                       @JsonProperty("doorAjarThreshold") final Long doorAjarThreshold,
                       @JsonProperty("invalidPINsThreshold") final Long invalidPINsThreshold,
                       @JsonProperty("invalidPINsShutdown") final Long invalidPINsShutdown,
                       @JsonProperty("passThroughPeriod") final Long passThroughPeriod,
                       @JsonProperty("debouncePeriod") final Long debouncePeriod,
                       @JsonProperty("useLockOnOpen") final Short useLockOnOpen,
                       @JsonProperty("lockOnOpenDelay") final Long lockOnOpenDelay,
                       @JsonProperty("useRequestToExit") final Short useRequestToExit,
                       @JsonProperty("rexUnlock") final Short rexUnlock,
                       @JsonProperty("useAlarmShunt") final Short useAlarmShunt,
                       @JsonProperty("alarmDelay") final Long alarmDelay,
                       @JsonProperty("scheduleEnabled") final Short scheduleEnabled,
                       @JsonProperty("cacheCredLimitTime") final Short cacheCredLimitTime,
                       @JsonProperty("enablePrivacyMode") final Short enablePrivacyMode,
                       @JsonProperty("invalidCredWin") final Long invalidCredWin) {
        this.board = board;
        this.node = node;
        this.osdpPdAddress = osdpPdAddress;
        this.doorAjarEnabled = doorAjarEnabled;
        this.doorAjarThreshold = doorAjarThreshold;
        this.invalidPINsThreshold = invalidPINsThreshold;
        this.invalidPINsShutdown = invalidPINsShutdown;
        this.passThroughPeriod = passThroughPeriod;
        this.debouncePeriod = debouncePeriod;
        this.useLockOnOpen = useLockOnOpen;
        this.lockOnOpenDelay = lockOnOpenDelay;
        this.useRequestToExit = useRequestToExit;
        this.rexUnlock = rexUnlock;
        this.useAlarmShunt = useAlarmShunt;
        this.alarmDelay = alarmDelay;
        this.scheduleEnabled = scheduleEnabled;
        this.cacheCredLimitTime = cacheCredLimitTime;
        this.enablePrivacyMode = enablePrivacyMode;
        this.invalidCredWin = invalidCredWin;
    }

    public Long getBoard() {
        return board;
    }

    public Long getNode() {
        return node;
    }

    public Long getOsdpPdAddress() {
        return osdpPdAddress;
    }

    public Short getDoorAjarEnabled() {
        return doorAjarEnabled;
    }

    public Long getDoorAjarThreshold() {
        return doorAjarThreshold;
    }

    public Long getInvalidPINsThreshold() {
        return invalidPINsThreshold;
    }

    public Long getInvalidPINsShutdown() {
        return invalidPINsShutdown;
    }

    public Long getPassThroughPeriod() {
        return passThroughPeriod;
    }

    public Long getDebouncePeriod() {
        return debouncePeriod;
    }

    public Short getUseLockOnOpen() {
        return useLockOnOpen;
    }

    public Short getUseRequestToExit() {
        return useRequestToExit;
    }

    public Short getRexUnlock() {
        return rexUnlock;
    }

    public Short getUseAlarmShunt() {
        return useAlarmShunt;
    }

    public Long getAlarmDelay() {
        return alarmDelay;
    }

    public Long getLockOnOpenDelay() {
        return lockOnOpenDelay;
    }

    public Short getScheduleEnabled() {
        return scheduleEnabled;
    }

    public Short getCacheCredLimitTime() {
        return cacheCredLimitTime;
    }

    public Short getEnablePrivacyMode() {
        return enablePrivacyMode;
    }

    public Long getInvalidCredWin() {
        return invalidCredWin;
    }
}
