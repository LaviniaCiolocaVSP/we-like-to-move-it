package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Events;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EventRepository extends CrudRepository<Events, Long> {

    @Query(
            value = "SELECT * FROM events e ORDER BY e.event_time DESC",
            nativeQuery = true
    )
    List<Events> findAllEventsInDescendingOrder();
}
