package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import com.brivo.panel.server.reference.dao.event.PostEventResolutionDao;
import com.brivo.panel.server.reference.service.CredentialService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UnknownCredentialEventDataProcessor implements EventDataProcessor {
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private EventResolutionDao eventResolutionDao;

    @Autowired
    private PostEventResolutionDao postEventResolutionDao;

    @Autowired
    private ActorEventDataProcessor actorEventDataProcessor;

    @Autowired
    private DeviceEventDataProcessor deviceEventDataProcessor;

    @Autowired
    private CredentialService credentialService;

    /*@Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveUnknownCredentialEvent ");

        Boolean result = false;
        Boolean unknownCredential = false;
        
        EventResolutionData resolutionData = null;
        if(EventType.UNKNOWN_CARD_CREDENTIAL.equals(eventType))
        {
            resolutionData = resolveUnknownCard(event, panel);
        }
        else if(EventType.UNKNOWN_PIN_CREDENTIAL.equals(eventType) || EventType.INVALID_CREDENTIAL_TYPE.equals(eventType))
        {
            resolutionData = resolveUnknownPin(event, panel);
        }

        // resolutionData is populated if it previously assigned
        // if it was not previously assigned put it in the unknown_card_log table.
        // and set unknownCredential so it will be resolved as a device event
        if(resolutionData == null)
        {
            if(EventType.UNKNOWN_CARD_CREDENTIAL.equals(eventType)){
                postEventResolutionDao.updateUnknownCredential(event);
                logger.trace("Could not find credential. Updated unknown card log.");
            }
            unknownCredential = true;
            logger.trace("<<< ResolveUnknownCredentialEvent");
            event.getEventData().setCredentialLabel(event.getEventData().getUnknownWeigand());
        } else {
            event.getEventData().setCredentialLabel(resolutionData.getReferenceId());
        }
       
        if(EventType.UNKNOWN_CARD_CREDENTIAL.equals(eventType))
        {
            event.getEventData().setDetail(
                    String.format("messages.Card_Hex:%s", event.getEventData().getUnknownWeigand()
                                                          + ":" +  event.getEventData().getCredentialBitLength()));
            event.setActorObjectId(0l);
        }
        else if(EventType.UNKNOWN_PIN_CREDENTIAL.equals(eventType))
        {
            event.getEventData().setDetail(
                    String.format("messages.PIN:%s", event.getEventData().getUnknownWeigand()));
            event.getEventData().setCredentialLabel(event.getEventData().getUnknownWeigand());
            event.setActorObjectId(0l);
        }
        

        // Found the credential, so see why it was rejected.
        if(resolutionData != null && (resolutionData.getOwnerId().longValue() == 0l || (resolutionData.getOwnerId() == null)) )
        {
            handleCredentialWithNoOwner(resolutionData, event);
        }
        else if (resolutionData != null)
        {
            handleCredentialWithOwner(resolutionData, event);
        }
        
        if(EventType.INVALID_CREDENTIAL_TYPE.equals(eventType))
        {
            if(event.getActorObjectId() != 0)
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_INVALID_CREDENTIAL_TYPE.getSecurityActionValue());  
            }
            else
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_INVALID_CREDENTIAL_TYPE_UNKNOWN_USER.getSecurityActionValue());
            }
        }
        
        if(unknownCredential == true  || (resolutionData.getOwnerId() == 0 && resolutionData.getLastOwnerId() == 0))
        {
            result = deviceEventDataProcessor.resolveEvent(eventType, event, panel);
        }
        
        else if (resolutionData != null)
        {
            result = actorEventDataProcessor.resolveEvent(eventType, event, panel);
        }

        logger.trace("<<< ResolveUnknownCredentialEvent ");

        return result;
    }

    *//**
     * Handle Unknown Card.
     *
     * This is CredentialSupport.resolveCredential
     * running the CREDENTIAL_BY_VALUE query.
     *//*
    EventResolutionData resolveUnknownCard(SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> HandleUnknownCard");

        if((event.getEventData().getUnknownWeigand() == null) ||
           (event.getEventData().getCredentialBitLength() == null))
        {
            logger.debug("Missing required card value or bit length.");
            logger.trace("<<< HandleUnknownCard");
            return null;
        }

        EventResolutionData result = null;
        List<EventResolutionData> dataList = null;

        dataList = this.eventResolutionDao.getCredentialByValueEvent(
                event.getEventData().getUnknownWeigand(),
                event.getEventData().getCredentialBitLength(),
                panel);

        if(dataList.size() == 0)
        {
            // If we can't find the card by it's value, try masking it.
            dataList = this.resolveMaskedUnknownCredential(event, panel);
        }

        if(dataList.size() == 1)
        {
            result = dataList.get(0);
        }

        logger.trace("<<< HandleUnknownCard");

        return result;
    }

    *//**
     * Handle Unknown Pin.
     *//*
    EventResolutionData resolveUnknownPin(SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> HandleUnknownPin");

        if(event.getEventData().getUnknownWeigand() == null)
        {
            logger.debug("Missing required pin value.");
            logger.trace("<<< HandleUnknownPin");
            return null;
        }

        EventResolutionData result = null;
        List<EventResolutionData> dataList = null;
        dataList = this.eventResolutionDao.getPinByValueEvent(event, panel);

        if(dataList.size() == 1)
        {
            result = dataList.get(0);
        }

        logger.trace("<<< HandleUnknownPin");

        return result;
    }

    *//**
     * Handle Credential with No Owner.
     *//*
    void handleCredentialWithNoOwner(EventResolutionData data, SecurityLogEvent event)
    {
        logger.trace(">>> HandleCredentialWithNoOwner");

        if(data.getLastOwnerId() == 0) // No last owner Id for credential.
        {
            event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_UNASSIGNED.getSecurityActionValue());
        }
        else
        {
            User user = eventResolutionDao.getUserByObjectId(data.getLastOwnerId());

            boolean disabled = (user != null) && (user.getDisabled() != null) && (user.getDisabled());

            if(disabled)
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_DELETED_USER
                                                         .getSecurityActionValue());
            }
            else if(user.getDeactivated()!= null)
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_DEACTIVATED.getSecurityActionValue());
            }
            else
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_OLD_CRED.getSecurityActionValue());
            }

            event.setActorObjectId(data.getLastOwnerId());
        }

        logger.trace("<<< HandleCredentialWithNoOwner");
    }

    *//**
     * Handle Credential with Owner.
     *//*
    void handleCredentialWithOwner(EventResolutionData data, SecurityLogEvent event)
    {
        logger.trace(">>> HandleCredentialWithOwner");

        User user = eventResolutionDao.getUserByObjectId(data.getOwnerId());
        boolean userDisabled = (user != null) && (user.getDisabled() != null) && (user.getDisabled());
        boolean userDeactivated = (user != null) && (user.getDeactivated() != null) &&(user.getDeactivated().isBefore(Instant.now()));

        event.setActorObjectId(data.getOwnerId());

        if(data.getLastOwnerId() != 0 && data.getDisabled())
        {
            event.setActorObjectId(data.getLastOwnerId());

            if(userDisabled)
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_DELETED_USER
                                                         .getSecurityActionValue());
            }
            else
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_OLD_CRED.getSecurityActionValue());
            }
        }
        else if(userDeactivated)
        {
            event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_DEACTIVATED.getSecurityActionValue());
        }
        else if (!data.getDisabled() &&
                 ((data.getEnableOn() != null && event.getOccurred().toInstant().isBefore(data.getEnableOn()))||
                  (data.getExpires() != null && event.getOccurred().toInstant().isAfter(data.getExpires()))))
        {
            event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_EXPIRED.getSecurityActionValue());
        }
        else
        {
            int permCount =
                    this.eventResolutionDao.getPermissionCount(
                            event.getObjectId(), event.getActorObjectId());
            if(permCount == 0)
            {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_WRONG_DOOR.getSecurityActionValue());
            } else {
                event.setSecurityActionId((long) SecurityAction.ACTION_ACCESS_FAIL_AUTHORIZATION_PENDING
                                                         .getSecurityActionValue());
            }
        }

        logger.trace("<<< HandleCredentialWithOwner");
    }

    *//**
     * Resolve Masked Unknown Credential.
     *
     * @see UnknownCredentialProcessor.process()
     * @see CredentialSupport.resolveCredential()
     *//*
    List<EventResolutionData> resolveMaskedUnknownCredential(SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveMaskedUnknownCredential");

        List<EventResolutionData> list = Collections.emptyList();

        // Try the 255 facility code mask.
        if(event.getEventData().getCredentialBitLength() == 26)
        {
            String unknownWeigandStringValue = event.getEventData().getUnknownWeigand();
            long rawValue = 0;
            if (StringUtils.isNotBlank(unknownWeigandStringValue))
            {
                rawValue = Long.parseLong(unknownWeigandStringValue, 16);
            }

            rawValue &= 0x1FFFF; // Mask the value
            rawValue |= 0x1FE0000; // Attach FC 255
            String maskedCredentialValue = Long.toHexString(rawValue);

            list = this.eventResolutionDao.getCredentialByValue255MaskEvent(maskedCredentialValue, panel);
        }

        // Try any custom masks we have stored.
        List<CardMask> masks = credentialService.getCardMasks(event.getEventData().getCredentialBitLength());
        for(CardMask mask : masks)
        {
            BigInteger maskValue = new BigInteger(mask.getMask(), 16);
            BigInteger credentialValue =
                    new BigInteger(event.getEventData().getUnknownWeigand(), 16);
            String maskedCredentialValue = maskValue.and(credentialValue).toString(16);

            list = this.eventResolutionDao.getCredentialByValueEvent(
                    maskedCredentialValue, event.getEventData().getCredentialBitLength(), panel);
        }

        logger.trace("<<< ResolveMaskedUnknownCredential");

        return list;
    }*/

}
