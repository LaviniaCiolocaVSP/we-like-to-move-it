package com.brivo.panel.server.reference.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class LoggingFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggingFilter.class);


    @Override
    protected void doFilterInternal(final HttpServletRequest request, final HttpServletResponse response,
                                    final FilterChain filterChain) throws ServletException, IOException {
        /*
        final Enumeration<String> headerNames = request.getHeaderNames();
        LOGGER.debug("------------------------- Headers ----------------------------------");
        LOGGER.debug("The request contains the following headers:");
        while (headerNames.hasMoreElements()) {
            final String headerName = headerNames.nextElement();
            LOGGER.debug("\t'{}' - '{}'", headerName, request.getHeader(headerName));
        }
        LOGGER.debug("------------------------- End headers ------------------------------");
        */

        filterChain.doFilter(request, response);
    }
}
