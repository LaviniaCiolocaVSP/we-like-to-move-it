package com.brivo.panel.server.reference.dao;

public interface FileQuery {
    String getQuery();
}
