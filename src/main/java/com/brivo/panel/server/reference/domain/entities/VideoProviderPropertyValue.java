package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "video_provider_property_value", schema = "brivo20", catalog = "onair")
public class VideoProviderPropertyValue {
    private long videoProvPropValueId;
    private String value;
    private long videoProvTypePropId;
    private long videoProviderId;
    private VideoProviderTypeProperty videoProviderTypePropertyByVideoProvTypePropId;
    private VideoProvider videoProviderByVideoProviderId;

    @Id
    @Column(name = "video_prov_prop_value_id", nullable = false)
    public long getVideoProvPropValueId() {
        return videoProvPropValueId;
    }

    public void setVideoProvPropValueId(long videoProvPropValueId) {
        this.videoProvPropValueId = videoProvPropValueId;
    }

    @Basic
    @Column(name = "value", nullable = true, length = 512)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "video_prov_type_prop_id", nullable = false)
    public long getVideoProvTypePropId() {
        return videoProvTypePropId;
    }

    public void setVideoProvTypePropId(long videoProvTypePropId) {
        this.videoProvTypePropId = videoProvTypePropId;
    }

    @Basic
    @Column(name = "video_provider_id", nullable = false)
    public long getVideoProviderId() {
        return videoProviderId;
    }

    public void setVideoProviderId(long videoProviderId) {
        this.videoProviderId = videoProviderId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoProviderPropertyValue that = (VideoProviderPropertyValue) o;

        if (videoProvPropValueId != that.videoProvPropValueId) {
            return false;
        }
        if (videoProvTypePropId != that.videoProvTypePropId) {
            return false;
        }
        if (videoProviderId != that.videoProviderId) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoProvPropValueId ^ (videoProvPropValueId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (int) (videoProvTypePropId ^ (videoProvTypePropId >>> 32));
        result = 31 * result + (int) (videoProviderId ^ (videoProviderId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "video_prov_type_prop_id", referencedColumnName = "video_prov_type_prop_id", nullable = false, insertable = false, updatable = false)
    public VideoProviderTypeProperty getVideoProviderTypePropertyByVideoProvTypePropId() {
        return videoProviderTypePropertyByVideoProvTypePropId;
    }

    public void setVideoProviderTypePropertyByVideoProvTypePropId(VideoProviderTypeProperty videoProviderTypePropertyByVideoProvTypePropId) {
        this.videoProviderTypePropertyByVideoProvTypePropId = videoProviderTypePropertyByVideoProvTypePropId;
    }

    @ManyToOne
    @JoinColumn(name = "video_provider_id", referencedColumnName = "video_provider_id", nullable = false, insertable = false, updatable = false)
    public VideoProvider getVideoProviderByVideoProviderId() {
        return videoProviderByVideoProviderId;
    }

    public void setVideoProviderByVideoProviderId(VideoProvider videoProviderByVideoProviderId) {
        this.videoProviderByVideoProviderId = videoProviderByVideoProviderId;
    }
}
