package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "custom_field_type", schema = "brivo20", catalog = "onair")
public class CustomFieldType {
    private long typeId;
    private String name;
    private Collection<CustomFieldDefinition> customFieldDefinitionsByTypeId;
    private Collection<GroupCustomFieldDefinition> groupCustomFieldDefinitionsByTypeId;

    @Id
    @Column(name = "type_id", nullable = false)
    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomFieldType that = (CustomFieldType) o;

        if (typeId != that.typeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (typeId ^ (typeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "customFieldTypeByTypeId")
    public Collection<CustomFieldDefinition> getCustomFieldDefinitionsByTypeId() {
        return customFieldDefinitionsByTypeId;
    }

    public void setCustomFieldDefinitionsByTypeId(Collection<CustomFieldDefinition> customFieldDefinitionsByTypeId) {
        this.customFieldDefinitionsByTypeId = customFieldDefinitionsByTypeId;
    }

    @OneToMany(mappedBy = "customFieldTypeByTypeId")
    public Collection<GroupCustomFieldDefinition> getGroupCustomFieldDefinitionsByTypeId() {
        return groupCustomFieldDefinitionsByTypeId;
    }

    public void setGroupCustomFieldDefinitionsByTypeId(Collection<GroupCustomFieldDefinition> groupCustomFieldDefinitionsByTypeId) {
        this.groupCustomFieldDefinitionsByTypeId = groupCustomFieldDefinitionsByTypeId;
    }
}
