package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.Collection;

/**
 * DeviceDTO refers to all devices, except Doors
 * A door is a device having deviceTypeId = 1
 */
public class DeviceDTO extends AbstractDTO {

    protected final Long deviceId;
    protected final Long objectId;
    protected final Long deviceTypeId;
    protected final Collection<BigInteger> siteIds;
    protected final String deviceName;
    protected final Long brainId;
    protected final Long panelId;
    protected final String bluetoothReaderId;
    protected final Short maximumREXExtension;
    protected final Short isEgress;
    protected final Short isIngress;
    protected final Collection<Long> deviceUnlockScheduleIds;
    protected final Long twoFactorSchedule;
    protected final Long twoFactorInterval;
    protected final Long cardRequiredSchedule;
    protected final Short controlFromBrowser;
    protected final Short deleted;
    protected Collection<ProgDevicePutDTO> progDevicePuts;
    protected Collection<ProgDeviceDataDTO> progDeviceData;

    @JsonCreator
    public DeviceDTO(@JsonProperty("id") final Long deviceId, @JsonProperty("objectId") final Long objectId,
                     @JsonProperty("brainId") final Long brainId, @JsonProperty("deviceTypeId") final Long deviceTypeId,
                     @JsonProperty("siteIds") final Collection<BigInteger> siteIds, @JsonProperty("deviceName") final String deviceName,
                     @JsonProperty("panelId") final Long panelId,
                     @JsonProperty("bluetoothReaderId") final String bluetoothReaderId,
                     @JsonProperty("maximumREXExtension") final Short maximumREXExtension,
                     @JsonProperty("isIngress") final Short isIngress,
                     @JsonProperty("isEgress") final Short isEgress,
                     @JsonProperty("deviceUnlockScheduleIds") final Collection<Long> deviceUnlockScheduleIds,
                     @JsonProperty("twoFactorSchedule") final Long twoFactorSchedule,
                     @JsonProperty("twoFactorInterval") final Long twoFactorInterval,
                     @JsonProperty("cardRequiredSchedule") final Long cardRequiredSchedule,
                     @JsonProperty("controlFromBrowser") final Short controlFromBrowser,
                     @JsonProperty("deleted") final Short deleted,
                     @JsonProperty("progDevicePuts") final Collection<ProgDevicePutDTO> progDevicePuts,
                     @JsonProperty("progDeviceData") final Collection<ProgDeviceDataDTO> progDeviceData) {
        this.deviceId = deviceId;
        this.siteIds = siteIds;
        this.deviceName = deviceName;
        this.panelId = panelId;
        this.bluetoothReaderId = bluetoothReaderId;
        this.maximumREXExtension = maximumREXExtension;
        this.isIngress = isIngress;
        this.deviceUnlockScheduleIds = deviceUnlockScheduleIds;
        this.isEgress = isEgress;
        this.twoFactorSchedule = twoFactorSchedule;
        this.twoFactorInterval = twoFactorInterval;
        this.cardRequiredSchedule = cardRequiredSchedule;
        this.controlFromBrowser = controlFromBrowser;
        this.objectId = objectId;
        this.brainId = brainId;
        this.deviceTypeId = deviceTypeId;
        this.deleted = deleted;
        this.progDevicePuts = progDevicePuts;
        this.progDeviceData = progDeviceData;
    }

    @Override
    public Long getId() {
        return deviceId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public Collection<BigInteger> getSiteIds() {
        return siteIds;
    }

    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public Long getBrainId() {
        return brainId;
    }

    public Long getPanelId() {
        return panelId;
    }

    public String getBluetoothReaderId() {
        return bluetoothReaderId;
    }

    public Short getMaximumREXExtension() {
        return maximumREXExtension;
    }

    public Collection<Long> getDeviceUnlockScheduleIds() {
        return deviceUnlockScheduleIds;
    }

    public Long getTwoFactorSchedule() {
        return twoFactorSchedule;
    }

    public Long getTwoFactorInterval() {
        return twoFactorInterval;
    }

    public Long getCardRequiredSchedule() {
        return cardRequiredSchedule;
    }

    public Short getControlFromBrowser() {
        return controlFromBrowser;
    }

    public Short getIsEgress() {
        return isEgress;
    }

    public Short getIsIngress() {
        return isIngress;
    }

    public Short getDeleted() {
        return deleted;
    }

    public Collection<ProgDevicePutDTO> getProgDevicePuts() {
        return progDevicePuts;
    }

    public Collection<ProgDeviceDataDTO> getProgDeviceData() {
        return progDeviceData;
    }

    public void setProgDevicePuts(Collection<ProgDevicePutDTO> progDevicePuts) {
        this.progDevicePuts = progDevicePuts;
    }

    public void setProgDeviceData(Collection<ProgDeviceDataDTO> progDeviceData) {
        this.progDeviceData = progDeviceData;
    }

}
