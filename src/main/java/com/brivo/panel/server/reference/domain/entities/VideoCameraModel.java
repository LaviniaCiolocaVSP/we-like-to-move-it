package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "video_camera_model", schema = "brivo20", catalog = "onair")
public class VideoCameraModel {
    private long videoCameraModelId;
    private String name;
    private Collection<VideoCamera> videoCamerasByVideoCameraModelId;
    private Collection<VideoCameraModelParameter> videoCameraModelParametersByVideoCameraModelId;

    @Id
    @Column(name = "video_camera_model_id", nullable = false)
    public long getVideoCameraModelId() {
        return videoCameraModelId;
    }

    public void setVideoCameraModelId(long videoCameraModelId) {
        this.videoCameraModelId = videoCameraModelId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoCameraModel that = (VideoCameraModel) o;

        if (videoCameraModelId != that.videoCameraModelId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoCameraModelId ^ (videoCameraModelId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "videoCameraModelByVideoCameraModelId")
    public Collection<VideoCamera> getVideoCamerasByVideoCameraModelId() {
        return videoCamerasByVideoCameraModelId;
    }

    public void setVideoCamerasByVideoCameraModelId(Collection<VideoCamera> videoCamerasByVideoCameraModelId) {
        this.videoCamerasByVideoCameraModelId = videoCamerasByVideoCameraModelId;
    }

    @OneToMany(mappedBy = "videoCameraModelByVideoCameraModelId")
    public Collection<VideoCameraModelParameter> getVideoCameraModelParametersByVideoCameraModelId() {
        return videoCameraModelParametersByVideoCameraModelId;
    }

    public void setVideoCameraModelParametersByVideoCameraModelId(Collection<VideoCameraModelParameter> videoCameraModelParametersByVideoCameraModelId) {
        this.videoCameraModelParametersByVideoCameraModelId = videoCameraModelParametersByVideoCameraModelId;
    }
}
