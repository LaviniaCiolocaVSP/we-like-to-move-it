package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TwoFactorCredentialThreatSettings {
    private Integer severityCheckpoint;
    private Integer treatmentOperator;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getSeverityCheckpoint() {
        return severityCheckpoint;
    }

    public void setSeverityCheckpoint(Integer severityCheckpoint) {
        this.severityCheckpoint = severityCheckpoint;
    }

    public Integer getTreatmentOperator() {
        return treatmentOperator;
    }

    public void setTreatmentOperator(Integer treatmentOperator) {
        this.treatmentOperator = treatmentOperator;
    }
}
