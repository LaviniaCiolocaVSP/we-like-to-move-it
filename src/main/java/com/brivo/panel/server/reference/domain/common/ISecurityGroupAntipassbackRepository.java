package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.SecurityGroupAntipassback;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ISecurityGroupAntipassbackRepository extends CrudRepository<SecurityGroupAntipassback, Long> {

    @Query(
            value = "SELECT sga.* " +
                    "FROM brivo20.security_group_antipassback sga " +
                    "WHERE sga.security_group_id = :securityGroupId",
            nativeQuery = true
    )
    Optional<SecurityGroupAntipassback> findBySecurityGroupId(@Param("securityGroupId") long securityGroupId);
}