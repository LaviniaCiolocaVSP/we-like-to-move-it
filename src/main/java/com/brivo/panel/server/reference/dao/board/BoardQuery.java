package com.brivo.panel.server.reference.dao.board;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum BoardQuery implements FileQuery {
    DELETE_BOARD;

    private String query;

    BoardQuery() {
        this.query = QueryUtils.getQueryText(BoardQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}