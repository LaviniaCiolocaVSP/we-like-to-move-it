package com.brivo.panel.server.reference.service.util;

import java.io.BufferedReader;
import java.io.IOException;

public final class IOUtils {

    private static final String NEW_LINE = System.getProperty("line.separator");

    public static String getBufferContent(final BufferedReader bufferedReader) throws IOException {
        final StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line)
                         .append(NEW_LINE);
        }
        return stringBuilder.toString();
    }
}
