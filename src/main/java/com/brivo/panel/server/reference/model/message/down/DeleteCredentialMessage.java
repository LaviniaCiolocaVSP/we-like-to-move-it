package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

import java.util.Arrays;

public class DeleteCredentialMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long[] credentials;
    private String refId;

    public DeleteCredentialMessage() {
        super(DownstreamMessageType.DeleteCredential);
    }

    @Override
    public String toString() {
        return "DeleteCredentialMessage{" +
                "panelSerialNumber='" + panelSerialNumber + '\'' +
                ", credentials=" + Arrays.toString(credentials) +
                ", refId='" + refId + '\'' +
                '}';
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(final String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long[] getCredentials() {
        return credentials;
    }

    public void setCredentials(final Long[] credentials) {
        this.credentials = credentials;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(final String refId) {
        this.refId = refId;
    }
}
