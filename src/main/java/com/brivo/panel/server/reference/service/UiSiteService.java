package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.Address;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupRepository;
import com.brivo.panel.server.reference.dto.AddressDTO;
import com.brivo.panel.server.reference.dto.SiteDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UiSiteService {

    private final static Logger LOGGER = LoggerFactory.getLogger(UiSiteService.class);

    private final SecurityGroupRepository securityGroupRepository;

    @Autowired
    public UiSiteService(final SecurityGroupRepository securityGroupRepository) {
        this.securityGroupRepository = securityGroupRepository;
    }


    public Collection<SiteDTO> getSiteDTOS(final Collection<SecurityGroup> sites) {
        return sites.stream()
                    .map(convertSite())
                    .collect(Collectors.toSet());
    }

    private Function<SecurityGroup, SiteDTO> convertSite() {
        return site -> {
            final Collection<AddressDTO> addresses = getAddressesForSite(site);

            //FIXME - are the addresses really needed for site? Security_group_antipassback is only for user groups
            return new SiteDTO(site.getSecurityGroupId(), site.getObjectByObjectId().getObjectId(), site.getName(),
                    addresses, site.getLockdown(), site.getDeleted(), site.getDisabled());
        };
    }

    private Collection<AddressDTO> getAddressesForSite(final SecurityGroup site) {
        Collection<Address> addresses = site.getObjectByObjectId()
                                            .getAddressesByObjectId();

        return addresses.stream()
                        .map(convertAddress())
                        .collect(Collectors.toSet());
    }

    private Function<Address, AddressDTO> convertAddress() {
        return address -> new AddressDTO(address.getAddressId(), address.getAddress1(), address.getAddress2(), address.getCity(),
                address.getState(), address.getState(), address.getZip(), address.getPoBox(), address.getTimeZone());
    }
}
