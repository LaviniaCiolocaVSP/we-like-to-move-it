package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

public class UserDTO extends AbstractDTO {

    private final Long userId;
    private final Long objectId;
    private final Long userTypeId;
    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final Collection<BigInteger> groupIds;
    private final Short disabled;
    private final LocalDateTime deactivated;

    @JsonCreator
    public UserDTO(@JsonProperty("id") final Long userId, @JsonProperty("objectId") final Long objectId,
                   @JsonProperty("userTypeId") final Long userTypeId, @JsonProperty("firstName") final String firstName,
                   @JsonProperty("middleName") final String middleName, @JsonProperty("lastName") final String lastName,
                   @JsonProperty("groupIds") final Collection<BigInteger> groupIds,
                   @JsonProperty("disabled") final Short disabled,
                   @JsonProperty("deactivated") final LocalDateTime deactivated) {
        this.userId = userId;
        this.objectId = objectId;
        this.userTypeId = userTypeId;
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.groupIds = groupIds;
        this.disabled = disabled;
        this.deactivated = deactivated;
    }

    @Override
    public Long getId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public Long getObjectId() {
        return objectId;
    }

    public Long getUserTypeId() {
        return userTypeId;
    }

    public Collection<BigInteger> getGroupIds() {
        return groupIds;
    }

    public Short getDisabled() {
        return disabled;
    }

    public LocalDateTime getDeactivated() {
        return deactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(userId, userDTO.userId) &&
                Objects.equals(objectId, userDTO.objectId) &&
                Objects.equals(userTypeId, userDTO.userTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, objectId, userTypeId);
    }
}
