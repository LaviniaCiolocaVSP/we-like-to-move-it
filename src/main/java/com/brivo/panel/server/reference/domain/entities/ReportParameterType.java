package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_parameter_type", schema = "brivo20", catalog = "onair")
public class ReportParameterType {
    private long id;
    private String name;
    private Short forPrompting;
    private Short requiresAuthorization;
    private String permissionType;
    private Collection<ReportParameter> reportParametersById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "for_prompting", nullable = true)
    public Short getForPrompting() {
        return forPrompting;
    }

    public void setForPrompting(Short forPrompting) {
        this.forPrompting = forPrompting;
    }

    @Basic
    @Column(name = "requires_authorization", nullable = true)
    public Short getRequiresAuthorization() {
        return requiresAuthorization;
    }

    public void setRequiresAuthorization(Short requiresAuthorization) {
        this.requiresAuthorization = requiresAuthorization;
    }

    @Basic
    @Column(name = "permission_type", nullable = true, length = 30)
    public String getPermissionType() {
        return permissionType;
    }

    public void setPermissionType(String permissionType) {
        this.permissionType = permissionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportParameterType that = (ReportParameterType) o;

        if (id != that.id) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (forPrompting != null ? !forPrompting.equals(that.forPrompting) : that.forPrompting != null) {
            return false;
        }
        if (requiresAuthorization != null ? !requiresAuthorization.equals(that.requiresAuthorization) : that.requiresAuthorization != null) {
            return false;
        }
        return permissionType != null ? permissionType.equals(that.permissionType) : that.permissionType == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (forPrompting != null ? forPrompting.hashCode() : 0);
        result = 31 * result + (requiresAuthorization != null ? requiresAuthorization.hashCode() : 0);
        result = 31 * result + (permissionType != null ? permissionType.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportParameterTypeByReportParameterType")
    public Collection<ReportParameter> getReportParametersById() {
        return reportParametersById;
    }

    public void setReportParametersById(Collection<ReportParameter> reportParametersById) {
        this.reportParametersById = reportParametersById;
    }
}
