package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;

@Entity
public class Schedule {
    private long scheduleId;
    private long accountId;
    private long scheduleTypeId;
    private String name;
    private String description;
    private byte[] schedule;
    private long cadmScheduleId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Long siteId;
    private Long enablingGroupId;
    private Long enablingGracePeriod;
    private Collection<DeviceSchedule> deviceSchedulesByScheduleId;
    private Collection<NotifRule> notifRulesByScheduleId;
    private Collection<ObjectPermission> objectPermissionsByScheduleId;
    private Account accountByAccountId;
    private ScheduleType scheduleTypeByScheduleTypeId;
    private Collection<ScheduleData> scheduleDataByScheduleId;
    private Collection<ScheduleExceptionData> scheduleExceptionDataByScheduleId;
    private Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId;
    private Collection<TesResident> tesResidentsByScheduleId;

    @Id
    @Column(name = "schedule_id", nullable = false)
    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "account_id", nullable = false, insertable = false, updatable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "schedule_type_id", nullable = false, insertable = false, updatable = false)
    public long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "schedule", nullable = true)
    public byte[] getSchedule() {
        return schedule;
    }

    public void setSchedule(byte[] schedule) {
        this.schedule = schedule;
    }

    @Basic
    @Column(name = "cadm_schedule_id", nullable = false)
    public long getCadmScheduleId() {
        return cadmScheduleId;
    }

    public void setCadmScheduleId(long cadmScheduleId) {
        this.cadmScheduleId = cadmScheduleId;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "site_id", nullable = true)
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "enabling_group_id", nullable = true)
    public Long getEnablingGroupId() {
        return enablingGroupId;
    }

    public void setEnablingGroupId(Long enablingGroupId) {
        this.enablingGroupId = enablingGroupId;
    }

    @Basic
    @Column(name = "enabling_grace_period", nullable = true)
    public Long getEnablingGracePeriod() {
        return enablingGracePeriod;
    }

    public void setEnablingGracePeriod(Long enablingGracePeriod) {
        this.enablingGracePeriod = enablingGracePeriod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Schedule schedule1 = (Schedule) o;

        if (scheduleId != schedule1.scheduleId) {
            return false;
        }
        if (accountId != schedule1.accountId) {
            return false;
        }
        if (cadmScheduleId != schedule1.cadmScheduleId) {
            return false;
        }
        if (scheduleTypeByScheduleTypeId.getScheduleTypeId() != schedule1.scheduleTypeByScheduleTypeId.getScheduleTypeId()) {
            return false;
        }
        if (name != null ? !name.equals(schedule1.name) : schedule1.name != null) {
            return false;
        }
        if (description != null ? !description.equals(schedule1.description) : schedule1.description != null) {
            return false;
        }
        if (!Arrays.equals(schedule, schedule1.schedule)) {
            return false;
        }
        if (created != null ? !created.equals(schedule1.created) : schedule1.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(schedule1.updated) : schedule1.updated != null) {
            return false;
        }
        if (siteId != null ? !siteId.equals(schedule1.siteId) : schedule1.siteId != null) {
            return false;
        }
        if (enablingGroupId != null ? !enablingGroupId.equals(schedule1.enablingGroupId) : schedule1.enablingGroupId != null) {
            return false;
        }
        return enablingGracePeriod != null ? enablingGracePeriod.equals(schedule1.enablingGracePeriod) : schedule1.enablingGracePeriod == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleId ^ (scheduleId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(schedule);
        result = 31 * result + (int) (cadmScheduleId ^ (cadmScheduleId >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (scheduleTypeByScheduleTypeId.getScheduleTypeId() ^ (scheduleTypeByScheduleTypeId.getScheduleTypeId() >>> 32));
        result = 31 * result + (siteId != null ? siteId.hashCode() : 0);
        result = 31 * result + (enablingGroupId != null ? enablingGroupId.hashCode() : 0);
        result = 31 * result + (enablingGracePeriod != null ? enablingGracePeriod.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "scheduleByScheduleId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<DeviceSchedule> getDeviceSchedulesByScheduleId() {
        return deviceSchedulesByScheduleId;
    }

    public void setDeviceSchedulesByScheduleId(Collection<DeviceSchedule> deviceSchedulesByScheduleId) {
        this.deviceSchedulesByScheduleId = deviceSchedulesByScheduleId;
    }

    @OneToMany(mappedBy = "scheduleByScheduleId")
    public Collection<NotifRule> getNotifRulesByScheduleId() {
        return notifRulesByScheduleId;
    }

    public void setNotifRulesByScheduleId(Collection<NotifRule> notifRulesByScheduleId) {
        this.notifRulesByScheduleId = notifRulesByScheduleId;
    }

    @OneToMany(mappedBy = "scheduleByScheduleId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ObjectPermission> getObjectPermissionsByScheduleId() {
        return objectPermissionsByScheduleId;
    }

    public void setObjectPermissionsByScheduleId(Collection<ObjectPermission> objectPermissionsByScheduleId) {
        this.objectPermissionsByScheduleId = objectPermissionsByScheduleId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "schedule_type_id", referencedColumnName = "schedule_type_id", nullable = false)
    public ScheduleType getScheduleTypeByScheduleTypeId() {
        return scheduleTypeByScheduleTypeId;
    }

    public void setScheduleTypeByScheduleTypeId(ScheduleType scheduleTypeByScheduleTypeId) {
        this.scheduleTypeByScheduleTypeId = scheduleTypeByScheduleTypeId;
    }

    @OneToMany(mappedBy = "scheduleByScheduleId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ScheduleData> getScheduleDataByScheduleId() {
        return scheduleDataByScheduleId;
    }

    public void setScheduleDataByScheduleId(Collection<ScheduleData> scheduleDataByScheduleId) {
        this.scheduleDataByScheduleId = scheduleDataByScheduleId;
    }

    @OneToMany(mappedBy = "scheduleByScheduleId", cascade = CascadeType.ALL)
    public Collection<ScheduleExceptionData> getScheduleExceptionDataByScheduleId() {
        return scheduleExceptionDataByScheduleId;
    }

    public void setScheduleExceptionDataByScheduleId(Collection<ScheduleExceptionData> scheduleExceptionDataByScheduleId) {
        this.scheduleExceptionDataByScheduleId = scheduleExceptionDataByScheduleId;
    }

    @OneToMany(mappedBy = "scheduleByScheduleOid", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ScheduleHolidayMap> getScheduleHolidayMapsByScheduleId() {
        return scheduleHolidayMapsByScheduleId;
    }

    public void setScheduleHolidayMapsByScheduleId(Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId) {
        this.scheduleHolidayMapsByScheduleId = scheduleHolidayMapsByScheduleId;
    }

    @OneToMany(mappedBy = "scheduleByDndScheduleId")
    public Collection<TesResident> getTesResidentsByScheduleId() {
        return tesResidentsByScheduleId;
    }

    public void setTesResidentsByScheduleId(Collection<TesResident> tesResidentsByScheduleId) {
        this.tesResidentsByScheduleId = tesResidentsByScheduleId;
    }
}
