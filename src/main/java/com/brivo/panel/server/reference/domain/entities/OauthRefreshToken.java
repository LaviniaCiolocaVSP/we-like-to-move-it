package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Arrays;
import java.util.Collection;

@Entity
@Table(name = "oauth_refresh_token", schema = "brivo20", catalog = "onair")
public class OauthRefreshToken {
    private String tokenId;
    private byte[] token;
    private byte[] authentication;
    private Collection<OauthAccessToken> oauthAccessTokensByTokenId;

    @Id
    @Column(name = "token_id", nullable = false, length = 50)
    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Basic
    @Column(name = "token", nullable = true)
    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    @Basic
    @Column(name = "authentication", nullable = true)
    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OauthRefreshToken that = (OauthRefreshToken) o;

        if (tokenId != null ? !tokenId.equals(that.tokenId) : that.tokenId != null) {
            return false;
        }
        if (!Arrays.equals(token, that.token)) {
            return false;
        }
        return Arrays.equals(authentication, that.authentication);
    }

    @Override
    public int hashCode() {
        int result = tokenId != null ? tokenId.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(token);
        result = 31 * result + Arrays.hashCode(authentication);
        return result;
    }

    @OneToMany(mappedBy = "oauthRefreshTokenByRefreshTokenId")
    public Collection<OauthAccessToken> getOauthAccessTokensByTokenId() {
        return oauthAccessTokensByTokenId;
    }

    public void setOauthAccessTokensByTokenId(Collection<OauthAccessToken> oauthAccessTokensByTokenId) {
        this.oauthAccessTokensByTokenId = oauthAccessTokensByTokenId;
    }
}
