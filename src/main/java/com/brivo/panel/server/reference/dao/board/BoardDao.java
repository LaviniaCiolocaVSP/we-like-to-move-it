package com.brivo.panel.server.reference.dao.board;


import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.domain.entities.Board;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class BoardDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(BoardDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteBoard(final Board board) {
        LOGGER.info("Deleting board with object id {}", board.getObjectId());

        String query = BoardQuery.DELETE_BOARD.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("boardObjectId", board.getObjectId());
        paramMap.put("brainObjectId", board.getPanelOid());
        paramMap.put("boardNumber", board.getBoardNumber());

        jdbcTemplate.update(query, paramMap);
    }
}