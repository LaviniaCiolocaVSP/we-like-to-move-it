package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "security_event_type", schema = "brivo20", catalog = "onair")
public class SecurityEventType {
    private long securityEventTypeId;
    private String nameKey;
    private String description;
    private Collection<SecurityAction> securityActionsBySecurityEventTypeId;

    @Id
    @Column(name = "security_event_type_id", nullable = false)
    public long getSecurityEventTypeId() {
        return securityEventTypeId;
    }

    public void setSecurityEventTypeId(long securityEventTypeId) {
        this.securityEventTypeId = securityEventTypeId;
    }

    @Basic
    @Column(name = "name_key", nullable = true, length = 65)
    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityEventType that = (SecurityEventType) o;

        if (securityEventTypeId != that.securityEventTypeId) {
            return false;
        }
        if (nameKey != null ? !nameKey.equals(that.nameKey) : that.nameKey != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityEventTypeId ^ (securityEventTypeId >>> 32));
        result = 31 * result + (nameKey != null ? nameKey.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "securityEventTypeBySecurityEventTypeId")
    public Collection<SecurityAction> getSecurityActionsBySecurityEventTypeId() {
        return securityActionsBySecurityEventTypeId;
    }

    public void setSecurityActionsBySecurityEventTypeId(Collection<SecurityAction> securityActionsBySecurityEventTypeId) {
        this.securityActionsBySecurityEventTypeId = securityActionsBySecurityEventTypeId;
    }
}
