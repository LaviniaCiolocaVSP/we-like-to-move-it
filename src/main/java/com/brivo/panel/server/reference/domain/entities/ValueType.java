package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "value_type", schema = "brivo20", catalog = "onair")
public class ValueType {
    private long valueTypeId;
    private String name;
    private String description;
    private Collection<AccountSettings> accountSettingsByValueTypeId;
    private Collection<BoardProperty> boardPropertiesByValueTypeId;
    private Collection<ProximityProperty> proximityPropertiesByValueTypeId;
    private Collection<VideoCameraModelParameter> videoCameraModelParametersByValueTypeId;
    private Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByValueTypeId;

    @Id
    @Column(name = "value_type_id", nullable = false)
    public long getValueTypeId() {
        return valueTypeId;
    }

    public void setValueTypeId(long valueTypeId) {
        this.valueTypeId = valueTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ValueType valueType = (ValueType) o;

        if (valueTypeId != valueType.valueTypeId) {
            return false;
        }
        if (name != null ? !name.equals(valueType.name) : valueType.name != null) {
            return false;
        }
        return description != null ? description.equals(valueType.description) : valueType.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (valueTypeId ^ (valueTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "valueTypeByValueTypeId")
    public Collection<AccountSettings> getAccountSettingsByValueTypeId() {
        return accountSettingsByValueTypeId;
    }

    public void setAccountSettingsByValueTypeId(Collection<AccountSettings> accountSettingsByValueTypeId) {
        this.accountSettingsByValueTypeId = accountSettingsByValueTypeId;
    }

    @OneToMany(mappedBy = "valueTypeByValueTypeId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Collection<BoardProperty> getBoardPropertiesByValueTypeId() {
        return boardPropertiesByValueTypeId;
    }

    public void setBoardPropertiesByValueTypeId(Collection<BoardProperty> boardPropertiesByValueTypeId) {
        this.boardPropertiesByValueTypeId = boardPropertiesByValueTypeId;
    }

    @OneToMany(mappedBy = "valueTypeByValueTypeId")
    public Collection<ProximityProperty> getProximityPropertiesByValueTypeId() {
        return proximityPropertiesByValueTypeId;
    }

    public void setProximityPropertiesByValueTypeId(Collection<ProximityProperty> proximityPropertiesByValueTypeId) {
        this.proximityPropertiesByValueTypeId = proximityPropertiesByValueTypeId;
    }

    @OneToMany(mappedBy = "valueTypeByValueTypeId")
    public Collection<VideoCameraModelParameter> getVideoCameraModelParametersByValueTypeId() {
        return videoCameraModelParametersByValueTypeId;
    }

    public void setVideoCameraModelParametersByValueTypeId(Collection<VideoCameraModelParameter> videoCameraModelParametersByValueTypeId) {
        this.videoCameraModelParametersByValueTypeId = videoCameraModelParametersByValueTypeId;
    }

    @OneToMany(mappedBy = "valueTypeByValueTypeId")
    public Collection<VideoProviderTypeProperty> getVideoProviderTypePropertiesByValueTypeId() {
        return videoProviderTypePropertiesByValueTypeId;
    }

    public void setVideoProviderTypePropertiesByValueTypeId(Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByValueTypeId) {
        this.videoProviderTypePropertiesByValueTypeId = videoProviderTypePropertiesByValueTypeId;
    }
}
