package com.brivo.panel.server.reference.service.account;

import com.brivo.panel.server.reference.dao.user.UserDao;
import com.brivo.panel.server.reference.dto.MessageDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.io.File;

@Component
class DeleteAccount extends AbstractAccountCRUDComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteAccount.class);

    private static final String PATH_SEPARATOR = File.separator;

    private final Environment environment;
    private final DataSource dataSource;

    @Autowired
    private UserDao userDao;

    @Value("${sql.scripts.folder}")
    private String sqlScriptsFolder;

    @Value("${database.cleanup.file-name}")
    private String databaseCleanupFileName;

    @Autowired
    public DeleteAccount(final Environment environment, @Qualifier("dataSource") final DataSource dataSource) {
        this.environment = environment;
        this.dataSource = dataSource;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteUser() {
        userDao.deleteUserByUserObjectId(Long.valueOf(60071));
        return new MessageDTO(String.format("User with object id '{}' successfully deleted.", "60071"));
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public void deleteAccounts() {
        final String currentProfile = environment.getActiveProfiles()[0];
        final String profileName = currentProfile.indexOf("-") > 0 ? currentProfile.split("-")[0] : currentProfile;

        final Resource resource = getSQLFileName(profileName);
        LOGGER.info("Executing the account deletion SQL statements from the file '{}'...", resource.getFilename());

        final ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(resource);
        databasePopulator.execute(dataSource);

        LOGGER.info("The SQL statements were successfully executed!");
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public void deleteBOLAccounts() {
        final Resource resource = getSQLFileName("BOL");
        LOGGER.info("Executing the account deletion SQL statements from the file '{}'...", resource.getFilename());

        final ResourceDatabasePopulator databasePopulator = new ResourceDatabasePopulator(resource);
        databasePopulator.execute(dataSource);

        LOGGER.info("The SQL statements were successfully executed!");
    }

    private ClassPathResource getSQLFileName(final String profileName) {
        return new ClassPathResource(sqlScriptsFolder + PATH_SEPARATOR +
                databaseCleanupFileName.replaceFirst("\\$profileName", profileName));
    }
}
