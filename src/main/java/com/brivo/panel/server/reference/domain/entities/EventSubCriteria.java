package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "event_sub_criteria", schema = "brivo20", catalog = "onair")
public class EventSubCriteria {
    private long eventSubCriteriaId;
    private long eventSubId;
    private long keyTypeId;
    private long opTypeId;
    private long numberValue;
    private EventSubscription eventSubscriptionByEventSubId;
    private EventSubKeyType eventSubKeyTypeByKeyTypeId;
    private EventSubOpType eventSubOpTypeByOpTypeId;

    @Id
    @Column(name = "event_sub_criteria_id", nullable = false)
    public long getEventSubCriteriaId() {
        return eventSubCriteriaId;
    }

    public void setEventSubCriteriaId(long eventSubCriteriaId) {
        this.eventSubCriteriaId = eventSubCriteriaId;
    }

    @Basic
    @Column(name = "event_sub_id", nullable = false)
    public long getEventSubId() {
        return eventSubId;
    }

    public void setEventSubId(long eventSubId) {
        this.eventSubId = eventSubId;
    }

    @Basic
    @Column(name = "key_type_id", nullable = false)
    public long getKeyTypeId() {
        return keyTypeId;
    }

    public void setKeyTypeId(long keyTypeId) {
        this.keyTypeId = keyTypeId;
    }

    @Basic
    @Column(name = "op_type_id", nullable = false)
    public long getOpTypeId() {
        return opTypeId;
    }

    public void setOpTypeId(long opTypeId) {
        this.opTypeId = opTypeId;
    }

    @Basic
    @Column(name = "number_value", nullable = false)
    public long getNumberValue() {
        return numberValue;
    }

    public void setNumberValue(long numberValue) {
        this.numberValue = numberValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventSubCriteria that = (EventSubCriteria) o;

        if (eventSubCriteriaId != that.eventSubCriteriaId) {
            return false;
        }
        if (eventSubId != that.eventSubId) {
            return false;
        }
        if (keyTypeId != that.keyTypeId) {
            return false;
        }
        if (opTypeId != that.opTypeId) {
            return false;
        }
        return numberValue == that.numberValue;
    }

    @Override
    public int hashCode() {
        int result = (int) (eventSubCriteriaId ^ (eventSubCriteriaId >>> 32));
        result = 31 * result + (int) (eventSubId ^ (eventSubId >>> 32));
        result = 31 * result + (int) (keyTypeId ^ (keyTypeId >>> 32));
        result = 31 * result + (int) (opTypeId ^ (opTypeId >>> 32));
        result = 31 * result + (int) (numberValue ^ (numberValue >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "event_sub_id", referencedColumnName = "event_sub_id", nullable = false, insertable = false, updatable = false)
    public EventSubscription getEventSubscriptionByEventSubId() {
        return eventSubscriptionByEventSubId;
    }

    public void setEventSubscriptionByEventSubId(EventSubscription eventSubscriptionByEventSubId) {
        this.eventSubscriptionByEventSubId = eventSubscriptionByEventSubId;
    }

    @ManyToOne
    @JoinColumn(name = "key_type_id", referencedColumnName = "key_type_id", nullable = false, insertable = false, updatable = false)
    public EventSubKeyType getEventSubKeyTypeByKeyTypeId() {
        return eventSubKeyTypeByKeyTypeId;
    }

    public void setEventSubKeyTypeByKeyTypeId(EventSubKeyType eventSubKeyTypeByKeyTypeId) {
        this.eventSubKeyTypeByKeyTypeId = eventSubKeyTypeByKeyTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "op_type_id", referencedColumnName = "op_type_id", nullable = false, insertable = false, updatable = false)
    public EventSubOpType getEventSubOpTypeByOpTypeId() {
        return eventSubOpTypeByOpTypeId;
    }

    public void setEventSubOpTypeByOpTypeId(EventSubOpType eventSubOpTypeByOpTypeId) {
        this.eventSubOpTypeByOpTypeId = eventSubOpTypeByOpTypeId;
    }
}
