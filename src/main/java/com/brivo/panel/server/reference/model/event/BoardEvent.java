package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class BoardEvent extends Event {
    private Long boardId;

    @JsonCreator
    public BoardEvent(@JsonProperty("eventType") final EventType eventType,
                      @JsonProperty("eventTime") final Instant eventTime,
                      @JsonProperty("boardId") final Long boardId) {
        super(eventType, eventTime);
        this.boardId = boardId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }
}
