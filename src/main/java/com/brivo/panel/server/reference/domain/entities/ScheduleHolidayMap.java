package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schedule_holiday_map", schema = "brivo20", catalog = "onair")
@IdClass(ScheduleHolidayMapPK.class)
public class ScheduleHolidayMap {
    private long scheduleOid;
    private long holidayOid;
    private Schedule scheduleByScheduleOid;
    private Holiday holidayByHolidayOid;

    @Id
    @Column(name = "schedule_oid", nullable = false)
    public long getScheduleOid() {
        return scheduleOid;
    }

    public void setScheduleOid(long scheduleOid) {
        this.scheduleOid = scheduleOid;
    }

    @Id
    @Column(name = "holiday_oid", nullable = false)
    public long getHolidayOid() {
        return holidayOid;
    }

    public void setHolidayOid(long holidayOid) {
        this.holidayOid = holidayOid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleHolidayMap that = (ScheduleHolidayMap) o;

        if (scheduleOid != that.scheduleOid) {
            return false;
        }
        return holidayOid == that.holidayOid;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleOid ^ (scheduleOid >>> 32));
        result = 31 * result + (int) (holidayOid ^ (holidayOid >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "schedule_oid", referencedColumnName = "schedule_id", nullable = false, insertable = false, updatable = false)
    public Schedule getScheduleByScheduleOid() {
        return scheduleByScheduleOid;
    }

    public void setScheduleByScheduleOid(Schedule scheduleByScheduleOid) {
        this.scheduleByScheduleOid = scheduleByScheduleOid;
    }

    @ManyToOne
    @JoinColumn(name = "holiday_oid", referencedColumnName = "holiday_id", nullable = false, insertable = false, updatable = false)
    public Holiday getHolidayByHolidayOid() {
        return holidayByHolidayOid;
    }

    public void setHolidayByHolidayOid(Holiday holidayByHolidayOid) {
        this.holidayByHolidayOid = holidayByHolidayOid;
    }
}
