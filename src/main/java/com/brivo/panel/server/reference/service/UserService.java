package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.user.UserDao;
import com.brivo.panel.server.reference.domain.common.ISecurityGroupRepository;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import com.brivo.panel.server.reference.domain.entities.UserType;
import com.brivo.panel.server.reference.domain.entities.Users;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.BrivoObjectRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupMemberRepository;
import com.brivo.panel.server.reference.domain.repository.UserRepository;
import com.brivo.panel.server.reference.domain.repository.UserTypeRepository;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.UserDTO;
import com.brivo.panel.server.reference.dto.ui.UIUserSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiUserDTO;
import com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    private static final Long ROOT_SECURITY_GROUP_TYPE_ID = 0L;

    private final UserRepository userRepository;
    private final UserTypeRepository userTypeRepository;
    private final AccountRepository accountRepository;
    private final BrivoObjectRepository brivoObjectRepository;
    private final SecurityGroupMemberRepository securityGroupMemberRepository;
    private final UserDao userDao;
    private final UiObjectService objectService;

    @Autowired
    public UserService(UserRepository userRepository,
                       UserTypeRepository userTypeRepository,
                       AccountRepository accountRepository,
                       BrivoObjectRepository brivoObjectRepository,
                       SecurityGroupMemberRepository securityGroupMemberRepository,
                       UserDao userDao, final UiObjectService objectService) {
        this.userRepository = userRepository;
        this.userTypeRepository = userTypeRepository;
        this.accountRepository = accountRepository;
        this.brivoObjectRepository = brivoObjectRepository;
        this.securityGroupMemberRepository = securityGroupMemberRepository;
        this.userDao = userDao;
        this.objectService = objectService;
    }

    public Collection<UserDTO> getUserDTOS(final Collection<Users> users,
                                           final ISecurityGroupRepository dynamicSecurityGroupRepository) {
        return users.stream()
                    .map(convertUser(dynamicSecurityGroupRepository))
                    .collect(Collectors.toSet());
    }

    private Function<Users, UserDTO> convertUser(final ISecurityGroupRepository dynamicSecurityGroupRepository) {
        return user -> {
            final long userObjectId = user.getObjectByBrivoObjectId().getObjectId();
            final Collection<BigInteger> groupIds = getGroupIdsForUser(userObjectId, dynamicSecurityGroupRepository);

            return new UserDTO(user.getUsersId(), userObjectId, user.getUserTypeByUserTypeId().getUserTypeId(),
                               user.getFirstName(), user.getMiddleName(), user.getLastName(), groupIds, user.getDisabled(),
                               user.getDeactivated());
        };
    }

    private Collection<BigInteger> getGroupIdsForUser(final long userObjectId,
                                                      final ISecurityGroupRepository dynamicSecurityGroupRepository) {
        return dynamicSecurityGroupRepository.findByUserObjectId(userObjectId)
                                             .orElse(Collections.emptyList());
    }

    public Collection<UiUserDTO> getUiUserDTOS(final Collection<Users> users) {
        return users.stream()
                    .map(convertUserUI())
                    .sorted(uiUserDTOComparator)
                    .collect(Collectors.toList());
    }

    Comparator<UiUserDTO> uiUserDTOComparator = new Comparator<UiUserDTO>() {
        @Override
        public int compare(UiUserDTO o1, UiUserDTO o2) {
            return o2.getUserId().compareTo(o1.getUserId());
        }
    };

    private Function<Users, UiUserDTO> convertUserUI() {
        return user -> new UiUserDTO(
                user.getObjectByBrivoObjectId().getObjectId(),
                user.getUsersId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getAccountId(),
                user.getUserTypeByUserTypeId().getUserTypeId());
    }

    public Set<UiGroupSummaryDTO> getUiSecurityGroupMember(final Collection<SecurityGroupMember> securityGroupMembersForUser) {
        return securityGroupMembersForUser.stream()
                                          .filter(securityGroupMember -> securityGroupMember.getSecurityGroupBySecurityGroupId().getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId() != ROOT_SECURITY_GROUP_TYPE_ID)
                                          .map(convertSecurityGroupMemberUI())
                                          .collect(Collectors.toSet());
    }

    private Function<SecurityGroupMember, UiGroupSummaryDTO> convertSecurityGroupMemberUI() {
        return group -> new UiGroupSummaryDTO(group.getSecurityGroupId(), group.getSecurityGroupBySecurityGroupId().getName(),
                                              group.getSecurityGroupBySecurityGroupId().getObjectId());
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UIUserSummaryDTO> getUserSummaryForAccount(final Long accountId) {
        Collection<UIUserSummaryDTO> availableUsers = userRepository.findByAccountId(accountId)
                                                                    .stream()
                                                                    .map(convertUserSummaryUI())
                                                                    .collect(Collectors.toSet());

        Collection<UIUserSummaryDTO> allAvailableUsers = new ArrayList<>();
        UIUserSummaryDTO noOwner = new UIUserSummaryDTO(AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE, "None", "");
        allAvailableUsers.add(noOwner);
        allAvailableUsers.addAll(availableUsers);

        return allAvailableUsers;
    }

    private Function<Users, UIUserSummaryDTO> convertUserSummaryUI() {
        return users -> new UIUserSummaryDTO(users.getObjectByBrivoObjectId().getObjectId(), users.getFirstName(),
                                             users.getLastName());
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiUserDTO> getUiUsersForAccountId(final Long accountId) {
        final Collection<UiUserDTO> uiUserDTOSet = getUiUserDTOS(userRepository.findByAccountId(accountId));

        uiUserDTOSet.forEach(uiUserDTO -> {
                                 uiUserDTO.setUiSecurityGroup(new HashSet<>());
                                 securityGroupMemberRepository.findByObjectId(uiUserDTO.getObjectId())
                                                              .ifPresent(securityGroupMembers ->
                                                                                 uiUserDTO.setUiSecurityGroup(getUiSecurityGroupMember(securityGroupMembers)));
                             }
                            );

        return uiUserDTOSet;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO create(UiUserDTO uiUserDTO) {
        final Optional<Long> user_id = userRepository.getMaxUserId();
        uiUserDTO.setUserId(user_id.get() + 1);

        final Long nextObjectId = objectService.getNextObjectId();
        uiUserDTO.setObjectId(nextObjectId);

        validateNewUser(uiUserDTO);

        userRepository.save(createUser(uiUserDTO));
        LOGGER.debug("User {} {} successfully persisted to database...", uiUserDTO.getFirstName(), uiUserDTO.getLastName());

        saveUserToSecurityGroupMember(uiUserDTO);

        return new MessageDTO("The user '" + uiUserDTO.getFirstName() + " " + uiUserDTO.getLastName() + "' was successfully created!");
    }

    private void saveUserToSecurityGroupMember(final UiUserDTO uiUserDTO) {
        uiUserDTO.getUiSecurityGroup()
                 .forEach(uiSecurityGroupMember -> {
                     SecurityGroupMember securityGroupMember = new SecurityGroupMember();
                     securityGroupMember.setSecurityGroupId(uiSecurityGroupMember.getGroupId());
                     securityGroupMember.setObjectId(uiUserDTO.getObjectId());

                     securityGroupMemberRepository.save(securityGroupMember);
                 });
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO delete(List<Long> objects_ids) {
        final Optional<Users> user = userRepository.findByObjectId(objects_ids.get(0));

        for (Long id : objects_ids) {
            validateUserObject(id);
            //brivoObjectRepository.deleteById(id);
            userDao.deleteUserByUserObjectId(id);
            LOGGER.debug("Object {} successfully deleted from database...", id);
        }

        return new MessageDTO("Users with object id '" + objects_ids + "' were successfully deleted!");
    }


    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO update(UiUserDTO user) {
        validateExistingUser(user);
        Users existingUser = userRepository.findById(user.getUserId()).get();

        existingUser.setUsername(user.getUsername());
        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());

        final Users updatedUser = userRepository.save(existingUser);

        securityGroupMemberRepository.findByObjectId(user.getObjectId())
                                     .ifPresent(securityGroupMembers -> securityGroupMembers.forEach(securityGroupMember -> securityGroupMemberRepository.delete(securityGroupMember)));

        saveUserToSecurityGroupMember(user);

        LOGGER.debug("User {} was successfully updated...", updatedUser.getUsersId());

        return new MessageDTO("User" + updatedUser.getUsersId() + " was successfully updated!");
    }

    private Users createUser(final UiUserDTO uiUserDTO) {
        final Users user = new Users();

        user.setUsersId(uiUserDTO.getUserId());
        user.setFirstName(uiUserDTO.getFirstName());
        user.setLastName(uiUserDTO.getLastName());
        user.setUsername(uiUserDTO.getUsername());
        user.setDisabled(Short.valueOf("0"));
        final UserType userType = userTypeRepository.findById(uiUserDTO.getUserTypeId()).get();
        user.setUserTypeByUserTypeId(userType);

        final Account account = accountRepository.findById(uiUserDTO.getAccountId()).get();
        user.setAccountByAccountId(account);

        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.USER);
        brivoObject.setObjectId(uiUserDTO.getObjectId());
        user.setObjectByBrivoObjectId(brivoObject);

        return user;
    }

    private void validateUser(final UiUserDTO uiUserDTO) {
        final String userLastName = uiUserDTO.getLastName();
        Assert.hasLength(userLastName, "The user must have a last name");

        final String userFistName = uiUserDTO.getFirstName();
        Assert.hasLength(userFistName, "The user must have a first name");

        validateUserType(uiUserDTO);
        validateUserAccount(uiUserDTO);

        //TODO add other validations, if needed
    }

    private void validateUserType(UiUserDTO uiUserDTO) {
        final long userTypeId = uiUserDTO.getUserTypeId();
        Assert.state(userTypeRepository.findById(userTypeId).isPresent(),
                     "There is no user type with the id " + userTypeId);
    }

    private void validateUserAccount(UiUserDTO uiUserDTO) {
        final long accountId = uiUserDTO.getAccountId();
        Assert.state(accountRepository.findById(accountId).isPresent(),
                     "There is no account with the id " + accountId);

    }

    private void validateUserObject(Long object_id) {
        Optional<BrivoObject> objectById = brivoObjectRepository.findById(object_id);
        Assert.state(objectById.isPresent(),
                     "There is no object with the id " + object_id);

        Optional<Users> user = userRepository.findByObjectId(object_id);
        Assert.state(user.isPresent(),
                     "There is no user with the object id " + object_id);
    }

    private void validateNewUser(UiUserDTO uiUserDTO) {
        validateUser(uiUserDTO);
    }

    private void validateExistingUser(UiUserDTO uiUserDTO) {
        validateUser(uiUserDTO);
        Optional<Users> user = userRepository.findById(uiUserDTO.getUserId());
        Assert.state(user.isPresent(),
                     "There is no user with the user_id " + uiUserDTO.getUserId());
    }

    public Collection<UiUserDTO> getUiUsersForGroupId(final Long groupId) {
        return getUiUserDTOS(userRepository.findByGroupId(groupId));
    }
}
