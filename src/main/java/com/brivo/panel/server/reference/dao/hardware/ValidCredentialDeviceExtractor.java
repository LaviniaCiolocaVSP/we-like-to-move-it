package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.TwoFactorCredentialSettings;
import com.brivo.panel.server.reference.model.hardware.ValidCredentialDevice;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Valid Credential Device Extractor.
 * <p>
 * OnAir does not support:
 * - inputBoardId
 * - AntipassbackSettings
 * - reportLiveStatus
 * - KeypadCommand
 */
@Component
public class ValidCredentialDeviceExtractor implements
        ResultSetExtractor<List<ValidCredentialDevice>> {
    @Override
    public List<ValidCredentialDevice> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<ValidCredentialDevice> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Integer boardNumber = rs.getInt("boardNumber");
            Integer pointAddress = rs.getInt("pointAddress");

            Integer notifyFlagInt = rs.getInt("notifyFlag");
            Boolean notify = false;
            if (notifyFlagInt != null && notifyFlagInt == 1) {
                notify = true;
            }

            Long twoFactorScheduleId = rs.getLong("twoFactorScheduleID");
            Integer twoFactorInterval = rs.getInt("twoFactorInterval");
            Long cardRequiredScheduleId = rs.getLong("cardRequiredScheduleID");

            ValidCredentialDevice device = new ValidCredentialDevice();

            device.setDeviceId(deviceId);
            device.setInputBoardAddress(boardNumber);
            device.setInputPointAddress(pointAddress);
            device.setReportEngage(notify);

            // Set two factor credential settings.
            TwoFactorCredentialSettings twoFactor = new TwoFactorCredentialSettings();
            twoFactor.setScheduleId(twoFactorScheduleId);
            twoFactor.setInterval(twoFactorInterval);
            device.setTwoFactorCredential(twoFactor);

            device.setCardRequiredScheduleId(cardRequiredScheduleId);

            list.add(device);
        }

        return list;
    }
}
