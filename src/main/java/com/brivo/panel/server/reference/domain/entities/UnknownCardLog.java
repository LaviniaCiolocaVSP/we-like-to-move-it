package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "unknown_card_log", schema = "brivo20", catalog = "onair")
public class UnknownCardLog {
    private long unknownCardLogId;
    private Long accountId;
    private long deviceOid;
    private String doorName;
    private Timestamp occurred;
    private String hexValue;
    private Long numberBits;
    private Timestamp created;

    @Id
    @Column(name = "unknown_card_log_id", nullable = false)
    public long getUnknownCardLogId() {
        return unknownCardLogId;
    }

    public void setUnknownCardLogId(long unknownCardLogId) {
        this.unknownCardLogId = unknownCardLogId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "device_oid", nullable = false)
    public long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(long deviceOid) {
        this.deviceOid = deviceOid;
    }

    @Basic
    @Column(name = "door_name", nullable = true, length = 100)
    public String getDoorName() {
        return doorName;
    }

    public void setDoorName(String doorName) {
        this.doorName = doorName;
    }

    @Basic
    @Column(name = "occurred", nullable = false)
    public Timestamp getOccurred() {
        return occurred;
    }

    public void setOccurred(Timestamp occurred) {
        this.occurred = occurred;
    }

    @Basic
    @Column(name = "hex_value", nullable = true, length = 100)
    public String getHexValue() {
        return hexValue;
    }

    public void setHexValue(String hexValue) {
        this.hexValue = hexValue;
    }

    @Basic
    @Column(name = "number_bits", nullable = true)
    public Long getNumberBits() {
        return numberBits;
    }

    public void setNumberBits(Long numberBits) {
        this.numberBits = numberBits;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UnknownCardLog that = (UnknownCardLog) o;

        if (unknownCardLogId != that.unknownCardLogId) {
            return false;
        }
        if (deviceOid != that.deviceOid) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (doorName != null ? !doorName.equals(that.doorName) : that.doorName != null) {
            return false;
        }
        if (occurred != null ? !occurred.equals(that.occurred) : that.occurred != null) {
            return false;
        }
        if (hexValue != null ? !hexValue.equals(that.hexValue) : that.hexValue != null) {
            return false;
        }
        if (numberBits != null ? !numberBits.equals(that.numberBits) : that.numberBits != null) {
            return false;
        }
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (unknownCardLogId ^ (unknownCardLogId >>> 32));
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (int) (deviceOid ^ (deviceOid >>> 32));
        result = 31 * result + (doorName != null ? doorName.hashCode() : 0);
        result = 31 * result + (occurred != null ? occurred.hashCode() : 0);
        result = 31 * result + (hexValue != null ? hexValue.hashCode() : 0);
        result = 31 * result + (numberBits != null ? numberBits.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }
}
