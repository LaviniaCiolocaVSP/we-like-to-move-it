package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "device_status", schema = "brivo20", catalog = "onair")
public class DeviceStatus {
    private long deviceStatusId;
    private String name;
    private String description;
    private Collection<BrainStatusLog> brainStatusLogsByDeviceStatusId;

    @Id
    @Column(name = "device_status_id", nullable = false)
    public long getDeviceStatusId() {
        return deviceStatusId;
    }

    public void setDeviceStatusId(long deviceStatusId) {
        this.deviceStatusId = deviceStatusId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 30)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 512)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceStatus that = (DeviceStatus) o;

        if (deviceStatusId != that.deviceStatusId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceStatusId ^ (deviceStatusId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "deviceStatusByDeviceStatusId")
    public Collection<BrainStatusLog> getBrainStatusLogsByDeviceStatusId() {
        return brainStatusLogsByDeviceStatusId;
    }

    public void setBrainStatusLogsByDeviceStatusId(Collection<BrainStatusLog> brainStatusLogsByDeviceStatusId) {
        this.brainStatusLogsByDeviceStatusId = brainStatusLogsByDeviceStatusId;
    }
}
