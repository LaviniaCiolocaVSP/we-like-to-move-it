package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class TriggerDeviceOutputMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long deviceId;
    private Long administratorId;
    private Long currentState;
    private Long behavior;

    public TriggerDeviceOutputMessage() {
        super(DownstreamMessageType.TRIGGER_DEVICE_OUTPUT);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(final String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getAdministratorId() {
        return administratorId;
    }

    public void setAdministratorId(final Long administratorId) {
        this.administratorId = administratorId;
    }

    public Long getCurrentState() {
        return currentState;
    }

    public void setCurrentState(final Long currentState) {
        this.currentState = currentState;
    }

    public Long getBehavior() {
        return behavior;
    }

    public void setBehavior(final Long behavior) {
        this.behavior = behavior;
    }
}
