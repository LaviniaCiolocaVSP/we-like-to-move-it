package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.ICredentialFieldRepository;

public interface BrivoCredentialFieldRepository extends ICredentialFieldRepository {
}
