package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import java.io.Serializable;

@Embeddable
public class BoardPK implements Serializable {
    private long panelOid;
    private short boardNumber;

    @Id
    @Column(name = "panel_oid", nullable = false)
    public long getPanelOid() {
        return panelOid;
    }

    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }

    @Id
    @Column(name = "board_number", nullable = false)
    public short getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(short boardNumber) {
        this.boardNumber = boardNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardPK boardPK = (BoardPK) o;

        if (panelOid != boardPK.panelOid) {
            return false;
        }
        return boardNumber == boardPK.boardNumber;
    }

    @Override
    public int hashCode() {
        int result = (int) (panelOid ^ (panelOid >>> 32));
        result = 31 * result + (int) boardNumber;
        return result;
    }
}
