package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.DeviceSchedule;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DeviceScheduleRepository extends CrudRepository<DeviceSchedule, Long> {
    @Query(
            value = "SELECT * FROM brivo20.device_schedule ds WHERE ds.device_id = :deviceId LIMIT 1",
            nativeQuery = true
    )
    Optional<DeviceSchedule> findByDeviceId(@Param("deviceId") long deviceId);

    @Query(
            value = "DELETE FROM brivo20.device_schedule ds WHERE ds.device_id = :deviceId",
            nativeQuery = true
    )
    @Modifying
    void deleteDeviceSchedulesByDeviceId(@Param("deviceId") long deviceId);
    
}
