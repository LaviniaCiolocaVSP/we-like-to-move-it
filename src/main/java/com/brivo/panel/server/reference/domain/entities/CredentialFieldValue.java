package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "credential_field_value", schema = "brivo20", catalog = "onair")
@IdClass(CredentialFieldValuePK.class)
public class CredentialFieldValue {
    private long accessCredentialId;
    private long credentialFieldId;
    private String value;
    private AccessCredential accessCredentialByAccessCredentialId;
    private CredentialField credentialFieldByCredentialFieldId;

    @Id
    @Column(name = "access_credential_id", nullable = false)
    public long getAccessCredentialId() {
        return accessCredentialId;
    }

    public void setAccessCredentialId(long accessCredentialId) {
        this.accessCredentialId = accessCredentialId;
    }

    @Id
    @Column(name = "credential_field_id", nullable = false)
    public long getCredentialFieldId() {
        return credentialFieldId;
    }

    public void setCredentialFieldId(long credentialFieldId) {
        this.credentialFieldId = credentialFieldId;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 256)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialFieldValue that = (CredentialFieldValue) o;

        if (accessCredentialId != that.accessCredentialId) {
            return false;
        }
        if (credentialFieldId != that.credentialFieldId) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accessCredentialId ^ (accessCredentialId >>> 32));
        result = 31 * result + (int) (credentialFieldId ^ (credentialFieldId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "access_credential_id", referencedColumnName = "access_credential_id", nullable = false, insertable = false, updatable = false)
    public AccessCredential getAccessCredentialByAccessCredentialId() {
        return accessCredentialByAccessCredentialId;
    }

    public void setAccessCredentialByAccessCredentialId(AccessCredential accessCredentialByAccessCredentialId) {
        this.accessCredentialByAccessCredentialId = accessCredentialByAccessCredentialId;
    }

    @ManyToOne
    @JoinColumn(name = "credential_field_id", referencedColumnName = "credential_field_id", nullable = false, insertable = false, updatable = false)
    public CredentialField getCredentialFieldByCredentialFieldId() {
        return credentialFieldByCredentialFieldId;
    }

    public void setCredentialFieldByCredentialFieldId(CredentialField credentialFieldByCredentialFieldId) {
        this.credentialFieldByCredentialFieldId = credentialFieldByCredentialFieldId;
    }
}
