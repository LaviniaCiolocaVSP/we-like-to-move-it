package com.brivo.panel.server.reference.model.schedule;

public enum ScheduleExceptionRepeatWeek {
    NONE(-1, -1),
    FIRST_WEEK(0, 1),
    SECOND_WEEK(1, 2),
    THIRD_WEEK(2, 3),
    FOURTH_WEEK(3, 4),
    FIFTH_WEEK(4, 5);

    private int weekId;
    private int ordinalValue;

    ScheduleExceptionRepeatWeek(int id, int ordinal) {
        this.weekId = id;
        this.ordinalValue = ordinal;
    }

    public static ScheduleExceptionRepeatWeek getScheduleExceptionRepeatWeek(int weekId) {
        ScheduleExceptionRepeatWeek week = ScheduleExceptionRepeatWeek.NONE;

        if (weekId == 0) {
            week = ScheduleExceptionRepeatWeek.FIRST_WEEK;
        } else if (weekId == 1) {
            week = ScheduleExceptionRepeatWeek.SECOND_WEEK;
        } else if (weekId == 2) {
            week = ScheduleExceptionRepeatWeek.THIRD_WEEK;
        } else if (weekId == 3) {
            week = ScheduleExceptionRepeatWeek.FOURTH_WEEK;
        } else if (weekId == 4) {
            week = ScheduleExceptionRepeatWeek.FIFTH_WEEK;
        }

        return week;
    }

    public int getScheduleExceptionRepeatWeekId() {
        return this.weekId;
    }

    public int getOrdinalValue() {
        return this.ordinalValue;
    }
}
