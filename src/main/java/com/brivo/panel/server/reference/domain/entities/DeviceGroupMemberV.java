package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "device_group_member_v", schema = "brivo20", catalog = "onair")
public class DeviceGroupMemberV {
    private Long securityGroupId;
    private Long deviceId;
    private Long deviceObjectId;
    private Long deviceTypeId;
    private Short isIngress;
    private Short isEgress;
    private Long accountId;
    private String name;
    private String description;
    private Timestamp deviceCreated;
    private Timestamp deviceUpdated;
    private Long brainId;
    private Long brainObjectId;
    private Long brainTypeId;
    private Long networkId;
    private Long messageSpecId;
    private String electronicSerialNumber;
    private String hardwareControllerVersion;
    private String firmwareVersion;
    private String physicalAddress;
    private String password;
    private Short isRegistered;
    private String timeZone;
    private Timestamp brainCreated;
    private Timestamp brainUpdated;
    private Long addressId;
    private String addressType;
    private String address1;
    private String address2;
    private String addressTimeZone;
    private String city;
    private String state;
    private String zip;
    private String poBox;
    private String country;
    private Timestamp addressCreated;
    private Timestamp addressUpdated;
    private Long panelId;
    private String firstName;
    private String lastName;
    private String notes;
    private Long brainOpsFlag;
    private Long deviceOpsFlag;
    private Timestamp brainActivated;
    private Timestamp brainDeactivated;
    private Timestamp panelChanged;
    private Timestamp personsChanged;
    private Timestamp schedulesChanged;
    private Timestamp panelChecked;
    private Timestamp personsChecked;
    private Timestamp schedulesChecked;
    private String brainName;

    @Id
    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "device_id", nullable = true)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "device_object_id", nullable = true)
    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public void setDeviceObjectId(Long deviceObjectId) {
        this.deviceObjectId = deviceObjectId;
    }

    @Basic
    @Column(name = "device_type_id", nullable = true)
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    @Basic
    @Column(name = "is_ingress", nullable = true)
    public Short getIsIngress() {
        return isIngress;
    }

    public void setIsIngress(Short isIngress) {
        this.isIngress = isIngress;
    }

    @Basic
    @Column(name = "is_egress", nullable = true)
    public Short getIsEgress() {
        return isEgress;
    }

    public void setIsEgress(Short isEgress) {
        this.isEgress = isEgress;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "device_created", nullable = true)
    public Timestamp getDeviceCreated() {
        return deviceCreated;
    }

    public void setDeviceCreated(Timestamp deviceCreated) {
        this.deviceCreated = deviceCreated;
    }

    @Basic
    @Column(name = "device_updated", nullable = true)
    public Timestamp getDeviceUpdated() {
        return deviceUpdated;
    }

    public void setDeviceUpdated(Timestamp deviceUpdated) {
        this.deviceUpdated = deviceUpdated;
    }

    @Basic
    @Column(name = "brain_id", nullable = true)
    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "brain_object_id", nullable = true)
    public Long getBrainObjectId() {
        return brainObjectId;
    }

    public void setBrainObjectId(Long brainObjectId) {
        this.brainObjectId = brainObjectId;
    }

    @Basic
    @Column(name = "brain_type_id", nullable = true)
    public Long getBrainTypeId() {
        return brainTypeId;
    }

    public void setBrainTypeId(Long brainTypeId) {
        this.brainTypeId = brainTypeId;
    }

    @Basic
    @Column(name = "network_id", nullable = true)
    public Long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }

    @Basic
    @Column(name = "message_spec_id", nullable = true)
    public Long getMessageSpecId() {
        return messageSpecId;
    }

    public void setMessageSpecId(Long messageSpecId) {
        this.messageSpecId = messageSpecId;
    }

    @Basic
    @Column(name = "electronic_serial_number", nullable = true, length = 32)
    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    @Basic
    @Column(name = "hardware_controller_version", nullable = true, length = 10)
    public String getHardwareControllerVersion() {
        return hardwareControllerVersion;
    }

    public void setHardwareControllerVersion(String hardwareControllerVersion) {
        this.hardwareControllerVersion = hardwareControllerVersion;
    }

    @Basic
    @Column(name = "firmware_version", nullable = true, length = 10)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Basic
    @Column(name = "physical_address", nullable = true, length = 128)
    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 30)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "is_registered", nullable = true)
    public Short getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Short isRegistered) {
        this.isRegistered = isRegistered;
    }

    @Basic
    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "brain_created", nullable = true)
    public Timestamp getBrainCreated() {
        return brainCreated;
    }

    public void setBrainCreated(Timestamp brainCreated) {
        this.brainCreated = brainCreated;
    }

    @Basic
    @Column(name = "brain_updated", nullable = true)
    public Timestamp getBrainUpdated() {
        return brainUpdated;
    }

    public void setBrainUpdated(Timestamp brainUpdated) {
        this.brainUpdated = brainUpdated;
    }

    @Basic
    @Column(name = "address_id", nullable = true)
    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "address_type", nullable = true, length = 20)
    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    @Basic
    @Column(name = "address1", nullable = true, length = 64)
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Basic
    @Column(name = "address2", nullable = true, length = 64)
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Basic
    @Column(name = "address_time_zone", nullable = true, length = 32)
    public String getAddressTimeZone() {
        return addressTimeZone;
    }

    public void setAddressTimeZone(String addressTimeZone) {
        this.addressTimeZone = addressTimeZone;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 64)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "state", nullable = true, length = 32)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "zip", nullable = true, length = 10)
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "po_box", nullable = true, length = 10)
    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 32)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "address_created", nullable = true)
    public Timestamp getAddressCreated() {
        return addressCreated;
    }

    public void setAddressCreated(Timestamp addressCreated) {
        this.addressCreated = addressCreated;
    }

    @Basic
    @Column(name = "address_updated", nullable = true)
    public Timestamp getAddressUpdated() {
        return addressUpdated;
    }

    public void setAddressUpdated(Timestamp addressUpdated) {
        this.addressUpdated = addressUpdated;
    }

    @Basic
    @Column(name = "panel_id", nullable = true)
    public Long getPanelId() {
        return panelId;
    }

    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }

    @Basic
    @Column(name = "first_name", nullable = true, length = 30)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = true, length = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "notes", nullable = true, length = 4000)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "brain_ops_flag", nullable = true)
    public Long getBrainOpsFlag() {
        return brainOpsFlag;
    }

    public void setBrainOpsFlag(Long brainOpsFlag) {
        this.brainOpsFlag = brainOpsFlag;
    }

    @Basic
    @Column(name = "device_ops_flag", nullable = true)
    public Long getDeviceOpsFlag() {
        return deviceOpsFlag;
    }

    public void setDeviceOpsFlag(Long deviceOpsFlag) {
        this.deviceOpsFlag = deviceOpsFlag;
    }

    @Basic
    @Column(name = "brain_activated", nullable = true)
    public Timestamp getBrainActivated() {
        return brainActivated;
    }

    public void setBrainActivated(Timestamp brainActivated) {
        this.brainActivated = brainActivated;
    }

    @Basic
    @Column(name = "brain_deactivated", nullable = true)
    public Timestamp getBrainDeactivated() {
        return brainDeactivated;
    }

    public void setBrainDeactivated(Timestamp brainDeactivated) {
        this.brainDeactivated = brainDeactivated;
    }

    @Basic
    @Column(name = "panel_changed", nullable = true)
    public Timestamp getPanelChanged() {
        return panelChanged;
    }

    public void setPanelChanged(Timestamp panelChanged) {
        this.panelChanged = panelChanged;
    }

    @Basic
    @Column(name = "persons_changed", nullable = true)
    public Timestamp getPersonsChanged() {
        return personsChanged;
    }

    public void setPersonsChanged(Timestamp personsChanged) {
        this.personsChanged = personsChanged;
    }

    @Basic
    @Column(name = "schedules_changed", nullable = true)
    public Timestamp getSchedulesChanged() {
        return schedulesChanged;
    }

    public void setSchedulesChanged(Timestamp schedulesChanged) {
        this.schedulesChanged = schedulesChanged;
    }

    @Basic
    @Column(name = "panel_checked", nullable = true)
    public Timestamp getPanelChecked() {
        return panelChecked;
    }

    public void setPanelChecked(Timestamp panelChecked) {
        this.panelChecked = panelChecked;
    }

    @Basic
    @Column(name = "persons_checked", nullable = true)
    public Timestamp getPersonsChecked() {
        return personsChecked;
    }

    public void setPersonsChecked(Timestamp personsChecked) {
        this.personsChecked = personsChecked;
    }

    @Basic
    @Column(name = "schedules_checked", nullable = true)
    public Timestamp getSchedulesChecked() {
        return schedulesChecked;
    }

    public void setSchedulesChecked(Timestamp schedulesChecked) {
        this.schedulesChecked = schedulesChecked;
    }

    @Basic
    @Column(name = "brain_name", nullable = true, length = 32)
    public String getBrainName() {
        return brainName;
    }

    public void setBrainName(String brainName) {
        this.brainName = brainName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceGroupMemberV that = (DeviceGroupMemberV) o;

        if (securityGroupId != null ? !securityGroupId.equals(that.securityGroupId) : that.securityGroupId != null) {
            return false;
        }
        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) {
            return false;
        }
        if (deviceObjectId != null ? !deviceObjectId.equals(that.deviceObjectId) : that.deviceObjectId != null) {
            return false;
        }
        if (deviceTypeId != null ? !deviceTypeId.equals(that.deviceTypeId) : that.deviceTypeId != null) {
            return false;
        }
        if (isIngress != null ? !isIngress.equals(that.isIngress) : that.isIngress != null) {
            return false;
        }
        if (isEgress != null ? !isEgress.equals(that.isEgress) : that.isEgress != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (deviceCreated != null ? !deviceCreated.equals(that.deviceCreated) : that.deviceCreated != null) {
            return false;
        }
        if (deviceUpdated != null ? !deviceUpdated.equals(that.deviceUpdated) : that.deviceUpdated != null) {
            return false;
        }
        if (brainId != null ? !brainId.equals(that.brainId) : that.brainId != null) {
            return false;
        }
        if (brainObjectId != null ? !brainObjectId.equals(that.brainObjectId) : that.brainObjectId != null) {
            return false;
        }
        if (brainTypeId != null ? !brainTypeId.equals(that.brainTypeId) : that.brainTypeId != null) {
            return false;
        }
        if (networkId != null ? !networkId.equals(that.networkId) : that.networkId != null) {
            return false;
        }
        if (messageSpecId != null ? !messageSpecId.equals(that.messageSpecId) : that.messageSpecId != null) {
            return false;
        }
        if (electronicSerialNumber != null ? !electronicSerialNumber.equals(that.electronicSerialNumber) : that.electronicSerialNumber != null) {
            return false;
        }
        if (hardwareControllerVersion != null ? !hardwareControllerVersion.equals(that.hardwareControllerVersion) : that.hardwareControllerVersion != null) {
            return false;
        }
        if (firmwareVersion != null ? !firmwareVersion.equals(that.firmwareVersion) : that.firmwareVersion != null) {
            return false;
        }
        if (physicalAddress != null ? !physicalAddress.equals(that.physicalAddress) : that.physicalAddress != null) {
            return false;
        }
        if (password != null ? !password.equals(that.password) : that.password != null) {
            return false;
        }
        if (isRegistered != null ? !isRegistered.equals(that.isRegistered) : that.isRegistered != null) {
            return false;
        }
        if (timeZone != null ? !timeZone.equals(that.timeZone) : that.timeZone != null) {
            return false;
        }
        if (brainCreated != null ? !brainCreated.equals(that.brainCreated) : that.brainCreated != null) {
            return false;
        }
        if (brainUpdated != null ? !brainUpdated.equals(that.brainUpdated) : that.brainUpdated != null) {
            return false;
        }
        if (addressId != null ? !addressId.equals(that.addressId) : that.addressId != null) {
            return false;
        }
        if (addressType != null ? !addressType.equals(that.addressType) : that.addressType != null) {
            return false;
        }
        if (address1 != null ? !address1.equals(that.address1) : that.address1 != null) {
            return false;
        }
        if (address2 != null ? !address2.equals(that.address2) : that.address2 != null) {
            return false;
        }
        if (addressTimeZone != null ? !addressTimeZone.equals(that.addressTimeZone) : that.addressTimeZone != null) {
            return false;
        }
        if (city != null ? !city.equals(that.city) : that.city != null) {
            return false;
        }
        if (state != null ? !state.equals(that.state) : that.state != null) {
            return false;
        }
        if (zip != null ? !zip.equals(that.zip) : that.zip != null) {
            return false;
        }
        if (poBox != null ? !poBox.equals(that.poBox) : that.poBox != null) {
            return false;
        }
        if (country != null ? !country.equals(that.country) : that.country != null) {
            return false;
        }
        if (addressCreated != null ? !addressCreated.equals(that.addressCreated) : that.addressCreated != null) {
            return false;
        }
        if (addressUpdated != null ? !addressUpdated.equals(that.addressUpdated) : that.addressUpdated != null) {
            return false;
        }
        if (panelId != null ? !panelId.equals(that.panelId) : that.panelId != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (notes != null ? !notes.equals(that.notes) : that.notes != null) {
            return false;
        }
        if (brainOpsFlag != null ? !brainOpsFlag.equals(that.brainOpsFlag) : that.brainOpsFlag != null) {
            return false;
        }
        if (deviceOpsFlag != null ? !deviceOpsFlag.equals(that.deviceOpsFlag) : that.deviceOpsFlag != null) {
            return false;
        }
        if (brainActivated != null ? !brainActivated.equals(that.brainActivated) : that.brainActivated != null) {
            return false;
        }
        if (brainDeactivated != null ? !brainDeactivated.equals(that.brainDeactivated) : that.brainDeactivated != null) {
            return false;
        }
        if (panelChanged != null ? !panelChanged.equals(that.panelChanged) : that.panelChanged != null) {
            return false;
        }
        if (personsChanged != null ? !personsChanged.equals(that.personsChanged) : that.personsChanged != null) {
            return false;
        }
        if (schedulesChanged != null ? !schedulesChanged.equals(that.schedulesChanged) : that.schedulesChanged != null) {
            return false;
        }
        if (panelChecked != null ? !panelChecked.equals(that.panelChecked) : that.panelChecked != null) {
            return false;
        }
        if (personsChecked != null ? !personsChecked.equals(that.personsChecked) : that.personsChecked != null) {
            return false;
        }
        if (schedulesChecked != null ? !schedulesChecked.equals(that.schedulesChecked) : that.schedulesChecked != null) {
            return false;
        }
        return brainName != null ? brainName.equals(that.brainName) : that.brainName == null;
    }

    @Override
    public int hashCode() {
        int result = securityGroupId != null ? securityGroupId.hashCode() : 0;
        result = 31 * result + (deviceId != null ? deviceId.hashCode() : 0);
        result = 31 * result + (deviceObjectId != null ? deviceObjectId.hashCode() : 0);
        result = 31 * result + (deviceTypeId != null ? deviceTypeId.hashCode() : 0);
        result = 31 * result + (isIngress != null ? isIngress.hashCode() : 0);
        result = 31 * result + (isEgress != null ? isEgress.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (deviceCreated != null ? deviceCreated.hashCode() : 0);
        result = 31 * result + (deviceUpdated != null ? deviceUpdated.hashCode() : 0);
        result = 31 * result + (brainId != null ? brainId.hashCode() : 0);
        result = 31 * result + (brainObjectId != null ? brainObjectId.hashCode() : 0);
        result = 31 * result + (brainTypeId != null ? brainTypeId.hashCode() : 0);
        result = 31 * result + (networkId != null ? networkId.hashCode() : 0);
        result = 31 * result + (messageSpecId != null ? messageSpecId.hashCode() : 0);
        result = 31 * result + (electronicSerialNumber != null ? electronicSerialNumber.hashCode() : 0);
        result = 31 * result + (hardwareControllerVersion != null ? hardwareControllerVersion.hashCode() : 0);
        result = 31 * result + (firmwareVersion != null ? firmwareVersion.hashCode() : 0);
        result = 31 * result + (physicalAddress != null ? physicalAddress.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (isRegistered != null ? isRegistered.hashCode() : 0);
        result = 31 * result + (timeZone != null ? timeZone.hashCode() : 0);
        result = 31 * result + (brainCreated != null ? brainCreated.hashCode() : 0);
        result = 31 * result + (brainUpdated != null ? brainUpdated.hashCode() : 0);
        result = 31 * result + (addressId != null ? addressId.hashCode() : 0);
        result = 31 * result + (addressType != null ? addressType.hashCode() : 0);
        result = 31 * result + (address1 != null ? address1.hashCode() : 0);
        result = 31 * result + (address2 != null ? address2.hashCode() : 0);
        result = 31 * result + (addressTimeZone != null ? addressTimeZone.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (poBox != null ? poBox.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (addressCreated != null ? addressCreated.hashCode() : 0);
        result = 31 * result + (addressUpdated != null ? addressUpdated.hashCode() : 0);
        result = 31 * result + (panelId != null ? panelId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (brainOpsFlag != null ? brainOpsFlag.hashCode() : 0);
        result = 31 * result + (deviceOpsFlag != null ? deviceOpsFlag.hashCode() : 0);
        result = 31 * result + (brainActivated != null ? brainActivated.hashCode() : 0);
        result = 31 * result + (brainDeactivated != null ? brainDeactivated.hashCode() : 0);
        result = 31 * result + (panelChanged != null ? panelChanged.hashCode() : 0);
        result = 31 * result + (personsChanged != null ? personsChanged.hashCode() : 0);
        result = 31 * result + (schedulesChanged != null ? schedulesChanged.hashCode() : 0);
        result = 31 * result + (panelChecked != null ? panelChecked.hashCode() : 0);
        result = 31 * result + (personsChecked != null ? personsChecked.hashCode() : 0);
        result = 31 * result + (schedulesChecked != null ? schedulesChecked.hashCode() : 0);
        result = 31 * result + (brainName != null ? brainName.hashCode() : 0);
        return result;
    }
}
