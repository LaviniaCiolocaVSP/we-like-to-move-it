package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiDoorDTO implements Serializable {
    private Long siteId;
    private String siteName;
    private Long accountId;
    private Long deviceId;
    private Long deviceObjectId;
    private String name;
    private Long brainId;

    //panel_id column from device table
    private Long panelId;

    private String panelSerialNumber;
    private String boardDescription;
    private Long boardNumber;
    private Long nodeNumber;
    private Short maxRexExtension;
    private UiDoorDataDTO doorData;
    private String doorUnlockScheduleName;
    private Long twoFactorInterval;
    private String twoFactorSchedule;
    private String cardRequiredSchedule;
    private Long twoFactorScheduleId;
    private Long cardRequiredScheduleId;
    private Long doorUnlockScheduleId;
    private int inOut;
    private boolean controlFromBrowser;

    public UiDoorDTO() {
    }

    public UiDoorDTO(Long siteId, String siteName, Long accountId, Long deviceId, Long deviceObjectId, String name,
                     Long brainId, String panelSerialNumber, String boardDescription,
                     Long boardNumber, Long nodeNumber, UiDoorDataDTO doorData, Short maxRexExtension,
                     String doorUnlockScheduleName, Long doorUnlockScheduleId, Long twoFactorInterval, String twoFactorSchedule,
                     Long twoFactorScheduleId, String cardRequiredSchedule, Long cardRequiredScheduleId, Long panelId, int inOut,
                     boolean controlFromBrowser) {
        this.accountId = accountId;
        this.deviceId = deviceId;
        this.name = name;
        this.deviceObjectId = deviceObjectId;
        this.brainId = brainId;
        this.panelSerialNumber = panelSerialNumber;
        this.boardDescription = boardDescription;
        this.boardNumber = boardNumber;
        this.nodeNumber = nodeNumber;
        this.doorData = doorData;
        this.maxRexExtension = maxRexExtension;
        this.doorUnlockScheduleName = doorUnlockScheduleName;
        this.twoFactorInterval = twoFactorInterval;
        this.twoFactorSchedule = twoFactorSchedule;
        this.cardRequiredSchedule = cardRequiredSchedule;
        this.twoFactorScheduleId = twoFactorScheduleId;
        this.cardRequiredScheduleId = cardRequiredScheduleId;
        this.doorUnlockScheduleId = doorUnlockScheduleId;
        this.panelId = panelId;
        this.inOut = inOut;
        this.siteId = siteId;
        this.siteName = siteName;
        this.controlFromBrowser = controlFromBrowser;
    }

    public Long getSiteId() {
        return siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public Long getPanelId() {
        return panelId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public String getBoardDescription() {
        return boardDescription;
    }

    public Long getBoardNumber() {
        return boardNumber;
    }

    public Long getNodeNumber() {
        return nodeNumber;
    }

    public String getName() {
        return name;
    }

    public Long getBrainId() {
        return brainId;
    }

    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public UiDoorDataDTO getDoorData() {
        return doorData;
    }

    public Short getMaxRexExtension() {
        return maxRexExtension;
    }

    public String getDoorUnlockScheduleName() {
        return doorUnlockScheduleName;
    }

    public Long getTwoFactorInterval() {
        return twoFactorInterval;
    }

    public String getTwoFactorSchedule() {
        return twoFactorSchedule;
    }

    public String getCardRequiredSchedule() {
        return cardRequiredSchedule;
    }

    public Long getTwoFactorScheduleId() {
        return twoFactorScheduleId;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public Long getDoorUnlockScheduleId() {
        return doorUnlockScheduleId;
    }

    public int getInOut() {
        return inOut;
    }

    public boolean getControlFromBrowser() {
        return controlFromBrowser;
    }

    public static class UiDoorDataDTO {
        private Boolean notifyOnAjar;
        private Long doorAjar;
        private Long maxInvalid;
        private Long invalidShutout;
        private Long passthrough;
        private Boolean notifyOnForced;
        private Boolean rexFiresDoor;
        private Boolean useAlarmShunt;
        private Long alarmDelay;

        public UiDoorDataDTO(Boolean notifyOnAjar, Long doorAjar, Long maxInvalid, Long invalidShutout, Long passthrough,
                             Boolean notifyOnForced, Boolean rexFiresDoor, Boolean useAlarmShunt, Long alarmDelay) {
            this.notifyOnAjar = notifyOnAjar;
            this.doorAjar = doorAjar;
            this.maxInvalid = maxInvalid;
            this.invalidShutout = invalidShutout;
            this.passthrough = passthrough;
            this.notifyOnForced = notifyOnForced;
            this.rexFiresDoor = rexFiresDoor;
            this.useAlarmShunt = useAlarmShunt;
            this.alarmDelay = alarmDelay;
        }

        public UiDoorDataDTO() {
        }

        public Boolean getNotifyOnAjar() {
            return notifyOnAjar;
        }

        public Long getDoorAjar() {
            return doorAjar;
        }

        public Long getMaxInvalid() {
            return maxInvalid;
        }

        public Long getInvalidShutout() {
            return invalidShutout;
        }

        public Long getPassthrough() {
            return passthrough;
        }

        public Boolean getNotifyOnForced() {
            return notifyOnForced;
        }

        public Boolean getRexFiresDoor() {
            return rexFiresDoor;
        }

        public Boolean getUseAlarmShunt() {
            return useAlarmShunt;
        }

        public Long getAlarmDelay() {
            return alarmDelay;
        }
    }
}
