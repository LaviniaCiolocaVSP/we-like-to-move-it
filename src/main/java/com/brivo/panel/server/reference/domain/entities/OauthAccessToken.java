package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Arrays;

@Entity
@Table(name = "oauth_access_token", schema = "brivo20", catalog = "onair")
public class OauthAccessToken {
    private String tokenId;
    private byte[] token;
    private String authenticationId;
    private byte[] authentication;
    private Long userObjectId;
    private String clientId;
    private String refreshTokenId;
    private OauthClientDetails oauthClientDetailsByClientId;
    private OauthRefreshToken oauthRefreshTokenByRefreshTokenId;

    @Id
    @Column(name = "token_id", nullable = false, length = 40)
    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    @Basic
    @Column(name = "token", nullable = true)
    public byte[] getToken() {
        return token;
    }

    public void setToken(byte[] token) {
        this.token = token;
    }

    @Basic
    @Column(name = "authentication_id", nullable = true, length = 40)
    public String getAuthenticationId() {
        return authenticationId;
    }

    public void setAuthenticationId(String authenticationId) {
        this.authenticationId = authenticationId;
    }

    @Basic
    @Column(name = "authentication", nullable = true)
    public byte[] getAuthentication() {
        return authentication;
    }

    public void setAuthentication(byte[] authentication) {
        this.authentication = authentication;
    }

    @Basic
    @Column(name = "user_object_id", nullable = true)
    public Long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(Long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "client_id", nullable = true, length = 40)
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "refresh_token_id", nullable = true, length = 50)
    public String getRefreshTokenId() {
        return refreshTokenId;
    }

    public void setRefreshTokenId(String refreshTokenId) {
        this.refreshTokenId = refreshTokenId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OauthAccessToken that = (OauthAccessToken) o;

        if (tokenId != null ? !tokenId.equals(that.tokenId) : that.tokenId != null) {
            return false;
        }
        if (!Arrays.equals(token, that.token)) {
            return false;
        }
        if (authenticationId != null ? !authenticationId.equals(that.authenticationId) : that.authenticationId != null) {
            return false;
        }
        if (!Arrays.equals(authentication, that.authentication)) {
            return false;
        }
        if (userObjectId != null ? !userObjectId.equals(that.userObjectId) : that.userObjectId != null) {
            return false;
        }
        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) {
            return false;
        }
        return refreshTokenId != null ? refreshTokenId.equals(that.refreshTokenId) : that.refreshTokenId == null;
    }

    @Override
    public int hashCode() {
        int result = tokenId != null ? tokenId.hashCode() : 0;
        result = 31 * result + Arrays.hashCode(token);
        result = 31 * result + (authenticationId != null ? authenticationId.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(authentication);
        result = 31 * result + (userObjectId != null ? userObjectId.hashCode() : 0);
        result = 31 * result + (clientId != null ? clientId.hashCode() : 0);
        result = 31 * result + (refreshTokenId != null ? refreshTokenId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "client_id", referencedColumnName = "client_id", insertable = false, updatable = false)
    public OauthClientDetails getOauthClientDetailsByClientId() {
        return oauthClientDetailsByClientId;
    }

    public void setOauthClientDetailsByClientId(OauthClientDetails oauthClientDetailsByClientId) {
        this.oauthClientDetailsByClientId = oauthClientDetailsByClientId;
    }

    @ManyToOne
    @JoinColumn(name = "refresh_token_id", referencedColumnName = "token_id", insertable = false, updatable = false)
    public OauthRefreshToken getOauthRefreshTokenByRefreshTokenId() {
        return oauthRefreshTokenByRefreshTokenId;
    }

    public void setOauthRefreshTokenByRefreshTokenId(OauthRefreshToken oauthRefreshTokenByRefreshTokenId) {
        this.oauthRefreshTokenByRefreshTokenId = oauthRefreshTokenByRefreshTokenId;
    }
}
