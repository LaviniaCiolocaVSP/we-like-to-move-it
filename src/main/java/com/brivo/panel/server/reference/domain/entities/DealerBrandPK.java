package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DealerBrandPK implements Serializable {
    private long accountId;
    private long brandId;

    @Column(name = "account_id", nullable = false)
    @Id
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "brand_id", nullable = false)
    @Id
    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DealerBrandPK that = (DealerBrandPK) o;

        if (accountId != that.accountId) {
            return false;
        }
        return brandId == that.brandId;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (brandId ^ (brandId >>> 32));
        return result;
    }
}
