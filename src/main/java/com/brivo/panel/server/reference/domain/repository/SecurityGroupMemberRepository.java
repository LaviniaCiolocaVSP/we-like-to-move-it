package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface SecurityGroupMemberRepository extends CrudRepository<SecurityGroupMember, Long> {
    Optional<List<SecurityGroupMember>> findByObjectId(@Param("objectId") long objectId);
}
