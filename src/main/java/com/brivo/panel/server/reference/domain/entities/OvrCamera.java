package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "ovr_camera", schema = "brivo20", catalog = "onair")
public class OvrCamera {
    private long cameraId;
    private long objectId;
    private long accountId;
    private long siteId;
    private String name;
    private long cameraModelId;
    private String serialNumber;
    private String authKey;
    private String username;
    private String password;
    private short deleted;
    private Timestamp created;
    private Timestamp updated;
    private Long ovrCameraSubscriptionId;
    private long avhsServerId;
    private Collection<CameraDeviceMapping> cameraDeviceMappingsByCameraId;
    private Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByCameraId;
    private CameraMigrationCredential cameraMigrationCredentialByCameraId;
    private Collection<CameraMigrationHistory> cameraMigrationHistoriesByCameraId;
    private CameraMotionStatus cameraMotionStatusByCameraId;
    private Collection<CameraParam> cameraParamsByCameraId;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    private SecurityGroup securityGroupBySiteId;
    private CameraModel cameraModelByCameraModelId;
    private OvrCameraSubscription ovrCameraSubscriptionByOvrCameraSubscriptionId;
    private AvhsServer avhsServerByAvhsServerId;

    @Id
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "site_id", nullable = false)
    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "camera_model_id", nullable = false)
    public long getCameraModelId() {
        return cameraModelId;
    }

    public void setCameraModelId(long cameraModelId) {
        this.cameraModelId = cameraModelId;
    }

    @Basic
    @Column(name = "serial_number", nullable = false, length = 32)
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Basic
    @Column(name = "auth_key", nullable = false, length = 32)
    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 32)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 32)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "ovr_camera_subscription_id", nullable = true)
    public Long getOvrCameraSubscriptionId() {
        return ovrCameraSubscriptionId;
    }

    public void setOvrCameraSubscriptionId(Long ovrCameraSubscriptionId) {
        this.ovrCameraSubscriptionId = ovrCameraSubscriptionId;
    }

    @Basic
    @Column(name = "avhs_server_id", nullable = false)
    public long getAvhsServerId() {
        return avhsServerId;
    }

    public void setAvhsServerId(long avhsServerId) {
        this.avhsServerId = avhsServerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OvrCamera ovrCamera = (OvrCamera) o;

        if (cameraId != ovrCamera.cameraId) {
            return false;
        }
        if (objectId != ovrCamera.objectId) {
            return false;
        }
        if (accountId != ovrCamera.accountId) {
            return false;
        }
        if (siteId != ovrCamera.siteId) {
            return false;
        }
        if (cameraModelId != ovrCamera.cameraModelId) {
            return false;
        }
        if (deleted != ovrCamera.deleted) {
            return false;
        }
        if (avhsServerId != ovrCamera.avhsServerId) {
            return false;
        }
        if (name != null ? !name.equals(ovrCamera.name) : ovrCamera.name != null) {
            return false;
        }
        if (serialNumber != null ? !serialNumber.equals(ovrCamera.serialNumber) : ovrCamera.serialNumber != null) {
            return false;
        }
        if (authKey != null ? !authKey.equals(ovrCamera.authKey) : ovrCamera.authKey != null) {
            return false;
        }
        if (username != null ? !username.equals(ovrCamera.username) : ovrCamera.username != null) {
            return false;
        }
        if (password != null ? !password.equals(ovrCamera.password) : ovrCamera.password != null) {
            return false;
        }
        if (created != null ? !created.equals(ovrCamera.created) : ovrCamera.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(ovrCamera.updated) : ovrCamera.updated != null) {
            return false;
        }
        return ovrCameraSubscriptionId != null ? ovrCameraSubscriptionId.equals(ovrCamera.ovrCameraSubscriptionId) : ovrCamera.ovrCameraSubscriptionId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (siteId ^ (siteId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (cameraModelId ^ (cameraModelId >>> 32));
        result = 31 * result + (serialNumber != null ? serialNumber.hashCode() : 0);
        result = 31 * result + (authKey != null ? authKey.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (int) deleted;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (ovrCameraSubscriptionId != null ? ovrCameraSubscriptionId.hashCode() : 0);
        result = 31 * result + (int) (avhsServerId ^ (avhsServerId >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "ovrCameraByCameraId")
    public Collection<CameraDeviceMapping> getCameraDeviceMappingsByCameraId() {
        return cameraDeviceMappingsByCameraId;
    }

    public void setCameraDeviceMappingsByCameraId(Collection<CameraDeviceMapping> cameraDeviceMappingsByCameraId) {
        this.cameraDeviceMappingsByCameraId = cameraDeviceMappingsByCameraId;
    }

    @OneToMany(mappedBy = "ovrCameraByOvrCameraId")
    public Collection<CameraGroupOvrCamera> getCameraGroupOvrCamerasByCameraId() {
        return cameraGroupOvrCamerasByCameraId;
    }

    public void setCameraGroupOvrCamerasByCameraId(Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByCameraId) {
        this.cameraGroupOvrCamerasByCameraId = cameraGroupOvrCamerasByCameraId;
    }

    @OneToOne(mappedBy = "ovrCameraByCameraId")
    public CameraMigrationCredential getCameraMigrationCredentialByCameraId() {
        return cameraMigrationCredentialByCameraId;
    }

    public void setCameraMigrationCredentialByCameraId(CameraMigrationCredential cameraMigrationCredentialByCameraId) {
        this.cameraMigrationCredentialByCameraId = cameraMigrationCredentialByCameraId;
    }

    @OneToMany(mappedBy = "ovrCameraByCameraId")
    public Collection<CameraMigrationHistory> getCameraMigrationHistoriesByCameraId() {
        return cameraMigrationHistoriesByCameraId;
    }

    public void setCameraMigrationHistoriesByCameraId(Collection<CameraMigrationHistory> cameraMigrationHistoriesByCameraId) {
        this.cameraMigrationHistoriesByCameraId = cameraMigrationHistoriesByCameraId;
    }

    @OneToOne(mappedBy = "ovrCameraByCameraId")
    public CameraMotionStatus getCameraMotionStatusByCameraId() {
        return cameraMotionStatusByCameraId;
    }

    public void setCameraMotionStatusByCameraId(CameraMotionStatus cameraMotionStatusByCameraId) {
        this.cameraMotionStatusByCameraId = cameraMotionStatusByCameraId;
    }

    @OneToMany(mappedBy = "ovrCameraByCameraId")
    public Collection<CameraParam> getCameraParamsByCameraId() {
        return cameraParamsByCameraId;
    }

    public void setCameraParamsByCameraId(Collection<CameraParam> cameraParamsByCameraId) {
        this.cameraParamsByCameraId = cameraParamsByCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "site_id", referencedColumnName = "security_group_id", nullable = false, insertable = false, updatable = false)
    public SecurityGroup getSecurityGroupBySiteId() {
        return securityGroupBySiteId;
    }

    public void setSecurityGroupBySiteId(SecurityGroup securityGroupBySiteId) {
        this.securityGroupBySiteId = securityGroupBySiteId;
    }

    @ManyToOne
    @JoinColumn(name = "camera_model_id", referencedColumnName = "camera_model_id", nullable = false, insertable = false, updatable = false)
    public CameraModel getCameraModelByCameraModelId() {
        return cameraModelByCameraModelId;
    }

    public void setCameraModelByCameraModelId(CameraModel cameraModelByCameraModelId) {
        this.cameraModelByCameraModelId = cameraModelByCameraModelId;
    }

    @ManyToOne
    @JoinColumn(name = "ovr_camera_subscription_id", referencedColumnName = "ovr_camera_subscription_id", insertable = false, updatable = false)
    public OvrCameraSubscription getOvrCameraSubscriptionByOvrCameraSubscriptionId() {
        return ovrCameraSubscriptionByOvrCameraSubscriptionId;
    }

    public void setOvrCameraSubscriptionByOvrCameraSubscriptionId(OvrCameraSubscription ovrCameraSubscriptionByOvrCameraSubscriptionId) {
        this.ovrCameraSubscriptionByOvrCameraSubscriptionId = ovrCameraSubscriptionByOvrCameraSubscriptionId;
    }

    @ManyToOne
    @JoinColumn(name = "avhs_server_id", referencedColumnName = "avhs_server_id", nullable = false, insertable = false, updatable = false)
    public AvhsServer getAvhsServerByAvhsServerId() {
        return avhsServerByAvhsServerId;
    }

    public void setAvhsServerByAvhsServerId(AvhsServer avhsServerByAvhsServerId) {
        this.avhsServerByAvhsServerId = avhsServerByAvhsServerId;
    }
}
