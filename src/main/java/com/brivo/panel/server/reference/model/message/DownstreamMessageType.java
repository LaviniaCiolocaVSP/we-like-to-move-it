package com.brivo.panel.server.reference.model.message;

import com.brivo.panel.server.reference.model.message.down.DisengageDeviceOutputMessage;
import com.brivo.panel.server.reference.model.message.down.EngageDeviceOutputMessage;
import com.brivo.panel.server.reference.model.message.down.FlushDataMessage;
import com.brivo.panel.server.reference.model.message.down.FollowScheduleMessage;
import com.brivo.panel.server.reference.model.message.down.HeartbeatMessageDown;
import com.brivo.panel.server.reference.model.message.down.RequestPanelLogMessage;
import com.brivo.panel.server.reference.model.message.down.UpdateCredentialMessage;
import com.brivo.panel.server.reference.model.message.down.UpgradeFirmwareMessage;
import com.brivo.panel.server.reference.model.message.down.PulseRequestMessage;
import com.brivo.panel.server.reference.model.message.down.TriggerDeviceOutputMessage;
import com.brivo.panel.server.reference.model.message.down.DeleteCredentialMessage;
import com.brivo.panel.server.reference.model.message.down.SetDeviceOutputBehaviorMessage;
import com.brivo.panel.server.reference.model.message.down.SetDeviceThreatLevelMessage;
import com.brivo.panel.server.reference.model.message.down.AuthorizeCredentialMessage;
import com.brivo.panel.server.reference.model.message.down.UnlatchDeviceMessage;
import com.brivo.panel.server.reference.model.message.down.ExecuteCommandMessage;
import com.brivo.panel.server.reference.model.message.down.ToggleDeviceMessage;
import com.brivo.panel.server.reference.model.message.down.IgnoreScheduleMessage;
import com.brivo.panel.server.reference.model.message.down.SetScheduleThreatLevelMessage;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum DownstreamMessageType {
    Flush_Data_Channel("Flush_Data_Channel", FlushDataMessage.class),
    Heartbeat("Heartbeat", HeartbeatMessageDown.class),
    Request_Panel_Log("Request_Panel_Log", RequestPanelLogMessage.class),
    Upgrade_Firmware("Upgrade_Firmware", UpgradeFirmwareMessage.class),
    Pulse_Device("Pulse_Device", PulseRequestMessage.class),
    TRIGGER_DEVICE_OUTPUT("Trigger_Device_Output", TriggerDeviceOutputMessage.class),
    DeleteCredential("DeleteCredential", DeleteCredentialMessage.class),
    SetDeviceOutputBehaviorMessage("Device_Output_Behavior", SetDeviceOutputBehaviorMessage.class),
    SetDeviceThreatLevelMessage("Device_Threat_Level", SetDeviceThreatLevelMessage.class),
    AuthorizeCredentialMessage("Authorize_Credential", AuthorizeCredentialMessage.class),
    Unlatch_Device("Unlatch_Device", UnlatchDeviceMessage.class),
    DisengageDeviceOutputMessage("Disengage_Device_Output", DisengageDeviceOutputMessage.class),
    ExecuteCommandMessage("Execute_Command", ExecuteCommandMessage.class),
    EngageDeviceOutputMessage("Engage_Device_Output", EngageDeviceOutputMessage.class),
    FollowScheduleMessage("Follow_Schedule", FollowScheduleMessage.class),
    ToggleDeviceMessage("Toggle_Device", ToggleDeviceMessage.class),
    IgnoreScheduleMessage("Ignore_Schedule", IgnoreScheduleMessage.class),
    UpdateCredentialMessage("UpdateCredential", UpdateCredentialMessage.class),
    SetScheduleThreatLevelMessage("Schedule_Threat_Level", SetScheduleThreatLevelMessage.class);

    private static final Map<String, DownstreamMessageType> messageTypeMap;

    static {
        messageTypeMap = Stream.of(DownstreamMessageType.values())
                               .collect(Collectors.toMap(DownstreamMessageType::getMessageType, Function.identity()));
    }

    private final String messageType;
    private final Class<? extends DownstreamMessage> messageClass;

    DownstreamMessageType(String messageType,
                          Class<? extends DownstreamMessage> messageClass) {
        this.messageType = messageType;
        this.messageClass = messageClass;
    }

    @JsonCreator
    public static DownstreamMessageType lookupMessageTypeByType(String messageType) {
        return messageTypeMap.get(messageType.toLowerCase());
    }

    public Class<? extends DownstreamMessage> getMessageClass() {
        return messageClass;
    }

    @JsonValue
    public String getMessageType() {
        return messageType;
    }

}
