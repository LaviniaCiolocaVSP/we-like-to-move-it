package com.brivo.panel.server.reference.dao.event;

import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.dao.device.DeviceQuery;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class EventDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(EventDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteAllEvents() {
        LOGGER.info("Deleting events");

        String query = EventQuery.DELETE_ALL_EVENTS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();

        jdbcTemplate.update(query, paramMap);
    }

}
