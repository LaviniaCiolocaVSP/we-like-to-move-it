package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class Camera {
    private long cameraId;
    private String name;
    private String num;
    private long dvrId;
    private Long deviceId;
    private String authKey;
    private Timestamp configVersion;
    private Long cameraTypeId;
    private Timestamp created;
    private Dvr dvrByDvrId;
    //    private Device deviceByDeviceId;
    private CameraType cameraTypeByCameraTypeId;

    @Id
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 35)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "num", nullable = false, length = 50)
    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    @Basic
    @Column(name = "dvr_id", nullable = false)
    public long getDvrId() {
        return dvrId;
    }

    public void setDvrId(long dvrId) {
        this.dvrId = dvrId;
    }

    @Basic
    @Column(name = "device_id", nullable = true)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "auth_key", nullable = true, length = 50)
    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    @Basic
    @Column(name = "config_version", nullable = true)
    public Timestamp getConfigVersion() {
        return configVersion;
    }

    public void setConfigVersion(Timestamp configVersion) {
        this.configVersion = configVersion;
    }

    @Basic
    @Column(name = "camera_type_id", nullable = true)
    public Long getCameraTypeId() {
        return cameraTypeId;
    }

    public void setCameraTypeId(Long cameraTypeId) {
        this.cameraTypeId = cameraTypeId;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Camera camera = (Camera) o;

        if (cameraId != camera.cameraId) {
            return false;
        }
        if (dvrId != camera.dvrId) {
            return false;
        }
        if (name != null ? !name.equals(camera.name) : camera.name != null) {
            return false;
        }
        if (num != null ? !num.equals(camera.num) : camera.num != null) {
            return false;
        }
        if (deviceId != null ? !deviceId.equals(camera.deviceId) : camera.deviceId != null) {
            return false;
        }
        if (authKey != null ? !authKey.equals(camera.authKey) : camera.authKey != null) {
            return false;
        }
        if (configVersion != null ? !configVersion.equals(camera.configVersion) : camera.configVersion != null) {
            return false;
        }
        if (cameraTypeId != null ? !cameraTypeId.equals(camera.cameraTypeId) : camera.cameraTypeId != null) {
            return false;
        }
        return created != null ? created.equals(camera.created) : camera.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (num != null ? num.hashCode() : 0);
        result = 31 * result + (int) (dvrId ^ (dvrId >>> 32));
        result = 31 * result + (deviceId != null ? deviceId.hashCode() : 0);
        result = 31 * result + (authKey != null ? authKey.hashCode() : 0);
        result = 31 * result + (configVersion != null ? configVersion.hashCode() : 0);
        result = 31 * result + (cameraTypeId != null ? cameraTypeId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "dvr_id", referencedColumnName = "dvr_id", nullable = false, insertable = false, updatable = false)
    public Dvr getDvrByDvrId() {
        return dvrByDvrId;
    }

    public void setDvrByDvrId(Dvr dvrByDvrId) {
        this.dvrByDvrId = dvrByDvrId;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "device_id", insertable = false, updatable = false)
    public Device getDeviceByDeviceId() {
        return deviceByDeviceId;
    }

    public void setDeviceByDeviceId(Device deviceByDeviceId) {
        this.deviceByDeviceId = deviceByDeviceId;
    }
    */

    @ManyToOne
    @JoinColumn(name = "camera_type_id", referencedColumnName = "camera_type_id", insertable = false, updatable = false)
    public CameraType getCameraTypeByCameraTypeId() {
        return cameraTypeByCameraTypeId;
    }

    public void setCameraTypeByCameraTypeId(CameraType cameraTypeByCameraTypeId) {
        this.cameraTypeByCameraTypeId = cameraTypeByCameraTypeId;
    }
}
