package com.brivo.panel.server.reference.auth;

import com.brivo.panel.server.reference.model.hardware.Panel;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

@Component
public class PanelPrincipal implements UserDetails {
    private static final long serialVersionUID = -6592858883058100914L;

    private Panel panel;

    public PanelPrincipal() {
    }

    public PanelPrincipal(Panel panel) {
        this.panel = panel;
    }

    // The below are required by UserDetails and not relevant to our situation
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        ArrayList<GrantedAuthority> ga = new ArrayList<GrantedAuthority>();
        ga.add(new SimpleGrantedAuthority("ROLE_PANEL"));
        return Collections.unmodifiableList(ga);
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public String getUsername() {
        return "";
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(Panel panel) {
        this.panel = panel;
    }

}
