package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class AllegionLock {
    private Long deviceId;
    private Integer lockId;
    private Long unlockScheduleId;
    private Integer passthroughDelay;
    private Integer doorAjarDuration;
    private Boolean reportDoorAjar;
    private Boolean privacyModeOn;
    private List<EnterPermission> enterPermissions;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getLockId() {
        return lockId;
    }

    public void setLockId(Integer lockId) {
        this.lockId = lockId;
    }

    public Long getUnlockScheduleId() {
        return unlockScheduleId;
    }

    public void setUnlockScheduleId(Long unlockScheduleId) {
        this.unlockScheduleId = unlockScheduleId;
    }

    public Integer getPassthroughDelay() {
        return passthroughDelay;
    }

    public void setPassthroughDelay(Integer passthroughDelay) {
        this.passthroughDelay = passthroughDelay;
    }

    public Integer getDoorAjarDuration() {
        return doorAjarDuration;
    }

    public void setDoorAjarDuration(Integer doorAjarDuration) {
        this.doorAjarDuration = doorAjarDuration;
    }

    public Boolean getReportDoorAjar() {
        return reportDoorAjar;
    }

    public void setReportDoorAjar(Boolean reportDoorAjar) {
        this.reportDoorAjar = reportDoorAjar;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public Boolean getPrivacyModeOn() {
        return privacyModeOn;
    }

    public void setPrivacyModeOn(Boolean privacyModeOn) {
        this.privacyModeOn = privacyModeOn;
    }
}
