package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.SecurityGroupType;
import org.springframework.data.repository.CrudRepository;

public interface SecurityGroupTypeRepository extends CrudRepository<SecurityGroupType, Long> {

}
