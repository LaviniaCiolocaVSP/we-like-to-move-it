package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class ElevatorFloorMapPK implements Serializable {
    private long elevatorOid;
    private long floorOid;

    @Column(name = "elevator_oid", nullable = false)
    public long getElevatorOid() {
        return elevatorOid;
    }

    public void setElevatorOid(long elevatorOid) {
        this.elevatorOid = elevatorOid;
    }

    @Column(name = "floor_oid", nullable = false)
    public long getFloorOid() {
        return floorOid;
    }

    public void setFloorOid(long floorOid) {
        this.floorOid = floorOid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ElevatorFloorMapPK that = (ElevatorFloorMapPK) o;

        if (elevatorOid != that.elevatorOid) {
            return false;
        }
        return floorOid == that.floorOid;
    }

    @Override
    public int hashCode() {
        int result = (int) (elevatorOid ^ (elevatorOid >>> 32));
        result = 31 * result + (int) (floorOid ^ (floorOid >>> 32));
        return result;
    }
}
