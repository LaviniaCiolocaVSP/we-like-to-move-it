package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
public class Application {
    private long objectId;
    private long applicationId;
    private String description;
    private long accountId;
    private String name;
    private Timestamp created;
    private Timestamp updated;
    private Timestamp enabled;
    private Timestamp disabled;
    private short deleted;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    private Collection<ApplicationAccount> applicationAccountsByApplicationId;
    private Collection<ApplicationBlackList> applicationBlackListsByApplicationId;
    private Collection<ApplicationKey> applicationKeysByApplicationId;
    private Collection<LoginSession> loginSessionsByApplicationId;

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Id
    @Column(name = "application_id", nullable = false)
    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "enabled", nullable = false)
    public Timestamp getEnabled() {
        return enabled;
    }

    public void setEnabled(Timestamp enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "disabled", nullable = true)
    public Timestamp getDisabled() {
        return disabled;
    }

    public void setDisabled(Timestamp disabled) {
        this.disabled = disabled;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Application that = (Application) o;

        if (objectId != that.objectId) {
            return false;
        }
        if (applicationId != that.applicationId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (enabled != null ? !enabled.equals(that.enabled) : that.enabled != null) {
            return false;
        }
        return disabled != null ? disabled.equals(that.disabled) : that.disabled == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (applicationId ^ (applicationId >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        result = 31 * result + (disabled != null ? disabled.hashCode() : 0);
        result = 31 * result + (int) deleted;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "applicationByApplicationId")
    public Collection<ApplicationAccount> getApplicationAccountsByApplicationId() {
        return applicationAccountsByApplicationId;
    }

    public void setApplicationAccountsByApplicationId(Collection<ApplicationAccount> applicationAccountsByApplicationId) {
        this.applicationAccountsByApplicationId = applicationAccountsByApplicationId;
    }

    @OneToMany(mappedBy = "applicationByApplicationId")
    public Collection<ApplicationBlackList> getApplicationBlackListsByApplicationId() {
        return applicationBlackListsByApplicationId;
    }

    public void setApplicationBlackListsByApplicationId(Collection<ApplicationBlackList> applicationBlackListsByApplicationId) {
        this.applicationBlackListsByApplicationId = applicationBlackListsByApplicationId;
    }

    @OneToMany(mappedBy = "applicationByApplicationId")
    public Collection<ApplicationKey> getApplicationKeysByApplicationId() {
        return applicationKeysByApplicationId;
    }

    public void setApplicationKeysByApplicationId(Collection<ApplicationKey> applicationKeysByApplicationId) {
        this.applicationKeysByApplicationId = applicationKeysByApplicationId;
    }

    @OneToMany(mappedBy = "applicationByApplicationId")
    public Collection<LoginSession> getLoginSessionsByApplicationId() {
        return loginSessionsByApplicationId;
    }

    public void setLoginSessionsByApplicationId(Collection<LoginSession> loginSessionsByApplicationId) {
        this.loginSessionsByApplicationId = loginSessionsByApplicationId;
    }
}
