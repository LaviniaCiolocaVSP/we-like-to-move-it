package com.brivo.panel.server.reference.dto.ui;

public class UiGroupAntipassbackDTO {
    private Long resetTime;
    private Boolean immuneToAntipassback;

    public UiGroupAntipassbackDTO() {
    }

    public UiGroupAntipassbackDTO(Long resetTime, Boolean immuneToAntipassback) {
        this.resetTime = resetTime;
        this.immuneToAntipassback = immuneToAntipassback;
    }

    public Long getResetTime() {
        return resetTime;
    }

    public Boolean getImmuneToAntipassback() {
        return immuneToAntipassback;
    }

    public void setResetTime(Long resetTime) {
        this.resetTime = resetTime;
    }

    public void setImmuneToAntipassback(Boolean immuneToAntipassback) {
        this.immuneToAntipassback = immuneToAntipassback;
    }

    @Override
    public String toString() {
        return "UiGroupAntipassbackDTO{" +
                "resetTime=" + resetTime +
                ", immuneToAntipassback=" + immuneToAntipassback +
                '}';
    }
}
