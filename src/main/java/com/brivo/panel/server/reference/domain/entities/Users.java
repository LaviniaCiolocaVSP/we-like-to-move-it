package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
public class Users implements Serializable {
    private long usersId;
    private String username;
    private long accountId;
    private String title;
    private String firstName;
    private String middleName;
    private String lastName;
    private String suffix;
    private String password;
    private String hint;
    private String lastFourSsn;
    private String mothersMaidenName;
    private String timeZone;
    private String usersStatus;
    private String note;
    private short displayUserTime;
    private String securityQuestion;
    private String securityAnswer;
    private Short disabled;
    private LocalDateTime created;
    private LocalDateTime updated;
    private String imageDir;
    private Long localizedTemplateId;
    private String externalId;
    private Short owner;
    private Long userImageId;
    private Short firstLogin;
    private Timestamp lastLoginTime;
    private LocalDateTime deactivated;
    //    private BadgePrintJob badgePrintJobByObjectId;
    private Collection<LoginSession> loginSessionsByUsersId;
    private Collection<LoginSession> loginSessionsByUsersId_0;
    //    private MobileInvitation mobileInvitationByObjectId;
    private OauthServiceToken oauthServiceTokenByObjectId;
    //    private UserPrintStatus userPrintStatusByObjectId;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    private UserType userTypeByUserTypeId;
    private UserImage userImageByUserImageId;

    @Id
    @Column(name = "users_id", nullable = false)
    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    @Column(name = "account_id", nullable = false, insertable = false, updatable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "username", nullable = true, length = 36)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Column(name = "title", nullable = true, length = 10)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Column(name = "first_name", nullable = false, length = 35)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "middle_name", nullable = true, length = 35)
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    @Column(name = "last_name", nullable = false, length = 35)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "suffix", nullable = true, length = 10)
    public String getSuffix() {
        return suffix;
    }

    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @Column(name = "password", nullable = true, length = 32)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "hint", nullable = true, length = 64)
    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

    @Basic
    @Column(name = "last_four_ssn", nullable = true, length = 4)
    public String getLastFourSsn() {
        return lastFourSsn;
    }

    public void setLastFourSsn(String lastFourSsn) {
        this.lastFourSsn = lastFourSsn;
    }

    @Column(name = "mothers_maiden_name", nullable = true, length = 32)
    public String getMothersMaidenName() {
        return mothersMaidenName;
    }

    public void setMothersMaidenName(String mothersMaidenName) {
        this.mothersMaidenName = mothersMaidenName;
    }

    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Column(name = "users_status", nullable = true, length = 32)
    public String getUsersStatus() {
        return usersStatus;
    }

    public void setUsersStatus(String usersStatus) {
        this.usersStatus = usersStatus;
    }

    @Column(name = "note", nullable = true, length = 4000)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Column(name = "display_user_time", nullable = false)
    public short getDisplayUserTime() {
        return displayUserTime;
    }

    public void setDisplayUserTime(short displayUserTime) {
        this.displayUserTime = displayUserTime;
    }

    @Column(name = "security_question", nullable = true, length = 512)
    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    @Column(name = "security_answer", nullable = true, length = 512)
    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    @Column(name = "disabled", nullable = true)
    public Short getDisabled() {
        return disabled;
    }

    public void setDisabled(Short disabled) {
        this.disabled = disabled;
    }

    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Column(name = "image_dir", nullable = true, length = 200)
    public String getImageDir() {
        return imageDir;
    }

    public void setImageDir(String imageDir) {
        this.imageDir = imageDir;
    }

    @Column(name = "localized_template_id", nullable = true)
    public Long getLocalizedTemplateId() {
        return localizedTemplateId;
    }

    public void setLocalizedTemplateId(Long localizedTemplateId) {
        this.localizedTemplateId = localizedTemplateId;
    }

    @Column(name = "external_id", nullable = true, length = 64)
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Column(name = "owner", nullable = true)
    public Short getOwner() {
        return owner;
    }

    public void setOwner(Short owner) {
        this.owner = owner;
    }

    @Column(name = "user_image_id", nullable = true)
    public Long getUserImageId() {
        return userImageId;
    }

    public void setUserImageId(Long userImageId) {
        this.userImageId = userImageId;
    }

    @Column(name = "first_login", nullable = true)
    public Short getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(Short firstLogin) {
        this.firstLogin = firstLogin;
    }

    @Column(name = "last_login_time", nullable = true)
    public Timestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Timestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    @Column(name = "deactivated", nullable = true)
    public LocalDateTime getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(LocalDateTime deactivated) {
        this.deactivated = deactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Users users = (Users) o;

        if (usersId != users.usersId) {
            return false;
        }
        if (displayUserTime != users.displayUserTime) {
            return false;
        }
        if (username != null ? !username.equals(users.username) : users.username != null) {
            return false;
        }
        if (title != null ? !title.equals(users.title) : users.title != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(users.firstName) : users.firstName != null) {
            return false;
        }
        if (middleName != null ? !middleName.equals(users.middleName) : users.middleName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(users.lastName) : users.lastName != null) {
            return false;
        }
        if (suffix != null ? !suffix.equals(users.suffix) : users.suffix != null) {
            return false;
        }
        if (password != null ? !password.equals(users.password) : users.password != null) {
            return false;
        }
        if (hint != null ? !hint.equals(users.hint) : users.hint != null) {
            return false;
        }
        if (lastFourSsn != null ? !lastFourSsn.equals(users.lastFourSsn) : users.lastFourSsn != null) {
            return false;
        }
        if (mothersMaidenName != null ? !mothersMaidenName.equals(users.mothersMaidenName) : users.mothersMaidenName != null) {
            return false;
        }
        if (timeZone != null ? !timeZone.equals(users.timeZone) : users.timeZone != null) {
            return false;
        }
        if (usersStatus != null ? !usersStatus.equals(users.usersStatus) : users.usersStatus != null) {
            return false;
        }
        if (note != null ? !note.equals(users.note) : users.note != null) {
            return false;
        }
        if (securityQuestion != null ? !securityQuestion.equals(users.securityQuestion) : users.securityQuestion != null) {
            return false;
        }
        if (securityAnswer != null ? !securityAnswer.equals(users.securityAnswer) : users.securityAnswer != null) {
            return false;
        }
        if (disabled != null ? !disabled.equals(users.disabled) : users.disabled != null) {
            return false;
        }
        if (created != null ? !created.equals(users.created) : users.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(users.updated) : users.updated != null) {
            return false;
        }
        if (imageDir != null ? !imageDir.equals(users.imageDir) : users.imageDir != null) {
            return false;
        }
        if (localizedTemplateId != null ? !localizedTemplateId.equals(users.localizedTemplateId) : users.localizedTemplateId != null) {
            return false;
        }
        if (externalId != null ? !externalId.equals(users.externalId) : users.externalId != null) {
            return false;
        }
        if (owner != null ? !owner.equals(users.owner) : users.owner != null) {
            return false;
        }
        if (userImageId != null ? !userImageId.equals(users.userImageId) : users.userImageId != null) {
            return false;
        }
        if (firstLogin != null ? !firstLogin.equals(users.firstLogin) : users.firstLogin != null) {
            return false;
        }
        if (lastLoginTime != null ? !lastLoginTime.equals(users.lastLoginTime) : users.lastLoginTime != null) {
            return false;
        }
        return deactivated != null ? deactivated.equals(users.deactivated) : users.deactivated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (usersId ^ (usersId >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (middleName != null ? middleName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (suffix != null ? suffix.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (hint != null ? hint.hashCode() : 0);
        result = 31 * result + (lastFourSsn != null ? lastFourSsn.hashCode() : 0);
        result = 31 * result + (mothersMaidenName != null ? mothersMaidenName.hashCode() : 0);
        result = 31 * result + (timeZone != null ? timeZone.hashCode() : 0);
        result = 31 * result + (usersStatus != null ? usersStatus.hashCode() : 0);
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + (int) displayUserTime;
        result = 31 * result + (securityQuestion != null ? securityQuestion.hashCode() : 0);
        result = 31 * result + (securityAnswer != null ? securityAnswer.hashCode() : 0);
        result = 31 * result + (disabled != null ? disabled.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (imageDir != null ? imageDir.hashCode() : 0);
        result = 31 * result + (localizedTemplateId != null ? localizedTemplateId.hashCode() : 0);
        result = 31 * result + (externalId != null ? externalId.hashCode() : 0);
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (userImageId != null ? userImageId.hashCode() : 0);
        result = 31 * result + (firstLogin != null ? firstLogin.hashCode() : 0);
        result = 31 * result + (lastLoginTime != null ? lastLoginTime.hashCode() : 0);
        result = 31 * result + (deactivated != null ? deactivated.hashCode() : 0);
        return result;
    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "object_id", referencedColumnName = "owner_object_id", nullable = false, insertable = false, updatable = false)
//    public BadgePrintJob getBadgePrintJobByObjectId() {
//        return badgePrintJobByObjectId;
//    }
//
//    public void setBadgePrintJobByObjectId(BadgePrintJob badgePrintJobByObjectId) {
//        this.badgePrintJobByObjectId = badgePrintJobByObjectId;
//    }

    @OneToMany(mappedBy = "usersByUsersId")
    public Collection<LoginSession> getLoginSessionsByUsersId() {
        return loginSessionsByUsersId;
    }

    public void setLoginSessionsByUsersId(Collection<LoginSession> loginSessionsByUsersId) {
        this.loginSessionsByUsersId = loginSessionsByUsersId;
    }

    @OneToMany(mappedBy = "usersByProxyUsersId")
    public Collection<LoginSession> getLoginSessionsByUsersId_0() {
        return loginSessionsByUsersId_0;
    }

    public void setLoginSessionsByUsersId_0(Collection<LoginSession> loginSessionsByUsersId_0) {
        this.loginSessionsByUsersId_0 = loginSessionsByUsersId_0;
    }
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "object_id", referencedColumnName = "user_object_id", nullable = false, insertable = false, updatable = false)
//    public MobileInvitation getMobileInvitationByObjectId() {
//        return mobileInvitationByObjectId;
//    }
//
//    public void setMobileInvitationByObjectId(MobileInvitation mobileInvitationByObjectId) {
//        this.mobileInvitationByObjectId = mobileInvitationByObjectId;
//    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_id", referencedColumnName = "owner_object_id", nullable = false, insertable = false, updatable = false)
    public OauthServiceToken getOauthServiceTokenByObjectId() {
        return oauthServiceTokenByObjectId;
    }

    public void setOauthServiceTokenByObjectId(OauthServiceToken oauthServiceTokenByObjectId) {
        this.oauthServiceTokenByObjectId = oauthServiceTokenByObjectId;
    }

//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "object_id", referencedColumnName = "user_object_id", nullable = false, insertable = false, updatable = false)
//    public UserPrintStatus getUserPrintStatusByObjectId() {
//        return userPrintStatusByObjectId;
//    }
//
//    public void setUserPrintStatusByObjectId(UserPrintStatus userPrintStatusByObjectId) {
//        this.userPrintStatusByObjectId = userPrintStatusByObjectId;
//    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "user_type_id", referencedColumnName = "user_type_id", nullable = false)
    public UserType getUserTypeByUserTypeId() {
        return userTypeByUserTypeId;
    }

    public void setUserTypeByUserTypeId(UserType userTypeByUserTypeId) {
        this.userTypeByUserTypeId = userTypeByUserTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_image_id", referencedColumnName = "user_image_id", insertable = false, updatable = false)
    public UserImage getUserImageByUserImageId() {
        return userImageByUserImageId;
    }

    public void setUserImageByUserImageId(UserImage userImageByUserImageId) {
        this.userImageByUserImageId = userImageByUserImageId;
    }
}
