package com.brivo.panel.server.reference.service.account;

import com.brivo.panel.server.reference.domain.entities.AccessCredential;
import com.brivo.panel.server.reference.domain.entities.AccessCredentialType;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.AccountType;
import com.brivo.panel.server.reference.domain.entities.Address;
import com.brivo.panel.server.reference.domain.entities.Board;
import com.brivo.panel.server.reference.domain.entities.BoardProperty;
import com.brivo.panel.server.reference.domain.entities.BoardType;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.BrainState;
import com.brivo.panel.server.reference.domain.entities.BrainType;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.CredentialField;
import com.brivo.panel.server.reference.domain.entities.CredentialFieldValue;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.DeviceProperty;
import com.brivo.panel.server.reference.domain.entities.DeviceSchedule;
import com.brivo.panel.server.reference.domain.entities.DeviceType;
import com.brivo.panel.server.reference.domain.entities.DoorData;
import com.brivo.panel.server.reference.domain.entities.DoorDataAntipassback;
import com.brivo.panel.server.reference.domain.entities.ElevatorFloorMap;
import com.brivo.panel.server.reference.domain.entities.Holiday;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import com.brivo.panel.server.reference.domain.entities.ObjectProperty;
import com.brivo.panel.server.reference.domain.entities.ProgDeviceData;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.domain.entities.Rs485Settings;
import com.brivo.panel.server.reference.domain.entities.Schedule;
import com.brivo.panel.server.reference.domain.entities.ScheduleData;
import com.brivo.panel.server.reference.domain.entities.ScheduleExceptionData;
import com.brivo.panel.server.reference.domain.entities.ScheduleHolidayMap;
import com.brivo.panel.server.reference.domain.entities.ScheduleType;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupAntipassback;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupType;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupTypes;
import com.brivo.panel.server.reference.domain.entities.UserType;
import com.brivo.panel.server.reference.domain.entities.Users;
import com.brivo.panel.server.reference.domain.repository.AccessCredentialTypeRepository;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.AddressRepository;
import com.brivo.panel.server.reference.domain.repository.BoardTypeRepository;
import com.brivo.panel.server.reference.domain.repository.BrainTypeRepository;
import com.brivo.panel.server.reference.domain.repository.CredentialFieldRepository;
import com.brivo.panel.server.reference.domain.repository.CredentialFieldValueRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceScheduleRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceTypeRepository;
import com.brivo.panel.server.reference.domain.repository.ElevatorFloorMapRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPropertyRepository;
import com.brivo.panel.server.reference.domain.repository.ScheduleHolidayMapRepository;
import com.brivo.panel.server.reference.domain.repository.ScheduleTypeRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityActionRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupMemberRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupTypeRepository;
import com.brivo.panel.server.reference.domain.repository.UserTypeRepository;
import com.brivo.panel.server.reference.dto.*;
import com.brivo.panel.server.reference.service.UiCredentialService;
import com.brivo.panel.server.reference.service.UiDeviceService;
import com.brivo.panel.server.reference.service.util.IOUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Random;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
class CreateAccount extends AbstractAccountCRUDComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(CreateAccount.class);

    private final AccountRepository accountRepository;
    private final BrainTypeRepository brainTypeRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final SecurityGroupTypeRepository securityGroupTypeRepository;
    private final UserTypeRepository userTypeRepository;
    private final ScheduleTypeRepository scheduleTypeRepository;
    private final SecurityActionRepository securityActionRepository;
    private final AccessCredentialTypeRepository accessCredentialTypeRepository;
    private final SecurityGroupMemberRepository securityGroupMemberRepository;
    private final DeviceScheduleRepository deviceScheduleRepository;
    private final AddressRepository addressRepository;
    private final ScheduleHolidayMapRepository scheduleHolidayMapRepository;
    private final BoardTypeRepository boardTypeRepository;
    private final ElevatorFloorMapRepository elevatorFloorMapRepository;
    private final CredentialFieldRepository credentialFieldRepository;
    private final ObjectPropertyRepository objectPropertyRepository;
    private final CredentialFieldValueRepository credentialFieldValueRepository;
    private final UiCredentialService uiCredentialService;

    private final UiDeviceService deviceService;

    @Autowired
    public CreateAccount(final AccountRepository accountRepository, final BrainTypeRepository brainTypeRepository,
                         final DeviceTypeRepository deviceTypeRepository,
                         final SecurityGroupTypeRepository securityGroupTypeRepository,
                         final UserTypeRepository userTypeRepository, final ScheduleTypeRepository scheduleTypeRepository,
                         final SecurityActionRepository securityActionRepository,
                         final AccessCredentialTypeRepository accessCredentialTypeRepository,
                         final SecurityGroupMemberRepository securityGroupMemberRepository,
                         final DeviceScheduleRepository deviceScheduleRepository,
                         final AddressRepository addressRepository,
                         final ScheduleHolidayMapRepository scheduleHolidayMapRepository,
                         final BoardTypeRepository boardTypeRepository,
                         final ElevatorFloorMapRepository elevatorFloorMapRepository,
                         final CredentialFieldRepository credentialFieldRepository,
                         final ObjectPropertyRepository objectPropertyRepository,
                         final UiDeviceService deviceService,
                         final CredentialFieldValueRepository credentialFieldValueRepository,
                         final UiCredentialService uiCredentialService) {
        this.accountRepository = accountRepository;
        this.brainTypeRepository = brainTypeRepository;
        this.deviceTypeRepository = deviceTypeRepository;
        this.securityGroupTypeRepository = securityGroupTypeRepository;
        this.userTypeRepository = userTypeRepository;
        this.scheduleTypeRepository = scheduleTypeRepository;
        this.securityActionRepository = securityActionRepository;
        this.accessCredentialTypeRepository = accessCredentialTypeRepository;
        this.securityGroupMemberRepository = securityGroupMemberRepository;
        this.deviceScheduleRepository = deviceScheduleRepository;
        this.addressRepository = addressRepository;
        this.scheduleHolidayMapRepository = scheduleHolidayMapRepository;
        this.boardTypeRepository = boardTypeRepository;
        this.elevatorFloorMapRepository = elevatorFloorMapRepository;
        this.credentialFieldRepository = credentialFieldRepository;
        this.objectPropertyRepository = objectPropertyRepository;
        this.deviceService = deviceService;
        this.credentialFieldValueRepository = credentialFieldValueRepository;
        this.uiCredentialService = uiCredentialService;
    }

    public MessageDTO create(final AccountDTO accountDTO) {
        final String accountName = accountDTO.getName();
        LOGGER.debug("Creating the account '{}'...", accountName);

        try {
            validateNewAccount(accountDTO);

            final Account account = createAccount(accountDTO);

            final Collection<SecurityGroup> userGroups = createUserGroups(account, accountDTO.getGroups());
            final Collection<SecurityGroup> sites = createSites(account, accountDTO.getSites());
            userGroups.addAll(sites);
            account.setSecurityGroupsByAccountId(userGroups);

            final Collection<Users> users = createUsers(account, accountDTO.getUsers());
            account.setUsersByAccountId(users);

            final Collection<Brain> panels = createPanels(account, accountDTO.getPanels());
            account.setBrainsByAccountId(panels);

            final Collection<Device> devices = createDevices(account, accountDTO.getDevices());
            final Collection<Device> doors = createDoors(account, accountDTO.getDoors());
            doors.addAll(devices);
            account.setDevicesByAccountId(doors);

            final Collection<Holiday> holidays = createHolidays(account, accountDTO.getHolidays());
            account.setHolidaysByAccountId(holidays);

            final Collection<Schedule> schedules = createSchedules(account, accountDTO.getSchedules());
            account.setSchedulesByAccountId(schedules);

            final Collection<AccessCredential> credentials = createCredentials(account, accountDTO.getCredentials());
            account.setAccessCredentialsByAccountId(credentials);

            final Account savedAccount = accountRepository.save(account);
            LOGGER.debug("Account {} successfully persisted to database...", accountName);

            persistMissingAccessCredentialTypes(accountDTO.getAccessCredentialTypeDTOS(), accountName);
            persistObjectPermissions(accountDTO.getGroups(), savedAccount);
            persistSecurityGroupMembers(accountDTO);

            persistDeviceProperties(getDevicesAndDoorsForAccount(accountDTO), savedAccount);
            persistDeviceSchedules(getDevicesAndDoorsForAccount(accountDTO), accountName);
            persistDoorDataAntipassback(accountDTO.getDoors(), accountName);

            persistSiteAddresses(accountDTO.getSites(), accountName);
            persistScheduleHolidayMaps(accountDTO.getSchedules(), accountName);
            persistElevatorFloorMaps(accountDTO.getPanels(), accountName);
            persistCredentialFields(accountDTO.getCredentialFields(), accountName);
            persistObjectProperties(accountDTO.getObjectProperties(), accountName);
            persistCredentialFieldValues(accountDTO.getCredentials(), accountName);
        } catch (Exception ex) {
            LOGGER.error("An error occured while inserting the account {}: {}", accountName, ex.getMessage());
            return new MessageDTO("The account " + accountName + " does not have all the expected JSON elements ");
//            throw ex;
        }

        return new MessageDTO("The account '" + accountDTO.getName() + "' was successfully created!");
    }

    private Collection<DeviceDTO> getDevicesAndDoorsForAccount(final AccountDTO accountDTO) {
        final Collection<DeviceDTO> deviceDTOS = accountDTO.getDevices();
        deviceDTOS.addAll(accountDTO.getDoors());

        return deviceDTOS;
    }

    public MessageDTO create(final String accountName) {
        try {
            final Optional<String> accountFile = Files.list(Paths.get(accountsUploadFolder))
                    .map(file -> file.getFileName().toFile().getName())
                    .filter(file -> file.equals(accountName + ".json"))
                    .findFirst();
            if (!accountFile.isPresent()) {
                LOGGER.error("There is no account file for the account '" + accountName + "'");
                throw new AssertionError("There is no account file for the account '" + accountName + "'");
            }

            final String accountData = getFileContent(accountFile.get());
            return create(objectMapper.readValue(accountData, AccountDTO.class));
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalArgumentException("An I/O exception occurred while reading the account '" + accountName + "'");
        }
    }

    private String getFileContent(final String accountFile) {
        final File accountJsonFile = new File(accountsUploadFolder + PATH_SEPARATOR + accountFile);

        try (final InputStreamReader fileReader = new InputStreamReader(new FileInputStream(accountJsonFile));
             final BufferedReader bufferedReader = new BufferedReader(fileReader)) {

            return IOUtils.getBufferContent(bufferedReader);
        } catch (Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalArgumentException(e.getMessage());
        }
    }

    private void persistCredentialFieldValues(final Collection<CredentialDTO> credentialDTOS, final String accountName) {
        LOGGER.debug("Adding credential field values for account {} ", accountName);

        if (credentialDTOS.size() == 0) {
            LOGGER.debug("There are no credentials in account {} to persist credential field values for...", accountName);
            return;
        }

        Collection<CredentialFieldValue> credentialFieldValues = buildCredentialFieldValues(credentialDTOS);
        credentialFieldValueRepository.saveAll(credentialFieldValues);

        LOGGER.debug("Successfully added credential field values for account {}", accountName);
    }

    private void persistMissingAccessCredentialTypes(final Collection<AccessCredentialTypeDTO> accessCredentialTypeDTOS, final String accountName) {
        LOGGER.debug("Adding missing access credential types for account {}", accountName);

        if (accessCredentialTypeDTOS == null || accessCredentialTypeDTOS.size() == 0) {
            LOGGER.error("There are no access credential types to persist for account {}", accountName);
            return;
        }

        Collection<AccessCredentialType> accessCredentialTypes = accessCredentialTypeDTOS.stream()
                                                                                         .map(uiCredentialService.convertAccessCredentialTypeDTO())
                                                                                         .collect(Collectors.toList());

        final Collection<AccessCredentialType> currentAccessCredentialTypes = StreamSupport.stream(accessCredentialTypeRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        final Collection<AccessCredentialType> missingCredentialTypes = findMissingAccessCredentialTypes(currentAccessCredentialTypes, accessCredentialTypes);

        if (missingCredentialTypes.size() == 0) {
            LOGGER.warn("All the access credential types fof the account {}(from the JSON file) already exist in the database");
            return;
        }

        LOGGER.debug("Missing access credential types, that will be persisted to the database: {}", ArrayUtils.toString(missingCredentialTypes.toArray()));

        accessCredentialTypeRepository.saveAll(missingCredentialTypes);
        LOGGER.debug("Successfully saved missing access credential types");
    }

    private Collection<AccessCredentialType> findMissingAccessCredentialTypes(Collection<AccessCredentialType> currentAccessCredentialTypes,
                                                                              Collection<AccessCredentialType> jsonAccessCredentialTypes) {
        return jsonAccessCredentialTypes.stream()
                                        .filter(isAccessCredentialTypeMissing(currentAccessCredentialTypes))
                                        .collect(Collectors.toList());
    }

    private Predicate<AccessCredentialType> isAccessCredentialTypeMissing(Collection<AccessCredentialType> currentAccessCredentialTypes) {
        return accessCredentialType -> !currentAccessCredentialTypes.contains(accessCredentialType);
    }

    private Collection<CredentialFieldValue> buildCredentialFieldValues(final Collection<CredentialDTO> credentialDTOS) {
        Collection<CredentialFieldValue> credentialFieldValuesCollection = new ArrayList<>();

        credentialDTOS.forEach(credentialDTO -> {
            Collection<CredentialFieldValue> credentialFieldValues = getCredentialFieldValueForCredentialDTO(credentialDTO);
            credentialFieldValuesCollection.addAll(credentialFieldValues);
        });

        return credentialFieldValuesCollection;
    }

    private void persistDoorDataAntipassback(final Collection<DoorDTO> doorDTOS, final String accountName) {
        LOGGER.debug("Adding door data antipassback for account {} ", accountName);

        if (doorDTOS.size() == 0) {
            LOGGER.debug("There are no doors in account {} to persist door data antipassback for...", accountName);
            return;
        }

        final Collection<DoorDataAntipassback> doorDataAntipassbacks = buildDoorDataAntipassbacks(doorDTOS);
        doorDataAntipassbackRepository.saveAll(doorDataAntipassbacks);

        LOGGER.debug("Successfully added door data antipassback for account {}", accountName);
    }


    private void persistCredentialFields(final Collection<CredentialFieldDTO> credentialFieldDTOS, final String accountName) {
        LOGGER.debug("Adding credential fields for account {} ", accountName);

        if (credentialFieldDTOS.size() == 0) {
            LOGGER.debug("There are no credential fields in account {} to persist ...", accountName);
            return;
        }

        final Collection<CredentialField> credentialFields = buildCredentialFields(credentialFieldDTOS);
        credentialFieldRepository.saveAll(credentialFields);

        LOGGER.debug("Successfully added credential fields for account {}", accountName);
    }

    private void persistObjectProperties(final Collection<ObjectPropertyDTO> objectPropertyDTOS, final String accountName) {
        LOGGER.debug("Adding object properties for account {} ", accountName);

        if (objectPropertyDTOS.size() == 0) {
            LOGGER.debug("There are no object properties in account {} to persist ...", accountName);
            return;
        }

        final Collection<ObjectProperty> objectProperties = buildObjectProperties(objectPropertyDTOS);
        objectPropertyRepository.saveAll(objectProperties);

        LOGGER.debug("Successfully added object properties for account {}", accountName);
    }

    private void persistElevatorFloorMaps(final Collection<PanelDTO> panels, final String accountName) {
        LOGGER.debug("Adding elevator floor maps for account {} ", accountName);

        if (panels.size() == 0) {
            LOGGER.debug("There are no panels in account {} to build elevator floor maps for ...", accountName);
            return;
        }

        final Collection<ElevatorFloorMap> elevatorFloorMaps = buildElevatorFloorMaps(panels);

        if (elevatorFloorMaps.size() == 0) {
            LOGGER.debug("There were no elevator floor maps to insert for account {}", accountName);
            return;
        }

        elevatorFloorMapRepository.saveAll(elevatorFloorMaps);
        LOGGER.debug("Successfully added elevator floor maps for account {}", accountName);
    }

    private Collection<DoorDataAntipassback> buildDoorDataAntipassbacks(final Collection<DoorDTO> doorDTOS) {
        return doorDTOS.stream()
                .filter(doorHasDoorDataAntipassback())
                .map(buildDoorDataAntipassback())
                .collect(Collectors.toSet());
    }

    private Predicate<DoorDTO> doorHasDoorDataAntipassback() {
        return doorDTO -> doorDTO.getDoorDataAntipassback().getDoorDataAntipassbackId() != NOT_AVAILABLE_LONG_VALUE;
    }

    private Function<DoorDTO, DoorDataAntipassback> buildDoorDataAntipassback() {
        return doorDTO -> {
            final DoorDataAntipassbackDTO doorDataAntipassbackDTO = doorDTO.getDoorDataAntipassback();
            final DoorDataAntipassback doorDataAntipassback = new DoorDataAntipassback();

            doorDataAntipassback.setDoorDataAntipassbackId(doorDataAntipassbackDTO.getDoorDataAntipassbackId());
            doorDataAntipassback.setAltReaderBoardNum(doorDataAntipassbackDTO.getAltReaderBoardNum());
            doorDataAntipassback.setAltReaderPanelOid(doorDataAntipassbackDTO.getAltReaderPanelOid());
            doorDataAntipassback.setAltReaderPointAddress(doorDataAntipassbackDTO.getAltReaderPointAddress());
            doorDataAntipassback.setZone(doorDataAntipassbackDTO.getZone());
            doorDataAntipassback.setAltZone(doorDataAntipassbackDTO.getAltZone());
            doorDataAntipassback.setDeviceOid(doorDTO.getObjectId());

            return doorDataAntipassback;
        };
    }

    private Collection<CredentialField> buildCredentialFields(final Collection<CredentialFieldDTO> credentialFieldDTOS) {
        return credentialFieldDTOS.stream()
                .map(buildCredentialField())
                .collect(Collectors.toSet());
    }

    private Function<CredentialFieldDTO, CredentialField> buildCredentialField() {
        return credentialFieldDTO -> {
            CredentialField credentialField = new CredentialField();

            credentialField.setCredentialFieldId(credentialFieldDTO.getCredentialFieldId());
            credentialField.setAccessCredentialTypeId(credentialFieldDTO.getAccessCredentialTypeId());
            credentialField.setFormat(credentialFieldDTO.getFormat());
            credentialField.setCredentialFieldTypeId(credentialFieldDTO.getCredentialFieldTypeId());
            credentialField.setDisplay(credentialFieldDTO.getDisplay());
            credentialField.setForReference(credentialFieldDTO.getForReference());
            credentialField.setName(credentialFieldDTO.getName());
            credentialField.setOrdering(credentialFieldDTO.getOrdering());

            return credentialField;
        };
    }

    private Collection<ObjectProperty> buildObjectProperties(final Collection<ObjectPropertyDTO> objectPropertyDTOS) {
        return objectPropertyDTOS.stream()
                .map(buildObjectProperty())
                .collect(Collectors.toSet());
    }

    private Function<ObjectPropertyDTO, ObjectProperty> buildObjectProperty() {
        return objectPropertyDTO -> {
            final LocalDateTime now = LocalDateTime.now();
            ObjectProperty objectProperty = new ObjectProperty();

            objectProperty.setId(objectPropertyDTO.getId());
            objectProperty.setObjectId(objectPropertyDTO.getObjectId());
            objectProperty.setValue(objectPropertyDTO.getValue());
            objectProperty.setCreated(now);
            objectProperty.setUpdated(now);

            return objectProperty;
        };
    }

    private Collection<ElevatorFloorMap> buildElevatorFloorMaps(final Collection<PanelDTO> panelDTOS) {
        Collection<ElevatorFloorMap> elevatorFloorMaps = new HashSet<>();

        panelDTOS.forEach(panelDTO -> {
            final Collection<ElevatorFloorMap> elevatorFloorMapsForPanel = buildElevatorFloorMapsForPanel(panelDTO);

            elevatorFloorMaps.addAll(elevatorFloorMapsForPanel);
        });

        return elevatorFloorMaps;
    }

    private Collection<ElevatorFloorMap> buildElevatorFloorMapsForPanel(final PanelDTO panelDTO) {
        return panelDTO.getElevatorFloorMaps()
                .stream()
                .map(buildElevatorFloorMapForPanel(panelDTO.getObjectId()))
                .collect(Collectors.toSet());
    }

    private Function<ElevatorFloorMapDTO, ElevatorFloorMap> buildElevatorFloorMapForPanel(final Long brainObjectId) {
        return elevatorFloorMapDTO -> {
            final ElevatorFloorMap elevatorFloorMap = new ElevatorFloorMap();

            elevatorFloorMap.setBoardNumber(elevatorFloorMapDTO.getBoardNumber());
            elevatorFloorMap.setFloorOid(elevatorFloorMapDTO.getFloorOid());
            elevatorFloorMap.setElevatorOid(elevatorFloorMapDTO.getElevatorOid());
            elevatorFloorMap.setPointAddress(elevatorFloorMapDTO.getPointAddress());
            elevatorFloorMap.setPanelOid(brainObjectId);

            return elevatorFloorMap;
        };
    }

    private void persistScheduleHolidayMaps(final Collection<ScheduleDTO> scheduleDTOS, final String accountName) {
        LOGGER.debug("Adding schedule holiday mappings for account {}", accountName);

        if (scheduleDTOS.size() == 0) {
            LOGGER.debug("There are no schedules in account {} to build schedule holiday mappings for...", accountName);
            return;
        }

        final Collection<ScheduleHolidayMap> scheduleHolidayMaps = buildScheduleHolidayMapsForAccount(scheduleDTOS);

        if (scheduleHolidayMaps.size() == 0) {
            LOGGER.debug("There were no schedule holiday mappings to insert for account {}", accountName);
            return;
        }

        scheduleHolidayMapRepository.saveAll(scheduleHolidayMaps);
        LOGGER.debug("Successfully added schedule holiday mappings for account {}", accountName);
    }

    private Collection<ScheduleHolidayMap> buildScheduleHolidayMapsForAccount(final Collection<ScheduleDTO> scheduleDTOS) {
        final Collection<ScheduleHolidayMap> scheduleHolidayMaps = new HashSet<>();

        scheduleDTOS.forEach(scheduleDTO -> createAndSetScheduleHolidayMaps(scheduleHolidayMaps, scheduleDTO));

        return scheduleHolidayMaps;
    }

    private void createAndSetScheduleHolidayMaps(final Collection<ScheduleHolidayMap> scheduleHolidayMaps,
                                                 final ScheduleDTO scheduleDTO) {
        final Collection<Long> holidayIds = scheduleDTO.getHolidayIds();

        if (holidayIds.size() > 0) {
            holidayIds.forEach(holidayId -> {
                ScheduleHolidayMap scheduleHolidayMap = new ScheduleHolidayMap();
                scheduleHolidayMap.setHolidayOid(holidayId);
                scheduleHolidayMap.setScheduleOid(scheduleDTO.getId());

                scheduleHolidayMaps.add(scheduleHolidayMap);
            });
        }
    }

    private void persistSiteAddresses(final Collection<SiteDTO> siteDTOS, final String accountName) {
        LOGGER.debug("Adding site addresses for account {}", accountName);

        if (siteDTOS.size() == 0) {
            LOGGER.debug("There are no sites in account {} to build addresses for...", accountName);
            return;
        }

        final Collection<Address> addresses = buildAddressesForAccount(siteDTOS);

        if (addresses.size() == 0) {
            LOGGER.debug("There were no addresses to insert for account " + accountName);
            return;
        }

        addressRepository.saveAll(addresses);
        LOGGER.debug("Successfully added addresses for account {}", accountName);

    }

    private Collection<Address> buildAddressesForAccount(final Collection<SiteDTO> siteDTOS) {
        final Collection<Address> addresses = new HashSet<>();

        siteDTOS.forEach(siteDTO -> createAndSetAddresses(addresses, siteDTO));

        return addresses;
    }

    private void createAndSetAddresses(final Collection<Address> addresses, final SiteDTO siteDTO) {
        final Collection<AddressDTO> addressDTOS = siteDTO.getAddresses();

        if (addressDTOS.size() > 0) {
            addressDTOS.forEach(addressDTO -> {
                Address address = new Address();

                address.setAddressId(addressDTO.getId());
                address.setState(addressDTO.getState());
                address.setCity(addressDTO.getCity());
                address.setAddress1(addressDTO.getAddress1());
                address.setAddress2(addressDTO.getAddress2());
                address.setTimeZone(addressDTO.getTimeZone());
                address.setZip(addressDTO.getZipCode());
                address.setPoBox(addressDTO.getPostalCode());
                address.setOwnerObjectId(siteDTO.getObjectId());

                addresses.add(address);
            });
        }
    }

    private void persistDeviceSchedules(final Collection<DeviceDTO> devices, final String accountName) {
        LOGGER.debug("Adding device schedules for account {}", accountName);

        if (devices.size() == 0) {
            LOGGER.debug("There are no devices in account {} to build device schedules for...", accountName);
            return;
        }

        final Collection<DeviceSchedule> deviceSchedules = buildDeviceSchedulesForAccount(devices);

        if (deviceSchedules.size() == 0) {
            LOGGER.debug("There were no device schedules to insert for account " + accountName);
            return;
        }

        deviceScheduleRepository.saveAll(deviceSchedules);
        LOGGER.debug("Successfully added device schedules for account {}", accountName);
    }

    private Collection<DeviceSchedule> buildDeviceSchedulesForAccount(final Collection<DeviceDTO> devices) {
        return devices.stream()
                .map(buildDeviceSchedulesForDevice())
                .flatMap(devSchedules -> devSchedules.stream())
                .collect(Collectors.toSet());
    }

    private Function<DeviceDTO, Collection<DeviceSchedule>> buildDeviceSchedulesForDevice() {
        return deviceDTO -> {
            return deviceDTO.getDeviceUnlockScheduleIds()
                    .stream()
                    .map(buildDeviceSchedule(deviceDTO.getId()))
                    .collect(Collectors.toSet());
        };
    }

    private Function<Long, DeviceSchedule> buildDeviceSchedule(final Long deviceId) {
        return scheduleId -> {
            DeviceSchedule deviceSchedule = new DeviceSchedule();

            deviceSchedule.setDeviceId(deviceId);
            deviceSchedule.setScheduleId(scheduleId);

            return deviceSchedule;
        };
    }

    private void persistSecurityGroupMembers(final AccountDTO accountDTO) {
        LOGGER.debug("Adding security group members for account {}", accountDTO.getName());

        final Collection<SecurityGroupMember> userGroups = buildUserGroupMembersForAccount(accountDTO);
        LOGGER.debug("Successfully built {} user groups for account {}", userGroups.size(), accountDTO.getName());

        final Collection<SecurityGroupMember> deviceGroups = buildDeviceGroupMembersForAccount(accountDTO);
        LOGGER.debug("Successfully built {} device groups for account {}", deviceGroups.size(), accountDTO.getName());

        userGroups.addAll(deviceGroups);

        if (userGroups.size() == 0) {
            LOGGER.debug("There were no security group members to insert for account {}", accountDTO.getName());
            return;
        }

        securityGroupMemberRepository.saveAll(userGroups);
        LOGGER.debug("Successfully added security group members...");
    }

    private Collection<SecurityGroupMember> buildUserGroupMembersForAccount(final AccountDTO accountDTO) {
        Collection<SecurityGroupMember> securityGroupMembers = new HashSet<>();

        accountDTO.getUsers()
                .forEach(userDTO -> createAndSetSecurityGroupMembersFromUserGroups(securityGroupMembers, userDTO));

        return securityGroupMembers;
    }

    private void createAndSetSecurityGroupMembersFromUserGroups(final Collection<SecurityGroupMember> securityGroupMembers,
                                                                final UserDTO userDTO) {
        Collection<BigInteger> groupIds = userDTO.getGroupIds();

        // if no groupIds are provided for the user, the root group will be automatically assigned via trigger
        if (groupIds.size() > 0) {
            groupIds.forEach(groupId -> {
                // Check if group is not root account group(security_group_type_id = 0) --> connection
                // between user and root group is already inserted via trigger
                SecurityGroup group = securityGroupRepository.findById(groupId.longValue()).get();

                if (group.getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId()
                        != SecurityGroupTypes.ACCOUNT_ROOT_GROUP.getSecurityGroupTypeID()) {
                    SecurityGroupMember securityGroupMember = new SecurityGroupMember();

                    securityGroupMember.setObjectId(userDTO.getObjectId());
                    securityGroupMember.setSecurityGroupId(groupId.longValue());

                    securityGroupMembers.add(securityGroupMember);
                }
            });
        }
    }

    private Collection<SecurityGroupMember> buildDeviceGroupMembersForAccount(final AccountDTO accountDTO) {
        Collection<SecurityGroupMember> securityGroupMembers = new HashSet<>();

        final Collection<DeviceDTO> allDevices = getDevicesAndDoorsForAccount(accountDTO);
        if (allDevices.size() > 0) {
            allDevices.forEach(deviceDTO ->
                    createAndSetSecurityGroupMembersFromDeviceGroups(securityGroupMembers, deviceDTO));
        }

        return securityGroupMembers;
    }

    private void createAndSetSecurityGroupMembersFromDeviceGroups(final Collection<SecurityGroupMember> securityGroupMembers,
                                                                  final DeviceDTO deviceDTO) {
        Collection<BigInteger> siteIds = deviceDTO.getSiteIds();

        if (siteIds.size() > 0) {
            siteIds.forEach(siteId -> {
                SecurityGroupMember securityGroupMember = new SecurityGroupMember();

                securityGroupMember.setObjectId(deviceDTO.getObjectId());
                securityGroupMember.setSecurityGroupId(siteId.longValue());

                securityGroupMembers.add(securityGroupMember);
            });
        }
    }

    private void persistDeviceProperties(final Collection<DeviceDTO> deviceDTOS, final Account savedAccount) {
        LOGGER.debug("Adding device properties for account {}", savedAccount.getName());

        final Collection<DeviceProperty> deviceProperties = buildDevicePropertiesForAccount(deviceDTOS, savedAccount);

        if (deviceProperties.size() == 0) {
            LOGGER.debug("There were no device properties to insert for account {}", savedAccount.getName());
            return;
        }

        devicePropertyRepository.saveAll(deviceProperties);
        LOGGER.debug("Successfully added device properties for account {}", savedAccount.getName());
    }

    private Collection<DeviceProperty> buildDevicePropertiesForAccount(final Collection<DeviceDTO> deviceDTOS,
                                                                       final Account savedAccount) {
        Collection<DeviceProperty> deviceProperties = new HashSet<>();

        deviceDTOS.forEach(deviceDTO -> {
            final Device currentDevice = findDeviceByDeviceIdInAccount(savedAccount, deviceDTO.getId());
            Collection<DeviceProperty> currentDeviceProperties = buildDevicePropertiesForDevice(currentDevice, deviceDTO);

            deviceProperties.addAll(currentDeviceProperties);
        });

        return deviceProperties;
    }

    private void persistObjectPermissions(final Collection<GroupDTO> groupDTOS, final Account savedAccount) {
        final Collection<ObjectPermission> objectPermissionsForGroups =
                groupDTOS.stream()
                        .flatMap(groupDTO -> buildObjectPermissionsForGroup(groupDTO, savedAccount).stream())
                        .collect(Collectors.toSet());

        if (objectPermissionsForGroups.size() == 0) {
            LOGGER.debug("There are no object permissions to insert for the account {}", savedAccount.getName());
            return;
        }

        objectPermissionRepository.saveAll(objectPermissionsForGroups);
        LOGGER.debug("Successfully added object permissions");
    }

    private Collection<ObjectPermission> buildObjectPermissionsForGroup(final GroupDTO groupDTO, final Account savedAccount) {
        Collection<ObjectPermission> objectPermissionsForGroup = new HashSet<>();
        final LocalDateTime now = LocalDateTime.now();

        Collection<ObjectPermissionForGroupDTO> objectPermissionForGroup = groupDTO.getObjectPermissions();
        if (!objectPermissionForGroup.isEmpty()) {

            objectPermissionForGroup.forEach(scheduleDevicePair ->
                    createAndSetObjectPermissionsForGroup(savedAccount, objectPermissionsForGroup, now, scheduleDevicePair));
        }

        return objectPermissionsForGroup;
    }

    private void createAndSetObjectPermissionsForGroup(final Account savedAccount,
                                                       final Collection<ObjectPermission> objectPermissionsForGroup,
                                                       final LocalDateTime now,
                                                       final ObjectPermissionForGroupDTO scheduleDevicePair) {
        final Long deviceObjectId = scheduleDevicePair.getDeviceObjectId();
        final Long scheduleId = scheduleDevicePair.getScheduleId();

        if (!deviceObjectId.equals(NOT_AVAILABLE_LONG_VALUE) && !scheduleId.equals(NOT_AVAILABLE_LONG_VALUE)) {
            ObjectPermission objectPermission = new ObjectPermission();

            objectPermission.setCreated(now);
            objectPermission.setUpdated(now);
            objectPermission.setIgnoreLockdown(scheduleDevicePair.getIgnoreLockDown());

            objectPermission.setActorObjectId(scheduleDevicePair.getActorObjectId());
            objectPermission.setObjectId(deviceObjectId);
            objectPermission.setScheduleId(scheduleId);

            objectPermission.setSecurityActionId(scheduleDevicePair.getSecurityActionId());

            objectPermissionsForGroup.add(objectPermission);
        }
    }

    private Account createAccount(final AccountDTO accountDTO) {
        final Account account = new Account();

        account.setAccountId(accountDTO.getId());
        final AccountType accountType = accountTypeRepository.findById(accountDTO.getAccountTypeId())
                .get(); // we know it exists, as it passed the validation
        account.setAccountTypeByAccountTypeId(accountType);
        account.setName(accountDTO.getName());
        account.setAccountNumber(generateAccountNumber());
        account.setAccountStatus("new"); //TODO do we need it?

        final LocalDateTime downloadTime = accountDTO.getDownloadTime();
        account.setCreated(downloadTime);
        account.setActivated(downloadTime);
        account.setRegistered(downloadTime);
        account.setUpdated(downloadTime);
        account.setDescription("Created via RPS, on " + downloadTime.toString());

        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.ACCOUNT);
        brivoObject.setObjectId(accountDTO.getObjectId());
        account.setObjectByObjectId(brivoObject);

        final Collection<Account> accounts = new HashSet<>();
        accounts.add(account);
        brivoObject.setAccountsByObjectId(accounts);

        Long brandId = accountRepository.getMinBrandId();
        LOGGER.debug("Braind id for account {} is {}", accountDTO.getName(), brandId);
        if (brandId == null) {
            brandId = 1l;
        }
        account.setBrandId(brandId);

        return account;
    }

    private Collection<SecurityGroup> createUserGroups(final Account account, final Collection<GroupDTO> groups) {
        return groups.stream()
                .map(group -> createUserGroup(group, account))
                .collect(Collectors.toSet());
    }

    private Collection<Device> createDevices(final Account account, final Collection<DeviceDTO> devices) {
        return devices.stream()
                .map(deviceDTO -> createDevice(deviceDTO, account))
                .collect(Collectors.toSet());
    }

    private Collection<Device> createDoors(final Account account, final Collection<DoorDTO> doors) {
        return doors.stream()
                .map(doorDTO -> createDoor(doorDTO, account))
                .collect(Collectors.toSet());
    }

    private Collection<Users> createUsers(final Account account, final Collection<UserDTO> users) {
        return users.stream()
                .map(user -> createUser(user, account))
                .collect(Collectors.toSet());
    }

    private Collection<AccessCredential> createCredentials(final Account account, final Collection<CredentialDTO> credentials) {
        return credentials.stream()
                .map(credential -> createCredential(credential, account))
                .collect(Collectors.toSet());
    }

    private Collection<Schedule> createSchedules(final Account account, final Collection<ScheduleDTO> scheduleDTOS) {
        return scheduleDTOS.stream()
                .map(scheduleDTO -> createSchedule(scheduleDTO, account))
                .collect(Collectors.toSet());
    }

    private Collection<Holiday> createHolidays(final Account account, final Collection<HolidayDTO> holidayDTOS) {
        return holidayDTOS.stream()
                .map(holidayDTO -> createHoliday(holidayDTO, account))
                .collect(Collectors.toSet());
    }

    private Collection<Brain> createPanels(final Account account, final Collection<PanelDTO> panels) {
        return panels.stream()
                .map(panel -> createPanel(panel, account))
                .collect(Collectors.toSet());
    }

    private Collection<SecurityGroup> createSites(final Account account, final Collection<SiteDTO> sites) {
        return sites.stream()
                .map(site -> createSite(site, account))
                .collect(Collectors.toSet());
    }

    private Holiday createHoliday(final HolidayDTO holidayDTO, final Account account) {
        final LocalDateTime now = LocalDateTime.now();
        final Holiday holiday = new Holiday();

        holiday.setHolidayId(holidayDTO.getId());
        holiday.setAccountByAccountId(account);
        holiday.setCadmHolidayId(holidayDTO.getCadmHolidayId());
        holiday.setName(holidayDTO.getName());
        holiday.setSiteId(holidayDTO.getSiteId());
        holiday.setStartDate(holidayDTO.getFrom());
        holiday.setStopDate(holidayDTO.getTo());
        holiday.setCreated(now);
        holiday.setUpdated(now);

        return holiday;
    }

    private SecurityGroup createSite(final SiteDTO site, final Account account) {
        final LocalDateTime now = LocalDateTime.now();
        final SecurityGroup securityGroup = new SecurityGroup();

        securityGroup.setSecurityGroupId(site.getId());
        securityGroup.setAccountByAccountId(account);
        securityGroup.setName(site.getSiteName());
        securityGroup.setCreated(now);
        securityGroup.setUpdated(now);
        securityGroup.setLockdown(site.getLockdown());
        securityGroup.setDeleted(site.getDeleted());
        securityGroup.setDisabled(site.getDisabled());

        SecurityGroupType securityGroupType = securityGroupTypeRepository.findById(SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID()).get();
        securityGroup.setSecurityGroupTypeBySecurityGroupTypeId(securityGroupType);

        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.SECURITY_GROUP);
        brivoObject.setObjectId(site.getObjectId());
        securityGroup.setObjectByObjectId(brivoObject);

        return securityGroup;
    }


    private SecurityGroup createUserGroup(final GroupDTO group, final Account account) {
        final LocalDateTime now = LocalDateTime.now();
        final SecurityGroup securityGroup = new SecurityGroup();

        securityGroup.setSecurityGroupId(group.getId());
        securityGroup.setName(group.getGroupName());
        securityGroup.setParentId(group.getParentId());
        securityGroup.setCreated(now);
        securityGroup.setUpdated(now);
        securityGroup.setAccountByAccountId(account);
        securityGroup.setLockdown(group.getLockdown());
        securityGroup.setDeleted(group.getDeleted());
        securityGroup.setDescription("Created via RPS, on " + now.toString());
        securityGroup.setDisabled(group.getDisabled());

        //security group type id already validated --> validateNewAccountGroups
        SecurityGroupType securityGroupType = securityGroupTypeRepository.findById(group.getSecurityGroupTypeId()).get();
        securityGroup.setSecurityGroupTypeBySecurityGroupTypeId(securityGroupType);

        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.SECURITY_GROUP);
        brivoObject.setObjectId(group.getObjectId());
        securityGroup.setObjectByObjectId(brivoObject);

        if (shouldSecurityGroupAntipassbackBeInsertedForGroup(group)) {
            final SecurityGroupAntipassback securityGroupAntipassback = createSecurityGroupAntipassbackForGroup(group);
            securityGroupAntipassback.setSecurityGroupBySecurityGroupId(securityGroup);
            securityGroup.setSecurityGroupAntipassbackBySecurityGroupId(securityGroupAntipassback);
        }

        Collection<SecurityGroup> groups = new HashSet<>();
        groups.add(securityGroup);
        brivoObject.setSecurityGroupsByObjectId(groups);

        return securityGroup;
    }

    private boolean shouldSecurityGroupAntipassbackBeInsertedForGroup(final GroupDTO groupDTO) {
        return groupDTO.getAntipassbackImunity() != NOT_AVAILABLE_SHORT_VALUE &&
                groupDTO.getAntipassbackResetTime() != NOT_AVAILABLE_LONG_VALUE;
    }

    private SecurityGroupAntipassback createSecurityGroupAntipassbackForGroup(final GroupDTO group) {
        SecurityGroupAntipassback securityGroupAntipassback = new SecurityGroupAntipassback();

        securityGroupAntipassback.setResetTime(group.getAntipassbackResetTime());
        securityGroupAntipassback.setImmunity(group.getAntipassbackImunity());
        securityGroupAntipassback.setSecurityGroupId(group.getId());

        return securityGroupAntipassback;

    }

    private Device createDevice(final DeviceDTO deviceDTO, final Account account) {
        return createBasicDevice(deviceDTO, account);
    }

    private Device createBasicDevice(final DeviceDTO deviceDTO, final Account account) {
        final Device device = new Device();

        device.setDeviceId(deviceDTO.getId());
        device.setAccountId(account.getAccountId());
        device.setName(deviceDTO.getDeviceName());
        device.setPanelId(deviceDTO.getPanelId());
        device.setTwoFactorInterval(deviceDTO.getTwoFactorInterval());
        device.setTwoFactorScheduleId(deviceDTO.getTwoFactorSchedule());
        device.setCardRequiredScheduleId(deviceDTO.getCardRequiredSchedule());
        device.setEnableLiveControl(deviceDTO.getControlFromBrowser());
        device.setIsIngress(deviceDTO.getIsIngress());
        device.setIsEgress(deviceDTO.getIsEgress());
        device.setDeleted(deviceDTO.getDeleted());

        final LocalDateTime now = LocalDateTime.now();
        device.setCreated(now);
        device.setUpdated(now);

        //already validated
        DeviceType deviceType = deviceTypeRepository.findById(deviceDTO.getDeviceTypeId()).get();
        device.setDeviceTypeByDeviceTypeId(deviceType);
        device.setBrainId(deviceDTO.getBrainId());

        BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.DEVICE);
        brivoObject.setObjectId(deviceDTO.getObjectId());
        device.setObjectByBrivoObjectId(brivoObject);

        Collection<ProgDevicePuts> progDevicePuts = buildProgDevicePutsForDevice(deviceDTO.getProgDevicePuts(), device);
        device.setProgDevicePutsByObjectId(progDevicePuts);

        Collection<ProgDeviceData> progDeviceData = buildProgDeviceDataForDevice(deviceDTO.getProgDeviceData(), device);
        device.setProgDeviceDataByObjectId(progDeviceData);

        return device;
    }

    private Collection<DeviceProperty> buildDevicePropertiesForDevice(final Device device, final DeviceDTO deviceDTO) {
        Collection<DeviceProperty> deviceProperties = new HashSet<>();

        final Short maximumREXExtensionValue = deviceDTO.getMaximumREXExtension();
        if (maximumREXExtensionValue != NOT_AVAILABLE_SHORT_VALUE) {
            DeviceProperty maxRexExtension = deviceService.buildDeviceProperty(device,
                    MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY, maximumREXExtensionValue + "");

            deviceProperties.add(maxRexExtension);
        }

        final String bluetoothReaderIdValue = deviceDTO.getBluetoothReaderId();
        if (!bluetoothReaderIdValue.equals(NOT_AVAILABLE)) {
            DeviceProperty bluetoothReaderId = deviceService.buildDeviceProperty(device,
                    BLUETOOTH_READER_ID_DEVICE_TYPE_PROPERTY, bluetoothReaderIdValue);

            deviceProperties.add(bluetoothReaderId);
        }

        return deviceProperties;
    }

    private Device createDoor(final DoorDTO doorDTO, final Account account) {
        final Device device = createBasicDevice(doorDTO, account);

        if (deviceHasDoorData().test(doorDTO)) {
            final DoorData doorData = buildDoorDataForDevice(doorDTO, device);
            device.setDoorData(doorData);
        }

        return device;
    }

    private Predicate<DoorDTO> deviceHasDoorData() {
        return doorDTO -> {
            final DoorDataDTO doorDataDTO = doorDTO.getDoorData();
            return doorDataDTO.getBoard() != NOT_AVAILABLE_LONG_VALUE && doorDataDTO.getNode() != NOT_AVAILABLE_LONG_VALUE;
        };
    }

    private DoorData buildDoorDataForDevice(final DoorDTO doorDTO, final Device device) {
        final DoorDataDTO doorDataDTO = doorDTO.getDoorData();
        final DoorData doorData = new DoorData();

        doorData.setDeviceOid(doorDTO.getObjectId());
        doorData.setBoardNum(doorDataDTO.getBoard());
        doorData.setNodeNum(doorDataDTO.getNode());
        doorData.setNotifyOnAjar(doorDataDTO.getDoorAjarEnabled());
        doorData.setDoorAjar(doorDataDTO.getDoorAjarThreshold());
        doorData.setMaxInvalid(doorDataDTO.getInvalidPINsThreshold());
        doorData.setPassthrough(doorDataDTO.getPassThroughPeriod());
        doorData.setDebouncePeriod(doorDataDTO.getDebouncePeriod());
        doorData.setUseLockOnOpen(doorDataDTO.getUseLockOnOpen());
        doorData.setLockOnOpenDelay(doorDataDTO.getLockOnOpenDelay());
        doorData.setNotifyOnForced(doorDataDTO.getUseRequestToExit());
        doorData.setRexFiresDoor(doorDataDTO.getRexUnlock());
        doorData.setScheduleEnabled(doorDataDTO.getScheduleEnabled());
        doorData.setEnablePrivacyMode(doorDataDTO.getEnablePrivacyMode());
        doorData.setInvalidCredWin(doorDataDTO.getInvalidCredWin());
        doorData.setCacheCredTimeLimit(doorDataDTO.getCacheCredLimitTime());
        doorData.setOsdpPdAddress(doorDataDTO.getOsdpPdAddress());
        doorData.setUseAlarmShunt(doorDataDTO.getUseAlarmShunt());
        doorData.setAlarmDelay(doorDataDTO.getAlarmDelay());

        return doorData;
    }

    private Schedule createSchedule(final ScheduleDTO scheduleDTO, final Account account) {
        Schedule schedule = new Schedule();

        schedule.setScheduleId(scheduleDTO.getId());
        schedule.setAccountByAccountId(account);
        schedule.setName(scheduleDTO.getScheduleName());
        schedule.setDescription(scheduleDTO.getDescription());
        schedule.setCadmScheduleId(scheduleDTO.getCadmScheduleId());
        schedule.setEnablingGracePeriod(scheduleDTO.getEnablingGracePeriod());
        schedule.setEnablingGroupId(scheduleDTO.getEnablingGroupId());
        schedule.setSiteId(scheduleDTO.getSiteId());

        final LocalDateTime now = LocalDateTime.now();
        schedule.setCreated(now);
        schedule.setUpdated(now);

        //already validated
        ScheduleType scheduleType = scheduleTypeRepository.findById(scheduleDTO.getScheduleTypeId()).get();
        schedule.setScheduleTypeByScheduleTypeId(scheduleType);

        Collection<ScheduleData> scheduleDataBlocks = buildScheduleDataBlocksFromScheduleDTOBlocks(schedule, scheduleDTO.getScheduledBlocks());
        schedule.setScheduleDataByScheduleId(scheduleDataBlocks);

        Collection<ScheduleExceptionData> scheduleExceptionBlocks = buildScheduleExceptionBlocksFromScheduleDTOBlocks(schedule,
                scheduleDTO.getScheduleExceptionBlocks());
        schedule.setScheduleExceptionDataByScheduleId(scheduleExceptionBlocks);

        return schedule;
    }

    private Collection<ScheduleData> buildScheduleDataBlocksFromScheduleDTOBlocks(final Schedule schedule,
                                                                                  final Collection<ScheduleDTO.ScheduledBlock> scheduledBlocks) {
        Collection<ScheduleData> scheduleData = new HashSet<>();

        if (scheduledBlocks.size() > 0) {
            scheduledBlocks.forEach(scheduledBlock -> createAndSetScheduleData(schedule, scheduleData, scheduledBlock));
        }

        return scheduleData;
    }

    private void createAndSetScheduleData(final Schedule schedule,
                                          final Collection<ScheduleData> scheduleData,
                                          final ScheduleDTO.ScheduledBlock scheduledBlock) {
        ScheduleData block = new ScheduleData();

        block.setStartTime(scheduledBlock.getStartMinute());
        block.setStopTime(scheduledBlock.getStopMinute());
        block.setScheduleId(schedule.getScheduleId());
        block.setScheduleByScheduleId(schedule);

        scheduleData.add(block);
    }

    private Collection<ScheduleExceptionData> buildScheduleExceptionBlocksFromScheduleDTOBlocks(final Schedule schedule,
                                                                                                final Collection<ScheduleDTO.ScheduledExceptionBlock> scheduledExceptionBlocks) {
        Collection<ScheduleExceptionData> scheduleExceptionData = new HashSet<>();

        if (scheduledExceptionBlocks.size() > 0) {
            scheduledExceptionBlocks.forEach(exceptionBlock ->
                    createAndSetScheduleExceptionData(schedule, scheduleExceptionData, exceptionBlock));
        }

        return scheduleExceptionData;
    }

    private void createAndSetScheduleExceptionData(final Schedule schedule, final Collection<ScheduleExceptionData> scheduleExceptionData, final ScheduleDTO.ScheduledExceptionBlock exceptionBlock) {
        ScheduleExceptionData exceptionData = new ScheduleExceptionData();

        exceptionData.setExceptionId(exceptionBlock.getExceptionId());
        exceptionData.setStartMin(exceptionBlock.getStartMinute());
        exceptionData.setEndMin(exceptionBlock.getStopMinute());
        exceptionData.setRepeatIndex(exceptionBlock.getRepeatIndex());
        exceptionData.setIsEnableBlock(exceptionBlock.getIsEnableBlock());
        exceptionData.setRepeatOrdinal(exceptionBlock.getRepeatOrdinal());
        exceptionData.setRepeatType(exceptionBlock.getRepeatType());

        exceptionData.setScheduleByScheduleId(schedule);

        scheduleExceptionData.add(exceptionData);
    }

    private Users createUser(final UserDTO userDTO, final Account account) {
        final LocalDateTime now = LocalDateTime.now();
        final Users user = new Users();

        user.setUsersId(userDTO.getId());
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setMiddleName(userDTO.getMiddleName());
        user.setAccountByAccountId(account);
        user.setDisabled(userDTO.getDisabled());
        user.setCreated(now);
        user.setUpdated(now);
        user.setDeactivated(userDTO.getDeactivated());

        // user type id already validated --> validateNewAccountUsers
        final UserType userType = userTypeRepository.findById(userDTO.getUserTypeId()).get();
        user.setUserTypeByUserTypeId(userType);

        BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.USER);
        brivoObject.setObjectId(userDTO.getObjectId());
        brivoObject.setUsersByObjectId(user);

        user.setObjectByBrivoObjectId(brivoObject);

        return user;
    }

    private AccessCredential createCredential(final CredentialDTO credentialDTO, final Account account) {
        final LocalDateTime now = LocalDateTime.now();
        final AccessCredential credential = new AccessCredential();

        credential.setAccessCredentialId(credentialDTO.getId());
        credential.setAccountByAccountId(account);
        credential.setAccountId(account.getAccountId());
        credential.setReferenceId(credentialDTO.getReferenceId());
        credential.setCredential(credentialDTO.getCredential());
        credential.setCreated(now);
        credential.setUpdated(now);
        credential.setEnableOn(credentialDTO.getEnableOn());
        credential.setExpires(credentialDTO.getExpires());
        credential.setDisabled(credentialDTO.getDisabled());
        credential.setLastOwnerObjectId(credentialDTO.getLastOwnerObjectId());
        credential.setNumBits(credentialDTO.getNumBits());

        //already validated
        AccessCredentialType credentialType = accessCredentialTypeRepository.findById(credentialDTO.getAccessCredentialTypeId())
                .get();
        credential.setAccessCredentialTypeByAccessCredentialTypeId(credentialType);

        //some credentials don't have an owner
        final long userObjectId = credentialDTO.getUserObjectId();
        if (userObjectId != NOT_AVAILABLE_LONG_VALUE) {
            BrivoObject ownerObjectId = this.findUserObjectByObjectIdInAccount(account, userObjectId);
            if (ownerObjectId.getObjectId() != NOT_AVAILABLE_LONG_VALUE) {
                credential.setObjectByOwnerObjectId(ownerObjectId);
            }
        }

        return credential;
    }

    private Collection<CredentialFieldValue> getCredentialFieldValueForCredentialDTO(final CredentialDTO credentialDTO) {
        return credentialDTO.getCredentialFieldValues()
                .stream()
                .map(convertCredentialFieldValueDTO(credentialDTO.getId()))
                .collect(Collectors.toSet());
    }

    private Function<CredentialDTO.CredentialFieldValueDTO, CredentialFieldValue> convertCredentialFieldValueDTO(final Long accessCredentialId) {
        return credentialFieldValueDTO -> {
            CredentialFieldValue credentialFieldValue = new CredentialFieldValue();

            credentialFieldValue.setValue(credentialFieldValueDTO.getValue());
            credentialFieldValue.setAccessCredentialId(accessCredentialId);
            credentialFieldValue.setCredentialFieldId(credentialFieldValueDTO.getCredentialFieldId());

            return credentialFieldValue;
        };
    }

    private Brain createPanel(final PanelDTO panel, final Account account) {
        final Brain brain = new Brain();

        brain.setBrainId(panel.getId());
        brain.setObjectId(panel.getObjectId());
        brain.setAccountByAccountId(account);
        brain.setName(panel.getPanelName());
        brain.setElectronicSerialNumber(panel.getElectronicSerialNumber());
        brain.setFirmwareVersion(panel.getFirmwareVersion());
        brain.setHardwareControllerVersion(panel.getHardwareControllerVersion());
        brain.setPhysicalAddress(panel.getPhysicalAddress());
        brain.setPassword(panel.getPassword());
        brain.setTimeZone(panel.getTimeZone());
        brain.setIsRegistered(panel.getIsRegistered());
        brain.setNetworkId(panel.getNetworkId());
        brain.setPanelChanged(panel.getPanelChanged());
        brain.setSchedulesChanged(panel.getSchedulesChanged());
        brain.setPersonsChanged(panel.getPersonsChanged());
        brain.setPanelChecked(panel.getPanelChecked());
        brain.setLogPeriod(panel.getLogPeriod());
        brain.setAntipassbackResetInterval(panel.getAntipassbackResetInterval());

        final LocalDateTime now = LocalDateTime.now();
        brain.setCreated(now);
        brain.setUpdated(now);

        // brain type id already validated --> validateNewAccountPanels
        final BrainType brainType = brainTypeRepository.findById(panel.getBrainTypeId()).get();
        brain.setBrainTypeByBrainTypeId(brainType);

        BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.BRAIN);
        brivoObject.setObjectId(panel.getObjectId());
        brain.setObjectByObjectId(brivoObject);

        if (!shouldNotBrainStateBePersistedForPanel(panel.getBrainState())) {
            BrainState brainState = buildBrainStateForBrain(panel, brivoObject);
            brivoObject.setBrainStateByObjectId(brainState);
        }

        Collection<Board> boards = buildBoardsForPanel(panel.getBoards(), brain);
        brain.setBoardsByObjectId(boards);

        Collection<ProgIoPoints> progIoPoints = buildProgIoPointsForPanel(panel.getProgIoPoints(), brain);
        brain.setProgIoPointsByObjectId(progIoPoints);

        Collection<Rs485Settings> rs485Settings = buildRs485SettingsForPanel(panel.getRs485Settings(), brain);
        brain.setRs485SettingsByBrainId(rs485Settings);

        return brain;
    }

    private Collection<Board> buildBoardsForPanel(Collection<BoardDTO> boardDTOS, final Brain brain) {
        return boardDTOS.stream()
                .map(buildBoardForPanel(brain))
                .collect(Collectors.toSet());
    }

    private Function<BoardDTO, Board> buildBoardForPanel(final Brain brain) {
        return boardDTO -> {
            Board board = new Board();

            board.setObjectId(boardDTO.getObjectId());
            board.setBoardLocation(boardDTO.getBoardLocation());
            board.setBoardNumber(boardDTO.getBoardNumber());

            //already validated
            BoardType boardType = boardTypeRepository.findById(boardDTO.getBoardTypeId()).get();
            board.setBoardTypeByBoardType(boardType);

            BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.BOARD);
            brivoObject.setObjectId(boardDTO.getObjectId());
            board.setObjectByBrivoObjectId(brivoObject);

            Collection<BoardProperty> boardProperties = buildBoardPropertiesForBoard(boardDTO.getBoardProperties(), board);
            board.setBoardPropertiesByObjectId(boardProperties);

            board.setBrainByPanelOid(brain);
            board.setPanelOid(brain.getObjectByObjectId().getObjectId());

            return board;
        };
    }

    private Collection<BoardProperty> buildBoardPropertiesForBoard(final Collection<BoardDTO.BoardPropertyDTO> boardPropertiesDTOS,
                                                                   final Board board) {
        return boardPropertiesDTOS.stream()
                .map(buildBoardPropertyForBoard(board))
                .collect(Collectors.toSet());
    }

    private Function<BoardDTO.BoardPropertyDTO, BoardProperty> buildBoardPropertyForBoard(final Board board) {
        return boardPropertyDTO -> {
            BoardProperty boardProperty = new BoardProperty();

            boardProperty.setNameKey(boardPropertyDTO.getKey());
            boardProperty.setValue(boardPropertyDTO.getValue());
            boardProperty.setValueTypeId(boardPropertyDTO.getValueTypeId());
            boardProperty.setBoardByObjectId(board);
            boardProperty.setBoardObjectId(board.getObjectId());

            return boardProperty;
        };
    }

    private Collection<ProgIoPoints> buildProgIoPointsForPanel(final Collection<ProgIoPointDTO> progIoPointDTOS, final Brain brain) {
        return progIoPointDTOS.stream()
                .map(buildProgIoPointForPanel(brain))
                .collect(Collectors.toSet());
    }

    private Function<ProgIoPointDTO, ProgIoPoints> buildProgIoPointForPanel(final Brain brain) {
        return progIoPointDTO -> {
            ProgIoPoints progIoPoints = new ProgIoPoints();

            progIoPoints.setName(progIoPointDTO.getName());
            progIoPoints.setBoardNumber(progIoPointDTO.getBoardNumber());
            progIoPoints.setEol(progIoPointDTO.getEol());
            progIoPoints.setState(progIoPointDTO.getState());
            progIoPoints.setInuse(progIoPointDTO.getInuse());
            progIoPoints.setType(progIoPointDTO.getType());
            progIoPoints.setPointAddress(progIoPointDTO.getPointAddress());

            progIoPoints.setBrainByObjectId(brain);
            progIoPoints.setPanelOid(brain.getObjectId());

            return progIoPoints;
        };
    }

    private Collection<ProgDevicePuts> buildProgDevicePutsForDevice(final Collection<ProgDevicePutDTO> progDevicePutDTOS,
                                                                    final Device device) {
        return progDevicePutDTOS.stream()
                .map(buildProgDevicePutForDevice(device))
                .collect(Collectors.toSet());
    }

    private Function<ProgDevicePutDTO, ProgDevicePuts> buildProgDevicePutForDevice(final Device device) {
        return progDevicePutDTO -> {
            ProgDevicePuts progDevicePuts = new ProgDevicePuts();

            progDevicePuts.setArgument(progDevicePutDTO.getArgument());
            progDevicePuts.setPointAddress(progDevicePutDTO.getPointAddress());
            progDevicePuts.setBehavior(progDevicePutDTO.getBehavior());
            progDevicePuts.setBoardNumber(progDevicePutDTO.getBoardNumber());
            progDevicePuts.setPanelOid(progDevicePutDTO.getPanelOid());
            progDevicePuts.setDeviceByObjectId(device);
            progDevicePuts.setDeviceOid(device.getObjectByBrivoObjectId().getObjectId());

            return progDevicePuts;
        };
    }

    private Collection<ProgDeviceData> buildProgDeviceDataForDevice(final Collection<ProgDeviceDataDTO> progDeviceDataDTOS,
                                                                    final Device device) {
        return progDeviceDataDTOS.stream()
                .map(buildProgDeviceDataForDevice(device))
                .collect(Collectors.toSet());
    }

    private Function<ProgDeviceDataDTO, ProgDeviceData> buildProgDeviceDataForDevice(final Device device) {
        return progDeviceDataDTO -> {
            ProgDeviceData progDeviceData = new ProgDeviceData();

            progDeviceData.setDisengageMsg(progDeviceDataDTO.getDisengageMsg());
            progDeviceData.setEngageMsg(progDeviceDataDTO.getEngageMsg());
            progDeviceData.setNotifyDiseng(progDeviceDataDTO.getNotifyDiseng());
            progDeviceData.setNotifyFlag(progDeviceDataDTO.getNotifyFlag());
            progDeviceData.setDoorId(progDeviceDataDTO.getDoorId());
            progDeviceData.setEventType(progDeviceDataDTO.getEventType());
            progDeviceData.setDeviceOid(device.getObjectByBrivoObjectId().getObjectId());
            progDeviceData.setDeviceByObjectId(device);
            return progDeviceData;
        };
    }

    private Collection<Rs485Settings> buildRs485SettingsForPanel(final Collection<Rs485SettingDTO> rs485SettingDTOS,
                                                                 final Brain brain) {
        return rs485SettingDTOS.stream()
                .map(buildRs485SettingForPanel(brain))
                .collect(Collectors.toSet());
    }

    private Function<Rs485SettingDTO, Rs485Settings> buildRs485SettingForPanel(final Brain brain) {
        return rs485SettingDTO -> {
            Rs485Settings rs485Settings = new Rs485Settings();

            rs485Settings.setBaudRate(rs485SettingDTO.getBaudRate());
            rs485Settings.setErrorDetectionMethod(rs485SettingDTO.getErrorDetectionMethod());
            rs485Settings.setOperationMode(rs485SettingDTO.getOperationMode());
            rs485Settings.setPort(rs485SettingDTO.getPort());
            rs485Settings.setBrainId(brain.getBrainId());
            rs485Settings.setBrainByBrainId(brain);

            return rs485Settings;
        };
    }

    private Boolean shouldNotBrainStateBePersistedForPanel(final PanelDTO.BrainStateDTO brainStateDTO) {
        return (brainStateDTO.getIpAddress() != null && brainStateDTO.getIpAddress().equals(NOT_AVAILABLE)) &&
                (brainStateDTO.getFirmwareVersion() != null && brainStateDTO.getFirmwareVersion().equals(NOT_AVAILABLE)) &&
                (brainStateDTO.getPanelChecked() != null && brainStateDTO.getPanelChecked().equals(NOT_AVAILABLE_DATE));
    }

    private BrainState buildBrainStateForBrain(PanelDTO panelDTO, BrivoObject brivoObject) {
        final LocalDateTime now = LocalDateTime.now();

        BrainState brainState = new BrainState();
        brainState.setObjectId(panelDTO.getObjectId());

        final PanelDTO.BrainStateDTO brainStateDTO = panelDTO.getBrainState();
        brainState.setIpAddress(brainStateDTO.getIpAddress());
        brainState.setFirmwareVersion(brainStateDTO.getFirmwareVersion());
        brainState.setPanelChecked(brainStateDTO.getPanelChecked());
        brainState.setCreated(now);
        brainState.setUpdated(now);
        brainState.setObjectByBrivoObjectId(brivoObject);

        return brainState;
    }

    private Schedule findScheduleByScheduleIdInAccount(final Account savedAccount, final long scheduleId) {
        return savedAccount.getSchedulesByAccountId()
                .stream()
                .filter(schedule -> schedule.getScheduleId() == scheduleId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Schedule with schedule id "
                        + scheduleId + " not found in saved account " + savedAccount.getName()));
    }

    private BrivoObject findUserGroupObjectByObjectIdInSavedAccount(final Account savedAccount, final long groupObjectId) {
        final SecurityGroup userGroup = savedAccount.getSecurityGroupsByAccountId()
                .stream()
                .filter(group -> group.getObjectByObjectId().getObjectId() == groupObjectId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("SecurityGroup with object id "
                        + groupObjectId + " not found in saved account " + savedAccount.getName()));

        return userGroup.getObjectByObjectId();
    }

    private BrivoObject findDeviceObjectByObjectIdInAccount(final Account savedAccount, final long deviceObjectId) {
        final Device device = savedAccount.getDevicesByAccountId()
                .stream()
                .filter(d -> d.getObjectByBrivoObjectId().getObjectId() == deviceObjectId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Device with object id "
                        + deviceObjectId + " not found in saved account " + savedAccount.getName()));

        return device.getObjectByBrivoObjectId();
    }

    private Device findDeviceByDeviceIdInAccount(final Account savedAccount, final long deviceId) {
        return savedAccount.getDevicesByAccountId()
                .stream()
                .filter(d -> d.getDeviceId() == deviceId)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("Device with id "
                        + deviceId + " not found in saved account " + savedAccount.getName()));
    }

    private BrivoObject findUserObjectByObjectIdInAccount(final Account savedAccount, final long userObjectId) {
        final Users user = savedAccount.getUsersByAccountId()
                .stream()
                .filter(u -> u.getObjectByBrivoObjectId().getObjectId() == userObjectId)
                .findFirst()
                .orElse(getNotAvailableOwnerObject());
//                                       .orElseThrow(() -> new IllegalArgumentException("User with object id "
//                                               + userObjectId + " not found in saved account " + savedAccount.getName()));

        return user.getObjectByBrivoObjectId();
    }

    private Users getNotAvailableOwnerObject() {
        final Users user = new Users();
        final BrivoObject notAvailableOwner = new BrivoObject(BrivoObjectTypes.USER);

        notAvailableOwner.setObjectId(NOT_AVAILABLE_LONG_VALUE);
        user.setObjectByBrivoObjectId(notAvailableOwner);

        return user;
    }

    private void validateNewAccount(final AccountDTO accountDTO) {
        final String accountName = accountDTO.getName();
        if (accountName.isEmpty()) {
            LOGGER.error("The account must have a name");
            throw new AssertionError("The account must have a name");
        }

        final long accountTypeId = accountDTO.getAccountTypeId();
        if (!accountTypeRepository.findById(accountTypeId).isPresent()) {
            LOGGER.error("There is no account type with the ID " + accountTypeId);
            throw new AssertionError("There is no account type with the ID " + accountTypeId);
        }

        validateNewAccountGroups(accountDTO.getGroups());
        validateNewAccountUsers(accountDTO);
        validateNewAccountPanels(accountDTO.getPanels());
        validateNewAccountDevices(accountDTO);
        validateNewAccountSchedules(accountDTO);
        validateNewAccountCredentials(accountDTO.getCredentials());
        validateNewAccountHolidays(accountDTO);

        //TODO add other validations, if needed
    }

    private Boolean isValidSiteId(final AccountDTO accountDTO, final Long siteObjectId) {
        return accountDTO.getSites()
                .stream()
                .anyMatch(site -> site.getObjectId() == siteObjectId);
    }

    private Boolean isValidGroupId(final AccountDTO accountDTO, final Long groupId) {
        return accountDTO.getGroups()
                .stream()
                .anyMatch(group -> group.getId().equals(groupId));
    }

    private Boolean isValidScheduleId(final AccountDTO accountDTO, final Long scheduleId) {
        return accountDTO.getSchedules()
                .stream()
                .anyMatch(scheduleDTO -> scheduleDTO.getId().equals(scheduleId));
    }

    private Boolean isValidBrainObjectId(final AccountDTO accountDTO, final Long brainObjectId) {
        return accountDTO.getPanels()
                .stream()
                .anyMatch(brain -> brain.getObjectId() == brainObjectId);
    }

    private void validateNewAccountHolidays(final AccountDTO accountDTO) {
        accountDTO.getHolidays()
                .forEach(holidayDTO -> {
//                    final Long siteId = holidayDTO.getSiteId();
//                    Assert.state(isValidSiteId(accountDTO, siteId), "There is no site in account with object id "
//                            + siteId + " for holiday with id " + holidayDTO.getId());
                });
    }

    private void validateNewAccountSchedules(final AccountDTO accountDTO) {
        accountDTO.getSchedules()
                .forEach(scheduleDTO -> {
                    final long scheduleTypeId = scheduleDTO.getScheduleTypeId();
                    if (!scheduleTypeRepository.findById(scheduleTypeId).isPresent()) {
                        LOGGER.error("There is no schedule type with the id " + scheduleTypeId);
                        throw new AssertionError("There is no schedule type with the id " + scheduleTypeId);
                    }

                    // FIXME - are siteIds allowed to be 0? (siteId = 0 --> Universal schedule)
//                    final Long siteId = scheduleDTO.getSiteId();
//                    Assert.state(isValidSiteId(accountDTO, siteId), "There is no site in account with object id "
//                            + siteId + " for schedule with id " + scheduleDTO.getId());
                });
    }

    private void validateNewAccountDevices(final AccountDTO accountDTO) {
        Collection<DeviceDTO> deviceDTOS = accountDTO.getDevices();
        deviceDTOS.addAll(accountDTO.getDoors());

        deviceDTOS.forEach(deviceDTO -> {
            final long deviceTypeId = deviceDTO.getDeviceTypeId();
            if (!deviceTypeRepository.findById(deviceTypeId).isPresent()) {
                LOGGER.error("There is no device type with the id " + deviceTypeId);
                throw new AssertionError("There is no device type with the id " + deviceTypeId);
            }

            final Collection<ProgDevicePutDTO> progDevicePutDTOS = deviceDTO.getProgDevicePuts();
            if (progDevicePutDTOS.size() > 0) {
                progDevicePutDTOS.forEach(progDevicePutDTO -> {
                    final Long panelOid = progDevicePutDTO.getPanelOid();
                    if (!isValidBrainObjectId(accountDTO, panelOid)) {
                        LOGGER.error("There is no brain with object id " + panelOid + " for device puts");
                        throw new AssertionError("There is no brain with object id " + panelOid + " for device puts");
                    }
                });
            }

            final Collection<Long> deviceScheduleIds = deviceDTO.getDeviceUnlockScheduleIds();
            if (deviceScheduleIds.size() > 0) {
                deviceScheduleIds.forEach(scheduleId -> {
                    if (!isValidScheduleId(accountDTO, scheduleId)) {
                        LOGGER.error("There is no schedule with schedule id " + scheduleId + " for device schedules");
                        throw new AssertionError("There is no schedule with schedule id " + scheduleId + " for device schedules");
                    }
                });
            }
        });
    }

    private void validateNewAccountGroups(Collection<GroupDTO> groupDTOS) {
        groupDTOS.forEach(groupDTO -> {
            final long groupTypeId = groupDTO.getSecurityGroupTypeId();
            if (!securityGroupTypeRepository.findById(groupTypeId).isPresent()) {
                LOGGER.error("There is no security group type with the id " + groupTypeId);
                throw new AssertionError("There is no security group type with the id " + groupTypeId);
            }
        });
    }

    private void validateNewAccountPanels(Collection<PanelDTO> panelDTOS) {
        panelDTOS.forEach(panelDTO -> {
            final long panelTypeId = panelDTO.getBrainTypeId();
            if (!brainTypeRepository.findById(panelTypeId).isPresent()) {
                LOGGER.error("There is no brain type with the id " + panelTypeId);
                throw new AssertionError("There is no brain type with the id " + panelTypeId);
            }

            final Collection<BoardDTO> boards = panelDTO.getBoards();
            boards.forEach(boardDTO -> {
                final long boardTypeId = boardDTO.getBoardTypeId();
                if (!boardTypeRepository.findById(boardTypeId).isPresent()) {
                    LOGGER.error("There is no board type with the id " + boardTypeId);
                    throw new AssertionError("There is no board type with the id " + boardTypeId);
                }
            });
        });
    }

    private void validateNewAccountUsers(final AccountDTO accountDTO) {
        final Collection<UserDTO> userDTOS = accountDTO.getUsers();

        userDTOS.forEach(userDTO -> {
            final long userTypeId = userDTO.getUserTypeId();
            if (!userTypeRepository.findById(userTypeId).isPresent()) {
                LOGGER.error("There is no user type with the id " + userTypeId);
                throw new AssertionError("There is no user type with the id " + userTypeId);
            }

            final Collection<BigInteger> groupIds = userDTO.getGroupIds();
            groupIds.forEach(groupId -> {
                if (!isValidGroupId(accountDTO, groupId.longValue())) {
                    LOGGER.error("There is no group with securityGroupId = " + groupId + " for user with id " + userDTO.getId());
                    throw new AssertionError("There is no group with securityGroupId = " + groupId + " for user with id " + userDTO.getId());
                }
            });
        });
    }

    private void validateNewAccountCredentials(Collection<CredentialDTO> credentialDTOS) {
        credentialDTOS.forEach(credentialDTO -> {
            final long credentialTypeId = credentialDTO.getAccessCredentialTypeId();
            if (!accessCredentialTypeRepository.findById(credentialTypeId).isPresent()) {
                LOGGER.error("There is no access credential type with the id " + credentialTypeId);
                throw new AssertionError("There is no access credential type with the id " + credentialTypeId);
            }
        });
    }

    private String generateAccountNumber() {
        StringBuilder buffer = new StringBuilder();
        Random rng = new Random();
        int lastDigit = -1;
        while (buffer.length() < 10) {
            int current = rng.nextInt(10);
            if (lastDigit != current) {
                buffer.append(current);
                lastDigit = current;
                continue;
            }
        }
        return buffer.toString();
    }
}
