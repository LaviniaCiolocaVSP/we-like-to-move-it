package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigInteger;
import java.util.Collection;

@Entity
@Table(name = "badge_item", schema = "brivo20", catalog = "onair")
public class BadgeItem {
    private long badgeItemId;
    private long badgeTemplateId;
    private long badgeItemTypeId;
    private long itemOrder;
    private BigInteger width;
    private BigInteger height;
    private BigInteger xPosition;
    private BigInteger yPosition;
    private String backgroundColor;
    private BigInteger backgroundOpacity;
    private String color;
    private Long angle;
    private BigInteger opacity;
    private Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeItemId;
    private Collection<BadgeImageItem> badgeImageItemsByBadgeItemId;
    private BadgeTemplate badgeTemplateByBadgeTemplateId;
    private BadgeItemType badgeItemTypeByBadgeItemTypeId;
    private Collection<BadgeTextItem> badgeTextItemsByBadgeItemId;

    @Id
    @Column(name = "badge_item_id", nullable = false)
    public long getBadgeItemId() {
        return badgeItemId;
    }

    public void setBadgeItemId(long badgeItemId) {
        this.badgeItemId = badgeItemId;
    }

    @Basic
    @Column(name = "badge_template_id", nullable = false)
    public long getBadgeTemplateId() {
        return badgeTemplateId;
    }

    public void setBadgeTemplateId(long badgeTemplateId) {
        this.badgeTemplateId = badgeTemplateId;
    }

    @Basic
    @Column(name = "badge_item_type_id", nullable = false)
    public long getBadgeItemTypeId() {
        return badgeItemTypeId;
    }

    public void setBadgeItemTypeId(long badgeItemTypeId) {
        this.badgeItemTypeId = badgeItemTypeId;
    }

    @Basic
    @Column(name = "item_order", nullable = false)
    public long getItemOrder() {
        return itemOrder;
    }

    public void setItemOrder(long itemOrder) {
        this.itemOrder = itemOrder;
    }

    @Basic
    @Column(name = "width", nullable = false, precision = 0)
    public BigInteger getWidth() {
        return width;
    }

    public void setWidth(BigInteger width) {
        this.width = width;
    }

    @Basic
    @Column(name = "height", nullable = false, precision = 0)
    public BigInteger getHeight() {
        return height;
    }

    public void setHeight(BigInteger height) {
        this.height = height;
    }

    @Basic
    @Column(name = "x_position", nullable = false, precision = 0)
    public BigInteger getxPosition() {
        return xPosition;
    }

    public void setxPosition(BigInteger xPosition) {
        this.xPosition = xPosition;
    }

    @Basic
    @Column(name = "y_position", nullable = false, precision = 0)
    public BigInteger getyPosition() {
        return yPosition;
    }

    public void setyPosition(BigInteger yPosition) {
        this.yPosition = yPosition;
    }

    @Basic
    @Column(name = "background_color", nullable = true, length = 32)
    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Basic
    @Column(name = "background_opacity", nullable = true, precision = 0)
    public BigInteger getBackgroundOpacity() {
        return backgroundOpacity;
    }

    public void setBackgroundOpacity(BigInteger backgroundOpacity) {
        this.backgroundOpacity = backgroundOpacity;
    }

    @Basic
    @Column(name = "color", nullable = true, length = 32)
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Basic
    @Column(name = "angle", nullable = true)
    public Long getAngle() {
        return angle;
    }

    public void setAngle(Long angle) {
        this.angle = angle;
    }

    @Basic
    @Column(name = "opacity", nullable = true, precision = 0)
    public BigInteger getOpacity() {
        return opacity;
    }

    public void setOpacity(BigInteger opacity) {
        this.opacity = opacity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeItem badgeItem = (BadgeItem) o;

        if (badgeItemId != badgeItem.badgeItemId) {
            return false;
        }
        if (badgeTemplateId != badgeItem.badgeTemplateId) {
            return false;
        }
        if (badgeItemTypeId != badgeItem.badgeItemTypeId) {
            return false;
        }
        if (itemOrder != badgeItem.itemOrder) {
            return false;
        }
        if (width != null ? !width.equals(badgeItem.width) : badgeItem.width != null) {
            return false;
        }
        if (height != null ? !height.equals(badgeItem.height) : badgeItem.height != null) {
            return false;
        }
        if (xPosition != null ? !xPosition.equals(badgeItem.xPosition) : badgeItem.xPosition != null) {
            return false;
        }
        if (yPosition != null ? !yPosition.equals(badgeItem.yPosition) : badgeItem.yPosition != null) {
            return false;
        }
        if (backgroundColor != null ? !backgroundColor.equals(badgeItem.backgroundColor) : badgeItem.backgroundColor != null) {
            return false;
        }
        if (backgroundOpacity != null ? !backgroundOpacity.equals(badgeItem.backgroundOpacity) : badgeItem.backgroundOpacity != null) {
            return false;
        }
        if (color != null ? !color.equals(badgeItem.color) : badgeItem.color != null) {
            return false;
        }
        if (angle != null ? !angle.equals(badgeItem.angle) : badgeItem.angle != null) {
            return false;
        }
        return opacity != null ? opacity.equals(badgeItem.opacity) : badgeItem.opacity == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeItemId ^ (badgeItemId >>> 32));
        result = 31 * result + (int) (badgeTemplateId ^ (badgeTemplateId >>> 32));
        result = 31 * result + (int) (badgeItemTypeId ^ (badgeItemTypeId >>> 32));
        result = 31 * result + (int) (itemOrder ^ (itemOrder >>> 32));
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (xPosition != null ? xPosition.hashCode() : 0);
        result = 31 * result + (yPosition != null ? yPosition.hashCode() : 0);
        result = 31 * result + (backgroundColor != null ? backgroundColor.hashCode() : 0);
        result = 31 * result + (backgroundOpacity != null ? backgroundOpacity.hashCode() : 0);
        result = 31 * result + (color != null ? color.hashCode() : 0);
        result = 31 * result + (angle != null ? angle.hashCode() : 0);
        result = 31 * result + (opacity != null ? opacity.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeItemByBadgeItemId")
    public Collection<BadgeBarcodeItem> getBadgeBarcodeItemsByBadgeItemId() {
        return badgeBarcodeItemsByBadgeItemId;
    }

    public void setBadgeBarcodeItemsByBadgeItemId(Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeItemId) {
        this.badgeBarcodeItemsByBadgeItemId = badgeBarcodeItemsByBadgeItemId;
    }

    @OneToMany(mappedBy = "badgeItemByBadgeItemId")
    public Collection<BadgeImageItem> getBadgeImageItemsByBadgeItemId() {
        return badgeImageItemsByBadgeItemId;
    }

    public void setBadgeImageItemsByBadgeItemId(Collection<BadgeImageItem> badgeImageItemsByBadgeItemId) {
        this.badgeImageItemsByBadgeItemId = badgeImageItemsByBadgeItemId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_template_id", referencedColumnName = "badge_template_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeTemplate getBadgeTemplateByBadgeTemplateId() {
        return badgeTemplateByBadgeTemplateId;
    }

    public void setBadgeTemplateByBadgeTemplateId(BadgeTemplate badgeTemplateByBadgeTemplateId) {
        this.badgeTemplateByBadgeTemplateId = badgeTemplateByBadgeTemplateId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_type_id", referencedColumnName = "badge_item_type_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeItemType getBadgeItemTypeByBadgeItemTypeId() {
        return badgeItemTypeByBadgeItemTypeId;
    }

    public void setBadgeItemTypeByBadgeItemTypeId(BadgeItemType badgeItemTypeByBadgeItemTypeId) {
        this.badgeItemTypeByBadgeItemTypeId = badgeItemTypeByBadgeItemTypeId;
    }

    @OneToMany(mappedBy = "badgeItemByBadgeItemId")
    public Collection<BadgeTextItem> getBadgeTextItemsByBadgeItemId() {
        return badgeTextItemsByBadgeItemId;
    }

    public void setBadgeTextItemsByBadgeItemId(Collection<BadgeTextItem> badgeTextItemsByBadgeItemId) {
        this.badgeTextItemsByBadgeItemId = badgeTextItemsByBadgeItemId;
    }
}
