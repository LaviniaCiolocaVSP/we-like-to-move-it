package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IProgDeviceDataRepository;

public interface BrivoProgDeviceDataRepository extends IProgDeviceDataRepository {
}
