package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "video_provider", schema = "brivo20", catalog = "onair")
public class VideoProvider {
    private long videoProviderId;
    private Long objectId;
    private Long accountId;
    private String name;
    private Timestamp created;
    private Timestamp updated;
    private Long deleted;
    private Long parentVideoProviderId;
    private long videoProviderTypeId;
    private Long videoSubscriptionId;
    private Long securityGroupId;
    private Collection<SmartvueNvrRegistration> smartvueNvrRegistrationsByVideoProviderId;
    private Collection<VideoCamera> videoCamerasByVideoProviderId;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    private VideoProvider videoProviderByParentVideoProviderId;
    private Collection<VideoProvider> videoProvidersByVideoProviderId;
    private VideoProviderType videoProviderTypeByVideoProviderTypeId;
    private VideoSubscription videoSubscriptionByVideoSubscriptionId;
    private SecurityGroup securityGroupBySecurityGroupId;
    private Collection<VideoProviderPropertyValue> videoProviderPropertyValuesByVideoProviderId;

    @Id
    @Column(name = "video_provider_id", nullable = false)
    public long getVideoProviderId() {
        return videoProviderId;
    }

    public void setVideoProviderId(long videoProviderId) {
        this.videoProviderId = videoProviderId;
    }

    @Basic
    @Column(name = "object_id", nullable = true)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Long getDeleted() {
        return deleted;
    }

    public void setDeleted(Long deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "parent_video_provider_id", nullable = true)
    public Long getParentVideoProviderId() {
        return parentVideoProviderId;
    }

    public void setParentVideoProviderId(Long parentVideoProviderId) {
        this.parentVideoProviderId = parentVideoProviderId;
    }

    @Basic
    @Column(name = "video_provider_type_id", nullable = false)
    public long getVideoProviderTypeId() {
        return videoProviderTypeId;
    }

    public void setVideoProviderTypeId(long videoProviderTypeId) {
        this.videoProviderTypeId = videoProviderTypeId;
    }

    @Basic
    @Column(name = "video_subscription_id", nullable = true)
    public Long getVideoSubscriptionId() {
        return videoSubscriptionId;
    }

    public void setVideoSubscriptionId(Long videoSubscriptionId) {
        this.videoSubscriptionId = videoSubscriptionId;
    }

    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoProvider that = (VideoProvider) o;

        if (videoProviderId != that.videoProviderId) {
            return false;
        }
        if (videoProviderTypeId != that.videoProviderTypeId) {
            return false;
        }
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) {
            return false;
        }
        if (parentVideoProviderId != null ? !parentVideoProviderId.equals(that.parentVideoProviderId) : that.parentVideoProviderId != null) {
            return false;
        }
        if (videoSubscriptionId != null ? !videoSubscriptionId.equals(that.videoSubscriptionId) : that.videoSubscriptionId != null) {
            return false;
        }
        return securityGroupId != null ? securityGroupId.equals(that.securityGroupId) : that.securityGroupId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoProviderId ^ (videoProviderId >>> 32));
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        result = 31 * result + (parentVideoProviderId != null ? parentVideoProviderId.hashCode() : 0);
        result = 31 * result + (int) (videoProviderTypeId ^ (videoProviderTypeId >>> 32));
        result = 31 * result + (videoSubscriptionId != null ? videoSubscriptionId.hashCode() : 0);
        result = 31 * result + (securityGroupId != null ? securityGroupId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "videoProviderByVideoProviderId")
    public Collection<SmartvueNvrRegistration> getSmartvueNvrRegistrationsByVideoProviderId() {
        return smartvueNvrRegistrationsByVideoProviderId;
    }

    public void setSmartvueNvrRegistrationsByVideoProviderId(Collection<SmartvueNvrRegistration> smartvueNvrRegistrationsByVideoProviderId) {
        this.smartvueNvrRegistrationsByVideoProviderId = smartvueNvrRegistrationsByVideoProviderId;
    }

    @OneToMany(mappedBy = "videoProviderByVideoProviderId")
    public Collection<VideoCamera> getVideoCamerasByVideoProviderId() {
        return videoCamerasByVideoProviderId;
    }

    public void setVideoCamerasByVideoProviderId(Collection<VideoCamera> videoCamerasByVideoProviderId) {
        this.videoCamerasByVideoProviderId = videoCamerasByVideoProviderId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_video_provider_id", referencedColumnName = "video_provider_id", insertable = false, updatable = false)
    public VideoProvider getVideoProviderByParentVideoProviderId() {
        return videoProviderByParentVideoProviderId;
    }

    public void setVideoProviderByParentVideoProviderId(VideoProvider videoProviderByParentVideoProviderId) {
        this.videoProviderByParentVideoProviderId = videoProviderByParentVideoProviderId;
    }

    @OneToMany(mappedBy = "videoProviderByParentVideoProviderId")
    public Collection<VideoProvider> getVideoProvidersByVideoProviderId() {
        return videoProvidersByVideoProviderId;
    }

    public void setVideoProvidersByVideoProviderId(Collection<VideoProvider> videoProvidersByVideoProviderId) {
        this.videoProvidersByVideoProviderId = videoProvidersByVideoProviderId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "video_provider_type_id", referencedColumnName = "video_provider_type_id", nullable = false, insertable = false, updatable = false)
    public VideoProviderType getVideoProviderTypeByVideoProviderTypeId() {
        return videoProviderTypeByVideoProviderTypeId;
    }

    public void setVideoProviderTypeByVideoProviderTypeId(VideoProviderType videoProviderTypeByVideoProviderTypeId) {
        this.videoProviderTypeByVideoProviderTypeId = videoProviderTypeByVideoProviderTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "video_subscription_id", referencedColumnName = "video_subscription_id", insertable = false, updatable = false)
    public VideoSubscription getVideoSubscriptionByVideoSubscriptionId() {
        return videoSubscriptionByVideoSubscriptionId;
    }

    public void setVideoSubscriptionByVideoSubscriptionId(VideoSubscription videoSubscriptionByVideoSubscriptionId) {
        this.videoSubscriptionByVideoSubscriptionId = videoSubscriptionByVideoSubscriptionId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "security_group_id", referencedColumnName = "security_group_id", insertable = false, updatable = false)
    public SecurityGroup getSecurityGroupBySecurityGroupId() {
        return securityGroupBySecurityGroupId;
    }

    public void setSecurityGroupBySecurityGroupId(SecurityGroup securityGroupBySecurityGroupId) {
        this.securityGroupBySecurityGroupId = securityGroupBySecurityGroupId;
    }

    @OneToMany(mappedBy = "videoProviderByVideoProviderId")
    public Collection<VideoProviderPropertyValue> getVideoProviderPropertyValuesByVideoProviderId() {
        return videoProviderPropertyValuesByVideoProviderId;
    }

    public void setVideoProviderPropertyValuesByVideoProviderId(Collection<VideoProviderPropertyValue> videoProviderPropertyValuesByVideoProviderId) {
        this.videoProviderPropertyValuesByVideoProviderId = videoProviderPropertyValuesByVideoProviderId;
    }
}
