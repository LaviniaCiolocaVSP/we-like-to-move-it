package com.brivo.panel.server.reference.service.credential.utils;

import com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding;
import org.apache.commons.lang.StringUtils;

import java.math.BigInteger;

import static com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding.BCD;
import static com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding.BCD_PARITY;
import static com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding.REVERSE_BCD_PARITY;

public class RadixUtils
{
    
    public static String convertBase(String source, int toRadix) {
        return convertBase(source, 10, toRadix);
    }
    
    public static String convertBase(String source, int fromRadix, int toRadix) {
        return new BigInteger(source, fromRadix).toString(toRadix);
    }
    
    
    public static String toBCD(String decimalString, CredentialEncoding bcdEncoding, int numDigits) {
        
        decimalString = StringUtils.leftPad(decimalString, numDigits, '0');
        
        return toBCD(decimalString, bcdEncoding);
    }    
    
    public static String toBCD(String decimalString, CredentialEncoding bcdEncoding) {
        
        // BCD and BCD_PARITY as defined by FIPS-201 are little endian, whereas
        // our calculations here are big endian. Hence the counterintuitive need
        // for reversing the encodings that are not REVERSE BCD
        boolean needsReversing = bcdEncoding == BCD || bcdEncoding == BCD_PARITY;
        boolean needsParity = bcdEncoding == BCD_PARITY || bcdEncoding == REVERSE_BCD_PARITY;
        
        StringBuilder bcdValue = new StringBuilder();
        
        for (int i = 0; i < decimalString.length(); i++) {
            String digitToEncode = decimalString.substring(i, i+1);
            
            String binaryEncodedDigit = convertBase(digitToEncode, 10, 2);
            
            binaryEncodedDigit = StringUtils.leftPad(binaryEncodedDigit, 4, '0');
            
            if (needsReversing) {
                binaryEncodedDigit = StringUtils.reverse(binaryEncodedDigit);
            }            
            if (needsParity) {
                binaryEncodedDigit += calculateParityBit(binaryEncodedDigit, false);
            }
            
            bcdValue.append(binaryEncodedDigit);            
        }
        
        return bcdValue.toString();
        
    }
    
    public static String fromBCD(String bcdString, CredentialEncoding bcdEncoding) {
        
        int digitLength = bcdEncoding.bitsPerDigit();
        
        // BCD and BCD_PARITY as defined by FIPS-201 are little endian, whereas
        // our calculations here are big endian. Hence the counterintuitive need
        // for reversing the encodings that are not REVERSE BCD
        boolean needsReversing = bcdEncoding == BCD || bcdEncoding == BCD_PARITY;
        boolean needsParity = bcdEncoding == BCD_PARITY || bcdEncoding == REVERSE_BCD_PARITY;
        
        StringBuilder decodedValue = new StringBuilder();
        
        for (int i = 0; i < bcdString.length(); i+= digitLength) {
            
            int end = i+4;
            
            String binaryDigit = bcdString.substring(i, i+4);
            
            // ensure parity matches
            if (needsParity)
            {
                char correctParityBit = calculateParityBit(binaryDigit, false);
                char actualParityBit = bcdString.charAt(end);
                if (correctParityBit != actualParityBit)
                {
                    // the input bcd string was not actually valid bcd
                    // they didn't match
                    return null;
                }
            }
            
            if (needsReversing) {
                binaryDigit = StringUtils.reverse(binaryDigit);
            }            
           
            String decimalDigit = convertBase(binaryDigit, 2, 10);
            decodedValue.append(decimalDigit);
        }
        
        return decodedValue.toString();
    }
    
    public static char calculateParityBit(String bitsToCheck, boolean even) {
        char parityBit = even ? '0' : '1';
        
        for (int i = 0; i < bitsToCheck.length(); i++) {
            parityBit = parityBit == bitsToCheck.charAt(i) ? '0' : '1';            
        }
        return parityBit;        
    }

}
