package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class Floor {
    private Long deviceId;
    private Long unlockScheduleId;

    private List<FloorElevator> floorElevators;

    private List<EnterPermission> enterPermissions;

    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getUnlockScheduleId() {
        return unlockScheduleId;
    }

    public void setUnlockScheduleId(Long unlockScheduleId) {
        this.unlockScheduleId = unlockScheduleId;
    }

    public List<FloorElevator> getFloorElevators() {
        return floorElevators;
    }

    public void setFloorElevators(List<FloorElevator> floorElevators) {
        this.floorElevators = floorElevators;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }
}
