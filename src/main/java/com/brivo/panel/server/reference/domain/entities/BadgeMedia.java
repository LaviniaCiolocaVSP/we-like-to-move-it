package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_media", schema = "brivo20", catalog = "onair")
public class BadgeMedia {
    private long badgeMediaId;
    private String filename;
    private String contentType;
    private String mediaContent;
    private Long mediaByteSize;
    private Collection<BadgeImageItem> badgeImageItemsByBadgeMediaId;
    private Collection<BadgeTemplate> badgeTemplatesByBadgeMediaId;

    @Id
    @Column(name = "badge_media_id", nullable = false)
    public long getBadgeMediaId() {
        return badgeMediaId;
    }

    public void setBadgeMediaId(long badgeMediaId) {
        this.badgeMediaId = badgeMediaId;
    }

    @Basic
    @Column(name = "filename", nullable = true, length = 256)
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Basic
    @Column(name = "content_type", nullable = true, length = 128)
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Basic
    @Column(name = "media_content", nullable = false, length = -1)
    public String getMediaContent() {
        return mediaContent;
    }

    public void setMediaContent(String mediaContent) {
        this.mediaContent = mediaContent;
    }

    @Basic
    @Column(name = "media_byte_size", nullable = true)
    public Long getMediaByteSize() {
        return mediaByteSize;
    }

    public void setMediaByteSize(Long mediaByteSize) {
        this.mediaByteSize = mediaByteSize;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeMedia that = (BadgeMedia) o;

        if (badgeMediaId != that.badgeMediaId) {
            return false;
        }
        if (filename != null ? !filename.equals(that.filename) : that.filename != null) {
            return false;
        }
        if (contentType != null ? !contentType.equals(that.contentType) : that.contentType != null) {
            return false;
        }
        if (mediaContent != null ? !mediaContent.equals(that.mediaContent) : that.mediaContent != null) {
            return false;
        }
        return mediaByteSize != null ? mediaByteSize.equals(that.mediaByteSize) : that.mediaByteSize == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeMediaId ^ (badgeMediaId >>> 32));
        result = 31 * result + (filename != null ? filename.hashCode() : 0);
        result = 31 * result + (contentType != null ? contentType.hashCode() : 0);
        result = 31 * result + (mediaContent != null ? mediaContent.hashCode() : 0);
        result = 31 * result + (mediaByteSize != null ? mediaByteSize.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeMediaByBadgeMediaId")
    public Collection<BadgeImageItem> getBadgeImageItemsByBadgeMediaId() {
        return badgeImageItemsByBadgeMediaId;
    }

    public void setBadgeImageItemsByBadgeMediaId(Collection<BadgeImageItem> badgeImageItemsByBadgeMediaId) {
        this.badgeImageItemsByBadgeMediaId = badgeImageItemsByBadgeMediaId;
    }

    @OneToMany(mappedBy = "badgeMediaByBgImageMediaId")
    public Collection<BadgeTemplate> getBadgeTemplatesByBadgeMediaId() {
        return badgeTemplatesByBadgeMediaId;
    }

    public void setBadgeTemplatesByBadgeMediaId(Collection<BadgeTemplate> badgeTemplatesByBadgeMediaId) {
        this.badgeTemplatesByBadgeMediaId = badgeTemplatesByBadgeMediaId;
    }
}
