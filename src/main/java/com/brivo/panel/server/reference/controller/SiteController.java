package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.ui.UiSiteDTO;
import com.brivo.panel.server.reference.service.GroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/site")
public class SiteController {

    private final GroupService securityGroupService;

    @Autowired
    public SiteController(GroupService securityGroupService) {
        this.securityGroupService = securityGroupService;
    }

    @GetMapping("/{accountId}")
    public Collection<UiSiteDTO> getSitesForAccount(@PathVariable final long accountId) {
        return this.securityGroupService.getUiSiteDTOSForAccount(accountId);
    }

    @GetMapping("/addAllSitesAndCurrentPrivileges/{accountId}")
    public Collection<UiSiteDTO> getSitesForAccountForEditGroup(@PathVariable final long accountId) {
        return this.securityGroupService.getUISiteDTOSForAccountForEditGroup(accountId);
    }
}
