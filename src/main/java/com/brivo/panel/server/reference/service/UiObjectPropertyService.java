package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.common.IObjectPropertyRepository;
import com.brivo.panel.server.reference.domain.entities.ObjectProperty;
import com.brivo.panel.server.reference.domain.repository.ObjectPropertyRepository;
import com.brivo.panel.server.reference.dto.ObjectPropertyDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Collections;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UiObjectPropertyService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiObjectPropertyService.class);

    private final ObjectPropertyRepository objectPropertyRepository;

    @Autowired
    public UiObjectPropertyService(final ObjectPropertyRepository objectPropertyRepository) {
        this.objectPropertyRepository = objectPropertyRepository;
    }

    public Collection<ObjectPropertyDTO> getObjectPropertyDTOS(final Collection<ObjectProperty> objectProperties) {
        return objectProperties.stream()
                               .map(convertObjectProperty())
                               .collect(Collectors.toSet());
    }

    private Function<ObjectProperty, ObjectPropertyDTO> convertObjectProperty() {
        return objectProperty -> new ObjectPropertyDTO(objectProperty.getObjectId(), objectProperty.getId(),
                objectProperty.getValue());
    }

    public Collection<ObjectProperty> getObjectProperties(final Long accountId,
                                                          final IObjectPropertyRepository dynamicObjectPropertyRepository) {
        return dynamicObjectPropertyRepository.findByAccountId(accountId)
                                              .orElse(Collections.emptyList());
    }
}
