package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "badge_image_item", schema = "brivo20", catalog = "onair")
public class BadgeImageItem {
    private long badgeImageItemId;
    private long badgeImageTypeId;
    private long badgeItemId;
    private Long badgeMediaId;
    private Short lockedAspectRatio;
    private BadgeImageType badgeImageTypeByBadgeImageTypeId;
    private BadgeItem badgeItemByBadgeItemId;
    private BadgeMedia badgeMediaByBadgeMediaId;

    @Id
    @Column(name = "badge_image_item_id", nullable = false)
    public long getBadgeImageItemId() {
        return badgeImageItemId;
    }

    public void setBadgeImageItemId(long badgeImageItemId) {
        this.badgeImageItemId = badgeImageItemId;
    }

    @Basic
    @Column(name = "badge_image_type_id", nullable = false)
    public long getBadgeImageTypeId() {
        return badgeImageTypeId;
    }

    public void setBadgeImageTypeId(long badgeImageTypeId) {
        this.badgeImageTypeId = badgeImageTypeId;
    }

    @Basic
    @Column(name = "badge_item_id", nullable = false)
    public long getBadgeItemId() {
        return badgeItemId;
    }

    public void setBadgeItemId(long badgeItemId) {
        this.badgeItemId = badgeItemId;
    }

    @Basic
    @Column(name = "badge_media_id", nullable = true)
    public Long getBadgeMediaId() {
        return badgeMediaId;
    }

    public void setBadgeMediaId(Long badgeMediaId) {
        this.badgeMediaId = badgeMediaId;
    }

    @Basic
    @Column(name = "locked_aspect_ratio", nullable = true)
    public Short getLockedAspectRatio() {
        return lockedAspectRatio;
    }

    public void setLockedAspectRatio(Short lockedAspectRatio) {
        this.lockedAspectRatio = lockedAspectRatio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeImageItem that = (BadgeImageItem) o;

        if (badgeImageItemId != that.badgeImageItemId) {
            return false;
        }
        if (badgeImageTypeId != that.badgeImageTypeId) {
            return false;
        }
        if (badgeItemId != that.badgeItemId) {
            return false;
        }
        if (badgeMediaId != null ? !badgeMediaId.equals(that.badgeMediaId) : that.badgeMediaId != null) {
            return false;
        }
        return lockedAspectRatio != null ? lockedAspectRatio.equals(that.lockedAspectRatio) : that.lockedAspectRatio == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeImageItemId ^ (badgeImageItemId >>> 32));
        result = 31 * result + (int) (badgeImageTypeId ^ (badgeImageTypeId >>> 32));
        result = 31 * result + (int) (badgeItemId ^ (badgeItemId >>> 32));
        result = 31 * result + (badgeMediaId != null ? badgeMediaId.hashCode() : 0);
        result = 31 * result + (lockedAspectRatio != null ? lockedAspectRatio.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "badge_image_type_id", referencedColumnName = "badge_image_type_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeImageType getBadgeImageTypeByBadgeImageTypeId() {
        return badgeImageTypeByBadgeImageTypeId;
    }

    public void setBadgeImageTypeByBadgeImageTypeId(BadgeImageType badgeImageTypeByBadgeImageTypeId) {
        this.badgeImageTypeByBadgeImageTypeId = badgeImageTypeByBadgeImageTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_id", referencedColumnName = "badge_item_id", nullable = false, insertable = false,
            updatable = false)
    public BadgeItem getBadgeItemByBadgeItemId() {
        return badgeItemByBadgeItemId;
    }

    public void setBadgeItemByBadgeItemId(BadgeItem badgeItemByBadgeItemId) {
        this.badgeItemByBadgeItemId = badgeItemByBadgeItemId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_media_id", referencedColumnName = "badge_media_id", insertable = false, updatable = false)
    public BadgeMedia getBadgeMediaByBadgeMediaId() {
        return badgeMediaByBadgeMediaId;
    }

    public void setBadgeMediaByBadgeMediaId(BadgeMedia badgeMediaByBadgeMediaId) {
        this.badgeMediaByBadgeMediaId = badgeMediaByBadgeMediaId;
    }
}
