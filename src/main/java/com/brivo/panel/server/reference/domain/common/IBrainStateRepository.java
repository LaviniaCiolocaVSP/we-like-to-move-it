package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.BrainState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IBrainStateRepository extends CrudRepository<BrainState, Long> {
    Optional<BrainState> findByObjectId(@Param("objectId") long objectId);
}