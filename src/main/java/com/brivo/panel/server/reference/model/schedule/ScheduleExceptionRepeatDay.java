package com.brivo.panel.server.reference.model.schedule;

import java.time.DayOfWeek;

/**
 * Schedule exception day of the week.
 *
 * @author brandon
 */
public enum ScheduleExceptionRepeatDay {
    NONE(-1, null),
    SUNDAY(0, DayOfWeek.SUNDAY),
    MONDAY(1, DayOfWeek.MONDAY),
    TUESDAY(2, DayOfWeek.TUESDAY),
    WEDNESDAY(3, DayOfWeek.WEDNESDAY),
    THURSDAY(4, DayOfWeek.THURSDAY),
    FRIDAY(5, DayOfWeek.FRIDAY),
    SATURDAY(6, DayOfWeek.SATURDAY);

    private int dayId;
    private DayOfWeek dayOfWeek;

    ScheduleExceptionRepeatDay(int dayId, DayOfWeek dayOfWeek) {
        this.dayId = dayId;
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * Return the enumeration value given the integer Id.
     */
    public static ScheduleExceptionRepeatDay getScheduleExceptionRepeatDay(int dayId) {
        ScheduleExceptionRepeatDay day = ScheduleExceptionRepeatDay.NONE;

        if (dayId == 0) {
            day = ScheduleExceptionRepeatDay.SUNDAY;
        } else if (dayId == 1) {
            day = ScheduleExceptionRepeatDay.MONDAY;
        } else if (dayId == 2) {
            day = ScheduleExceptionRepeatDay.TUESDAY;
        } else if (dayId == 3) {
            day = ScheduleExceptionRepeatDay.WEDNESDAY;
        } else if (dayId == 4) {
            day = ScheduleExceptionRepeatDay.THURSDAY;
        } else if (dayId == 5) {
            day = ScheduleExceptionRepeatDay.FRIDAY;
        } else if (dayId == 6) {
            day = ScheduleExceptionRepeatDay.SATURDAY;
        }

        return day;
    }

    public int getScheduleExceptionRepeatDayId() {
        return this.dayId;
    }

    public DayOfWeek getDayOfWeek() {
        return this.dayOfWeek;
    }
}
