package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.config.brivo.accounts.AbstractBrivoAccountsDataSourceConfig;
import com.brivo.panel.server.reference.dto.AccountDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountDashboardSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import com.brivo.panel.server.reference.service.account.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;

@RestController
@RequestMapping(
        path = "/account",
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE
)
public class AccountController {

    private final AccountService accountService;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    public AccountController(final AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/databaseConnectionName")
    public Map getDatabaseConnectionName() {
        return Collections.singletonMap("name", AbstractBrivoAccountsDataSourceConfig.DATABASE_CONNECTION_NAME);
    }

    @GetMapping("/current")
    public UiAccountSummaryDTO getCurrentAccount() {
        return accountService.getCurrentAccount();
    }

    @GetMapping("/currentDashboardSummary")
    public UiAccountDashboardSummaryDTO getCurrentAccountDashboardSummary() {
        return accountService.getCurrentUiAccountDashboardSummary();
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final AccountDTO accountDTO) {
        return accountService.create(accountDTO);
    }

    @PutMapping("/current")
    public MessageDTO setCurrentAccount(@RequestParam final String accountName) {
        LOGGER.debug("Setting current account {}", accountName);
        return accountService.setCurrentAccount(accountName);
    }

    @PutMapping("/bol")
    public MessageDTO setBOLAccount(@RequestParam final String accountName) {
        LOGGER.debug("Setting BOL account {}", accountName);
        return accountService.setBOLAccount(accountName);
    }

    @GetMapping
    public Collection<UiAccountSummaryDTO> getAccountNamesAndIds(@RequestParam String nameFilter,
                                                                 @RequestParam int pageIndex,
                                                                 @RequestParam int pageSize) {
        return accountService.getAccountNamesAndIds(nameFilter, pageIndex, pageSize);
    }

    @GetMapping("/download/current/{accountId}")
    public ResponseEntity<StreamingResponseBody> downloadCurrentAccount(@PathVariable final long accountId) {
        return accountService.downloadCurrentAccount(accountId);
    }

    @GetMapping("/download/brivo/{accountId}")
    public ResponseEntity<StreamingResponseBody> downloadBrivoAccount(@PathVariable final long accountId) {
        return accountService.downloadBrivoAccount(accountId);
    }
}
