package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.EventTrackDevice;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Event Track Device Extractor
 */
@Component
public class EventTrackDeviceExtractor
        implements ResultSetExtractor<List<EventTrackDevice>> {
    @Override
    public List<EventTrackDevice> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<EventTrackDevice> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Long doorId = rs.getLong("doorID");
            Integer eventTypeId = rs.getInt("eventTypeID");

            Integer notifyFlagInt = rs.getInt("notifyFlag");
            Boolean notify = false;
            if (notifyFlagInt != null && notifyFlagInt == 1) {
                notify = true;
            }

            Long scheduleId = rs.getLong("scheduleID");

            EventTrackDevice device = new EventTrackDevice();
            device.setDeviceId(deviceId);
            device.setMonitoredDoorId(doorId);
            device.setMonitoredEventTypeId(eventTypeId);
            device.setReportEngage(notify);
            device.setActiveScheduleId(scheduleId);
            list.add(device);
        }

        return list;
    }
}
