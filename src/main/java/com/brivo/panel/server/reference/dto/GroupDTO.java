package com.brivo.panel.server.reference.dto;


import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.Objects;

public class GroupDTO extends AbstractDTO {

    private final Long objectId;
    private final Long groupId;
    private final String groupName;
    private final Long securityGroupTypeId;
    private final Long parentId;
    private final Short antipassbackImunity;
    private final Long antipassbackResetTime;
    private final Collection<ObjectPermissionForGroupDTO> objectPermissions;
    private final Short lockdown;
    private final Short deleted;
    private final Short disabled;

    @JsonCreator
    public GroupDTO(@JsonProperty("objectId") final Long objectId, @JsonProperty("id") final Long groupId,
                    @JsonProperty("groupName") final String groupName,
                    @JsonProperty("securityGroupTypeId") final Long securityGroupTypeId,
                    @JsonProperty("parentId") final Long parentId,
                    @JsonProperty("antipassbackImunity") final Short antipassbackImunity,
                    @JsonProperty("antipassbackResetTime") final Long antipassbackResetTime,
                    @JsonProperty("objectPermissions") Collection<ObjectPermissionForGroupDTO> objectPermissions,
                    @JsonProperty("lockdown") final Short lockdown, @JsonProperty("deleted") final Short deleted,
                    @JsonProperty("disabled") final Short disabled) {
        this.objectId = objectId;
        this.groupId = groupId;
        this.groupName = groupName;
        this.parentId = parentId;
        this.antipassbackImunity = antipassbackImunity;
        this.antipassbackResetTime = antipassbackResetTime;
        this.objectPermissions = objectPermissions;
        this.securityGroupTypeId = securityGroupTypeId;
        this.lockdown = lockdown;
        this.deleted = deleted;
        this.disabled = disabled;
    }

    @Override
    public Long getId() {
        return groupId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public String getGroupName() {
        return groupName;
    }

    public Long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public Long getParentId() {
        return parentId;
    }

    public Short getAntipassbackImunity() {
        return antipassbackImunity;
    }

    public Long getAntipassbackResetTime() {
        return antipassbackResetTime;
    }

    public Short getLockdown() {
        return lockdown;
    }

    public Short getDeleted() {
        return deleted;
    }

    public Short getDisabled() {
        return disabled;
    }

    public Collection<ObjectPermissionForGroupDTO> getObjectPermissions() {
        return objectPermissions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupDTO groupDTO = (GroupDTO) o;
        return Objects.equals(groupId, groupDTO.groupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(groupId);
    }
}
