package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class PanelEventDataProcessor implements EventDataProcessor {
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private EventResolutionDao eventResolutionDao;

    private String allegionGatewayName = "Allegion Gateway";
    /*@Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolvePanelEvent");

        Boolean result = false;

        List<EventResolutionData> dataList = this.eventResolutionDao.getPanelEvent(event);

        if(dataList.size() == 1)
        {
            event.setAccountId(dataList.get(0).getAccountId());
            event.setObjectId(panel.getObjectId());
            event.getEventData().setObjectTypeId(dataList.get(0).getObjectTypeId());
            event.setObjectGroupObjectId(dataList.get(0).getSiteOid());
            event.getEventData().setObjectGroupName(dataList.get(0).getSecurityGroupName());
            event.getEventData().setActionAllowed(false);

            if (event.getSecurityActionId() != null && (event.getSecurityActionId() == SecurityAction.GATEWAY_DISCONNECTED.getSecurityActionValue()
                    || event.getSecurityActionId() == SecurityAction.GATEWAY_RECONNECTED.getSecurityActionValue()))
            {
                event.setObjectId(event.getActorObjectId());
                event.setActorObjectId(panel.getObjectId());
                event.getEventData().setActorName(panel.getElectronicSerialNumber());
                event.getEventData().setObjectName(allegionGatewayName);
            } else if (event.getSecurityActionId() != null && (event.getSecurityActionId() == SecurityAction.WIRELESS_LOCK_DISCONNECTED.getSecurityActionValue()
                    || event.getSecurityActionId() == SecurityAction.WIRELESS_LOCK_RECONNECTED.getSecurityActionValue()))
            {
                event.getEventData().setActorName(event.getEventData().getActorName());
                event.getEventData().setObjectName(event.getEventData().getActorName());
                event.setObjectId(event.getActorObjectId());
            } else
            {
                event.setActorObjectId(panel.getObjectId());
                event.getEventData().setActorName(panel.getElectronicSerialNumber());
                event.getEventData().setObjectName(dataList.get(0).getDeviceName());
            }
            
            result = true;
        }

        logger.trace("<<< ResolvePanelEvent");

        return result;
    }*/

}
