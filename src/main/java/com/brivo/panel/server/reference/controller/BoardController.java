package com.brivo.panel.server.reference.controller;


import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiBoardDTO;
import com.brivo.panel.server.reference.dto.ui.UiBoardTypeDTO;
import com.brivo.panel.server.reference.dto.ui.UiDoorBoardDTO;
import com.brivo.panel.server.reference.dto.ui.UiIOBoardDTO;
import com.brivo.panel.server.reference.service.UiBoardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/board")
public class BoardController {
    
    private final UiBoardService boardService;
    
    @Autowired
    public BoardController(final UiBoardService boardService) {
        this.boardService = boardService;
    }
    
    @GetMapping("/{panelOid}")
    public Collection<UiBoardDTO> getBoardsForPanel(@PathVariable final Long panelOid) {
        return boardService.getUiBoardDTOSForPanel(panelOid);
    }
    
    @GetMapping("/nodes/{boardObjectId}")
    public Collection<UiBoardDTO.UiNodeDTO> getNodesForBoard(@PathVariable final Long boardObjectId) {
        return boardService.getUiNodeDTOSForBoard(boardObjectId);
    }
    
    @GetMapping("/boardTypes")
    public Collection<UiBoardTypeDTO> getBoardTypes() {
        return boardService.getUiBoardTypeDTOS();
    }
    
    @GetMapping("/boardTypes/{panelOid}")
    public Collection<UiBoardTypeDTO> getBoardTypesForPanel(@PathVariable final Long panelOid) {
        return boardService.getUiBoardTypeDTOSForPanel(panelOid);
    }
    
    @GetMapping("/availableBoardNumbers/{panelOid}")
    public Collection<Integer> getAvailableBoardNumbers(@PathVariable final Long panelOid) {
        return boardService.getAvailableBoardNumbers(panelOid);
    }
    
    @GetMapping("/doorBoardTemplatePoints/{panelOid}/{boardTypeId}")
    public UiDoorBoardDTO getDoorBoardTemplatePoints(@PathVariable final Long panelOid, @PathVariable final Long boardTypeId) {
        return boardService.getUiDoorBoardIoPointsTemplate(panelOid, boardTypeId);
    }
    
    @GetMapping("/doorBoardProgIoPoints/{panelOid}/{boardNumber}")
    public UiDoorBoardDTO getDoorBoardProgIoPoints(@PathVariable final Long panelOid, @PathVariable final Long boardNumber) {
        return boardService.getUiDoorBoardProgIoPoints(panelOid, boardNumber);
    }
    
    @GetMapping("/ioBoardTemplatePoints/{panelOid}/{boardTypeId}")
    public UiIOBoardDTO getIOBoardTemplatePoints(@PathVariable final Long panelOid, @PathVariable final Long boardTypeId) {
        return boardService.getUiIOBoardIoPointsTemplate(panelOid, boardTypeId);
    }
    
    @GetMapping("/ioBoardProgIoPoints/{panelOid}/{boardNumber}")
    public UiIOBoardDTO getIOBoardProgIoPoints(@PathVariable final Long panelOid, @PathVariable final Long boardNumber) {
        return boardService.getUiIOBoardProgIoPoints(panelOid, boardNumber);
    }
    
    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final UiBoardDTO boardDTO) {
        return this.boardService.create(boardDTO);
    }
    
    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update(@RequestBody final UiBoardDTO boardDTO) {
        return this.boardService.update(boardDTO);
    }
    
    
    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteBoards(@RequestBody List<Long> boardObjectIds) {
        return this.boardService.deleteBoardsByIds(boardObjectIds);
    }
}