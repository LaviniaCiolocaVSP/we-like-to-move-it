package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

public class CredentialDTO extends AbstractDTO {
    private final long userObjectId;
    private final long accessCredentialId;
    private final long accessCredentialTypeId;
    private final String credential;
    private final String referenceId;
    private final LocalDateTime enableOn;
    private final LocalDateTime expires;
    private final Short disabled;
    private final Long numBits;
    private final Long lastOwnerObjectId;
    private final Collection<CredentialFieldValueDTO> credentialFieldValues;

    @JsonCreator
    public CredentialDTO(@JsonProperty("userObjectId") final long userObjectId,
                         @JsonProperty("id") final long accessCredentialId,
                         @JsonProperty("accessCredentialTypeId") final long accessCredentialTypeId,
                         @JsonProperty("credential") final String credential,
                         @JsonProperty("referenceId") final String referenceId,
                         @JsonProperty("enableOn") final LocalDateTime enableOn,
                         @JsonProperty("expires") final LocalDateTime expires,
                         @JsonProperty("disabled") final Short disabled,
                         @JsonProperty("numBits") final Long numBits,
                         @JsonProperty("lastOwnerObjectId") final Long lastOwnerObjectId,
                         @JsonProperty("credentialFieldValues") final Collection<CredentialFieldValueDTO> credentialFieldValues) {
        this.userObjectId = userObjectId;
        this.accessCredentialId = accessCredentialId;
        this.accessCredentialTypeId = accessCredentialTypeId;
        this.credential = credential;
        this.referenceId = referenceId;
        this.enableOn = enableOn;
        this.expires = expires;
        this.disabled = disabled;
        this.numBits = numBits;
        this.lastOwnerObjectId = lastOwnerObjectId;
        this.credentialFieldValues = credentialFieldValues;
    }

    @Override
    public Long getId() {
        return accessCredentialId;
    }

    public long getUserObjectId() {
        return userObjectId;
    }

    public long getAccessCredentialTypeId() {
        return accessCredentialTypeId;
    }

    public String getCredential() {
        return credential;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public LocalDateTime getEnableOn() {
        return enableOn;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    public Short getDisabled() {
        return disabled;
    }

    public Long getNumBits() {
        return numBits;
    }

    public Long getLastOwnerObjectId() {
        return lastOwnerObjectId;
    }

    public Collection<CredentialFieldValueDTO> getCredentialFieldValues() {
        return credentialFieldValues;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CredentialDTO that = (CredentialDTO) o;
        return accessCredentialId == that.accessCredentialId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessCredentialId);
    }

    public static class CredentialFieldValueDTO {
        private final Long credentialFieldId;
        private final String value;

        @JsonCreator
        public CredentialFieldValueDTO(@JsonProperty("credentialFieldId") final Long credentialFieldId,
                                       @JsonProperty("value") final String value) {
            this.credentialFieldId = credentialFieldId;
            this.value = value;
        }

        public Long getCredentialFieldId() {
            return credentialFieldId;
        }

        public String getValue() {
            return value;
        }
    }
}
