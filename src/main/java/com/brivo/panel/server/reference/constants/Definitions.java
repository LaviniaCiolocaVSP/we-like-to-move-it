package com.brivo.panel.server.reference.constants;

public class Definitions {

    //Types of I/O points (or reader)
    public static final int INPUT_POINT = 1;
    public static final int OUTPUT_POINT = 2;
    public static final int READER_POINT = 3;

    //Used for determining what environment we are operating in
    //panel-wise.
    public static final int MIN_PROGIO_VERSION = 5;
    public static final int PROGIO_SCHEDULE_TYPE = 3;
    public static final int GENIV_SCHEDULE_TYPE = 1;

    //States of I/O points
    public static final int IO_FREE = 0;
    public static final int IO_DEVICE = 1;
    public static final int IO_DOOR = 2;

    // IPAC IO Points layout
    public static final int IPAC_REX_1 = 1;
    public static final int IPAC_INPUT_1 = 2;
    public static final int IPAC_RELAY_1 = 4;
    public static final int IPAC_AUX_RELAY_1 = 5;
    public static final int IPAC_READER_1 = 10;
    public static final int IPAC_REX_2 = 14;
    public static final int IPAC_INPUT_2 = 15;
    public static final int IPAC_RELAY_2 = 17;
    public static final int IPAC_AUX_RELAY_2 = 18;
    public static final int IPAC_READER_2 = 23;

    //Used by door
    public static final int REXA = 1;

    //Used by door
    public static final int DOOR_SWA = 2;

    //Used by door
    public static final int DOOR_LOCKA = 3;

    //Used by doors with alarm shunts
    public static final int AUX_RELAYA = 5;

    //Used by door
    public static final int READERA = 10;

    //Used by door
    public static final int REXB = 14;

    //Used by door
    public static final int DOOR_SWB = 15;

    //Used by door
    public static final int DOOR_LOCKB = 16;

    //Used by doors with alarm shunts
    public static final int AUX_RELAYB = 18;

    //Used by door
    public static final int READERB = 23;

    public static final int IPDC_DOOR_LOCKA = 4;
    public static final int IPDC_DOOR_LOCKB = 17;

    // IPAC follows IPDC IO Points layout
    public static final int[] IPAC_NODE_ONE_SHUNT = {IPAC_REX_1, IPAC_INPUT_1, IPAC_RELAY_1, IPAC_AUX_RELAY_1, IPAC_READER_1};
    public static final int[] IPAC_NODE_TWO_SHUNT = {IPAC_REX_2, IPAC_INPUT_2, IPAC_RELAY_2, IPAC_AUX_RELAY_2, IPAC_READER_2};

    public static final int[] NODE_ONE_SHUNT = {REXA, DOOR_SWA, AUX_RELAYA, DOOR_LOCKA, READERA};
    public static final int[] NODE_TWO_SHUNT = {REXB, DOOR_SWB, AUX_RELAYB, DOOR_LOCKB, READERB};

    public static final int[] IPDC_NODE_ONE_SHUNT = {REXA, DOOR_SWA, AUX_RELAYA, IPDC_DOOR_LOCKA, READERA};
    public static final int[] IPDC_NODE_TWO_SHUNT = {REXB, DOOR_SWB, AUX_RELAYB, IPDC_DOOR_LOCKB, READERB};

    public static final int[] IPAC_NODE_ONE = { IPAC_REX_1, IPAC_INPUT_1, IPAC_RELAY_1, IPAC_READER_1 };
    public static final int[] IPAC_NODE_TWO = { IPAC_REX_2, IPAC_INPUT_2, IPAC_RELAY_2, IPAC_READER_2 };

    public static final int[] NODE_ONE = { REXA, DOOR_SWA, DOOR_LOCKA, READERA };
    public static final int[] NODE_TWO = { REXB, DOOR_SWB, DOOR_LOCKB, READERB };

    public static final int[] IPDC_NODE_ONE = { REXA, DOOR_SWA, IPDC_DOOR_LOCKA, READERA };
    public static final int[] IPDC_NODE_TWO = { REXB, DOOR_SWB, IPDC_DOOR_LOCKB, READERB };


    public static final int[] G4PANEL_NODE_ONE = {REXA, DOOR_SWA, AUX_RELAYA, IPDC_DOOR_LOCKA, READERA};
    public static final int[] G4PANEL_NODE_TWO =  {REXB, DOOR_SWB, AUX_RELAYB, IPDC_DOOR_LOCKB, READERB};

}
