package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.SecurityGroupType;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupTypes;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupTypeRepository;
import com.brivo.panel.server.reference.dto.ui.UiGroupTypeDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


@Service
public class GroupTypeService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupTypeService.class);
    private static final Long ROOT_SECURITY_GROUP_TYPE_ID = 0L;
    private SecurityGroupTypeRepository securityGroupTypeRepository;

    @Autowired
    public GroupTypeService(SecurityGroupTypeRepository securityGroupTypeRepository) {
        this.securityGroupTypeRepository = securityGroupTypeRepository;
    }

    public static Set<UiGroupTypeDTO> getUiGroupTypesDTOS(final Iterable<SecurityGroupType> groups) {
        return StreamSupport.stream(groups.spliterator(), false)
                            .filter(isUserGroup())
                            .map(convertGroupTypeUI())
                            .collect(Collectors.toSet());
    }

    private static Predicate<SecurityGroupType> isUserGroup() {
        return securityGroupType -> securityGroupType.getSecurityGroupTypeId() != SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID();
    }

    private static Function<SecurityGroupType, UiGroupTypeDTO> convertGroupTypeUI() {
        return securityGroupType -> new UiGroupTypeDTO(securityGroupType.getSecurityGroupTypeId(), securityGroupType.getName());
    }

    public Set<UiGroupTypeDTO> getUIUserGroupTypes() {
        final Iterable<SecurityGroupType> groupsList;
        final Set<UiGroupTypeDTO> uiGroupTypeDTOS;

        // Remove the ROOT group type: 0, because it can be only one per account
        groupsList = () -> StreamSupport.stream(securityGroupTypeRepository.findAll().spliterator(), false)
                                        .filter(securityGroupType -> securityGroupType.getSecurityGroupTypeId() != ROOT_SECURITY_GROUP_TYPE_ID)
                                        .iterator();

        uiGroupTypeDTOS = getUiGroupTypesDTOS(groupsList);
        return uiGroupTypeDTOS;
    }
}
