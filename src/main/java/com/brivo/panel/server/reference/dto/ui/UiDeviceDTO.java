package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.time.LocalDateTime;

public class UiDeviceDTO implements Serializable {
    protected long accountId;
    protected Long deviceId;
    protected String name;
    protected long brainId;
    protected String panelSerialNumber;
    protected String panelName;
    protected Long panelOid;
    protected LocalDateTime created;
    protected Long twoFactorScheduleId;
    protected Long twoFactorInterval;
    protected Long cardRequiredScheduleId;
    protected Long siteId;
    protected Long siteObjectId;
    protected String siteName;
    protected String deviceType;
    protected Long deviceTypeId;
    protected Long scheduleId;
    protected Long deviceObjectId;

    public UiDeviceDTO() {
    }

    public UiDeviceDTO(Long accountId, Long siteId, String siteName, final Long siteObjectId, final long brainId, final Long panelOid,
                       final String panelName, final Long deviceId, final String name,
                       final String panelSerialNumber, final LocalDateTime created,
                       final Long twoFactorScheduleId, final Long twoFactorInterval,
                       final Long cardRequiredScheduleId, final Long deviceTypeId, final String deviceType,
                       final Long scheduleId, final Long deviceObjectId) {
        this.accountId = accountId;
        this.brainId = brainId;
        this.deviceId = deviceId;
        this.name = name;
        this.panelOid = panelOid;
        this.panelSerialNumber = panelSerialNumber;
        this.panelName = panelName;
        this.created = created;
        this.twoFactorScheduleId = twoFactorScheduleId;
        this.twoFactorInterval = twoFactorInterval;
        this.cardRequiredScheduleId = cardRequiredScheduleId;
        this.siteId = siteId;
        this.siteObjectId = siteObjectId;
        this.siteName = siteName;
        this.deviceTypeId = deviceTypeId;
        this.deviceType = deviceType;
        this.scheduleId = scheduleId;
        this.deviceObjectId = deviceObjectId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public String getSiteName() {
        return siteName;
    }

    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    public String getPanelName() {
        return panelName;
    }

    public void setPanelName(String panelName) {
        this.panelName = panelName;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(final long deviceId) {
        this.deviceId = deviceId;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Long getPanelOid() {
        return panelOid;
    }

    public void setPanelOid(Long panelOid) {
        this.panelOid = panelOid;
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(final String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(final LocalDateTime created) {
        this.created = created;
    }

    public Long getTwoFactorScheduleId() {
        return twoFactorScheduleId;
    }

    public void setTwoFactorScheduleId(final Long twoFactorScheduleId) {
        this.twoFactorScheduleId = twoFactorScheduleId;
    }

    public Long getTwoFactorInterval() {
        return twoFactorInterval;
    }

    public void setTwoFactorInterval(final Long twoFactorInterval) {
        this.twoFactorInterval = twoFactorInterval;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(final Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getSiteObjectId() {
        return siteObjectId;
    }

    public void setSiteObjectId(Long siteObjectId) {
        this.siteObjectId = siteObjectId;
    }

    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public void setDeviceObjectId(Long deviceObjectId) {
        this.deviceObjectId = deviceObjectId;
    }
}
