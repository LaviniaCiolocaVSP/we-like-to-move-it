package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.auth.PanelPrincipal;
import com.brivo.panel.server.reference.model.ConfigurationStatus;
import com.brivo.panel.server.reference.model.hardware.Panel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

@RestController
public class ConfigStatusController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigStatusController.class);

    // @Autowired
    // private CredentialService credentialService;

    @Value("${credential.page.size}")
    private Integer credentialPageSize;

    @Value("${ble.auth.time.frame}")
    private Integer bleAuthTimeFrame;

    /**
     * Returns the Configuration status for this panel.
     */
    @RequestMapping(
            path = "/paneldata/configstatus",
            method = RequestMethod.GET
    )
    public ConfigurationStatus getConfigurationStatus(@AuthenticationPrincipal PanelPrincipal panelPrincipal,
                                                      HttpServletRequest request) {
        Panel panel = panelPrincipal.getPanel();
        // Panel panel = PanelBuilder.getMockPanel();

        LOGGER.info("Panel {} requesting updated config status", panel.getElectronicSerialNumber());

        DateTimeFormatter UTCFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")
                                                       .withZone(ZoneOffset.UTC);

        // ConfigurationStatus status = ConfigurationStatusBuilder.getMockConfigurationStatus();
        ConfigurationStatus status = new ConfigurationStatus();
        status.setCredential(UTCFormat.format(panel.getPersonsChanged()));
        status.setSchedule(UTCFormat.format(panel.getSchedulesChanged()));
        status.setHardware(UTCFormat.format(panel.getPanelChanged()));
        status.setCredentialPageSize(credentialPageSize);
        status.setRegistered(panel.getIsRegistered());
        status.setBleAuthTimeFrame(bleAuthTimeFrame);

        LOGGER.info("Sending panel {} updated configuration status data [{}]",
                panel.getElectronicSerialNumber(),
                status);

        return status;
    }

}
