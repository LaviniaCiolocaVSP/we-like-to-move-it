package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DeviceHolidayPK implements Serializable {
    private long deviceId;
    private long holidayId;

    @Column(name = "device_id", nullable = false)
    @Id
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "holiday_id", nullable = false)
    @Id
    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceHolidayPK that = (DeviceHolidayPK) o;

        if (deviceId != that.deviceId) {
            return false;
        }
        return holidayId == that.holidayId;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceId ^ (deviceId >>> 32));
        result = 31 * result + (int) (holidayId ^ (holidayId >>> 32));
        return result;
    }
}
