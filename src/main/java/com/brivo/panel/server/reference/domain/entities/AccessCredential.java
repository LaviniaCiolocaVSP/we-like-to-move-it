
package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "access_credential", schema = "brivo20", catalog = "onair")
public class AccessCredential {
    private long accessCredentialId;
    private Long accountId;
    private String credential;
    private String comments;
    private short disabled;
    private Long lastOwnerObjectId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private LocalDateTime enableOn;
    private LocalDateTime expires;
    private Long totalUses;
    private Long remainingUses;
    private String referenceId;
    private Long numBits;
    private String facilityCode;
    private Long cardNumber;
    private String agencyCode;
    private AccessCredentialType accessCredentialTypeByAccessCredentialTypeId;
    private BrivoObject objectByOwnerObjectId;
    private Account accountByAccountId;
    private Collection<CredentialFieldValue> credentialFieldValuesByAccessCredentialId;

    @Id
    @Column(name = "access_credential_id", nullable = false)
    public long getAccessCredentialId() {
        return accessCredentialId;
    }

    public void setAccessCredentialId(long accessCredentialId) {
        this.accessCredentialId = accessCredentialId;
    }

    @Basic
    @Column(name = "account_id", nullable = true, insertable = false, updatable = false)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "credential", nullable = false, length = 128)
    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    @Basic
    @Column(name = "last_owner_object_id", nullable = true)
    public Long getLastOwnerObjectId() {
        return lastOwnerObjectId;
    }

    public void setLastOwnerObjectId(Long lastOwnerObjectId) {
        this.lastOwnerObjectId = lastOwnerObjectId;
    }

    @Basic
    @Column(name = "comments", nullable = true, length = 256)
    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Basic
    @Column(name = "disabled", nullable = false)
    public short getDisabled() {
        return disabled;
    }

    public void setDisabled(short disabled) {
        this.disabled = disabled;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "enable_on", nullable = true)
    public LocalDateTime getEnableOn() {
        return enableOn;
    }

    public void setEnableOn(LocalDateTime enableOn) {
        this.enableOn = enableOn;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public LocalDateTime getExpires() {
        return expires;
    }

    public void setExpires(LocalDateTime expires) {
        this.expires = expires;
    }

    @Basic
    @Column(name = "total_uses", nullable = true)
    public Long getTotalUses() {
        return totalUses;
    }

    public void setTotalUses(Long totalUses) {
        this.totalUses = totalUses;
    }

    @Basic
    @Column(name = "remaining_uses", nullable = true)
    public Long getRemainingUses() {
        return remainingUses;
    }

    public void setRemainingUses(Long remainingUses) {
        this.remainingUses = remainingUses;
    }

    @Basic
    @Column(name = "reference_id", nullable = true, length = 128)
    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    @Basic
    @Column(name = "num_bits", nullable = true)
    public Long getNumBits() {
        return numBits;
    }

    public void setNumBits(Long numBits) {
        this.numBits = numBits;
    }

    @Basic
    @Column(name = "facility_code", nullable = true, length = 50)
    public String getFacilityCode() {
        return facilityCode;
    }

    public void setFacilityCode(String facilityCode) {
        this.facilityCode = facilityCode;
    }

    @Basic
    @Column(name = "card_number", nullable = true)
    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    @Basic
    @Column(name = "agency_code", nullable = true, length = 10)
    public String getAgencyCode() {
        return agencyCode;
    }

    public void setAgencyCode(String agencyCode) {
        this.agencyCode = agencyCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccessCredential that = (AccessCredential) o;
        return accessCredentialId == that.accessCredentialId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessCredentialId);
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "access_credential_type_id", referencedColumnName = "access_credential_type_id", nullable = false)
    public AccessCredentialType getAccessCredentialTypeByAccessCredentialTypeId() {
        return accessCredentialTypeByAccessCredentialTypeId;
    }

    public void setAccessCredentialTypeByAccessCredentialTypeId(AccessCredentialType accessCredentialTypeByAccessCredentialTypeId) {
        this.accessCredentialTypeByAccessCredentialTypeId = accessCredentialTypeByAccessCredentialTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByOwnerObjectId() {
        return objectByOwnerObjectId;
    }

    public void setObjectByOwnerObjectId(BrivoObject objectByOwnerObjectId) {
        this.objectByOwnerObjectId = objectByOwnerObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "accessCredentialByAccessCredentialId", cascade = CascadeType.ALL)
    public Collection<CredentialFieldValue> getCredentialFieldValuesByAccessCredentialId() {
        return credentialFieldValuesByAccessCredentialId;
    }

    public void setCredentialFieldValuesByAccessCredentialId(Collection<CredentialFieldValue> credentialFieldValuesByAccessCredentialId) {
        this.credentialFieldValuesByAccessCredentialId = credentialFieldValuesByAccessCredentialId;
    }
}
