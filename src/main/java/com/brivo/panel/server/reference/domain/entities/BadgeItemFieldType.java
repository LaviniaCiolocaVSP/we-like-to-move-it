package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_item_field_type", schema = "brivo20", catalog = "onair")
public class BadgeItemFieldType {
    private long badgeItemFieldTypeId;
    private String name;
    private Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeItemFieldTypeId;
    private Collection<BadgeTextItem> badgeTextItemsByBadgeItemFieldTypeId;

    @Id
    @Column(name = "badge_item_field_type_id", nullable = false)
    public long getBadgeItemFieldTypeId() {
        return badgeItemFieldTypeId;
    }

    public void setBadgeItemFieldTypeId(long badgeItemFieldTypeId) {
        this.badgeItemFieldTypeId = badgeItemFieldTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeItemFieldType that = (BadgeItemFieldType) o;

        if (badgeItemFieldTypeId != that.badgeItemFieldTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeItemFieldTypeId ^ (badgeItemFieldTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeItemFieldTypeByBadgeItemFieldTypeId")
    public Collection<BadgeBarcodeItem> getBadgeBarcodeItemsByBadgeItemFieldTypeId() {
        return badgeBarcodeItemsByBadgeItemFieldTypeId;
    }

    public void setBadgeBarcodeItemsByBadgeItemFieldTypeId(Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeItemFieldTypeId) {
        this.badgeBarcodeItemsByBadgeItemFieldTypeId = badgeBarcodeItemsByBadgeItemFieldTypeId;
    }

    @OneToMany(mappedBy = "badgeItemFieldTypeByBadgeItemFieldTypeId")
    public Collection<BadgeTextItem> getBadgeTextItemsByBadgeItemFieldTypeId() {
        return badgeTextItemsByBadgeItemFieldTypeId;
    }

    public void setBadgeTextItemsByBadgeItemFieldTypeId(Collection<BadgeTextItem> badgeTextItemsByBadgeItemFieldTypeId) {
        this.badgeTextItemsByBadgeItemFieldTypeId = badgeTextItemsByBadgeItemFieldTypeId;
    }
}
