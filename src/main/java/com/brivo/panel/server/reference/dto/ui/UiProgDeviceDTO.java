package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Collection;

public class UiProgDeviceDTO extends UiDeviceDTO implements Serializable {
    protected Long doorId;
    protected Short eventTypeId;
    protected Collection<UiPhysicalPointDTO> availableOutputs;
    protected Long disengageDelay;
    protected Boolean reportEngage;
    protected Boolean reportDisengage;
    protected String engageMessage;
    protected String disengageMessage;
    protected Long behavior;

    protected Collection<UiPhysicalPointDTO> outputs;

    public UiProgDeviceDTO() {
    }

    public UiProgDeviceDTO(final UiDeviceDTO uiDeviceDTO) {
        super(uiDeviceDTO.getAccountId(), uiDeviceDTO.getSiteId(), uiDeviceDTO.getSiteName(), uiDeviceDTO.getSiteObjectId(),
                uiDeviceDTO.getBrainId(), uiDeviceDTO.getPanelOid(), uiDeviceDTO.getPanelName(), uiDeviceDTO.getDeviceId(),
                uiDeviceDTO.getName(), uiDeviceDTO.getPanelSerialNumber(), uiDeviceDTO.getCreated(), uiDeviceDTO.getTwoFactorScheduleId(),
                uiDeviceDTO.getTwoFactorInterval(), uiDeviceDTO.getCardRequiredScheduleId(), uiDeviceDTO.getDeviceTypeId(),
                uiDeviceDTO.getDeviceType(), uiDeviceDTO.getScheduleId(), uiDeviceDTO.getDeviceObjectId());
    }

    public UiProgDeviceDTO(final UiDeviceDTO uiDeviceDTO, Long doorId, Short eventTypeId, Collection<UiPhysicalPointDTO> availableOutputs,
                           Long disengageDelay, Boolean reportEngage, Boolean reportDisengage, String engageMessage,
                           String disengageMessage, Long behavior, Collection<UiPhysicalPointDTO> outputs) {
        super(uiDeviceDTO.getAccountId(), uiDeviceDTO.getSiteId(), uiDeviceDTO.getSiteName(), uiDeviceDTO.getSiteObjectId(),
                uiDeviceDTO.getBrainId(), uiDeviceDTO.getPanelOid(), uiDeviceDTO.getPanelName(), uiDeviceDTO.getDeviceId(),
                uiDeviceDTO.getName(), uiDeviceDTO.getPanelSerialNumber(), uiDeviceDTO.getCreated(), uiDeviceDTO.getTwoFactorScheduleId(),
                uiDeviceDTO.getTwoFactorInterval(), uiDeviceDTO.getCardRequiredScheduleId(), uiDeviceDTO.getDeviceTypeId(),
                uiDeviceDTO.getDeviceType(), uiDeviceDTO.getScheduleId(), uiDeviceDTO.getDeviceObjectId());
        this.doorId = doorId;
        this.eventTypeId = eventTypeId;
        this.availableOutputs = availableOutputs;
        this.disengageDelay = disengageDelay;
        this.reportEngage = reportEngage;
        this.reportDisengage = reportDisengage;
        this.engageMessage = engageMessage;
        this.disengageMessage = disengageMessage;
        this.behavior = behavior;
        this.outputs = outputs;
    }

    public UiProgDeviceDTO(final Long disengageDelay, final Boolean reportEngage, final Boolean reportDisengage,
                           final String engageMessage, final String disengageMessage, final Long doorId, final Short eventTypeId) {
        this.disengageDelay = disengageDelay;
        this.reportDisengage = reportDisengage;
        this.reportEngage = reportEngage;
        this.engageMessage = engageMessage;
        this.disengageMessage = disengageMessage;
        this.doorId = doorId;
        this.eventTypeId = eventTypeId;
    }

    public Collection<UiPhysicalPointDTO> getOutputs() {
        return outputs;
    }

    public void setOutputs(Collection<UiPhysicalPointDTO> outputs) {
        this.outputs = outputs;
    }

    public Collection<UiPhysicalPointDTO> getAvailableOutputs() {
        return availableOutputs;
    }

    public void setAvailableOutputs(Collection<UiPhysicalPointDTO> availableOutputs) {
        this.availableOutputs = availableOutputs;
    }

    public Long getDisengageDelay() {
        return disengageDelay;
    }

    public void setDisengageDelay(Long disengageDelay) {
        this.disengageDelay = disengageDelay;
    }

    public Boolean getReportEngage() {
        return reportEngage;
    }

    public void setReportEngage(Boolean reportEngage) {
        this.reportEngage = reportEngage;
    }

    public Boolean getReportDisengage() {
        return reportDisengage;
    }

    public void setReportDisengage(Boolean reportDisengage) {
        this.reportDisengage = reportDisengage;
    }

    public String getEngageMessage() {
        return engageMessage;
    }

    public void setEngageMessage(String engageMessage) {
        this.engageMessage = engageMessage;
    }

    public String getDisengageMessage() {
        return disengageMessage;
    }

    public void setDisengageMessage(String disengageMessage) {
        this.disengageMessage = disengageMessage;
    }

    public Long getBehavior() {
        return behavior;
    }

    public void setBehavior(Long behavior) {
        this.behavior = behavior;
    }

    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }

    public Short getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(Short eventTypeId) {
        this.eventTypeId = eventTypeId;
    }
}
