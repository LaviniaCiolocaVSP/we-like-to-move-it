package com.brivo.panel.server.reference.model.hardware;

public class DoorIoPoint {
    private String name;
    private int ioPointAddress;

    public int getIoPointAddress() {
        return ioPointAddress;
    }

    public void setIoPointAddress(int ioPointAddress) {
        this.ioPointAddress = ioPointAddress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
