package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "axis_dispatcher_credential", schema = "brivo20", catalog = "onair")
public class AxisDispatcherCredential {
    private long axisDispatcherCredentialId;
    private String providerName;
    private String password;
    private String brandspec;
    private Long avhsServerId;
    private Timestamp created;
    private Timestamp associated;
    private AvhsServer avhsServerByAvhsServerId;

    @Id
    @Column(name = "axis_dispatcher_credential_id", nullable = false)
    public long getAxisDispatcherCredentialId() {
        return axisDispatcherCredentialId;
    }

    public void setAxisDispatcherCredentialId(long axisDispatcherCredentialId) {
        this.axisDispatcherCredentialId = axisDispatcherCredentialId;
    }

    @Basic
    @Column(name = "provider_name", nullable = false, length = 40)
    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 128)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "brandspec", nullable = true, length = 12)
    public String getBrandspec() {
        return brandspec;
    }

    public void setBrandspec(String brandspec) {
        this.brandspec = brandspec;
    }

    @Basic
    @Column(name = "avhs_server_id", nullable = true)
    public Long getAvhsServerId() {
        return avhsServerId;
    }

    public void setAvhsServerId(Long avhsServerId) {
        this.avhsServerId = avhsServerId;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "associated", nullable = true)
    public Timestamp getAssociated() {
        return associated;
    }

    public void setAssociated(Timestamp associated) {
        this.associated = associated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AxisDispatcherCredential that = (AxisDispatcherCredential) o;

        if (axisDispatcherCredentialId != that.axisDispatcherCredentialId) {
            return false;
        }
        if (providerName != null ? !providerName.equals(that.providerName) : that.providerName != null) {
            return false;
        }
        if (password != null ? !password.equals(that.password) : that.password != null) {
            return false;
        }
        if (brandspec != null ? !brandspec.equals(that.brandspec) : that.brandspec != null) {
            return false;
        }
        if (avhsServerId != null ? !avhsServerId.equals(that.avhsServerId) : that.avhsServerId != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return associated != null ? associated.equals(that.associated) : that.associated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (axisDispatcherCredentialId ^ (axisDispatcherCredentialId >>> 32));
        result = 31 * result + (providerName != null ? providerName.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (brandspec != null ? brandspec.hashCode() : 0);
        result = 31 * result + (avhsServerId != null ? avhsServerId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (associated != null ? associated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "avhs_server_id", referencedColumnName = "avhs_server_id", insertable = false, updatable = false)
    public AvhsServer getAvhsServerByAvhsServerId() {
        return avhsServerByAvhsServerId;
    }

    public void setAvhsServerByAvhsServerId(AvhsServer avhsServerByAvhsServerId) {
        this.avhsServerByAvhsServerId = avhsServerByAvhsServerId;
    }
}
