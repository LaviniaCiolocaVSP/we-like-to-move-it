package com.brivo.panel.server.reference.service.account;

import com.brivo.panel.server.reference.domain.brivo.*;
import com.brivo.panel.server.reference.domain.common.*;
import com.brivo.panel.server.reference.domain.entities.AccessCredential;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.Holiday;
import com.brivo.panel.server.reference.domain.entities.ObjectProperty;
import com.brivo.panel.server.reference.domain.entities.Schedule;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.entities.Users;
import com.brivo.panel.server.reference.domain.repository.*;
import com.brivo.panel.server.reference.dto.*;
import com.brivo.panel.server.reference.dto.ui.UiAccountDashboardSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import com.brivo.panel.server.reference.service.GroupService;
import com.brivo.panel.server.reference.service.HolidayService;
import com.brivo.panel.server.reference.service.UiCredentialService;
import com.brivo.panel.server.reference.service.UiDeviceService;
import com.brivo.panel.server.reference.service.UiObjectPropertyService;
import com.brivo.panel.server.reference.service.UiPanelService;
import com.brivo.panel.server.reference.service.UiScheduleService;
import com.brivo.panel.server.reference.service.UiSiteService;
import com.brivo.panel.server.reference.service.UserService;
import com.brivo.panel.server.reference.service.util.DownloadUtils;
import org.h2.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.StopWatch;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.File;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Component
public class ReadAccount extends AbstractAccountCRUDComponent {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractAccountCRUDComponent.class);

    private final AccountRepository accountRepository;
    private final BrivoAccountRepository brivoAccountRepository;
    private final DoorDataRepository doorDataRepository;
    private final SecurityGroupAntipassbackRepository securityGroupAntipassbackRepository;
    private final BrivoDoorDataRepository brivoDoorDataRepository;
    private final BrivoDoorDataAntipassbackRepository brivoDoorDataAntipassbackRepository;
    private final BrivoObjectPropertyRepository brivoObjectPropertyRepository;
    private final BrivoDevicePropertyRepository brivoDevicePropertyRepository;
    private final BrivoBrainStateRepository brivoBrainStateRepository;
    private final BrivoSecurityGroupAntipassbackRepository brivoSecurityGroupAntipassbackRepository;
    private final BrivoObjectPermissionRepository brivoObjectPermissionRepository;
    private final BrivoSecurityGroupRepository brivoSecurityGroupRepository;
    private final BrivoProgDeviceDataRepository brivoProgDeviceDataRepository;
    private final BrivoProgDevicePutsRepository brivoProgDevicePutsRepository;
    private final BrivoAccessCredentialTypeRepository brivoAccessCredentialTypeRepository;
    private final PanelRepository brainRepository;
    private final UserRepository userRepository;
    private final HolidayRepository holidayRepository;
    private final DeviceRepository deviceRepository;
    private final ScheduleRepository scheduleRepository;
    private final GroupRepository groupRepository;
    private final CredentialRepository credentialRepository;
    private final ObjectPropertyRepository objectPropertyRepository;
    private final ProgDevicePutsRepository progDevicePutsRepository;
    private final ProgDeviceDataRepository progDeviceDataRepository;
    private final BrivoCredentialFieldRepository brivoCredentialFieldRepository;
    private final CredentialFieldRepository credentialFieldRepository;
    private final AccessCredentialTypeRepository accessCredentialTypeRepository;

    private final UiCredentialService credentialService;
    private final UiScheduleService scheduleService;
    private final UiDeviceService deviceService;
    private final UserService userService;
    private final HolidayService holidayService;
    private final UiPanelService panelService;
    private final UiObjectPropertyService objectPropertyService;
    private final GroupService groupService;
    private final UiSiteService siteService;

    @Value("${download-folder.root}")
    private String rootFolder;

    @Value("${download-folder.accounts}")
    private String accountsFolder;

    @Autowired
    public ReadAccount(final AccountRepository accountRepository, final BrivoAccountRepository brivoAccountRepository,
                       final DoorDataRepository doorDataRepository,
                       final SecurityGroupAntipassbackRepository securityGroupAntipassbackRepository,
                       final BrivoDoorDataAntipassbackRepository brivoDoorDataAntipassbackRepository,
                       final BrivoDoorDataRepository brivoDoorDataRepository,
                       final BrivoObjectPropertyRepository brivoObjectPropertyRepository,
                       final BrivoDevicePropertyRepository brivoDevicePropertyRepository,
                       final BrivoProgDevicePutsRepository brivoProgDevicePutsRepository,
                       final BrivoProgDeviceDataRepository brivoProgDeviceDataRepository,
                       final ProgDeviceDataRepository progDeviceDataRepository,
                       final ProgDevicePutsRepository progDevicePutsRepository,
                       final BrivoBrainStateRepository brivoBrainStateRepository, final PanelRepository brainRepository,
                       final UserRepository userRepository, final HolidayRepository holidayRepository,
                       final DeviceRepository deviceRepository, final ScheduleRepository scheduleRepository,
                       final GroupRepository groupRepository, final CredentialRepository credentialRepository,
                       final BrivoSecurityGroupAntipassbackRepository brivoSecurityGroupAntipassbackRepository,
                       final BrivoObjectPermissionRepository brivoObjectPermissionRepository,
                       final BrivoSecurityGroupRepository brivoSecurityGroupRepository,
                       final BrivoCredentialFieldRepository brivoCredentialFieldRepository,
                       final CredentialFieldRepository credentialFieldRepository,
                       final ObjectPropertyRepository objectPropertyRepository, final UiScheduleService scheduleService,
                       final UiCredentialService credentialService, final UiDeviceService deviceService,
                       final UserService userService, final HolidayService holidayService, final UiPanelService panelService,
                       final UiObjectPropertyService objectPropertyService, final GroupService groupService,
                       final UiSiteService siteService, final BrivoAccessCredentialTypeRepository brivoAccessCredentialTypeRepository,
                       final AccessCredentialTypeRepository accessCredentialTypeRepository) {
        this.accountRepository = accountRepository;
        this.brivoAccountRepository = brivoAccountRepository;
        this.doorDataRepository = doorDataRepository;
        this.securityGroupAntipassbackRepository = securityGroupAntipassbackRepository;
        this.brivoDoorDataAntipassbackRepository = brivoDoorDataAntipassbackRepository;
        this.brivoDoorDataRepository = brivoDoorDataRepository;
        this.brivoObjectPropertyRepository = brivoObjectPropertyRepository;
        this.brivoDevicePropertyRepository = brivoDevicePropertyRepository;
        this.brivoBrainStateRepository = brivoBrainStateRepository;
        this.brivoProgDeviceDataRepository = brivoProgDeviceDataRepository;
        this.brivoProgDevicePutsRepository = brivoProgDevicePutsRepository;
        this.brainRepository = brainRepository;
        this.userRepository = userRepository;
        this.holidayRepository = holidayRepository;
        this.deviceRepository = deviceRepository;
        this.scheduleRepository = scheduleRepository;
        this.groupRepository = groupRepository;
        this.credentialRepository = credentialRepository;
        this.brivoSecurityGroupAntipassbackRepository = brivoSecurityGroupAntipassbackRepository;
        this.brivoObjectPermissionRepository = brivoObjectPermissionRepository;
        this.brivoSecurityGroupRepository = brivoSecurityGroupRepository;
        this.objectPropertyRepository = objectPropertyRepository;
        this.deviceService = deviceService;
        this.scheduleService = scheduleService;
        this.credentialService = credentialService;
        this.userService = userService;
        this.holidayService = holidayService;
        this.panelService = panelService;
        this.objectPropertyService = objectPropertyService;
        this.groupService = groupService;
        this.siteService = siteService;
        this.progDeviceDataRepository = progDeviceDataRepository;
        this.progDevicePutsRepository = progDevicePutsRepository;
        this.credentialFieldRepository = credentialFieldRepository;
        this.brivoCredentialFieldRepository = brivoCredentialFieldRepository;
        this.brivoAccessCredentialTypeRepository = brivoAccessCredentialTypeRepository;
        this.accessCredentialTypeRepository = accessCredentialTypeRepository;
    }

    public ResponseEntity<StreamingResponseBody> download(final long accountId, final String repositoryIdentifier) {
        LOGGER.debug("Downloading account from repository {}", repositoryIdentifier);

        final FileUrlResource accountResource = getAccountResource(accountId, repositoryIdentifier);
        LOGGER.info("Successfully retrieved accountResource for the account '{}'", accountId);

        return ResponseEntity.ok()
                .headers(DownloadUtils.buildHttpHeaders(accountResource))
                .body(outputStream -> IOUtils.copy(accountResource.getInputStream(), outputStream));
    }

    private FileUrlResource getAccountResource(final long accountId, final String repositoryIdentifier) {
        final String filePath = getAccountFilePath(accountId);
        createAccountJsonFile(accountId, repositoryIdentifier);

        try {
            return new FileUrlResource(filePath);
        } catch (MalformedURLException e) {
            LOGGER.error("An error occurred while building FileUrlResource from file... {}", e.getMessage());
            throw new IllegalArgumentException("Cannot download the file '" + filePath + "'");
        }
    }

    private void createAccountJsonFile(final long accountId, final String repositoryIdentifier) {
        final AccountDTO accountDTO = buildAccountDTOFromRepository(accountId, repositoryIdentifier);
        LOGGER.info("Successfully built AccountDTO for account_id {}", accountId);

        final String filePath = getAccountFilePath(accountId);

        try {
            objectMapper.writeValue(new File(filePath), accountDTO);
        } catch (IOException e) {
            LOGGER.error("An error occurred while writing AccountDTO to JSON file...{}", e.getMessage());
        }
    }

    private AccountDTO buildAccountDTOFromRepository(final long accountId, final String repositoryIdentifier) {
        if (repositoryIdentifier.equals(AccountService.BRIVO_REPOSITORY_IDENTIFIER)) {
            return getBrivoAccount(accountId);
        } else {
            return getCurrentAccount(accountId);
        }
    }

    private String getAccountFilePath(final long accountId) {
        return rootFolder + PATH_SEPARATOR + accountsFolder + PATH_SEPARATOR + accountId + ".json";
    }

    public AccountDTO getBrivoAccount(final long accountId) {
        final Account account = getAccountOrThrow(accountId, brivoAccountRepository, " Brivo");

        return getAccount(account, brivoDoorDataRepository, brivoDoorDataAntipassbackRepository,
                brivoObjectPropertyRepository, brivoDevicePropertyRepository, brivoBrainStateRepository,
                brivoSecurityGroupAntipassbackRepository, brivoObjectPermissionRepository, brivoSecurityGroupRepository,
                brivoProgDeviceDataRepository, brivoProgDevicePutsRepository, brivoCredentialFieldRepository, brivoAccessCredentialTypeRepository);
    }

    public AccountDTO getCurrentAccount(final long accountId) {
        final Account account = getAccountOrThrow(accountId, accountRepository, "RPS");

        return getAccount(account, doorDataRepository, doorDataAntipassbackRepository, objectPropertyRepository,
                devicePropertyRepository, brainStateRepository, securityGroupAntipassbackRepository,
                objectPermissionRepository, securityGroupRepository, progDeviceDataRepository, progDevicePutsRepository,
                credentialFieldRepository, accessCredentialTypeRepository);
    }

    private Account getAccountOrThrow(long accountId, final CrudRepository<Account, Long> crudRepository,
                                      final String source) {
        return crudRepository.findById(accountId)
                .orElseThrow(() ->
                        new IllegalArgumentException("There is no account with the ID '" + accountId + "' in " + source));
    }

    private AccountDTO getAccount(final Account account, final IDoorDataRepository dynamicDoorDataRepository,
                                  final IDoorDataAntipassbackRepository dynamicDoorDataAntipassbackRepository,
                                  final IObjectPropertyRepository dynamicObjectPropertyRepository,
                                  final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                  final IBrainStateRepository dynamicBrainStateRepository,
                                  final ISecurityGroupAntipassbackRepository dynamicSecurityGroupAntipassbackRepository,
                                  final IObjectPermissionRepository dynamicObjectPermissionRepository,
                                  final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                  final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                  final IProgDevicePutsRepository dynamicProgDevicePutsRepository,
                                  final ICredentialFieldRepository credentialFieldRepository,
                                  final IAccessCredentialTypeRepository accessCredentialTypeRepository) {
        final StopWatch stopWatch = new StopWatch("create AccountDTO");
        stopWatch.start("get sites");
        final Collection<SecurityGroup> sites = getSites(account);
        stopWatch.stop();

        stopWatch.start("get brains");
        final Collection<Brain> panels = getBrains(account);
        stopWatch.stop();

        stopWatch.start("get devices");
        final Collection<Device> devices = getDevices(account);
        stopWatch.stop();

        stopWatch.start("get doors");
        final Collection<Device> doors = getDoors(account);
        stopWatch.stop();

        stopWatch.start("get schedules");
        final Collection<Schedule> schedules = account.getSchedulesByAccountId();
        stopWatch.stop();

        stopWatch.start("get holidays");
        final Collection<Holiday> holidays = account.getHolidaysByAccountId();
        stopWatch.stop();

        stopWatch.start("get groups");
        final Collection<SecurityGroup> groups = getGroups(account);
        stopWatch.stop();

        stopWatch.start("get users");
        final Collection<Users> users = account.getUsersByAccountId();
        stopWatch.stop();

        stopWatch.start("get credentials");
        final Collection<AccessCredential> credentials = account.getAccessCredentialsByAccountId();
        stopWatch.stop();

        stopWatch.start("get object properties");
        final Collection<ObjectProperty> objectProperties = objectPropertyService.getObjectProperties(account.getAccountId(),
                dynamicObjectPropertyRepository);
        stopWatch.stop();

        stopWatch.start("get siteDTOS");
        final Collection<SiteDTO> siteDTOS = siteService.getSiteDTOS(sites);
        stopWatch.stop();

        stopWatch.start("get panelDTOS");
        final Collection<PanelDTO> panelDTOS = panelService.getPanelDTOS(panels, dynamicBrainStateRepository);
        stopWatch.stop();

        stopWatch.start("get deviceDTOS");
        final Collection<DeviceDTO> deviceDTOS = deviceService.getDeviceDTOS(devices, dynamicDevicePropertyRepository,
                dynamicSecurityGroupRepository, dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository);
        stopWatch.stop();

        stopWatch.start("get doorDTOS");
        final Collection<DoorDTO> doorDTOS = deviceService.getDoorDTOS(doors, dynamicDoorDataRepository,
                dynamicDoorDataAntipassbackRepository, dynamicDevicePropertyRepository, dynamicSecurityGroupRepository,
                dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository);
        stopWatch.stop();

        stopWatch.start("get userDTOS");
        final Collection<UserDTO> userDTOS = userService.getUserDTOS(users, dynamicSecurityGroupRepository);
        stopWatch.stop();

        stopWatch.start("get groupDTOS");
        final Collection<GroupDTO> groupDTOS = groupService.getGroupDTOS(groups, dynamicSecurityGroupAntipassbackRepository,
                dynamicObjectPermissionRepository);
        stopWatch.stop();

        stopWatch.start("get credentialDTOS");
        final Collection<CredentialDTO> credentialDTOS = credentialService.getCredentialDTOS(credentials);
        stopWatch.stop();

        stopWatch.start("get credentialFieldDTOS");
        final Collection<CredentialFieldDTO> credentialFieldDTOS = credentialService.getAllCredentialFieldDTOS(credentialFieldRepository);
        stopWatch.stop();

        stopWatch.start("get holidayDTOS");
        final Collection<HolidayDTO> holidayDTOS = holidayService.getHolidayDTOS(holidays);
        stopWatch.stop();

        stopWatch.start("get scheduleDTOS");
        final Collection<ScheduleDTO> scheduleDTOS = scheduleService.getScheduleDTOS(schedules);
        stopWatch.stop();

        stopWatch.start("get objectPropertyDTOS");
        final Collection<ObjectPropertyDTO> objectPropertyDTOS = objectPropertyService.getObjectPropertyDTOS(objectProperties);
        stopWatch.stop();

        stopWatch.start("get access credential type DTOS");
        final Collection<AccessCredentialTypeDTO> accessCredentialTypeDTOS = StreamSupport.stream(accessCredentialTypeRepository.findAll().spliterator(), false)
                                                                                          .map(credentialService.convertAccessCredentialTypeToDTO())
                                                                                          .collect(Collectors.toList());

        stopWatch.stop();

        LOGGER.debug("{}", stopWatch.prettyPrint());

        return new AccountDTO(account.getAccountId(), account.getAccountTypeByAccountTypeId().getAccountTypeId(),
                LocalDateTime.now(), account.getObjectByObjectId().getObjectId(),
                account.getName(), siteDTOS, panelDTOS, deviceDTOS,
                doorDTOS, userDTOS, groupDTOS, credentialDTOS, holidayDTOS, scheduleDTOS, credentialFieldDTOS,
                objectPropertyDTOS, accessCredentialTypeDTOS);
    }

    private Collection<SecurityGroup> getSites(final Account account) {
        return account.getSecurityGroupsByAccountId()
                .stream()
                .filter(group -> group.getSecurityGroupTypeBySecurityGroupTypeId()
                        .getName()
                        .equals("Device Group"))
                .collect(Collectors.toSet());
    }

    private Collection<Brain> getBrains(final Account account) {
        return new HashSet<>(account.getBrainsByAccountId());
    }

    private Collection<SecurityGroup> getGroups(final Account account) {
        return account.getSecurityGroupsByAccountId()
                .stream()
                .filter(groupService.isUserGroup())
                .collect(Collectors.toSet());
    }

    private Collection<Device> getDevices(final Account account) {
        return account.getDevicesByAccountId()
                .stream()
                .filter(deviceService.isDeviceDoor().negate())
                .collect(Collectors.toSet());
    }

    private Collection<Device> getDoors(final Account account) {
        return account.getDevicesByAccountId()
                .stream()
                .filter(deviceService.isDeviceDoor())
                .collect(Collectors.toSet());
    }

    Collection<UiAccountSummaryDTO> getAccountNamesAndIds(String nameFilter, int pageNumber, int pageSize) {
        LOGGER.debug("Getting account name and ids...");
        Pageable pageable = PageRequest.of(pageNumber, pageSize);
        LinkedList<UiAccountSummaryDTO> resultList;
        resultList = new LinkedList<>(brivoAccountRepository.getAccountNamesAndIds(nameFilter, pageable));
        return resultList;
    }

    public UiAccountSummaryDTO getCurrentAccount() {
        return accountRepository.getCurrentAccount(PageRequest.of(0, 1))
                .get()
                .findFirst()
                .orElse(null);
    }

    UiAccountDashboardSummaryDTO getCurrentAccountDashboardSummary() {
        return StreamSupport.stream(accountRepository.findAll().spliterator(), false)
                .findFirst()
                .map(convertAccountToAccountDashboardSummaryUIListing())
                .orElse(null);
    }

    private Function<Account, UiAccountDashboardSummaryDTO> convertAccountToAccountDashboardSummaryUIListing() {
        return this::getUiAccountDashboardSummaryDTO;
    }

    private UiAccountDashboardSummaryDTO getUiAccountDashboardSummaryDTO(final Account account) {
        LOGGER.debug("Getting UiAccountSummaryDTO...");
        final long panelsCount = brainRepository.getBrainCountForAccount(account.getAccountId());
        final long doorsCount = deviceRepository.getDoorCountForAccount(account.getAccountId());
        final long devicesCount = deviceRepository.getDeviceCountForAccount(account.getAccountId());
        final long holidaysCount = holidayRepository.getHolidayCountForAccount(account.getAccountId());
        final long schedulesCount = scheduleRepository.getScheduleCountForAccount(account.getAccountId());
        final long groupsCount = groupRepository.getGroupCountForAccount(account.getAccountId());
        final long usersCount = userRepository.getUserCountForAccount(account.getAccountId());
        final long credentialsCount = credentialRepository.getAccessCredentialCountForAccount(account.getAccountId());

        return new UiAccountDashboardSummaryDTO(account.getAccountId(), account.getName(), panelsCount, doorsCount,
                devicesCount, holidaysCount, schedulesCount, groupsCount, usersCount, credentialsCount);
    }
}
