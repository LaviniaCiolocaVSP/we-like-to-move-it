package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.credential.CredentialDao;
import com.brivo.panel.server.reference.domain.entities.AccessCredential;
import com.brivo.panel.server.reference.domain.entities.AccessCredentialType;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import com.brivo.panel.server.reference.domain.entities.credential.Credential;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialField;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldType;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldValue;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFormat;
import com.brivo.panel.server.reference.domain.repository.AccessCredentialTypeRepository;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.BrivoObjectRepository;
import com.brivo.panel.server.reference.domain.repository.CredentialRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupMemberRepository;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiCardSimulatorDTO;
import com.brivo.panel.server.reference.dto.ui.UiCredentialDTO;
import com.brivo.panel.server.reference.model.event.SecurityAction;
import com.brivo.panel.server.reference.service.credential.utils.BinaryCredentialEncoder;
import com.brivo.panel.server.reference.service.credential.utils.CredentialEncoder;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UiAccessCredentialService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiAccessCredentialService.class);
    private static final String DEFAULT_REF_ID = "20eb23d7-7b5e-463a-b608-2cc4098bb541";

    private final CredentialRepository credentialRepository;
    private final BrivoObjectRepository objectRepository;
    private final AccessCredentialTypeRepository accessCredentialTypeRepository;
    private final AccountRepository accountRepository;
    private final SecurityGroupMemberRepository securityGroupMemberRepository;
    private final ObjectPermissionRepository objectPermissionRepository;
    private final CredentialDao credentialDao;

    @Value("${card-simulator.url}")
    private String simulatorUrl;

    @Autowired
    public UiAccessCredentialService(final CredentialRepository credentialRepository, final BrivoObjectRepository objectRepository,
                                     final AccessCredentialTypeRepository accessCredentialTypeRepository,
                                     final AccountRepository accountRepository, final CredentialDao credentialDao,
                                     final SecurityGroupMemberRepository securityGroupMemberRepository,
                                     final ObjectPermissionRepository objectPermissionRepository) {
        this.credentialRepository = credentialRepository;
        this.objectRepository = objectRepository;
        this.accessCredentialTypeRepository = accessCredentialTypeRepository;
        this.accountRepository = accountRepository;
        this.credentialDao = credentialDao;
        this.securityGroupMemberRepository = securityGroupMemberRepository;
        this.objectPermissionRepository = objectPermissionRepository;
    }

    private CredentialFormat resolveFormat(long credential_format_id) {
        CredentialFormat resolvedFormat = credentialDao.retrieveCredentialFormat(credential_format_id);
        return resolvedFormat;
    }

    private Function<UiCredentialDTO, AccessCredential> convertCredentialUIDTOCreateWithDates() {
        return credentialDTO -> {

            Credential credential_rps = new Credential();


            CredentialFormat format = resolveFormat(credentialDTO.getCredentialTypeId());

            List<CredentialFieldValue> values = createCredentialFieldValuesForEncoding(credentialDTO, format);

            credential_rps.setFieldValues(values);
            credential_rps.setNumBits(format.getNumBits());
            credential_rps.setFormat(format);
            encodeCredential(credential_rps, format);

            return setAccessCredentialDataCreateWithDates(credentialDTO, credential_rps, format);
        };
    }


    private Function<UiCredentialDTO, AccessCredential> convertCredentialUIDTOCreate() {
        return credentialDTO -> {

            Credential credential_rps = new Credential();


            CredentialFormat format = resolveFormat(credentialDTO.getCredentialTypeId());

            List<CredentialFieldValue> values = createCredentialFieldValuesForEncoding(credentialDTO, format);

            credential_rps.setFieldValues(values);
            credential_rps.setNumBits(format.getNumBits());
            credential_rps.setFormat(format);
            encodeCredential(credential_rps, format);

            return setAccessCredentialDataCreate(credentialDTO, credential_rps, format);
        };
    }

    private AccessCredential setAccessCredentialDataCreate(UiCredentialDTO credentialDTO, Credential credential_rps, CredentialFormat format) {
        AccessCredential credential = setAccessCredentialValueCommon(credentialDTO, credential_rps, format);
        final LocalDateTime now = LocalDateTime.now();
        credential.setEnableOn(now);
        return credential;
    }

    private AccessCredential setAccessCredentialDataCreateWithDates(UiCredentialDTO credentialDTO, Credential credential_rps, CredentialFormat format) {
        AccessCredential credential = setAccessCredentialValueCommon(credentialDTO, credential_rps, format);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM-dd-yyyy hh:mm a");
        if (!credentialDTO.getEnableOnString().equals("")) {
            try {

                LocalDateTime enable = LocalDateTime.parse(credentialDTO.getEnableOnString().toUpperCase(), formatter);
                credential.setEnableOn(enable);
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        }
        if (!credentialDTO.getExpiresString().equals("")) {
            try {
                LocalDateTime expires = LocalDateTime.parse(credentialDTO.getExpiresString().toUpperCase(), formatter);
                credential.setExpires(expires);
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }
        }


        return credential;
    }

    private AccessCredential setAccessCredentialValueCommon(UiCredentialDTO credentialDTO, Credential credential_rps, CredentialFormat format) {
        AccessCredential credential = new AccessCredential();
        final LocalDateTime now = LocalDateTime.now();
        final Long credentialId = getNewCredentialId();
        credential.setAccessCredentialId(credentialId);
        credential.setNumBits(Integer.valueOf(format.getNumBits()).longValue());
        credential.setReferenceId(credentialDTO.getReferenceId());
        credential.setAccountId(credentialDTO.getAccountId());
        final AccessCredentialType accessCredentialType = getAccessCredentialTypeOrThrow(credentialDTO.getCredentialTypeId());
        credential.setAccessCredentialTypeByAccessCredentialTypeId(accessCredentialType);
        credential.setCredential(credential_rps.getEncodedCredential());
        credential.setCreated(now);
        credential.setUpdated(now);

        final Account account = getAccountOrThrow(credentialDTO.getAccountId());
        credential.setAccountByAccountId(account);
        credential.setAccountId(credentialDTO.getAccountId());
        final BrivoObject owner = objectRepository.findById(credentialDTO.getOwnerObjectId())
                                                  .orElse(null);
        if (owner != null) {
            credential.setLastOwnerObjectId(owner.getObjectId());
        }
        credential.setObjectByOwnerObjectId(owner);


        Collection<com.brivo.panel.server.reference.domain.entities.CredentialFieldValue> credentialFieldValueCollection = createCredentialFieldValuesCollection(credentialDTO, format, credentialId);

        credential.setCredentialFieldValuesByAccessCredentialId(credentialFieldValueCollection);
        return credential;
    }


    private List<CredentialFieldValue> createCredentialFieldValuesForEncoding(UiCredentialDTO credentialDTO, CredentialFormat format) {
        List<CredentialFieldValue> credentialFieldValues = new ArrayList<>();
        CredentialFieldValue credentialFieldValue = new CredentialFieldValue();
        credentialFieldValue.setValue(credentialDTO.getFacility_code());
        credentialFieldValue.setCredentialField(format.getFields().get(1));
        credentialFieldValues.add(credentialFieldValue);

        CredentialField referenceField = populateFormatFieldsForValue(format, credentialFieldValues, false);


        List<CredentialFieldValue> values = new ArrayList<CredentialFieldValue>(
                credentialFieldValues);

        String referenceValue = credentialDTO.getReferenceId();
        CredentialFieldValue referenceFieldValue = new CredentialFieldValue();
        referenceFieldValue.setValue(referenceValue);
        referenceFieldValue.setCredentialField(referenceField);
        values.add(referenceFieldValue);
        return values;
    }

    private Collection<com.brivo.panel.server.reference.domain.entities.CredentialFieldValue> createCredentialFieldValuesCollection(UiCredentialDTO credentialDTO, CredentialFormat format, Long credentialId) {
        Collection<com.brivo.panel.server.reference.domain.entities.CredentialFieldValue> credentialFieldValueCollection = new ArrayList<>();
        com.brivo.panel.server.reference.domain.entities.CredentialFieldValue credentialFieldValueForAccessCredential;

        credentialFieldValueForAccessCredential = new com.brivo.panel.server.reference.domain.entities.CredentialFieldValue();
        credentialFieldValueForAccessCredential.setAccessCredentialId(credentialId);
        credentialFieldValueForAccessCredential.setCredentialFieldId(format.getFields().get(0).getCredentialFieldId()); // card_no
        credentialFieldValueForAccessCredential.setValue(credentialDTO.getReferenceId());
        credentialFieldValueCollection.add(credentialFieldValueForAccessCredential);

        credentialFieldValueForAccessCredential = new com.brivo.panel.server.reference.domain.entities.CredentialFieldValue();
        credentialFieldValueForAccessCredential.setAccessCredentialId(credentialId);
        credentialFieldValueForAccessCredential.setCredentialFieldId(format.getFields().get(1).getCredentialFieldId()); // facility_no
        credentialFieldValueForAccessCredential.setValue(credentialDTO.getFacility_code());
        credentialFieldValueCollection.add(credentialFieldValueForAccessCredential);
        return credentialFieldValueCollection;
    }

    private Long getNewCredentialId() {
        final Long maxCredentialId = credentialRepository.getMaxAccessCredentialId()
                                                         .orElseThrow(() -> new IllegalArgumentException(
                                                                 "An error occured while getting max(access_credential_id)"));
        return maxCredentialId + 1;
    }


    private CredentialFormat resolveFormat(Long format_type_id) {
        CredentialFormat resolvedFormat = credentialDao.retrieveCredentialFormat(format_type_id);
        return resolvedFormat;
    }

    private CredentialField populateFormatFieldsForValue(CredentialFormat format, List<CredentialFieldValue> values, boolean skipReference) {

        CredentialField referenceField = null;

        Map<Long, CredentialFieldValue> fieldValuesById = new HashMap<Long, CredentialFieldValue>();

        for (CredentialFieldValue value : values) {
            fieldValuesById.put(value.getCredentialField().getCredentialFieldId(), value);
        }

        for (CredentialField field : format.getFields()) {
            // only values need to be specified from input
            if (field.getCredentialFieldType() == CredentialFieldType.VALUE) {
                if (field.isForReference()) {
                    referenceField = field;
                }
                if (!field.isForReference() || skipReference) {
                    CredentialFieldValue fieldValue = fieldValuesById.get(field
                                                                                  .getCredentialFieldId());
                    if (fieldValue != null) {
                        fieldValue.setCredentialField(field);
                        fieldValuesById.remove(field.getCredentialFieldId());
                    }
                }

            } else {
                CredentialFieldValue fieldValue = new CredentialFieldValue();
                fieldValue.setValue(null); // not a value type, no value
                fieldValue.setCredentialField(field);
                values.add(fieldValue);
            }
        }

        return referenceField;
    }

    private void encodeCredential(Credential credential, CredentialFormat credentialFormat) {

        CredentialEncoder encoder = null;
        if (credentialFormat.getEncoding() == CredentialEncoding.BINARY) {
            encoder = new BinaryCredentialEncoder();
        }
        encoder.encode(credential);
    }

    @Transactional
    public MessageDTO create26Bit(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Adding a new credential for account {}", credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTOCreate().apply(credentialDTO);

        credentialRepository.save(credential);
        LOGGER.debug("Successfully added credential to account {}", credentialDTO.getAccountId());

        return new MessageDTO("The credential was successfully added");
    }

    @Transactional
    public MessageDTO create26BitCucumber(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Adding a new credential for account {}", credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTOCreate().apply(credentialDTO);
        credentialRepository.save(credential);
        //  sendFlushMessageToAssociatedPanels(credential);
        LOGGER.debug("Successfully added credential to account {}", credentialDTO.getAccountId());
        return new MessageDTO("The credential was successfully added");
    }

    @Transactional
    public MessageDTO create26BitCucumberWithDates(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Adding a new credential for account {}", credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTOCreateWithDates().apply(credentialDTO);
        credentialRepository.save(credential);
        //  sendFlushMessageToAssociatedPanels(credential);
        LOGGER.debug("Successfully added credential to account {}", credentialDTO.getAccountId());
        return new MessageDTO("The credential was successfully added");
    }


    @Transactional
    public MessageDTO update26Bit(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Adding a new credential for account {}", credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTOUpdate().apply(credentialDTO);
        credentialDao.deleteCredentialById(credentialDTO.getCredentialId());

        credentialRepository.save(credential);
        LOGGER.debug("Successfully updated credential to account {}", credentialDTO.getAccountId());

        return new MessageDTO("The credential was successfully updated");
    }

    private Function<UiCredentialDTO, AccessCredential> convertCredentialUIDTOUpdate() {
        return credentialDTO -> {

            Credential credential_rps = new Credential();


            CredentialFormat format = resolveFormat(credentialDTO.getCredentialTypeId());

            List<CredentialFieldValue> values = createCredentialFieldValuesForEncoding(credentialDTO, format);

            credential_rps.setFieldValues(values);
            credential_rps.setNumBits(format.getNumBits());
            credential_rps.setFormat(format);
            encodeCredential(credential_rps, format);

            return setAccessCredentialDataUpdate(credentialDTO, credential_rps, format);
        };
    }

    private AccessCredential setAccessCredentialDataUpdate(UiCredentialDTO credentialDTO, Credential credential_rps, CredentialFormat format) {
        AccessCredential credential = new AccessCredential();
        final LocalDateTime now = LocalDateTime.now();
        final Long credentialId = getNewCredentialId();

        credential.setAccessCredentialId(credentialId);
        credential.setNumBits(Integer.valueOf(format.getNumBits()).longValue());
        credential.setReferenceId(credentialDTO.getReferenceId());
        credential.setAccountId(credentialDTO.getAccountId());

        final AccessCredentialType accessCredentialType = getAccessCredentialTypeOrThrow(credentialDTO.getCredentialTypeId());
        credential.setAccessCredentialTypeByAccessCredentialTypeId(accessCredentialType);
        credential.setCredential(credential_rps.getEncodedCredential());

        credential.setCreated(now);
        credential.setUpdated(now);
        credential.setEnableOn(credentialDTO.getEnableOn());
        credential.setExpires(credentialDTO.getExpires());

        final Account account = getAccountOrThrow(credentialDTO.getAccountId());
        credential.setAccountByAccountId(account);
        credential.setAccountId(credentialDTO.getAccountId());
        final BrivoObject owner = objectRepository.findById(credentialDTO.getOwnerObjectId())
                                                  .orElse(null);
        if (owner != null) {
            credential.setLastOwnerObjectId(owner.getObjectId());
        }
        credential.setObjectByOwnerObjectId(owner);


        Collection<com.brivo.panel.server.reference.domain.entities.CredentialFieldValue> credentialFieldValueCollection = createCredentialFieldValuesCollection(credentialDTO, format, credentialId);

        credential.setCredentialFieldValuesByAccessCredentialId(credentialFieldValueCollection);

        return credential;
    }

    private AccessCredentialType getAccessCredentialTypeOrThrow(Long accessCredentialTypeId) {
        return accessCredentialTypeRepository.findById(accessCredentialTypeId)
                                             .orElseThrow(() -> new IllegalArgumentException(
                                                     "Access credential type with id " + accessCredentialTypeId + " not found in database"));
    }

    private Account getAccountOrThrow(final Long accountId) {
        return accountRepository.findById(accountId)
                                .orElseThrow(() -> new IllegalArgumentException(
                                        "Account with id " + accountId + " not found in database"));
    }


    public MessageDTO send26BitCredentialToCardSimulator(final UiCardSimulatorDTO uiCardSimulatorDTO) {
        LOGGER.debug("sending a credential to card simulator");

        String encodedValue = calculateEncodedValueOfCredential(uiCardSimulatorDTO);

        String simulatorMessageToSend = uiCardSimulatorDTO.getIterationCount() + ";" + encodedValue;
        return new MessageDTO("Credential has been sent to simulator");
    }

    public MessageDTO send26BitCredentialToArduinoCardSimulator(final UiCardSimulatorDTO uiCardSimulatorDTO) {
        LOGGER.debug("sending a credential to Arduino card simulator");

        String encodedValue = calculateEncodedValueOfCredential(uiCardSimulatorDTO);

        String simulatorMessageToSend = uiCardSimulatorDTO.getReaderNumber() + "$" + encodedValue + ";";
        return new MessageDTO("Credential has been sent to Arduino simulator");
    }

    public String calculateEncodedValueOfCredential(final UiCardSimulatorDTO uiCardSimulatorDTO) {
        UiCredentialDTO credentialDTO = new UiCredentialDTO();
        credentialDTO.setFacility_code(uiCardSimulatorDTO.getFacilityCode());
        credentialDTO.setReferenceId(uiCardSimulatorDTO.getCardNumber());

        CredentialFormat format = resolveFormat(100);
        List<CredentialFieldValue> values = createCredentialFieldValuesForEncoding(credentialDTO, format);
        Credential credential_rps = new Credential();
        credential_rps.setFieldValues(values);
        credential_rps.setNumBits(format.getNumBits());
        credential_rps.setFormat(format);

        BinaryCredentialEncoder encoder = new BinaryCredentialEncoder();
        encoder.encodeToBinaryBase(credential_rps);
        return credential_rps.getEncodedCredential();
    }

    private Collection<Device> getDevicesForCredential(final AccessCredential credential) {
        final long accessCredentialId = credential.getAccessCredentialId();
        final Collection<Device> notAvailablePanelsForCredential = Collections.emptyList();

        LOGGER.debug("Getting brainIds for credential with id {}", accessCredentialId);

        final BrivoObject owner = credential.getObjectByOwnerObjectId();

        if (owner == null || owner.getUsersByObjectId() == null) {
            LOGGER.debug("The credential {} doesn't have an owner, therefore it isn't associated to any panel", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final long userObjectId = owner.getObjectId();
        final Optional<List<SecurityGroupMember>> groupMembers = securityGroupMemberRepository.findByObjectId(userObjectId);
        if (!groupMembers.isPresent()) {
            LOGGER.debug("The owner of the credential with id {} is user with object id {}. which is not associated to " +
                         "any groups, therefore the credential isn't associated to any panel", accessCredentialId, userObjectId);
            return notAvailablePanelsForCredential;
        }

        final Collection<Long> groupObjectIds = groupMembers.get()
                                                            .stream()
                                                            .map(securityGroupMember -> securityGroupMember.getSecurityGroupBySecurityGroupId()
                                                                                                           .getObjectByObjectId()
                                                                                                           .getObjectId())
                                                            .collect(Collectors.toList());
        if (groupObjectIds.isEmpty()) {
            LOGGER.debug("There are no security groups associated to credential with id {}", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final Collection<ObjectPermission> objectPermissions = objectPermissionRepository.findAllByGroupObjectId(groupObjectIds, SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue())
                                                                                         .orElse(Collections.emptyList());

        if (objectPermissions.isEmpty()) {
            LOGGER.debug("There are no object permissions associated to credential with id {}", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final Collection<Device> devices = objectPermissions.stream()
                                                            .map(objectPermission -> objectPermission.getObjectByBrivoObjectId().getDeviceByObjectId())
                                                            .collect(Collectors.toSet());

        LOGGER.debug("Successfully retrieved devices = {} for credential with id {}",
                     StringUtils.join(devices.toArray(), "; "), accessCredentialId);

        return devices;
    }

    public String getHexValueFor26BitCredentialByFacilityCodeAndCardNumber(UiCardSimulatorDTO uiCardSimulatorDTO) {
        UiCredentialDTO credentialDTO = new UiCredentialDTO();
        credentialDTO.setFacility_code(uiCardSimulatorDTO.getFacilityCode());
        credentialDTO.setReferenceId(uiCardSimulatorDTO.getCardNumber());

        CredentialFormat format = resolveFormat(100);
        List<CredentialFieldValue> values = createCredentialFieldValuesForEncoding(credentialDTO, format);
        Credential credential_rps = new Credential();
        credential_rps.setFieldValues(values);
        credential_rps.setNumBits(format.getNumBits());
        credential_rps.setFormat(format);

        BinaryCredentialEncoder encoder = new BinaryCredentialEncoder();
        encoder.encode(credential_rps);

        return credential_rps.getEncodedCredential();
    }
}
