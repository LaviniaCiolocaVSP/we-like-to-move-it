package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class FollowScheduleMessage extends DeviceAdministratorMessage {

    public FollowScheduleMessage() {
        super(DownstreamMessageType.FollowScheduleMessage);
    }

    public String toString() {
        return "FollowScheduleMessage : {objectId:" + this.deviceId + ", adminId:" + administratorId + "}";
    }
}
