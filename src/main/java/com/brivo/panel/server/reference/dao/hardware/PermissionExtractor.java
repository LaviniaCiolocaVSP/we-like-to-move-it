package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.event.SecurityAction;
import com.brivo.panel.server.reference.model.hardware.EnterPermission;
import com.brivo.panel.server.reference.model.hardware.LockPermission;
import com.brivo.panel.server.reference.model.hardware.Permission;
import com.google.common.collect.Maps;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Permission Extractor.
 */
@Component
public class PermissionExtractor implements ResultSetExtractor<List<Permission>> {
    @Override
    public List<Permission> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<Permission> list = new ArrayList<>();
        Map<String, EnterPermission> enterPermissionMap = Maps.newHashMap();

        while (rs.next()) {
            Long scheduleId = rs.getLong("scheduleID");
            Long groupId = rs.getLong("actorOID");
            Integer securityActionId = rs.getInt("securityActionID");

            SecurityAction action = SecurityAction.getSecurityAction(securityActionId);

            if (SecurityAction.ACTION_DEVICE_OPEN.equals(action) ||
                    SecurityAction.ACTION_DOOR_LOCK_OPEN_PRIVACY_OVERRIDE.equals(action)) {
                String enterPermKey = groupId + ":" + scheduleId;

                EnterPermission enterPerm = enterPermissionMap.get(enterPermKey);

                if (enterPerm == null) {
                    enterPerm = new EnterPermission();
                    enterPerm.setGroupId(groupId);
                    enterPerm.setScheduleId(scheduleId);
                    enterPerm.setAllowedPrivacyModeOverride(false);

                    enterPermissionMap.put(enterPermKey, enterPerm);
                    list.add(enterPerm);
                }

                boolean isPrivacyModeOverrideAllowed = enterPerm.getAllowedPrivacyModeOverride() ||
                        SecurityAction.ACTION_DOOR_LOCK_OPEN_PRIVACY_OVERRIDE
                                .equals(action);

                enterPerm.setAllowedPrivacyModeOverride(isPrivacyModeOverrideAllowed);

            } else if (SecurityAction.ACTION_LOCK_KEYPAD.equals(action)) {
                LockPermission lockPerm = new LockPermission();

                lockPerm.setGroupId(groupId);
                lockPerm.setScheduleId(scheduleId);

                list.add(lockPerm);
            }
        }

        return list;
    }
}
