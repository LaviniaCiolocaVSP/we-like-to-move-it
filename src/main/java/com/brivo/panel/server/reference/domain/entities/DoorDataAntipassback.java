package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "door_data_antipassback", schema = "brivo20", catalog = "onair")
public class DoorDataAntipassback implements Serializable {
    private long doorDataAntipassbackId;
    private Long deviceOid;
    private Long zone;
    private Long altReaderPanelOid;
    private Long altReaderBoardNum;
    private Long altReaderPointAddress;
    private Long altZone;
    private Device doorDataAntipassbackByObjectId;

    @Id
    @Column(name = "door_data_antipassback_id", nullable = false)
    public long getDoorDataAntipassbackId() {
        return doorDataAntipassbackId;
    }

    public void setDoorDataAntipassbackId(long doorDataAntipassbackId) {
        this.doorDataAntipassbackId = doorDataAntipassbackId;
    }

    @Column(name = "device_oid", nullable = true)
    public Long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(Long deviceOid) {
        this.deviceOid = deviceOid;
    }

    @Column(name = "zone", nullable = true)
    public Long getZone() {
        return zone;
    }

    public void setZone(Long zone) {
        this.zone = zone;
    }

    @Column(name = "alt_reader_panel_oid", nullable = true)
    public Long getAltReaderPanelOid() {
        return altReaderPanelOid;
    }

    public void setAltReaderPanelOid(Long altReaderPanelOid) {
        this.altReaderPanelOid = altReaderPanelOid;
    }

    @Column(name = "alt_reader_board_num", nullable = true)
    public Long getAltReaderBoardNum() {
        return altReaderBoardNum;
    }

    public void setAltReaderBoardNum(Long altReaderBoardNum) {
        this.altReaderBoardNum = altReaderBoardNum;
    }

    @Column(name = "alt_reader_point_address", nullable = true)
    public Long getAltReaderPointAddress() {
        return altReaderPointAddress;
    }

    public void setAltReaderPointAddress(Long altReaderPointAddress) {
        this.altReaderPointAddress = altReaderPointAddress;
    }

    @Column(name = "alt_zone", nullable = true)
    public Long getAltZone() {
        return altZone;
    }

    public void setAltZone(Long altZone) {
        this.altZone = altZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DoorDataAntipassback that = (DoorDataAntipassback) o;

        if (doorDataAntipassbackId != that.doorDataAntipassbackId) {
            return false;
        }
        if (deviceOid != null ? !deviceOid.equals(that.deviceOid) : that.deviceOid != null) {
            return false;
        }
        if (zone != null ? !zone.equals(that.zone) : that.zone != null) {
            return false;
        }
        if (altReaderPanelOid != null ? !altReaderPanelOid.equals(that.altReaderPanelOid) : that.altReaderPanelOid != null) {
            return false;
        }
        if (altReaderBoardNum != null ? !altReaderBoardNum.equals(that.altReaderBoardNum) : that.altReaderBoardNum != null) {
            return false;
        }
        if (altReaderPointAddress != null ? !altReaderPointAddress.equals(that.altReaderPointAddress) : that.altReaderPointAddress != null) {
            return false;
        }
        return altZone != null ? altZone.equals(that.altZone) : that.altZone == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (doorDataAntipassbackId ^ (doorDataAntipassbackId >>> 32));
        result = 31 * result + (deviceOid != null ? deviceOid.hashCode() : 0);
        result = 31 * result + (zone != null ? zone.hashCode() : 0);
        result = 31 * result + (altReaderPanelOid != null ? altReaderPanelOid.hashCode() : 0);
        result = 31 * result + (altReaderBoardNum != null ? altReaderBoardNum.hashCode() : 0);
        result = 31 * result + (altReaderPointAddress != null ? altReaderPointAddress.hashCode() : 0);
        result = 31 * result + (altZone != null ? altZone.hashCode() : 0);
        return result;
    }

    @OneToOne(mappedBy = "doorDataAntipassbackByObjectId", cascade = CascadeType.ALL)
    @JoinColumn(name = "device_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Device getDoorDataAntipassbackByObjectId() {
        return doorDataAntipassbackByObjectId;
    }

    public void setDoorDataAntipassbackByObjectId(Device doorDataAntipassbackByObjectId) {
        this.doorDataAntipassbackByObjectId = doorDataAntipassbackByObjectId;
    }
}
