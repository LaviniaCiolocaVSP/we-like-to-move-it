package com.brivo.panel.server.reference.model.mocks;

import com.brivo.panel.server.reference.model.credential.CardMask;
import com.brivo.panel.server.reference.model.usergroup.UserGroup;
import com.brivo.panel.server.reference.model.usergroup.UserGroupData;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class UserGroupBuilder {

    public static UserGroupData getMockUserGroup() {
        final UserGroupData userGroupData = new UserGroupData();

        final List<CardMask> cardMasks = getCardMasks();
        userGroupData.setCardmasks(cardMasks);

        final List<UserGroup> userGroups = getUserGroups();
        userGroupData.setUserGroups(userGroups);

        userGroupData.setLastUpdate(Instant.now());

        return userGroupData;
    }

    private static List<UserGroup> getUserGroups() {
        final List<UserGroup> userGroups = new ArrayList<>();
        final UserGroup userGroup = new UserGroup();
        userGroup.setUserGroupId(0L);

        final List<Long> userIds = new ArrayList<>();
        userIds.add(0L);
        userIds.add(0L);
        userIds.add(0L);
        userIds.add(0L);
        userGroup.setUserIds(userIds);

        userGroup.setAntiPassbackResetTime(0);
        userGroup.setImmuneToAntipassback(false);
        userGroup.setActive(Instant.now());
        userGroup.setExpires(Instant.now());

        userGroups.add(userGroup);

        return userGroups;
    }

    private static List<CardMask> getCardMasks() {
        final List<CardMask> cardMasks = new ArrayList<>();
        final CardMask cardMask = new CardMask();
        cardMask.setMask("ffff");
        cardMask.setNumberBits(0);

        return cardMasks;
    }
}
