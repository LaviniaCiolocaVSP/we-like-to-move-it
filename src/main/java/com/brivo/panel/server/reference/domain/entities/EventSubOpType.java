package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "event_sub_op_type", schema = "brivo20", catalog = "onair")
public class EventSubOpType {
    private long opTypeId;
    private String name;
    private Collection<EventSubCriteria> eventSubCriteriaByOpTypeId;

    @Id
    @Column(name = "op_type_id", nullable = false)
    public long getOpTypeId() {
        return opTypeId;
    }

    public void setOpTypeId(long opTypeId) {
        this.opTypeId = opTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventSubOpType that = (EventSubOpType) o;

        if (opTypeId != that.opTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (opTypeId ^ (opTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "eventSubOpTypeByOpTypeId")
    public Collection<EventSubCriteria> getEventSubCriteriaByOpTypeId() {
        return eventSubCriteriaByOpTypeId;
    }

    public void setEventSubCriteriaByOpTypeId(Collection<EventSubCriteria> eventSubCriteriaByOpTypeId) {
        this.eventSubCriteriaByOpTypeId = eventSubCriteriaByOpTypeId;
    }
}
