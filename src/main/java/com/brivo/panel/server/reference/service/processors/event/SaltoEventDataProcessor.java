package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 */
@Service
public class SaltoEventDataProcessor implements EventDataProcessor {
    private static final long SALTO_ROUTER_SUBTYPE_ID = 6L;
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private EventResolutionDao eventResolutionDao;

    @Autowired
    private ActorEventDataProcessor actorEventDataProcessor;

    @Autowired
    private DeviceEventDataProcessor deviceEventDataProcessor;

    /*@Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveSaltoEvent");

        Boolean result = false;

        switch(eventType)
        {
            case SALTO_REJECT_LOW_BATTERY:
            case FAILED_ACCESS_OFFLINE:
                result = actorEventDataProcessor.resolveEvent(eventType, event, panel);
                break;

            case SALTO_HELLO:
            case SALTO_SYSTEM_COMMS_FAILURE_SET:
            case SALTO_SYSTEM_COMMS_FAILURE_CLR:
            case SALTO_ROUTER_COMMS_FAILURE:
            {
                List<EventResolutionData> dataList = eventResolutionDao.getSaltoRouterEvent(event);

                if(dataList.size() == 1)
                {
                    event.setAccountId(dataList.get(0).getAccountId());
                    event.setObjectId(dataList.get(0).getDeviceOid());
                    event.setActorObjectId(dataList.get(0).getObjectId());
                    event.getEventData().setActorName(panel.getElectronicSerialNumber());
                    event.getEventData().setBoardTypeId(Long.valueOf(BoardType.SALTO_ROUTER.getTypeId()));
                    event.getEventData().setObjectTypeId(dataList.get(0).getObjectTypeId());
                    event.getEventData().setObjectName(dataList.get(0).getDeviceName());
                    event.getEventData().setObjectGroupName(dataList.get(0).getSecurityGroupName());
                    event.setObjectGroupObjectId(dataList.get(0).getSiteOid());
                    event.getEventData().setObjectSubtypeId(SALTO_ROUTER_SUBTYPE_ID);
                    event.getEventData().setActionAllowed(false);

                    Integer boardNumber = eventResolutionDao.getBoardNumber(event.getObjectId());
                    event.getEventData().setBoardNumber(boardNumber);

                    result = true;
                }
                break;
            }

            default:
                event.getEventData().setActorName(" ");
                result = deviceEventDataProcessor.resolveEvent(eventType, event, panel);
                break;
        }

        logger.trace("<<< ResolveSaltoEvent");

        return result;
    }*/

}
