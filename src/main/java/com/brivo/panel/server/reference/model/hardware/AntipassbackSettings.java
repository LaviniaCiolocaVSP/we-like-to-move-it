package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class AntipassbackSettings {
    private Integer alternateReaderBoardAddress;
    private Integer alternateReaderPointAddress;
    private Boolean inputReaderUsesAntipassback;
    private Integer inputReaderZone;
    private Boolean alternateReaderUsesAntipassback;
    private Integer alternateReaderZone;
    private Integer resetInterval;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getAlternateReaderBoardAddress() {
        return alternateReaderBoardAddress;
    }

    public void setAlternateReaderBoardAddress(Integer alternateReaderBoardAddress) {
        this.alternateReaderBoardAddress = alternateReaderBoardAddress;
    }

    public Integer getAlternateReaderPointAddress() {
        return alternateReaderPointAddress;
    }

    public void setAlternateReaderPointAddress(Integer alternateReaderPointAddress) {
        this.alternateReaderPointAddress = alternateReaderPointAddress;
    }

    public Boolean getInputReaderUsesAntipassback() {
        return inputReaderUsesAntipassback;
    }

    public void setInputReaderUsesAntipassback(Boolean inputReaderUsesAntipassback) {
        this.inputReaderUsesAntipassback = inputReaderUsesAntipassback;
    }

    public Integer getInputReaderZone() {
        return inputReaderZone;
    }

    public void setInputReaderZone(Integer inputReaderZone) {
        this.inputReaderZone = inputReaderZone;
    }

    public Boolean getAlternateReaderUsesAntipassback() {
        return alternateReaderUsesAntipassback;
    }

    public void setAlternateReaderUsesAntipassback(
            Boolean alternateReaderUsesAntipassback) {
        this.alternateReaderUsesAntipassback = alternateReaderUsesAntipassback;
    }

    public Integer getAlternateReaderZone() {
        return alternateReaderZone;
    }

    public void setAlternateReaderZone(Integer alternateReaderZone) {
        this.alternateReaderZone = alternateReaderZone;
    }

    public Integer getResetInterval() {
        return resetInterval;
    }

    public void setResetInterval(Integer resetInterval) {
        this.resetInterval = resetInterval;
    }


}
