package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.schedule.ScheduleDao;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.schedule.Holiday;
import com.brivo.panel.server.reference.model.schedule.PanelScheduleData;
import com.brivo.panel.server.reference.model.schedule.Schedule;
import com.brivo.panel.server.reference.model.schedule.ScheduleBlock;
import com.brivo.panel.server.reference.model.schedule.ScheduleException;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionBlock;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatDay;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatType;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatWeek;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TimeZone;

/**
 * Service class for handling schedule data.
 */
@Service
public class ScheduleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleService.class);

    private static final int ONE_SECOND_IN_MILLISECONDS = 1000;
    private static final int ONE_WEEK_IN_DAYS = 7;
    private static final int ONE_DAY_IN_HOURS = 24;
    private static final int ONE_HOUR_IN_MINUTES = 60;
    private static final int ONE_DAY_IN_MINUTES = ONE_DAY_IN_HOURS * ONE_HOUR_IN_MINUTES;
    private static final int ONE_WEEK_IN_MINUTES =
            ONE_WEEK_IN_DAYS * ONE_DAY_IN_HOURS * ONE_HOUR_IN_MINUTES;
    private static final int END_OF_WEEK_BLOCK_MIN = ONE_WEEK_IN_MINUTES - 1;
    private static final int ONE_MINUTE_IN_SECONDS = 60;
    private static final int MINIMUM_HOLIDAY_BLOCK_MINUTE = ONE_WEEK_IN_MINUTES;
    private static final int MAX_HOLIDAY_BLOCK_MINUTE = END_OF_WEEK_BLOCK_MIN + ONE_DAY_IN_MINUTES;

    @Autowired
    private ScheduleDao scheduleDao;

    @Value("${use.panel.local.time:true}")
    private boolean usePanelLocalTime;

    /**
     * Retrieve schedule and holiday date for this panel.
     *
     * @param panel The panel.
     * @return The schedule and holiday data.
     */
    public PanelScheduleData getSchedules(Panel panel) {
        LOGGER.info(">>> GetSchedules");

        PanelScheduleData data = getScheduleData(panel);

        data.setLastUpdate(panel.getSchedulesChanged());

        LOGGER.info("<<< GetSchedules");

        return data;
    }

    /**
     * Retrieve the schedule data for this panel.
     *
     * @param panel The panel.
     * @return PanelScheduleData.
     */
    private PanelScheduleData getScheduleData(Panel panel) {
        LOGGER.info(">>> GetScheduleData");

        PanelScheduleData data = new PanelScheduleData();
        data.setSchedules(loadSchedules(panel));
        data.setHolidays(loadHolidays(panel));

        LOGGER.info("<<< GetScheduleData");

        return data;
    }

    private List<Schedule> loadSchedules(Panel panel) {
        List<Schedule> scheduleList = scheduleDao.getAccountSchedules(panel);

        for (Schedule schedule : scheduleList) {
            loadScheduleBlocks(panel, schedule);
            loadScheduleExceptions(panel, schedule);
            schedule.setHolidayIds(scheduleDao.getScheduleHolidays(schedule));
        }

        return scheduleList;
    }

    /**
     * Load the schedule blocks for this schedule.
     * <p>
     * Run select_schedule_blocks.sql to load schedule data.
     *
     * @param panel
     * @param schedule
     */
    private void loadScheduleBlocks(Panel panel, Schedule schedule) {
//        LOGGER.info(">>> LoadScheduleBlocks");

        List<ScheduleBlock> normalBlocks = new ArrayList<>();
        List<ScheduleBlock> holidayBlocks = new ArrayList<>();

        List<ScheduleBlock> blocks = scheduleDao.getScheduleBlocks(schedule);
        TimeZone panelTz = TimeZone.getTimeZone(panel.getTimezone());
        int timeZoneOffsetSec = panelTz.getOffset(System.currentTimeMillis()) / ONE_SECOND_IN_MILLISECONDS;
        int timeZoneOffsetMin = timeZoneOffsetSec / ONE_MINUTE_IN_SECONDS;

        for (ScheduleBlock block : blocks) {
            if (is24x7Schedule(block)) {
                normalBlocks.add(fullWeekBlock());
                holidayBlocks.add(fullHolidayBlock());
            } else if (block.getStartMinute() >= MINIMUM_HOLIDAY_BLOCK_MINUTE) {
                adjustHolidayScheduleBlock(block, timeZoneOffsetMin);

                if (block.getStartMinute() > block.getEndMinute()) {
                    ScheduleBlock splitBlock = new ScheduleBlock();
                    splitBlock.setStartMinute(MINIMUM_HOLIDAY_BLOCK_MINUTE);
                    splitBlock.setEndMinute(block.getEndMinute());

                    block.setStartMinute(block.getStartMinute());
                    block.setEndMinute(MAX_HOLIDAY_BLOCK_MINUTE);

                    holidayBlocks.add(splitBlock);
                }

                holidayBlocks.add(block);

            } else {
                // adjustNormalScheduleBlock(block, timeZoneOffsetMin);
                if (!usePanelLocalTime) {
                    adjustNormalScheduleBlock(block, timeZoneOffsetMin);
                }
                if (block.getStartMinute() > block.getEndMinute()) {
                    ScheduleBlock splitBlock = new ScheduleBlock();
                    splitBlock.setStartMinute(0);
                    splitBlock.setEndMinute(block.getEndMinute());

                    block.setStartMinute(block.getStartMinute());
                    block.setEndMinute(END_OF_WEEK_BLOCK_MIN);

                    normalBlocks.add(splitBlock);
                }

                normalBlocks.add(block);
            }
        }

        Collections.sort(normalBlocks);
        Collections.sort(holidayBlocks);

        schedule.setScheduleBlocks(normalBlocks);
        schedule.setHolidayBlocks(holidayBlocks);

//        LOGGER.info("<<< LoadScheduleBlocks");
    }

    private boolean is24x7Schedule(ScheduleBlock block) {
        return block.getStartMinute().equals(0) && block.getEndMinute() > END_OF_WEEK_BLOCK_MIN;
    }

    private ScheduleBlock fullWeekBlock() {
        ScheduleBlock block = new ScheduleBlock();
        block.setStartMinute(0);
        block.setEndMinute(END_OF_WEEK_BLOCK_MIN);

        return block;
    }

    private ScheduleBlock fullHolidayBlock() {
        ScheduleBlock block = new ScheduleBlock();
        block.setStartMinute(MINIMUM_HOLIDAY_BLOCK_MINUTE);
        block.setEndMinute(MAX_HOLIDAY_BLOCK_MINUTE);

        return block;
    }

    /**
     * Panel deals with dates in UTC but schedules refer to a number of min into the week, which would
     * mean all schedule offsets are based on UTC. To make this work for the particular timezone a panel
     * actually exists in (so that 8am would refer to 8am appropriately for the panel) we need to adjust
     * the minutes in the same fashion as a TZ is offset from UTC.
     */
    void adjustNormalScheduleBlock(ScheduleBlock block, int timeZoneOffsetMin) {
        int adjustedStartMin = block.getStartMinute() - timeZoneOffsetMin;
        int adjustedEndMin = block.getEndMinute() - timeZoneOffsetMin;

        adjustedStartMin = adjustMinutesIntoBoundaries(adjustedStartMin);
        adjustedEndMin = adjustMinutesIntoBoundaries(adjustedEndMin);

        block.setStartMinute(adjustedStartMin);
        block.setEndMinute(adjustedEndMin);
    }

    int adjustMinutesIntoBoundaries(int blockTime) {
        if (blockTime < 0) {
            blockTime += ONE_WEEK_IN_MINUTES;
        } else if (blockTime >= ONE_WEEK_IN_MINUTES) {
            blockTime -= ONE_WEEK_IN_MINUTES;
        }

        return blockTime;
    }

    /**
     * Adjust holiday block values.
     * <p>
     * 1. Subtract one week of minutes. This is because we store holiday block values as offset
     * from 10080 (one week of minutes) and they should be transmitted to the panel as offset
     * from zero.
     * <p>
     * TODO Reconsider Old server adjustments to holiday schedule blocks.
     *
     * @param block The ScheduleBlock to adjust.
     */
    void adjustHolidayScheduleBlock(ScheduleBlock block, int timeZoneOffsetMin) {
        LOGGER.info(">>> AdjustHolidayScheduleBlock");

        //If it is the full day holiday block, don't shift blocks
        if (!(block.getStartMinute().equals(MINIMUM_HOLIDAY_BLOCK_MINUTE) && block.getEndMinute().equals(MAX_HOLIDAY_BLOCK_MINUTE))) {
          /*  int adjustedStartMinute = block.getStartMinute() - timeZoneOffsetMin;
            int adjustedEndMinute = block.getEndMinute() - timeZoneOffsetMin;

            adjustedStartMinute = adjustMinutesIntoHolidayBoundaries(adjustedStartMinute);
            adjustedEndMinute = adjustMinutesIntoHolidayBoundaries(adjustedEndMinute);
*/
            int adjustedStartMinute, adjustedEndMinute;
            if (!usePanelLocalTime) {
                adjustedStartMinute = block.getStartMinute() - timeZoneOffsetMin;
                adjustedEndMinute = block.getEndMinute() - timeZoneOffsetMin;
                adjustedStartMinute = adjustMinutesIntoHolidayBoundaries(adjustedStartMinute);
                adjustedEndMinute = adjustMinutesIntoHolidayBoundaries(adjustedEndMinute);
            } else {
                adjustedStartMinute = adjustMinutesIntoHolidayBoundaries(block.getStartMinute());
                adjustedEndMinute = adjustMinutesIntoHolidayBoundaries(block.getEndMinute());
            }
            block.setStartMinute(adjustedStartMinute);
            block.setEndMinute(adjustedEndMinute);
        }

        LOGGER.info("<<< AdjustHolidayScheduleBlock");
    }

    int adjustMinutesIntoHolidayBoundaries(int blockTime) {
        if (blockTime < MINIMUM_HOLIDAY_BLOCK_MINUTE) {
            blockTime += ONE_DAY_IN_MINUTES;
        } else if (blockTime > MAX_HOLIDAY_BLOCK_MINUTE) {
            blockTime -= ONE_DAY_IN_MINUTES;
        }

        return blockTime;
    }

    private List<Holiday> loadHolidays(Panel panel) {
        List<Holiday> holidayList = scheduleDao.getAccountHolidays(panel);

        for (Holiday holiday : holidayList) {
            Instant startDateSystemTime = holiday.getStartDate();
            Instant endDateSystemTime = holiday.getEndDate();
            holiday.setStartDate(convertTimeZoneFromSystemDefault(startDateSystemTime, panel.getTimezone()));
            holiday.setEndDate(convertTimeZoneFromSystemDefault(endDateSystemTime, panel.getTimezone()));
        }

        return holidayList;
    }

    Instant convertTimeZoneFromSystemDefault(Instant date, ZoneId zoneId) {
        // LOGGER.debug("Initial time [{}] in milliseconds.", date);

        Instant timeInstant = LocalDateTime.ofInstant(date, ZoneId.systemDefault()).atZone(zoneId).toInstant();

        //LOGGER.debug("Time [{}] as Instant in time zone of {}", timeInstant, zoneId);

        return timeInstant;
    }

    /**
     * Load schedule exceptions.
     * <p>
     * There is no firmware version check because panels using this protocol
     * support this feature.
     * <p>
     * For panels version 5.0.16 or later, run select_schedule_exception_blocks.sql to retrieve schedule exceptions
     * Add schedule exceptions (if panel meets minimum firmware requirements)
     * For each schedule exception rule, generate schedule exception object(s)
     * Begin Loop
     * If it's a one-time schedule exception,
     * Determine epoch start and end time given the date/time from the schedule exception
     * rule adjusted with panel's time zone
     * Create ScheduleException object with the start and end times
     * Add to collection of ScheduleException to be returned
     * If it's a recurring schedule exception,
     * Use the recurring rule (repeat ordinal indicates 1st, 2nd, 3rd, 4th or 5th week of the
     * month, repeat index indicates the day of the week - Sunday / Monday / Tuesday /
     * Wednesday / Thursday / Friday) and start/end time (minutes of the day) to come up
     * with the epoch start and end times for the next year (12 blocks, once per month).
     * Time zone to be used should be panel's time zone.
     * For each schedule exception block, create ScheduleException object with the start and end times
     * Add the ScheduleException objects to collection of ScheduleException to be returned
     * Add the collection of ScheduleException objects to the Schedule object
     * End Loop
     *
     * @param panel    The panel.
     * @param schedule The schedule.
     */
    private void loadScheduleExceptions(Panel panel, Schedule schedule) {
//        LOGGER.info(">>> LoadScheduleExceptions");

        List<ScheduleExceptionBlock> blockList = new ArrayList<>();

        List<ScheduleException> exceptionList = scheduleDao.getScheduleExceptions(schedule);

        for (ScheduleException exception : exceptionList) {
            if (ScheduleExceptionRepeatType.ONE_TIME_EXCEPTION.equals(exception.getRepeatType())) {
                blockList.add(calculateOneTimeExceptionBlock(panel, exception));
            } else {
                blockList.addAll(calculateRepeatingExceptionBlocks(panel, exception));
            }
        }

        Collections.sort(blockList);

        schedule.setExceptionBlocks(blockList);

//        LOGGER.info("<<< LoadScheduleExceptions");
    }

    /**
     * Calculate a ScheduleExceptionBlock for a one time exception.
     * <p>
     * This method operates on the repeat_ordinal value which we
     * store in ScheduleException as StartDateSeconds. I assume
     * this value is an epoch date in seconds which represents
     * the date of the exception at midnight in the system default
     * time zone (America\New_York).
     * <p>
     * This epoch date is essentially an Instant. We need to convert
     * this to a ZonedDateTime. This ZonedDateTime should have
     * exactly the same date and time as was set by the user in the
     * OnAir user interface. It will be in the system default time
     * zone (America\New_York). For example: 12/23/2016 midnight
     * America\New_York.
     *
     * @param panel     The panel.
     * @param exception The exception.
     * @return
     */
    ScheduleExceptionBlock calculateOneTimeExceptionBlock(Panel panel, ScheduleException exception) {
        // The date of the exception at midnight in system default time zone.
        LocalDateTime localDateTime =
                LocalDateTime.ofInstant(
                        Instant.ofEpochSecond(exception.getStartDateSeconds()),
                        ZoneId.systemDefault());

        // LOGGER.debug("Exception block initial time [{}] as LocalDateTime in system default time zone.", localDateTime);

        // Create two ZonedDateTime by adding start and end minutes to midnight.
        // These will have the same date as the LocalDate.
        ZonedDateTime dateAtMidnight = ZonedDateTime.of(localDateTime.toLocalDate(), LocalTime.MIDNIGHT, panel.getTimezone());
        ZonedDateTime startTimePanelZoned = dateAtMidnight.plusMinutes(exception.getStartMinute());
        ZonedDateTime endTimePanelZoned = dateAtMidnight.plusMinutes(exception.getEndMinute());

        // LOGGER.debug("Exception block start time [{}] as ZonedDateTime in panel time zone [{}].", startTimePanelZoned, panel.getTimezone());
        // LOGGER.debug("Exception block end time [{}] as ZonedDateTime in panel time zone [{}].", endTimePanelZoned, panel.getTimezone());

        ScheduleExceptionBlock block = new ScheduleExceptionBlock();

        block.setEnableBlock(exception.getIsEnable());
        block.setStartDate(startTimePanelZoned.toInstant());
        block.setEndDate(endTimePanelZoned.toInstant());

        return block;
    }

    /**
     * Calculate the schedule exception blocks for a repeating exception.
     * <p>
     * First get a list of twelve dates over twelve months, starting now,
     * each of which is on the ordinal week and specified week day (e.g.,
     * second Wednesday) of the month.
     * <p>
     * Loop through the dates and calculate LocalDateTimes for the start
     * and end times for the exception blocks. Do this by starting at
     * midnight and adding start or end minutes. Then translate these to
     * ZonedDateTime by adding the panel's time zone.
     *
     * @param panel     The panel.
     * @param exception The ScheduleException
     * @return List of ScheduleExceptionBlock
     */
    List<ScheduleExceptionBlock> calculateRepeatingExceptionBlocks(Panel panel, ScheduleException exception) {
        LOGGER.info(">>> CalculateRepeatingExceptionBlocks");

        List<ScheduleExceptionBlock> blockList = new ArrayList<>();

        // Get twelve dates, one per month, on the week\day specified.
        List<LocalDate> twelveMatchingDates =
                getTwelveDates(LocalDate.now(), exception.getRepeatWeek(), exception.getRepeatDay());

        // For each date calculate an exception block.
        for (LocalDate date : twelveMatchingDates) {
            //  LOGGER.debug("Exception Block date [{}] as LocalDate.", date);

            // Create two ZonedDateTime by adding start and end minutes to midnight.
            // These will have the same date as the LocalDate.
            ZonedDateTime dateAtMidnight = ZonedDateTime.of(date, LocalTime.MIDNIGHT, panel.getTimezone());
            ZonedDateTime startDateTime = dateAtMidnight.plusMinutes(exception.getStartMinute());
            ZonedDateTime endDateTime = dateAtMidnight.plusMinutes(exception.getEndMinute());

            //    LOGGER.debug("Exception Block start [{}] as ZonedDateTime.", startDateTime);
            //    LOGGER.debug("Exception Block end [{}] as ZonedDateTime.", endDateTime);

            ScheduleExceptionBlock block = new ScheduleExceptionBlock();

            block.setEnableBlock(exception.getIsEnable());
            block.setStartDate(startDateTime.toInstant());
            block.setEndDate(endDateTime.toInstant());

            //  LOGGER.debug("Returned exception block [{}].", block);

            blockList.add(block);
        }

        LOGGER.info("<<< CalculateRepeatingExceptionBlocks");

        return blockList;
    }

    /**
     * Calculate twelve dates that are on the week ordinal and day of week
     * specified, one month apart, starting in the month of the date
     * specified.
     *
     * @param dateToStart Date to start calculating from.
     * @param week        Week of Month.
     * @param day         Day of Week.
     * @return Twelve dates.
     */
    List<LocalDate> getTwelveDates(LocalDate dateToStart,
                                   ScheduleExceptionRepeatWeek week, ScheduleExceptionRepeatDay day) {
        List<LocalDate> dateList = new ArrayList<>();

        LocalDate calcDate = dateToStart;

        for (int i = 0; i < 12; i++) {
            calcDate = calcDate.with(
                    TemporalAdjusters.dayOfWeekInMonth(week.getOrdinalValue(), day.getDayOfWeek()));

            dateList.add(calcDate);

            calcDate = calcDate.with(TemporalAdjusters.firstDayOfNextMonth());
        }

        return dateList;
    }
}