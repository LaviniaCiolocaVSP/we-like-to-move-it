package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.DeviceType;
import org.springframework.data.repository.CrudRepository;

public interface DeviceTypeRepository extends CrudRepository<DeviceType, Long> {
}
