package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "access_credential_type", schema = "brivo20", catalog = "onair")
public class AccessCredentialType {
    private long accessCredentialTypeId;
    private String name;
    private String description;
    private Long numBits;
    private String format;
    private short supportedBy4000;
    private Long credentialEncodingId;
    private Timestamp newCardEngineDate;
    private Collection<AccessCredential> accessCredentialsByAccessCredentialTypeId;
    private CredentialEncoding credentialEncodingByCredentialEncodingId;
    private Collection<CredentialField> credentialFieldsByAccessCredentialTypeId;
    private Collection<CredentialMask> credentialMasksByAccessCredentialTypeId;

    @Override
    public String toString() {
        return "AccessCredentialType{" +
                "accessCredentialTypeId=" + accessCredentialTypeId +
                ", name='" + name + '\'' +
                ", numBits=" + numBits +
                ", format='" + format + '\'' +
                '}';
    }

    @Id
    @Column(name = "access_credential_type_id", nullable = false)
    public long getAccessCredentialTypeId() {
        return accessCredentialTypeId;
    }

    public void setAccessCredentialTypeId(long accessCredentialTypeId) {
        this.accessCredentialTypeId = accessCredentialTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "num_bits", nullable = true)
    public Long getNumBits() {
        return numBits;
    }

    public void setNumBits(Long numBits) {
        this.numBits = numBits;
    }

    @Basic
    @Column(name = "format", nullable = true, length = 255)
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Basic
    @Column(name = "supported_by_4000", nullable = false)
    public short getSupportedBy4000() {
        return supportedBy4000;
    }

    public void setSupportedBy4000(short supportedBy4000) {
        this.supportedBy4000 = supportedBy4000;
    }

    @Basic
    @Column(name = "credential_encoding_id", nullable = true)
    public Long getCredentialEncodingId() {
        return credentialEncodingId;
    }

    public void setCredentialEncodingId(Long credentialEncodingId) {
        this.credentialEncodingId = credentialEncodingId;
    }

    @Basic
    @Column(name = "new_card_engine_date", nullable = true)
    public Timestamp getNewCardEngineDate() {
        return newCardEngineDate;
    }

    public void setNewCardEngineDate(Timestamp newCardEngineDate) {
        this.newCardEngineDate = newCardEngineDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AccessCredentialType that = (AccessCredentialType) o;
        return accessCredentialTypeId == that.accessCredentialTypeId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(accessCredentialTypeId);
    }

    @OneToMany(mappedBy = "accessCredentialTypeByAccessCredentialTypeId", cascade = CascadeType.ALL)
    public Collection<AccessCredential> getAccessCredentialsByAccessCredentialTypeId() {
        return accessCredentialsByAccessCredentialTypeId;
    }

    public void setAccessCredentialsByAccessCredentialTypeId(Collection<AccessCredential> accessCredentialsByAccessCredentialTypeId) {
        this.accessCredentialsByAccessCredentialTypeId = accessCredentialsByAccessCredentialTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "credential_encoding_id", referencedColumnName = "credential_encoding_id", insertable = false, updatable = false)
    public CredentialEncoding getCredentialEncodingByCredentialEncodingId() {
        return credentialEncodingByCredentialEncodingId;
    }

    public void setCredentialEncodingByCredentialEncodingId(CredentialEncoding credentialEncodingByCredentialEncodingId) {
        this.credentialEncodingByCredentialEncodingId = credentialEncodingByCredentialEncodingId;
    }

    @OneToMany(mappedBy = "accessCredentialTypeByAccessCredentialTypeId", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    public Collection<CredentialField> getCredentialFieldsByAccessCredentialTypeId() {
        return credentialFieldsByAccessCredentialTypeId;
    }

    public void setCredentialFieldsByAccessCredentialTypeId(Collection<CredentialField> credentialFieldsByAccessCredentialTypeId) {
        this.credentialFieldsByAccessCredentialTypeId = credentialFieldsByAccessCredentialTypeId;
    }

    @OneToMany(mappedBy = "accessCredentialTypeByAccessCredentialType")
    public Collection<CredentialMask> getCredentialMasksByAccessCredentialTypeId() {
        return credentialMasksByAccessCredentialTypeId;
    }

    public void setCredentialMasksByAccessCredentialTypeId(Collection<CredentialMask> credentialMasksByAccessCredentialTypeId) {
        this.credentialMasksByAccessCredentialTypeId = credentialMasksByAccessCredentialTypeId;
    }
}
