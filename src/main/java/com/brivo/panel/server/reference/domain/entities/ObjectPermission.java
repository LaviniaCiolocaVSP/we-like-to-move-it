package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "object_permission", schema = "brivo20", catalog = "onair")
@IdClass(ObjectPermissionPK.class)
public class ObjectPermission {
    private long securityActionId;
    private long actorObjectId;
    private long objectId;
    private long scheduleId;
    private short ignoreLockdown;
    private LocalDateTime created;
    private LocalDateTime updated;
    private SecurityAction securityActionBySecurityActionId;
    private BrivoObject objectByActorBrivoObjectId;
    private BrivoObject objectByBrivoObjectId;
    private Schedule scheduleByScheduleId;

    @Id
    @Column(name = "security_action_id", nullable = false)
    public long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Id
    @Column(name = "actor_object_id", nullable = false)
    public long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "schedule_id", nullable = false)
    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "ignore_lockdown", nullable = false)
    public short getIgnoreLockdown() {
        return ignoreLockdown;
    }

    public void setIgnoreLockdown(short ignoreLockdown) {
        this.ignoreLockdown = ignoreLockdown;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ObjectPermission that = (ObjectPermission) o;

        if (securityActionId != that.securityActionId) return false;
        if (actorObjectId != that.actorObjectId) return false;
        if (objectId != that.objectId) return false;
        if (scheduleId != that.scheduleId) return false;
        if (ignoreLockdown != that.ignoreLockdown) return false;
        if (created != null ? !created.equals(that.created) : that.created != null) return false;
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityActionId ^ (securityActionId >>> 32));
        result = 31 * result + (int) (actorObjectId ^ (actorObjectId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (scheduleId ^ (scheduleId >>> 32));
        result = 31 * result + (int) ignoreLockdown;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "security_action_id", referencedColumnName = "security_action_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityAction getSecurityActionBySecurityActionId() {
        return securityActionBySecurityActionId;
    }

    public void setSecurityActionBySecurityActionId(SecurityAction securityActionBySecurityActionId) {
        this.securityActionBySecurityActionId = securityActionBySecurityActionId;
    }

    @ManyToOne
    @JoinColumn(name = "actor_object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public BrivoObject getObjectByActorBrivoObjectId() {
        return objectByActorBrivoObjectId;
    }

    public void setObjectByActorBrivoObjectId(BrivoObject objectByActorBrivoObjectId) {
        this.objectByActorBrivoObjectId = objectByActorBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", nullable = false, insertable = false,
            updatable = false)
    public Schedule getScheduleByScheduleId() {
        return scheduleByScheduleId;
    }

    public void setScheduleByScheduleId(Schedule scheduleByScheduleId) {
        this.scheduleByScheduleId = scheduleByScheduleId;
    }

    @Override
    public String toString() {
        return "ObjectPermission{" +
                "securityActionId=" + securityActionId +
                ", actorObjectId=" + actorObjectId +
                ", objectId=" + objectId +
                ", scheduleId=" + scheduleId +
                '}';
    }
}
