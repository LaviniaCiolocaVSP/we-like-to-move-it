package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.model.PanelInfoBean;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiCardSimulatorDTO;
import com.brivo.panel.server.reference.dto.ui.UiCredentialDTO;
import com.brivo.panel.server.reference.dto.ui.UiCredentialFormatDTO;
import com.brivo.panel.server.reference.service.UiAccessCredentialService;
import com.brivo.panel.server.reference.service.UiCredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/credential")
public class UiCredentialController {

    private final UiCredentialService credentialService;
    private final UiAccessCredentialService accessCredentialService;

    @Autowired
    public UiCredentialController(UiCredentialService credentialService, UiAccessCredentialService accessCredentialService) {
        this.credentialService = credentialService;
        this.accessCredentialService = accessCredentialService;
    }

    @GetMapping("/{accountId}")
    public Collection<UiCredentialDTO> getCredentialsForAccount(@PathVariable final long accountId) {
        return this.credentialService.getUiCredentialsForAccount(accountId);
    }

    @GetMapping("/panel/{panelOid}")
    public Collection<UiCredentialDTO> getCredentialsForPanel(@PathVariable final long panelOid) {
        return this.credentialService.getUiCredentialsForPanel(panelOid);
    }

    @GetMapping("/credentialFormats")
    public Collection<UiCredentialFormatDTO> getCredentialFormats() {
        return this.credentialService.getUiCredentialFormats();
    }

    @GetMapping("/getPanelsForCredential/{accessCredentialId}")
    public Collection<PanelInfoBean> getPanelsForCredential(@PathVariable final long accessCredentialId) {
        return this.credentialService.getPanelsForCredential(accessCredentialId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.credentialService.create(credentialDTO);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.credentialService.update(credentialDTO);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteCredentials(@RequestBody List<Long> credentialIds) {
        return this.credentialService.deleteCredentials(credentialIds);
    }

    @PostMapping(path = "/26Bit", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create26Bit(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.accessCredentialService.create26Bit(credentialDTO);
    }

    @PostMapping(path = "/26BitCucumber", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create26BitCucumber(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.accessCredentialService.create26BitCucumber(credentialDTO);
    }

    @PostMapping(path = "/26BitCucumberDates", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create26BitCucumberWithDates(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.accessCredentialService.create26BitCucumberWithDates(credentialDTO);
    }

    @PostMapping(path = "/cardSimulator26Bit", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO send26BitCredentialToCardSimulator(@RequestBody final UiCardSimulatorDTO uiCardSimulatorDTO) {
        return this.accessCredentialService.send26BitCredentialToArduinoCardSimulator(uiCardSimulatorDTO);
    }

    @PutMapping(path = "/26Bit", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update26Bit(@RequestBody final UiCredentialDTO credentialDTO) {
        return this.accessCredentialService.update26Bit(credentialDTO);
    }

    @DeleteMapping(path = "/cucumber", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteCredentialsCucumber(@RequestBody List<Long> credentialIds) {
        return this.credentialService.deleteCredentialsCucumber(credentialIds);
    }
}
