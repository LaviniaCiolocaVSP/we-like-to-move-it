package com.brivo.panel.server.reference.service.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileUrlResource;
import org.springframework.http.CacheControl;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

public final class DownloadUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(DownloadUtils.class);

    public static HttpHeaders buildHttpHeaders(final FileUrlResource accountResource) {
        final HttpHeaders headers = new HttpHeaders();
        headers.setContentDisposition(buildContentDisposition(accountResource));
        try {
            headers.setContentLength(accountResource.contentLength());
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalArgumentException(e.getMessage());
        }
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.setCacheControl(CacheControl.maxAge(7, TimeUnit.DAYS));
        return headers;
    }

    private static ContentDisposition buildContentDisposition(final FileUrlResource accountResource) {
        return ContentDisposition.builder("attachment")
                                 .filename(accountResource.getFilename())
                                 .creationDate(LocalDateTime.now().atZone(ZoneId.of("Europe/Bucharest")))
                                 .build();
    }
}
