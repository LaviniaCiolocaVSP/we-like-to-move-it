package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class CredentialEvent extends Event {
    private Long deviceId;
    private Long userId;
    private Long credentialId;
    private Integer bitCount;
    private String rawValue;
    private Long readerZoneId;
    private Long userCurrentZoneId;
    private Long secondCredentialId;

    @JsonCreator
    public CredentialEvent(@JsonProperty("eventType") final EventType eventType,
                           @JsonProperty("eventTime") final Instant eventTime,
                           @JsonProperty("deviceId") final Long deviceId,
                           @JsonProperty("userId") final Long userId,
                           @JsonProperty("credentialId") final Long credentialId,
                           @JsonProperty("bitCount") final Integer bitCount,
                           @JsonProperty("rawValue") final String rawValue,
                           @JsonProperty("readerZoneId") final Long readerZoneId,
                           @JsonProperty("userCurrentZoneId") final Long userCurrentZoneId,
                           @JsonProperty("secondCredentialId") final Long secondCredentialId) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.userId = userId;
        this.credentialId = credentialId;
        this.bitCount = bitCount;
        this.rawValue = rawValue;
        this.readerZoneId = readerZoneId;
        this.userCurrentZoneId = userCurrentZoneId;
        this.secondCredentialId = secondCredentialId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Long credentialId) {
        this.credentialId = credentialId;
    }

    public Integer getBitCount() {
        return bitCount;
    }

    public void setBitCount(Integer bitCount) {
        this.bitCount = bitCount;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public Long getReaderZoneId() {
        return readerZoneId;
    }

    public void setReaderZoneId(Long readerZoneId) {
        this.readerZoneId = readerZoneId;
    }

    public Long getUserCurrentZoneId() {
        return userCurrentZoneId;
    }

    public void setUserCurrentZoneId(Long userCurrentZoneId) {
        this.userCurrentZoneId = userCurrentZoneId;
    }

    public Long getSecondCredentialId() {
        return secondCredentialId;
    }

    public void setSecondCredentialId(Long secondCredentialId) {
        this.secondCredentialId = secondCredentialId;
    }

}
