package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ObjectPermissionForGroupDTO {
    private final Long deviceObjectId;
    private final Long scheduleId;
    private final Short ignoreLockDown;
    private final Long securityActionId;
    private final Long actorObjectId;

    @JsonCreator
    public ObjectPermissionForGroupDTO(@JsonProperty("securityActionId") final Long securityActionId,
                                       @JsonProperty("deviceObjectId") final Long deviceObjectId,
                                       @JsonProperty("scheduleId") final Long scheduleId,
                                       @JsonProperty("actorObjectId") final Long actorObjectId,
                                       @JsonProperty("ignoreLockDown") final Short ignoreLockDown) {
        this.securityActionId = securityActionId;
        this.deviceObjectId = deviceObjectId;
        this.scheduleId = scheduleId;
        this.ignoreLockDown = ignoreLockDown;
        this.actorObjectId = actorObjectId;
    }

    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public Short getIgnoreLockDown() {
        return ignoreLockDown;
    }

    public Long getSecurityActionId() {
        return securityActionId;
    }

    public Long getActorObjectId() {
        return actorObjectId;
    }
}
