package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "default_camera_group", schema = "brivo20", catalog = "onair")
public class DefaultCameraGroup {
    private long defaultCameraGroupId;
    private long cameraGroupObjectId;
    private long usersId;
    private CameraGroup cameraGroupByCameraGroupObjectId;

    @Id
    @Column(name = "default_camera_group_id", nullable = false)
    public long getDefaultCameraGroupId() {
        return defaultCameraGroupId;
    }

    public void setDefaultCameraGroupId(long defaultCameraGroupId) {
        this.defaultCameraGroupId = defaultCameraGroupId;
    }

    @Basic
    @Column(name = "camera_group_object_id", nullable = false)
    public long getCameraGroupObjectId() {
        return cameraGroupObjectId;
    }

    public void setCameraGroupObjectId(long cameraGroupObjectId) {
        this.cameraGroupObjectId = cameraGroupObjectId;
    }

    @Basic
    @Column(name = "users_id", nullable = false)
    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DefaultCameraGroup that = (DefaultCameraGroup) o;

        if (defaultCameraGroupId != that.defaultCameraGroupId) {
            return false;
        }
        if (cameraGroupObjectId != that.cameraGroupObjectId) {
            return false;
        }
        return usersId == that.usersId;
    }

    @Override
    public int hashCode() {
        int result = (int) (defaultCameraGroupId ^ (defaultCameraGroupId >>> 32));
        result = 31 * result + (int) (cameraGroupObjectId ^ (cameraGroupObjectId >>> 32));
        result = 31 * result + (int) (usersId ^ (usersId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "camera_group_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public CameraGroup getCameraGroupByCameraGroupObjectId() {
        return cameraGroupByCameraGroupObjectId;
    }

    public void setCameraGroupByCameraGroupObjectId(CameraGroup cameraGroupByCameraGroupObjectId) {
        this.cameraGroupByCameraGroupObjectId = cameraGroupByCameraGroupObjectId;
    }
}
