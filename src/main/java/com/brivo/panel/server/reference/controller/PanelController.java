package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.PanelIPAndSerialNumber;
import com.brivo.panel.server.reference.dto.ui.UiPanelDTO;
import com.brivo.panel.server.reference.dto.ui.UiPanelTypeDTO;
import com.brivo.panel.server.reference.service.SessionRegistryService;
import com.brivo.panel.server.reference.service.UiPanelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.socket.WebSocketSession;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/panel")
public class PanelController {

    private final UiPanelService panelService;
    private final SessionRegistryService sessionRegistryService;

    @Autowired
    public PanelController(final UiPanelService panelService, final SessionRegistryService sessionRegistryService) {
        this.panelService = panelService;
        this.sessionRegistryService = sessionRegistryService;
    }

    @GetMapping("/{accountId}")
    public Collection<UiPanelDTO> getPanelsForAccount(@PathVariable final Long accountId) {
        return panelService.getUiPanelsForAccountId(accountId);
    }

    @GetMapping("/get/{brainId}")
    public UiPanelDTO getPanelByBrainId(@PathVariable final Long brainId) {
        return panelService.getPanelByBrainId(brainId);
    }

    @GetMapping("/get/object/{objectId}")
    public UiPanelDTO getPanelByObjectId(@PathVariable final Long objectId) {
        return panelService.getPanelByObjectId(objectId);
    }

    @GetMapping("/get/serialNumber/{panelSerialNumber}")
    public UiPanelDTO getPanelByObjectId(@PathVariable final String panelSerialNumber) {
        return panelService.getPanelBySerialNumber(panelSerialNumber);
    }

    @GetMapping("/sessionActive/{panelObjectId}")
    public MessageDTO isPanelConnectedToRPS(@PathVariable final Long panelObjectId) {
        final WebSocketSession session = sessionRegistryService.getWebSocketSession(panelObjectId);
        String message = "CONNECTED";

        if (session == null)
            message = "NOT CONNECTED";

        return new MessageDTO(message);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deletePanels(@RequestBody List<Long> panelIds) {
        return this.panelService.deletePanels(panelIds);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final UiPanelDTO panelDTO) {
        return this.panelService.create(panelDTO);
    }

    @GetMapping("/panelTypes")
    public Collection<UiPanelTypeDTO> getPanelTypes() {
        return this.panelService.getPanelTypes();
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update(@RequestBody final UiPanelDTO panelDTO) {
        return this.panelService.update(panelDTO);
    }

    @PostMapping(
            path = "/updatePanelIP",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updatePanelIP(@RequestBody final PanelIPAndSerialNumber panelIPAndSerialNumber) {
        System.out.println("UPDATING PANEL IP");
        System.out.println(panelIPAndSerialNumber.toString());

        return panelService.updatePanelIP(panelIPAndSerialNumber);
    }

}
