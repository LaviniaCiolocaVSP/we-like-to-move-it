package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "oauth_client_details", schema = "brivo20", catalog = "onair")
public class OauthClientDetails {
    private String clientId;
    private String clientSecret;
    private String redirectUri;
    private long accessTokenTtl;
    private long refreshTokenTtl;
    private String authorizationGrantType;
    private Short whiteListed;
    private Collection<OauthAccessToken> oauthAccessTokensByClientId;
    private Collection<ApplicationKey> mobileInvitationByObjectId;

    @Id
    @Column(name = "client_id", nullable = false, length = 40)
    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Basic
    @Column(name = "client_secret", nullable = false, length = 256)
    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    @Basic
    @Column(name = "redirect_uri", nullable = true, length = 256)
    public String getRedirectUri() {
        return redirectUri;
    }

    public void setRedirectUri(String redirectUri) {
        this.redirectUri = redirectUri;
    }

    @Basic
    @Column(name = "access_token_ttl", nullable = false)
    public long getAccessTokenTtl() {
        return accessTokenTtl;
    }

    public void setAccessTokenTtl(long accessTokenTtl) {
        this.accessTokenTtl = accessTokenTtl;
    }

    @Basic
    @Column(name = "refresh_token_ttl", nullable = false)
    public long getRefreshTokenTtl() {
        return refreshTokenTtl;
    }

    public void setRefreshTokenTtl(long refreshTokenTtl) {
        this.refreshTokenTtl = refreshTokenTtl;
    }

    @Basic
    @Column(name = "authorization_grant_type", nullable = false, length = 256)
    public String getAuthorizationGrantType() {
        return authorizationGrantType;
    }

    public void setAuthorizationGrantType(String authorizationGrantType) {
        this.authorizationGrantType = authorizationGrantType;
    }

    @Basic
    @Column(name = "white_listed", nullable = true)
    public Short getWhiteListed() {
        return whiteListed;
    }

    public void setWhiteListed(Short whiteListed) {
        this.whiteListed = whiteListed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OauthClientDetails that = (OauthClientDetails) o;

        if (accessTokenTtl != that.accessTokenTtl) {
            return false;
        }
        if (refreshTokenTtl != that.refreshTokenTtl) {
            return false;
        }
        if (clientId != null ? !clientId.equals(that.clientId) : that.clientId != null) {
            return false;
        }
        if (clientSecret != null ? !clientSecret.equals(that.clientSecret) : that.clientSecret != null) {
            return false;
        }
        if (redirectUri != null ? !redirectUri.equals(that.redirectUri) : that.redirectUri != null) {
            return false;
        }
        if (authorizationGrantType != null ? !authorizationGrantType.equals(that.authorizationGrantType) : that.authorizationGrantType != null) {
            return false;
        }
        return whiteListed != null ? whiteListed.equals(that.whiteListed) : that.whiteListed == null;
    }

    @Override
    public int hashCode() {
        int result = clientId != null ? clientId.hashCode() : 0;
        result = 31 * result + (clientSecret != null ? clientSecret.hashCode() : 0);
        result = 31 * result + (redirectUri != null ? redirectUri.hashCode() : 0);
        result = 31 * result + (int) (accessTokenTtl ^ (accessTokenTtl >>> 32));
        result = 31 * result + (int) (refreshTokenTtl ^ (refreshTokenTtl >>> 32));
        result = 31 * result + (authorizationGrantType != null ? authorizationGrantType.hashCode() : 0);
        result = 31 * result + (whiteListed != null ? whiteListed.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "oauthClientDetailsByClientId")
    public Collection<OauthAccessToken> getOauthAccessTokensByClientId() {
        return oauthAccessTokensByClientId;
    }

    public void setOauthAccessTokensByClientId(Collection<OauthAccessToken> oauthAccessTokensByClientId) {
        this.oauthAccessTokensByClientId = oauthAccessTokensByClientId;
    }

    @OneToMany(mappedBy = "oauthClientDetailsByAppKey")
    public Collection<ApplicationKey> getMobileInvitationByObjectId() {
        return mobileInvitationByObjectId;
    }

    public void setMobileInvitationByObjectId(Collection<ApplicationKey> mobileInvitationByObjectId) {
        this.mobileInvitationByObjectId = mobileInvitationByObjectId;
    }
}
