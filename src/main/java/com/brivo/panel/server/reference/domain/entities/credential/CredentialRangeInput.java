package com.brivo.panel.server.reference.domain.entities.credential;


import java.math.BigInteger;
import java.util.List;

public class CredentialRangeInput
{
    private long credentialFormatId;
    private BigInteger firstExternalNumber;
    private BigInteger lastExternalNumber;
    private BigInteger startValue;
    
    private List<CredentialInput.CredentialFieldValueInput> fieldValues;
    
    private boolean create;
    
    public CredentialRange toCredentialRange() {
        CredentialRange range = new CredentialRange();
        range.setFormat(new CredentialFormat(credentialFormatId));
        range.setFirstExternalNumber(getFirstExternalNumber());
        range.setLastExternalNumber(getLastExternalNumber());
        range.setStartValue(getStartValue());
        range.setFieldValues(CredentialInput.CredentialFieldValueInput.toList(getFieldValues()));
        
        return range;
    }
    
    public boolean create()
    {
        return create;
    }

    public void setCreate(boolean create)
    {
        this.create = create;
    }

    

    public long getCredentialFormatId()
    {
        return credentialFormatId;
    }


    public void setCredentialFormatId(long credentialFormatId)
    {
        this.credentialFormatId = credentialFormatId;
    }


    public BigInteger getFirstExternalNumber()
    {
        return firstExternalNumber;
    }

    public void setFirstExternalNumber(BigInteger firstExternalNumber)
    {
        this.firstExternalNumber = firstExternalNumber;
    }

    public BigInteger getLastExternalNumber()
    {
        return lastExternalNumber;
    }

    public void setLastExternalNumber(BigInteger lastExternalNumber)
    {
        this.lastExternalNumber = lastExternalNumber;
    }

    public BigInteger getStartValue()
    {
        return startValue;
    }

    public void setStartValue(BigInteger startValue)
    {
        this.startValue = startValue;
    }

    public List<CredentialInput.CredentialFieldValueInput> getFieldValues()
    {
        return fieldValues;
    }

    public void setFieldValues(List<CredentialInput.CredentialFieldValueInput> values)
    {
        this.fieldValues = values;
    }

}
