package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "device")
public class Device implements Serializable {
    private long deviceId;
    private Long deviceTypeId;
    private Long objectId;
    private long brainId;
    private String name;
    private String description;
    private short isLockedDown;
    private long panelId;
    private LocalDateTime created;
    private LocalDateTime updated;
    private long opsFlag;
    private short deleted;
    private Short isIngress;
    private Short isEgress;
    private Long twoFactorScheduleId;
    private Long twoFactorInterval;
    private Short enableLiveControl;
    private Long cardRequiredScheduleId;
    //    private Collection<Camera> camerasByDeviceId;
//    private Collection<CameraDeviceMapping> cameraDeviceMappingsByDeviceId;
    private BrivoObject objectByBrivoObjectId;
    private DeviceType deviceTypeByDeviceTypeId;
    private Brain brainByBrainId;
    private long accountId;
    private Account accountByAccountId;
    //    private Collection<DeviceHoliday> deviceHolidaysByDeviceId;
    private Collection<DeviceProperty> devicePropertiesByDeviceId;
    private Collection<DeviceSchedule> deviceSchedulesByDeviceId;
    private DoorDataAntipassback doorDataAntipassbackByObjectId;
    //    private Collection<ElevatorFloorMap> elevatorFloorMapByObjectId;
//    private Collection<ElevatorFloorMap> elevatorFloorMapByObjectId_0;
//    //    private Collection<IpacData> ipacDataByDeviceId;
//    private Collection<IpacData> ipacDataByDeviceId_0;
    private Collection<ProgDeviceData> progDeviceDataByObjectId;
    private Collection<ProgDevicePuts> progDevicePutsByObjectId;
    //    //    private Collection<VideoCameraDeviceMapping> videoCameraDeviceMappingsByDeviceId;
    private DoorData doorData;

    @Id
    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Column(name = "object_id", nullable = false, insertable = false, updatable = false)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Column(name = "device_type_id", nullable = false, insertable = false, updatable = false)
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "is_locked_down", nullable = false)
    public short getIsLockedDown() {
        return isLockedDown;
    }

    public void setIsLockedDown(short isLockedDown) {
        this.isLockedDown = isLockedDown;
    }

    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Column(name = "panel_id", nullable = false)
    public long getPanelId() {
        return panelId;
    }

    public void setPanelId(long panelId) {
        this.panelId = panelId;
    }

    @Column(name = "ops_flag", nullable = false)
    public long getOpsFlag() {
        return opsFlag;
    }

    public void setOpsFlag(long opsFlag) {
        this.opsFlag = opsFlag;
    }

    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Column(name = "is_ingress", nullable = true)
    public Short getIsIngress() {
        return isIngress;
    }

    public void setIsIngress(Short isIngress) {
        this.isIngress = isIngress;
    }

    @Column(name = "is_egress", nullable = true)
    public Short getIsEgress() {
        return isEgress;
    }

    public void setIsEgress(Short isEgress) {
        this.isEgress = isEgress;
    }

    @Column(name = "two_factor_schedule_id", nullable = true)
    public Long getTwoFactorScheduleId() {
        return twoFactorScheduleId;
    }

    public void setTwoFactorScheduleId(Long twoFactorScheduleId) {
        this.twoFactorScheduleId = twoFactorScheduleId;
    }

    @Column(name = "two_factor_interval", nullable = true)
    public Long getTwoFactorInterval() {
        return twoFactorInterval;
    }

    public void setTwoFactorInterval(Long twoFactorInterval) {
        this.twoFactorInterval = twoFactorInterval;
    }

    @Column(name = "enable_live_control", nullable = true)
    public Short getEnableLiveControl() {
        return enableLiveControl;
    }

    public void setEnableLiveControl(Short enableLiveControl) {
        this.enableLiveControl = enableLiveControl;
    }

    @Column(name = "card_required_schedule_id", nullable = true)
    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Device device = (Device) o;
        return deviceId == device.deviceId &&
                deviceTypeByDeviceTypeId.getDeviceTypeId() == device.deviceTypeByDeviceTypeId.getDeviceTypeId() &&
                objectByBrivoObjectId.getObjectId() == device.objectByBrivoObjectId.getObjectId() &&
                brainId == device.getBrainId() &&
                Objects.equals(accountId, device.getAccountId()) &&
                Objects.equals(name, device.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, deviceTypeByDeviceTypeId.getDeviceTypeId(), objectByBrivoObjectId.getObjectId(),
                brainId, accountId, name);
    }

    /*
    @OneToMany(mappedBy = "deviceByDeviceId")
    public Collection<Camera> getCamerasByDeviceId() {
        return camerasByDeviceId;
    }

    public void setCamerasByDeviceId(Collection<Camera> camerasByDeviceId) {
        this.camerasByDeviceId = camerasByDeviceId;
    }

    @OneToMany(mappedBy = "deviceByDeviceId")
    public Collection<CameraDeviceMapping> getCameraDeviceMappingsByDeviceId() {
        return cameraDeviceMappingsByDeviceId;
    }

    public void setCameraDeviceMappingsByDeviceId(Collection<CameraDeviceMapping> cameraDeviceMappingsByDeviceId) {
        this.cameraDeviceMappingsByDeviceId = cameraDeviceMappingsByDeviceId;
    }
    */

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "device_type_id", referencedColumnName = "device_type_id", nullable = false)
    public DeviceType getDeviceTypeByDeviceTypeId() {
        return deviceTypeByDeviceTypeId;
    }

    public void setDeviceTypeByDeviceTypeId(DeviceType deviceTypeByDeviceTypeId) {
        this.deviceTypeByDeviceTypeId = deviceTypeByDeviceTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByBrainId() {
        return brainByBrainId;
    }

    public void setBrainByBrainId(Brain brainByBrainId) {
        this.brainByBrainId = brainByBrainId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    /*
    @OneToMany(mappedBy = "deviceByDeviceId")
    public Collection<DeviceHoliday> getDeviceHolidaysByDeviceId() {
        return deviceHolidaysByDeviceId;
    }

    public void setDeviceHolidaysByDeviceId(Collection<DeviceHoliday> deviceHolidaysByDeviceId) {
        this.deviceHolidaysByDeviceId = deviceHolidaysByDeviceId;
    }
    */

    @OneToMany(mappedBy = "deviceByDeviceId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<DeviceProperty> getDevicePropertiesByDeviceId() {
        return devicePropertiesByDeviceId;
    }

    public void setDevicePropertiesByDeviceId(Collection<DeviceProperty> devicePropertiesByDeviceId) {
        this.devicePropertiesByDeviceId = devicePropertiesByDeviceId;
    }

    @OneToMany(mappedBy = "deviceByDeviceId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<DeviceSchedule> getDeviceSchedulesByDeviceId() {
        return deviceSchedulesByDeviceId;
    }

    public void setDeviceSchedulesByDeviceId(Collection<DeviceSchedule> deviceSchedulesByDeviceId) {
        this.deviceSchedulesByDeviceId = deviceSchedulesByDeviceId;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "device_oid", nullable = false, insertable = false, updatable = false)
    public DoorDataAntipassback getDoorDataAntipassbackByObjectId() {
        return doorDataAntipassbackByObjectId;
    }

    public void setDoorDataAntipassbackByObjectId(DoorDataAntipassback doorDataAntipassbackByObjectId) {
        this.doorDataAntipassbackByObjectId = doorDataAntipassbackByObjectId;
    }

//    @OneToMany(mappedBy = "elevatorByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    public Collection<ElevatorFloorMap> getElevatorFloorMapByObjectId() {
//        return elevatorFloorMapByObjectId;
//    }
//
//    public void setElevatorFloorMapByObjectId(Collection<ElevatorFloorMap> elevatorFloorMapByObjectId) {
//        this.elevatorFloorMapByObjectId = elevatorFloorMapByObjectId;
//    }
//
//    @OneToMany(mappedBy = "floorByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
//    public Collection<ElevatorFloorMap> getElevatorFloorMapByObjectId_0() {
//        return elevatorFloorMapByObjectId_0;
//    }
//
//    public void setElevatorFloorMapByObjectId_0(Collection<ElevatorFloorMap> elevatorFloorMapByObjectId_0) {
//        this.elevatorFloorMapByObjectId_0 = elevatorFloorMapByObjectId_0;
//    }

    /*
    @OneToMany(mappedBy = "deviceByGate1DeviceId")
    public Collection<IpacData> getIpacDataByDeviceId() {
        return ipacDataByDeviceId;
    }

    public void setIpacDataByDeviceId(Collection<IpacData> ipacDataByDeviceId) {
        this.ipacDataByDeviceId = ipacDataByDeviceId;
    }

    @OneToMany(mappedBy = "deviceByGate2DeviceId")
    public Collection<IpacData> getIpacDataByDeviceId_0() {
        return ipacDataByDeviceId_0;
    }

    public void setIpacDataByDeviceId_0(Collection<IpacData> ipacDataByDeviceId_0) {
        this.ipacDataByDeviceId_0 = ipacDataByDeviceId_0;
    }
    */

    @OneToMany(mappedBy = "deviceByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ProgDeviceData> getProgDeviceDataByObjectId() {
        Collection<ProgDeviceData> copyOfProgDeviceData = new ArrayList<>();

        if (progDeviceDataByObjectId != null) {
            progDeviceDataByObjectId.forEach(progDeviceData -> {
                copyOfProgDeviceData.add(progDeviceData);
            });
        }
        return copyOfProgDeviceData;
    }

    public void setProgDeviceDataByObjectId(Collection<ProgDeviceData> progDeviceDataByObjectId) {
        this.progDeviceDataByObjectId = progDeviceDataByObjectId;
    }

    @OneToMany(mappedBy = "deviceByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ProgDevicePuts> getProgDevicePutsByObjectId() {
        Collection<ProgDevicePuts> copyOfProgDevicePuts = new ArrayList<>();

        if (progDevicePutsByObjectId != null) {
            progDevicePutsByObjectId.forEach(progDevicePuts -> {
                copyOfProgDevicePuts.add(progDevicePuts);
            });
        }

        return copyOfProgDevicePuts;
    }

    public void setProgDevicePutsByObjectId(Collection<ProgDevicePuts> progDevicePutsByObjectId) {
        this.progDevicePutsByObjectId = progDevicePutsByObjectId;
    }

    /*
    @OneToMany(mappedBy = "deviceByDeviceId")
    public Collection<VideoCameraDeviceMapping> getVideoCameraDeviceMappingsByDeviceId() {
        return videoCameraDeviceMappingsByDeviceId;
    }

    public void setVideoCameraDeviceMappingsByDeviceId(Collection<VideoCameraDeviceMapping> videoCameraDeviceMappingsByDeviceId) {
        this.videoCameraDeviceMappingsByDeviceId = videoCameraDeviceMappingsByDeviceId;
    }
    */

    @OneToOne(mappedBy = "device", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = DoorData.class,
            optional = false)
    public DoorData getDoorData() {
        return doorData;
    }

    public void setDoorData(DoorData doorData) {
        this.doorData = doorData;
    }
}
