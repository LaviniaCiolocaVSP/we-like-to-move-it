package com.brivo.panel.server.reference.model.mocks;

import com.brivo.panel.server.reference.model.hardware.Panel;

import java.time.ZoneId;

public class PanelBuilder {

    private static final Long DEFAULT_PANEL_ACCOUNT_ID = 82836L;
    private static final Long DEFAULT_OBJECT_ID = 928264L;
    private static String panelElectronicSerialNumberFor338 = "THB-3C-YYYVQ";
    private static String panelElectronicSerialNumberFor553 = "THB-3F-YYYVR";

    private static String testPanelElectronicSerialNumber = "THB-3C-YYYVQ";

    public static Panel getMockPanel() {
        Panel panel = new Panel();

        panel.setIsRegistered(true);
        panel.setAccountId(DEFAULT_PANEL_ACCOUNT_ID);
        panel.setObjectId(DEFAULT_OBJECT_ID);
        panel.setElectronicSerialNumber(testPanelElectronicSerialNumber);
        panel.setTimezone(ZoneId.systemDefault());

        return panel;
    }

    public static void setPanelElectronicSerialNumber(final String panelElectronicSerialNumber) {
        PanelBuilder.testPanelElectronicSerialNumber = panelElectronicSerialNumber;
    }
}
