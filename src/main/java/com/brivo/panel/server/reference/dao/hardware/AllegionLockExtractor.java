package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.AllegionLock;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class AllegionLockExtractor implements ResultSetExtractor<List<AllegionLock>> {

    @Override
    public List<AllegionLock> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<AllegionLock> list = new ArrayList<AllegionLock>();

        while (rs.next()) {
            AllegionLock lock = new AllegionLock();
            lock.setDeviceId(rs.getLong("deviceId"));
            lock.setDoorAjarDuration(rs.getInt("doorAjarDuration"));

            Integer reportDoorAjarInt = rs.getInt("reportDoorAjar");
            Boolean reportDoorAjar = false;
            if (reportDoorAjarInt != null && reportDoorAjarInt == 1) {
                reportDoorAjar = true;
            }
            lock.setReportDoorAjar(reportDoorAjar);

            lock.setPassthroughDelay(rs.getInt("passthroughDelay"));
            lock.setLockId(rs.getInt("lockId"));

            Integer enablePrivacyModeInt = rs.getInt("enablePrivacyMode");
            Boolean enablePrivacyMode = false;
            if (enablePrivacyModeInt != null && enablePrivacyModeInt == 1) {
                enablePrivacyMode = true;
            }
            lock.setPrivacyModeOn(enablePrivacyMode);

            long unlockScheduleId = 0;
            long lockdown = rs.getLong("lockedDown");
            if (rs.wasNull()) {
                unlockScheduleId = rs.getLong("unlockScheduleId");
            } else if (lockdown > 0) {
                lock.setPrivacyModeOn(false);
            }
            lock.setUnlockScheduleId(unlockScheduleId);

            list.add(lock);
        }

        return list;
    }

}
