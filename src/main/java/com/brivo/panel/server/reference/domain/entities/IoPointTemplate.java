package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "io_point_template", schema = "brivo20", catalog = "onair")
@IdClass(IoPointTemplatePK.class)
public class IoPointTemplate {
    private long boardType;
    private short pointAddress;
    private String silkscreen;
    private String defaultLabel;
    private long type;
    private Short eol;
    private Short state;

    @Id
    @Column(name = "board_type", nullable = false)
    public long getBoardType() {
        return boardType;
    }

    public void setBoardType(long boardType) {
        this.boardType = boardType;
    }

    @Id
    @Column(name = "point_address", nullable = false)
    public short getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(short pointAddress) {
        this.pointAddress = pointAddress;
    }

    @Basic
    @Column(name = "silkscreen", nullable = true, length = 32)
    public String getSilkscreen() {
        return silkscreen;
    }

    public void setSilkscreen(String silkscreen) {
        this.silkscreen = silkscreen;
    }

    @Basic
    @Column(name = "default_label", nullable = true, length = 32)
    public String getDefaultLabel() {
        return defaultLabel;
    }

    public void setDefaultLabel(String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    @Basic
    @Column(name = "eol", nullable = true)
    public Short getEol() {
        return eol;
    }

    public void setEol(Short eol) {
        this.eol = eol;
    }

    @Basic
    @Column(name = "state", nullable = true)
    public Short getState() {
        return state;
    }

    public void setState(Short state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IoPointTemplate that = (IoPointTemplate) o;

        if (boardType != that.boardType) {
            return false;
        }
        if (pointAddress != that.pointAddress) {
            return false;
        }
        if (type != that.type) {
            return false;
        }
        if (silkscreen != null ? !silkscreen.equals(that.silkscreen) : that.silkscreen != null) {
            return false;
        }
        if (defaultLabel != null ? !defaultLabel.equals(that.defaultLabel) : that.defaultLabel != null) {
            return false;
        }
        if (eol != null ? !eol.equals(that.eol) : that.eol != null) {
            return false;
        }
        return state != null ? state.equals(that.state) : that.state == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (boardType ^ (boardType >>> 32));
        result = 31 * result + (int) pointAddress;
        result = 31 * result + (silkscreen != null ? silkscreen.hashCode() : 0);
        result = 31 * result + (defaultLabel != null ? defaultLabel.hashCode() : 0);
        result = 31 * result + (int) (type ^ (type >>> 32));
        result = 31 * result + (eol != null ? eol.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        return result;
    }
}
