package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.AccountType;
import org.springframework.data.repository.CrudRepository;

public interface AccountTypeRepository extends CrudRepository<AccountType, Long> {
}
