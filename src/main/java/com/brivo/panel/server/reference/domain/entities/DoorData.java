package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "door_data", schema = "brivo20", catalog = "onair")
public class DoorData {
    private long deviceOid;
    private Long boardNum;
    private Long nodeNum;
    private Long maxInvalid;
    private Long invalidShutout;
    private Long passthrough;
    private Short useAlarmShunt;
    private Long alarmDelay;
    private Long doorAjar;
    private Short notifyOnAjar;
    private Short notifyOnForced;
    private Long invalidCredWin;
    private Short rexFiresDoor;
    private Short scheduleEnabled;
    private Short cacheCredTimeLimit;
    private Short enablePrivacyMode;
    private Long debouncePeriod;
    private Short useLockOnOpen;
    private Long lockOnOpenDelay;
    private Long osdpPdAddress;
    private Device device;
    //private Board board;

    @Id
    @Column(name = "device_oid", nullable = false)
    public long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(long deviceOid) {
        this.deviceOid = deviceOid;
    }

    @Column(name = "board_num", nullable = true)
    public Long getBoardNum() {
        return boardNum;
    }

    public void setBoardNum(Long boardNum) {
        this.boardNum = boardNum;
    }

    @Column(name = "node_num", nullable = true)
    public Long getNodeNum() {
        return nodeNum;
    }

    public void setNodeNum(Long nodeNum) {
        this.nodeNum = nodeNum;
    }

    @Column(name = "max_invalid", nullable = true)
    public Long getMaxInvalid() {
        return maxInvalid;
    }

    public void setMaxInvalid(Long maxInvalid) {
        this.maxInvalid = maxInvalid;
    }

    @Column(name = "invalid_shutout", nullable = true)
    public Long getInvalidShutout() {
        return invalidShutout;
    }

    public void setInvalidShutout(Long invalidShutout) {
        this.invalidShutout = invalidShutout;
    }

    @Column(name = "passthrough", nullable = true)
    public Long getPassthrough() {
        return passthrough;
    }

    public void setPassthrough(Long passthrough) {
        this.passthrough = passthrough;
    }

    @Column(name = "use_alarm_shunt", nullable = true)
    public Short getUseAlarmShunt() {
        return useAlarmShunt;
    }

    public void setUseAlarmShunt(Short useAlarmShunt) {
        this.useAlarmShunt = useAlarmShunt;
    }

    @Column(name = "alarm_delay", nullable = true)
    public Long getAlarmDelay() {
        return alarmDelay;
    }

    public void setAlarmDelay(Long alarmDelay) {
        this.alarmDelay = alarmDelay;
    }

    @Column(name = "door_ajar", nullable = true)
    public Long getDoorAjar() {
        return doorAjar;
    }

    public void setDoorAjar(Long doorAjar) {
        this.doorAjar = doorAjar;
    }

    @Column(name = "notify_on_ajar", nullable = true)
    public Short getNotifyOnAjar() {
        return notifyOnAjar;
    }

    public void setNotifyOnAjar(Short notifyOnAjar) {
        this.notifyOnAjar = notifyOnAjar;
    }

    @Column(name = "notify_on_forced", nullable = true)
    public Short getNotifyOnForced() {
        return notifyOnForced;
    }

    public void setNotifyOnForced(Short notifyOnForced) {
        this.notifyOnForced = notifyOnForced;
    }

    @Column(name = "invalid_cred_win", nullable = true)
    public Long getInvalidCredWin() {
        return invalidCredWin;
    }

    public void setInvalidCredWin(Long invalidCredWin) {
        this.invalidCredWin = invalidCredWin;
    }

    @Column(name = "rex_fires_door", nullable = true)
    public Short getRexFiresDoor() {
        return rexFiresDoor;
    }

    public void setRexFiresDoor(Short rexFiresDoor) {
        this.rexFiresDoor = rexFiresDoor;
    }

    @Column(name = "schedule_enabled", nullable = true)
    public Short getScheduleEnabled() {
        return scheduleEnabled;
    }

    public void setScheduleEnabled(Short scheduleEnabled) {
        this.scheduleEnabled = scheduleEnabled;
    }

    @Column(name = "cache_cred_time_limit", nullable = true)
    public Short getCacheCredTimeLimit() {
        return cacheCredTimeLimit;
    }

    public void setCacheCredTimeLimit(Short cacheCredTimeLimit) {
        this.cacheCredTimeLimit = cacheCredTimeLimit;
    }

    @Column(name = "enable_privacy_mode", nullable = true)
    public Short getEnablePrivacyMode() {
        return enablePrivacyMode;
    }

    public void setEnablePrivacyMode(Short enablePrivacyMode) {
        this.enablePrivacyMode = enablePrivacyMode;
    }

    @Column(name = "debounce_period", nullable = true)
    public Long getDebouncePeriod() {
        return debouncePeriod;
    }

    public void setDebouncePeriod(Long debouncePeriod) {
        this.debouncePeriod = debouncePeriod;
    }

    @Column(name = "use_lock_on_open", nullable = true)
    public Short getUseLockOnOpen() {
        return useLockOnOpen;
    }

    public void setUseLockOnOpen(Short useLockOnOpen) {
        this.useLockOnOpen = useLockOnOpen;
    }

    @Column(name = "lock_on_open_delay", nullable = true)
    public Long getLockOnOpenDelay() {
        return lockOnOpenDelay;
    }

    public void setLockOnOpenDelay(Long lockOnOpenDelay) {
        this.lockOnOpenDelay = lockOnOpenDelay;
    }

    @Column(name = "osdp_pd_address", nullable = true)
    public Long getOsdpPdAddress() {
        return osdpPdAddress;
    }

    public void setOsdpPdAddress(Long osdpPdAddress) {
        this.osdpPdAddress = osdpPdAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DoorData doorData = (DoorData) o;
        return deviceOid == doorData.deviceOid &&
                Objects.equals(boardNum, doorData.boardNum) &&
                Objects.equals(nodeNum, doorData.nodeNum);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceOid, boardNum, nodeNum);
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Device.class, optional = false)
    @JoinColumn(name = "device_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    /*
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = Board.class, optional = false)
    @JoinColumn(name = "board_num", referencedColumnName = "board_number", nullable = false)
    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }
    */
}
