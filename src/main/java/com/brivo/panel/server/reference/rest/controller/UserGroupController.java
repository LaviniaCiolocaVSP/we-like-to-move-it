package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.auth.PanelPrincipal;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.usergroup.UserGroupData;
import com.brivo.panel.server.reference.service.UserGroupService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserGroupController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupController.class);

    @Autowired
    private UserGroupService userGroupService;

    /**
     * Get User Groups and Card Masks.
     */
    @RequestMapping(
            path = "/paneldata/usergroups",
            method = RequestMethod.GET
    )
    public UserGroupData getUserGroupData(@AuthenticationPrincipal PanelPrincipal panelPrincipal) {
        Panel panel = panelPrincipal.getPanel();
        // Panel panel = PanelBuilder.getMockPanel();

        LOGGER.info("Panel {} requesting updated user group data", panel.getElectronicSerialNumber());

        /*
        UserGroupData data;
        if (panel.getIsRegistered()) {
            data = userGroupService.getUserGroupData(panel);
        } else {
            data = new UserGroupData();
        }
        */
        // UserGroupData data = UserGroupBuilder.getMockUserGroup();
        UserGroupData data = userGroupService.getUserGroupData(panel);

        LOGGER.debug("Sending panel {} updated user group data", panel.getElectronicSerialNumber());
        //LOGGER.debug("Sending panel {} updated user group data [{}]", panel.getElectronicSerialNumber(),data);

        return data;
    }
}
