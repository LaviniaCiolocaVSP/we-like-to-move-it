package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "video_provider_type", schema = "brivo20", catalog = "onair")
public class VideoProviderType {
    private long videoProviderTypeId;
    private String name;
    private Short creatable;
    private Long featureId;
    private Collection<VideoCameraModelParameter> videoCameraModelParametersByVideoProviderTypeId;
    private Collection<VideoProvider> videoProvidersByVideoProviderTypeId;
    private Feature featureByFeatureId;
    private Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByVideoProviderTypeId;

    @Id
    @Column(name = "video_provider_type_id", nullable = false)
    public long getVideoProviderTypeId() {
        return videoProviderTypeId;
    }

    public void setVideoProviderTypeId(long videoProviderTypeId) {
        this.videoProviderTypeId = videoProviderTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "creatable", nullable = true)
    public Short getCreatable() {
        return creatable;
    }

    public void setCreatable(Short creatable) {
        this.creatable = creatable;
    }

    @Basic
    @Column(name = "feature_id", nullable = true)
    public Long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Long featureId) {
        this.featureId = featureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoProviderType that = (VideoProviderType) o;

        if (videoProviderTypeId != that.videoProviderTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (creatable != null ? !creatable.equals(that.creatable) : that.creatable != null) {
            return false;
        }
        return featureId != null ? featureId.equals(that.featureId) : that.featureId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoProviderTypeId ^ (videoProviderTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (creatable != null ? creatable.hashCode() : 0);
        result = 31 * result + (featureId != null ? featureId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "videoProviderTypeByVideoProviderTypeId")
    public Collection<VideoCameraModelParameter> getVideoCameraModelParametersByVideoProviderTypeId() {
        return videoCameraModelParametersByVideoProviderTypeId;
    }

    public void setVideoCameraModelParametersByVideoProviderTypeId(Collection<VideoCameraModelParameter> videoCameraModelParametersByVideoProviderTypeId) {
        this.videoCameraModelParametersByVideoProviderTypeId = videoCameraModelParametersByVideoProviderTypeId;
    }

    @OneToMany(mappedBy = "videoProviderTypeByVideoProviderTypeId")
    public Collection<VideoProvider> getVideoProvidersByVideoProviderTypeId() {
        return videoProvidersByVideoProviderTypeId;
    }

    public void setVideoProvidersByVideoProviderTypeId(Collection<VideoProvider> videoProvidersByVideoProviderTypeId) {
        this.videoProvidersByVideoProviderTypeId = videoProvidersByVideoProviderTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "feature_id", referencedColumnName = "feature_id", insertable = false, updatable = false)
    public Feature getFeatureByFeatureId() {
        return featureByFeatureId;
    }

    public void setFeatureByFeatureId(Feature featureByFeatureId) {
        this.featureByFeatureId = featureByFeatureId;
    }

    @OneToMany(mappedBy = "videoProviderTypeByVideoProviderTypeId")
    public Collection<VideoProviderTypeProperty> getVideoProviderTypePropertiesByVideoProviderTypeId() {
        return videoProviderTypePropertiesByVideoProviderTypeId;
    }

    public void setVideoProviderTypePropertiesByVideoProviderTypeId(Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByVideoProviderTypeId) {
        this.videoProviderTypePropertiesByVideoProviderTypeId = videoProviderTypePropertiesByVideoProviderTypeId;
    }
}
