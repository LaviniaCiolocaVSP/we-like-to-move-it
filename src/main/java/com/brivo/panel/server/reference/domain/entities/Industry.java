package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Industry {
    private long industryId;
    private String name;
    private String description;

    @Id
    @Column(name = "industry_id", nullable = false)
    public long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(long industryId) {
        this.industryId = industryId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 4000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Industry industry = (Industry) o;

        if (industryId != industry.industryId) {
            return false;
        }
        if (name != null ? !name.equals(industry.name) : industry.name != null) {
            return false;
        }
        return description != null ? description.equals(industry.description) : industry.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (industryId ^ (industryId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }
}
