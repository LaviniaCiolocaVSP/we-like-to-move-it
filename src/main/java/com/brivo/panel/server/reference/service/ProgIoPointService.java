package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.constants.Definitions;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.domain.repository.ProgIoPointsRepository;
import com.brivo.panel.server.reference.dto.ui.UIValidCredentialDTO;
import com.brivo.panel.server.reference.dto.ui.UiProgDeviceDTO;
import com.brivo.panel.server.reference.dto.ui.UiPhysicalPointDTO;
import com.brivo.panel.server.reference.dto.ui.UiSwitchDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ProgIoPointService {
    private final static Logger LOGGER = LoggerFactory.getLogger(ProgIoPointService.class);

    private final ProgIoPointsRepository progIoPointsRepository;

    private final static Long DEFAULT_DOOR_ID = 0L;
    private final static Short DEFAULT_EVENT_TYPE = 0;
    private final static Long DEFAULT_DISENGAGE_DELAY = 0L;
    private final static Boolean DEFAULT_REPORT_ENGAGE = true;
    private final static Boolean DEFAULT_REPORT_DISENGAGE = false;
    private final static String DEFAULT_ENGAGE_MESSAGE = "Device Engaged";
    private final static String DEFAULT_DISENGAGE_MESSAGE = "Device Disengaged";
    private final static Long DEFAULT_TWO_FACTOR_INTERVAL = 3L;

    @Autowired
    public ProgIoPointService(final ProgIoPointsRepository progIoPointsRepository) {
        this.progIoPointsRepository = progIoPointsRepository;
    }

    @Transactional
    public Optional<List<ProgIoPoints>> getProgIoPointsForBoard(final Long panelOid, final Short boardNumber,
                                                                final List<Integer> pointAddresses) {
        return progIoPointsRepository.getProgIoPointsForBoard(panelOid, boardNumber, pointAddresses);
    }

    public void useIoPointsForDoor(final int[] pointAddressesForNode, final Long panelOid, final Long boardNumber) {
        progIoPointsRepository.useIoPointsForDoor(pointAddressesForNode, panelOid, boardNumber, Definitions.IO_DOOR, Definitions.IO_FREE);
    }

    public Optional<List<ProgIoPoints>> getProgIoPointsForBoardByPanelOidAndBoardNumber(final Long panelOid, final Long boardNumber) {
        return progIoPointsRepository.getProgIoPointsForBoardByPanelOidAndBoardNumber(panelOid, boardNumber);
    }

    public Collection<UiPhysicalPointDTO> getAvailablePhysicalPointDTOSByPanelOid(final Long panelOid, final int pointType) {
        Collection<ProgIoPoints> inputProgIoPoints = progIoPointsRepository.getPhysicalPointsByPanelOid(panelOid, pointType, Definitions.IO_DEVICE, Definitions.IO_FREE)
                                                                           .orElse(Collections.emptyList());

        return inputProgIoPoints.stream()
                                .map(convertProgIoPointToUiPhysicalInput())
                                .collect(Collectors.toSet());
    }

    public Function<ProgIoPoints, UiPhysicalPointDTO> convertProgIoPointToUiPhysicalInput() {
        return progIoPoints -> new UiPhysicalPointDTO(progIoPoints.getName(), progIoPoints.getBoardNumber(), progIoPoints.getPointAddress(), progIoPoints.getType());
    }

    public Collection<UiPhysicalPointDTO> convertProgIoPointsToUiPhysicalInputs(final Collection<ProgIoPoints> progIoPointsCollection) {
        return progIoPointsCollection.stream()
                                     .map(convertProgIoPointToUiPhysicalInput())
                                     .collect(Collectors.toSet());
    }

    public UiSwitchDTO getUiSwitchDTOTemplateByPanelOid(final Long panelOid) {
        UiSwitchDTO uiSwitchDTO = new UiSwitchDTO(DEFAULT_DISENGAGE_DELAY, DEFAULT_REPORT_ENGAGE, DEFAULT_REPORT_DISENGAGE,
                DEFAULT_ENGAGE_MESSAGE, DEFAULT_DISENGAGE_MESSAGE);

        uiSwitchDTO.setPanelOid(panelOid);
        uiSwitchDTO = setAvailablePointsCollectionsForSwitch(uiSwitchDTO);

        return uiSwitchDTO;
    }

    public UiSwitchDTO setAvailablePointsCollectionsForSwitch(final UiSwitchDTO uiSwitchDTO) {
        final Long panelOid = uiSwitchDTO.getPanelOid();
        final Collection<UiPhysicalPointDTO> inputs = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.INPUT_POINT);
        final Collection<UiPhysicalPointDTO> outputs = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.OUTPUT_POINT);

        uiSwitchDTO.setAvailableInputs(inputs);
        uiSwitchDTO.setAvailableOutputs(outputs);

        return uiSwitchDTO;
    }

    public UiProgDeviceDTO setAvailableOutputPointsForProgDevice(final UiProgDeviceDTO uiProgDeviceDTO) {
        final Long panelOid = uiProgDeviceDTO.getPanelOid();
        final Collection<UiPhysicalPointDTO> outputs = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.OUTPUT_POINT);

        uiProgDeviceDTO.setAvailableOutputs(outputs);

        return uiProgDeviceDTO;
    }

    public UiProgDeviceDTO getUiProgDeviceDTOTemplateByPanelOid(final Long panelOid) {
        UiProgDeviceDTO uiProgDeviceDTO = new UiProgDeviceDTO(DEFAULT_DISENGAGE_DELAY, DEFAULT_REPORT_ENGAGE, DEFAULT_REPORT_DISENGAGE,
                DEFAULT_ENGAGE_MESSAGE, DEFAULT_DISENGAGE_MESSAGE, DEFAULT_DOOR_ID, DEFAULT_EVENT_TYPE);

        uiProgDeviceDTO.setPanelOid(panelOid);

        final Collection<UiPhysicalPointDTO> outputs = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.OUTPUT_POINT);
        uiProgDeviceDTO.setAvailableOutputs(outputs);

        return uiProgDeviceDTO;
    }

    @Transactional
    public void useProgIoPointsForDevice(final Long panelOid, final Collection<UiPhysicalPointDTO> points) {
        points.forEach(uiPhysicalPointDTO -> {
            progIoPointsRepository.useProgIoPointForDevice(panelOid, uiPhysicalPointDTO.getBoardNumber(),
                    uiPhysicalPointDTO.getPointAddress(), Definitions.IO_DEVICE, Definitions.IO_FREE);
        });
    }

    public Function<ProgDevicePuts, ProgIoPoints> convertProgDevicePutToProgIoPoint() {
        return progDevicePuts -> progIoPointsRepository.findByPanelOidAndBoardNumberAndPointAddress(
                progDevicePuts.getPanelOid(), progDevicePuts.getBoardNumber(), progDevicePuts.getPointAddress());
    }

    public Collection<ProgIoPoints> filterProgIoPointsByType(final Collection<ProgIoPoints> progIoPointsCollection, final long pointType) {
        return progIoPointsCollection.stream()
                                     .filter(progIoPoints -> progIoPoints.getType() == pointType)
                                     .collect(Collectors.toSet());
    }

    public ProgIoPoints getProgIoPointOfTypeFromCollectionOrThrow(final Long deviceOid, final Collection<ProgIoPoints> progIoPoints,
                                                                  final int progIoPointType) {
        return filterProgIoPointsByType(progIoPoints, progIoPointType).stream()
                                                                      .findFirst()
                                                                      .orElseThrow(() -> new IllegalArgumentException(
                                                                              "There aren't any input prog io points set for device " + deviceOid));
    }

    public UIValidCredentialDTO getUiValidCredentialDTOTemplateByPanelOid(final Long panelOid) {
        LOGGER.debug("Getting valid credential template for adding a new one to panel with object id {}", panelOid);
        UIValidCredentialDTO uiValidCredentialDTO = new UIValidCredentialDTO(DEFAULT_DISENGAGE_DELAY, DEFAULT_REPORT_ENGAGE, DEFAULT_REPORT_DISENGAGE,
                DEFAULT_ENGAGE_MESSAGE, DEFAULT_DISENGAGE_MESSAGE);

        uiValidCredentialDTO.setDoorId(DEFAULT_DOOR_ID);
        uiValidCredentialDTO.setEventTypeId(DEFAULT_EVENT_TYPE);
        uiValidCredentialDTO.setPanelOid(panelOid);
        uiValidCredentialDTO.setTwoFactorInterval(DEFAULT_TWO_FACTOR_INTERVAL);
        uiValidCredentialDTO = setAvailablePointsCollectionsForValidCredential(uiValidCredentialDTO);

        LOGGER.debug("Successfully retrieved valid credential template {}", uiValidCredentialDTO.toString());
        return uiValidCredentialDTO;
    }

    public UIValidCredentialDTO setAvailablePointsCollectionsForValidCredential(final UIValidCredentialDTO uiValidCredentialDTO) {
        final Long panelOid = uiValidCredentialDTO.getPanelOid();
        final Collection<UiPhysicalPointDTO> readers = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.READER_POINT);
        final Collection<UiPhysicalPointDTO> outputs = getAvailablePhysicalPointDTOSByPanelOid(panelOid, Definitions.OUTPUT_POINT);

        uiValidCredentialDTO.setAvailableReaders(readers);
        uiValidCredentialDTO.setAvailableOutputs(outputs);

        return uiValidCredentialDTO;
    }

}