package com.brivo.panel.server.reference.dao.usergroup;

import com.brivo.panel.server.reference.model.usergroup.UserGroup;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class UserGroupExtractor implements ResultSetExtractor<List<UserGroup>> {
    private static final String BMP_CRED_TYPE_NAME = "Brivo Mobile Pass";
    private static final int PIN_CREDENTIAL_TYPE_ID_MAGIC_NUMBER_CEILING = 10;

    @Override
    public List<UserGroup> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<UserGroup> list = new ArrayList<>();

        Long prevUserGroupId = null;
        List<Long> userIdList = null;
        UserGroup currentGroup;
        while (rs.next()) {
            Long userGroupId = rs.getLong("group_object_id");

            // Check for null since it's an outer join (a group may not have any user/members in it)
            Long userId = rs.getLong("user_object_id");
            if (rs.wasNull()) {
                userId = null;
            }

            // Since the SQL is ordered by Group, we know we are starting a new UserGroup object here
            if (!userGroupId.equals(prevUserGroupId)) {
                currentGroup = new UserGroup();
                userIdList = new ArrayList<>();
                currentGroup.setUserIds(userIdList);

                currentGroup.setUserGroupId(userGroupId);
                currentGroup.setAntiPassbackResetTime(rs.getInt("reset_time"));
                currentGroup.setImmuneToAntipassback(rs.getBoolean("immunity"));

                list.add(currentGroup);
            }

            prevUserGroupId = userGroupId;

            if (userId != null) {
                userIdList.add(userId);  // userIdList can never be null if we get here
            }
        }

        return list;
    }
}
