package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class Feature {
    private long featureId;
    private String name;
    private Short enabled;
    private Collection<FeatureOverride> featureOverridesByFeatureId;
    private Collection<Menu> menusByFeatureId;
    private Collection<OvrCameraSubscription> ovrCameraSubscriptionsByFeatureId;
    private Collection<VideoProviderType> videoProviderTypesByFeatureId;

    @Id
    @Column(name = "feature_id", nullable = false)
    public long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(long featureId) {
        this.featureId = featureId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "enabled", nullable = true)
    public Short getEnabled() {
        return enabled;
    }

    public void setEnabled(Short enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Feature feature = (Feature) o;

        if (featureId != feature.featureId) {
            return false;
        }
        if (name != null ? !name.equals(feature.name) : feature.name != null) {
            return false;
        }
        return enabled != null ? enabled.equals(feature.enabled) : feature.enabled == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (featureId ^ (featureId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "featureByFeatureId")
    public Collection<FeatureOverride> getFeatureOverridesByFeatureId() {
        return featureOverridesByFeatureId;
    }

    public void setFeatureOverridesByFeatureId(Collection<FeatureOverride> featureOverridesByFeatureId) {
        this.featureOverridesByFeatureId = featureOverridesByFeatureId;
    }

    @OneToMany(mappedBy = "featureByFeatureId")
    public Collection<Menu> getMenusByFeatureId() {
        return menusByFeatureId;
    }

    public void setMenusByFeatureId(Collection<Menu> menusByFeatureId) {
        this.menusByFeatureId = menusByFeatureId;
    }

    @OneToMany(mappedBy = "featureByFeatureId")
    public Collection<OvrCameraSubscription> getOvrCameraSubscriptionsByFeatureId() {
        return ovrCameraSubscriptionsByFeatureId;
    }

    public void setOvrCameraSubscriptionsByFeatureId(Collection<OvrCameraSubscription> ovrCameraSubscriptionsByFeatureId) {
        this.ovrCameraSubscriptionsByFeatureId = ovrCameraSubscriptionsByFeatureId;
    }

    @OneToMany(mappedBy = "featureByFeatureId")
    public Collection<VideoProviderType> getVideoProviderTypesByFeatureId() {
        return videoProviderTypesByFeatureId;
    }

    public void setVideoProviderTypesByFeatureId(Collection<VideoProviderType> videoProviderTypesByFeatureId) {
        this.videoProviderTypesByFeatureId = videoProviderTypesByFeatureId;
    }
}
