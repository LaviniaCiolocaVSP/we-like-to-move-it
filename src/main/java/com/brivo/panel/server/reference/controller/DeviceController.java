package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UIValidCredentialDTO;
import com.brivo.panel.server.reference.dto.ui.UiDeviceDTO;
import com.brivo.panel.server.reference.dto.ui.UiDoorDTO;
import com.brivo.panel.server.reference.dto.ui.UiProgDeviceDTO;
import com.brivo.panel.server.reference.dto.ui.UiSwitchDTO;
import com.brivo.panel.server.reference.service.ProgIoPointService;
import com.brivo.panel.server.reference.service.UiDeviceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("/device")
public class DeviceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceController.class);

    private final UiDeviceService deviceService;
    private final ProgIoPointService progIoPointService;

    @Autowired
    public DeviceController(final UiDeviceService deviceService, final ProgIoPointService progIoPointService) {
        this.deviceService = deviceService;
        this.progIoPointService = progIoPointService;
    }

    @GetMapping("/doors/{accountId}")
    public Collection<UiDoorDTO> getDoorsForAccount(@PathVariable final long accountId) {
        return deviceService.getUiDoorDtosForAccount(accountId);
    }

    @GetMapping("/doors/ofPanel/{panelOid}")
    public Collection<UiDoorDTO> getDoorsForPanel(@PathVariable final long panelOid) {
        return deviceService.getUiDoorDTOSForPanel(panelOid);
    }

    @DeleteMapping(path = "/doors", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteDoorsById(@RequestBody List<Long> doorIds) {
        return deviceService.deleteDoorsByIds(doorIds);
    }

    @DeleteMapping(path = "/devices", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteDevicesById(@RequestBody List<Long> deviceIds) {
        return deviceService.deleteDevicesByIds(deviceIds);
    }

    @PostMapping(path = "/door", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO createDoor(@RequestBody final UiDoorDTO uiDoorDTO) {
        return deviceService.createDoor(uiDoorDTO);
    }

    @PutMapping(path = "/door", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updateDoor(@RequestBody final UiDoorDTO uiDoorDTO) {
        return deviceService.updateDoor(uiDoorDTO);
    }

    @GetMapping("/{accountId}")
    public Collection<UiDeviceDTO> get(@PathVariable final long accountId) {
        return deviceService.getUiDeviceDtosForAccount(accountId);
    }

    @GetMapping("/switchTemplate/{panelOid}")
    public UiSwitchDTO getUiSwitchDTOTemplateByPanelOid(@PathVariable final long panelOid) {
        return progIoPointService.getUiSwitchDTOTemplateByPanelOid(panelOid);
    }

    @GetMapping("/switch/{deviceId}")
    public UiSwitchDTO getUiSwitchDTOByDeviceId(@PathVariable final long deviceId) {
        return deviceService.getUiSwitchDTOByDeviceId(deviceId);
    }

    @PostMapping(path = "/switch", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO createSwitch(@RequestBody final UiSwitchDTO uiSwitchDTO) {
        return deviceService.createSwitch(uiSwitchDTO);
    }

    @PutMapping(path = "/switch", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updateSwitch(@RequestBody final UiSwitchDTO uiSwitchDTO) {
        deviceService.prepareDeviceUpdate(uiSwitchDTO.getDeviceId());
        return deviceService.updateSwitch(uiSwitchDTO);
    }

    @GetMapping("/eventTrackTemplate/{panelOid}")
    public UiProgDeviceDTO getUiEventTrackDTOTemplateByPanelOid(@PathVariable final long panelOid) {
        return progIoPointService.getUiProgDeviceDTOTemplateByPanelOid(panelOid);
    }

    @PostMapping(path = "/eventTrack", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO createEventTrack(@RequestBody final UiProgDeviceDTO uiEventTrackDTO) {
        return deviceService.createEventTrack(uiEventTrackDTO);
    }

    @GetMapping("/eventTrack/{deviceId}")
    public UiProgDeviceDTO getUiEventTrackDTOByDeviceId(@PathVariable final long deviceId) {
        return deviceService.getUProgDeviceDTOByDeviceId(deviceId);
    }

    @PutMapping(path = "/eventTrack", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updateEventTrack(@RequestBody final UiProgDeviceDTO uiProgDeviceDTO) {
        deviceService.prepareDeviceUpdate(uiProgDeviceDTO.getDeviceId());
        return deviceService.updateProgDevice(uiProgDeviceDTO);
    }

    @GetMapping("/timerTemplate/{panelOid}")
    public UiProgDeviceDTO getUiTimerDTOTemplateByPanelOid(@PathVariable final long panelOid) {
        return progIoPointService.getUiProgDeviceDTOTemplateByPanelOid(panelOid);
    }

    @GetMapping("/timer/{deviceId}")
    public UiProgDeviceDTO getUiTimerDTOByDeviceId(@PathVariable final long deviceId) {
        return deviceService.getUProgDeviceDTOByDeviceId(deviceId);
    }

    @PostMapping(path = "/timer", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO createTimer(@RequestBody final UiProgDeviceDTO uiProgDeviceDTO) {
        return deviceService.createTimer(uiProgDeviceDTO);
    }

    @PutMapping(path = "/timer", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updateTimer(@RequestBody final UiProgDeviceDTO uiTimerDTO) {
        deviceService.prepareDeviceUpdate(uiTimerDTO.getDeviceId());
        return deviceService.updateProgDevice(uiTimerDTO);
    }

    @GetMapping("/validCredentialTemplate/{panelOid}")
    public UIValidCredentialDTO getUiValidCredentialDTOTemplateByPanelOid(@PathVariable final long panelOid) {
        return progIoPointService.getUiValidCredentialDTOTemplateByPanelOid(panelOid);
    }

    @PostMapping(path = "/validCredential", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO createValidCredential(@RequestBody final UIValidCredentialDTO uiValidCredentialDTO) {
        return deviceService.createValidCredential(uiValidCredentialDTO);
    }

    @PutMapping(path = "/validCredential", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO updateSwitch(@RequestBody final UIValidCredentialDTO uiValidCredentialDTO) {
        deviceService.prepareDeviceUpdate(uiValidCredentialDTO.getDeviceId());
        return deviceService.updateValidCredential(uiValidCredentialDTO);
    }

    @GetMapping("/validCredential/{deviceId}")
    public UIValidCredentialDTO getUiValidCredentialDTOByDeviceId(@PathVariable final long deviceId) {
        return deviceService.getUiValidCredentialDTOByDeviceId(deviceId);
    }
}
