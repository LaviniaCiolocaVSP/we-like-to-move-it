package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "viewable_groups_v", schema = "brivo20", catalog = "onair")
public class ViewableGroupsV {
    private Long actorObjectId;
    private Integer read;
    private Integer write;
    private Integer activateDevices;
    private Integer append;
    private Long securityGroupId;
    private Long objectId;
    private Long securityGroupTypeId;
    private Long parentId;
    private Long accountId;
    private String name;
    private String description;
    private Short disabled;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "read", nullable = true)
    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }

    @Basic
    @Column(name = "write", nullable = true)
    public Integer getWrite() {
        return write;
    }

    public void setWrite(Integer write) {
        this.write = write;
    }

    @Basic
    @Column(name = "activate_devices", nullable = true)
    public Integer getActivateDevices() {
        return activateDevices;
    }

    public void setActivateDevices(Integer activateDevices) {
        this.activateDevices = activateDevices;
    }

    @Basic
    @Column(name = "append", nullable = true)
    public Integer getAppend() {
        return append;
    }

    public void setAppend(Integer append) {
        this.append = append;
    }

    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "object_id", nullable = true)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "security_group_type_id", nullable = true)
    public Long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public void setSecurityGroupTypeId(Long securityGroupTypeId) {
        this.securityGroupTypeId = securityGroupTypeId;
    }

    @Basic
    @Column(name = "parent_id", nullable = true)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 35)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "disabled", nullable = true)
    public Short getDisabled() {
        return disabled;
    }

    public void setDisabled(Short disabled) {
        this.disabled = disabled;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ViewableGroupsV that = (ViewableGroupsV) o;
        return Objects.equals(actorObjectId, that.actorObjectId) &&
                Objects.equals(read, that.read) &&
                Objects.equals(write, that.write) &&
                Objects.equals(activateDevices, that.activateDevices) &&
                Objects.equals(append, that.append) &&
                Objects.equals(securityGroupId, that.securityGroupId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(actorObjectId, read, write, activateDevices, append, securityGroupId);
    }
}
