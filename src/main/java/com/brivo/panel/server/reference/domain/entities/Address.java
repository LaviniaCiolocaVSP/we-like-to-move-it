package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class Address {
    private long addressId;
    private Long ownerObjectId;
    private String firstName;
    private String lastName;
    private String addressType;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String poBox;
    private String country;
    private String timeZone;
    private String notes;
    private Timestamp created;
    private Timestamp updated;
    private BrivoObject objectByOwnerBrivoObjectId;

    @Id
    @Column(name = "address_id", nullable = false)
    public long getAddressId() {
        return addressId;
    }

    public void setAddressId(long addressId) {
        this.addressId = addressId;
    }

    @Column(name = "owner_object_id", nullable = true)
    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Column(name = "first_name", nullable = true, length = 30)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Column(name = "last_name", nullable = true, length = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Column(name = "address_type", nullable = true, length = 20)
    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    @Column(name = "address1", nullable = true, length = 64)
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Column(name = "address2", nullable = true, length = 64)
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Column(name = "city", nullable = true, length = 64)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Column(name = "state", nullable = true, length = 32)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name = "zip", nullable = true, length = 10)
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Column(name = "po_box", nullable = true, length = 10)
    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    @Column(name = "country", nullable = true, length = 32)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "notes", nullable = true, length = 4000)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Address address = (Address) o;

        if (addressId != address.addressId) {
            return false;
        }
        if (ownerObjectId != null ? !ownerObjectId.equals(address.ownerObjectId) : address.ownerObjectId != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(address.firstName) : address.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(address.lastName) : address.lastName != null) {
            return false;
        }
        if (addressType != null ? !addressType.equals(address.addressType) : address.addressType != null) {
            return false;
        }
        if (address1 != null ? !address1.equals(address.address1) : address.address1 != null) {
            return false;
        }
        if (address2 != null ? !address2.equals(address.address2) : address.address2 != null) {
            return false;
        }
        if (city != null ? !city.equals(address.city) : address.city != null) {
            return false;
        }
        if (state != null ? !state.equals(address.state) : address.state != null) {
            return false;
        }
        if (zip != null ? !zip.equals(address.zip) : address.zip != null) {
            return false;
        }
        if (poBox != null ? !poBox.equals(address.poBox) : address.poBox != null) {
            return false;
        }
        if (country != null ? !country.equals(address.country) : address.country != null) {
            return false;
        }
        if (timeZone != null ? !timeZone.equals(address.timeZone) : address.timeZone != null) {
            return false;
        }
        if (notes != null ? !notes.equals(address.notes) : address.notes != null) {
            return false;
        }
        if (created != null ? !created.equals(address.created) : address.created != null) {
            return false;
        }
        return updated != null ? updated.equals(address.updated) : address.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (addressId ^ (addressId >>> 32));
        result = 31 * result + (ownerObjectId != null ? ownerObjectId.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (addressType != null ? addressType.hashCode() : 0);
        result = 31 * result + (address1 != null ? address1.hashCode() : 0);
        result = 31 * result + (address2 != null ? address2.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (zip != null ? zip.hashCode() : 0);
        result = 31 * result + (poBox != null ? poBox.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        result = 31 * result + (timeZone != null ? timeZone.hashCode() : 0);
        result = 31 * result + (notes != null ? notes.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByOwnerBrivoObjectId() {
        return objectByOwnerBrivoObjectId;
    }

    public void setObjectByOwnerBrivoObjectId(BrivoObject objectByOwnerBrivoObjectId) {
        this.objectByOwnerBrivoObjectId = objectByOwnerBrivoObjectId;
    }
}
