package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class Rs485SettingsPK implements Serializable {
    private long brainId;
    private long port;

    @Column(name = "brain_id", nullable = false, insertable = false, updatable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Column(name = "port", nullable = false, insertable = false, updatable = false)
    public long getPort() {
        return port;
    }

    public void setPort(long port) {
        this.port = port;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Rs485SettingsPK that = (Rs485SettingsPK) o;

        if (brainId != that.brainId) {
            return false;
        }
        return port == that.port;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainId ^ (brainId >>> 32));
        result = 31 * result + (int) (port ^ (port >>> 32));
        return result;
    }
}
