package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prog_device_puts", schema = "brivo20", catalog = "onair")
@IdClass(ProgDevicePutsPK.class)
public class ProgDevicePuts {
    private long deviceOid;
    private long panelOid;
    private long boardNumber;
    private Long pointAddress;
    private Long behavior;
    private Long argument;
    private Device deviceByObjectId;
    private Board board;
    private Brain brainByObjectId;

    @Override
    public String toString() {
        return "ProgDevicePuts{" +
                "deviceOid=" + deviceOid +
                ", panelOid=" + panelOid +
                ", boardNumber=" + boardNumber +
                ", pointAddress=" + pointAddress +
                ", behavior=" + behavior +
                ", argument=" + argument +
                ", deviceByObjectId=" + deviceByObjectId +
                ", board=" + board +
                ", brainByObjectId=" + brainByObjectId +
                '}';
    }

    @Id
    @Column(name = "device_oid", nullable = false)
    public long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(long deviceOid) {
        this.deviceOid = deviceOid;
    }

    @Id
    @Column(name = "panel_oid", nullable = false)
    public long getPanelOid() {
        return panelOid;
    }

    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }

    @Id
    @Column(name = "board_number", nullable = false)
    public long getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(long boardNumber) {
        this.boardNumber = boardNumber;
    }

    @Id
    @Column(name = "point_address", nullable = false)
    public Long getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Long pointAddress) {
        this.pointAddress = pointAddress;
    }

    @Column(name = "behavior", nullable = true)
    public Long getBehavior() {
        return behavior;
    }

    public void setBehavior(Long behavior) {
        this.behavior = behavior;
    }

    @Column(name = "argument", nullable = true)
    public Long getArgument() {
        return argument;
    }

    public void setArgument(Long argument) {
        this.argument = argument;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgDevicePuts that = (ProgDevicePuts) o;

        if (deviceOid != that.deviceOid) {
            return false;
        }
        if (panelOid != that.panelOid) {
            return false;
        }
        if (boardNumber != that.boardNumber) {
            return false;
        }
        if (pointAddress != that.pointAddress) {
            return false;
        }
        if (behavior != null ? !behavior.equals(that.behavior) : that.behavior != null) {
            return false;
        }
        return argument != null ? argument.equals(that.argument) : that.argument == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceOid ^ (deviceOid >>> 32));
        result = 31 * result + (int) (panelOid ^ (panelOid >>> 32));
        result = 31 * result + (int) (boardNumber ^ (boardNumber >>> 32));
        result = 31 * result + (int) (pointAddress ^ (pointAddress >>> 32));
        result = 31 * result + (behavior != null ? behavior.hashCode() : 0);
        result = 31 * result + (argument != null ? argument.hashCode() : 0);
        return result;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "device_oid", referencedColumnName = "object_id", insertable = false, updatable = false)
    public Device getDeviceByObjectId() {
        return deviceByObjectId;
    }

    public void setDeviceByObjectId(Device deviceByObjectId) {
        this.deviceByObjectId = deviceByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "panel_oid", referencedColumnName = "panel_oid", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "board_number", referencedColumnName = "board_number", nullable = false, insertable = false, updatable = false)
    })
    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }


    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "panel_oid", referencedColumnName = "object_id", insertable = false, updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }

}
