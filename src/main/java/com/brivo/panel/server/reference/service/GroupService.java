package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.group.GroupDao;
import com.brivo.panel.server.reference.domain.common.IObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.common.ISecurityGroupAntipassbackRepository;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupAntipassback;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupType;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupTypes;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.BrivoObjectRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceRepository;
import com.brivo.panel.server.reference.domain.repository.GroupRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupAntipassbackRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupMemberRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupTypeRepository;
import com.brivo.panel.server.reference.dto.GroupDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ObjectPermissionForGroupDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupAntipassbackDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupUpdateModelDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiSiteDTO;
import com.brivo.panel.server.reference.model.event.SecurityAction;
import com.brivo.panel.server.reference.service.util.ConverterUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_SHORT_VALUE;


@Service
public class GroupService {
    private static final Logger LOGGER = LoggerFactory.getLogger(GroupService.class);

    private final ObjectPermissionRepository objectPermissionRepository;
    private final BrivoObjectRepository brivoObjectRepository;
    private final SecurityGroupTypeRepository securityGroupTypeRepository;
    private final SecurityGroupMemberRepository securityGroupMemberRepository;
    private final SecurityGroupAntipassbackRepository securityGroupAntipassbackRepository;
    private final UiScheduleService scheduleService;
    private final DeviceRepository deviceRepository;
    private final AccountRepository accountRepository;
    private final GroupDao groupDao;
    private final GroupRepository groupRepository;
    private final UiObjectService objectService;

    public static final Long ALL_SITES_IDENTIFIER = -1L;
    public static final Long CURRENT_PRIVILEGES_IDENTIFIER = -2L;

    @Autowired
    public GroupService(ObjectPermissionRepository objectPermissionRepository,
                        GroupRepository groupRespository, BrivoObjectRepository brivoObjectRepository,
                        SecurityGroupTypeRepository securityGroupTypeRepository, AccountRepository accountRepository,
                        GroupDao groupDao, final UiObjectService objectService,
                        final SecurityGroupMemberRepository securityGroupMemberRepository,
                        UiScheduleService scheduleService,
                        DeviceRepository deviceRepository, SecurityGroupAntipassbackRepository securityGroupAntipassbackRepository) {
        this.groupRepository = groupRespository;
        this.brivoObjectRepository = brivoObjectRepository;
        this.securityGroupTypeRepository = securityGroupTypeRepository;
        this.accountRepository = accountRepository;
        this.groupDao = groupDao;
        this.objectService = objectService;
        this.securityGroupMemberRepository = securityGroupMemberRepository;
        this.objectPermissionRepository = objectPermissionRepository;
        this.deviceRepository = deviceRepository;
        this.scheduleService = scheduleService;
        this.securityGroupAntipassbackRepository = securityGroupAntipassbackRepository;
    }

    public SecurityGroup getSiteNotRootForDevice(final Long deviceObjectId) {
        return getSecurityGroupMembersForObject(deviceObjectId).stream()
                                                               .filter(securityGroupMember -> securityGroupMember.getSecurityGroupBySecurityGroupId()
                                                                                                                 .getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId() == SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID())
                                                               .map(securityGroupMember -> securityGroupMember.getSecurityGroupBySecurityGroupId())
                                                               .findFirst()
                                                               .orElse(getNotAvailableSecurityGroup());
    }

    private SecurityGroup getNotAvailableSecurityGroup() {
        SecurityGroup securityGroup = new SecurityGroup();

        securityGroup.setSecurityGroupId(NOT_AVAILABLE_LONG_VALUE);
        securityGroup.setName("");

        return securityGroup;
    }

    @Transactional
    public void addDeviceToSite(final Long deviceObjectId, final Long siteId) {
        LOGGER.debug("Adding device with object id {} to site with id {}", deviceObjectId, siteId);

        SecurityGroupMember securityGroupMember = new SecurityGroupMember();

        securityGroupMember.setObjectId(deviceObjectId);
        securityGroupMember.setSecurityGroupId(siteId);

        securityGroupMemberRepository.save(securityGroupMember);
    }

    private Collection<SecurityGroupMember> getSecurityGroupMembersForObject(final long objectId) {
        return securityGroupMemberRepository.findByObjectId(objectId)
                                            .orElse(Collections.emptyList());
    }

    @Transactional
    public Collection<SecurityGroup> getSitesByAccountId(final long accountId) {
        return groupRepository.findByAccountId(accountId)
                              .stream()
                              .filter(isValidDeviceGroup())
                              .collect(Collectors.toSet());
    }

    @Transactional
    public Collection<UiSiteDTO> getUiSiteDTOSForAccount(final long accountId) {
        return getSitesByAccountId(accountId).stream()
                                             .map(convertUiSiteDTO())
                                             .collect(Collectors.toSet());
    }

    @Transactional
    public Collection<UiSiteDTO> getUISiteDTOSForAccountForEditGroup(final long accountId) {
        Collection<UiSiteDTO> sites = getSitesByAccountId(accountId).stream()
                                                                    .map(convertUiSiteDTO())
                                                                    .collect(Collectors.toList());


        Collection<UiSiteDTO> allSitesForEditGroup = new ArrayList<>();

        //FIXME - for now, current privileges and all sites are not available
        allSitesForEditGroup.add(new UiSiteDTO(CURRENT_PRIVILEGES_IDENTIFIER, CURRENT_PRIVILEGES_IDENTIFIER, "Current privileges"));
//        allSitesForEditGroup.add(new UiSiteDTO(ALL_SITES_IDENTIFIER, ALL_SITES_IDENTIFIER, "All sites"));

        ((ArrayList<UiSiteDTO>) allSitesForEditGroup).addAll(sites);

        return allSitesForEditGroup;
    }

    private Function<SecurityGroup, UiSiteDTO> convertUiSiteDTO() {
        return securityGroup -> new UiSiteDTO(securityGroup.getSecurityGroupId(),
                                              securityGroup.getObjectByObjectId().getObjectId(), securityGroup.getName());
    }

    public Collection<GroupDTO> getGroupDTOS(final Collection<SecurityGroup> groups,
                                             final ISecurityGroupAntipassbackRepository dynamicSecurityGroupAntipassbackRepository,
                                             final IObjectPermissionRepository dynamicObjectPermissionRepository) {
        return groups.stream()
                     .map(convertGroup(dynamicSecurityGroupAntipassbackRepository, dynamicObjectPermissionRepository))
                     .collect(Collectors.toSet());
    }

    private Function<SecurityGroup, GroupDTO> convertGroup(final ISecurityGroupAntipassbackRepository dynamicSecurityGroupAntipassbackRepository,
                                                           final IObjectPermissionRepository dynamicObjectPermissionRepository) {
        return group -> {
            long securityGroupId = group.getSecurityGroupId();
            final SecurityGroupAntipassback securityGroupAntipassback = getSecurityGroupAntipassback(securityGroupId,
                                                                                                     dynamicSecurityGroupAntipassbackRepository);

            long groupObjectId = group.getObjectByObjectId().getObjectId();
            final Collection<ObjectPermissionForGroupDTO> scheduleDevicePairs = getObjectPermissionsForGroup(group,
                                                                                                             dynamicObjectPermissionRepository);

            return new GroupDTO(groupObjectId, securityGroupId, group.getName(),
                                group.getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId(),
                                group.getParentId(), securityGroupAntipassback.getImmunity(), securityGroupAntipassback.getResetTime(),
                                scheduleDevicePairs, group.getLockdown(), group.getDeleted(), group.getDisabled());
        };
    }

    private Collection<ObjectPermissionForGroupDTO> getObjectPermissionsForGroup(final SecurityGroup group,
                                                                                 final IObjectPermissionRepository dynamicObjectPermissionRepository) {
        if (group.getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId() == SecurityGroupTypes.ACCOUNT_ROOT_GROUP.getSecurityGroupTypeID())
            return dynamicObjectPermissionRepository.findKeypadLockObjectPermissions(group.getObjectId(), SecurityAction.ACTION_LOCK_KEYPAD.getSecurityActionValue())
                                                    .orElse(Collections.emptyList())
                                                    .stream()
                                                    .map(convertObjectPermission())
                                                    .collect(Collectors.toList());
        else
            return dynamicObjectPermissionRepository.findByGroupObjectId(group.getObjectId())
                                                    .map(objectPermissions -> objectPermissions.stream()
                                                                                               .map(convertObjectPermission())
                                                                                               .collect(Collectors.toList()))
                                                    .orElse(Collections.emptyList());
    }

    private Function<ObjectPermission, ObjectPermissionForGroupDTO> convertObjectPermission() {
        return objectPermission -> new ObjectPermissionForGroupDTO(objectPermission.getSecurityActionId(),
                                                                   objectPermission.getObjectId(), objectPermission.getScheduleByScheduleId().getScheduleId(),
                                                                   objectPermission.getActorObjectId(), objectPermission.getIgnoreLockdown()
        );
    }

    private SecurityGroupAntipassback getSecurityGroupAntipassback(final long securityGroupId,
                                                                   final ISecurityGroupAntipassbackRepository dynamicSecurityGroupAntipassbackrepository) {
        return dynamicSecurityGroupAntipassbackrepository.findBySecurityGroupId(securityGroupId)
                                                         .orElse(getNotAvailableSecurityGroupAntipassback());
    }

    private SecurityGroupAntipassback getNotAvailableSecurityGroupAntipassback() {
        final SecurityGroupAntipassback securityGroupAntipassback = new SecurityGroupAntipassback();
        securityGroupAntipassback.setImmunity(NOT_AVAILABLE_SHORT_VALUE);
        securityGroupAntipassback.setResetTime(NOT_AVAILABLE_LONG_VALUE);
        return securityGroupAntipassback;
    }

    Comparator<UiGroupDTO> uiGroupComparator = new Comparator<UiGroupDTO>() {
        @Override
        public int compare(UiGroupDTO o1, UiGroupDTO o2) {
            return o2.getGroupId().compareTo(o1.getGroupId());
        }
    };

    public Set<UiGroupDTO> getUiDeviceGroupDTOS(final Collection<SecurityGroup> groups) {
        return groups.stream()
                     .filter(isValidDeviceGroup())
                     .map(convertGroupUIDropdown())
                     .collect(Collectors.toSet());
    }

    public Set<UiGroupDTO> getUiUsersGroupDTOS(final Collection<SecurityGroup> groups) {
        return groups.stream()
                     .filter(isValidUserGroup())
                     .map(convertGroupUIDropdown())
                     .collect(Collectors.toSet());
    }

    public Predicate<SecurityGroup> isValidUserGroup() {
        return securityGroup -> {
            final SecurityGroupType securityGroupType = securityGroup.getSecurityGroupTypeBySecurityGroupTypeId();
            return securityGroupType.getSecurityGroupTypeId() == SecurityGroupTypes.USER_GROUP.getSecurityGroupTypeID()
                   && securityGroup.getDeleted() == 0;
        };
    }

    public Predicate<SecurityGroup> isUserGroup() {
        return securityGroup -> {
            final SecurityGroupType securityGroupType = securityGroup.getSecurityGroupTypeBySecurityGroupTypeId();
            return securityGroupType.getSecurityGroupTypeId() != SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID();
        };
    }

    private Predicate<SecurityGroup> isValidDeviceGroup() {
        return securityGroup -> {
            final SecurityGroupType securityGroupType = securityGroup.getSecurityGroupTypeBySecurityGroupTypeId();
            return securityGroupType.getSecurityGroupTypeId() == SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID()
                   && securityGroup.getDeleted() == 0;
        };
    }

    private Function<SecurityGroup, UiGroupDTO> convertGroupUIDropdown() {
        return group -> new UiGroupDTO(group.getObjectByObjectId().getObjectId(), group.getName());
    }

    private Function<UiGroupDTO, UiGroupDTO> addDetailsToGroup(final Long accountId) {
        return group -> {
            final SecurityGroupAntipassback securityGroupAntipassback = securityGroupAntipassbackRepository.findBySecurityGroupId(group.getGroupId())
                                                                                                           .orElse(null);
            final UiGroupAntipassbackDTO uiGroupAntipassbackDTO = convertSecurityGroupAntipassback().apply(securityGroupAntipassback);
            group.setUiGroupAntipassbackDTO(uiGroupAntipassbackDTO);

            final Optional<ObjectPermission> keypadUnlockHoldPrivilege = objectPermissionRepository.findKeypadUnlockHoldPrivilege(group.getObjectId(), SecurityAction.ACTION_LOCK_KEYPAD.getSecurityActionValue());
            group.setKeypadUnlockHoldPrivileges(keypadUnlockHoldPrivilege.isPresent());

            group.setAccountId(accountId);

            return group;
        };
    }

    private Function<ObjectPermission, UiGroupPermissionDTO> convertGroupPermission(Collection<UiScheduleSummaryDTO> schedulesForSite) {
        return groupPermission -> {
            LOGGER.debug("Converting group permission: {}", groupPermission.toString());
            final Device device = groupPermission.getObjectByBrivoObjectId().getDeviceByObjectId();
            final UiSiteDTO site = findSiteForDevice(device.getObjectId());

//            final Collection<UiScheduleSummaryDTO> schedulesForSite = availableSchedulesMap.get(siteObjectId);
//            LOGGER.debug("SchedulesForSite {}:", siteObjectId);
            schedulesForSite.forEach(schedule -> LOGGER.debug(schedule.toString()));

            //FIXME

            final Optional<UiScheduleSummaryDTO> selectedSchedule = schedulesForSite.stream()
                                                                                    .filter(uiScheduleSummaryDTO -> uiScheduleSummaryDTO.getScheduleId() != null &&
                                                                                            uiScheduleSummaryDTO.getScheduleId() == groupPermission.getScheduleId())
                                                                                    .findFirst();
            String deviceType = deviceRepository.findDeviceTypeDescriptionById(device.getDeviceTypeId());
            return new UiGroupPermissionDTO(site.getSiteName(), groupPermission.getScheduleId(), groupPermission.getSecurityActionId(),
                                            device.getDeviceId(), groupPermission.getObjectId(), device.getName(), deviceType,
                                            selectedSchedule.get(), schedulesForSite);
        };

    }

    @Transactional
    public Function<ObjectPermission, UiGroupPermissionDTO> convertCurrentPrivilege(Map<Long, Collection<UiScheduleSummaryDTO>> availableSchedulesMap) {
        return groupPermission -> {
            LOGGER.debug("Converting current privilege: {}", groupPermission.toString());
            final Device device = groupPermission.getObjectByBrivoObjectId().getDeviceByObjectId();
            final UiSiteDTO site = findSiteForDevice(device.getObjectId());

            final Long siteObjectId = site.getSiteObjectId();
            final Collection<UiScheduleSummaryDTO> schedulesForSite = availableSchedulesMap.get(siteObjectId);
            LOGGER.debug("SchedulesForSite {}:", siteObjectId);
            schedulesForSite.forEach(schedule -> LOGGER.debug(schedule.toString()));

            final Optional<UiScheduleSummaryDTO> selectedSchedule = schedulesForSite.stream()
                                                                                    .filter(uiScheduleSummaryDTO -> uiScheduleSummaryDTO.getScheduleId() != null &&
                                                                                            uiScheduleSummaryDTO.getScheduleId() == groupPermission.getScheduleId())
                                                                                    .findFirst();

            String deviceType = deviceRepository.findDeviceTypeDescriptionById(device.getDeviceTypeId());
            return new UiGroupPermissionDTO(site.getSiteName(), groupPermission.getScheduleId(), groupPermission.getSecurityActionId(),
                                            device.getDeviceId(), groupPermission.getObjectId(), device.getName(), deviceType,
                                            selectedSchedule.get(), schedulesForSite);
        };

    }

    private UiSiteDTO findSiteForDevice(final Long deviceObjectId) {
        return deviceRepository.getUiSiteDTOForDevice(deviceObjectId);
    }

    private Function<SecurityGroupAntipassback, UiGroupAntipassbackDTO> convertSecurityGroupAntipassback() {
        return securityGroupAntipassback -> {
            if (securityGroupAntipassback != null)
                return new UiGroupAntipassbackDTO(
                        securityGroupAntipassback.getResetTime(),
                        ConverterUtils.convertShortToBoolean(securityGroupAntipassback.getImmunity()));
            else
                return new UiGroupAntipassbackDTO(NOT_AVAILABLE_LONG_VALUE, false);
        };
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiGroupDTO> getUiGroupsForAccountId(final Long accountId) {
        LOGGER.debug("Getting groups for account {}", accountId);

        Collection<UiGroupDTO> uiGroupDTOS = groupRepository.findUiGroupDTOSByAccountId(accountId);
        LOGGER.debug("Successfully retrieved groups for account {}", accountId);

        Collection<UiGroupDTO> detailedUiGroupDTOS = addDetailsToGroups(uiGroupDTOS, accountId);
        LOGGER.debug("Successfully added details to groups for account {}", accountId);

        return uiGroupDTOS;
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiGroupSummaryDTO> getUiGroupSummaryDTOSForAccountId(final Long accountId) {
        LOGGER.debug("Getting groups summary for account {}", accountId);

        Collection<UiGroupSummaryDTO> groupsSummary = groupRepository.getUiGroupSummaryForAccount(accountId);
        LOGGER.debug("Successfully retrieved {} groups summary for account {}", groupsSummary.size(), accountId);

        return groupsSummary;
    }

    public Collection<UiGroupDTO> addDetailsToGroups(final Collection<UiGroupDTO> groups, final Long accountId) {
        return groups.stream()
                     .map(addDetailsToGroup(accountId))
                     .sorted(uiGroupComparator)
                     .collect(Collectors.toList());
    }

    public Set<UiGroupDTO> getUiDeviceGroupsForAccountId(final Long accountId) {
        final List<SecurityGroup> groupsList;
        final Set<UiGroupDTO> uiGroupDTOSet;

        groupsList = groupRepository.findDeviceGroupsByAccountId(accountId);
        uiGroupDTOSet = getUiDeviceGroupDTOS(groupsList);
        return uiGroupDTOSet;
    }

    public Set<UiGroupDTO> getUiUsersGroupsForAccountId(final Long accountId) {
        final List<SecurityGroup> groupsList;
        final Set<UiGroupDTO> uiGroupDTOSet;

        groupsList = groupRepository.findByAccountId(accountId);
        uiGroupDTOSet = getUiUsersGroupDTOS(groupsList);
        return uiGroupDTOSet;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO create(UiGroupDTO uiGroupDTO) {

        final Optional<Long> group_id = groupRepository.getMaxGroupId();
        uiGroupDTO.setGroupId(group_id.get() + 1);

        final Long nextObjectId = objectService.getNextObjectId();
        uiGroupDTO.setObjectId(nextObjectId);

        validateNewGroup(uiGroupDTO);

        final SecurityGroup securityGroup = createGroup(uiGroupDTO);

        final SecurityGroup savedGroup = groupRepository.save(securityGroup);
        LOGGER.debug("Group {} successfully persisted to database...", savedGroup.getName());

        return new MessageDTO("Group '" + savedGroup.getName() + "' was successfully created!");
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO delete(List<Long> groupObjectIds) {
        LOGGER.debug("Deleting groups with object ids: {}", StringUtils.join(groupObjectIds.toArray(), "; "));

        groupObjectIds.forEach(groupObjectId -> {
            groupDao.deleteGroupByObjectId(groupObjectId);
        });

        sendFlushMessageToAssociatedPanels(groupObjectIds);

        LOGGER.debug("Successfully deleted groups with object ids: {}", StringUtils.join(groupObjectIds.toArray(), "; "));
        return new MessageDTO("Groups with object id '" + StringUtils.join(groupObjectIds.toArray(), "; ") + "' were successfully deleted!");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO update(UiGroupUpdateModelDTO uiGroupUpdateModelDTO) {
        final UiGroupDTO group = uiGroupUpdateModelDTO.getGroup();
        final Long groupObjectId = group.getObjectId();
        LOGGER.debug("Updating group {}", group.toString());

        final Optional<SecurityGroup> securityGroup = groupRepository.findByObjectId(groupObjectId);

        if (!securityGroup.isPresent()) {
            LOGGER.error("There is no group with object id {} in the database, update aborted...", groupObjectId);
            return new MessageDTO("There is no group with object id " + groupObjectId + " in the database, update aborted...");
        }

        SecurityGroup updatedGroup = securityGroup.get();
        updatedGroup.setName(group.getName());

        SecurityGroupAntipassback originalSecurityGroupAntipassback = updatedGroup.getSecurityGroupAntipassbackBySecurityGroupId();
        SecurityGroupAntipassback updatedSecurityGroupAntipassback = updateSecurityGroupAntipassback(updatedGroup.getSecurityGroupId(),
                                                                                                     originalSecurityGroupAntipassback, group.getUiGroupAntipassbackDTO());

        updatedGroup.setSecurityGroupAntipassbackBySecurityGroupId(updatedSecurityGroupAntipassback);
        LOGGER.debug("Updating group: {}", updatedGroup.toString());

        final SecurityGroup savedGroup = groupRepository.save(updatedGroup);
        LOGGER.debug("Group {} successfully persisted to database...", savedGroup.getName());

        if (!updateGroupPermissionsForGroup(groupObjectId, uiGroupUpdateModelDTO.getGroupPermissions())) {
            LOGGER.error("Group permissions could not be updated for group with object id {}", groupObjectId);
            return new MessageDTO("Group permissions could not be updated for group with object id " + groupObjectId);
        }

        final long accountId = group.getAccountId();
        final Optional<SecurityGroup> rootGroup = groupRepository.getRootGroupForAccount(accountId);
        if (!rootGroup.isPresent()) {
            LOGGER.error("Could not find root group for account {}, will not update keypad unlock hold privileges", accountId);
            return new MessageDTO("Could not find root group for account " + accountId + ", will not update keypad unlock hold privileges");
        }

        final Long defaultScheduleId = scheduleService.getIdOfDefaultSchedule();
        if (!updateKeypadUnlockHoldPrivilegesForGroup(group, rootGroup.get().getObjectId(), defaultScheduleId)) {
            LOGGER.error("Keypad unlock hold privileges could not be updated for group with object id {}", groupObjectId);
            return new MessageDTO("Keypad unlock hold privileges could not be updated for group with object id " + groupObjectId);
        }

        sendFlushMessageToAssociatedPanels(Collections.singletonList(groupObjectId));

        return new MessageDTO("Group '" + group.getName() + "' was successfully updated!");
    }

    private void sendFlushMessageToAssociatedPanels(final Collection<Long> groupObjectIds) {
        final Collection<ObjectPermission> objectPermissions = objectPermissionRepository.findAllByGroupObjectId(groupObjectIds, SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue())
                                                                                         .orElse(Collections.emptyList());

        if (objectPermissions.isEmpty()) {
            LOGGER.debug("There are no object permissions for group with object ids {}", StringUtils.join(groupObjectIds.toArray(), "; "));
            return;
        }

        final Collection<Long> brainIds = objectPermissions.stream()
                                                           .map(objectPermission -> objectPermission.getObjectByBrivoObjectId())
                                                           .filter(object -> object != null)
                                                           .map(object -> object.getDeviceByObjectId())
                                                           .filter(device -> device != null)
                                                           .map(device -> device.getBrainId())
                                                           .collect(Collectors.toSet());

        LOGGER.debug("Successfully retrieved brainIds = {} for group with object ids {}",
                     StringUtils.join(brainIds.toArray(), "; "), StringUtils.join(groupObjectIds.toArray(), "; "));
    }

    private boolean updateKeypadUnlockHoldPrivilegesForGroup(final UiGroupDTO uiGroupDTO, final Long rootGroupObjectId, final Long defaultScheduleId) {
        LOGGER.debug("Updating keypad unlock hold privileges for group {}, rootGroupObjectId: {}, defaultScheduleId: {}", uiGroupDTO.toString(), rootGroupObjectId, defaultScheduleId);

        final Optional<ObjectPermission> currentKeypadUnlockHoldPrivileges = objectPermissionRepository.findKeypadUnlockHoldPrivilege(uiGroupDTO.getObjectId(), SecurityAction.ACTION_LOCK_KEYPAD.getSecurityActionValue());

        if (uiGroupDTO.isKeypadUnlockHoldPrivileges() && !currentKeypadUnlockHoldPrivileges.isPresent()) {
            insertSingleGroupPermission(Long.valueOf(SecurityAction.ACTION_LOCK_KEYPAD.getSecurityActionValue()), uiGroupDTO.getObjectId(), rootGroupObjectId, defaultScheduleId);
        } else if (!uiGroupDTO.isKeypadUnlockHoldPrivileges() && currentKeypadUnlockHoldPrivileges.isPresent()) {
            deleteSingleGroupPermission(currentKeypadUnlockHoldPrivileges.get());
        }

        LOGGER.debug("Successfully updated keypad unlock hold privileges for group {}", uiGroupDTO.toString());
        return true;
    }

    private boolean updateGroupPermissionsForGroup(final Long groupObjectId, final Collection<UiGroupPermissionDTO> groupPermissionDTOS) {
        LOGGER.debug("Updating group permissions for group with object id {}", groupObjectId);

        Collection<ObjectPermission> originalObjectPermissionsForGroup = objectPermissionRepository.findObjectPermissionsByGroupObjectId(groupObjectId, SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue())
                                                                                                   .orElse(Collections.emptyList());
        groupPermissionDTOS.forEach(groupPermissionDTO -> {
            Optional<ObjectPermission> originalObjectPermission = originalObjectPermissionsForGroup.stream()
                                                                                                   .filter(objectPermission -> objectPermission.getObjectId() == groupPermissionDTO.getDeviceObjectId())
                                                                                                   .findFirst();

            final Long newScheduleId = groupPermissionDTO.getSelectedSchedule().getScheduleId();

            if (originalObjectPermission.isPresent()) {
                final Long originalScheduleId = originalObjectPermission.get().getScheduleId();

                if (!originalScheduleId.equals(newScheduleId)) {
                    if (newScheduleId > 0) {
                        updateSingleGroupPermission(originalObjectPermission.get(), newScheduleId);
                    } else {
                        deleteSingleGroupPermission(originalObjectPermission.get());
                    }
                }
            } else {
                if (newScheduleId > 0) {
                    insertSingleGroupPermission(Long.valueOf(SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue()), groupObjectId, groupPermissionDTO.getDeviceObjectId(), newScheduleId);
                }
            }
        });

        return true;
    }

    private void updateSingleGroupPermission(ObjectPermission originalObjectPermission, Long newScheduleId) {
        LOGGER.debug("Updating existing object permission {}, setting schedule_id = {}", originalObjectPermission, newScheduleId);

        originalObjectPermission.setScheduleId(newScheduleId);
        ObjectPermission updatedObjectPermission = objectPermissionRepository.save(originalObjectPermission);

        LOGGER.debug("Successfully updated object permission {}", updatedObjectPermission);
    }

    private void deleteSingleGroupPermission(final ObjectPermission objectPermission) {
        LOGGER.debug("Deleting existing object permission {}", objectPermission);

        objectPermissionRepository.deleteObjectPermission(objectPermission.getObjectId(), objectPermission.getActorObjectId(), objectPermission.getScheduleId());

        LOGGER.debug("Successfully deleted object permission {}", objectPermission);
    }

    private void insertSingleGroupPermission(final Long securityActionId, final Long groupObjectId, final Long deviceObjectId, final Long scheduleId) {
        LOGGER.debug("Inserting object permission for group with object id = {} to device {} for schedule {}", groupObjectId, deviceObjectId, scheduleId);

        ObjectPermission objectPermission = new ObjectPermission();
        objectPermission.setScheduleId(scheduleId);
        objectPermission.setObjectId(deviceObjectId);
        objectPermission.setActorObjectId(groupObjectId);
        objectPermission.setSecurityActionId(securityActionId);

        objectPermissionRepository.save(objectPermission);

        LOGGER.debug("Successfully inserted object permission {}", objectPermission);
    }

    private SecurityGroupAntipassback updateSecurityGroupAntipassback(final Long groupId,
                                                                      SecurityGroupAntipassback securityGroupAntipassback,
                                                                      final UiGroupAntipassbackDTO uiGroupAntipassbackDTO) {
        //if reset time is NOT_AVAILABLE_LONG_VALUE, there is no security_group_antipassback set for security_group
        final Long resetTime = uiGroupAntipassbackDTO.getResetTime();

        if (securityGroupAntipassback == null) {
            securityGroupAntipassback = new SecurityGroupAntipassback();
            securityGroupAntipassback.setSecurityGroupId(groupId);
        }

        securityGroupAntipassback.setImmunity(ConverterUtils.convertBooleanToShort(uiGroupAntipassbackDTO.getImmuneToAntipassback()));

        if (resetTime != NOT_AVAILABLE_LONG_VALUE) {
            securityGroupAntipassback.setResetTime(resetTime);
        } else {
            securityGroupAntipassback.setResetTime(null);
        }

        return securityGroupAntipassback;
    }

    private SecurityGroup createGroup(final UiGroupDTO uiGroupDTO) {
        final SecurityGroup securityGroup = new SecurityGroup();

        securityGroup.setName(uiGroupDTO.getName());
        securityGroup.setDescription(uiGroupDTO.getDescription());
        securityGroup.setSecurityGroupId(uiGroupDTO.getGroupId());

        final SecurityGroupType securityGroupType = securityGroupTypeRepository.findById(SecurityGroupTypes.USER_GROUP.getSecurityGroupTypeID()).get();
        securityGroup.setSecurityGroupTypeBySecurityGroupTypeId(securityGroupType);

        final Account account = accountRepository.findById(uiGroupDTO.getAccountId()).get();
        securityGroup.setAccountByAccountId(account);

        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.SECURITY_GROUP);
        brivoObject.setObjectId(uiGroupDTO.getObjectId());
        securityGroup.setObjectByObjectId(brivoObject);

        return securityGroup;
    }


    private void validateNewGroup(final UiGroupDTO uiGroupDTO) {
        validateGroup(uiGroupDTO);
    }


    private void validateGroup(final UiGroupDTO uiGroupDTO) {
        final String name = uiGroupDTO.getName();
        Assert.hasLength(name, "The group must have a name");

        validateAccount(uiGroupDTO);

        //TODO add other validations, if needed
    }

    private void validateAccount(final UiGroupDTO uiGroupDTO) {
        final long accountId = uiGroupDTO.getAccountId();
        Assert.state(accountRepository.findById(accountId).isPresent(),
                     "There is no account with the id " + accountId);

    }

    public Set<UiGroupDTO> getUiUsersGroupsForDeviceObjId(final Long deviceObjId) {
        final List<SecurityGroup> groupsList;
        final Set<UiGroupDTO> uiGroupDTOSet;

        groupsList = groupRepository.findByDeviceObjId(deviceObjId);
        //uiGroupDTOSet = getUiUsersGroupDTOS(groupsList);
        uiGroupDTOSet = convertToUiUsersGroupDTO(groupsList);
        return uiGroupDTOSet;
    }

    public Set<UiGroupDTO> convertToUiUsersGroupDTO(final Collection<SecurityGroup> groups) {
        return groups.stream()
                     .filter(isValidUserGroup())
                     .map(convertGroupUICucumberTest())
                     .collect(Collectors.toSet());
    }

    private Function<SecurityGroup, UiGroupDTO> convertGroupUICucumberTest() {
        return group -> new UiGroupDTO(group.getAccountId(),
                                       group.getSecurityGroupId(), group.getName());
    }

    public Collection<UiGroupPermissionDTO> getUiGroupPermissionDTOSCurrentPrivileges(final Long accountId,
                                                                                      final Long groupObjectId) {
        LOGGER.debug("Getting current privileges for account {}, groupObjectId {}", accountId, groupObjectId);
        List<ObjectPermission> groupPermissions;

        Collection<Integer> securityActions = new ArrayList();
        securityActions.add(SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue());
        securityActions.add(SecurityAction.ACTION_DOOR_LOCK_OPEN_PRIVACY_OVERRIDE.getSecurityActionValue());

        groupPermissions = objectPermissionRepository.findCurrentPrivileges(accountId, groupObjectId, securityActions)
                                                     .orElse(Collections.emptyList());

        if (groupPermissions.isEmpty()) {
            LOGGER.debug("There are no current privileges for group with id {}", groupObjectId);
        } else {
            LOGGER.debug("Current privileges retrieved from database: {}", groupPermissions.size());
        }

        Collection<UiSiteDTO> allSites = getUISiteDTOSForAccountForEditGroup(accountId);
        final Map<Long, Collection<UiScheduleSummaryDTO>> availableSchedulesMap = scheduleService.getAvailableSchedulesMapForAllSites(accountId, allSites);

        Collection<UiGroupPermissionDTO> groupPermissionDTOS = groupPermissions.stream()
                                                                               .map(convertCurrentPrivilege(availableSchedulesMap))
                                                                               .collect(Collectors.toList());

        return groupPermissionDTOS.stream()
                                  .sorted(uiGroupPermissionComparator)
                                  .collect(Collectors.toList());
    }

    public Collection<UiGroupPermissionDTO> getUiGroupPermissionDTOS(final Long accountId,
                                                                     final Long groupObjectId, final Long siteObjectId) {
        LOGGER.debug("Getting group permissions for account {}, groupObjectId {}, siteObjectId {}", accountId, groupObjectId, siteObjectId);
        List<ObjectPermission> groupPermissions;

        if (siteObjectId == ALL_SITES_IDENTIFIER) {
            //FIXME
            return Collections.emptyList();
        }

        Collection<Integer> securityActions = new ArrayList();
        securityActions.add(com.brivo.panel.server.reference.model.event.SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue());

        groupPermissions = objectPermissionRepository.findObjectPermissionsByGroupObjectIdAndSiteId(
                accountId, groupObjectId, siteObjectId, securityActions).orElse(Collections.emptyList());

        if (groupPermissions.isEmpty()) {
            LOGGER.debug("There are no group permissions for group with id {}", groupObjectId);
        } else {
            LOGGER.debug("Group permissions retrieved from database: {}", groupPermissions.size());
        }

//        Collection<UiSiteDTO> allSites = getUISiteDTOSForAccountForEditGroup(accountId);
//        final Map<Long, Collection<UiScheduleSummaryDTO>> availableSchedulesMap = scheduleService.getAvailableSchedulesMapForAllSites(accountId, allSites);
        Collection<UiScheduleSummaryDTO> schedulesForSite = scheduleService.getUiScheduleSummaryDTOSForSiteWithNoAccessAndAlwaysAccess(accountId, siteObjectId);

        Collection<UiGroupPermissionDTO> groupPermissionDTOS = groupPermissions.stream()
                                                                               .map(convertGroupPermission(schedulesForSite))
                                                                               .collect(Collectors.toList());

        Collection<Long> deviceIdsWithPermissions = groupPermissions.stream()
                                                                    .map(objectPermission -> objectPermission.getObjectId())
                                                                    .collect(Collectors.toList());

        final String siteName = groupRepository.getSiteNameBySiteObjectId(siteObjectId);
        Collection<UiGroupPermissionDTO> noPermissionsForDevices = buildNoPermissionForDevices(siteObjectId, siteName,
                                                                                               deviceIdsWithPermissions, schedulesForSite);

        ((List<UiGroupPermissionDTO>) groupPermissionDTOS).addAll(noPermissionsForDevices);


        return groupPermissionDTOS.stream()
                                  .sorted(uiGroupPermissionComparator)
                                  .collect(Collectors.toList());
    }


    Comparator<UiGroupPermissionDTO> uiGroupPermissionComparator = new Comparator<UiGroupPermissionDTO>() {
        @Override
        public int compare(UiGroupPermissionDTO o1, UiGroupPermissionDTO o2) {
            return o1.getDeviceId().compareTo(o2.getDeviceId());
        }
    };

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED)
    public Collection<UiGroupPermissionDTO> buildNoPermissionForDevices(final Long siteObjectId, final String siteName,
                                                                        Collection<Long> deviceIdsWithPermissions,
                                                                        Collection<UiScheduleSummaryDTO> availableSchedules) {
        LOGGER.debug("Building groupPermissionDTOS for devices without permissions. SiteObjectId = {}, siteName = {}, deviceIdsWithPermissions: {}",
                     siteObjectId, siteName, StringUtils.join(deviceIdsWithPermissions.toArray(), "; "));

        if (deviceIdsWithPermissions.isEmpty()) {
            LOGGER.debug("There are no devices that have permissions for site with object id {}, will add a fake one for the query to work");
            deviceIdsWithPermissions.add(0L);
        }

        Collection<Device> devicesWithoutPermissions = deviceRepository.findDoorsFromSiteExceptTheOnesWithPermissions(siteObjectId, deviceIdsWithPermissions);

        if (devicesWithoutPermissions == null || devicesWithoutPermissions.size() == 0) {
            LOGGER.debug("There are no devices without permission for site with object id {}", siteObjectId);
            return Collections.emptyList();
        }

        Collection<UiGroupPermissionDTO> devicesWithoutPermissionGroups = new ArrayList<>();

        devicesWithoutPermissions.forEach(device -> {
            String deviceType = deviceRepository.findDeviceTypeDescriptionById(device.getDeviceTypeId());

            UiGroupPermissionDTO uiGroupPermissionDTO = new UiGroupPermissionDTO(siteName, UiScheduleService.NO_ACCESS_SCHEDULE.getScheduleId(),
                                                                                 0L, device.getDeviceId(), device.getObjectId(), device.getName(), deviceType,
                                                                                 UiScheduleService.NO_ACCESS_SCHEDULE, availableSchedules);

            devicesWithoutPermissionGroups.add(uiGroupPermissionDTO);
        });

        return devicesWithoutPermissionGroups;
    }

//    //  FIXME -remove after done testing
//    public Collection<UiGroupPermissionDTO> testGroupPermissions(final Long accountId,
//                                                                 final Long groupId, final Long siteId) {
//        Collection<Long> securityActions = new ArrayList();
//        securityActions.add(2004L);
//
//        Collection<UiGroupPermissionDTO> all = objectPermissionRepository.findUiGroupPermissionDTOForSite(accountId, groupId, siteId, securityActions);
//
//        return all;
//        //return objectPermissionRepository.findGroupPermissions(accountId, groupId, securityActions);
//    }
}

