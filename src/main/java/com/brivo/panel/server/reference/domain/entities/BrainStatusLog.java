package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "brain_status_log", schema = "brivo20", catalog = "onair")
public class BrainStatusLog {
    private long brainStatusLogId;
    private long brainId;
    private long deviceStatusId;
    private String statusType;
    private Long memoryFileSpace;
    private Long memoryFileEntries;
    private Long memoryRam;
    private Long batteryStrength;
    private Long temperature;
    private Long signalStrength;
    private String picCodeVersion;
    private String rimFirmwareVersion;
    private String brainState;
    private short deviceRestart;
    private short externallyPowered;
    private Timestamp occured;
    private Timestamp created;
    private Timestamp updated;
    private DeviceStatus deviceStatusByDeviceStatusId;

    @Id
    @Column(name = "brain_status_log_id", nullable = false)
    public long getBrainStatusLogId() {
        return brainStatusLogId;
    }

    public void setBrainStatusLogId(long brainStatusLogId) {
        this.brainStatusLogId = brainStatusLogId;
    }

    @Basic
    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "device_status_id", nullable = false)
    public long getDeviceStatusId() {
        return deviceStatusId;
    }

    public void setDeviceStatusId(long deviceStatusId) {
        this.deviceStatusId = deviceStatusId;
    }

    @Basic
    @Column(name = "status_type", nullable = false, length = 32)
    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    @Basic
    @Column(name = "memory_file_space", nullable = true)
    public Long getMemoryFileSpace() {
        return memoryFileSpace;
    }

    public void setMemoryFileSpace(Long memoryFileSpace) {
        this.memoryFileSpace = memoryFileSpace;
    }

    @Basic
    @Column(name = "memory_file_entries", nullable = true)
    public Long getMemoryFileEntries() {
        return memoryFileEntries;
    }

    public void setMemoryFileEntries(Long memoryFileEntries) {
        this.memoryFileEntries = memoryFileEntries;
    }

    @Basic
    @Column(name = "memory_ram", nullable = true)
    public Long getMemoryRam() {
        return memoryRam;
    }

    public void setMemoryRam(Long memoryRam) {
        this.memoryRam = memoryRam;
    }

    @Basic
    @Column(name = "battery_strength", nullable = true)
    public Long getBatteryStrength() {
        return batteryStrength;
    }

    public void setBatteryStrength(Long batteryStrength) {
        this.batteryStrength = batteryStrength;
    }

    @Basic
    @Column(name = "temperature", nullable = true)
    public Long getTemperature() {
        return temperature;
    }

    public void setTemperature(Long temperature) {
        this.temperature = temperature;
    }

    @Basic
    @Column(name = "signal_strength", nullable = true)
    public Long getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(Long signalStrength) {
        this.signalStrength = signalStrength;
    }

    @Basic
    @Column(name = "pic_code_version", nullable = true, length = 10)
    public String getPicCodeVersion() {
        return picCodeVersion;
    }

    public void setPicCodeVersion(String picCodeVersion) {
        this.picCodeVersion = picCodeVersion;
    }

    @Basic
    @Column(name = "rim_firmware_version", nullable = true, length = 10)
    public String getRimFirmwareVersion() {
        return rimFirmwareVersion;
    }

    public void setRimFirmwareVersion(String rimFirmwareVersion) {
        this.rimFirmwareVersion = rimFirmwareVersion;
    }

    @Basic
    @Column(name = "brain_state", nullable = true, length = 30)
    public String getBrainState() {
        return brainState;
    }

    public void setBrainState(String brainState) {
        this.brainState = brainState;
    }

    @Basic
    @Column(name = "device_restart", nullable = false)
    public short getDeviceRestart() {
        return deviceRestart;
    }

    public void setDeviceRestart(short deviceRestart) {
        this.deviceRestart = deviceRestart;
    }

    @Basic
    @Column(name = "externally_powered", nullable = false)
    public short getExternallyPowered() {
        return externallyPowered;
    }

    public void setExternallyPowered(short externallyPowered) {
        this.externallyPowered = externallyPowered;
    }

    @Basic
    @Column(name = "occured", nullable = false)
    public Timestamp getOccured() {
        return occured;
    }

    public void setOccured(Timestamp occured) {
        this.occured = occured;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrainStatusLog that = (BrainStatusLog) o;

        if (brainStatusLogId != that.brainStatusLogId) {
            return false;
        }
        if (brainId != that.brainId) {
            return false;
        }
        if (deviceStatusId != that.deviceStatusId) {
            return false;
        }
        if (deviceRestart != that.deviceRestart) {
            return false;
        }
        if (externallyPowered != that.externallyPowered) {
            return false;
        }
        if (statusType != null ? !statusType.equals(that.statusType) : that.statusType != null) {
            return false;
        }
        if (memoryFileSpace != null ? !memoryFileSpace.equals(that.memoryFileSpace) : that.memoryFileSpace != null) {
            return false;
        }
        if (memoryFileEntries != null ? !memoryFileEntries.equals(that.memoryFileEntries) : that.memoryFileEntries != null) {
            return false;
        }
        if (memoryRam != null ? !memoryRam.equals(that.memoryRam) : that.memoryRam != null) {
            return false;
        }
        if (batteryStrength != null ? !batteryStrength.equals(that.batteryStrength) : that.batteryStrength != null) {
            return false;
        }
        if (temperature != null ? !temperature.equals(that.temperature) : that.temperature != null) {
            return false;
        }
        if (signalStrength != null ? !signalStrength.equals(that.signalStrength) : that.signalStrength != null) {
            return false;
        }
        if (picCodeVersion != null ? !picCodeVersion.equals(that.picCodeVersion) : that.picCodeVersion != null) {
            return false;
        }
        if (rimFirmwareVersion != null ? !rimFirmwareVersion.equals(that.rimFirmwareVersion) : that.rimFirmwareVersion != null) {
            return false;
        }
        if (brainState != null ? !brainState.equals(that.brainState) : that.brainState != null) {
            return false;
        }
        if (occured != null ? !occured.equals(that.occured) : that.occured != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainStatusLogId ^ (brainStatusLogId >>> 32));
        result = 31 * result + (int) (brainId ^ (brainId >>> 32));
        result = 31 * result + (int) (deviceStatusId ^ (deviceStatusId >>> 32));
        result = 31 * result + (statusType != null ? statusType.hashCode() : 0);
        result = 31 * result + (memoryFileSpace != null ? memoryFileSpace.hashCode() : 0);
        result = 31 * result + (memoryFileEntries != null ? memoryFileEntries.hashCode() : 0);
        result = 31 * result + (memoryRam != null ? memoryRam.hashCode() : 0);
        result = 31 * result + (batteryStrength != null ? batteryStrength.hashCode() : 0);
        result = 31 * result + (temperature != null ? temperature.hashCode() : 0);
        result = 31 * result + (signalStrength != null ? signalStrength.hashCode() : 0);
        result = 31 * result + (picCodeVersion != null ? picCodeVersion.hashCode() : 0);
        result = 31 * result + (rimFirmwareVersion != null ? rimFirmwareVersion.hashCode() : 0);
        result = 31 * result + (brainState != null ? brainState.hashCode() : 0);
        result = 31 * result + (int) deviceRestart;
        result = 31 * result + (int) externallyPowered;
        result = 31 * result + (occured != null ? occured.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "device_status_id", referencedColumnName = "device_status_id", nullable = false,
            insertable = false, updatable = false)
    public DeviceStatus getDeviceStatusByDeviceStatusId() {
        return deviceStatusByDeviceStatusId;
    }

    public void setDeviceStatusByDeviceStatusId(DeviceStatus deviceStatusByDeviceStatusId) {
        this.deviceStatusByDeviceStatusId = deviceStatusByDeviceStatusId;
    }
}
