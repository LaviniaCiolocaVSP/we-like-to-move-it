package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "mac_address", schema = "brivo20", catalog = "onair")
public class MacAddress {
    private String prefix;
    private String currentValue;
    private String maxValue;

    @Id
    @Basic
    @Column(name = "prefix", nullable = false, length = 12)
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @Basic
    @Column(name = "current_value", nullable = false, length = 12)
    public String getCurrentValue() {
        return currentValue;
    }

    public void setCurrentValue(String currentValue) {
        this.currentValue = currentValue;
    }

    @Basic
    @Column(name = "max_value", nullable = false, length = 12)
    public String getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(String maxValue) {
        this.maxValue = maxValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MacAddress that = (MacAddress) o;

        if (prefix != null ? !prefix.equals(that.prefix) : that.prefix != null) {
            return false;
        }
        if (currentValue != null ? !currentValue.equals(that.currentValue) : that.currentValue != null) {
            return false;
        }
        return maxValue != null ? maxValue.equals(that.maxValue) : that.maxValue == null;
    }

    @Override
    public int hashCode() {
        int result = prefix != null ? prefix.hashCode() : 0;
        result = 31 * result + (currentValue != null ? currentValue.hashCode() : 0);
        result = 31 * result + (maxValue != null ? maxValue.hashCode() : 0);
        return result;
    }
}
