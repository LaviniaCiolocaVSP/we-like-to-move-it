package com.brivo.panel.server.reference.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class Heartbeat {
    @Autowired
    private SessionRegistryService sessionRegistryService;

    private Long panelId;

    public Heartbeat(Long panelId) {
        this.panelId = panelId;
    }

    @Async
    @Scheduled(initialDelay = 30000, fixedRate = 30000)
    public void sendHeartBeat() {
        sessionRegistryService.processHeartbeat(panelId);
    }

}
