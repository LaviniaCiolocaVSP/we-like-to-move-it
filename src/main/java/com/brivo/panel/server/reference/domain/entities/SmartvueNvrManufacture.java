package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "smartvue_nvr_manufacture", schema = "brivo20", catalog = "onair")
public class SmartvueNvrManufacture {
    private long smartvueNvrId;
    private String serialNumber;
    private String authKey;
    private String cloudvueAccountId;
    private String cloudvueUsername;
    private String cloudvuePassword;
    private Long nvrPortNumber;
    private String nvrUsername;
    private String nvrPassword;
    private String manufactureStatus;
    private String manufactureClientId;
    private Timestamp manufactured;
    private Timestamp created;
    private Timestamp updated;
    private String nvrModel;
    private String nvrFirmwareVersion;
    private Long cloudvueUserId;
    private Collection<SmartvueNvrRegistration> smartvueNvrRegistrationsBySmartvueNvrId;

    @Id
    @Column(name = "smartvue_nvr_id", nullable = false)
    public long getSmartvueNvrId() {
        return smartvueNvrId;
    }

    public void setSmartvueNvrId(long smartvueNvrId) {
        this.smartvueNvrId = smartvueNvrId;
    }

    @Basic
    @Column(name = "serial_number", nullable = false, length = 32)
    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Basic
    @Column(name = "auth_key", nullable = false, length = 32)
    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    @Basic
    @Column(name = "cloudvue_account_id", nullable = true, length = 32)
    public String getCloudvueAccountId() {
        return cloudvueAccountId;
    }

    public void setCloudvueAccountId(String cloudvueAccountId) {
        this.cloudvueAccountId = cloudvueAccountId;
    }

    @Basic
    @Column(name = "cloudvue_username", nullable = false, length = 512)
    public String getCloudvueUsername() {
        return cloudvueUsername;
    }

    public void setCloudvueUsername(String cloudvueUsername) {
        this.cloudvueUsername = cloudvueUsername;
    }

    @Basic
    @Column(name = "cloudvue_password", nullable = false, length = 64)
    public String getCloudvuePassword() {
        return cloudvuePassword;
    }

    public void setCloudvuePassword(String cloudvuePassword) {
        this.cloudvuePassword = cloudvuePassword;
    }

    @Basic
    @Column(name = "nvr_port_number", nullable = true)
    public Long getNvrPortNumber() {
        return nvrPortNumber;
    }

    public void setNvrPortNumber(Long nvrPortNumber) {
        this.nvrPortNumber = nvrPortNumber;
    }

    @Basic
    @Column(name = "nvr_username", nullable = false, length = 32)
    public String getNvrUsername() {
        return nvrUsername;
    }

    public void setNvrUsername(String nvrUsername) {
        this.nvrUsername = nvrUsername;
    }

    @Basic
    @Column(name = "nvr_password", nullable = false, length = 64)
    public String getNvrPassword() {
        return nvrPassword;
    }

    public void setNvrPassword(String nvrPassword) {
        this.nvrPassword = nvrPassword;
    }

    @Basic
    @Column(name = "manufacture_status", nullable = false, length = 32)
    public String getManufactureStatus() {
        return manufactureStatus;
    }

    public void setManufactureStatus(String manufactureStatus) {
        this.manufactureStatus = manufactureStatus;
    }

    @Basic
    @Column(name = "manufacture_client_id", nullable = false, length = 32)
    public String getManufactureClientId() {
        return manufactureClientId;
    }

    public void setManufactureClientId(String manufactureClientId) {
        this.manufactureClientId = manufactureClientId;
    }

    @Basic
    @Column(name = "manufactured", nullable = true)
    public Timestamp getManufactured() {
        return manufactured;
    }

    public void setManufactured(Timestamp manufactured) {
        this.manufactured = manufactured;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "nvr_model", nullable = false, length = 128)
    public String getNvrModel() {
        return nvrModel;
    }

    public void setNvrModel(String nvrModel) {
        this.nvrModel = nvrModel;
    }

    @Basic
    @Column(name = "nvr_firmware_version", nullable = false, length = 32)
    public String getNvrFirmwareVersion() {
        return nvrFirmwareVersion;
    }

    public void setNvrFirmwareVersion(String nvrFirmwareVersion) {
        this.nvrFirmwareVersion = nvrFirmwareVersion;
    }

    @Basic
    @Column(name = "cloudvue_user_id", nullable = true)
    public Long getCloudvueUserId() {
        return cloudvueUserId;
    }

    public void setCloudvueUserId(Long cloudvueUserId) {
        this.cloudvueUserId = cloudvueUserId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SmartvueNvrManufacture that = (SmartvueNvrManufacture) o;
        return smartvueNvrId == that.smartvueNvrId &&
                Objects.equals(serialNumber, that.serialNumber) &&
                Objects.equals(authKey, that.authKey) &&
                Objects.equals(cloudvueAccountId, that.cloudvueAccountId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(smartvueNvrId, serialNumber, authKey, cloudvueAccountId);
    }

    @OneToMany(mappedBy = "smartvueNvrManufactureBySmartvueNvrId")
    public Collection<SmartvueNvrRegistration> getSmartvueNvrRegistrationsBySmartvueNvrId() {
        return smartvueNvrRegistrationsBySmartvueNvrId;
    }

    public void setSmartvueNvrRegistrationsBySmartvueNvrId(Collection<SmartvueNvrRegistration> smartvueNvrRegistrationsBySmartvueNvrId) {
        this.smartvueNvrRegistrationsBySmartvueNvrId = smartvueNvrRegistrationsBySmartvueNvrId;
    }
}
