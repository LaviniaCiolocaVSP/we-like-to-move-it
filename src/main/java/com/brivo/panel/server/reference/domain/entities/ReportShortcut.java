package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "report_shortcut", schema = "brivo20", catalog = "onair")
public class ReportShortcut {
    private long id;
    private long userObjectId;
    private long reportConfigurationId;
    private BrivoObject objectByUserBrivoObjectId;
    private ReportConfiguration reportConfigurationByReportConfigurationId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "report_configuration_id", nullable = false)
    public long getReportConfigurationId() {
        return reportConfigurationId;
    }

    public void setReportConfigurationId(long reportConfigurationId) {
        this.reportConfigurationId = reportConfigurationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportShortcut that = (ReportShortcut) o;

        if (id != that.id) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        return reportConfigurationId == that.reportConfigurationId;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (int) (reportConfigurationId ^ (reportConfigurationId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "user_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByUserBrivoObjectId() {
        return objectByUserBrivoObjectId;
    }

    public void setObjectByUserBrivoObjectId(BrivoObject objectByUserBrivoObjectId) {
        this.objectByUserBrivoObjectId = objectByUserBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "report_configuration_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportConfiguration getReportConfigurationByReportConfigurationId() {
        return reportConfigurationByReportConfigurationId;
    }

    public void setReportConfigurationByReportConfigurationId(ReportConfiguration reportConfigurationByReportConfigurationId) {
        this.reportConfigurationByReportConfigurationId = reportConfigurationByReportConfigurationId;
    }
}
