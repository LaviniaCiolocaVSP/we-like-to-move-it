package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "camera_group", schema = "brivo20", catalog = "onair")
public class CameraGroup {
    private long objectId;
    private String groupName;
    private long accountId;
    private long userObjectId;
    private Long defaultCountShownX;
    private Long defaultCountShownY;
    private BrivoObject objectByBrivoObjectId;
    private Account accountByAccountId;
    //private BrivoObject objectByUserBrivoObjectId;
    private Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByObjectId;
    private Collection<DefaultCameraGroup> defaultCameraGroupsByObjectId;

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "group_name", nullable = false, length = 255)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "default_count_shown_x", nullable = true)
    public Long getDefaultCountShownX() {
        return defaultCountShownX;
    }

    public void setDefaultCountShownX(Long defaultCountShownX) {
        this.defaultCountShownX = defaultCountShownX;
    }

    @Basic
    @Column(name = "default_count_shown_y", nullable = true)
    public Long getDefaultCountShownY() {
        return defaultCountShownY;
    }

    public void setDefaultCountShownY(Long defaultCountShownY) {
        this.defaultCountShownY = defaultCountShownY;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraGroup that = (CameraGroup) o;

        if (objectId != that.objectId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) {
            return false;
        }
        if (defaultCountShownX != null ? !defaultCountShownX.equals(that.defaultCountShownX) : that.defaultCountShownX != null) {
            return false;
        }
        return defaultCountShownY != null ? defaultCountShownY.equals(that.defaultCountShownY) : that.defaultCountShownY == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (defaultCountShownX != null ? defaultCountShownX.hashCode() : 0);
        result = 31 * result + (defaultCountShownY != null ? defaultCountShownY.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "user_object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByUserBrivoObjectId() {
        return objectByUserBrivoObjectId;
    }

    public void setObjectByUserBrivoObjectId(BrivoObject objectByUserBrivoObjectId) {
        this.objectByUserBrivoObjectId = objectByUserBrivoObjectId;
    }
    */

    @OneToMany(mappedBy = "cameraGroupByCameraGroupId")
    public Collection<CameraGroupOvrCamera> getCameraGroupOvrCamerasByObjectId() {
        return cameraGroupOvrCamerasByObjectId;
    }

    public void setCameraGroupOvrCamerasByObjectId(Collection<CameraGroupOvrCamera> cameraGroupOvrCamerasByObjectId) {
        this.cameraGroupOvrCamerasByObjectId = cameraGroupOvrCamerasByObjectId;
    }

    @OneToMany(mappedBy = "cameraGroupByCameraGroupObjectId")
    public Collection<DefaultCameraGroup> getDefaultCameraGroupsByObjectId() {
        return defaultCameraGroupsByObjectId;
    }

    public void setDefaultCameraGroupsByObjectId(Collection<DefaultCameraGroup> defaultCameraGroupsByObjectId) {
        this.defaultCameraGroupsByObjectId = defaultCameraGroupsByObjectId;
    }
}
