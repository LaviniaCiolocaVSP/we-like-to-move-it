package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiSiteDTO implements Serializable {
    private final Long siteId;
    private final String siteName;
    private final Long siteObjectId;

    public UiSiteDTO(Long siteId, Long siteObjectId, String siteName) {
        this.siteId = siteId;
        this.siteName = siteName;
        this.siteObjectId = siteObjectId;
    }

    public Long getSiteObjectId() {
        return siteObjectId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public String getSiteName() {
        return siteName;
    }
}
