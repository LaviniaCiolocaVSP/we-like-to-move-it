package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.CredentialField;
import org.springframework.data.repository.CrudRepository;

public interface ICredentialFieldRepository extends CrudRepository<CredentialField, Long> {
}
