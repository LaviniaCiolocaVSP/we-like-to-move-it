package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupTypeDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupUpdateModelDTO;
import com.brivo.panel.server.reference.service.GroupService;
import com.brivo.panel.server.reference.service.GroupTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/group")
public class GroupController {
    private GroupService groupService;
    private GroupTypeService groupTypeService;

    @Autowired
    public GroupController(GroupService groupService, GroupTypeService groupTypeService) {
        this.groupService = groupService;
        this.groupTypeService = groupTypeService;
    }

    @GetMapping("/{accountId}")
    public Collection<UiGroupDTO> get(@PathVariable final long accountId) {
        return this.groupService.getUiGroupsForAccountId(accountId);
    }

    @GetMapping("/summary/{accountId}")
    public Collection<UiGroupSummaryDTO> getGroupsSummary(@PathVariable final long accountId) {
        return this.groupService.getUiGroupSummaryDTOSForAccountId(accountId);
    }

    @GetMapping("/devices/{accountId}")
    public Set<UiGroupDTO> getDevicesForAccount(@PathVariable final long accountId) {
        return this.groupService.getUiDeviceGroupsForAccountId(accountId);
    }

    @GetMapping("/users/{accountId}")
    public Set<UiGroupDTO> getUsersForAccount(@PathVariable final long accountId) {
        return this.groupService.getUiUsersGroupsForAccountId(accountId);
    }

    @GetMapping("/users/device/{deviceObjId}")
    public Set<UiGroupDTO> getUsersForDevice(@PathVariable final long deviceObjId) {
        return this.groupService.getUiUsersGroupsForDeviceObjId(deviceObjId);
    }

    @GetMapping("/groupPermissions/{accountId}/{groupObjectId}/{siteObjectId}")
    public Collection<UiGroupPermissionDTO> getGroupPermissions(@PathVariable final long accountId,
                                                                @PathVariable final long groupObjectId,
                                                                @PathVariable final long siteObjectId) {
        if (siteObjectId != GroupService.CURRENT_PRIVILEGES_IDENTIFIER)
            return this.groupService.getUiGroupPermissionDTOS(accountId, groupObjectId, siteObjectId);
        else
            return this.groupService.getUiGroupPermissionDTOSCurrentPrivileges(accountId, groupObjectId);
    }

    @PostMapping(
            path = "/add",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO create(@RequestBody final UiGroupDTO uiGroupDTO) {
        return groupService.create(uiGroupDTO);
    }

    @DeleteMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO delete(@RequestBody List<Long> groupObjectIds) {
        return groupService.delete(groupObjectIds);
    }

    @PutMapping(
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO update(@RequestBody final UiGroupUpdateModelDTO uiGroupUpdateModelDTO) {
        return groupService.update(uiGroupUpdateModelDTO);
    }

}
