package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "external_id_type", schema = "brivo20", catalog = "onair")
public class ExternalIdType {
    private long externalTypeId;
    private String name;
    private Collection<LoginGroup> loginGroupsByExternalTypeId;

    @Id
    @Column(name = "external_type_id", nullable = false)
    public long getExternalTypeId() {
        return externalTypeId;
    }

    public void setExternalTypeId(long externalTypeId) {
        this.externalTypeId = externalTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ExternalIdType that = (ExternalIdType) o;

        if (externalTypeId != that.externalTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (externalTypeId ^ (externalTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "externalIdTypeByExternalTypeId")
    public Collection<LoginGroup> getLoginGroupsByExternalTypeId() {
        return loginGroupsByExternalTypeId;
    }

    public void setLoginGroupsByExternalTypeId(Collection<LoginGroup> loginGroupsByExternalTypeId) {
        this.loginGroupsByExternalTypeId = loginGroupsByExternalTypeId;
    }
}
