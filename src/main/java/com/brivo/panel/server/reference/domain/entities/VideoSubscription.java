package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "video_subscription", schema = "brivo20", catalog = "onair")
public class VideoSubscription {
    private long videoSubscriptionId;
    private String name;
    private Long storageLimit;
    private String liveViewResolution;
    private String recordedResolution;
    private String externalId;
    private Long retentionPeriod;
    private Long liveViewFramerate;
    private Long recordedFramerate;
    private Collection<VideoCamera> videoCamerasByVideoSubscriptionId;
    private Collection<VideoProvider> videoProvidersByVideoSubscriptionId;

    @Id
    @Column(name = "video_subscription_id", nullable = false)
    public long getVideoSubscriptionId() {
        return videoSubscriptionId;
    }

    public void setVideoSubscriptionId(long videoSubscriptionId) {
        this.videoSubscriptionId = videoSubscriptionId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "storage_limit", nullable = true)
    public Long getStorageLimit() {
        return storageLimit;
    }

    public void setStorageLimit(Long storageLimit) {
        this.storageLimit = storageLimit;
    }

    @Basic
    @Column(name = "live_view_resolution", nullable = true, length = 256)
    public String getLiveViewResolution() {
        return liveViewResolution;
    }

    public void setLiveViewResolution(String liveViewResolution) {
        this.liveViewResolution = liveViewResolution;
    }

    @Basic
    @Column(name = "recorded_resolution", nullable = true, length = 256)
    public String getRecordedResolution() {
        return recordedResolution;
    }

    public void setRecordedResolution(String recordedResolution) {
        this.recordedResolution = recordedResolution;
    }

    @Basic
    @Column(name = "external_id", nullable = true, length = 256)
    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    @Basic
    @Column(name = "retention_period", nullable = true)
    public Long getRetentionPeriod() {
        return retentionPeriod;
    }

    public void setRetentionPeriod(Long retentionPeriod) {
        this.retentionPeriod = retentionPeriod;
    }

    @Basic
    @Column(name = "live_view_framerate", nullable = true)
    public Long getLiveViewFramerate() {
        return liveViewFramerate;
    }

    public void setLiveViewFramerate(Long liveViewFramerate) {
        this.liveViewFramerate = liveViewFramerate;
    }

    @Basic
    @Column(name = "recorded_framerate", nullable = true)
    public Long getRecordedFramerate() {
        return recordedFramerate;
    }

    public void setRecordedFramerate(Long recordedFramerate) {
        this.recordedFramerate = recordedFramerate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoSubscription that = (VideoSubscription) o;

        if (videoSubscriptionId != that.videoSubscriptionId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (storageLimit != null ? !storageLimit.equals(that.storageLimit) : that.storageLimit != null) {
            return false;
        }
        if (liveViewResolution != null ? !liveViewResolution.equals(that.liveViewResolution) : that.liveViewResolution != null) {
            return false;
        }
        if (recordedResolution != null ? !recordedResolution.equals(that.recordedResolution) : that.recordedResolution != null) {
            return false;
        }
        if (externalId != null ? !externalId.equals(that.externalId) : that.externalId != null) {
            return false;
        }
        if (retentionPeriod != null ? !retentionPeriod.equals(that.retentionPeriod) : that.retentionPeriod != null) {
            return false;
        }
        if (liveViewFramerate != null ? !liveViewFramerate.equals(that.liveViewFramerate) : that.liveViewFramerate != null) {
            return false;
        }
        return recordedFramerate != null ? recordedFramerate.equals(that.recordedFramerate) : that.recordedFramerate == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoSubscriptionId ^ (videoSubscriptionId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (storageLimit != null ? storageLimit.hashCode() : 0);
        result = 31 * result + (liveViewResolution != null ? liveViewResolution.hashCode() : 0);
        result = 31 * result + (recordedResolution != null ? recordedResolution.hashCode() : 0);
        result = 31 * result + (externalId != null ? externalId.hashCode() : 0);
        result = 31 * result + (retentionPeriod != null ? retentionPeriod.hashCode() : 0);
        result = 31 * result + (liveViewFramerate != null ? liveViewFramerate.hashCode() : 0);
        result = 31 * result + (recordedFramerate != null ? recordedFramerate.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "videoSubscriptionByVideoSubscriptionId")
    public Collection<VideoCamera> getVideoCamerasByVideoSubscriptionId() {
        return videoCamerasByVideoSubscriptionId;
    }

    public void setVideoCamerasByVideoSubscriptionId(Collection<VideoCamera> videoCamerasByVideoSubscriptionId) {
        this.videoCamerasByVideoSubscriptionId = videoCamerasByVideoSubscriptionId;
    }

    @OneToMany(mappedBy = "videoSubscriptionByVideoSubscriptionId")
    public Collection<VideoProvider> getVideoProvidersByVideoSubscriptionId() {
        return videoProvidersByVideoSubscriptionId;
    }

    public void setVideoProvidersByVideoSubscriptionId(Collection<VideoProvider> videoProvidersByVideoSubscriptionId) {
        this.videoProvidersByVideoSubscriptionId = videoProvidersByVideoSubscriptionId;
    }
}
