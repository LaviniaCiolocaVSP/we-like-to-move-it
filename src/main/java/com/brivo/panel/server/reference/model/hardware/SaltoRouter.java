package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class SaltoRouter {
    private Long deviceId;
    private String address;
    private Integer port;
    private Boolean isIPV6;
    private List<SaltoLock> locks;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Boolean getIsIPV6() {
        return isIPV6;
    }

    public void setIsIPV6(Boolean isIPV6) {
        this.isIPV6 = isIPV6;
    }

    public List<SaltoLock> getLocks() {
        return locks;
    }

    public void setLocks(List<SaltoLock> locks) {
        this.locks = locks;
    }
}
