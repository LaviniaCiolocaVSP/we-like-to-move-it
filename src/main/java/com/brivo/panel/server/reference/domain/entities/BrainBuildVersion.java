package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "brain_build_version", schema = "brivo20", catalog = "onair")
public class BrainBuildVersion {
    private long brainBuildVersionId;
    private long brainId;
    private String firmwareVersion;
    private String buildVersion;
    private Timestamp updateDate;
    private Brain brainByObjectId;

    @Id
    @Column(name = "brain_build_version_id", nullable = false)
    public long getBrainBuildVersionId() {
        return brainBuildVersionId;
    }

    public void setBrainBuildVersionId(long brainBuildVersionId) {
        this.brainBuildVersionId = brainBuildVersionId;
    }

    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Column(name = "firmware_version", nullable = false)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Column(name = "build_version", nullable = false)
    public String getBuildVersion() {
        return buildVersion;
    }

    public void setBuildVersion(String buildVersion) {
        this.buildVersion = buildVersion;
    }

    @Column(name = "update_date", nullable = false)
    public Timestamp getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Timestamp updateDate) {
        this.updateDate = updateDate;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Brain.class)
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false,
            updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BrainBuildVersion that = (BrainBuildVersion) o;
        return brainId == that.brainId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(brainId);
    }

    @Override
    public String toString() {
        return "BrainBuildVersion{" +
                "brainId=" + brainId +
                ", firmwareVersion='" + firmwareVersion + '\'' +
                ", buildVersion='" + buildVersion + '\'' +
                '}';
    }
}
