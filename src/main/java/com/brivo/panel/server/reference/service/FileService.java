package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.FileNameDTO;
import com.brivo.panel.server.reference.service.account.AccountService;
import com.brivo.panel.server.reference.service.util.DownloadUtils;
import com.brivo.panel.server.reference.service.util.FolderUtils;
import org.h2.util.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.annotation.PostConstruct;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class FileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);

    private static final String PATH_SEPARATOR = File.separator;
    private static final Integer RPS_WEB_APP_PORT_NUMBER = 8080;

    @Value("${docs-folder}")
    private String docsFolder;

    @Value("${upload-folder.root}")
    protected String uploadRootFolder;

    @Value("${upload-folder.accounts}")
    protected String accountsFolder;

    private String accountsUploadFolder;

    @Value("${config-files-folder}")
    private String configFilesFolder;

    @Autowired
    private AccountService accountService;

    @Autowired
    private PanelRepository panelRepository;

    @PostConstruct
    public void initialize() {
        accountsUploadFolder = uploadRootFolder + PATH_SEPARATOR + accountsFolder;
        FolderUtils.checkFolderExistence(accountsUploadFolder, "accounts upload");
        //        FolderUtils.checkFolderExistence(docsFolder, "docs");
    }

    public MessageDTO uploadAccountFile(final MultipartFile file) {
        final String fileName = file.getOriginalFilename();
        LOGGER.debug("Uploading an account from the file '{}'...", fileName);

        try {
            file.transferTo(new File(accountsUploadFolder + PATH_SEPARATOR + file.getOriginalFilename()));
            LOGGER.debug("The file '{}' was successfully saved in '{}'", file.getOriginalFilename(), accountsUploadFolder);
            return new MessageDTO("The account file was successfully uploaded!");
        } catch (final Exception e) {
            LOGGER.error("An error occurred while reading AccountDTO from file...", e.getMessage());
            return new MessageDTO(e.getMessage());
        }
    }

    public MessageDTO uploadAndUseAccountFile(final MultipartFile file) {
        final String fileName = file.getOriginalFilename();
        LOGGER.debug("Uploading and using an account from the file '{}'...", fileName);

        try {
            file.transferTo(new File(accountsUploadFolder + PATH_SEPARATOR + file.getOriginalFilename()));
            LOGGER.debug("The file '{}' was successfully saved in '{}'", file.getOriginalFilename(), accountsUploadFolder);

            if (!fileName.endsWith(".json")) {
                LOGGER.debug("The uploaded file, {}, is not a JSON file", fileName);
                return new MessageDTO("The uploaded file, " + fileName + ", is not a JSON file");
            }

            String fileNameWithoutExtension = fileName.substring(0, fileName.length() - 5);
            LOGGER.debug("Filename without extension: {}", fileNameWithoutExtension);
            return this.accountService.setCurrentAccount(fileNameWithoutExtension);
        } catch (final Exception e) {
            LOGGER.error("An error occurred while reading AccountDTO from file and persisting it to database...", e.getMessage());
            return new MessageDTO(e.getMessage());
        }
    }

    public MessageDTO deleteAccountFile(String fileName) {
        try {
            final String filePath = accountsUploadFolder + PATH_SEPARATOR + fileName + ".json";
            Files.delete(Paths.get(filePath));
            return new MessageDTO("Account file deleted successfully!");
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalArgumentException("Cannot list the JSON accounts folder");
        }
    }

    public List<FileNameDTO> getAccountJSONFilesNames() {
        return getFileNames(accountsUploadFolder, "\\.json");
    }

    public List<FileNameDTO> getDocFilesNames() {
        return getFileNames(docsFolder, "\\.pdf");
    }

    private List<FileNameDTO> getFileNames(String folder, String extension) {
        Comparator<FileNameDTO> nameComparator = Comparator.comparing(FileNameDTO::getFileName);
        try {
            return Files.list(Paths.get(folder))
                        .map(file -> file.getFileName().toFile().getName())
                        .map(accountName -> new FileNameDTO(accountName.replaceFirst(extension, "")))
                        .sorted(nameComparator)
                        .collect(Collectors.toList());
        } catch (final IOException e) {
            LOGGER.warn(e.getMessage(), e);
            throw new IllegalArgumentException("Cannot list the JSON accounts folder");
        }
    }

    public Set<String> getConfigFilesNames() {
        try {
            final Stream<Path> path = Files.list(Paths.get(configFilesFolder));

            return path.map(file -> file.getFileName().toFile().getName())
                       .collect(Collectors.toSet());
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalArgumentException("Cannot list the uploaded config folder");
        }
    }

    public ResponseEntity<StreamingResponseBody> download(final String accountName, final boolean isAccount) {
        final String decodedAccountName = getDecodedAccountName(accountName);
        LOGGER.debug("Decoded account name: {}", decodedAccountName);

        final FileUrlResource accountResource = getAccountResource(decodedAccountName, isAccount);
        LOGGER.debug("Successfully retrieved accountResource file for the account '{}'", decodedAccountName);

        return ResponseEntity.ok()
                             .headers(DownloadUtils.buildHttpHeaders(accountResource))
                             .body(outputStream -> IOUtils.copy(accountResource.getInputStream(), outputStream));
    }

    private String getDecodedAccountName(final String accountName) {
        try {
            return java.net.URLDecoder.decode(accountName, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            LOGGER.error(e.getStackTrace().toString());
            return accountName;
        }
    }

    private FileUrlResource getAccountResource(final String accountName, final boolean isAccount) {
        final String filePath;

        if (isAccount) {
            filePath = getAccountFilePath(accountName);
        } else {
            filePath = getDocsFilePath(accountName);
        }

        try {
            return new FileUrlResource(filePath);
        } catch (MalformedURLException e) {
            LOGGER.error("An error occurred while building FileUrlResource from file... {}", e.getMessage());
            throw new IllegalArgumentException("Cannot download the file '" + filePath + "'");
        }
    }

    private String getAccountFilePath(final String accountName) {
        return accountsUploadFolder + PATH_SEPARATOR + accountName + ".json";
    }

    private String getDocsFilePath(final String docName) {
        return docsFolder + PATH_SEPARATOR + docName + ".pdf";
    }

    public ResponseEntity<StreamingResponseBody> downloadConfigFile(final String fileName) {
        final FileUrlResource accountResource;
        try {
            accountResource = new FileUrlResource(configFilesFolder + PATH_SEPARATOR + fileName);
            return ResponseEntity.ok()
                                 .headers(DownloadUtils.buildHttpHeaders(accountResource))
                                 .body(outputStream -> IOUtils.copy(accountResource.getInputStream(), outputStream));
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Cannot download the file '" + fileName + "'");
        }
    }

    public MessageDTO saveUpdatedConfigFile(final String fileName, final String fileContent) {
        try {
            final BufferedWriter writer =
                    new BufferedWriter(new FileWriter(configFilesFolder + PATH_SEPARATOR + fileName));

            writer.write(fileContent);

            writer.close();
        } catch (IOException e) {
            LOGGER.error("IOException occurred: ", e);
            return new MessageDTO("Error while updating config file " + fileName);
        }

        return new MessageDTO("Successfully updated config file " + fileName);
    }

    public MessageDTO generateConfigurationFilesBasedOnIP() {
        LOGGER.debug("Will replace the IP address in server.conf and run files");

        final String newIP = showIPAddressFromConfigFile();

        final String serverConfFileName = "server.conf";
        final String runFileName = "run";

        final Path serverConfFilePath = Paths.get(configFilesFolder + PATH_SEPARATOR + serverConfFileName);
        final Path runFilePath = Paths.get(configFilesFolder + PATH_SEPARATOR + runFileName);

        replaceIPInFile(serverConfFilePath, newIP);

        LOGGER.debug("The replacement in server.conf file was successfully done");

        return replaceIPInFile(runFilePath, newIP);
    }

    private String showIPAddressFromConfigFile() {
        final String IPFileName = "RPS_host_IP_address";
        final Path IPFilePath = Paths.get(configFilesFolder + PATH_SEPARATOR + IPFileName);
        try {
            final Stream<String> lines = Files.lines(IPFilePath);
            final Optional<String> IPAddress = lines.findFirst();

            LOGGER.info("The IP address stored in config folder is: {}", IPAddress.get());
            return IPAddress.get();
        } catch (IOException e) {
            LOGGER.error("IOException occurred: ", e);
        }

        return null;
    }

    private MessageDTO replaceIPInFile(final Path path, final String newIP) {
        try {
            final Stream<String> lines = Files.lines(path);
            final List<String> replaced =
                    lines.map(line -> line.replaceAll("192.168.1.201", newIP)).collect(Collectors.toList());
            Files.write(path, replaced);
            lines.close();

            return new MessageDTO("Successfully generated configuration file based on RPS IP " + newIP);
        } catch (IOException e) {
            LOGGER.error("IOException occurred: ", e);
            return new MessageDTO("Error while generating configuration file " + path.toString());
        }
    }

    @Transactional
    public void savePanelPassword(final String panelSerialNumber, final String panelPassword) {
        LOGGER.debug("savePanelPassword method start for panel {}", panelSerialNumber);

        final Brain panel = panelRepository.findByElectronicSerialNumber(panelSerialNumber).get();
        panel.setPassword(panelPassword);

        panelRepository.save(panel);

        LOGGER.debug("savePanelPassword method end");
    }

    public MessageDTO deleteConfigurationFile(String fileName) {
        LOGGER.debug("Deleting configuration file {}", fileName);

        try {
            final String filePath = configFilesFolder + PATH_SEPARATOR + fileName;

            Files.delete(Paths.get(filePath));
            LOGGER.debug("The configuration file {} was successfully deleted", fileName);

            return new MessageDTO("Configuration file deleted successfully!");
        } catch (final IOException e) {
            LOGGER.error("Cannot list the configuration folder ", e.getMessage(), e);
            throw new IllegalArgumentException("Cannot list the configuration folder");
        }
    }

    public MessageDTO uploadNewConfigFile(final MultipartFile file) {
        final String fileName = file.getOriginalFilename();
        LOGGER.debug("Uploading the configuration file {}", fileName);

        try {
            file.transferTo(new File(configFilesFolder + PATH_SEPARATOR + fileName));
            LOGGER.debug("The configuration file {} was successfully saved in {}", fileName,
                         configFilesFolder + PATH_SEPARATOR);
            return new MessageDTO("The configuration file was successfully uploaded!");
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
            return new MessageDTO("Error while uploading new configuration file");
        }
    }
}
