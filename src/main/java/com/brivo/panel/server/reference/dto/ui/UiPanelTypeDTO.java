package com.brivo.panel.server.reference.dto.ui;

public class UiPanelTypeDTO {
    private final Long paneTypeId;
    private final String panelTypeName;

    public UiPanelTypeDTO(Long paneTypeId, String panelTypeName) {
        this.paneTypeId = paneTypeId;
        this.panelTypeName = panelTypeName;
    }

    public Long getPaneTypeId() {
        return paneTypeId;
    }

    public String getPanelTypeName() {
        return panelTypeName;
    }
}
