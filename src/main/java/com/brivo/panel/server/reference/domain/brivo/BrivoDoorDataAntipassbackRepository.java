package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IDoorDataAntipassbackRepository;
import com.brivo.panel.server.reference.domain.entities.DoorDataAntipassback;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrivoDoorDataAntipassbackRepository extends IDoorDataAntipassbackRepository {

    Optional<DoorDataAntipassback> findByDeviceObjectId(@Param("deviceObjectId") long deviceObjectId);
}
