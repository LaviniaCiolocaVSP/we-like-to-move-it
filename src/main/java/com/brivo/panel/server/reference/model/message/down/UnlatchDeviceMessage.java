package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class UnlatchDeviceMessage extends DeviceAdministratorMessage {

    public UnlatchDeviceMessage() {
        super(DownstreamMessageType.Unlatch_Device);
    }

    public String toString() {
        return "UnlatchDeviceMessage : {objectId:" + this.deviceId + ", adminId:" + administratorId + "}";
    }

}
