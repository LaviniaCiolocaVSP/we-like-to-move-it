package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.List;

public class PanelScheduleData {
    private List<Schedule> schedules;
    private List<Holiday> holidays;
    private Instant lastUpdate;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public List<Schedule> getSchedules() {
        return schedules;
    }


    public void setSchedules(List<Schedule> schedules) {
        this.schedules = schedules;
    }


    public List<Holiday> getHolidays() {
        return holidays;
    }


    public void setHolidays(List<Holiday> holidays) {
        this.holidays = holidays;
    }


    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
