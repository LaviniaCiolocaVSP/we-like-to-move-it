package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "account_settings", schema = "brivo20", catalog = "onair")
public class AccountSettings {
    private long accountSettingsId;
    private String key;
    private String defaultValue;
    private long valueTypeId;
    private Timestamp created;
    private ValueType valueTypeByValueTypeId;
    private Collection<AccountSettingsValue> accountSettingsValuesByAccountSettingsId;

    @Id
    @Column(name = "account_settings_id", nullable = false)
    public long getAccountSettingsId() {
        return accountSettingsId;
    }

    public void setAccountSettingsId(long accountSettingsId) {
        this.accountSettingsId = accountSettingsId;
    }

    @Basic
    @Column(name = "key", nullable = false, length = 55)
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Basic
    @Column(name = "default_value", nullable = true, length = 250)
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Basic
    @Column(name = "value_type_id", nullable = false)
    public long getValueTypeId() {
        return valueTypeId;
    }

    public void setValueTypeId(long valueTypeId) {
        this.valueTypeId = valueTypeId;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountSettings that = (AccountSettings) o;

        if (accountSettingsId != that.accountSettingsId) {
            return false;
        }
        if (valueTypeId != that.valueTypeId) {
            return false;
        }
        if (key != null ? !key.equals(that.key) : that.key != null) {
            return false;
        }
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null) {
            return false;
        }
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountSettingsId ^ (accountSettingsId >>> 32));
        result = 31 * result + (key != null ? key.hashCode() : 0);
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (int) (valueTypeId ^ (valueTypeId >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "value_type_id", referencedColumnName = "value_type_id", nullable = false, insertable = false, updatable = false)
    public ValueType getValueTypeByValueTypeId() {
        return valueTypeByValueTypeId;
    }

    public void setValueTypeByValueTypeId(ValueType valueTypeByValueTypeId) {
        this.valueTypeByValueTypeId = valueTypeByValueTypeId;
    }

    @OneToMany(mappedBy = "accountSettingsByAccountSettingsId")
    public Collection<AccountSettingsValue> getAccountSettingsValuesByAccountSettingsId() {
        return accountSettingsValuesByAccountSettingsId;
    }

    public void setAccountSettingsValuesByAccountSettingsId(Collection<AccountSettingsValue> accountSettingsValuesByAccountSettingsId) {
        this.accountSettingsValuesByAccountSettingsId = accountSettingsValuesByAccountSettingsId;
    }
}
