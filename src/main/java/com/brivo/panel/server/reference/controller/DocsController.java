package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.ui.FileNameDTO;
import com.brivo.panel.server.reference.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.List;

@RestController
@RequestMapping("/docs")
public class DocsController {

    private final FileService fileService;

    @Autowired
    public DocsController(final FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<FileNameDTO> getDocFileNames() {
        return fileService.getDocFilesNames();
    }

    @GetMapping(
            path = "/download/{docName}",
            produces = MediaType.APPLICATION_PDF_VALUE
    )
    public ResponseEntity<StreamingResponseBody> downloadDoc(@PathVariable final String docName) {
        return fileService.download(docName, false);
    }
}
