package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.ICredentialFieldRepository;

public interface CredentialFieldRepository extends ICredentialFieldRepository {
}
