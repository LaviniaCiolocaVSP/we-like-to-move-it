package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface BrivoAccountRepository extends CrudRepository<Account, Long> {

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO(acc.accountId, acc.name)" +
                    "FROM Account acc " +
                    "WHERE UPPER(acc.name) LIKE CONCAT('%',UPPER(:nameFilter),'%') " +
                    "AND acc.name IN ('CSR ACCOUNT', 'Angry Birds', 'BaconIsTheBest', 'Dan''s Account', 'Dream Force " +
                    "One', 'Epsilon', 'Mr Data', 'Nappy House', 'Simulated Widgets', 'Tyco Admin', 'VSP Panel team'," +
                    "'XML Monkeys')",
            countQuery = "SELECT COUNT(acc.accountId) " +
                    "FROM Account acc " +
                    "WHERE UPPER(acc.name) LIKE CONCAT('%',UPPER(:nameFilter),'%') " +
                     "AND acc.name IN ('CSR ACCOUNT', 'Angry Birds', 'BaconIsTheBest', 'Dan''s Account', 'Dream Force " +
                     "One', 'Epsilon', 'Mr Data', 'Nappy House', 'Simulated Widgets', 'Tyco Admin', 'VSP Panel team'," +
                     "'XML Monkeys')"
    )
    List<UiAccountSummaryDTO> getAccountNamesAndIds(@Param("nameFilter")String nameFilter, Pageable pageable);
}
