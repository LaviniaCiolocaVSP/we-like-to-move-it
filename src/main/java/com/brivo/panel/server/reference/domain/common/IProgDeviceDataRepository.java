package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.ProgDeviceData;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface IProgDeviceDataRepository extends CrudRepository<ProgDeviceData, Long> {
    @Query(
            value = "SELECT * " +
                    "FROM brivo20.prog_device_data " +
                    "WHERE device_oid = :deviceOid",
            nativeQuery = true
    )
    ProgDeviceData findByDeviceOid(@Param("deviceOid") long deviceOid);

}
