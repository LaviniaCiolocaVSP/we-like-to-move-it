package com.brivo.panel.server.reference.dao.usergroup;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum UserGroupQuery implements FileQuery {
    SELECT_USER_GROUPS;

    private String query;

    UserGroupQuery() {
        this.query = QueryUtils.getQueryText(UserGroupQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}
