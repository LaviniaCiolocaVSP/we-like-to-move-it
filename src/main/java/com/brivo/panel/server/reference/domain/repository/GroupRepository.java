package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.dto.ui.UiGroupDTO;
import com.brivo.panel.server.reference.dto.ui.UiGroupSummaryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface GroupRepository extends CrudRepository<SecurityGroup, Long> {

    @Query(
            value = "SELECT s.* FROM brivo20.security_group s WHERE s.account_id = :accountId and s.disabled = 0",
            nativeQuery = true
    )
    List<SecurityGroup> findByAccountId(@Param("accountId") long accountId);
    
    @Query(
            value = "SELECT s.* FROM brivo20.security_group s WHERE s.account_id = :accountId and s.security_group_type_id = 1 and s.disabled = 0",
            nativeQuery = true
    )
    List<SecurityGroup> findDeviceGroupsByAccountId(@Param("accountId") long accountId);
    
    
    @Query(
            value = "SELECT coalesce(max(s.security_group_id),0) FROM brivo20.security_group s",
            nativeQuery = true
    )
    Optional<Long> getMaxGroupId();
    
    @Query(
            value = "SELECT u.* FROM brivo20.security_group u WHERE u.object_id = :objectId",
            nativeQuery = true
    )
    Optional<SecurityGroup> findByObjectId(@Param("objectId") long objectId);
    
    @Query(
            value = "select count(*) from security_group g where g.security_group_type_id = 2 and g.account_id = :account_id and g.disabled = 0",
            nativeQuery = true
    )
    long getGroupCountForAccount(@Param("account_id") long accountId);
    
    @Query(
            value = "SELECT s.* FROM brivo20.security_group s WHERE s.object_id in (select actor_object_id from brivo20.object_permission where object_id = :deviceObjId ) " +
                    "and s.disabled = 0",
            nativeQuery = true
    )
    List<SecurityGroup> findByDeviceObjId(@Param("deviceObjId") long deviceObjId);

    @Query(
            value = "SELECT s.name FROM brivo20.security_group s WHERE s.object_id = :siteObjectId",
            nativeQuery = true
    )
    String getSiteNameBySiteObjectId(@Param("siteObjectId") long siteObjectId);

    @Query(
            value = "SELECT s.* FROM brivo20.security_group s WHERE s.account_id = :accountId AND s.security_group_type_id = 0",
            nativeQuery = true
    )
    Optional<SecurityGroup> getRootGroupForAccount(@Param("accountId") long accountId);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiGroupDTO(sg.objectId, sg.securityGroupId, sg.name, sg.description, count(sgm.securityGroupId)) " +
                    "FROM SecurityGroup sg " +
                    "LEFT OUTER JOIN SecurityGroupMember sgm ON sg.securityGroupId = sgm.securityGroupId " +
                    "WHERE sg.accountId = :accountId " +
                    "AND sg.securityGroupTypeId = 2 " +
                    "AND sg.disabled = 0 " +
                    "GROUP BY sg.objectId, sg.securityGroupId, sgm.securityGroupId, sg.name, sg.description " +
                    "ORDER BY sg.securityGroupId DESC"
    )
    Collection<UiGroupDTO> findUiGroupDTOSByAccountId(@Param("accountId") Long accountId);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiGroupSummaryDTO(sg.securityGroupId,  sg.name, sg.objectId) " +
                    "FROM SecurityGroup sg " +
                    "WHERE sg.accountId = :accountId " +
                    "AND sg.securityGroupTypeId = 2 " +
                    "AND sg.disabled = 0 " +
                    "ORDER BY sg.securityGroupId DESC"
    )
    Collection<UiGroupSummaryDTO> getUiGroupSummaryForAccount(@Param("accountId") Long accountId);

    
}
