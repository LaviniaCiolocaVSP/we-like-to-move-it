package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.model.event.EventResolutionData;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component
public class CredentialEventExtractor
        implements ResultSetExtractor<List<EventResolutionData>> {

    @Override
    public List<EventResolutionData> extractData(ResultSet rs)
            throws SQLException, DataAccessException {

        List<EventResolutionData> list = new ArrayList<>();

        while (rs.next()) {
            Long ownerId = rs.getLong("owner_object_id");
            Long lastOwnerId = rs.getLong("last_owner_object_id");

            Integer disabledInt = rs.getInt("disabled");
            Boolean disabled = false;
            if (disabledInt != null && disabledInt == 1) {
                disabled = true;
            }

            String refId = rs.getString("reference_id");
            Date enableDate = rs.getTimestamp("enable_on");
            Date expiresDate = rs.getTimestamp("expires");

            Instant active = (enableDate == null) ? null : enableDate.toInstant();
            Instant expires = (expiresDate == null) ? null : expiresDate.toInstant();

            EventResolutionData data = new EventResolutionData();
            data.setOwnerId(ownerId);
            data.setLastOwnerId(lastOwnerId);
            data.setDisabled(disabled);
            data.setReferenceId(refId);
            data.setEnableOn(active);
            data.setExpires(expires);

            list.add(data);
        }

        return list;
    }

}
