package com.brivo.panel.server.reference.model.schedule;

public enum ScheduleExceptionRepeatType {
    NONE(-1),
    ONE_TIME_EXCEPTION(0),
    RECURRING_EXCEPTION(1);

    private int typeId;

    ScheduleExceptionRepeatType(int type) {
        this.typeId = type;
    }

    public static ScheduleExceptionRepeatType getScheduleExceptionRepeatType(int typeId) {
        ScheduleExceptionRepeatType type = ScheduleExceptionRepeatType.NONE;

        if (typeId == 0) {
            type = ScheduleExceptionRepeatType.ONE_TIME_EXCEPTION;
        } else if (typeId == 1) {
            type = ScheduleExceptionRepeatType.RECURRING_EXCEPTION;
        }

        return type;
    }

    public int getScheduleExceptionRepeatTypeId() {
        return this.typeId;
    }
}
