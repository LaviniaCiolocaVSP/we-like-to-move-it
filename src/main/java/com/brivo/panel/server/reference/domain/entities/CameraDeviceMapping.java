package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "camera_device_mapping", schema = "brivo20", catalog = "onair")
public class CameraDeviceMapping {
    private long cameraDeviceMappingId;
    private long cameraId;
    private long deviceId;
    private OvrCamera ovrCameraByCameraId;
//    private Device deviceByDeviceId;

    @Id
    @Column(name = "camera_device_mapping_id", nullable = false)
    public long getCameraDeviceMappingId() {
        return cameraDeviceMappingId;
    }

    public void setCameraDeviceMappingId(long cameraDeviceMappingId) {
        this.cameraDeviceMappingId = cameraDeviceMappingId;
    }

    @Basic
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraDeviceMapping that = (CameraDeviceMapping) o;

        if (cameraDeviceMappingId != that.cameraDeviceMappingId) {
            return false;
        }
        if (cameraId != that.cameraId) {
            return false;
        }
        return deviceId == that.deviceId;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraDeviceMappingId ^ (cameraDeviceMappingId >>> 32));
        result = 31 * result + (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (int) (deviceId ^ (deviceId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id", nullable = false, insertable = false, updatable = false)
    public OvrCamera getOvrCameraByCameraId() {
        return ovrCameraByCameraId;
    }

    public void setOvrCameraByCameraId(OvrCamera ovrCameraByCameraId) {
        this.ovrCameraByCameraId = ovrCameraByCameraId;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "device_id", nullable = false, insertable = false, updatable = false)
    public Device getDeviceByDeviceId() {
        return deviceByDeviceId;
    }

    public void setDeviceByDeviceId(Device deviceByDeviceId) {
        this.deviceByDeviceId = deviceByDeviceId;
    }
    */
}
