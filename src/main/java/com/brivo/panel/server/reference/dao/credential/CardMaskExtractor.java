package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.model.credential.CardMask;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.math.BigInteger;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Card Mask Extractor
 *
 * @author brandon
 */
@Component
public class CardMaskExtractor
        implements ResultSetExtractor<List<CardMask>> {
    @Override
    public List<CardMask> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<CardMask> list = new ArrayList<>();

        while (rs.next()) {
            int maskLength = rs.getInt("mask_length");
            String maskStr = rs.getString("card_mask");

            String maskHexStr = new BigInteger(maskStr, 2).toString(16);

            CardMask cardMask = new CardMask(maskLength, maskHexStr);

            list.add(cardMask);
        }

        return list;
    }
}
