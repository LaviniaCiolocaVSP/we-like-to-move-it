package com.brivo.panel.server.reference.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

import static com.brivo.panel.server.reference.config.AbstractDataSourceConfig.ENTITIES_PACKAGE;

@Configuration
@EntityScan(basePackages = ENTITIES_PACKAGE)
@EnableJpaRepositories(basePackages = "com.brivo.panel.server.reference.domain.repository")
@EnableTransactionManagement
public class DataSourceConfig extends AbstractDataSourceConfig {

    private static final int AVAILABLE_PROCESSORS = Runtime.getRuntime().availableProcessors();

    @Value("${spring.datasource.username}")
    private String userName;

    @Value("${spring.datasource.password}")
    private String password;

    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    @Value("${spring.datasource.url}")
    private String dataSourceURL;

    @Value("${spring.jpa.properties.hibernate.dialect}")
    private String hibernateDialect;

    @Bean
    @Primary
    public DataSource dataSource() {
        final HikariConfig hikariConfig = new HikariConfig();

        hikariConfig.setPoolName("reference-panel-server-connection-pool");
        // hikariConfig.setMaximumPoolSize(getMaxPoolSize());
        hikariConfig.setMaximumPoolSize(10);
        hikariConfig.setMinimumIdle(1);
        hikariConfig.setConnectionTimeout(5000);    // 5 seconds
        hikariConfig.setIdleTimeout(60_000);        // 1 min
        hikariConfig.setMaxLifetime(300_000);       // 5 minutes
        hikariConfig.setJdbcUrl(dataSourceURL);
        hikariConfig.setUsername(userName);
        hikariConfig.setPassword(password);
        hikariConfig.setDriverClassName(driverClassName);
        hikariConfig.setSchema(DEFAULT_SCHEMA_NAME);

        return new HikariDataSource(hikariConfig);
    }

    /*
    private int getMaxPoolSize() {
        return AVAILABLE_PROCESSORS > 0 && AVAILABLE_PROCESSORS % 2 == 0 ? AVAILABLE_PROCESSORS / 2 : 4;
    }
     */
    @Bean
    public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
        return new NamedParameterJdbcTemplate(dataSource());
    }

    @Bean
    @Primary
    public EntityManagerFactory entityManagerFactory() {
        final Properties properties = new Properties();
        properties.setProperty("hibernate.hibernateDialect", hibernateDialect);

        final LocalContainerEntityManagerFactoryBean emf = new LocalContainerEntityManagerFactoryBean();
        emf.setDataSource(dataSource());
        emf.setJpaVendorAdapter(jpaVendorAdapter);
        emf.setPackagesToScan(ENTITIES_PACKAGE);
        emf.setPersistenceUnitName("mainPersistenceUnit");
        emf.setJpaProperties(properties);
        emf.afterPropertiesSet();

        return emf.getObject();
    }

    @Bean
    @Primary
    public EntityManager entityManager() {
        return entityManagerFactory().createEntityManager();
    }

    @Bean
    @Primary
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(entityManagerFactory());
    }
}
