package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.constants.Definitions;
import com.brivo.panel.server.reference.dao.device.DeviceDao;
import com.brivo.panel.server.reference.domain.common.IDevicePropertyRepository;
import com.brivo.panel.server.reference.domain.common.IDoorDataAntipassbackRepository;
import com.brivo.panel.server.reference.domain.common.IDoorDataRepository;
import com.brivo.panel.server.reference.domain.common.IProgDeviceDataRepository;
import com.brivo.panel.server.reference.domain.common.IProgDevicePutsRepository;
import com.brivo.panel.server.reference.domain.common.ISecurityGroupRepository;
import com.brivo.panel.server.reference.domain.entities.Board;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.DeviceProperty;
import com.brivo.panel.server.reference.domain.entities.DeviceSchedule;
import com.brivo.panel.server.reference.domain.entities.DeviceType;
import com.brivo.panel.server.reference.domain.entities.DeviceTypes;
import com.brivo.panel.server.reference.domain.entities.DoorData;
import com.brivo.panel.server.reference.domain.entities.DoorDataAntipassback;
import com.brivo.panel.server.reference.domain.entities.ProgDeviceData;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.domain.entities.Schedule;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.repository.DevicePropertyRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceScheduleRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceTypeRepository;
import com.brivo.panel.server.reference.domain.repository.DoorDataRepository;
import com.brivo.panel.server.reference.domain.repository.ProgDeviceDataRepository;
import com.brivo.panel.server.reference.domain.repository.ProgDevicePutsRepository;
import com.brivo.panel.server.reference.dto.DeviceDTO;
import com.brivo.panel.server.reference.dto.DoorDTO;
import com.brivo.panel.server.reference.dto.DoorDataAntipassbackDTO;
import com.brivo.panel.server.reference.dto.DoorDataDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ProgDeviceDataDTO;
import com.brivo.panel.server.reference.dto.ProgDevicePutDTO;
import com.brivo.panel.server.reference.dto.ui.UIValidCredentialDTO;
import com.brivo.panel.server.reference.dto.ui.UiDeviceDTO;
import com.brivo.panel.server.reference.dto.ui.UiDoorDTO;
import com.brivo.panel.server.reference.dto.ui.UiPhysicalPointDTO;
import com.brivo.panel.server.reference.dto.ui.UiProgDeviceDTO;
import com.brivo.panel.server.reference.dto.ui.UiSwitchDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.BLUETOOTH_READER_ID_DEVICE_TYPE_PROPERTY;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_SHORT_VALUE;
import static com.brivo.panel.server.reference.service.util.ConverterUtils.convertBooleanToShort;
import static com.brivo.panel.server.reference.service.util.ConverterUtils.convertShortToBoolean;


@Service
public class UiDeviceService extends CommonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiDeviceService.class);

    private final DeviceRepository deviceRepository;
    private final DoorDataRepository doorDataRepository;
    private final DevicePropertyRepository devicePropertyRepository;
    private final DeviceScheduleRepository deviceScheduleRepository;
    private final DeviceTypeRepository deviceTypeRepository;
    private final ProgDeviceDataRepository progDeviceDataRepository;
    private final ProgDevicePutsRepository progDevicePutsRepository;
    private final DeviceDao deviceDao;

    private final UiBoardService boardService;
    private final UiObjectService objectService;
    private final UiScheduleService scheduleService;
    private final GroupService groupService;
    private final ProgIoPointService progIoPointService;

    private static final int INGRESS = 1;
    private static final int EGRESS = 2;
    private static final int NEITHER = 0;

    private static final short SCHEDULE_ENABLED = 1;
    private static final short SCHEDULE_DISABLED = 0;

    private static final Long PROG_DEVICE_FIELD_NOT_SET_LONG = 0L;
    private static final Short PROG_DEVICE_FIELD_NOT_SET_SHORT = 0;
    private static final Long DEFAULT_TWO_FACTOR_INTERVAL_VALUE = 3L;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    public UiDeviceService(final DeviceRepository deviceRepository, final DeviceDao deviceDao,
                           final UiBoardService boardService, final DoorDataRepository doorDataRepository,
                           final DevicePropertyRepository devicePropertyRepository, final UiScheduleService scheduleService,
                           final DeviceTypeRepository deviceTypeRepository,
                           final UiObjectService objectService, final DeviceScheduleRepository deviceScheduleRepository,
                           final GroupService groupService, final ProgIoPointService progIoPointService,
                           final ProgDeviceDataRepository progDeviceDataRepository,
                           final ProgDevicePutsRepository progDevicePutsRepository) {
        this.deviceRepository = deviceRepository;
        this.deviceDao = deviceDao;
        this.boardService = boardService;
        this.doorDataRepository = doorDataRepository;
        this.devicePropertyRepository = devicePropertyRepository;
        this.scheduleService = scheduleService;
        this.deviceTypeRepository = deviceTypeRepository;
        this.objectService = objectService;
        this.deviceScheduleRepository = deviceScheduleRepository;
        this.progDeviceDataRepository = progDeviceDataRepository;
        this.groupService = groupService;
        this.progIoPointService = progIoPointService;
        this.progDevicePutsRepository = progDevicePutsRepository;
    }

    public Collection<DoorDTO> getDoorDTOS(final Collection<Device> doors,
                                           final IDoorDataRepository dynamicDoorDataRepository,
                                           final IDoorDataAntipassbackRepository dynamicDoorDataAntipassbackRepository,
                                           final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                           final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                           final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                           final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        return doors.stream()
                    .map(convertDoor(dynamicDoorDataRepository, dynamicDoorDataAntipassbackRepository,
                            dynamicDevicePropertyRepository, dynamicSecurityGroupRepository,
                            dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository))
                    .collect(Collectors.toSet());
    }

    public Collection<DeviceDTO> getDeviceDTOS(final Collection<Device> devices,
                                               final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                               final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                               final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                               final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        return devices.stream()
                      .map(convertDevice(dynamicDevicePropertyRepository, dynamicSecurityGroupRepository,
                              dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository))
                      .collect(Collectors.toSet());
    }

    private Function<Device, DoorDTO> convertDoor(final IDoorDataRepository dynamicDoorDataRepository,
                                                  final IDoorDataAntipassbackRepository dynamicDoorDataAntipassbackRepository,
                                                  final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                                  final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                                  final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                                  final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        return door -> {
            final DeviceDTO deviceDTO = convertBasicDevice(dynamicDevicePropertyRepository, dynamicSecurityGroupRepository,
                    dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository).apply(door);

            final DoorDataAntipassback doorDataAntipassback = getDoorDataAntipassback(door.getObjectByBrivoObjectId().getObjectId(),
                    dynamicDoorDataAntipassbackRepository);
            final DoorDataAntipassbackDTO doorDataAntipassbackDTO = convertDoorDataAntipassback().apply(doorDataAntipassback);

            final DoorData doorData = getDoorData(door.getObjectByBrivoObjectId().getObjectId(), dynamicDoorDataRepository);
            final DoorDataDTO doorDataDTO = convertDoorData().apply(doorData);

            return new DoorDTO(deviceDTO.getId(), deviceDTO.getObjectId(), deviceDTO.getBrainId(), deviceDTO.getSiteIds(),
                    deviceDTO.getDeviceName(), deviceDTO.getPanelId(), deviceDTO.getBluetoothReaderId(),
                    deviceDTO.getMaximumREXExtension(), deviceDTO.getIsIngress(), deviceDTO.getIsEgress(),
                    deviceDTO.getDeviceUnlockScheduleIds(), deviceDTO.getTwoFactorSchedule(), deviceDTO.getTwoFactorInterval(),
                    deviceDTO.getCardRequiredSchedule(), deviceDTO.getControlFromBrowser(), deviceDTO.getDeleted(),
                    deviceDTO.getProgDevicePuts(), deviceDTO.getProgDeviceData(), doorDataDTO, doorDataAntipassbackDTO);
        };
    }

    private DoorData getDoorData(long deviceObjectId, final IDoorDataRepository dynamicDoorDataRepository) {
        return dynamicDoorDataRepository.findByObjectId(deviceObjectId)
                                        .orElse(getNotAvailableDoorData());
    }

    private DoorData getDoorDataOrThrow(long deviceObjectId) {
        return doorDataRepository.findByObjectId(deviceObjectId)
                                 .orElseThrow(() -> new IllegalArgumentException(
                                         "Door data with device object id " + deviceObjectId + " not found in the database"));
    }

    private DoorData getNotAvailableDoorData() {
        DoorData doorData = new DoorData();

        doorData.setBoardNum(NOT_AVAILABLE_LONG_VALUE);
        doorData.setNodeNum(NOT_AVAILABLE_LONG_VALUE);

        return doorData;
    }

    private Function<Device, DeviceDTO> convertDevice(final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                                      final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                                      final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                                      final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        return device -> {
            DeviceDTO deviceDTO = convertBasicDevice(dynamicDevicePropertyRepository, dynamicSecurityGroupRepository,
                    dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository).apply(device);

            deviceDTO = addProgDevicePutsAndDataToDeviceDTO(deviceDTO, dynamicProgDeviceDataRepository, dynamicProgDevicePutsRepository);

            return deviceDTO;
        };
    }

    private DeviceDTO addProgDevicePutsAndDataToDeviceDTO(final DeviceDTO deviceDTO, final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                                          final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        final Long deviceObjectId = deviceDTO.getObjectId();

        final Collection<ProgDevicePuts> progDevicePuts = dynamicProgDevicePutsRepository.findByDeviceOid(deviceObjectId);
        final Collection<ProgDevicePutDTO> progDevicePutDTOS = convertProgDevicePuts(progDevicePuts);

        final Collection<ProgDeviceData> progDeviceData = Collections.singletonList(dynamicProgDeviceDataRepository.findByDeviceOid(deviceObjectId));
        final Collection<ProgDeviceDataDTO> progDeviceDataDTOS = convertProgDeviceData(progDeviceData);

        deviceDTO.setProgDevicePuts(progDevicePutDTOS);
        deviceDTO.setProgDeviceData(progDeviceDataDTOS);

        return deviceDTO;
    }

    private Function<Device, DeviceDTO> convertBasicDevice(final IDevicePropertyRepository dynamicDevicePropertyRepository,
                                                           final ISecurityGroupRepository dynamicSecurityGroupRepository,
                                                           final IProgDeviceDataRepository dynamicProgDeviceDataRepository,
                                                           final IProgDevicePutsRepository dynamicProgDevicePutsRepository) {
        return device -> {
            final long deviceObjectId = device.getObjectByBrivoObjectId().getObjectId();

            final Optional<DeviceProperty> bluetoothReaderId = getDeviceProperty(device,
                    BLUETOOTH_READER_ID_DEVICE_TYPE_PROPERTY, dynamicDevicePropertyRepository);
            final Short maxRexExtension = getMaxRexExtensionForDevice(device, dynamicDevicePropertyRepository);

            final Collection<Long> deviceUnlockScheduleIds = getDeviceUnlockScheduleIds(device);
            final Collection<BigInteger> siteIds = getSiteIdsForDevice(deviceObjectId, dynamicSecurityGroupRepository);

            return new DeviceDTO(device.getDeviceId(), device.getObjectByBrivoObjectId().getObjectId(),
                    device.getBrainByBrainId().getBrainId(), device.getDeviceTypeByDeviceTypeId().getDeviceTypeId(),
                    siteIds, getOptionalValue(device.getName()), device.getPanelId(),
                    getDevicePropertyValue(bluetoothReaderId, String.class),
                    maxRexExtension, device.getIsIngress(),
                    device.getIsEgress(), deviceUnlockScheduleIds, getOptionalLongValue(device.getTwoFactorScheduleId()),
                    getOptionalLongValue(device.getTwoFactorInterval()), getOptionalLongValue(device.getCardRequiredScheduleId()),
                    device.getEnableLiveControl(), device.getDeleted(), Collections.emptyList(), Collections.emptyList());
        };
    }

    private Short getMaxRexExtensionForDevice(final Device device,
                                              final IDevicePropertyRepository dynamicDevicePropertyRepository) {
        final Optional<DeviceProperty> maxRexExtension = getDeviceProperty(device,
                MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY, dynamicDevicePropertyRepository);

        return getDevicePropertyValue(maxRexExtension, Short.class);
    }

    private Collection<ProgDevicePutDTO> convertProgDevicePuts(final Collection<ProgDevicePuts> progDevicePuts) {
        if (progDevicePuts == null || progDevicePuts.size() == 0)
            return Collections.emptyList();

        else
            return progDevicePuts.stream()
                                 .map(convertProgDevicePut())
                                 .collect(Collectors.toSet());
    }

    private Function<ProgDevicePuts, ProgDevicePutDTO> convertProgDevicePut() {
        return progDevicePuts -> new ProgDevicePutDTO(progDevicePuts.getPanelOid(), progDevicePuts.getBoardNumber(),
                progDevicePuts.getPointAddress(), progDevicePuts.getBehavior(), progDevicePuts.getArgument());
    }

    private Collection<ProgDeviceDataDTO> convertProgDeviceData(final Collection<ProgDeviceData> progDeviceData) {
        if (progDeviceData == null || progDeviceData.size() == 0)
            return Collections.emptyList();

        else
            return progDeviceData.stream()
                                 .filter(progDeviceData1 -> progDeviceData1 != null)
                                 .map(convertSingleProgDeviceData())
                                 .collect(Collectors.toSet());
    }

    private Function<ProgDeviceData, ProgDeviceDataDTO> convertSingleProgDeviceData() {
        return progDeviceData -> new ProgDeviceDataDTO(progDeviceData.getNotifyFlag(), progDeviceData.getNotifyDiseng(),
                progDeviceData.getEngageMsg(), progDeviceData.getDisengageMsg(), progDeviceData.getDoorId(),
                progDeviceData.getEventType());
    }

    private Collection<Long> getDeviceUnlockScheduleIds(final Device device) {
        return device.getDeviceSchedulesByDeviceId()
                     .stream()
                     .map(deviceSchedule -> deviceSchedule.getScheduleId())
                     .collect(Collectors.toSet());
    }

    private Schedule getFirstDeviceScheduleForDevice(final Device device) {
        Optional<DeviceSchedule> deviceSchedule = device.getDeviceSchedulesByDeviceId()
                                                        .stream()
                                                        .findFirst();

        if (deviceSchedule.isPresent()) {
            return deviceSchedule.get()
                                 .getScheduleByScheduleId();
        } else {
            return getNotAvailableSchedule();
        }
    }

    private Schedule getTwoFactorScheduleForDevice(final Device device) {
        final Long scheduleId = device.getTwoFactorScheduleId();

        final Optional<Schedule> schedule = scheduleService.findScheduleById(scheduleId);

        return schedule.orElse(getNotAvailableSchedule());
    }

    private Schedule getCardRequiredScheduleForDevice(final Device device) {
        final Long scheduleId = device.getCardRequiredScheduleId();

        final Optional<Schedule> schedule = scheduleService.findScheduleById(scheduleId);

        return schedule.orElse(getNotAvailableSchedule());
    }

    private Schedule getNotAvailableSchedule() {
        Schedule notAvailableSchedule = new Schedule();

        notAvailableSchedule.setScheduleId(NOT_AVAILABLE_LONG_VALUE);
        notAvailableSchedule.setName("NONE");

        return notAvailableSchedule;
    }

    private Optional<DeviceProperty> getDeviceProperty(final Device device, final String property,
                                                       final IDevicePropertyRepository dynamicDevicePropertyRepository) {
        return dynamicDevicePropertyRepository.findByDeviceId(device.getDeviceId(), property);
    }

    public  Boolean isValidDeviceObjectId(final Long deviceObjectId) {
        Optional<Device> device = deviceRepository.findByObjectId(deviceObjectId);

        return device.isPresent();
    }

    private <T> T getDevicePropertyValue(final Optional<DeviceProperty> deviceProperty, final Class<T> returnType) {
        final boolean returningString = returnType.equals(String.class);
        if (deviceProperty.isPresent()) {
            final String dpValue = deviceProperty.get().getValue();
            return returningString ? returnType.cast(dpValue) : returnType.cast(Short.parseShort(dpValue));
        } else {
            return returningString ? returnType.cast(NOT_AVAILABLE) : returnType.cast(NOT_AVAILABLE_SHORT_VALUE);
        }
    }

    private Function<DoorData, DoorDataDTO> convertDoorData() {
        return doorData -> new DoorDataDTO(doorData.getBoardNum(), doorData.getNodeNum(), doorData.getOsdpPdAddress(),
                doorData.getNotifyOnAjar(), doorData.getDoorAjar(), doorData.getMaxInvalid(), doorData.getInvalidShutout(),
                doorData.getPassthrough(), doorData.getDebouncePeriod(), doorData.getUseLockOnOpen(),
                doorData.getLockOnOpenDelay(), doorData.getNotifyOnForced(), doorData.getRexFiresDoor(),
                doorData.getUseAlarmShunt(), doorData.getAlarmDelay(), doorData.getScheduleEnabled(),
                doorData.getCacheCredTimeLimit(), doorData.getEnablePrivacyMode(), doorData.getInvalidCredWin());
    }

    private Function<DoorDataAntipassback, DoorDataAntipassbackDTO> convertDoorDataAntipassback() {
        return doorDataAntipassback -> new DoorDataAntipassbackDTO(doorDataAntipassback.getDoorDataAntipassbackId(),
                doorDataAntipassback.getZone(), doorDataAntipassback.getAltReaderPanelOid(),
                doorDataAntipassback.getAltReaderBoardNum(), doorDataAntipassback.getAltReaderPointAddress(),
                doorDataAntipassback.getAltZone());
    }

    private Collection<BigInteger> getSiteIdsForDevice(final Long deviceObjectId,
                                                       final ISecurityGroupRepository dynamicSecurityGroupRepository) {
        return dynamicSecurityGroupRepository.findByDeviceObjectId(deviceObjectId)
                                             .orElse(Collections.emptyList());
    }

    private DoorDataAntipassback getDoorDataAntipassback(final long deviceObjectId,
                                                         final IDoorDataAntipassbackRepository dynamicDoorDataAntipassbackRepository) {
        return dynamicDoorDataAntipassbackRepository.findByDeviceObjectId(deviceObjectId)
                                                    .orElse(getNotAvailableDoorDataAntipassback());
    }

    private DoorDataAntipassback getNotAvailableDoorDataAntipassback() {
        DoorDataAntipassback doorDataAntipassback = new DoorDataAntipassback();

        doorDataAntipassback.setDoorDataAntipassbackId(NOT_AVAILABLE_LONG_VALUE);

        return doorDataAntipassback;
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiDoorDTO> getUiDoorDtosForAccount(final Long accountId) {
        return deviceRepository.findByAccountId(accountId)
                               .orElse(Collections.emptyList())
                               .stream()
                               .filter(isValidDoor())
                               .map(convertDoorUiDTO())
                               .sorted(uiDoorDTOComparator)
                               .collect(Collectors.toList());
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiDoorDTO> getUiDoorDTOSForPanel(final Long panelOid) {
        return deviceRepository.findValidDoorsByPanelOid(panelOid)
                               .orElse(Collections.emptyList())
                               .stream()
                               .map(convertDoorUiDTO())
                               .sorted(uiDoorDTOComparator)
                               .collect(Collectors.toList());
    }

    Comparator<UiDoorDTO> uiDoorDTOComparator = new Comparator<UiDoorDTO>() {
        @Override
        public int compare(UiDoorDTO o1, UiDoorDTO o2) {
            return o2.getDeviceId().compareTo(o1.getDeviceId());
        }
    };

    private Predicate<Device> isValidDoor() {
        return device -> device.getDeviceTypeByDeviceTypeId().getDeviceTypeId() == DeviceTypes.DOOR_KEYPAD.getDeviceTypeID()
                && device.getDeleted() == 0;
    }

    private Predicate<Device> isValidDevice() {
        return device -> device.getDeviceTypeByDeviceTypeId().getDeviceTypeId() != DeviceTypes.DOOR_KEYPAD.getDeviceTypeID()
                && device.getDeleted() == 0;
    }

    private Function<Device, UiDoorDTO> convertDoorUiDTO() {
        return door -> {
            final Brain brainByBrainId = door.getBrainByBrainId();
            final long deviceObjectId = door.getObjectByBrivoObjectId().getObjectId();
            final DoorData doorData = getDoorData(deviceObjectId, doorDataRepository);
            final Long boardNumber = doorData.getBoardNum();
            final Long nodeNumber = doorData.getNodeNum();

            final Board board = boardService.getBoardByPanelOidAndBoardNumber(
                    brainByBrainId.getObjectByObjectId().getObjectId(), boardNumber);

            String boardDescription = "";
            if (board.getBoardNumber() != NOT_AVAILABLE_SHORT_VALUE) {
                boardDescription = board.getBoardTypeByBoardType().getDescription() + " "
                        + board.getBoardNumber() + ": " + board.getBoardLocation();
            }

            final Short maxRexExtension = getMaxRexExtensionForDevice(door, devicePropertyRepository);
            final Schedule scheduleForDevice = getFirstDeviceScheduleForDevice(door);
            final Schedule twoFactorSchedule = getTwoFactorScheduleForDevice(door);
            final Schedule cardRequiredSchedule = getCardRequiredScheduleForDevice(door);

            final int inOut = buildInOutForDevice(door.getIsIngress(), door.getIsEgress());

            final SecurityGroup site = groupService.getSiteNotRootForDevice(deviceObjectId);

            final UiDoorDTO.UiDoorDataDTO doorDataUiDTO = convertDoorDataUiDTO().apply(doorData);
            final boolean controlFromBrowser = convertShortToBoolean(door.getEnableLiveControl());
            return new UiDoorDTO(site.getSecurityGroupId(), site.getName(), door.getAccountId(), door.getDeviceId(), deviceObjectId, door.getName(),
                    brainByBrainId.getBrainId(), brainByBrainId.getElectronicSerialNumber(),
                    boardDescription, boardNumber, nodeNumber, doorDataUiDTO, maxRexExtension, scheduleForDevice.getName(),
                    scheduleForDevice.getScheduleId(), door.getTwoFactorInterval(), twoFactorSchedule.getName(),
                    twoFactorSchedule.getScheduleId(), cardRequiredSchedule.getName(), cardRequiredSchedule.getScheduleId(),
                    door.getPanelId(), inOut, controlFromBrowser);
        };
    }

    private Function<DoorData, UiDoorDTO.UiDoorDataDTO> convertDoorDataUiDTO() {
        return doorData -> new UiDoorDTO.UiDoorDataDTO(convertShortToBoolean(doorData.getNotifyOnAjar()), doorData.getDoorAjar(),
                doorData.getMaxInvalid(), doorData.getInvalidShutout(), doorData.getPassthrough(),
                convertShortToBoolean(doorData.getNotifyOnForced()), convertShortToBoolean(doorData.getRexFiresDoor()),
                convertShortToBoolean(doorData.getUseAlarmShunt()), doorData.getAlarmDelay());
    }

    private int buildInOutForDevice(final Short isIngress, final Short isEgress) {
        if (isIngress == 0) {
            if (isEgress == 0) {
                return NEITHER;
            } else {
                return EGRESS;
            }
        } else {
            return INGRESS;
        }
    }

    @Transactional(
            readOnly = true
    )
    public Collection<Device> getDevicesForPanel(final Long brainId) {
        return deviceRepository.findByBrainId(brainId)
                               .orElse(Collections.emptyList())
                               .stream()
                               .filter(isValidDevice())
                               .collect(Collectors.toSet());
    }

    @Transactional(
            readOnly = true
    )
    public Collection<Device> getDoorsForPanel(final Long brainId) {
        return deviceRepository.findByBrainId(brainId)
                               .orElse(Collections.emptyList())
                               .stream()
                               .filter(isValidDoor())
                               .collect(Collectors.toSet());
    }

    public Predicate<Device> isDeviceDoor() {
        return device -> device.getDeviceTypeByDeviceTypeId().getDeviceTypeId() == DeviceTypes.DOOR_KEYPAD.getDeviceTypeID();
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteDevices(final Collection<Device> devices) {
        final Collection<Long> deviceIds = devices.stream()
                                                  .map(device -> device.getDeviceId())
                                                  .collect(Collectors.toSet());

        LOGGER.debug("Deleting devices with ids " + StringUtils.join(deviceIds.toArray(), "; "));

        deleteDeviceCollection(devices);

        return new MessageDTO("The devices with ids " + StringUtils.join(deviceIds.toArray(), "; ")
                + " were successfully deleted");
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteDoorsByIds(final Collection<Long> doorIds) {
        if (doorIds.size() == 0) {
            LOGGER.debug("Doors collection is empty. No doors will be deleted");
            return new MessageDTO("Doors collection is empty. No doors will be deleted");
        }

        final Iterable<Device> doors = deviceRepository.findAllById(doorIds);
        Collection<Device> doorsCollection = StreamSupport.stream(doors.spliterator(), false)
                                                          .collect(Collectors.toList());
        MessageDTO response = deleteDoors(doorsCollection);

        return response;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteDevicesByIds(final Collection<Long> deviceIds) {
        if (deviceIds.size() == 0) {
            LOGGER.debug("Devices collection is empty. No devices will be deleted");
            return new MessageDTO("Devices collection is empty. No devices will be deleted");
        }

        final Iterable<Device> devices = deviceRepository.findAllById(deviceIds);
        Collection<Device> devicesCollection = StreamSupport.stream(devices.spliterator(), false)
                                                            .collect(Collectors.toList());
        MessageDTO response = deleteDevices(devicesCollection);

        return response;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteDoors(final Collection<Device> doors) {
        if (doors.size() == 0) {
            LOGGER.debug("Doors collection is empty. No doors will be deleted");
            return new MessageDTO("Doors collection is empty. No doors will be deleted");
        }

        final Collection<Long> deviceIds = doors.stream()
                                                .map(device -> device.getDeviceId())
                                                .collect(Collectors.toSet());

        LOGGER.debug("Deleting doors with ids " + StringUtils.join(deviceIds.toArray(), "; "));
        deleteDoorCollection(doors);

        return new MessageDTO("The devices with ids " + StringUtils.join(deviceIds.toArray(), "; ")
                + " were successfully deleted");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Collection<ProgDevicePuts> getProgDevicePutsByDeviceOid(final long deviceObjectId) {
        return progDevicePutsRepository.findByDeviceOid(deviceObjectId);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED)
    public void deleteDeviceCollection(final Collection<Device> devices) {
        for (Device device : devices) {
            final long deviceId = device.getDeviceId();

            final long deviceObjectId = device.getObjectByBrivoObjectId().getObjectId();
            entityManager.flush();
            final Collection<ProgDevicePuts> progDevicePutsCollection = getProgDevicePutsByDeviceOid(deviceObjectId);

            deviceDao.deleteDeviceById(deviceId, deviceObjectId);

            entityManager.flush();

            deviceDao.freeProgIoPointsForDevice(deviceObjectId, progDevicePutsCollection, Definitions.IO_FREE);

            LOGGER.debug("Successfully deleted device with id {}", deviceId);
        }
        ;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public void deleteDoorCollection(final Collection<Device> devices) {
        devices.forEach(device -> {
            final long deviceId = device.getDeviceId();
            final DoorData doorData = getDoorDataOrThrow(device.getObjectByBrivoObjectId().getObjectId());
            final Board board = boardService.getBoardByPanelOidAndBoardNumber(device.getBrainByBrainId().getObjectId(), doorData.getBoardNum());

            final Collection<Short> pointAddresses = boardService.getPointAddressesForBoardByNode(board, doorData.getNodeNum());

            deviceDao.deleteDoorById(deviceId, device.getObjectByBrivoObjectId().getObjectId(), doorData.getBoardNum(),
                    device.getBrainByBrainId().getObjectId(), pointAddresses);

            entityManager.flush();
            LOGGER.debug("Successfully deleted door with id {}", deviceId);
        });
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO createDoor(final UiDoorDTO uiDoorDTO) {
        final Long accountId = uiDoorDTO.getAccountId();
        final Long brainId = uiDoorDTO.getBrainId();
        LOGGER.debug("Adding a new door for account {}, panel with id {}", accountId, brainId);

        final Device door = createDoor().apply(uiDoorDTO);
        deviceRepository.save(door);

        saveDeviceProperties(door, uiDoorDTO.getMaxRexExtension());
        boardService.useProgIoPointsForDoor(door);

        groupService.addDeviceToSite(door.getObjectByBrivoObjectId().getObjectId(), uiDoorDTO.getSiteId());

        LOGGER.debug("Successfully added door for account {}, panel with id {}", accountId, brainId);

        return new MessageDTO("The door " + uiDoorDTO.getName() + " was successfully added to account " + accountId);
    }

    private Function<UiDoorDTO, Device> createDoor() {
        return uiDoorDTO -> {
            final Long nextObjectId = objectService.getNextObjectId();
            final Long nextDeviceId = getNextDeviceId();

            final Device newDoor = new Device();
            newDoor.setDeviceId(nextDeviceId);

            BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.DEVICE);
            brivoObject.setObjectId(nextObjectId);
            newDoor.setObjectByBrivoObjectId(brivoObject);

            final Long nextPanelId = getNextPanelId(uiDoorDTO.getBrainId(), DeviceTypes.DOOR_KEYPAD.getDeviceTypeID());
            newDoor.setPanelId(nextPanelId);

            newDoor.setCreated(LocalDateTime.now());

            final short enableLiveControl = convertBooleanToShort(uiDoorDTO.getControlFromBrowser());
            newDoor.setEnableLiveControl(enableLiveControl);

            final Long doorUnlockScheduleId = uiDoorDTO.getDoorUnlockScheduleId();
            if (doorUnlockScheduleId != null && doorUnlockScheduleId > 0) {
                final Collection<DeviceSchedule> deviceSchedules = buildDeviceSchedules(nextDeviceId, doorUnlockScheduleId);
                newDoor.setDeviceSchedulesByDeviceId(deviceSchedules);
            }

            final Device door = convertUiDoorDTO(newDoor).apply(uiDoorDTO);

            return door;
        };
    }

    private Long getNextDeviceId() {
        final Long maxDeviceId = deviceRepository.getMaxDeviceId()
                                                 .orElseThrow(() -> new IllegalArgumentException(
                                                         "An error occured while getting max(device_id)"));

        return maxDeviceId + 1;
    }

    private Long getNextPanelId(final Long brainId, final Long deviceTypeId) {
        final Long maxPanelId = deviceRepository.getMaxPanelId(brainId, deviceTypeId)
                                                .orElseThrow(() -> new IllegalArgumentException(
                                                        "An error occured while getting max(panel_id) from device table"));

        return maxPanelId + 1;
    }

    private Function<UiDoorDTO, Device> convertUiDoorDTO(final Device device) {
        return uiDoorDTO -> {
            device.setAccountId(uiDoorDTO.getAccountId());
            device.setName(uiDoorDTO.getName());
            device.setTwoFactorInterval(uiDoorDTO.getTwoFactorInterval());

            final short enableLiveControl = convertBooleanToShort(uiDoorDTO.getControlFromBrowser());
            device.setEnableLiveControl(enableLiveControl);

            if (uiDoorDTO.getTwoFactorScheduleId() != null)
                device.setTwoFactorScheduleId(uiDoorDTO.getTwoFactorScheduleId());

            if (uiDoorDTO.getCardRequiredScheduleId() != null)
                device.setCardRequiredScheduleId(uiDoorDTO.getCardRequiredScheduleId());

            short isIngress = 0, isEgress = 0;
            final int inOut = uiDoorDTO.getInOut();
            if (inOut == INGRESS)
                isIngress = 1;
            else if (inOut == EGRESS)
                isEgress = 1;

            device.setIsIngress(isIngress);
            device.setIsEgress(isEgress);

            device.setUpdated(LocalDateTime.now());

            DeviceType deviceType = deviceTypeRepository.findById(DeviceTypes.DOOR_KEYPAD.getDeviceTypeID()).get();
            device.setDeviceTypeByDeviceTypeId(deviceType);

            device.setBrainId(uiDoorDTO.getBrainId());

            final DoorData doorData = convertUiDoorDataDTO(device.getObjectByBrivoObjectId().getObjectId()).apply(uiDoorDTO);
            device.setDoorData(doorData);

            return device;
        };
    }

    public DeviceProperty buildDeviceProperty(final Device device, final String id, final String value) {
        DeviceProperty deviceProperty = new DeviceProperty();

        deviceProperty.setValue(value);
        deviceProperty.setId(id);
        deviceProperty.setDeviceId(device.getDeviceId());
        deviceProperty.setDeviceByDeviceId(device);

        return deviceProperty;
    }

    private void saveDeviceProperties(final Device device, final Short maxRexExtensionValue) {
        if (maxRexExtensionValue != null) {
            final DeviceProperty maxRexExtensionProperty = buildDeviceProperty(device, MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY,
                    maxRexExtensionValue + "");
            final Collection<DeviceProperty> deviceProperties = Collections.singletonList(maxRexExtensionProperty);

            devicePropertyRepository.saveAll(deviceProperties);
        } else {
            devicePropertyRepository.deleteById(device.getDeviceId(), MAX_REX_EXTENSION_DEVICE_TYPE_PROPERTY);
        }
    }

    private Function<UiDoorDTO, DoorData> convertUiDoorDataDTO(final Long deviceObjectId) {
        return uiDoorDTO -> {
            final UiDoorDTO.UiDoorDataDTO uiDoorData = uiDoorDTO.getDoorData();
            DoorData doorData = getDoorData(deviceObjectId, doorDataRepository);

            doorData.setDeviceOid(deviceObjectId);
            doorData.setBoardNum(uiDoorDTO.getBoardNumber());
            doorData.setNodeNum(uiDoorDTO.getNodeNumber());
            doorData.setNotifyOnAjar(convertBooleanToShort(uiDoorData.getNotifyOnAjar()));
            doorData.setDoorAjar(uiDoorData.getDoorAjar());
            doorData.setMaxInvalid(uiDoorData.getMaxInvalid());
            doorData.setPassthrough(uiDoorData.getPassthrough());
            doorData.setNotifyOnForced(convertBooleanToShort(uiDoorData.getNotifyOnForced()));
            doorData.setRexFiresDoor(convertBooleanToShort(uiDoorData.getRexFiresDoor()));
            doorData.setUseAlarmShunt(convertBooleanToShort(uiDoorData.getUseAlarmShunt()));
            doorData.setAlarmDelay(uiDoorData.getAlarmDelay());
            doorData.setInvalidShutout(uiDoorData.getInvalidShutout());

            final Long doorUnlockScheduleId = uiDoorDTO.getDoorUnlockScheduleId();
            if (doorUnlockScheduleId != null && doorUnlockScheduleId > 0) {
                doorData.setScheduleEnabled(SCHEDULE_ENABLED);
            } else {
                doorData.setScheduleEnabled(SCHEDULE_DISABLED);
            }

            return doorData;
        };
    }

    private Collection<DeviceSchedule> buildDeviceSchedules(final Long deviceId, final Long scheduleId) {
        final DeviceSchedule deviceSchedule = new DeviceSchedule();
        deviceSchedule.setScheduleId(scheduleId);
        deviceSchedule.setDeviceId(deviceId);

        final Collection<DeviceSchedule> deviceSchedules = new ArrayList<>();
        deviceSchedules.add(deviceSchedule);

        return deviceSchedules;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO updateDoor(final UiDoorDTO uiDoorDTO) {
        final Long accountId = uiDoorDTO.getAccountId();
        final Long brainId = uiDoorDTO.getBrainId();
        LOGGER.debug("Updating door with id {} for account {}", uiDoorDTO.getDeviceId(), accountId);

        final Device door = updateDoor().apply(uiDoorDTO);
        deviceRepository.save(door);

        saveDeviceProperties(door, uiDoorDTO.getMaxRexExtension());

        LOGGER.debug("Successfully updated door with id {} for account {}", uiDoorDTO.getDeviceId(), accountId);

        return new MessageDTO("The door " + uiDoorDTO.getName() + " was successfully updated");
    }

    private Function<UiDoorDTO, Device> updateDoor() {
        return uiDoorDTO -> {
            final Long deviceId = uiDoorDTO.getDeviceId();

            final Device originalDoor = deviceRepository.findById(uiDoorDTO.getDeviceId()).get();
            final Device door = convertUiDoorDTO(originalDoor).apply(uiDoorDTO);

            final Long doorUnlockScheduleId = uiDoorDTO.getDoorUnlockScheduleId();
            final Optional<DeviceSchedule> deviceSchedule = deviceScheduleRepository.findByDeviceId(deviceId);
            Collection<DeviceSchedule> deviceScheduleCollection = Collections.emptyList();

            if (deviceSchedule.isPresent()) {
                if (doorUnlockScheduleId != null && doorUnlockScheduleId > 0) {
                    deviceScheduleCollection = buildDeviceSchedules(deviceId, doorUnlockScheduleId);
                    deviceScheduleRepository.deleteDeviceSchedulesByDeviceId(deviceId);
                } else {
                    deviceScheduleCollection = null;
                    deviceScheduleRepository.deleteDeviceSchedulesByDeviceId(deviceId);
                }
            } else {
                if (doorUnlockScheduleId != null && doorUnlockScheduleId > 0) {
                    deviceScheduleCollection = buildDeviceSchedules(deviceId, doorUnlockScheduleId);
                }
            }
            door.setDeviceSchedulesByDeviceId(deviceScheduleCollection);

            return door;
        };
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiDeviceDTO> getUiDeviceDtosForAccount(final Long accountId) {
        return deviceRepository.findByAccountId(accountId)
                               .orElse(Collections.emptyList())
                               .stream()
                               .filter(isValidDevice())
                               .map(convertDeviceUiDTO())
                               .sorted(uiDeviceDTOComparator)
                               .collect(Collectors.toList());
    }

    Comparator<UiDeviceDTO> uiDeviceDTOComparator = new Comparator<UiDeviceDTO>() {
        @Override
        public int compare(UiDeviceDTO o1, UiDeviceDTO o2) {
            return o2.getDeviceId().compareTo(o1.getDeviceId());
        }
    };

    private Function<Device, UiDeviceDTO> convertDeviceUiDTO() {
        return device -> {
            final SecurityGroup site = groupService.getSiteNotRootForDevice(device.getObjectByBrivoObjectId().getObjectId());
            final DeviceType deviceType = device.getDeviceTypeByDeviceTypeId();

            final Long deviceScheduleId = device.getDeviceSchedulesByDeviceId()
                                                .stream()
                                                .findFirst()
                                                .map(deviceSchedule -> deviceSchedule.getScheduleId())
                                                .orElse(NOT_AVAILABLE_LONG_VALUE);

            final Brain brain = device.getBrainByBrainId();
            return new UiDeviceDTO(device.getAccountId(), site.getSecurityGroupId(), site.getName(), site.getObjectByObjectId().getObjectId(),
                    brain.getBrainId(), brain.getObjectId(), brain.getName(), device.getDeviceId(), device.getName(),
                    brain.getElectronicSerialNumber(), device.getCreated(), device.getTwoFactorScheduleId(),
                    device.getTwoFactorInterval(), device.getCardRequiredScheduleId(),
                    deviceType.getDeviceTypeId(), deviceType.getName(), deviceScheduleId, device.getObjectId());
        };
    }


    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO createEventTrack(final UiProgDeviceDTO uiEventTrackDTO) {
        final String eventTrackName = uiEventTrackDTO.getName();
        final long accountId = uiEventTrackDTO.getAccountId();

        LOGGER.debug("Inserting event track {} for account {}", eventTrackName, accountId);

        final Device eventTrackDevice = convertUiProgDeviceDTO(DeviceTypes.EVENT.getDeviceTypeID()).apply(uiEventTrackDTO);
        deviceRepository.save(eventTrackDevice);

        groupService.addDeviceToSite(eventTrackDevice.getObjectByBrivoObjectId().getObjectId(), uiEventTrackDTO.getSiteId());

        LOGGER.debug("Successfully inserted event track {} for account {}", eventTrackName, accountId);
        return new MessageDTO("Successfully inserted event track " + eventTrackName + " for account " + accountId);
    }

    private Function<UiProgDeviceDTO, Device> convertUiProgDeviceDTO(final Long deviceTypeId) {
        return uiEventTrackDTO -> {
            Device device = convertUiDeviceDTO(deviceTypeId).apply(uiEventTrackDTO);
            device = setDefaultFieldsForNewDevice().apply(device);
            device = setNextDeviceIdsForAdd().apply(device);

            final Long deviceOid = device.getObjectByBrivoObjectId().getObjectId();

            device = addProgDeviceDetails(device, uiEventTrackDTO.getOutputs()).apply(uiEventTrackDTO);

            final Collection<ProgDeviceData> progDeviceData = convertProgDeviceDataForDevice(deviceOid, uiEventTrackDTO);
            device.setProgDeviceDataByObjectId(progDeviceData);

            return device;
        };
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO createSwitch(final UiSwitchDTO uiSwitchDTO) {
        final String switchName = uiSwitchDTO.getName();
        final long accountId = uiSwitchDTO.getAccountId();

        LOGGER.debug("Inserting switch {} for account {}", switchName, accountId);

        final Device switchDevice = convertUiSwitchDTO().apply(uiSwitchDTO);
        deviceRepository.save(switchDevice);

        groupService.addDeviceToSite(switchDevice.getObjectByBrivoObjectId().getObjectId(), uiSwitchDTO.getSiteId());

        LOGGER.debug("Successfully inserted switch {} for account {}", switchName, accountId);
        return new MessageDTO("Successfully inserted switch " + switchName + " for account " + accountId);
    }

    private Function<UiDeviceDTO, Device> convertUiDeviceDTO(final Long deviceTypeId) {
        return uiDeviceDTO -> {
            final Device device = new Device();

            device.setAccountId(uiDeviceDTO.getAccountId());
            device.setName(uiDeviceDTO.getName());

            final long brainId = uiDeviceDTO.getBrainId();
            device.setBrainId(brainId);

            final Long nextPanelId = getNextPanelId(brainId, deviceTypeId);
            device.setPanelId(nextPanelId);

            final LocalDateTime now = LocalDateTime.now();
            device.setCreated(now);
            device.setUpdated(now);

            final DeviceType deviceType = deviceTypeRepository.findById(deviceTypeId).get();
            device.setDeviceTypeByDeviceTypeId(deviceType);

            return device;
        };
    }

    private Function<UiSwitchDTO, Device> convertUiSwitchDTO() {
        return uiSwitchDTO -> {
            Device device = convertUiDeviceDTO(DeviceTypes.SWITCH.getDeviceTypeID()).apply(uiSwitchDTO);
            device = setDefaultFieldsForNewDevice().apply(device);
            device = setNextDeviceIdsForAdd().apply(device);

            final Long deviceOid = device.getObjectByBrivoObjectId().getObjectId();

            final Collection<UiPhysicalPointDTO> points = getAllPointsFromSwitch(uiSwitchDTO);

            device = addProgDeviceDetails(device, points).apply(uiSwitchDTO);

            final Collection<ProgDeviceData> progDeviceData = convertProgDeviceDataForDevice(deviceOid, uiSwitchDTO);
            device.setProgDeviceDataByObjectId(progDeviceData);

            return device;
        };
    }

    private Function<UiProgDeviceDTO, Device> addProgDeviceDetails(final Device device, final Collection<UiPhysicalPointDTO> points) {
        return uiProgDeviceDTO -> {
            final Long deviceOid = device.getObjectByBrivoObjectId().getObjectId();

            final Collection<ProgDevicePuts> progDevicePuts = convertProgDevicePutsForDevice(deviceOid,
                    uiProgDeviceDTO.getPanelOid(), points, uiProgDeviceDTO.getBehavior(), uiProgDeviceDTO.getDisengageDelay());
            device.setProgDevicePutsByObjectId(progDevicePuts);

            final Long deviceScheduleId = uiProgDeviceDTO.getScheduleId();
            if (deviceScheduleId != null && deviceScheduleId > 0) {
                final Collection<DeviceSchedule> deviceSchedules = buildDeviceSchedules(device.getDeviceId(), deviceScheduleId);
                device.setDeviceSchedulesByDeviceId(deviceSchedules);
            }

            progIoPointService.useProgIoPointsForDevice(uiProgDeviceDTO.getPanelOid(), points);

            return device;
        };
    }

    private Function<Device, Device> setDefaultFieldsForNewDevice() {
        return device -> {
            device.setTwoFactorInterval(DEFAULT_TWO_FACTOR_INTERVAL_VALUE);
            device.setIsEgress(PROG_DEVICE_FIELD_NOT_SET_SHORT);
            device.setIsIngress(PROG_DEVICE_FIELD_NOT_SET_SHORT);
            device.setTwoFactorScheduleId(PROG_DEVICE_FIELD_NOT_SET_LONG);
            device.setEnableLiveControl(PROG_DEVICE_FIELD_NOT_SET_SHORT);
            device.setCardRequiredScheduleId(PROG_DEVICE_FIELD_NOT_SET_LONG);

            return device;
        };
    }

    private Function<Device, Device> setNextDeviceIdsForAdd() {
        return device -> {
            final Long nextDeviceId = getNextDeviceId();
            device.setDeviceId(nextDeviceId);

            final Long nextObjectId = objectService.getNextObjectId();
            BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.DEVICE);
            brivoObject.setObjectId(nextObjectId);
            device.setObjectByBrivoObjectId(brivoObject);

            return device;
        };
    }

    private Collection<ProgDeviceData> convertProgDeviceDataForDevice(final Long deviceOid, final UiProgDeviceDTO uiProgDeviceDTO) {
        final ProgDeviceData progDeviceData = new ProgDeviceData();

        progDeviceData.setDeviceOid(deviceOid);
        progDeviceData.setNotifyFlag(convertBooleanToShort(uiProgDeviceDTO.getReportEngage()));
        progDeviceData.setNotifyDiseng(convertBooleanToShort(uiProgDeviceDTO.getReportDisengage()));
        progDeviceData.setEngageMsg(uiProgDeviceDTO.getEngageMessage());
        progDeviceData.setDisengageMsg(uiProgDeviceDTO.getDisengageMessage());
        progDeviceData.setDoorId(uiProgDeviceDTO.getDoorId());
        progDeviceData.setEventType(uiProgDeviceDTO.getEventTypeId());

        return Collections.singletonList(progDeviceData);
    }

    private Collection<ProgDevicePuts> convertProgDevicePutsForDevice(final Long deviceOid, final Long panelOid,
                                                                      final Collection<UiPhysicalPointDTO> points,
                                                                      final Long behaviour, final Long argument) {
        return points.stream()
                     .map(convertUiPhysicalPointDTOToProgDevicePuts(deviceOid, panelOid, behaviour, argument))
                     .collect(Collectors.toSet());
    }

    private Function<UiPhysicalPointDTO, ProgDevicePuts> convertUiPhysicalPointDTOToProgDevicePuts(final Long deviceOid,
                                                                                                   final Long panelOid,
                                                                                                   final Long behaviour,
                                                                                                   final Long argument) {
        return uiPhysicalPointDTO -> {
            final ProgDevicePuts progDevicePuts = new ProgDevicePuts();

            progDevicePuts.setDeviceOid(deviceOid);
            progDevicePuts.setPanelOid(panelOid);
            progDevicePuts.setPointAddress((long) uiPhysicalPointDTO.getPointAddress());
            progDevicePuts.setBoardNumber(uiPhysicalPointDTO.getBoardNumber());
            progDevicePuts.setBehavior(behaviour);
            if (uiPhysicalPointDTO.getType() == Definitions.OUTPUT_POINT)
                progDevicePuts.setArgument(argument);
            else
                progDevicePuts.setArgument(PROG_DEVICE_FIELD_NOT_SET_LONG);

            return progDevicePuts;
        };
    }

    public Device findDeviceByIdOrThrow(final Long deviceId) {
        return deviceRepository.findById(deviceId)
                               .orElseThrow(() -> new IllegalArgumentException(
                                       "There is no device with device id " + deviceId + " in the database"));
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UiSwitchDTO getUiSwitchDTOByDeviceId(final Long deviceId) {
        final Device switchDevice = findDeviceByIdOrThrow(deviceId);

        return convertDeviceToUiSwitchDTO().apply(switchDevice);
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Function<Device, UiSwitchDTO> convertDeviceToUiSwitchDTO() {
        return device -> {
            final UiProgDeviceDTO uiProgDeviceDTO = convertDeviceToUiProgDeviceDTO().apply(device);
            UiSwitchDTO uiSwitchDTO = new UiSwitchDTO(uiProgDeviceDTO);

            uiSwitchDTO = progIoPointService.setAvailablePointsCollectionsForSwitch(uiSwitchDTO);

            final long deviceObjectId = device.getObjectByBrivoObjectId().getObjectId();

            final Collection<ProgDevicePuts> progDevicePutsCollection = getProgDevicePutsByDeviceOid(deviceObjectId);
            if (progDevicePutsCollection.size() < 1) {
                LOGGER.error("There aren't prog device puts set for switch {}, so they cannot be set",
                        deviceObjectId);
                uiSwitchDTO.setDisengageDelay(0L);
            } else {
                final Collection<ProgIoPoints> progIoPoints = convertProgDevicePutsToProgIoPoints(progDevicePutsCollection);
                final ProgIoPoints inputProgIoPoint = progIoPointService.getProgIoPointOfTypeFromCollectionOrThrow(deviceObjectId,
                        progIoPoints, Definitions.INPUT_POINT);

                final UiPhysicalPointDTO input = progIoPointService.convertProgIoPointToUiPhysicalInput().apply(inputProgIoPoint);
                uiSwitchDTO.setInput(input);
            }

            return uiSwitchDTO;
        };
    }

    private Function<Device, UiProgDeviceDTO> convertDeviceToUiProgDeviceDTO() {
        return device -> {
            final UiDeviceDTO uiDeviceDTO = convertDeviceUiDTO().apply(device);

            UiProgDeviceDTO uiProgDeviceDTO = new UiProgDeviceDTO(uiDeviceDTO);

            uiProgDeviceDTO = progIoPointService.setAvailableOutputPointsForProgDevice(uiProgDeviceDTO);

            final long deviceObjectId = device.getObjectByBrivoObjectId().getObjectId();

            final ProgDeviceData progDeviceData = progDeviceDataRepository.findByDeviceOid(deviceObjectId);
            uiProgDeviceDTO.setDisengageMessage(progDeviceData.getDisengageMsg());
            uiProgDeviceDTO.setEngageMessage(progDeviceData.getEngageMsg());
            uiProgDeviceDTO.setReportDisengage(convertShortToBoolean(progDeviceData.getNotifyDiseng()));
            uiProgDeviceDTO.setReportEngage(convertShortToBoolean(progDeviceData.getNotifyFlag()));
            uiProgDeviceDTO.setDoorId(progDeviceData.getDoorId());
            uiProgDeviceDTO.setEventTypeId(progDeviceData.getEventType());

            final Collection<ProgDevicePuts> progDevicePutsCollection = getProgDevicePutsByDeviceOid(deviceObjectId);
            if (progDevicePutsCollection.size() < 1) {
                LOGGER.error("There aren't prog device puts set for device {}, so they cannot be set",
                        deviceObjectId);
                uiProgDeviceDTO.setDisengageDelay(0L);
            } else {
                final ProgDevicePuts firstProgDevicePuts = progDevicePutsCollection.stream()
                                                                                   .findFirst()
                                                                                   .get();

                uiProgDeviceDTO.setBehavior(firstProgDevicePuts.getBehavior());
                uiProgDeviceDTO.setDisengageDelay(firstProgDevicePuts.getArgument());

                final Collection<ProgIoPoints> progIoPoints = convertProgDevicePutsToProgIoPoints(progDevicePutsCollection);
                final Collection<ProgIoPoints> outputProgIoPoints = progIoPointService.filterProgIoPointsByType(progIoPoints, Definitions.OUTPUT_POINT);

                final Collection<UiPhysicalPointDTO> outputs = progIoPointService.convertProgIoPointsToUiPhysicalInputs(outputProgIoPoints);

                uiProgDeviceDTO.setOutputs(outputs);
            }

            return uiProgDeviceDTO;
        };
    }


    private Collection<ProgIoPoints> convertProgDevicePutsToProgIoPoints(final Collection<ProgDevicePuts> progDevicePutsCollection) {
        return progDevicePutsCollection.stream()
                                       .map(progIoPointService.convertProgDevicePutToProgIoPoint())
                                       .collect(Collectors.toSet());
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UiProgDeviceDTO getUProgDeviceDTOByDeviceId(final Long deviceId) {
        final Device eventTrackDevice = findDeviceByIdOrThrow(deviceId);

        return convertDeviceToUiProgDeviceDTO().apply(eventTrackDevice);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRES_NEW)
    public MessageDTO updateSwitch(final UiSwitchDTO uiSwitchDTO) {
        final Long deviceId = uiSwitchDTO.getDeviceId();
        LOGGER.debug("Updating switch with device id {}", deviceId);

        final Device currentDevice = findDeviceByIdOrThrow(deviceId);
        final Device switchDevice = convertUiSwitchDTOForUpdate(currentDevice).apply(uiSwitchDTO);

        deviceRepository.save(switchDevice);

        progDeviceDataRepository.updateProgDeviceDataByDeviceOid(switchDevice.getObjectByBrivoObjectId().getObjectId(),
                convertBooleanToShort(uiSwitchDTO.getReportEngage()),
                convertBooleanToShort(uiSwitchDTO.getReportDisengage()), uiSwitchDTO.getEngageMessage(), uiSwitchDTO.getDisengageMessage(),
                PROG_DEVICE_FIELD_NOT_SET_LONG, PROG_DEVICE_FIELD_NOT_SET_SHORT);

        LOGGER.debug("Successfully updated switch {} with id", deviceId);

        return new MessageDTO("Successfully updated switch with id " + deviceId);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED)
    public void prepareDeviceUpdate(final Long deviceId) {
        LOGGER.debug("Preparing update for device with id {}", deviceId);

        Device currentDevice = findDeviceByIdOrThrow(deviceId);

        deviceScheduleRepository.deleteDeviceSchedulesByDeviceId(deviceId);
        progDevicePutsRepository.deleteByDeviceOid(currentDevice.getObjectByBrivoObjectId().getObjectId());

        entityManager.flush();
    }

    @Transactional
    public Function<UiSwitchDTO, Device> convertUiSwitchDTOForUpdate(final Device device) {
        return uiSwitchDTO -> {
            device.setName(uiSwitchDTO.getName());
            device.setUpdated(LocalDateTime.now());

            final Collection<UiPhysicalPointDTO> points = getAllPointsFromSwitch(uiSwitchDTO);
            addProgDeviceDetails(device, points).apply(uiSwitchDTO);

            return device;
        };
    }

    private Collection<UiPhysicalPointDTO> getAllPointsFromSwitch(final UiSwitchDTO uiSwitchDTO) {
        final Collection<UiPhysicalPointDTO> points = new ArrayList<>();

        points.add(uiSwitchDTO.getInput());
        if (uiSwitchDTO.getOutputs() != null && uiSwitchDTO.getOutputs().size() > 0)
            points.addAll(uiSwitchDTO.getOutputs());

        return points;
    }

    private Collection<UiPhysicalPointDTO> getAllPointsFromValidCredential(final UIValidCredentialDTO uiValidCredentialDTO) {
        final Collection<UiPhysicalPointDTO> points = new ArrayList<>();

        points.add(uiValidCredentialDTO.getSelectedReader());
        if (uiValidCredentialDTO.getOutputs() != null && uiValidCredentialDTO.getOutputs().size() > 0)
            points.addAll(uiValidCredentialDTO.getOutputs());

        return points;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO createTimer(final UiProgDeviceDTO uiTimerDTO) {
        final String timerName = uiTimerDTO.getName();
        final long accountId = uiTimerDTO.getAccountId();

        LOGGER.debug("Inserting timer {} for account {}", timerName, accountId);

        final Device timerDevice = convertUiProgDeviceDTO(DeviceTypes.TIMER.getDeviceTypeID()).apply(uiTimerDTO);
        deviceRepository.save(timerDevice);

        groupService.addDeviceToSite(timerDevice.getObjectByBrivoObjectId().getObjectId(), uiTimerDTO.getSiteId());

        LOGGER.debug("Successfully inserted timer {} for account {}", timerName, accountId);
        return new MessageDTO("Successfully inserted timer " + timerName + " for account " + accountId);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRES_NEW)
    public MessageDTO updateProgDevice(final UiProgDeviceDTO uiProgDeviceDTO) {
        final Long deviceId = uiProgDeviceDTO.getDeviceId();

        LOGGER.debug("Updating device with device id {}", deviceId);

        final Device currentDevice = findDeviceByIdOrThrow(deviceId);
        final Device device = convertUiProgDeviceDTOForUpdate(currentDevice).apply(uiProgDeviceDTO);

        deviceRepository.save(device);

        progDeviceDataRepository.updateProgDeviceDataByDeviceOid(device.getObjectByBrivoObjectId().getObjectId(),
                convertBooleanToShort(uiProgDeviceDTO.getReportEngage()),
                convertBooleanToShort(uiProgDeviceDTO.getReportDisengage()), uiProgDeviceDTO.getEngageMessage(),
                uiProgDeviceDTO.getDisengageMessage(), uiProgDeviceDTO.getDoorId(), uiProgDeviceDTO.getEventTypeId());

        LOGGER.debug("Successfully updated device {} with id", deviceId);

        return new MessageDTO("Successfully updated device with id " + deviceId);
    }

    @Transactional
    public Function<UiProgDeviceDTO, Device> convertUiProgDeviceDTOForUpdate(final Device device) {
        return uiProgDeviceDTO -> {
            device.setName(uiProgDeviceDTO.getName());
            device.setUpdated(LocalDateTime.now());

            if (uiProgDeviceDTO.getOutputs() == null || uiProgDeviceDTO.getOutputs().size() == 0) {
                LOGGER.debug("There are no outputs for device with id {} to update..", uiProgDeviceDTO.getDeviceId());
            } else {
                addProgDeviceDetails(device, uiProgDeviceDTO.getOutputs()).apply(uiProgDeviceDTO);
            }
            return device;
        };
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO createValidCredential(final UIValidCredentialDTO uiValidCredentialDTO) {
        final String validCredentialName = uiValidCredentialDTO.getName();
        final long accountId = uiValidCredentialDTO.getAccountId();

        LOGGER.debug("Inserting valid credential device {} for account {}", validCredentialName, accountId);

        final Device validCredentialDevice = convertUiValidCredentialDTO().apply(uiValidCredentialDTO);
        deviceRepository.save(validCredentialDevice);

        groupService.addDeviceToSite(validCredentialDevice.getObjectByBrivoObjectId().getObjectId(), uiValidCredentialDTO.getSiteId());

        LOGGER.debug("Successfully inserted valid credential {} for account {}", validCredentialName, accountId);
        return new MessageDTO("Successfully inserted valid credential " + validCredentialName + " for account " + accountId);
    }

    private Function<UIValidCredentialDTO, Device> convertUiValidCredentialDTO() {
        return uiValidCredentialDTO -> {
            Device device = convertUiDeviceDTO(DeviceTypes.WIEGAND.getDeviceTypeID()).apply(uiValidCredentialDTO);
            device = setDefaultFieldsForNewDevice().apply(device);
            device = setNextDeviceIdsForAdd().apply(device);

            final Long deviceOid = device.getObjectByBrivoObjectId().getObjectId();

            final Collection<UiPhysicalPointDTO> points = getAllPointsFromValidCredential(uiValidCredentialDTO);

            device = addProgDeviceDetails(device, points).apply(uiValidCredentialDTO);

            final Collection<ProgDeviceData> progDeviceData = convertProgDeviceDataForDevice(deviceOid, uiValidCredentialDTO);
            device.setProgDeviceDataByObjectId(progDeviceData);

            device.setCardRequiredScheduleId(uiValidCredentialDTO.getCardRequiredScheduleId());
            device.setTwoFactorScheduleId(uiValidCredentialDTO.getTwoFactorScheduleId());
            device.setTwoFactorInterval(uiValidCredentialDTO.getTwoFactorInterval());

            return device;
        };
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRES_NEW)
    public MessageDTO updateValidCredential(final UIValidCredentialDTO uiValidCredentialDTO) {
        final Long deviceId = uiValidCredentialDTO.getDeviceId();
        LOGGER.debug("Updating valid credential with device id {}", deviceId);

        final Device currentDevice = findDeviceByIdOrThrow(deviceId);
        final Device validCredentialDevice = convertUiValidCredentialDTOForUpdate(currentDevice).apply(uiValidCredentialDTO);

        deviceRepository.save(validCredentialDevice);

        progDeviceDataRepository.updateProgDeviceDataByDeviceOid(validCredentialDevice.getObjectByBrivoObjectId().getObjectId(),
                convertBooleanToShort(uiValidCredentialDTO.getReportEngage()),
                convertBooleanToShort(uiValidCredentialDTO.getReportDisengage()),
                uiValidCredentialDTO.getEngageMessage(), uiValidCredentialDTO.getDisengageMessage(),
                PROG_DEVICE_FIELD_NOT_SET_LONG, PROG_DEVICE_FIELD_NOT_SET_SHORT);

        LOGGER.debug("Successfully updated valid credential {} with id", deviceId);

        return new MessageDTO("Successfully updated valid credential with id " + deviceId);
    }


    @Transactional
    public Function<UIValidCredentialDTO, Device> convertUiValidCredentialDTOForUpdate(final Device device) {
        return uiValidCredentialDTO -> {
            device.setName(uiValidCredentialDTO.getName());
            device.setUpdated(LocalDateTime.now());

            final Collection<UiPhysicalPointDTO> points = getAllPointsFromValidCredential(uiValidCredentialDTO);
            addProgDeviceDetails(device, points).apply(uiValidCredentialDTO);

            device.setCardRequiredScheduleId(uiValidCredentialDTO.getCardRequiredScheduleId());
            device.setTwoFactorScheduleId(uiValidCredentialDTO.getTwoFactorScheduleId());
            device.setTwoFactorInterval(uiValidCredentialDTO.getTwoFactorInterval());

            return device;
        };
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UIValidCredentialDTO getUiValidCredentialDTOByDeviceId(final Long deviceId) {
        LOGGER.debug("Getting valid credential with id {} to edit...", deviceId);
        final Device validCredentialDevice = findDeviceByIdOrThrow(deviceId);

        UIValidCredentialDTO uiValidCredentialDTO = convertDeviceToUiValidCredentialDTO().apply(validCredentialDevice);
        LOGGER.debug("Successfully got valid credential to edit: {}", uiValidCredentialDTO.toString());

        return uiValidCredentialDTO;
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Function<Device, UIValidCredentialDTO> convertDeviceToUiValidCredentialDTO() {
        return device -> {
            final UiProgDeviceDTO uiProgDeviceDTO = convertDeviceToUiProgDeviceDTO().apply(device);
            UIValidCredentialDTO uiValidCredentialDTO = new UIValidCredentialDTO(uiProgDeviceDTO);

            uiValidCredentialDTO = progIoPointService.setAvailablePointsCollectionsForValidCredential(uiValidCredentialDTO);

            final long deviceObjectId = device.getObjectByBrivoObjectId().getObjectId();

            final Collection<ProgDevicePuts> progDevicePutsCollection = getProgDevicePutsByDeviceOid(deviceObjectId);
            if (progDevicePutsCollection.size() < 1) {
                LOGGER.error("There aren't prog device puts set for valid credential {}, so they cannot be set",
                        deviceObjectId);
                uiValidCredentialDTO.setDisengageDelay(0L);
            } else {
                final Collection<ProgIoPoints> progIoPoints = convertProgDevicePutsToProgIoPoints(progDevicePutsCollection);
                final ProgIoPoints readerProgIoPoint = progIoPointService.getProgIoPointOfTypeFromCollectionOrThrow(deviceObjectId,
                        progIoPoints, Definitions.READER_POINT);

                final UiPhysicalPointDTO reader = progIoPointService.convertProgIoPointToUiPhysicalInput().apply(readerProgIoPoint);
                uiValidCredentialDTO.setSelectedReader(reader);
            }

            return uiValidCredentialDTO;
        };
    }

}
