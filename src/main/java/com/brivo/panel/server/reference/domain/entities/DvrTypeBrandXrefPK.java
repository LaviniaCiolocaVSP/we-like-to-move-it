package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DvrTypeBrandXrefPK implements Serializable {
    private long dvrTypeId;
    private long brandId;

    @Column(name = "dvr_type_id", nullable = false)
    @Id
    public long getDvrTypeId() {
        return dvrTypeId;
    }

    public void setDvrTypeId(long dvrTypeId) {
        this.dvrTypeId = dvrTypeId;
    }

    @Column(name = "brand_id", nullable = false)
    @Id
    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DvrTypeBrandXrefPK that = (DvrTypeBrandXrefPK) o;

        if (dvrTypeId != that.dvrTypeId) {
            return false;
        }
        return brandId == that.brandId;
    }

    @Override
    public int hashCode() {
        int result = (int) (dvrTypeId ^ (dvrTypeId >>> 32));
        result = 31 * result + (int) (brandId ^ (brandId >>> 32));
        return result;
    }
}
