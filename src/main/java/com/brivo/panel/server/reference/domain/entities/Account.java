package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "account", schema="brivo20", catalog="onair")
public class Account {
    private long accountId;
    private long brandId;
    private String name;
    private String description;
    private String accountNumber;
    private LocalDateTime created;
    private LocalDateTime registered;
    private LocalDateTime updated;
    private LocalDateTime activated;
    private LocalDateTime closed;
    private String accountStatus;
    private long opsFlag;
    private Long industryId;
    private Long dealerAccountId;
    private String dealerRefNum;
    private String dealerRefNum5;
    private String dealerRefNum2;
    private String dealerRefNum3;
    private String dealerRefNum4;
    private Short videoBetaFlag;
    private Short persistBadgePrintRotate;
    private Collection<AccessCredential> accessCredentialsByAccountId;
    private BrivoObject objectByObjectId;
    private AccountType accountTypeByAccountTypeId;
    private Collection<AccountApiPermission> accountApiPermissionsByAccountId;
    //    private AccountLogo accountLogoByAccountId;
    private Collection<AccountSettingsValue> accountSettingsValuesByAccountId;
    private Collection<AllowedAvhsServer> allowedAvhsServersByAccountId;
    private Collection<AllowedAvhsServer> allowedAvhsServersByAccountId_0;
    private Collection<Application> applicationsByAccountId;
    private Collection<ApplicationAccount> applicationAccountsByAccountId;
    private Collection<ApplicationBlackList> applicationBlackListsByAccountId;
    private Collection<BadgePrintJob> badgePrintJobsByAccountId;
    private Collection<BadgeTemplate> badgeTemplatesByAccountId;
    private Collection<Brain> brainsByAccountId;
    private Collection<CameraGroup> cameraGroupsByAccountId;
    private Collection<CustomField> customFieldsByAccountId;
    private Collection<CustomFieldDefinition> customFieldDefinitionsByAccountId;
    private Collection<DealerBrand> dealerBrandsByAccountId;
    private Collection<Device> devicesByAccountId;
    private Collection<Dvr> dvrsByAccountId;
    private Collection<EventSubscription> eventSubscriptionsByAccountId;
    private Collection<FeatureOverride> featureOverridesByAccountId;
    private Collection<FeatureOverride> featureOverridesByAccountId_0;
    private Collection<GroupCustomFieldDefinition> groupCustomFieldDefinitionsByAccountId;
    private Collection<Holiday> holidaysByAccountId;
    private Collection<MobileInvitation> mobileInvitationsByAccountId;
    private Collection<NotifRule> notifRulesByAccountId;
    private Collection<OauthApplication> oauthApplicationsByAccountId;
    private Collection<ObjectPermissionTemplate> objectPermissionTemplatesByAccountId;
    private Collection<OvrCamera> ovrCamerasByAccountId;
    private Collection<Proximity> proximitiesByAccountId;
    private Collection<Report> reportsByAccountId;
    private Collection<ReportConfiguration> reportConfigurationsByAccountId;
    private Collection<ReportStorageLimit> reportStorageLimitsByAccountId;
    private Collection<Schedule> schedulesByAccountId;
    private Collection<SecurityGroup> securityGroupsByAccountId;
    private Collection<SecurityPolicyAccount> securityPolicyAccountsByAccountId;
    private Collection<SecurityPolicyParam> securityPolicyParamsByAccountId;
    private Collection<SwapHistory> swapHistoriesByAccountId;
    private Collection<TesDirectory> tesDirectoriesByAccountId;
    private Collection<TesResident> tesResidentsByAccountId;
    private Collection<UserImage> userImagesByAccountId;
    private Collection<Users> usersByAccountId;
    private Collection<VideoCamera> videoCamerasByAccountId;
    private Collection<VideoProvider> videoProvidersByAccountId;


    @Id
    @Column(name = "account_id", insertable = true, updatable = true, unique = true, nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "brand_id", nullable = false)
    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "account_number", nullable = false, length = 10)
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "registered", nullable = true)
    public LocalDateTime getRegistered() {
        return registered;
    }

    public void setRegistered(LocalDateTime registered) {
        this.registered = registered;
    }

    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Column(name = "activated", nullable = true)
    public LocalDateTime getActivated() {
        return activated;
    }

    public void setActivated(LocalDateTime activated) {
        this.activated = activated;
    }

    @Column(name = "closed", nullable = true)
    public LocalDateTime getClosed() {
        return closed;
    }

    public void setClosed(LocalDateTime closed) {
        this.closed = closed;
    }

    @Column(name = "account_status", nullable = true, length = 32)
    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Column(name = "ops_flag", nullable = false)
    public long getOpsFlag() {
        return opsFlag;
    }

    public void setOpsFlag(long opsFlag) {
        this.opsFlag = opsFlag;
    }

    @Column(name = "industry_id", nullable = true)
    public Long getIndustryId() {
        return industryId;
    }

    public void setIndustryId(Long industryId) {
        this.industryId = industryId;
    }

    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Column(name = "dealer_ref_num", nullable = true, length = 50)
    public String getDealerRefNum() {
        return dealerRefNum;
    }

    public void setDealerRefNum(String dealerRefNum) {
        this.dealerRefNum = dealerRefNum;
    }

    @Column(name = "dealer_ref_num5", nullable = true, length = 50)
    public String getDealerRefNum5() {
        return dealerRefNum5;
    }

    public void setDealerRefNum5(String dealerRefNum5) {
        this.dealerRefNum5 = dealerRefNum5;
    }

    @Column(name = "dealer_ref_num2", nullable = true, length = 50)
    public String getDealerRefNum2() {
        return dealerRefNum2;
    }

    public void setDealerRefNum2(String dealerRefNum2) {
        this.dealerRefNum2 = dealerRefNum2;
    }

    @Column(name = "dealer_ref_num3", nullable = true, length = 50)
    public String getDealerRefNum3() {
        return dealerRefNum3;
    }

    public void setDealerRefNum3(String dealerRefNum3) {
        this.dealerRefNum3 = dealerRefNum3;
    }

    @Column(name = "dealer_ref_num4", nullable = true, length = 50)
    public String getDealerRefNum4() {
        return dealerRefNum4;
    }

    public void setDealerRefNum4(String dealerRefNum4) {
        this.dealerRefNum4 = dealerRefNum4;
    }

    @Column(name = "video_beta_flag", nullable = true)
    public Short getVideoBetaFlag() {
        return videoBetaFlag;
    }

    public void setVideoBetaFlag(Short videoBetaFlag) {
        this.videoBetaFlag = videoBetaFlag;
    }

    @Column(name = "persist_badge_print_rotate", nullable = true)
    public Short getPersistBadgePrintRotate() {
        return persistBadgePrintRotate;
    }

    public void setPersistBadgePrintRotate(Short persistBadgePrintRotate) {
        this.persistBadgePrintRotate = persistBadgePrintRotate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Account account = (Account) o;
        return accountId == account.accountId &&
                brandId == account.brandId &&
                Objects.equals(name, account.name) &&
                Objects.equals(description, account.description) &&
                Objects.equals(accountNumber, account.accountNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, brandId, name, description, accountNumber);
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<AccessCredential> getAccessCredentialsByAccountId() {
        return accessCredentialsByAccountId;
    }

    public void setAccessCredentialsByAccountId(Collection<AccessCredential> accessCredentialsByAccountId) {
        this.accessCredentialsByAccountId = accessCredentialsByAccountId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByObjectId() {
        return objectByObjectId;
    }

    public void setObjectByObjectId(BrivoObject objectByObjectId) {
        this.objectByObjectId = objectByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_type_id", referencedColumnName = "account_type_id", nullable = false)
    public AccountType getAccountTypeByAccountTypeId() {
        return accountTypeByAccountTypeId;
    }

    public void setAccountTypeByAccountTypeId(AccountType accountTypeByAccountTypeId) {
        this.accountTypeByAccountTypeId = accountTypeByAccountTypeId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<AccountApiPermission> getAccountApiPermissionsByAccountId() {
        return accountApiPermissionsByAccountId;
    }

    public void setAccountApiPermissionsByAccountId(Collection<AccountApiPermission> accountApiPermissionsByAccountId) {
        this.accountApiPermissionsByAccountId = accountApiPermissionsByAccountId;
    }

//    @OneToOne(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
//    public AccountLogo getAccountLogoByAccountId() {
//        return accountLogoByAccountId;
//    }
//
//    public void setAccountLogoByAccountId(AccountLogo accountLogoByAccountId) {
//        this.accountLogoByAccountId = accountLogoByAccountId;
//    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<AccountSettingsValue> getAccountSettingsValuesByAccountId() {
        return accountSettingsValuesByAccountId;
    }

    public void setAccountSettingsValuesByAccountId(Collection<AccountSettingsValue> accountSettingsValuesByAccountId) {
        this.accountSettingsValuesByAccountId = accountSettingsValuesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<AllowedAvhsServer> getAllowedAvhsServersByAccountId() {
        return allowedAvhsServersByAccountId;
    }

    public void setAllowedAvhsServersByAccountId(Collection<AllowedAvhsServer> allowedAvhsServersByAccountId) {
        this.allowedAvhsServersByAccountId = allowedAvhsServersByAccountId;
    }

    @OneToMany(mappedBy = "accountByDealerAccountId", fetch = FetchType.LAZY)
    public Collection<AllowedAvhsServer> getAllowedAvhsServersByAccountId_0() {
        return allowedAvhsServersByAccountId_0;
    }

    public void setAllowedAvhsServersByAccountId_0(Collection<AllowedAvhsServer> allowedAvhsServersByAccountId_0) {
        this.allowedAvhsServersByAccountId_0 = allowedAvhsServersByAccountId_0;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<Application> getApplicationsByAccountId() {
        return applicationsByAccountId;
    }

    public void setApplicationsByAccountId(Collection<Application> applicationsByAccountId) {
        this.applicationsByAccountId = applicationsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<ApplicationAccount> getApplicationAccountsByAccountId() {
        return applicationAccountsByAccountId;
    }

    public void setApplicationAccountsByAccountId(Collection<ApplicationAccount> applicationAccountsByAccountId) {
        this.applicationAccountsByAccountId = applicationAccountsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<ApplicationBlackList> getApplicationBlackListsByAccountId() {
        return applicationBlackListsByAccountId;
    }

    public void setApplicationBlackListsByAccountId(Collection<ApplicationBlackList> applicationBlackListsByAccountId) {
        this.applicationBlackListsByAccountId = applicationBlackListsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<BadgePrintJob> getBadgePrintJobsByAccountId() {
        return badgePrintJobsByAccountId;
    }

    public void setBadgePrintJobsByAccountId(Collection<BadgePrintJob> badgePrintJobsByAccountId) {
        this.badgePrintJobsByAccountId = badgePrintJobsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<BadgeTemplate> getBadgeTemplatesByAccountId() {
        return badgeTemplatesByAccountId;
    }

    public void setBadgeTemplatesByAccountId(Collection<BadgeTemplate> badgeTemplatesByAccountId) {
        this.badgeTemplatesByAccountId = badgeTemplatesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Brain> getBrainsByAccountId() {
        return brainsByAccountId;
    }

    public void setBrainsByAccountId(Collection<Brain> brainsByAccountId) {
        this.brainsByAccountId = brainsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<CameraGroup> getCameraGroupsByAccountId() {
        return cameraGroupsByAccountId;
    }

    public void setCameraGroupsByAccountId(Collection<CameraGroup> cameraGroupsByAccountId) {
        this.cameraGroupsByAccountId = cameraGroupsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<CustomField> getCustomFieldsByAccountId() {
        return customFieldsByAccountId;
    }

    public void setCustomFieldsByAccountId(Collection<CustomField> customFieldsByAccountId) {
        this.customFieldsByAccountId = customFieldsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<CustomFieldDefinition> getCustomFieldDefinitionsByAccountId() {
        return customFieldDefinitionsByAccountId;
    }

    public void setCustomFieldDefinitionsByAccountId(Collection<CustomFieldDefinition> customFieldDefinitionsByAccountId) {
        this.customFieldDefinitionsByAccountId = customFieldDefinitionsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<DealerBrand> getDealerBrandsByAccountId() {
        return dealerBrandsByAccountId;
    }

    public void setDealerBrandsByAccountId(Collection<DealerBrand> dealerBrandsByAccountId) {
        this.dealerBrandsByAccountId = dealerBrandsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Device> getDevicesByAccountId() {
        return devicesByAccountId;
    }

    public void setDevicesByAccountId(Collection<Device> devicesByAccountId) {
        this.devicesByAccountId = devicesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<Dvr> getDvrsByAccountId() {
        return dvrsByAccountId;
    }

    public void setDvrsByAccountId(Collection<Dvr> dvrsByAccountId) {
        this.dvrsByAccountId = dvrsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<EventSubscription> getEventSubscriptionsByAccountId() {
        return eventSubscriptionsByAccountId;
    }

    public void setEventSubscriptionsByAccountId(Collection<EventSubscription> eventSubscriptionsByAccountId) {
        this.eventSubscriptionsByAccountId = eventSubscriptionsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<FeatureOverride> getFeatureOverridesByAccountId() {
        return featureOverridesByAccountId;
    }

    public void setFeatureOverridesByAccountId(Collection<FeatureOverride> featureOverridesByAccountId) {
        this.featureOverridesByAccountId = featureOverridesByAccountId;
    }

    @OneToMany(mappedBy = "accountByDealerAccountId", fetch = FetchType.LAZY)
    public Collection<FeatureOverride> getFeatureOverridesByAccountId_0() {
        return featureOverridesByAccountId_0;
    }

    public void setFeatureOverridesByAccountId_0(Collection<FeatureOverride> featureOverridesByAccountId_0) {
        this.featureOverridesByAccountId_0 = featureOverridesByAccountId_0;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<GroupCustomFieldDefinition> getGroupCustomFieldDefinitionsByAccountId() {
        return groupCustomFieldDefinitionsByAccountId;
    }

    public void setGroupCustomFieldDefinitionsByAccountId(Collection<GroupCustomFieldDefinition> groupCustomFieldDefinitionsByAccountId) {
        this.groupCustomFieldDefinitionsByAccountId = groupCustomFieldDefinitionsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Holiday> getHolidaysByAccountId() {
        return holidaysByAccountId;
    }

    public void setHolidaysByAccountId(Collection<Holiday> holidaysByAccountId) {
        this.holidaysByAccountId = holidaysByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<MobileInvitation> getMobileInvitationsByAccountId() {
        return mobileInvitationsByAccountId;
    }

    public void setMobileInvitationsByAccountId(Collection<MobileInvitation> mobileInvitationsByAccountId) {
        this.mobileInvitationsByAccountId = mobileInvitationsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<NotifRule> getNotifRulesByAccountId() {
        return notifRulesByAccountId;
    }

    public void setNotifRulesByAccountId(Collection<NotifRule> notifRulesByAccountId) {
        this.notifRulesByAccountId = notifRulesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<OauthApplication> getOauthApplicationsByAccountId() {
        return oauthApplicationsByAccountId;
    }

    public void setOauthApplicationsByAccountId(Collection<OauthApplication> oauthApplicationsByAccountId) {
        this.oauthApplicationsByAccountId = oauthApplicationsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<ObjectPermissionTemplate> getObjectPermissionTemplatesByAccountId() {
        return objectPermissionTemplatesByAccountId;
    }

    public void setObjectPermissionTemplatesByAccountId(Collection<ObjectPermissionTemplate> objectPermissionTemplatesByAccountId) {
        this.objectPermissionTemplatesByAccountId = objectPermissionTemplatesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<OvrCamera> getOvrCamerasByAccountId() {
        return ovrCamerasByAccountId;
    }

    public void setOvrCamerasByAccountId(Collection<OvrCamera> ovrCamerasByAccountId) {
        this.ovrCamerasByAccountId = ovrCamerasByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<Proximity> getProximitiesByAccountId() {
        return proximitiesByAccountId;
    }

    public void setProximitiesByAccountId(Collection<Proximity> proximitiesByAccountId) {
        this.proximitiesByAccountId = proximitiesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<Report> getReportsByAccountId() {
        return reportsByAccountId;
    }

    public void setReportsByAccountId(Collection<Report> reportsByAccountId) {
        this.reportsByAccountId = reportsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<ReportConfiguration> getReportConfigurationsByAccountId() {
        return reportConfigurationsByAccountId;
    }

    public void setReportConfigurationsByAccountId(Collection<ReportConfiguration> reportConfigurationsByAccountId) {
        this.reportConfigurationsByAccountId = reportConfigurationsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<ReportStorageLimit> getReportStorageLimitsByAccountId() {
        return reportStorageLimitsByAccountId;
    }

    public void setReportStorageLimitsByAccountId(Collection<ReportStorageLimit> reportStorageLimitsByAccountId) {
        this.reportStorageLimitsByAccountId = reportStorageLimitsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Schedule> getSchedulesByAccountId() {
        return schedulesByAccountId;
    }

    public void setSchedulesByAccountId(Collection<Schedule> schedulesByAccountId) {
        this.schedulesByAccountId = schedulesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<SecurityGroup> getSecurityGroupsByAccountId() {
        return securityGroupsByAccountId;
    }

    public void setSecurityGroupsByAccountId(Collection<SecurityGroup> securityGroupsByAccountId) {
        this.securityGroupsByAccountId = securityGroupsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<SecurityPolicyAccount> getSecurityPolicyAccountsByAccountId() {
        return securityPolicyAccountsByAccountId;
    }

    public void setSecurityPolicyAccountsByAccountId(Collection<SecurityPolicyAccount> securityPolicyAccountsByAccountId) {
        this.securityPolicyAccountsByAccountId = securityPolicyAccountsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<SecurityPolicyParam> getSecurityPolicyParamsByAccountId() {
        return securityPolicyParamsByAccountId;
    }

    public void setSecurityPolicyParamsByAccountId(Collection<SecurityPolicyParam> securityPolicyParamsByAccountId) {
        this.securityPolicyParamsByAccountId = securityPolicyParamsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<SwapHistory> getSwapHistoriesByAccountId() {
        return swapHistoriesByAccountId;
    }

    public void setSwapHistoriesByAccountId(Collection<SwapHistory> swapHistoriesByAccountId) {
        this.swapHistoriesByAccountId = swapHistoriesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<TesDirectory> getTesDirectoriesByAccountId() {
        return tesDirectoriesByAccountId;
    }

    public void setTesDirectoriesByAccountId(Collection<TesDirectory> tesDirectoriesByAccountId) {
        this.tesDirectoriesByAccountId = tesDirectoriesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<TesResident> getTesResidentsByAccountId() {
        return tesResidentsByAccountId;
    }

    public void setTesResidentsByAccountId(Collection<TesResident> tesResidentsByAccountId) {
        this.tesResidentsByAccountId = tesResidentsByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<UserImage> getUserImagesByAccountId() {
        return userImagesByAccountId;
    }

    public void setUserImagesByAccountId(Collection<UserImage> userImagesByAccountId) {
        this.userImagesByAccountId = userImagesByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Users> getUsersByAccountId() {
        return usersByAccountId;
    }

    public void setUsersByAccountId(Collection<Users> usersByAccountId) {
        this.usersByAccountId = usersByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<VideoCamera> getVideoCamerasByAccountId() {
        return videoCamerasByAccountId;
    }

    public void setVideoCamerasByAccountId(Collection<VideoCamera> videoCamerasByAccountId) {
        this.videoCamerasByAccountId = videoCamerasByAccountId;
    }

    @OneToMany(mappedBy = "accountByAccountId", fetch = FetchType.LAZY)
    public Collection<VideoProvider> getVideoProvidersByAccountId() {
        return videoProvidersByAccountId;
    }

    public void setVideoProvidersByAccountId(Collection<VideoProvider> videoProvidersByAccountId) {
        this.videoProvidersByAccountId = videoProvidersByAccountId;
    }
}
