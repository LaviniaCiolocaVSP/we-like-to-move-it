package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "device_group_v", schema = "brivo20", catalog = "onair")
public class DeviceGroupV {
    private Long securityGroupId;
    private Long objectId;
    private Long securityGroupTypeId;
    private Long parentId;
    private Long accountId;
    private String name;
    private String description;
    private Short disabled;
    private Timestamp created;
    private Timestamp updated;
    private Long doorCount;
    private Long auxDeviceCount;
    private Long memberCount;
    private Short deleted;

    @Id
    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "object_id", nullable = true)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "security_group_type_id", nullable = true)
    public Long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public void setSecurityGroupTypeId(Long securityGroupTypeId) {
        this.securityGroupTypeId = securityGroupTypeId;
    }

    @Basic
    @Column(name = "parent_id", nullable = true)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 35)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "disabled", nullable = true)
    public Short getDisabled() {
        return disabled;
    }

    public void setDisabled(Short disabled) {
        this.disabled = disabled;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "door_count", nullable = true)
    public Long getDoorCount() {
        return doorCount;
    }

    public void setDoorCount(Long doorCount) {
        this.doorCount = doorCount;
    }

    @Basic
    @Column(name = "aux_device_count", nullable = true)
    public Long getAuxDeviceCount() {
        return auxDeviceCount;
    }

    public void setAuxDeviceCount(Long auxDeviceCount) {
        this.auxDeviceCount = auxDeviceCount;
    }

    @Basic
    @Column(name = "member_count", nullable = true)
    public Long getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(Long memberCount) {
        this.memberCount = memberCount;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceGroupV that = (DeviceGroupV) o;

        if (securityGroupId != null ? !securityGroupId.equals(that.securityGroupId) : that.securityGroupId != null) {
            return false;
        }
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) {
            return false;
        }
        if (securityGroupTypeId != null ? !securityGroupTypeId.equals(that.securityGroupTypeId) : that.securityGroupTypeId != null) {
            return false;
        }
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (disabled != null ? !disabled.equals(that.disabled) : that.disabled != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (doorCount != null ? !doorCount.equals(that.doorCount) : that.doorCount != null) {
            return false;
        }
        if (auxDeviceCount != null ? !auxDeviceCount.equals(that.auxDeviceCount) : that.auxDeviceCount != null) {
            return false;
        }
        if (memberCount != null ? !memberCount.equals(that.memberCount) : that.memberCount != null) {
            return false;
        }
        return deleted != null ? deleted.equals(that.deleted) : that.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = securityGroupId != null ? securityGroupId.hashCode() : 0;
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (securityGroupTypeId != null ? securityGroupTypeId.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (disabled != null ? disabled.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (doorCount != null ? doorCount.hashCode() : 0);
        result = 31 * result + (auxDeviceCount != null ? auxDeviceCount.hashCode() : 0);
        result = 31 * result + (memberCount != null ? memberCount.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }
}
