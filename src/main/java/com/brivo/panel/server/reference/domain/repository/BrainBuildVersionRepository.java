package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.BrainBuildVersion;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrainBuildVersionRepository extends CrudRepository<BrainBuildVersion, Long> {
    @Query(
            value = "SELECT b.* " +
                    "FROM brivo20.brain_build_version b " +
                    "WHERE b.brain_id = :brainId",
            nativeQuery = true
    )
    Optional<BrainBuildVersion> findByBrainId(@Param("brainId") Long brainId);
}
