package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "report_schedule_recipient", schema = "brivo20", catalog = "onair")
public class ReportScheduleRecipient {
    private long id;
    private long reportScheduleId;
    private long userObjectId;
    private String reportUriExtension;
    private short suspended;
    private ReportSchedule reportScheduleByReportScheduleId;
    private BrivoObject objectByUserBrivoObjectId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_schedule_id", nullable = false)
    public long getReportScheduleId() {
        return reportScheduleId;
    }

    public void setReportScheduleId(long reportScheduleId) {
        this.reportScheduleId = reportScheduleId;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "report_uri_extension", nullable = true, length = 10)
    public String getReportUriExtension() {
        return reportUriExtension;
    }

    public void setReportUriExtension(String reportUriExtension) {
        this.reportUriExtension = reportUriExtension;
    }

    @Basic
    @Column(name = "suspended", nullable = false)
    public short getSuspended() {
        return suspended;
    }

    public void setSuspended(short suspended) {
        this.suspended = suspended;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportScheduleRecipient that = (ReportScheduleRecipient) o;

        if (id != that.id) {
            return false;
        }
        if (reportScheduleId != that.reportScheduleId) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        if (suspended != that.suspended) {
            return false;
        }
        return reportUriExtension != null ? reportUriExtension.equals(that.reportUriExtension) : that.reportUriExtension == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportScheduleId ^ (reportScheduleId >>> 32));
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (reportUriExtension != null ? reportUriExtension.hashCode() : 0);
        result = 31 * result + (int) suspended;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_schedule_id", referencedColumnName = "id", nullable = false, insertable = false,
            updatable = false)
    public ReportSchedule getReportScheduleByReportScheduleId() {
        return reportScheduleByReportScheduleId;
    }

    public void setReportScheduleByReportScheduleId(ReportSchedule reportScheduleByReportScheduleId) {
        this.reportScheduleByReportScheduleId = reportScheduleByReportScheduleId;
    }

    @ManyToOne
    @JoinColumn(name = "user_object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public BrivoObject getObjectByUserBrivoObjectId() {
        return objectByUserBrivoObjectId;
    }

    public void setObjectByUserBrivoObjectId(BrivoObject objectByUserBrivoObjectId) {
        this.objectByUserBrivoObjectId = objectByUserBrivoObjectId;
    }
}
