package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "credential_field", schema = "brivo20", catalog = "onair")
public class CredentialField {
    private long credentialFieldId;
    private long accessCredentialTypeId;
    private long credentialFieldTypeId;
    private long ordering;
    private String name;
    private String xmlName;
    private short forReference;
    private short display;
    private String format;
    private Long bcdLength;
    private AccessCredentialType accessCredentialTypeByAccessCredentialTypeId;
    private CredentialFieldType credentialFieldTypeByCredentialFieldTypeId;
    private Collection<CredentialFieldValue> credentialFieldValuesByCredentialFieldId;

    @Id
    @Column(name = "credential_field_id", nullable = false)
    public long getCredentialFieldId() {
        return credentialFieldId;
    }

    public void setCredentialFieldId(long credentialFieldId) {
        this.credentialFieldId = credentialFieldId;
    }

    @Basic
    @Column(name = "access_credential_type_id", nullable = false)
    public long getAccessCredentialTypeId() {
        return accessCredentialTypeId;
    }

    public void setAccessCredentialTypeId(long accessCredentialTypeId) {
        this.accessCredentialTypeId = accessCredentialTypeId;
    }

    @Basic
    @Column(name = "credential_field_type_id", nullable = false)
    public long getCredentialFieldTypeId() {
        return credentialFieldTypeId;
    }

    public void setCredentialFieldTypeId(long credentialFieldTypeId) {
        this.credentialFieldTypeId = credentialFieldTypeId;
    }

    @Basic
    @Column(name = "ordering", nullable = false)
    public long getOrdering() {
        return ordering;
    }

    public void setOrdering(long ordering) {
        this.ordering = ordering;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "xml_name", nullable = true, length = 64)
    public String getXmlName() {
        return xmlName;
    }

    public void setXmlName(String xmlName) {
        this.xmlName = xmlName;
    }

    @Basic
    @Column(name = "for_reference", nullable = false)
    public short getForReference() {
        return forReference;
    }

    public void setForReference(short forReference) {
        this.forReference = forReference;
    }

    @Basic
    @Column(name = "display", nullable = false)
    public short getDisplay() {
        return display;
    }

    public void setDisplay(short display) {
        this.display = display;
    }

    @Basic
    @Column(name = "format", nullable = true, length = 256)
    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    @Basic
    @Column(name = "bcd_length", nullable = true)
    public Long getBcdLength() {
        return bcdLength;
    }

    public void setBcdLength(Long bcdLength) {
        this.bcdLength = bcdLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CredentialField that = (CredentialField) o;
        return credentialFieldId == that.credentialFieldId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(credentialFieldId);
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "access_credential_type_id", referencedColumnName = "access_credential_type_id", nullable = false,
            insertable = false, updatable = false)
    public AccessCredentialType getAccessCredentialTypeByAccessCredentialTypeId() {
        return accessCredentialTypeByAccessCredentialTypeId;
    }

    public void setAccessCredentialTypeByAccessCredentialTypeId(AccessCredentialType accessCredentialTypeByAccessCredentialTypeId) {
        this.accessCredentialTypeByAccessCredentialTypeId = accessCredentialTypeByAccessCredentialTypeId;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "credential_field_type_id", referencedColumnName = "credential_field_type_id", nullable = false,
            insertable = false, updatable = false)
    public CredentialFieldType getCredentialFieldTypeByCredentialFieldTypeId() {
        return credentialFieldTypeByCredentialFieldTypeId;
    }

    public void setCredentialFieldTypeByCredentialFieldTypeId(CredentialFieldType credentialFieldTypeByCredentialFieldTypeId) {
        this.credentialFieldTypeByCredentialFieldTypeId = credentialFieldTypeByCredentialFieldTypeId;
    }

    @OneToMany(mappedBy = "credentialFieldByCredentialFieldId")
    public Collection<CredentialFieldValue> getCredentialFieldValuesByCredentialFieldId() {
        return credentialFieldValuesByCredentialFieldId;
    }

    public void setCredentialFieldValuesByCredentialFieldId(Collection<CredentialFieldValue> credentialFieldValuesByCredentialFieldId) {
        this.credentialFieldValuesByCredentialFieldId = credentialFieldValuesByCredentialFieldId;
    }
}
