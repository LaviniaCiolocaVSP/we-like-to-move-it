package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BoardPointEventDataProcessor implements EventDataProcessor {
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private EventResolutionDao eventResolutionDao;

   /* @Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveBoardPointEvent");

        Boolean result = false;

        List<EventResolutionData> dataList = this.eventResolutionDao.getBoardPointEvent(event);

        if(dataList.size() == 1)
        {
            event.setAccountId(dataList.get(0).getAccountId());
            event.getEventData().setObjectTypeId(dataList.get(0).getObjectTypeId());
            event.getEventData().setObjectName(dataList.get(0).getDeviceName());
            event.setObjectGroupObjectId(dataList.get(0).getSiteOid());
            event.getEventData().setObjectGroupName(dataList.get(0).getSecurityGroupName());
            event.getEventData().setObjectSubtypeId(dataList.get(0).getDeviceTypeId());
            event.getEventData().setActionAllowed(false);

            event.setActorObjectId(dataList.get(0).getDeviceOid());
            event.getEventData().setActorName(dataList.get(0).getDeviceName());

            result = true;
        }

        logger.trace("<<< ResolveBoardPointEvent");

        return result;
    }*/

}
