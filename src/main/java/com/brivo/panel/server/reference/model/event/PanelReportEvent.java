package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class PanelReportEvent extends Event {
    private String value;

    @JsonCreator
    public PanelReportEvent(@JsonProperty("eventType") final EventType eventType,
                            @JsonProperty("eventTime") final Instant eventTime,
                            @JsonProperty("value") final String value) {
        super(eventType, eventTime);
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
