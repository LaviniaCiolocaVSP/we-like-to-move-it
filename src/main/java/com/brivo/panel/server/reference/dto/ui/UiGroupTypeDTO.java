package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiGroupTypeDTO implements Serializable {

    private long id;
    private String name;

    public UiGroupTypeDTO() {

    }

    public UiGroupTypeDTO(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
