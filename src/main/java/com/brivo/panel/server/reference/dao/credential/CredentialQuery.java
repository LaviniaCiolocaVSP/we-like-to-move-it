package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum CredentialQuery implements FileQuery {
    SELECT_CARD_MASKS,
    SELECT_CARD_MASKS_WITH_NUM_OF_BITS,
    SELECT_CREDENTIALS,
    SELECT_CREDENTIALS_COUNT,
    SELECT_CREDENTIALS_PAGE,
    SELECT_CREDENTIAL_TYPE_BY_ID,
    SELECT_USED_CREDENTIALS,
    DELETE_CREDENTIAL,
    RETRIEVE_CREDENTIAL_FORMAT,
    RETRIEVE_CREDENTIAL_FORMAT_FIELDS;

    private String query;

    CredentialQuery() {
        this.query = QueryUtils.getQueryText(CredentialQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
