package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ObjectPropertyPK implements Serializable {
    private long objectId;
    private String id;

    @Column(name = "object_id", nullable = false)
    @Id
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Column(name = "id", nullable = false, length = 30)
    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectPropertyPK that = (ObjectPropertyPK) o;

        if (objectId != that.objectId) {
            return false;
        }
        return id != null ? id.equals(that.id) : that.id == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
