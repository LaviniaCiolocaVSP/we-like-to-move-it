package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "firmware_upgrade_config", schema = "brivo20", catalog = "onair")
public class FirmwareUpgradeConfig {
    private long brainTypeId;
    private String targetVersion;
    private String minimumUpgradeVersion;
    private long maxConcurrentUpgrades;
    private long firmwareUpgradeDelay;
    private BrainType brainTypeByBrainTypeId;

    @Id
    @Column(name = "brain_type_id", nullable = false)
    public long getBrainTypeId() {
        return brainTypeId;
    }

    public void setBrainTypeId(long brainTypeId) {
        this.brainTypeId = brainTypeId;
    }

    @Basic
    @Column(name = "target_version", nullable = false, length = 32)
    public String getTargetVersion() {
        return targetVersion;
    }

    public void setTargetVersion(String targetVersion) {
        this.targetVersion = targetVersion;
    }

    @Basic
    @Column(name = "minimum_upgrade_version", nullable = false, length = 32)
    public String getMinimumUpgradeVersion() {
        return minimumUpgradeVersion;
    }

    public void setMinimumUpgradeVersion(String minimumUpgradeVersion) {
        this.minimumUpgradeVersion = minimumUpgradeVersion;
    }

    @Basic
    @Column(name = "max_concurrent_upgrades", nullable = false)
    public long getMaxConcurrentUpgrades() {
        return maxConcurrentUpgrades;
    }

    public void setMaxConcurrentUpgrades(long maxConcurrentUpgrades) {
        this.maxConcurrentUpgrades = maxConcurrentUpgrades;
    }

    @Basic
    @Column(name = "firmware_upgrade_delay", nullable = false)
    public long getFirmwareUpgradeDelay() {
        return firmwareUpgradeDelay;
    }

    public void setFirmwareUpgradeDelay(long firmwareUpgradeDelay) {
        this.firmwareUpgradeDelay = firmwareUpgradeDelay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FirmwareUpgradeConfig that = (FirmwareUpgradeConfig) o;

        if (brainTypeId != that.brainTypeId) {
            return false;
        }
        if (maxConcurrentUpgrades != that.maxConcurrentUpgrades) {
            return false;
        }
        if (firmwareUpgradeDelay != that.firmwareUpgradeDelay) {
            return false;
        }
        if (targetVersion != null ? !targetVersion.equals(that.targetVersion) : that.targetVersion != null) {
            return false;
        }
        return minimumUpgradeVersion != null ? minimumUpgradeVersion.equals(that.minimumUpgradeVersion) : that.minimumUpgradeVersion == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainTypeId ^ (brainTypeId >>> 32));
        result = 31 * result + (targetVersion != null ? targetVersion.hashCode() : 0);
        result = 31 * result + (minimumUpgradeVersion != null ? minimumUpgradeVersion.hashCode() : 0);
        result = 31 * result + (int) (maxConcurrentUpgrades ^ (maxConcurrentUpgrades >>> 32));
        result = 31 * result + (int) (firmwareUpgradeDelay ^ (firmwareUpgradeDelay >>> 32));
        return result;
    }

    @OneToOne
    @JoinColumn(name = "brain_type_id", referencedColumnName = "brain_type_id", nullable = false)
    public BrainType getBrainTypeByBrainTypeId() {
        return brainTypeByBrainTypeId;
    }

    public void setBrainTypeByBrainTypeId(BrainType brainTypeByBrainTypeId) {
        this.brainTypeByBrainTypeId = brainTypeByBrainTypeId;
    }
}
