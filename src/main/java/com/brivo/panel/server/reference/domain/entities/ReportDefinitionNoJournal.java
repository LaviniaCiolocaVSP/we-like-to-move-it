package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "report_definition_no_journal", schema = "brivo20", catalog = "onair")
public class ReportDefinitionNoJournal {
    private long id;
    private long reportTypeId;
    private String nameKey;
    private String descriptionKey;
    private String resourceUri;
    private Timestamp available;
    private Timestamp expires;
    private short forNewAccount;
    private Timestamp created;
    private Timestamp updated;
    private short requiresPermission;
    private short additionalEmailEnabled;

    @Id
    @Basic
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_type_id", nullable = false)
    public long getReportTypeId() {
        return reportTypeId;
    }

    public void setReportTypeId(long reportTypeId) {
        this.reportTypeId = reportTypeId;
    }

    @Basic
    @Column(name = "name_key", nullable = false, length = 100)
    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    @Basic
    @Column(name = "description_key", nullable = true, length = 100)
    public String getDescriptionKey() {
        return descriptionKey;
    }

    public void setDescriptionKey(String descriptionKey) {
        this.descriptionKey = descriptionKey;
    }

    @Basic
    @Column(name = "resource_uri", nullable = false, length = 255)
    public String getResourceUri() {
        return resourceUri;
    }

    public void setResourceUri(String resourceUri) {
        this.resourceUri = resourceUri;
    }

    @Basic
    @Column(name = "available", nullable = false)
    public Timestamp getAvailable() {
        return available;
    }

    public void setAvailable(Timestamp available) {
        this.available = available;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    @Basic
    @Column(name = "for_new_account", nullable = false)
    public short getForNewAccount() {
        return forNewAccount;
    }

    public void setForNewAccount(short forNewAccount) {
        this.forNewAccount = forNewAccount;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "requires_permission", nullable = false)
    public short getRequiresPermission() {
        return requiresPermission;
    }

    public void setRequiresPermission(short requiresPermission) {
        this.requiresPermission = requiresPermission;
    }

    @Basic
    @Column(name = "additional_email_enabled", nullable = false)
    public short getAdditionalEmailEnabled() {
        return additionalEmailEnabled;
    }

    public void setAdditionalEmailEnabled(short additionalEmailEnabled) {
        this.additionalEmailEnabled = additionalEmailEnabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportDefinitionNoJournal that = (ReportDefinitionNoJournal) o;

        if (id != that.id) {
            return false;
        }
        if (reportTypeId != that.reportTypeId) {
            return false;
        }
        if (forNewAccount != that.forNewAccount) {
            return false;
        }
        if (requiresPermission != that.requiresPermission) {
            return false;
        }
        if (additionalEmailEnabled != that.additionalEmailEnabled) {
            return false;
        }
        if (nameKey != null ? !nameKey.equals(that.nameKey) : that.nameKey != null) {
            return false;
        }
        if (descriptionKey != null ? !descriptionKey.equals(that.descriptionKey) : that.descriptionKey != null) {
            return false;
        }
        if (resourceUri != null ? !resourceUri.equals(that.resourceUri) : that.resourceUri != null) {
            return false;
        }
        if (available != null ? !available.equals(that.available) : that.available != null) {
            return false;
        }
        if (expires != null ? !expires.equals(that.expires) : that.expires != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportTypeId ^ (reportTypeId >>> 32));
        result = 31 * result + (nameKey != null ? nameKey.hashCode() : 0);
        result = 31 * result + (descriptionKey != null ? descriptionKey.hashCode() : 0);
        result = 31 * result + (resourceUri != null ? resourceUri.hashCode() : 0);
        result = 31 * result + (available != null ? available.hashCode() : 0);
        result = 31 * result + (expires != null ? expires.hashCode() : 0);
        result = 31 * result + (int) forNewAccount;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) requiresPermission;
        result = 31 * result + (int) additionalEmailEnabled;
        return result;
    }
}
