package com.brivo.panel.server.reference.rest.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Map;

@RestController
public class CurrentTimeController {
    private static final Logger LOGGER = LoggerFactory.getLogger(CurrentTimeController.class);

    /**
     * Produces the current time as a string formatted for GMT time zone.
     * This is used by the panel to correct it's clock.
     */
    @RequestMapping(
            path = "/paneldata/currenttime",
            method = RequestMethod.GET
    )
    public Map<String, Object> getCurrentTime() {
        String UTCFormatTime = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssZ")
                                                .withZone(ZoneOffset.UTC)
                                                .format(Instant.now());

        // Map<String, Object> time = Collections.singletonMap("time", Instant.now());
        Map<String, Object> time = Collections.singletonMap("time", UTCFormatTime);

        return time;
    }
}
