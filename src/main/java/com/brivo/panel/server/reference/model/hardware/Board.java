package com.brivo.panel.server.reference.model.hardware;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class Board {

    @JsonProperty("address")
    private Integer boardNumber;
    private Integer boardType;
    private List<IOPoint> ioPoints;
    private Long boardId;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(Integer boardNumber) {
        this.boardNumber = boardNumber;
    }

    public Integer getBoardType() {
        return boardType;
    }

    public void setBoardType(Integer boardType) {
        this.boardType = boardType;
    }

    public List<IOPoint> getIoPoints() {
        return ioPoints;
    }

    public void setIoPoints(List<IOPoint> ioPoints) {
        this.ioPoints = ioPoints;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

}
