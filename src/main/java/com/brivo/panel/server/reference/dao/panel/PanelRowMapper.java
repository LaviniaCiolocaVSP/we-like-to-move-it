package com.brivo.panel.server.reference.dao.panel;

import com.brivo.panel.server.reference.model.hardware.Firmware;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.hardware.PanelType;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Date;

@Component
public class PanelRowMapper implements RowMapper<Panel> {

    @Override
    public Panel mapRow(ResultSet rs, int rowNum) throws SQLException {
        Long brainId = rs.getLong("brain_id");
        Long objectId = rs.getLong("object_id");
        Long accountId = rs.getLong("account_id");
        Integer networkId = rs.getInt("network_id");
        String serial = rs.getString("electronic_serial_number");
        String firmwareStr = rs.getString("firmware_version");
        String timezoneStr = rs.getString("time_zone");

        Integer brainTypeId = rs.getInt("brain_type_id");
        Integer isRegistered = rs.getInt("is_registered");
        Integer logPeriod = rs.getInt("log_period");

        Date panelChangedDate = rs.getTimestamp("panel_changed");
        Date personsChangedDate = rs.getTimestamp("persons_changed");
        Date schedulesChangedDate = rs.getTimestamp("schedules_changed");

        ZoneId tz = ZoneId.of(timezoneStr);

        Panel panel = new Panel();
        panel.setBrainId(brainId);
        panel.setObjectId(objectId);
        panel.setAccountId(accountId);
        panel.setNetworkId(networkId);
        panel.setElectronicSerialNumber(serial);
        Firmware firmware = new Firmware(firmwareStr);
        panel.setFirmware(firmware);
        panel.setTimezone(tz);

        panel.setPanelType(PanelType.getPanelType(brainTypeId));
        panel.setIsRegistered(isRegistered == 1);
        panel.setLogPeriod(logPeriod);

        Instant panelChanged = (panelChangedDate == null) ? null : panelChangedDate.toInstant();
        Instant personsChanged = (personsChangedDate == null) ? null : personsChangedDate.toInstant();
        Instant schedulesChanged = (schedulesChangedDate == null) ? null : schedulesChangedDate.toInstant();

        panel.setPanelChanged(panelChanged);
        panel.setPersonsChanged(personsChanged);
        panel.setSchedulesChanged(schedulesChanged);

        return panel;
    }
}
