package com.brivo.panel.server.reference.config;

import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

public abstract class AbstractDataSourceConfig {

    protected static final String DEFAULT_SCHEMA_NAME = "brivo20";

    protected static final String ENTITIES_PACKAGE = "com.brivo.panel.server.reference.domain.entities";

    protected JpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
}
