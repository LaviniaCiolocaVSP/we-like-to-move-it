package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class Elevator {
    private Long deviceId;
    private Integer inputBoardAddress;
    private Integer inputPointAddress;
    private Integer delayTime;
    private TwoFactorCredentialSettings twoFactorCredential;
    private Long cardRequiredScheduleId;
    private AntipassbackSettings antipassback;
    private Boolean reportLiveStatus;
    private KeypadCommand keypadCommand;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getInputBoardAddress() {
        return inputBoardAddress;
    }

    public void setInputBoardAddress(Integer inputBoardAddress) {
        this.inputBoardAddress = inputBoardAddress;
    }

    public Integer getInputPointAddress() {
        return inputPointAddress;
    }

    public void setInputPointAddress(Integer inputPointAddress) {
        this.inputPointAddress = inputPointAddress;
    }

    public Integer getDelayTime() {
        return delayTime;
    }

    public void setDelayTime(Integer delayTime) {
        this.delayTime = delayTime;
    }

    public TwoFactorCredentialSettings getTwoFactorCredential() {
        return twoFactorCredential;
    }

    public void setTwoFactorCredential(
            TwoFactorCredentialSettings twoFactorCredential) {
        this.twoFactorCredential = twoFactorCredential;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    public AntipassbackSettings getAntipassback() {
        return antipassback;
    }

    public void setAntipassback(AntipassbackSettings antipassback) {
        this.antipassback = antipassback;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

    public KeypadCommand getKeypadCommand() {
        return keypadCommand;
    }

    public void setKeypadCommand(KeypadCommand keypadCommand) {
        this.keypadCommand = keypadCommand;
    }
}
