package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "camera_model", schema = "brivo20", catalog = "onair")
public class CameraModel {
    private long cameraModelId;
    private String name;
    private Short unsupported;
    private Collection<CameraModelParam> cameraModelParamsByCameraModelId;
    private Collection<OvrCamera> ovrCamerasByCameraModelId;

    @Id
    @Column(name = "camera_model_id", nullable = false)
    public long getCameraModelId() {
        return cameraModelId;
    }

    public void setCameraModelId(long cameraModelId) {
        this.cameraModelId = cameraModelId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "unsupported", nullable = true)
    public Short getUnsupported() {
        return unsupported;
    }

    public void setUnsupported(Short unsupported) {
        this.unsupported = unsupported;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraModel that = (CameraModel) o;

        if (cameraModelId != that.cameraModelId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return unsupported != null ? unsupported.equals(that.unsupported) : that.unsupported == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraModelId ^ (cameraModelId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (unsupported != null ? unsupported.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "cameraModelByCameraModelId")
    public Collection<CameraModelParam> getCameraModelParamsByCameraModelId() {
        return cameraModelParamsByCameraModelId;
    }

    public void setCameraModelParamsByCameraModelId(Collection<CameraModelParam> cameraModelParamsByCameraModelId) {
        this.cameraModelParamsByCameraModelId = cameraModelParamsByCameraModelId;
    }

    @OneToMany(mappedBy = "cameraModelByCameraModelId")
    public Collection<OvrCamera> getOvrCamerasByCameraModelId() {
        return ovrCamerasByCameraModelId;
    }

    public void setOvrCamerasByCameraModelId(Collection<OvrCamera> ovrCamerasByCameraModelId) {
        this.ovrCamerasByCameraModelId = ovrCamerasByCameraModelId;
    }
}
