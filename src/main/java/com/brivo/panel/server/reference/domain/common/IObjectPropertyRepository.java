package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.ObjectProperty;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IObjectPropertyRepository extends CrudRepository<ObjectProperty, Long> {

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_property op " +
                    "JOIN brivo20.brain b on b.object_id = op.object_id " +
                    "WHERE b.account_id = :accountId",
            nativeQuery = true
    )
    Optional<List<ObjectProperty>> findByAccountId(@Param("accountId") long accountId);
}
