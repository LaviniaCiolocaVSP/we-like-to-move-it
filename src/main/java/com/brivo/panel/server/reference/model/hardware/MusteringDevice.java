package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class MusteringDevice {
    private Long deviceId;
    private Integer inputBoardAddress;
    private Integer inputPointAddress;
    private Boolean reportLiveStatus;
    private KeypadCommand keypadCommand;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getInputBoardAddress() {
        return inputBoardAddress;
    }

    public void setInputBoardAddress(Integer inputBoardAddress) {
        this.inputBoardAddress = inputBoardAddress;
    }

    public Integer getInputPointAddress() {
        return inputPointAddress;
    }

    public void setInputPointAddress(Integer inputPointAddress) {
        this.inputPointAddress = inputPointAddress;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

    public KeypadCommand getKeypadCommands() {
        return keypadCommand;
    }

    public void setKeypadCommand(KeypadCommand keypadCommand) {
        this.keypadCommand = keypadCommand;
    }
}
