package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Arrays;

@Entity
@Table(name = "report_job_output", schema = "brivo20", catalog = "onair")
public class ReportJobOutput {
    private long id;
    private byte[] reportOutput;
    private long reportJobId;
    private long reportFormatId;
    private long outputSize;
    private Timestamp created;
    private Timestamp updated;
    private ReportJob reportJobByReportJobId;
    private ReportFormat reportFormatByReportFormatId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_output", nullable = true)
    public byte[] getReportOutput() {
        return reportOutput;
    }

    public void setReportOutput(byte[] reportOutput) {
        this.reportOutput = reportOutput;
    }

    @Basic
    @Column(name = "report_job_id", nullable = false)
    public long getReportJobId() {
        return reportJobId;
    }

    public void setReportJobId(long reportJobId) {
        this.reportJobId = reportJobId;
    }

    @Basic
    @Column(name = "report_format_id", nullable = false)
    public long getReportFormatId() {
        return reportFormatId;
    }

    public void setReportFormatId(long reportFormatId) {
        this.reportFormatId = reportFormatId;
    }

    @Basic
    @Column(name = "output_size", nullable = false)
    public long getOutputSize() {
        return outputSize;
    }

    public void setOutputSize(long outputSize) {
        this.outputSize = outputSize;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportJobOutput that = (ReportJobOutput) o;

        if (id != that.id) {
            return false;
        }
        if (reportJobId != that.reportJobId) {
            return false;
        }
        if (reportFormatId != that.reportFormatId) {
            return false;
        }
        if (outputSize != that.outputSize) {
            return false;
        }
        if (!Arrays.equals(reportOutput, that.reportOutput)) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + Arrays.hashCode(reportOutput);
        result = 31 * result + (int) (reportJobId ^ (reportJobId >>> 32));
        result = 31 * result + (int) (reportFormatId ^ (reportFormatId >>> 32));
        result = 31 * result + (int) (outputSize ^ (outputSize >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_job_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportJob getReportJobByReportJobId() {
        return reportJobByReportJobId;
    }

    public void setReportJobByReportJobId(ReportJob reportJobByReportJobId) {
        this.reportJobByReportJobId = reportJobByReportJobId;
    }

    @ManyToOne
    @JoinColumn(name = "report_format_id", referencedColumnName = "format_id", nullable = false, insertable = false, updatable = false)
    public ReportFormat getReportFormatByReportFormatId() {
        return reportFormatByReportFormatId;
    }

    public void setReportFormatByReportFormatId(ReportFormat reportFormatByReportFormatId) {
        this.reportFormatByReportFormatId = reportFormatByReportFormatId;
    }
}
