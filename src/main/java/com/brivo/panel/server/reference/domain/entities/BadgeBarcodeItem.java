package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_barcode_item", schema = "brivo20", catalog = "onair")
public class BadgeBarcodeItem {
    private long badgeBarcodeItemId;
    private long badgeItemId;
    private long badgeItemFieldTypeId;
    private long badgeBarcodeEncodingId;
    private String staticValue;
    private Long customFieldId;
    private Long fontSize;
    private Short includeChecksum;
    private Short includeLabel;
    private BadgeItem badgeItemByBadgeItemId;
    private BadgeItemFieldType badgeItemFieldTypeByBadgeItemFieldTypeId;
    private BadgeBarcodeEncoding badgeBarcodeEncodingByBadgeBarcodeEncodingId;
    private Collection<CustomFieldDefinition> badgeBarcodeItemByCustomFieldId;

    @Id
    @Column(name = "badge_barcode_item_id", nullable = false)
    public long getBadgeBarcodeItemId() {
        return badgeBarcodeItemId;
    }

    public void setBadgeBarcodeItemId(long badgeBarcodeItemId) {
        this.badgeBarcodeItemId = badgeBarcodeItemId;
    }

    @Basic
    @Column(name = "badge_item_id", nullable = false)
    public long getBadgeItemId() {
        return badgeItemId;
    }

    public void setBadgeItemId(long badgeItemId) {
        this.badgeItemId = badgeItemId;
    }

    @Basic
    @Column(name = "badge_item_field_type_id", nullable = false)
    public long getBadgeItemFieldTypeId() {
        return badgeItemFieldTypeId;
    }

    public void setBadgeItemFieldTypeId(long badgeItemFieldTypeId) {
        this.badgeItemFieldTypeId = badgeItemFieldTypeId;
    }

    @Basic
    @Column(name = "badge_barcode_encoding_id", nullable = false)
    public long getBadgeBarcodeEncodingId() {
        return badgeBarcodeEncodingId;
    }

    public void setBadgeBarcodeEncodingId(long badgeBarcodeEncodingId) {
        this.badgeBarcodeEncodingId = badgeBarcodeEncodingId;
    }

    @Basic
    @Column(name = "static_value", nullable = true, length = 64)
    public String getStaticValue() {
        return staticValue;
    }

    public void setStaticValue(String staticValue) {
        this.staticValue = staticValue;
    }

    @Basic
    @Column(name = "custom_field_id", nullable = true)
    public Long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(Long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Basic
    @Column(name = "font_size", nullable = true)
    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    @Basic
    @Column(name = "include_checksum", nullable = true)
    public Short getIncludeChecksum() {
        return includeChecksum;
    }

    public void setIncludeChecksum(Short includeChecksum) {
        this.includeChecksum = includeChecksum;
    }

    @Basic
    @Column(name = "include_label", nullable = true)
    public Short getIncludeLabel() {
        return includeLabel;
    }

    public void setIncludeLabel(Short includeLabel) {
        this.includeLabel = includeLabel;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeBarcodeItem that = (BadgeBarcodeItem) o;

        if (badgeBarcodeItemId != that.badgeBarcodeItemId) {
            return false;
        }
        if (badgeItemId != that.badgeItemId) {
            return false;
        }
        if (badgeItemFieldTypeId != that.badgeItemFieldTypeId) {
            return false;
        }
        if (badgeBarcodeEncodingId != that.badgeBarcodeEncodingId) {
            return false;
        }
        if (staticValue != null ? !staticValue.equals(that.staticValue) : that.staticValue != null) {
            return false;
        }
        if (customFieldId != null ? !customFieldId.equals(that.customFieldId) : that.customFieldId != null) {
            return false;
        }
        if (fontSize != null ? !fontSize.equals(that.fontSize) : that.fontSize != null) {
            return false;
        }
        if (includeChecksum != null ? !includeChecksum.equals(that.includeChecksum) : that.includeChecksum != null) {
            return false;
        }
        return includeLabel != null ? includeLabel.equals(that.includeLabel) : that.includeLabel == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeBarcodeItemId ^ (badgeBarcodeItemId >>> 32));
        result = 31 * result + (int) (badgeItemId ^ (badgeItemId >>> 32));
        result = 31 * result + (int) (badgeItemFieldTypeId ^ (badgeItemFieldTypeId >>> 32));
        result = 31 * result + (int) (badgeBarcodeEncodingId ^ (badgeBarcodeEncodingId >>> 32));
        result = 31 * result + (staticValue != null ? staticValue.hashCode() : 0);
        result = 31 * result + (customFieldId != null ? customFieldId.hashCode() : 0);
        result = 31 * result + (fontSize != null ? fontSize.hashCode() : 0);
        result = 31 * result + (includeChecksum != null ? includeChecksum.hashCode() : 0);
        result = 31 * result + (includeLabel != null ? includeLabel.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_id", referencedColumnName = "badge_item_id", nullable = false, insertable = false, updatable = false)
    public BadgeItem getBadgeItemByBadgeItemId() {
        return badgeItemByBadgeItemId;
    }

    public void setBadgeItemByBadgeItemId(BadgeItem badgeItemByBadgeItemId) {
        this.badgeItemByBadgeItemId = badgeItemByBadgeItemId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_field_type_id", referencedColumnName = "badge_item_field_type_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeItemFieldType getBadgeItemFieldTypeByBadgeItemFieldTypeId() {
        return badgeItemFieldTypeByBadgeItemFieldTypeId;
    }

    public void setBadgeItemFieldTypeByBadgeItemFieldTypeId(BadgeItemFieldType badgeItemFieldTypeByBadgeItemFieldTypeId) {
        this.badgeItemFieldTypeByBadgeItemFieldTypeId = badgeItemFieldTypeByBadgeItemFieldTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_barcode_encoding_id", referencedColumnName = "badge_barcode_encoding_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeBarcodeEncoding getBadgeBarcodeEncodingByBadgeBarcodeEncodingId() {
        return badgeBarcodeEncodingByBadgeBarcodeEncodingId;
    }

    public void setBadgeBarcodeEncodingByBadgeBarcodeEncodingId(BadgeBarcodeEncoding badgeBarcodeEncodingByBadgeBarcodeEncodingId) {
        this.badgeBarcodeEncodingByBadgeBarcodeEncodingId = badgeBarcodeEncodingByBadgeBarcodeEncodingId;
    }

    @OneToMany(mappedBy = "badgeBarcodeItemByCustomFieldId")
    public Collection<CustomFieldDefinition> getBadgeBarcodeItemByCustomFieldId() {
        return badgeBarcodeItemByCustomFieldId;
    }

    public void setBadgeBarcodeItemByCustomFieldId(Collection<CustomFieldDefinition> badgeBarcodeItemByCustomFieldId) {
        this.badgeBarcodeItemByCustomFieldId = badgeBarcodeItemByCustomFieldId;
    }
}
