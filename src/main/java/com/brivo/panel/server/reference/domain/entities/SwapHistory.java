package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "swap_history", schema = "brivo20", catalog = "onair")
public class SwapHistory {
    private long brainId;
    private long accountId;
    private Timestamp swappedIn;
    private Long replacesBrainId;
    private String oldPhysicalAddress;
    private String newPhysicalAddress;
    private String description;
    private Brain brainByBrainId;
    private Account accountByAccountId;
    private Brain brainByReplacesBrainId;

    @Id
    @Basic
    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "swapped_in", nullable = false)
    public Timestamp getSwappedIn() {
        return swappedIn;
    }

    public void setSwappedIn(Timestamp swappedIn) {
        this.swappedIn = swappedIn;
    }

    @Basic
    @Column(name = "replaces_brain_id", nullable = true)
    public Long getReplacesBrainId() {
        return replacesBrainId;
    }

    public void setReplacesBrainId(Long replacesBrainId) {
        this.replacesBrainId = replacesBrainId;
    }

    @Basic
    @Column(name = "old_physical_address", nullable = true, length = 128)
    public String getOldPhysicalAddress() {
        return oldPhysicalAddress;
    }

    public void setOldPhysicalAddress(String oldPhysicalAddress) {
        this.oldPhysicalAddress = oldPhysicalAddress;
    }

    @Basic
    @Column(name = "new_physical_address", nullable = true, length = 128)
    public String getNewPhysicalAddress() {
        return newPhysicalAddress;
    }

    public void setNewPhysicalAddress(String newPhysicalAddress) {
        this.newPhysicalAddress = newPhysicalAddress;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SwapHistory that = (SwapHistory) o;
        return brainId == that.brainId &&
                accountId == that.accountId &&
                Objects.equals(swappedIn, that.swappedIn) &&
                Objects.equals(replacesBrainId, that.replacesBrainId) &&
                Objects.equals(oldPhysicalAddress, that.oldPhysicalAddress) &&
                Objects.equals(newPhysicalAddress, that.newPhysicalAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brainId, accountId, swappedIn, replacesBrainId, oldPhysicalAddress, newPhysicalAddress);
    }

    @ManyToOne
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByBrainId() {
        return brainByBrainId;
    }

    public void setBrainByBrainId(Brain brainByBrainId) {
        this.brainByBrainId = brainByBrainId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "replaces_brain_id", referencedColumnName = "brain_id", insertable = false, updatable = false)
    public Brain getBrainByReplacesBrainId() {
        return brainByReplacesBrainId;
    }

    public void setBrainByReplacesBrainId(Brain brainByReplacesBrainId) {
        this.brainByReplacesBrainId = brainByReplacesBrainId;
    }
}
