package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.OsdpSettings;
import com.brivo.panel.server.reference.model.hardware.Rs485Settings;
import com.google.common.collect.Lists;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class Rs485SettingsExtractor implements RowMapper<Rs485Settings> {
    @Override
    public Rs485Settings mapRow(ResultSet rs, int rowNum) throws SQLException {
        Rs485Settings settings = new Rs485Settings();

        settings.setBaudRate(rs.getInt("baud_rate"));
        settings.setOperationMode(rs.getInt("operation_mode"));
        settings.setPortNum(rs.getInt("port"));

        if (settings.getOperationMode() == 0) {
            settings.setOsdpSettings(new OsdpSettings());
            settings.getOsdpSettings().setErrorDetectionMethod(rs.getInt("error_detection_method"));
            settings.getOsdpSettings().setPdAddress(Lists.newArrayList(0));
        }

        return settings;
    }
}
