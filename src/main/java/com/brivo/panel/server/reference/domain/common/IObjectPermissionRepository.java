package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IObjectPermissionRepository extends CrudRepository<ObjectPermission, Long> {

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_permission op " +
                    "JOIN brivo20.device d ON op.object_id = d.object_id " +
                    "JOIN brivo20.security_group sg ON op.actor_object_id = sg.object_id " +
                    "WHERE sg.object_id = :objectId " +
                    "AND sg.security_group_type_id = 2 ",  //User group
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findByGroupObjectId(@Param("objectId") Long objectId);

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_permission op " +
                    "WHERE op.security_action_id = :securityActionId " +
                    "AND op.object_id = :rootObjectId ",
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findKeypadLockObjectPermissions(@Param("rootObjectId") Long rootObjectId,
                                                                     @Param("securityActionId") int securityActionId);
}