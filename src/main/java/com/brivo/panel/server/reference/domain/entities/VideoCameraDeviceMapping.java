package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "video_camera_device_mapping", schema = "brivo20", catalog = "onair")
public class VideoCameraDeviceMapping {
    private long videoCameraDeviceMappingId;
    private long videoCameraId;
    private long deviceId;
    private VideoCamera videoCameraByVideoCameraId;
//    private Device deviceByDeviceId;

    @Id
    @Column(name = "video_camera_device_mapping_id", nullable = false)
    public long getVideoCameraDeviceMappingId() {
        return videoCameraDeviceMappingId;
    }

    public void setVideoCameraDeviceMappingId(long videoCameraDeviceMappingId) {
        this.videoCameraDeviceMappingId = videoCameraDeviceMappingId;
    }

    @Basic
    @Column(name = "video_camera_id", nullable = false)
    public long getVideoCameraId() {
        return videoCameraId;
    }

    public void setVideoCameraId(long videoCameraId) {
        this.videoCameraId = videoCameraId;
    }

    @Basic
    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoCameraDeviceMapping that = (VideoCameraDeviceMapping) o;

        if (videoCameraDeviceMappingId != that.videoCameraDeviceMappingId) {
            return false;
        }
        if (videoCameraId != that.videoCameraId) {
            return false;
        }
        return deviceId == that.deviceId;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoCameraDeviceMappingId ^ (videoCameraDeviceMappingId >>> 32));
        result = 31 * result + (int) (videoCameraId ^ (videoCameraId >>> 32));
        result = 31 * result + (int) (deviceId ^ (deviceId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "video_camera_id", referencedColumnName = "video_camera_id", nullable = false, insertable = false, updatable = false)
    public VideoCamera getVideoCameraByVideoCameraId() {
        return videoCameraByVideoCameraId;
    }

    public void setVideoCameraByVideoCameraId(VideoCamera videoCameraByVideoCameraId) {
        this.videoCameraByVideoCameraId = videoCameraByVideoCameraId;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "device_id", nullable = false, insertable = false, updatable = false)
    public Device getDeviceByDeviceId() {
        return deviceByDeviceId;
    }

    public void setDeviceByDeviceId(Device deviceByDeviceId) {
        this.deviceByDeviceId = deviceByDeviceId;
    }
    */
}
