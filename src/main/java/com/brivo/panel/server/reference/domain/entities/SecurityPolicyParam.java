package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "security_policy_param", schema = "brivo20", catalog = "onair")
public class SecurityPolicyParam {
    private long policyParamId;
    private long policyId;
    private String name;
    private String value;
    private Long accountId;
    private SecurityPolicy securityPolicyByPolicyId;
    private Account accountByAccountId;

    @Id
    @Column(name = "policy_param_id", nullable = false)
    public long getPolicyParamId() {
        return policyParamId;
    }

    public void setPolicyParamId(long policyParamId) {
        this.policyParamId = policyParamId;
    }

    @Basic
    @Column(name = "policy_id", nullable = false)
    public long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 40)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityPolicyParam that = (SecurityPolicyParam) o;

        if (policyParamId != that.policyParamId) {
            return false;
        }
        if (policyId != that.policyId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        return accountId != null ? accountId.equals(that.accountId) : that.accountId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (policyParamId ^ (policyParamId >>> 32));
        result = 31 * result + (int) (policyId ^ (policyId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "policy_id", referencedColumnName = "policy_id", nullable = false, insertable = false, updatable = false)
    public SecurityPolicy getSecurityPolicyByPolicyId() {
        return securityPolicyByPolicyId;
    }

    public void setSecurityPolicyByPolicyId(SecurityPolicy securityPolicyByPolicyId) {
        this.securityPolicyByPolicyId = securityPolicyByPolicyId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
