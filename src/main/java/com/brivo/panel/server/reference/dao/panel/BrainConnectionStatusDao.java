package com.brivo.panel.server.reference.dao.panel;

import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.model.hardware.BrainConnectionStatus;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@BrivoDao
public class BrainConnectionStatusDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(BrainConnectionStatusDao.class);

    @Autowired
    private NamedParameterJdbcTemplate template;

    /**
     * Update all rows in brain_connection_status where the message_queue_name
     * is the input hostPort value. For all such rows set message_queue_name and
     * connect_time to null.
     *
     * @param messageQueueName Indicates rows for update.
     * @return Affected row count.
     */
    public void cleanAllServerConnectionStatus(String messageQueueName) {
        LOGGER.info("Removing panel connections from message queue {}", messageQueueName);

        String query = BrainConnectionQuery.CLEAR_ALL_SERVER_BRAIN_CONNECTION_STATUS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("messageQueueName", messageQueueName);

        template.update(query, paramMap);
    }

    /**
     * Update brain_connection_status and set the message_queue_name,
     * connect_time, version, and ip_address columns for the one
     * row associated with the panel serial number. This should affect
     * zero or one rows.
     *
     * @param panelObjId       Indicates the row to change.
     * @param messageQueueName The new name.
     * @param ipAddress        The new IP Address.
     * @return Count of affected rows.
     */
    public int upsertBrainConnectionStatus(Long panelObjId, String messageQueueName, String ipAddress) {
        String updateQuery = BrainConnectionQuery.UPDATE_BRAIN_CONNECTION_STATUS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("messageQueueName", messageQueueName);
        paramMap.put("ipAddress", ipAddress);
        paramMap.put("panelObjId", panelObjId);

        int rows = template.update(updateQuery, paramMap);
        if (rows == 0) {
            String insertQuery = BrainConnectionQuery.INSERT_BRAIN_CONNECTION_STATUS.getQuery();

            rows = template.update(insertQuery, paramMap);
        }

        return rows;
    }


    public BrainConnectionStatus getBrainConnectionStatus(Long panelObjId) {
        String query = BrainConnectionQuery.GET_BRAIN_CONNECTION_STATUS.getQuery();
        Map<String, Object> paramMap = Collections.singletonMap("panelObjId", panelObjId);

        List<BrainConnectionStatus> statuses = template.query(query, paramMap, (rs, rowNum) -> getBrainConnectionStatus(rs));

        return Iterables.getFirst(statuses, null);
    }

    private BrainConnectionStatus getBrainConnectionStatus(final ResultSet rs) throws SQLException {
        BrainConnectionStatus status = new BrainConnectionStatus();

        status.setBrainId(rs.getLong("brainId"));
        status.setMessageQueue(rs.getString("messageQueueName"));
        status.setConnectTime(rs.getTimestamp("connectTime"));
        status.setIpAddress(rs.getString("ipAddress"));

        return status;
    }


    public void upsertBrainState(Long panelObjId, String ipAddress, String firmwareVersion, Instant panelStateTime) {
        String updateQuery = BrainConnectionQuery.UPDATE_BRAIN_STATE.getQuery();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("ipAddress", ipAddress);
        paramMap.put("panelStateTime", Date.from(panelStateTime));
        paramMap.put("firmwareVersion", firmwareVersion);
        paramMap.put("panelObjId", panelObjId);
        int rows = template.update(updateQuery, paramMap);
        if (rows == 0) {
            String insertQuery = BrainConnectionQuery.INSERT_BRAIN_STATE.getQuery();
            template.update(insertQuery, paramMap);
        }
    }

    public String getLastReportedFirmwareVersion(Long panelObjectId) {
        Map<String, Object> params = Collections.singletonMap("panelObjId", panelObjectId);

        String query = BrainConnectionQuery.GET_REPORTED_FIRMWARE.getQuery();
        List<String> firmwareVersion = template.queryForList(query, params, String.class);
        return Iterables.getFirst(firmwareVersion, null);
    }

    /**
     * Update brain_connection_status and set the message_queue_name,
     * and connect_time columns to null for the one row associated with
     * the panel serial number. This should affect one row.
     *
     * @param panelSerialNumber Indicates the row to change.
     * @return Count of affected rows.
     */
    public void updateClearBrainConnectionStatus(String panelSerialNumber) {
        String query = BrainConnectionQuery.CLEAR_BRAIN_CONNECTION_STATUS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerial", panelSerialNumber);

        template.update(query, paramMap);
    }
}
