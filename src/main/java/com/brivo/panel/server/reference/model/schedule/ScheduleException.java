package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ScheduleException {
    private Long scheduleId;
    private Boolean isEnable;
    private ScheduleExceptionRepeatType repeatType;
    private ScheduleExceptionRepeatDay repeatDay;
    private ScheduleExceptionRepeatWeek repeatWeek;
    private Integer startMinute;
    private Integer endMinute;
    private Long startDateSeconds;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Boolean getIsEnable() {
        return isEnable;
    }

    public void setIsEnable(Boolean isEnable) {
        this.isEnable = isEnable;
    }

    public ScheduleExceptionRepeatType getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(ScheduleExceptionRepeatType repeatType) {
        this.repeatType = repeatType;
    }

    public ScheduleExceptionRepeatDay getRepeatDay() {
        return repeatDay;
    }

    public void setRepeatDay(ScheduleExceptionRepeatDay repeatDay) {
        this.repeatDay = repeatDay;
    }

    public ScheduleExceptionRepeatWeek getRepeatWeek() {
        return repeatWeek;
    }

    public void setRepeatWeek(ScheduleExceptionRepeatWeek repeatWeek) {
        this.repeatWeek = repeatWeek;
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Integer getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(Integer endMinute) {
        this.endMinute = endMinute;
    }

    public Long getStartDateSeconds() {
        return startDateSeconds;
    }

    public void setStartDateSeconds(Long startDate) {
        this.startDateSeconds = startDate;
    }
}
