package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.ProgrammableDeviceOutput;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Programmable Device Output Extractor.
 */
@Component
public class ProgrammableDeviceOutputExtractor implements ResultSetExtractor<List<ProgrammableDeviceOutput>> {

    public static final int DOOR_BOARD_5K_BOARD_TYPE = 1;
    public static final int DOOR_1_DB_DOOR_LOCK_RELAY_POINT = 3;
    public static final int DOOR_2_DB_DOOR_LOCK_RELAY_POINT = 16;

    @Override
    public List<ProgrammableDeviceOutput> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<ProgrammableDeviceOutput> list = new ArrayList<ProgrammableDeviceOutput>();

        while (rs.next()) {
            Integer boardNumber = rs.getInt("boardNumber");
            Integer pointAddress = rs.getInt("pointAddress");
            Integer behavior = rs.getInt("behavior");
            Integer argument = rs.getInt("argument");
            Integer boardType = rs.getInt("boardType");

            ProgrammableDeviceOutput output = new ProgrammableDeviceOutput();

            output.setBoardAddress(boardNumber);
            output.setPointAddress(pointAddress);
            output.setBehavior(behavior);
            output.setBehaviorArgument(argument);

            /**
             * FIXME: This is a hack that was previously done on the panel for 5K panels.
             * The IO Points template in the DB for 5K has the wrong points for the Door Lock Relays (3/16 instead of
             * 4/17).
             * This adjustment of the io points should be removed when we can start using a board type for the 6000s
             */
            if (boardType == DOOR_BOARD_5K_BOARD_TYPE &&
                    (output.getPointAddress() == DOOR_1_DB_DOOR_LOCK_RELAY_POINT ||
                            output.getPointAddress() == DOOR_2_DB_DOOR_LOCK_RELAY_POINT)) {
                output.setPointAddress(output.getPointAddress() + 1);
            }

            list.add(output);
        }

        return list;
    }
}
