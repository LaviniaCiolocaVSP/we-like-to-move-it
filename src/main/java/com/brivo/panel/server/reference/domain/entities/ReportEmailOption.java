package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_email_option", schema = "brivo20", catalog = "onair")
public class ReportEmailOption {
    private long id;
    private String name;
    private Short enabled;
    private Collection<ReportJob> reportJobsById;
    private Collection<ReportSchedule> reportSchedulesById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 35)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "enabled", nullable = true)
    public Short getEnabled() {
        return enabled;
    }

    public void setEnabled(Short enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportEmailOption that = (ReportEmailOption) o;

        if (id != that.id) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return enabled != null ? enabled.equals(that.enabled) : that.enabled == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportEmailOptionByEmailOptionId")
    public Collection<ReportJob> getReportJobsById() {
        return reportJobsById;
    }

    public void setReportJobsById(Collection<ReportJob> reportJobsById) {
        this.reportJobsById = reportJobsById;
    }

    @OneToMany(mappedBy = "reportEmailOptionByEmailOptionId")
    public Collection<ReportSchedule> getReportSchedulesById() {
        return reportSchedulesById;
    }

    public void setReportSchedulesById(Collection<ReportSchedule> reportSchedulesById) {
        this.reportSchedulesById = reportSchedulesById;
    }
}
