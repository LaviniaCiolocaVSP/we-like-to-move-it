package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "two_factor_auth_method", schema = "brivo20", catalog = "onair")
public class TwoFactorAuthMethod {
    private long twoFactorAuthMethodId;
    private String twoFactorAuthMethodName;
    private Collection<Login> loginsByTwoFactorAuthMethodId;

    @Id
    @Column(name = "two_factor_auth_method_id", nullable = false)
    public long getTwoFactorAuthMethodId() {
        return twoFactorAuthMethodId;
    }

    public void setTwoFactorAuthMethodId(long twoFactorAuthMethodId) {
        this.twoFactorAuthMethodId = twoFactorAuthMethodId;
    }

    @Basic
    @Column(name = "two_factor_auth_method_name", nullable = false, length = 32)
    public String getTwoFactorAuthMethodName() {
        return twoFactorAuthMethodName;
    }

    public void setTwoFactorAuthMethodName(String twoFactorAuthMethodName) {
        this.twoFactorAuthMethodName = twoFactorAuthMethodName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TwoFactorAuthMethod that = (TwoFactorAuthMethod) o;

        if (twoFactorAuthMethodId != that.twoFactorAuthMethodId) {
            return false;
        }
        return twoFactorAuthMethodName != null ? twoFactorAuthMethodName.equals(that.twoFactorAuthMethodName) : that.twoFactorAuthMethodName == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (twoFactorAuthMethodId ^ (twoFactorAuthMethodId >>> 32));
        result = 31 * result + (twoFactorAuthMethodName != null ? twoFactorAuthMethodName.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "twoFactorAuthMethodByTwoFactorAuthMethodId")
    public Collection<Login> getLoginsByTwoFactorAuthMethodId() {
        return loginsByTwoFactorAuthMethodId;
    }

    public void setLoginsByTwoFactorAuthMethodId(Collection<Login> loginsByTwoFactorAuthMethodId) {
        this.loginsByTwoFactorAuthMethodId = loginsByTwoFactorAuthMethodId;
    }
}
