package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "camera_motion_status", schema = "brivo20", catalog = "onair")
public class CameraMotionStatus {
    private long cameraId;
    private Timestamp startEmailInterval;
    private long emailCountSent;
    private long emailCountNotSent;
    private Timestamp startMotionInterval;
    private long motionIntervalCounter;
    private OvrCamera ovrCameraByCameraId;

    @Id
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "start_email_interval", nullable = false)
    public Timestamp getStartEmailInterval() {
        return startEmailInterval;
    }

    public void setStartEmailInterval(Timestamp startEmailInterval) {
        this.startEmailInterval = startEmailInterval;
    }

    @Basic
    @Column(name = "email_count_sent", nullable = false)
    public long getEmailCountSent() {
        return emailCountSent;
    }

    public void setEmailCountSent(long emailCountSent) {
        this.emailCountSent = emailCountSent;
    }

    @Basic
    @Column(name = "email_count_not_sent", nullable = false)
    public long getEmailCountNotSent() {
        return emailCountNotSent;
    }

    public void setEmailCountNotSent(long emailCountNotSent) {
        this.emailCountNotSent = emailCountNotSent;
    }

    @Basic
    @Column(name = "start_motion_interval", nullable = false)
    public Timestamp getStartMotionInterval() {
        return startMotionInterval;
    }

    public void setStartMotionInterval(Timestamp startMotionInterval) {
        this.startMotionInterval = startMotionInterval;
    }

    @Basic
    @Column(name = "motion_interval_counter", nullable = false)
    public long getMotionIntervalCounter() {
        return motionIntervalCounter;
    }

    public void setMotionIntervalCounter(long motionIntervalCounter) {
        this.motionIntervalCounter = motionIntervalCounter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraMotionStatus that = (CameraMotionStatus) o;

        if (cameraId != that.cameraId) {
            return false;
        }
        if (emailCountSent != that.emailCountSent) {
            return false;
        }
        if (emailCountNotSent != that.emailCountNotSent) {
            return false;
        }
        if (motionIntervalCounter != that.motionIntervalCounter) {
            return false;
        }
        if (startEmailInterval != null ? !startEmailInterval.equals(that.startEmailInterval) : that.startEmailInterval != null) {
            return false;
        }
        return startMotionInterval != null ? startMotionInterval.equals(that.startMotionInterval) : that.startMotionInterval == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (startEmailInterval != null ? startEmailInterval.hashCode() : 0);
        result = 31 * result + (int) (emailCountSent ^ (emailCountSent >>> 32));
        result = 31 * result + (int) (emailCountNotSent ^ (emailCountNotSent >>> 32));
        result = 31 * result + (startMotionInterval != null ? startMotionInterval.hashCode() : 0);
        result = 31 * result + (int) (motionIntervalCounter ^ (motionIntervalCounter >>> 32));
        return result;
    }

    @OneToOne
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id", nullable = false)
    public OvrCamera getOvrCameraByCameraId() {
        return ovrCameraByCameraId;
    }

    public void setOvrCameraByCameraId(OvrCamera ovrCameraByCameraId) {
        this.ovrCameraByCameraId = ovrCameraByCameraId;
    }
}
