package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

import java.util.List;

public class SetScheduleThreatLevelMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private List<ScheduleThreatLevelMessage> scheduleThreats;

    public SetScheduleThreatLevelMessage() {
        super(DownstreamMessageType.SetScheduleThreatLevelMessage);
    }

    public List<ScheduleThreatLevelMessage> getScheduleThreats() {
        return scheduleThreats;
    }

    public void setScheduleThreats(List<ScheduleThreatLevelMessage> scheduleThreats) {
        this.scheduleThreats = scheduleThreats;
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }
}
