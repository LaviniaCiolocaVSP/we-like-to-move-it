package com.brivo.panel.server.reference.domain.entities;


import java.io.Serializable;
import java.util.Objects;
import java.util.stream.Stream;

public enum PanelType {
    
    NULL_STATE(-1L, "none", 0),
    IRIS_5_0(5L, "IRIS V5.0", 15),
    IRIS_5_0_CDMA(6L, "IRIS V5.0 CDMA", 15),
    IRIS_5_0_GSM(7L, "IRIS V5.0 GSM", 15),
    STANDALONE_1_0(8L, "Standalone 1.0", 15),
    EDGE(9L, "HID Edge", 1),
    ONSITE_SE(10L, "OnSite SE", 1),
    ONSITE_XE(11L, "OnSite XE", 1),
    SDC(12L, "SDC", 1),
    SDC_S(13L, "SDC-S", 1),
    APARATO(14L, "Aparato", 15),
    IPDC1(15L, "IPDC 1-Door", 1),
    IPDC2(16L, "IPDC 2-Door", 1),
    IPAC_STANDALONE(20L, "IPAC Standalone", 1),
    IPAC_SERVER_APARATO(21L, "IPAC Server Aparato", 1),
    IPAC_CLOUD(22L, "IPAC Cloud", 1),
    ACS6000(28L, "ACS6000", 15),
    ACS300(33L, "ACS300", 1),
    MERCURY_EP4502(40L, "Mercury EP4502", 65),
    MERCURY_EP1502(41L, "Mercury EP1502", 33);
    
    private final Long panelTypeId;
    private final String description;
    private final int maxNumberOfBoards;
    
    PanelType(Long panelTypeId, String description, int maxNumberOfBoards) {
        this.panelTypeId = panelTypeId;
        this.description = description;
        this.maxNumberOfBoards = maxNumberOfBoards;
    }
    
    public static PanelType getPanelType(Long typeId) {
        for (PanelType type : PanelType.values()) {
            if (type.getPanelTypeId() == typeId) {
                return type;
            }
        }
        
        return NULL_STATE;
    }
    
    public Long getPanelTypeId() {
        return panelTypeId;
    }
    
    public String getDescription() {
        return description;
    }
    
    public int getMaxNumberOfBoards() {
        return maxNumberOfBoards;
    }
}
