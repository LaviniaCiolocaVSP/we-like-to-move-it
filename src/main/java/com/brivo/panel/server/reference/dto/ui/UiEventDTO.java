package com.brivo.panel.server.reference.dto.ui;

import net.sf.cglib.core.Local;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;

public class UiEventDTO implements Serializable {
    private long eventId;
    private long panelObjectId;
    private String eventType;
    private LocalDateTime eventTime;
    private String eventData;
    private String electronicSerialNumber;
    private String eventTimeString;

    public UiEventDTO() {

    }

    public UiEventDTO(final long eventId,
                      final long panelObjectId,
                      final String eventType,
                      final LocalDateTime eventTime,
                      final String eventData,
                      final String electronicSerialNumber) {
        this.eventId = eventId;
        this.panelObjectId = panelObjectId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.eventData = eventData;
        this.electronicSerialNumber = electronicSerialNumber;
    }

    public UiEventDTO(long eventId, long panelObjectId, String eventType, LocalDateTime eventTime, String eventData, String electronicSerialNumber, String eventTimeString) {
        this.eventId = eventId;
        this.panelObjectId = panelObjectId;
        this.eventType = eventType;
        this.eventTime = eventTime;
        this.eventData = eventData;
        this.electronicSerialNumber = electronicSerialNumber;
        this.eventTimeString = eventTimeString;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(final long eventId) {
        this.eventId = eventId;
    }

    public long getPanelObjectId() {
        return panelObjectId;
    }

    public void setPanelObjectId(final long panelObjectId) {
        this.panelObjectId = panelObjectId;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(final String eventType) {
        this.eventType = eventType;
    }

    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(final LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }

    public String getEventData() {
        return eventData;
    }

    public void setEventData(final String eventData) {
        this.eventData = eventData;
    }

    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(final String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    public String getEventTimeString() {
        return eventTimeString;
    }

    public void setEventTimeString(String eventTimeString) {
        this.eventTimeString = eventTimeString;
    }
}
