package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "property_type", schema = "brivo20", catalog = "onair")
public class PropertyType {
    private long propertyTypeId;
    private String name;
    private Collection<DefaultPanelProp> defaultPanelPropsByPropertyTypeId;

    @Id
    @Column(name = "property_type_id", nullable = false)
    public long getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(long propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PropertyType that = (PropertyType) o;

        if (propertyTypeId != that.propertyTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (propertyTypeId ^ (propertyTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "propertyTypeByPropertyTypeId")
    public Collection<DefaultPanelProp> getDefaultPanelPropsByPropertyTypeId() {
        return defaultPanelPropsByPropertyTypeId;
    }

    public void setDefaultPanelPropsByPropertyTypeId(Collection<DefaultPanelProp> defaultPanelPropsByPropertyTypeId) {
        this.defaultPanelPropsByPropertyTypeId = defaultPanelPropsByPropertyTypeId;
    }
}
