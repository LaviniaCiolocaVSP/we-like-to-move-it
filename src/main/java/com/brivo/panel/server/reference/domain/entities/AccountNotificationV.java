package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "account_notification_v", schema = "brivo20", catalog = "onair")
public class AccountNotificationV {
    private Long notificationTypeId;
    private Long emailId;
    private String emailValue;
    private String emailType;
    private Long emailNotifyBrivoInfo;
    private Timestamp emailCreated;
    private Timestamp emailUpdated;
    private Long emailLanguageId;
    private Long accountId;
    private Long accountObjectId;
    private String accountName;
    private String accountDescription;
    private String accountNumber;
    private Timestamp accountRegistered;
    private Timestamp accountActivated;
    private Timestamp accountClosed;
    private String accountStatus;
    private Timestamp accountCreated;
    private Timestamp accountUpdated;
    private Long opsFlag;
    private Long dealerAccountId;
    private String dealerRefNum;

    @Id
    @Basic
    @Column(name = "notification_type_id", nullable = true)
    public Long getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(Long notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    @Basic
    @Column(name = "email_id", nullable = true)
    public Long getEmailId() {
        return emailId;
    }

    public void setEmailId(Long emailId) {
        this.emailId = emailId;
    }

    @Basic
    @Column(name = "email_value", nullable = true, length = 512)
    public String getEmailValue() {
        return emailValue;
    }

    public void setEmailValue(String emailValue) {
        this.emailValue = emailValue;
    }

    @Basic
    @Column(name = "email_type", nullable = true, length = 32)
    public String getEmailType() {
        return emailType;
    }

    public void setEmailType(String emailType) {
        this.emailType = emailType;
    }

    @Basic
    @Column(name = "email_notify_brivo_info", nullable = true)
    public Long getEmailNotifyBrivoInfo() {
        return emailNotifyBrivoInfo;
    }

    public void setEmailNotifyBrivoInfo(Long emailNotifyBrivoInfo) {
        this.emailNotifyBrivoInfo = emailNotifyBrivoInfo;
    }

    @Basic
    @Column(name = "email_created", nullable = true)
    public Timestamp getEmailCreated() {
        return emailCreated;
    }

    public void setEmailCreated(Timestamp emailCreated) {
        this.emailCreated = emailCreated;
    }

    @Basic
    @Column(name = "email_updated", nullable = true)
    public Timestamp getEmailUpdated() {
        return emailUpdated;
    }

    public void setEmailUpdated(Timestamp emailUpdated) {
        this.emailUpdated = emailUpdated;
    }

    @Basic
    @Column(name = "email_language_id", nullable = true)
    public Long getEmailLanguageId() {
        return emailLanguageId;
    }

    public void setEmailLanguageId(Long emailLanguageId) {
        this.emailLanguageId = emailLanguageId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "account_object_id", nullable = true)
    public Long getAccountObjectId() {
        return accountObjectId;
    }

    public void setAccountObjectId(Long accountObjectId) {
        this.accountObjectId = accountObjectId;
    }

    @Basic
    @Column(name = "account_name", nullable = true, length = 32)
    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    @Basic
    @Column(name = "account_description", nullable = true, length = 256)
    public String getAccountDescription() {
        return accountDescription;
    }

    public void setAccountDescription(String accountDescription) {
        this.accountDescription = accountDescription;
    }

    @Basic
    @Column(name = "account_number", nullable = true, length = 10)
    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    @Basic
    @Column(name = "account_registered", nullable = true)
    public Timestamp getAccountRegistered() {
        return accountRegistered;
    }

    public void setAccountRegistered(Timestamp accountRegistered) {
        this.accountRegistered = accountRegistered;
    }

    @Basic
    @Column(name = "account_activated", nullable = true)
    public Timestamp getAccountActivated() {
        return accountActivated;
    }

    public void setAccountActivated(Timestamp accountActivated) {
        this.accountActivated = accountActivated;
    }

    @Basic
    @Column(name = "account_closed", nullable = true)
    public Timestamp getAccountClosed() {
        return accountClosed;
    }

    public void setAccountClosed(Timestamp accountClosed) {
        this.accountClosed = accountClosed;
    }

    @Basic
    @Column(name = "account_status", nullable = true, length = 32)
    public String getAccountStatus() {
        return accountStatus;
    }

    public void setAccountStatus(String accountStatus) {
        this.accountStatus = accountStatus;
    }

    @Basic
    @Column(name = "account_created", nullable = true)
    public Timestamp getAccountCreated() {
        return accountCreated;
    }

    public void setAccountCreated(Timestamp accountCreated) {
        this.accountCreated = accountCreated;
    }

    @Basic
    @Column(name = "account_updated", nullable = true)
    public Timestamp getAccountUpdated() {
        return accountUpdated;
    }

    public void setAccountUpdated(Timestamp accountUpdated) {
        this.accountUpdated = accountUpdated;
    }

    @Basic
    @Column(name = "ops_flag", nullable = true)
    public Long getOpsFlag() {
        return opsFlag;
    }

    public void setOpsFlag(Long opsFlag) {
        this.opsFlag = opsFlag;
    }

    @Basic
    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Basic
    @Column(name = "dealer_ref_num", nullable = true, length = 50)
    public String getDealerRefNum() {
        return dealerRefNum;
    }

    public void setDealerRefNum(String dealerRefNum) {
        this.dealerRefNum = dealerRefNum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountNotificationV that = (AccountNotificationV) o;
        return Objects.equals(notificationTypeId, that.notificationTypeId) &&
                Objects.equals(emailId, that.emailId) &&
                Objects.equals(emailValue, that.emailValue) &&
                Objects.equals(emailType, that.emailType) &&
                Objects.equals(emailNotifyBrivoInfo, that.emailNotifyBrivoInfo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notificationTypeId, emailId, emailValue, emailType, emailNotifyBrivoInfo);
    }
}
