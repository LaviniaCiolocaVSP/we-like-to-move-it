package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "camera_migration_history", schema = "brivo20", catalog = "onair")
public class CameraMigrationHistory {
    private long cameraMigrationHistoryId;
    private long cameraMigrationId;
    private long cameraId;
    private long previousAvhsServerId;
    private String migrationStatus;
    private String migrationDetails;
    private Timestamp migrationCompletionDate;
    private Timestamp created;
    private String ongoingStatus;
    private Long migrationTime;
    private CameraMigration cameraMigrationByCameraMigrationId;
    private OvrCamera ovrCameraByCameraId;
    private AvhsServer avhsServerByPreviousAvhsServerId;

    @Id
    @Column(name = "camera_migration_history_id", nullable = false)
    public long getCameraMigrationHistoryId() {
        return cameraMigrationHistoryId;
    }

    public void setCameraMigrationHistoryId(long cameraMigrationHistoryId) {
        this.cameraMigrationHistoryId = cameraMigrationHistoryId;
    }

    @Basic
    @Column(name = "camera_migration_id", nullable = false)
    public long getCameraMigrationId() {
        return cameraMigrationId;
    }

    public void setCameraMigrationId(long cameraMigrationId) {
        this.cameraMigrationId = cameraMigrationId;
    }

    @Basic
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "previous_avhs_server_id", nullable = false)
    public long getPreviousAvhsServerId() {
        return previousAvhsServerId;
    }

    public void setPreviousAvhsServerId(long previousAvhsServerId) {
        this.previousAvhsServerId = previousAvhsServerId;
    }

    @Basic
    @Column(name = "migration_status", nullable = false, length = 60)
    public String getMigrationStatus() {
        return migrationStatus;
    }

    public void setMigrationStatus(String migrationStatus) {
        this.migrationStatus = migrationStatus;
    }

    @Basic
    @Column(name = "migration_details", nullable = true, length = 200)
    public String getMigrationDetails() {
        return migrationDetails;
    }

    public void setMigrationDetails(String migrationDetails) {
        this.migrationDetails = migrationDetails;
    }

    @Basic
    @Column(name = "migration_completion_date", nullable = true)
    public Timestamp getMigrationCompletionDate() {
        return migrationCompletionDate;
    }

    public void setMigrationCompletionDate(Timestamp migrationCompletionDate) {
        this.migrationCompletionDate = migrationCompletionDate;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "ongoing_status", nullable = true, length = 64)
    public String getOngoingStatus() {
        return ongoingStatus;
    }

    public void setOngoingStatus(String ongoingStatus) {
        this.ongoingStatus = ongoingStatus;
    }

    @Basic
    @Column(name = "migration_time", nullable = true)
    public Long getMigrationTime() {
        return migrationTime;
    }

    public void setMigrationTime(Long migrationTime) {
        this.migrationTime = migrationTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraMigrationHistory that = (CameraMigrationHistory) o;

        if (cameraMigrationHistoryId != that.cameraMigrationHistoryId) {
            return false;
        }
        if (cameraMigrationId != that.cameraMigrationId) {
            return false;
        }
        if (cameraId != that.cameraId) {
            return false;
        }
        if (previousAvhsServerId != that.previousAvhsServerId) {
            return false;
        }
        if (migrationStatus != null ? !migrationStatus.equals(that.migrationStatus) : that.migrationStatus != null) {
            return false;
        }
        if (migrationDetails != null ? !migrationDetails.equals(that.migrationDetails) : that.migrationDetails != null) {
            return false;
        }
        if (migrationCompletionDate != null ? !migrationCompletionDate.equals(that.migrationCompletionDate) : that.migrationCompletionDate != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (ongoingStatus != null ? !ongoingStatus.equals(that.ongoingStatus) : that.ongoingStatus != null) {
            return false;
        }
        return migrationTime != null ? migrationTime.equals(that.migrationTime) : that.migrationTime == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraMigrationHistoryId ^ (cameraMigrationHistoryId >>> 32));
        result = 31 * result + (int) (cameraMigrationId ^ (cameraMigrationId >>> 32));
        result = 31 * result + (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (int) (previousAvhsServerId ^ (previousAvhsServerId >>> 32));
        result = 31 * result + (migrationStatus != null ? migrationStatus.hashCode() : 0);
        result = 31 * result + (migrationDetails != null ? migrationDetails.hashCode() : 0);
        result = 31 * result + (migrationCompletionDate != null ? migrationCompletionDate.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (ongoingStatus != null ? ongoingStatus.hashCode() : 0);
        result = 31 * result + (migrationTime != null ? migrationTime.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "camera_migration_id", referencedColumnName = "camera_migration_id", nullable = false, insertable = false, updatable = false)
    public CameraMigration getCameraMigrationByCameraMigrationId() {
        return cameraMigrationByCameraMigrationId;
    }

    public void setCameraMigrationByCameraMigrationId(CameraMigration cameraMigrationByCameraMigrationId) {
        this.cameraMigrationByCameraMigrationId = cameraMigrationByCameraMigrationId;
    }

    @ManyToOne
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id", nullable = false, insertable = false, updatable = false)
    public OvrCamera getOvrCameraByCameraId() {
        return ovrCameraByCameraId;
    }

    public void setOvrCameraByCameraId(OvrCamera ovrCameraByCameraId) {
        this.ovrCameraByCameraId = ovrCameraByCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "previous_avhs_server_id", referencedColumnName = "avhs_server_id", nullable = false, insertable = false, updatable = false)
    public AvhsServer getAvhsServerByPreviousAvhsServerId() {
        return avhsServerByPreviousAvhsServerId;
    }

    public void setAvhsServerByPreviousAvhsServerId(AvhsServer avhsServerByPreviousAvhsServerId) {
        this.avhsServerByPreviousAvhsServerId = avhsServerByPreviousAvhsServerId;
    }
}
