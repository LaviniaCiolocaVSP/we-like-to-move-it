package com.brivo.panel.server.reference.dto;

import java.io.Serializable;

public abstract class AbstractDTO implements Serializable {

    public abstract <T extends Number> T getId();
}
