package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IObjectPropertyRepository;
import com.brivo.panel.server.reference.domain.entities.ObjectProperty;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BrivoObjectPropertyRepository extends IObjectPropertyRepository {

    Optional<List<ObjectProperty>> findByAccountId(@Param("accountId") long accountId);
}
