package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Collection;

public class UIValidCredentialDTO extends UiProgDeviceDTO implements Serializable {
    Collection<UiPhysicalPointDTO> availableReaders;
    UiPhysicalPointDTO selectedReader;

    public UIValidCredentialDTO() {

    }

    public UIValidCredentialDTO(final UiProgDeviceDTO uiProgDeviceDTO) {
        super(uiProgDeviceDTO, uiProgDeviceDTO.getDoorId(), uiProgDeviceDTO.getEventTypeId(), uiProgDeviceDTO.getAvailableOutputs(),
                uiProgDeviceDTO.getDisengageDelay(), uiProgDeviceDTO.getReportEngage(), uiProgDeviceDTO.getReportDisengage(),
                uiProgDeviceDTO.getEngageMessage(), uiProgDeviceDTO.getDisengageMessage(), uiProgDeviceDTO.getBehavior(),
                uiProgDeviceDTO.getOutputs());
    }

    public UIValidCredentialDTO(final Long disengageDelay, final Boolean reportEngage, final Boolean reportDisengage,
                                final String engageMessage, final String disengageMessage) {
        this.disengageDelay = disengageDelay;
        this.reportDisengage = reportDisengage;
        this.reportEngage = reportEngage;
        this.engageMessage = engageMessage;
        this.disengageMessage = disengageMessage;
    }

    public Collection<UiPhysicalPointDTO> getAvailableReaders() {
        return availableReaders;
    }

    public void setAvailableReaders(Collection<UiPhysicalPointDTO> availableReaders) {
        this.availableReaders = availableReaders;
    }

    public UiPhysicalPointDTO getSelectedReader() {
        return selectedReader;
    }

    public void setSelectedReader(UiPhysicalPointDTO selectedReader) {
        this.selectedReader = selectedReader;
    }

    @Override
    public String toString() {
        return "UIValidCredentialDTO{" +
                "availableReaders=" + availableReaders +
                ", selectedReader=" + selectedReader +
                ", doorId=" + doorId +
                ", eventTypeId=" + eventTypeId +
                ", availableOutputs=" + availableOutputs +
                ", disengageDelay=" + disengageDelay +
                ", reportEngage=" + reportEngage +
                ", reportDisengage=" + reportDisengage +
                ", engageMessage='" + engageMessage + '\'' +
                ", disengageMessage='" + disengageMessage + '\'' +
                ", behavior=" + behavior +
                ", outputs=" + outputs +
                ", accountId=" + accountId +
                ", deviceId=" + deviceId +
                ", name='" + name + '\'' +
                ", panelOid=" + panelOid +
                ", twoFactorScheduleId=" + twoFactorScheduleId +
                ", twoFactorInterval=" + twoFactorInterval +
                ", cardRequiredScheduleId=" + cardRequiredScheduleId +
                ", siteName='" + siteName + '\'' +
                ", deviceType='" + deviceType + '\'' +
                ", scheduleId=" + scheduleId +
                '}';
    }
}
