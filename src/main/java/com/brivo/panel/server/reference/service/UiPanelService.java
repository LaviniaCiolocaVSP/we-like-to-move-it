package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.panel.PanelDao;
import com.brivo.panel.server.reference.domain.common.IBrainStateRepository;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.Board;
import com.brivo.panel.server.reference.domain.entities.BoardProperty;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.BrainBuildVersion;
import com.brivo.panel.server.reference.domain.entities.BrainIpAddress;
import com.brivo.panel.server.reference.domain.entities.BrainState;
import com.brivo.panel.server.reference.domain.entities.BrainType;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.ElevatorFloorMap;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.domain.entities.Rs485Settings;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.BrainBuildVersionRepository;
import com.brivo.panel.server.reference.domain.repository.BrainIpAddressRepository;
import com.brivo.panel.server.reference.domain.repository.BrainTypeRepository;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.dto.BoardDTO;
import com.brivo.panel.server.reference.dto.ElevatorFloorMapDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.PanelDTO;
import com.brivo.panel.server.reference.dto.ProgIoPointDTO;
import com.brivo.panel.server.reference.dto.Rs485SettingDTO;
import com.brivo.panel.server.reference.dto.ui.PanelIPAndSerialNumber;
import com.brivo.panel.server.reference.dto.ui.UiPanelDTO;
import com.brivo.panel.server.reference.dto.ui.UiPanelTypeDTO;
import com.brivo.panel.server.reference.model.PanelConnectionStatus;
import com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UiPanelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UiPanelService.class);

    private final SessionRegistryService sessionRegistryService;
    private final UiBoardService boardService;
    private final UiDeviceService deviceService;
    private final UiObjectService objectService;

    private final BrainBuildVersionRepository brainBuildVersionRepository;
    private final BrainIpAddressRepository brainIpAddressRepository;
    private final PanelRepository panelRepository;
    private final AccountRepository accountRepository;
    private final BrainTypeRepository brainTypeRepository;
    private final PanelDao panelDao;

    @Autowired
    public UiPanelService(final PanelRepository panelRepository, final SessionRegistryService sessionRegistryService,
                          final PanelDao panelDao, final UiBoardService boardService, final UiDeviceService deviceService,
                          final UiObjectService objectService, final AccountRepository accountRepository,
                          final BrainTypeRepository brainTypeRepository,
                          final BrainBuildVersionRepository brainBuildVersionRepository,
                          final BrainIpAddressRepository brainIpAddressRepository) {
        this.panelRepository = panelRepository;
        this.sessionRegistryService = sessionRegistryService;
        this.panelDao = panelDao;
        this.boardService = boardService;
        this.deviceService = deviceService;
        this.objectService = objectService;
        this.accountRepository = accountRepository;
        this.brainTypeRepository = brainTypeRepository;
        this.brainBuildVersionRepository = brainBuildVersionRepository;
        this.brainIpAddressRepository = brainIpAddressRepository;
    }

    public Collection<PanelDTO> getPanelDTOS(final Collection<Brain> panels,
                                             final IBrainStateRepository dynamicBrainStateRepository) {
        return panels.stream()
                     .map(convertPanel(dynamicBrainStateRepository))
                     .collect(Collectors.toSet());
    }

    private Function<Brain, PanelDTO> convertPanel(final IBrainStateRepository dynamicBrainStateRepository) {
        return panel -> {
            final Long panelObjectId = panel.getObjectByObjectId().getObjectId();
            final PanelDTO.BrainStateDTO brainStateDTO = getBrainStateForPanel(panelObjectId, dynamicBrainStateRepository);

            final Collection<Board> boards = panel.getBoardsByObjectId();
            final Collection<BoardDTO> boardDTOS = convertBoards(boards);

            final Collection<ProgIoPoints> progIoPoints = panel.getProgIoPointsByObjectId();
            final Collection<ProgIoPointDTO> progIoPointDTOS = convertProgIoPoints(progIoPoints);

            final Collection<Rs485Settings> rs485Settings = panel.getRs485SettingsByBrainId();
            final Collection<Rs485SettingDTO> rs485SettingDTOS = convertRs485Settings(rs485Settings);

            final Collection<ElevatorFloorMap> elevatorFloorMaps = panel.getElevatorFloorMapsByObjectId();
            final Collection<ElevatorFloorMapDTO> elevatorFloorMapDTOS = convertElevatorFloorMaps(elevatorFloorMaps);

            Long brainTypeId = panel.getBrainTypeByBrainTypeId().getBrainTypeId();
            if (brainTypeId.intValue() == 44 || brainTypeId.intValue() == 45) {
                System.out.println("panel " + panel.getName());
                brainTypeId = 41l;
            }

            return new PanelDTO(panel.getBrainId(), panel.getObjectByObjectId().getObjectId(),
                                brainTypeId,
                                panel.getName(), panel.getElectronicSerialNumber(), panel.getFirmwareVersion(),
                                panel.getHardwareControllerVersion(), panel.getPhysicalAddress(), panel.getPassword(),
                                panel.getIsRegistered(), panel.getTimeZone(), brainStateDTO, panel.getNetworkId(),
                                panel.getPanelChecked(), panel.getPanelChanged(), panel.getPersonsChanged(),
                                panel.getSchedulesChanged(), panel.getAntipassbackResetInterval(), panel.getLogPeriod(),
                                boardDTOS, progIoPointDTOS, rs485SettingDTOS, elevatorFloorMapDTOS);
        };
    }

    private PanelDTO.BrainStateDTO getBrainStateForPanel(final Long panelObjectId,
                                                         final IBrainStateRepository dynamicBrainStateRepository) {
        return dynamicBrainStateRepository.findByObjectId(panelObjectId)
                                          .map(convertBrainState())
                                          .orElse(getNotAvailableBrainStateDTO());
    }

    private Function<BrainState, PanelDTO.BrainStateDTO> convertBrainState() {
        return brainState -> new PanelDTO.BrainStateDTO(brainState.getIpAddress(), brainState.getPanelChecked(),
                                                        brainState.getFirmwareVersion());
    }

    private PanelDTO.BrainStateDTO getNotAvailableBrainStateDTO() {
        return new PanelDTO.BrainStateDTO(AbstractAccountCRUDComponent.NOT_AVAILABLE, AbstractAccountCRUDComponent.NOT_AVAILABLE_DATE,
                                          AbstractAccountCRUDComponent.NOT_AVAILABLE);
    }

    private Collection<BoardDTO> convertBoards(final Collection<Board> boards) {
        return boards.stream()
                     .map(convertBoard())
                     .collect(Collectors.toSet());
    }

    private Function<Board, BoardDTO> convertBoard() {
        return board -> {
            final Collection<BoardProperty> boardProperties = board.getBoardPropertiesByObjectId();
            final Collection<BoardDTO.BoardPropertyDTO> boardPropertyDTOS = convertBoardProperties(boardProperties);

            Long boardTypeId = board.getBoardTypeByBoardType().getBoardTypeId();
            if (boardTypeId.intValue() == 17 || boardTypeId.intValue() == 18) {
                System.out.println("board " + board.getBoardLocation());
                boardTypeId = 11l;
            }

            return new BoardDTO(board.getBoardLocation(), boardTypeId,
                                board.getBoardNumber(), board.getObjectId(), boardPropertyDTOS);
        };
    }

    private Collection<BoardDTO.BoardPropertyDTO> convertBoardProperties(final Collection<BoardProperty> boardProperties) {
        return boardProperties.stream()
                              .map(convertBoardProperty())
                              .collect(Collectors.toSet());
    }

    private Function<BoardProperty, BoardDTO.BoardPropertyDTO> convertBoardProperty() {
        return boardProperty -> new BoardDTO.BoardPropertyDTO(boardProperty.getValueTypeId(), boardProperty.getNameKey(),
                                                              boardProperty.getValue());
    }

    private Collection<ProgIoPointDTO> convertProgIoPoints(final Collection<ProgIoPoints> progIoPoints) {
        return progIoPoints.stream()
                           .map(convertProgIoPoint())
                           .collect(Collectors.toSet());
    }

    private Function<ProgIoPoints, ProgIoPointDTO> convertProgIoPoint() {
        return progIoPoints -> new ProgIoPointDTO(progIoPoints.getName(), progIoPoints.getBoardNumber(),
                                                  progIoPoints.getPointAddress(), progIoPoints.getType(), progIoPoints.getEol(), progIoPoints.getState(),
                                                  progIoPoints.getInuse());
    }

    private Collection<Rs485SettingDTO> convertRs485Settings(final Collection<Rs485Settings> rs485Settings) {
        return rs485Settings.stream()
                            .map(convertRs485Setting())
                            .collect(Collectors.toSet());
    }

    private Function<Rs485Settings, Rs485SettingDTO> convertRs485Setting() {
        return rs485Setting -> new Rs485SettingDTO(rs485Setting.getPort(), rs485Setting.getOperationMode(),
                                                   rs485Setting.getBaudRate(), rs485Setting.getErrorDetectionMethod());
    }

    private Collection<ElevatorFloorMapDTO> convertElevatorFloorMaps(final Collection<ElevatorFloorMap> elevatorFloorMaps) {
        return elevatorFloorMaps.stream()
                                .map(convertElevatorFloorMap())
                                .collect(Collectors.toSet());
    }

    private Function<ElevatorFloorMap, ElevatorFloorMapDTO> convertElevatorFloorMap() {
        return elevatorFloorMap -> new ElevatorFloorMapDTO(elevatorFloorMap.getElevatorOid(), elevatorFloorMap.getFloorOid(),
                                                           elevatorFloorMap.getBoardNumber(), elevatorFloorMap.getPointAddress());
    }

    private Function<Brain, UiPanelDTO> convertPanelUI() {
        return brain -> {
            Instant lastPanelHeartbeat = null;

            final Optional<BrainBuildVersion> brainBuildVersion = brainBuildVersionRepository.findByBrainId(brain.getBrainId());
            String buildVersion = "";
            if (brainBuildVersion.isPresent())
                buildVersion = brainBuildVersion.get().getBuildVersion();

            final Optional<BrainIpAddress> brainIpAddress = brainIpAddressRepository.findByBrainId(brain.getBrainId());
            String ipAddress = "";
            if (brainIpAddress.isPresent())
                ipAddress = brainIpAddress.get().getIpAddress();

            final BrainType brainType = brain.getBrainTypeByBrainTypeId();
            return new UiPanelDTO(brain.getBrainId(), brain.getObjectByObjectId().getObjectId(),
                                  brain.getElectronicSerialNumber(), brain.getFirmwareVersion(), buildVersion, ipAddress, lastPanelHeartbeat,
                                  brainType.getBrainTypeId(), brainType.getName(), brain.getName(), brain.getAccountId(),
                                  brain.getLogPeriod(), brain.getAntipassbackResetInterval(), brain.getNetworkId(), brain.getPassword(),
                                  brain.getTimeZone());
        };
    }

    @Transactional(
            readOnly = true
    )
    public Collection<UiPanelDTO> getUiPanelsForAccountId(final Long accountId) {
        return panelRepository.findByAccountId(accountId)
                              .orElse(Collections.emptyList())
                              .stream()
                              .map(convertPanelUI())
                              .sorted(uiPanelDTOComparator)
                              .collect(Collectors.toList());
    }

    Comparator<UiPanelDTO> uiPanelDTOComparator = new Comparator<UiPanelDTO>() {
        @Override
        public int compare(UiPanelDTO o1, UiPanelDTO o2) {
            return o2.getBrainId().compareTo(o1.getBrainId());
        }
    };

    @Transactional
    public UiPanelDTO getPanelByBrainId(final Long brainId) {
        LOGGER.debug("Getting panel by brain id {}", brainId);

        return panelRepository.findById(brainId)
                              .map(convertPanelUI())
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "Panel with brain id " + brainId + " not found in the database"));
    }

    @Transactional
    public UiPanelDTO getPanelByObjectId(final Long objectId) {
        LOGGER.debug("Getting panel by objectId {}", objectId);

        return panelRepository.findByObjectId(objectId)
                              .map(convertPanelUI())
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "Panel with object id " + objectId + " not found in the database"));
    }


    @Transactional
    public UiPanelDTO getPanelBySerialNumber(final String panelSerialNumber) {
        LOGGER.debug("Getting panel by serial number {}", panelSerialNumber);

        return panelRepository.findByElectronicSerialNumber(panelSerialNumber)
                              .map(convertPanelUI())
                              .orElse(new UiPanelDTO());
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO deletePanels(final List<Long> panelIds) {
        LOGGER.debug("Deleting panels with ids {}...", StringUtils.join(panelIds.toArray(), "; "));

        panelIds.forEach(brainId -> {
            final Long panelObjectId = getPanelObjectIdForPanel(brainId);

            deleteDevicesForPanel(brainId);
            deleteDoorsForPanel(brainId);
            deleteBoardsForPanel(panelObjectId);

            panelDao.deletePanelById(brainId, panelObjectId);
            LOGGER.debug("Successfully deleted panel with brain id {} from database", brainId);

            sessionRegistryService.remove(panelObjectId);
            LOGGER.debug("Successfully removed panel with object id {} from RSP's active sessions map", panelObjectId);
        });

        return new MessageDTO("The panels with ids " + StringUtils.join(panelIds.toArray(), "; ")
                              + " were successfully deleted");
    }

    private Long getPanelObjectIdForPanel(final Long brainId) {
        Long panelObjectId = AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;
        final Optional<Brain> currentPanel = panelRepository.findById(brainId);
        if (currentPanel.isPresent()) {
            panelObjectId = currentPanel.get()
                                        .getObjectId();
        }

        return panelObjectId;
    }

    private void deleteBoardsForPanel(final Long panelObjectId) {
        LOGGER.debug("Deleting boards for panel with object id {}", panelObjectId);

        Collection<Board> boardsForPanel = boardService.getBoardsForPanel(panelObjectId);
        final int boardsSize = boardsForPanel.size();

        if (boardsSize == 0) {
            LOGGER.debug("There are no boards to delete for panel with object id {}", panelObjectId);
            return;
        }

        boardService.deleteBoardsCollection(boardsForPanel);
        LOGGER.debug("Successfully deleted {} boards for panel with object id {}", boardsSize, panelObjectId);
    }

    private void deleteDoorsForPanel(final Long brainId) {
        LOGGER.debug("Deleting doors for panel with id {}", brainId);

        Collection<Device> doorsForPanel = deviceService.getDoorsForPanel(brainId);
        final int doorsSize = doorsForPanel.size();

        if (doorsSize == 0) {
            LOGGER.debug("There are no doors to delete for panel with id {}", brainId);
            return;
        }

        deviceService.deleteDoors(doorsForPanel);
        LOGGER.debug("Successfully deleted {} doors for panel with id {}", doorsSize, brainId);
    }

    private void deleteDevicesForPanel(final Long brainId) {
        LOGGER.debug("Deleting devices for panel with id {}", brainId);

        Collection<Device> devicesForPanel = deviceService.getDevicesForPanel(brainId);
        final int devicesSize = devicesForPanel.size();

        if (devicesSize == 0) {
            LOGGER.debug("There are no devices to delete for panel with id {}", brainId);
            return;
        }

        deviceService.deleteDevices(devicesForPanel);
        LOGGER.debug("Successfully deleted {} devices for panel with id {}", devicesSize, brainId);
    }

    @Transactional(
            readOnly = true
    )
    public Collection<UiPanelTypeDTO> getPanelTypes() {
        return StreamSupport.stream(brainTypeRepository.findAll().spliterator(), false)
                            .map(convertPanelTypeUI())
                            .collect(Collectors.toSet());
    }

    private Function<BrainType, UiPanelTypeDTO> convertPanelTypeUI() {
        return brainType -> new UiPanelTypeDTO(brainType.getBrainTypeId(), brainType.getName());
    }


    public Brain findBrainById(final Long brainId) {
        return panelRepository.findById(brainId)
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "An error occured while getting brain with id " + brainId));
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO update(final UiPanelDTO panelDTO) {
        final long brainId = panelDTO.getBrainId();
        LOGGER.debug("Updating panel with id {} for account {}", brainId, panelDTO.getAccountId());

        final Brain originalBrain = findBrainById(brainId);
        final Brain updatedBrain = this.convertPanelUiDTOForUpdate(originalBrain).apply(panelDTO);

        panelRepository.save(updatedBrain);

        LOGGER.debug("Successfully updated panel with id {} for account {}", brainId, panelDTO.getAccountId());

        return new MessageDTO("The panel with id " + brainId + " was successfully updated");
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO create(final UiPanelDTO panelDTO) {
        LOGGER.debug("Adding a new panel for account {}", panelDTO.getAccountId());

        final Brain brain = convertPanelUiDTOForCreate().apply(panelDTO);
        panelRepository.save(brain);

        LOGGER.debug("Successfully added panel to account {}", panelDTO.getAccountId());

        return new MessageDTO("The panel with CP " + panelDTO.getPanelCP() + " was successfully added");
    }

    private Function<UiPanelDTO, Brain> convertBasicPanelUiDTO(final Brain brain) {
        return panelDTO -> {
            final Long accountId = panelDTO.getAccountId();
            final Account account = accountRepository.findById(accountId)
                                                     .orElseThrow(() -> new IllegalArgumentException(
                                                             "An error occured while getting account with id " + accountId));
            brain.setAccountByAccountId(account);

            brain.setName(panelDTO.getName());
            brain.setElectronicSerialNumber(panelDTO.getPanelCP());
            brain.setFirmwareVersion(panelDTO.getFirmwareVersion());
            brain.setPhysicalAddress(panelDTO.getPanelCP());

            brain.setPassword(panelDTO.getPassword());
            brain.setTimeZone(panelDTO.getTimezone());
            brain.setIsRegistered((short) 1);
            brain.setNetworkId(panelDTO.getNetworkId());

            final LocalDateTime now = LocalDateTime.now();
            brain.setPanelChanged(now);
            brain.setSchedulesChanged(now);
            brain.setPersonsChanged(now);
            brain.setPanelChecked(now);

            brain.setLogPeriod(panelDTO.getLogPeriod());
            brain.setAntipassbackResetInterval(panelDTO.getAntipassBackResetInterval());

            brain.setCreated(now);
            brain.setUpdated(now);
            brain.setActivated(now);

            final BrainType brainType = getBrainTypeByBrainTypeId(panelDTO.getPanelTypeId());
            brain.setBrainTypeByBrainTypeId(brainType);

            //FIXME - set fields
            //            if (!shouldNotBrainStateBePersistedForPanel(panel.getBrainState())) {
            //                BrainState brainState = buildBrainStateForBrain(panel, brivoObject);
            //                brivoObject.setBrainStateByObjectId(brainState);
            //            }

            //            Collection<Board> boards = buildBoardsForPanel(panel.getBoards(), brain);
            //            brain.setBoardsByObjectId(boards);
            //
            //            Collection<ProgIoPoints> progIoPoints = buildProgIoPointsForPanel(panel.getProgIoPoints(), brain);
            //            brain.setProgIoPointsByObjectId(progIoPoints);
            //
            //            Collection<Rs485Settings> rs485Settings = buildRs485SettingsForPanel(panel.getRs485Settings(), brain);
            //            brain.setRs485SettingsByBrainId(rs485Settings);

            return brain;
        };
    }

    private Function<UiPanelDTO, Brain> convertPanelUiDTOForCreate() {
        return panelDTO -> {
            final Brain brain = convertBasicPanelUiDTO(new Brain()).apply(panelDTO);

            final Long nextBrainId = getNextBrainId();
            brain.setBrainId(nextBrainId);

            BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.BRAIN);
            final Long nextObjectId = objectService.getNextObjectId();
            brivoObject.setObjectId(nextObjectId);
            brain.setObjectByObjectId(brivoObject);

            return brain;
        };
    }

    private Function<UiPanelDTO, Brain> convertPanelUiDTOForUpdate(final Brain originalBrain) {
        return panelDTO -> {
            final Brain brain = convertBasicPanelUiDTO(originalBrain).apply(panelDTO);

            brain.setBrainId(panelDTO.getBrainId());

            BrivoObject brivoObject = objectService.findByObjectId(panelDTO.getObjectId());
            brain.setObjectId(panelDTO.getObjectId());
            brain.setObjectByObjectId(brivoObject);

            return brain;
        };
    }

    private BrainType getBrainTypeByBrainTypeId(final Long brainTypeId) {
        return brainTypeRepository.findById(brainTypeId)
                                  .orElseThrow(() -> new IllegalArgumentException(
                                          "There is not brain type with id " + brainTypeId + " in the database"));
    }

    private Long getNextBrainId() {
        final Long maxBrainId = panelRepository.getMaxBrainId()
                                               .orElseThrow(() -> new IllegalArgumentException(
                                                       "An error occured while getting max(brain_id)"));

        return maxBrainId + 1;
    }

    public MessageDTO updatePanelIP(final PanelIPAndSerialNumber panelIPAndSerialNumber) {
        LOGGER.debug("Updating panel IP for {}", panelIPAndSerialNumber.toString());

        final String panelSerialNumber = panelIPAndSerialNumber.getPanelSerialNumber();
        final Brain panel = panelRepository.findByElectronicSerialNumber(panelSerialNumber)
                                           .orElseThrow(() -> new IllegalArgumentException(
                                                   "Panel with serial number " + panelSerialNumber + " not found in the database"));

        Optional<BrainIpAddress> brainIpAddress = brainIpAddressRepository.findByBrainId(panel.getBrainId());

        BrainIpAddress updatedBrainIpAddress;
        if (brainIpAddress.isPresent()) {
            LOGGER.debug("Updating existing brain ip address for electronic serial number {}. Current database record: {}",
                         panelSerialNumber, brainIpAddress.get().toString());

            updatedBrainIpAddress = setFieldsForBrainIpAddress(panel, brainIpAddress.get(), panelIPAndSerialNumber);
        } else {
            LOGGER.debug("Brain ip address does not exist for electronic serial number {}, will insert a new one", panelSerialNumber);
            updatedBrainIpAddress = setFieldsForBrainIpAddress(panel, new BrainIpAddress(), panelIPAndSerialNumber);

            Long nextBrainIpAddressId = brainIpAddressRepository.getMaxBrainIpAddress() + 1;
            updatedBrainIpAddress.setBrainIpAddressId(nextBrainIpAddressId);
        }

        LOGGER.debug("Brain_ip_address record to persist to database: {}", updatedBrainIpAddress.toString());
        brainIpAddressRepository.save(updatedBrainIpAddress);

        LOGGER.debug("Successfully updated panel IP for {}", panelIPAndSerialNumber.toString());
        return new MessageDTO("Successfully updated panel IP to " + panelIPAndSerialNumber.getPanelIP()
                              + " for panel with electronic serial number " + panelSerialNumber);
    }

    private BrainIpAddress setFieldsForBrainIpAddress(Brain brain, BrainIpAddress brainIpAddress, final PanelIPAndSerialNumber panelIPAndSerialNumber) {
        brainIpAddress.setBrainId(brain.getBrainId());
        brainIpAddress.setElectronicSerialNumber(panelIPAndSerialNumber.getPanelSerialNumber());
        brainIpAddress.setIpAddress(panelIPAndSerialNumber.getPanelIP());

        return brainIpAddress;
    }

    @Transactional
    public Optional<String> getAValidPanelSerialNumber() {
        return panelRepository.findAValidPanelSerialNumber();
    }
}
