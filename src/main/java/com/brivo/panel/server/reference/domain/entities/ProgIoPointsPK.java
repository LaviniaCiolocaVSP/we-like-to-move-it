package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ProgIoPointsPK implements Serializable {
    private long panelOid;
    private long boardNumber;
    private short pointAddress;

    @Column(name = "panel_oid", nullable = false)
    @Id
    public long getPanelOid() {
        return panelOid;
    }

    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }

    @Column(name = "board_number", nullable = false)
    @Id
    public long getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(long boardNumber) {
        this.boardNumber = boardNumber;
    }

    @Column(name = "point_address", nullable = false)
    @Id
    public short getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(short pointAddress) {
        this.pointAddress = pointAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgIoPointsPK that = (ProgIoPointsPK) o;

        if (panelOid != that.panelOid) {
            return false;
        }
        if (boardNumber != that.boardNumber) {
            return false;
        }
        return pointAddress == that.pointAddress;
    }

    @Override
    public int hashCode() {
        int result = (int) (panelOid ^ (panelOid >>> 32));
        result = 31 * result + (int) (boardNumber ^ (boardNumber >>> 32));
        result = 31 * result + (int) pointAddress;
        return result;
    }
}
