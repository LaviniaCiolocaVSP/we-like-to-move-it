package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class SwitchDevice {
    private Long deviceId;
    private Integer inputBoardAddress;
    private Integer inputPointAddress;
    private Long inputBoardId;
    private Long activeScheduleId;
    private Boolean reportEngage;
    private Boolean reportDisengage;
    private List<ProgrammableDeviceOutput> outputs;
    private Integer inputDelay;
    private Integer maxActivation;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getInputBoardAddress() {
        return inputBoardAddress;
    }

    public void setInputBoardAddress(Integer inputBoardAddress) {
        this.inputBoardAddress = inputBoardAddress;
    }

    public Integer getInputPointAddress() {
        return inputPointAddress;
    }

    public void setInputPointAddress(Integer inputPointAddress) {
        this.inputPointAddress = inputPointAddress;
    }

    public Long getInputBoardId() {
        return inputBoardId;
    }

    public void setInputBoardId(Long inputBoardId) {
        this.inputBoardId = inputBoardId;
    }

    public Long getActiveScheduleId() {
        return activeScheduleId;
    }

    public void setActiveScheduleId(Long activeScheduleId) {
        this.activeScheduleId = activeScheduleId;
    }

    public Boolean getReportEngage() {
        return reportEngage;
    }

    public void setReportEngage(Boolean reportEngage) {
        this.reportEngage = reportEngage;
    }

    public Boolean getReportDisengage() {
        return reportDisengage;
    }

    public void setReportDisengage(Boolean reportDisengage) {
        this.reportDisengage = reportDisengage;
    }

    public List<ProgrammableDeviceOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<ProgrammableDeviceOutput> outputs) {
        this.outputs = outputs;
    }

    public Integer getInputDelay() {
        return inputDelay;
    }

    public void setInputDelay(Integer inputDelay) {
        this.inputDelay = inputDelay;
    }

    public Integer getMaxActivation() {
        return maxActivation;
    }

    public void setMaxActivation(Integer maxActivation) {
        this.maxActivation = maxActivation;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

}
