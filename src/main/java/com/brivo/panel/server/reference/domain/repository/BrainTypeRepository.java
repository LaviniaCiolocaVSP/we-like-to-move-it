package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.BrainType;
import org.springframework.data.repository.CrudRepository;

public interface BrainTypeRepository extends CrudRepository<BrainType, Long> {
}
