package com.brivo.panel.server.reference.dto.ui;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class PanelIPAndSerialNumber {
    private String panelIP;
    private String panelSerialNumber;

    @JsonCreator
    public PanelIPAndSerialNumber(@JsonProperty("panelIP") String panelIP,
                                  @JsonProperty("panelSerialNumber") String panelSerialNumber) {
        this.panelIP = panelIP;
        this.panelSerialNumber = panelSerialNumber;
    }

    public String getPanelIP() {
        return panelIP;
    }

    public void setPanelIP(String panelIP) {
        this.panelIP = panelIP;
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    @Override
    public String toString() {
        return "PanelIPAndSerialNumber{" +
                "panelIP='" + panelIP + '\'' +
                ", panelSerialNumber='" + panelSerialNumber + '\'' +
                '}';
    }
}
