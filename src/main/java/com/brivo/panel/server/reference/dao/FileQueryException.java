package com.brivo.panel.server.reference.dao;

public class FileQueryException extends BrivoDaoException {

    /**
     * Exception which is thrown when there is an Error loading a FileQuery sql file.
     */
    private static final long serialVersionUID = 6708545789295336851L;

    public FileQueryException() {
        super();
    }

    public FileQueryException(String exceptionInformation) {
        super(exceptionInformation);
    }

    public FileQueryException(String msg, Exception e) {
        super(msg);
        nested = e;
        super.initCause(e);
    }

}
