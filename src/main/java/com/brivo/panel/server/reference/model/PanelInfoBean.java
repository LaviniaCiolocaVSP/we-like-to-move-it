package com.brivo.panel.server.reference.model;

public class PanelInfoBean {
    private String panelSerialNo;
    private Long brainId;
    
    
    public PanelInfoBean(String panelSerialNo, Long brainId) {
        this.panelSerialNo = panelSerialNo;
        this.brainId = brainId;
    }
    
    public String getPanelSerialNo() {
        return panelSerialNo;
    }
    
    public void setPanelSerialNo(String panelSerialNo) {
        this.panelSerialNo = panelSerialNo;
    }
    
    public Long getBrainId() {
        return brainId;
    }
    
    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }
}
