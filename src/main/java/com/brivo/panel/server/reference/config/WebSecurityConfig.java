package com.brivo.panel.server.reference.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.web.filter.CommonsRequestLoggingFilter;

@Configuration
@AutoConfigureAfter(DataSourceConfig.class)
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private static final Logger LOGGER = LoggerFactory.getLogger(WebSecurityConfig.class);

    @Autowired
    public WebSecurityConfig() {
        super(true);
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        LOGGER.trace(">>> In method configure");
        http.authorizeRequests()
            .antMatchers("/websocket")
            .permitAll();

        /*
        //csrf is disabled because we don't want to have to pass additional headers needed to enable it
        http.addFilter(panelRequestHeaderAuthenticationFilter())
                .csrf().disable()
                .cors().disable()
                .authorizeRequests()
                .anyRequest().authenticated();

        */

        http.sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        LOGGER.trace("<<< Out of method configure");
    }

    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(false);
        loggingFilter.setIncludeQueryString(false);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setIncludeHeaders(true);
        loggingFilter.setMaxPayloadLength(5000);

        return loggingFilter;
    }
}
