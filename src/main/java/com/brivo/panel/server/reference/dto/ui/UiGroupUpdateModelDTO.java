package com.brivo.panel.server.reference.dto.ui;

import java.util.Collection;

public class UiGroupUpdateModelDTO {
    private UiGroupDTO group;
    private Collection<UiGroupPermissionDTO> groupPermissions;

    public UiGroupUpdateModelDTO(UiGroupDTO group, Collection<UiGroupPermissionDTO> groupPermissions) {
        this.group = group;
        this.groupPermissions = groupPermissions;
    }

    public UiGroupUpdateModelDTO() {
    }

    public UiGroupDTO getGroup() {
        return group;
    }

    public void setGroup(UiGroupDTO group) {
        this.group = group;
    }

    public Collection<UiGroupPermissionDTO> getGroupPermissions() {
        return groupPermissions;
    }

    public void setGroupPermissions(Collection<UiGroupPermissionDTO> groupPermissions) {
        this.groupPermissions = groupPermissions;
    }

    @Override
    public String toString() {
        return "UiGroupUpdateModelDTO{" +
                "group=" + group +
                ", groupPermissions=" + groupPermissions +
                '}';
    }
}
