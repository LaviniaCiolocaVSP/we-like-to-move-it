package com.brivo.panel.server.reference.config.async;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.interceptor.SimpleAsyncUncaughtExceptionHandler;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Arrays;

@Component
public class TaskExecutorExceptionHandler extends SimpleAsyncUncaughtExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(TaskExecutorExceptionHandler.class);

    @Override
    public void handleUncaughtException(final Throwable throwable, final Method method, final Object... objects) {
        LOGGER.error("{} for '{}', with the params '{}'", throwable.getClass().getSimpleName(), method.getName(),
                Arrays.asList(objects));

        final String message = throwable.getMessage();
        if (throwable instanceof RuntimeException && !(throwable instanceof IllegalStateException)) {
            LOGGER.warn(message, throwable);
        } else {
            LOGGER.error(message, throwable);
        }

        if (throwable instanceof NullPointerException) {
            LOGGER.error(message, throwable);
        }
    }
}
