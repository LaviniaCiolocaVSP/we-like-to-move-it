package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.time.Instant;

public class SiteOccupant {
    private Long userId;
    private Long deviceId;
    private Long entryDoorId;
    private Long exitDoorId;
    private Long siteId;
    private Boolean isIngress;
    private Boolean isEgress;
    private Instant entered;
    private Instant exited;
    private Boolean isPresent;

    public SiteOccupant() {

    }

    @JsonCreator
    public SiteOccupant(@JsonProperty("userId") final Long userId,
                        @JsonProperty("deviceId") final Long deviceId,
                        @JsonProperty("entryDoorId") final Long entryDoorId,
                        @JsonProperty("exitDoorId") final Long exitDoorId,
                        @JsonProperty("siteId") final Long siteId,
                        @JsonProperty("isIngress") final Boolean isIngress,
                        @JsonProperty("isEgress") final Boolean isEgress,
                        @JsonProperty("entered") final Instant entered,
                        @JsonProperty("exited") final Instant exited,
                        @JsonProperty("isPresent") final Boolean isPresent) {
        this.userId = userId;
        this.deviceId = deviceId;
        this.entryDoorId = entryDoorId;
        this.exitDoorId = exitDoorId;
        this.siteId = siteId;
        this.isIngress = isIngress;
        this.isEgress = isEgress;
        this.entered = entered;
        this.exited = exited;
        this.isPresent = isPresent;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    public Boolean getIsIngress() {
        return isIngress;
    }

    public void setIsIngress(Boolean isIngress) {
        this.isIngress = isIngress;
    }

    public Boolean getIsEgress() {
        return isEgress;
    }

    public void setIsEgress(Boolean isEgress) {
        this.isEgress = isEgress;
    }

    public Instant getEntered() {
        return entered;
    }

    public void setEntered(Instant entered) {
        this.entered = entered;
    }

    public Instant getExited() {
        return exited;
    }

    public void setExited(Instant exited) {
        this.exited = exited;
    }

    public Boolean getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(Boolean isPresent) {
        this.isPresent = isPresent;
    }

    public Long getEntryDoorId() {
        return entryDoorId;
    }

    public void setEntryDoorId(Long entryDoorId) {
        this.entryDoorId = entryDoorId;
    }

    public Long getExitDoorId() {
        return exitDoorId;
    }

    public void setExitDoorId(Long exitDoorId) {
        this.exitDoorId = exitDoorId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
