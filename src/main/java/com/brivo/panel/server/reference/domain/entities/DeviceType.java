package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "device_type", schema = "brivo20", catalog = "onair")
public class DeviceType {
    private long deviceTypeId;
    private String name;
    private String description;
    private Collection<Device> devicesByDeviceTypeId;

    @Id
    @Column(name = "device_type_id", nullable = false)
    public long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceType that = (DeviceType) o;

        if (deviceTypeId != that.deviceTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceTypeId ^ (deviceTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "deviceTypeByDeviceTypeId", cascade = CascadeType.ALL)
    public Collection<Device> getDevicesByDeviceTypeId() {
        return devicesByDeviceTypeId;
    }

    public void setDevicesByDeviceTypeId(Collection<Device> devicesByDeviceTypeId) {
        this.devicesByDeviceTypeId = devicesByDeviceTypeId;
    }
}
