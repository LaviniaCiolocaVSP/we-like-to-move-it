package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;

public class ScheduleDTO extends AbstractDTO {

    private final Long scheduleId;
    private final String scheduleName;
    private final String description;
    private final Long scheduleTypeId;
    private final Long siteId;
    private final Long enablingGroupId;
    private final Long enablingGracePeriod;
    private final Long cadmScheduleId;
    private final Collection<ScheduledBlock> scheduledBlocks;
    private final Collection<ScheduledExceptionBlock> scheduleExceptionBlocks;
    private final Collection<Long> holidayIds;

    @JsonCreator
    public ScheduleDTO(@JsonProperty("id") final Long scheduleId, @JsonProperty("scheduleName") final String scheduleName,
                       @JsonProperty("cadmScheduleId") final Long cadmScheduleId, @JsonProperty("description") final String description,
                       @JsonProperty("scheduleTypeId") final Long scheduleTypeId,
                       @JsonProperty("siteId") final Long siteId, @JsonProperty("enablingGroupId") final Long enablingGroupId,
                       @JsonProperty("enablingGracePeriod") final Long enablingGracePeriod,
                       @JsonProperty("scheduledBlocks") final Collection<ScheduledBlock> scheduledBlocks,
                       @JsonProperty("scheduleExceptionBlocks") final Collection<ScheduledExceptionBlock> scheduleExceptionBlocks,
                       @JsonProperty("holidayIds") final Collection<Long> holidayIds) {
        this.scheduleId = scheduleId;
        this.scheduleName = scheduleName;
        this.description = description;
        this.scheduleTypeId = scheduleTypeId;
        this.siteId = siteId;
        this.enablingGroupId = enablingGroupId;
        this.enablingGracePeriod = enablingGracePeriod;
        this.scheduledBlocks = scheduledBlocks;
        this.scheduleExceptionBlocks = scheduleExceptionBlocks;
        this.holidayIds = holidayIds;
        this.cadmScheduleId = cadmScheduleId;
    }

    @Override
    public Long getId() {
        return scheduleId;
    }

    public Long getCadmScheduleId() {
        return cadmScheduleId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public Long getSiteId() {
        return siteId;
    }

    public Collection<ScheduledBlock> getScheduledBlocks() {
        return scheduledBlocks;
    }

    public String getDescription() {
        return description;
    }

    public Long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public Long getEnablingGroupId() {
        return enablingGroupId;
    }

    public Long getEnablingGracePeriod() {
        return enablingGracePeriod;
    }

    public Collection<ScheduledExceptionBlock> getScheduleExceptionBlocks() {
        return scheduleExceptionBlocks;
    }

    public Collection<Long> getHolidayIds() {
        return holidayIds;
    }

    public static class ScheduledBlock {
        private final Long startMinute;

        private final Long stopMinute;

        @JsonCreator
        public ScheduledBlock(@JsonProperty("startMinute") final Long startMinute,
                              @JsonProperty("stopMinute") final Long stopMinute) {
            this.startMinute = startMinute;
            this.stopMinute = stopMinute;
        }

        public Long getStartMinute() {
            return startMinute;
        }

        public Long getStopMinute() {
            return stopMinute;
        }
    }

    public static class ScheduledExceptionBlock {
        private final Long startMinute;
        private final Long stopMinute;
        private final Long exceptionId;
        private final Long repeatOrdinal;
        private final Long repeatIndex;
        private final Long repeatType;
        private final Long isEnableBlock;

        @JsonCreator
        public ScheduledExceptionBlock(@JsonProperty("exceptionId") final Long exceptionId,
                                       @JsonProperty("startMinute") final Long startMinute,
                                       @JsonProperty("stopMinute") final Long stopMinute,
                                       @JsonProperty("repeatOrdinal") final Long repeatOrdinal,
                                       @JsonProperty("repeatIndex") final Long repeatIndex,
                                       @JsonProperty("repeatType") final Long repeatType,
                                       @JsonProperty("isEnableBlock") final Long isEnableBlock) {
            this.startMinute = startMinute;
            this.stopMinute = stopMinute;
            this.exceptionId = exceptionId;
            this.repeatIndex = repeatIndex;
            this.repeatOrdinal = repeatOrdinal;
            this.repeatType = repeatType;
            this.isEnableBlock = isEnableBlock;
        }

        public Long getExceptionId() {
            return exceptionId;
        }

        public Long getRepeatOrdinal() {
            return repeatOrdinal;
        }

        public Long getRepeatIndex() {
            return repeatIndex;
        }

        public Long getRepeatType() {
            return repeatType;
        }

        public Long getIsEnableBlock() {
            return isEnableBlock;
        }

        public Long getStartMinute() {
            return startMinute;
        }

        public Long getStopMinute() {
            return stopMinute;
        }
    }
}
