package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class AddressDTO extends AbstractDTO {
    private final long addressId;
    private final String address1;
    private final String address2;
    private final String city;
    private final String state;
    private final String province;
    private final String zipCode;
    private final String postalCode;
    private final String timeZone;

    @JsonCreator
    public AddressDTO(@JsonProperty("id") final long addressId, @JsonProperty("address1") final String address1,
                      @JsonProperty("address2") final String address2, @JsonProperty("city") final String city,
                      @JsonProperty("state") final String state, @JsonProperty("province") final String province,
                      @JsonProperty("zipCode") final String zipCode, @JsonProperty("postalCode") final String postalCode,
                      @JsonProperty("timeZone") final String timeZone) {
        this.addressId = addressId;
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.state = state;
        this.province = province;
        this.zipCode = zipCode;
        this.postalCode = postalCode;
        this.timeZone = timeZone;
    }

    @Override
    public Long getId() {
        return addressId;
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    public String getProvince() {
        return province;
    }

    public String getZipCode() {
        return zipCode;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getTimeZone() {
        return timeZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AddressDTO that = (AddressDTO) o;
        return addressId == that.addressId &&
                Objects.equals(address1, that.address1) &&
                Objects.equals(address2, that.address2) &&
                Objects.equals(city, that.city) &&
                Objects.equals(state, that.state) &&
                Objects.equals(province, that.province) &&
                Objects.equals(zipCode, that.zipCode) &&
                Objects.equals(postalCode, that.postalCode) &&
                Objects.equals(timeZone, that.timeZone);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressId, address1, address2, city, state, province, zipCode, postalCode, timeZone);
    }
}
