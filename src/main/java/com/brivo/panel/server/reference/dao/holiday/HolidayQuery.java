package com.brivo.panel.server.reference.dao.holiday;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum HolidayQuery implements FileQuery {
    DELETE_HOLIDAY, DELETE_SCHEDULE_HOLIDAY_MAP;

    private String query;

    HolidayQuery() {
        this.query = QueryUtils.getQueryText(HolidayQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}
