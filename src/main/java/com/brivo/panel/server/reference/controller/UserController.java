package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UIUserSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiUserDTO;
import com.brivo.panel.server.reference.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/user")
public class UserController {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    private final UserService userService;
    
    @Autowired
    public UserController(final UserService userService) {
        this.userService = userService;
    }
    
    @GetMapping("/{accountId}")
    public Collection<UiUserDTO> get(@PathVariable final long accountId) {
        return userService.getUiUsersForAccountId(accountId);
    }
    
    @GetMapping("/summary/{accountId}")
    public Collection<UIUserSummaryDTO> getSummary(@PathVariable final long accountId) {
        return userService.getUserSummaryForAccount(accountId);
    }
    
    @GetMapping("/group/{groupId}")
    public Collection<UiUserDTO> getUsersForGroup(@PathVariable final long groupId) {
        return userService.getUiUsersForGroupId(groupId);
    }
    
    @PostMapping(
            path = "/add",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO create(@RequestBody final UiUserDTO uiUserDTO) {
        return userService.create(uiUserDTO);
    }
    
    @PostMapping(
            path = "/delete",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO delete(@RequestBody List<Long> objects_ids) {
        return userService.delete(objects_ids);
    }
    
    @PostMapping(
            path = "/update",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public MessageDTO update(@RequestBody UiUserDTO user) {
        return userService.update(user);
    }
}
