package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "brain_lock", schema = "brivo20", catalog = "onair")
public class BrainLock {
    private long objectId;
    private BrivoObject objectByBrivoObjectId;

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrainLock brainLock = (BrainLock) o;

        return objectId == brainLock.objectId;
    }

    @Override
    public int hashCode() {
        return (int) (objectId ^ (objectId >>> 32));
    }

    @OneToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }
}
