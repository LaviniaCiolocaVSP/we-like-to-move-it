package com.brivo.panel.server.reference.dto.ui;

import com.brivo.panel.server.reference.constants.DoorBoardIOPointMap;
import com.brivo.panel.server.reference.dto.FirmwareVersionDTO;
import com.brivo.panel.server.reference.dto.IOPointTemplateDTO;
import com.google.common.base.MoreObjects;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UiDoorBoardDTO {
    
    public static final FirmwareVersionDTO FAILSAFE_FIRMWARE_VERSION = new FirmwareVersionDTO("5.1.2");
    
    private IOPointTemplateDTO door1Rex;
    private IOPointTemplateDTO door1DoorContact;
    private IOPointTemplateDTO door1DoorLock;
    private IOPointTemplateDTO door1AuxRelay1;
    private IOPointTemplateDTO door1AuxRelay2;
    private IOPointTemplateDTO door1AuxInput1;
    private IOPointTemplateDTO door1AuxInput2;
    private IOPointTemplateDTO door1Reader;
    
    private IOPointTemplateDTO door2Rex;
    private IOPointTemplateDTO door2DoorContact;
    private IOPointTemplateDTO door2DoorLock;
    private IOPointTemplateDTO door2AuxRelay1;
    private IOPointTemplateDTO door2AuxRelay2;
    private IOPointTemplateDTO door2AuxInput1;
    private IOPointTemplateDTO door2AuxInput2;
    private IOPointTemplateDTO door2Reader;
    
    private boolean failSafeStateAllowed;
    
    public UiDoorBoardDTO() {
    }
    
    public UiDoorBoardDTO(IOPointTemplateDTO door1Rex,
                          IOPointTemplateDTO door1DoorContact,
                          IOPointTemplateDTO door1DoorLock,
                          IOPointTemplateDTO door1AuxRelay1,
                          IOPointTemplateDTO door1AuxRelay2,
                          IOPointTemplateDTO door1AuxInput1,
                          IOPointTemplateDTO door1AuxInput2,
                          IOPointTemplateDTO door1Reader,
                          IOPointTemplateDTO door2Rex,
                          IOPointTemplateDTO door2DoorContact,
                          IOPointTemplateDTO door2DoorLock,
                          IOPointTemplateDTO door2AuxRelay1,
                          IOPointTemplateDTO door2AuxRelay2,
                          IOPointTemplateDTO door2AuxInput1,
                          IOPointTemplateDTO door2AuxInput2,
                          IOPointTemplateDTO door2Reader, boolean failSafeStateAllowed) {
        
        this.door1Rex = door1Rex;
        this.door1DoorContact = door1DoorContact;
        this.door1DoorLock = door1DoorLock;
        this.door1AuxRelay1 = door1AuxRelay1;
        this.door1AuxRelay2 = door1AuxRelay2;
        this.door1AuxInput1 = door1AuxInput1;
        this.door1AuxInput2 = door1AuxInput2;
        this.door1Reader = door1Reader;
        
        this.door2Rex = door2Rex;
        this.door2DoorContact = door2DoorContact;
        this.door2DoorLock = door2DoorLock;
        this.door2AuxRelay1 = door2AuxRelay1;
        this.door2AuxRelay2 = door2AuxRelay2;
        this.door2AuxInput1 = door2AuxInput1;
        this.door2AuxInput2 = door2AuxInput2;
        this.door2Reader = door2Reader;
        
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
    
    public IOPointTemplateDTO getDoor1Reader() {
        return door1Reader;
    }
    
    public void setDoor1Reader(IOPointTemplateDTO door1Reader) {
        this.door1Reader = door1Reader;
    }
    
    public IOPointTemplateDTO getDoor2Reader() {
        return door2Reader;
    }
    
    public void setDoor2Reader(IOPointTemplateDTO door2Reader) {
        this.door2Reader = door2Reader;
    }
    
    public IOPointTemplateDTO getDoor1Rex() {
        return door1Rex;
    }
    
    public void setDoor1Rex(IOPointTemplateDTO door1Rex) {
        this.door1Rex = door1Rex;
    }
    
    public IOPointTemplateDTO getDoor1DoorContact() {
        return door1DoorContact;
    }
    
    public void setDoor1DoorContact(IOPointTemplateDTO door1DoorContact) {
        this.door1DoorContact = door1DoorContact;
    }
    
    public IOPointTemplateDTO getDoor1DoorLock() {
        return door1DoorLock;
    }
    
    public void setDoor1DoorLock(IOPointTemplateDTO door1DoorLock) {
        this.door1DoorLock = door1DoorLock;
    }
    
    public IOPointTemplateDTO getDoor1AuxRelay1() {
        return door1AuxRelay1;
    }
    
    public void setDoor1AuxRelay1(IOPointTemplateDTO door1AuxRelay1) {
        this.door1AuxRelay1 = door1AuxRelay1;
    }
    
    public IOPointTemplateDTO getDoor1AuxRelay2() {
        return door1AuxRelay2;
    }
    
    public void setDoor1AuxRelay2(IOPointTemplateDTO door1AuxRelay2) {
        this.door1AuxRelay2 = door1AuxRelay2;
    }
    
    public IOPointTemplateDTO getDoor1AuxInput1() {
        return door1AuxInput1;
    }
    
    public void setDoor1AuxInput1(IOPointTemplateDTO door1AuxInput1) {
        this.door1AuxInput1 = door1AuxInput1;
    }
    
    public IOPointTemplateDTO getDoor1AuxInput2() {
        return door1AuxInput2;
    }
    
    public void setDoor1AuxInput2(IOPointTemplateDTO door1AuxInput2) {
        this.door1AuxInput2 = door1AuxInput2;
    }
    
    public IOPointTemplateDTO getDoor2Rex() {
        return door2Rex;
    }
    
    public void setDoor2Rex(IOPointTemplateDTO door2Rex) {
        this.door2Rex = door2Rex;
    }
    
    public IOPointTemplateDTO getDoor2DoorContact() {
        return door2DoorContact;
    }
    
    public void setDoor2DoorContact(IOPointTemplateDTO door2DoorContact) {
        this.door2DoorContact = door2DoorContact;
    }
    
    public IOPointTemplateDTO getDoor2DoorLock() {
        return door2DoorLock;
    }
    
    public void setDoor2DoorLock(IOPointTemplateDTO door2DoorLock) {
        this.door2DoorLock = door2DoorLock;
    }
    
    public IOPointTemplateDTO getDoor2AuxRelay1() {
        return door2AuxRelay1;
    }
    
    public void setDoor2AuxRelay1(IOPointTemplateDTO door2AuxRelay1) {
        this.door2AuxRelay1 = door2AuxRelay1;
    }
    
    public IOPointTemplateDTO getDoor2AuxRelay2() {
        return door2AuxRelay2;
    }
    
    public void setDoor2AuxRelay2(IOPointTemplateDTO door2AuxRelay2) {
        this.door2AuxRelay2 = door2AuxRelay2;
    }
    
    public IOPointTemplateDTO getDoor2AuxInput1() {
        return door2AuxInput1;
    }
    
    public void setDoor2AuxInput1(IOPointTemplateDTO door2AuxInput1) {
        this.door2AuxInput1 = door2AuxInput1;
    }
    
    public IOPointTemplateDTO getDoor2AuxInput2() {
        return door2AuxInput2;
    }
    
    public void setDoor2AuxInput2(IOPointTemplateDTO door2AuxInput2) {
        this.door2AuxInput2 = door2AuxInput2;
    }
    
    public boolean isFailSafeStateAllowed() {
        return failSafeStateAllowed;
    }
    
    public void setFailSafeStateAllowed(boolean failSafeStateAllowed) {
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
    
    public void setIOPoints(Collection<IOPointTemplateDTO> ioPoints, boolean failSafeStateAllowed) {
        if (ioPoints == null) return;
        
        Map<Integer, IOPointTemplateDTO> ioPointMap = new HashMap<Integer, IOPointTemplateDTO>();
        
        for (Iterator<IOPointTemplateDTO> i = ioPoints.iterator(); i.hasNext(); ) {
            IOPointTemplateDTO ioPoint = i.next();
            
            Integer pointAddress = ioPoint.getPointAddress();
            
            ioPointMap.put(pointAddress, ioPoint);
        }
        
        IOPointTemplateDTO door1Rex = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_REX.getState());
        if (door1Rex != null) {
            this.door1Rex = door1Rex;
            this.door1Rex.setDefaultLabel(door1Rex.getSilkscreen());
        }
        
        IOPointTemplateDTO door1DoorContact = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_DOOR_CONTACT.getState());
        if (door1DoorContact != null) {
            this.door1DoorContact = door1DoorContact;
            this.door1DoorContact.setDefaultLabel(door1DoorContact.getSilkscreen());
        }
        
        IOPointTemplateDTO door1DoorLockRelay = MoreObjects.firstNonNull(ioPointMap.get(DoorBoardIOPointMap.DOOR_1_DOOR_LOCK_RELAY.getState()),
                                                                         ioPointMap.get(DoorBoardIOPointMap.DOOR_1_DOOR_LOCK_RELAY_ALT.getState()));
        if (door1DoorLockRelay != null) {
            this.door1DoorLock = door1DoorLockRelay;
            this.door1DoorLock.setDefaultLabel(door1DoorLockRelay.getSilkscreen());
        }
        
        IOPointTemplateDTO door1AuxRelay1 = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_AUX_RELAY_1.getState());
        if (door1AuxRelay1 != null) {
            this.door1AuxRelay1 = door1AuxRelay1;
            this.door1AuxRelay1.setDefaultLabel(door1AuxRelay1.getSilkscreen());
        }
        
        IOPointTemplateDTO door1AuxInput1 = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_AUX_INPUT_1.getState());
        if (door1AuxInput1 != null) {
            this.door1AuxInput1 = door1AuxInput1;
            this.door1AuxInput1.setDefaultLabel(door1AuxInput1.getSilkscreen());
        }
        
        IOPointTemplateDTO door1AuxInput2 = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_AUX_INPUT_2.getState());
        if (door1AuxInput2 != null) {
            this.door1AuxInput2 = door1AuxInput2;
            this.door1AuxInput2.setDefaultLabel(door1AuxInput2.getSilkscreen());
        }
        
        IOPointTemplateDTO door1AuxRelay2 = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_AUX_RELAY_2.getState());
        if (door1AuxRelay2 != null) {
            this.door1AuxRelay2 = door1AuxRelay2;
            this.door1AuxRelay2.setDefaultLabel(door1AuxRelay2.getSilkscreen());
        }
        
        IOPointTemplateDTO door1Reader = ioPointMap.get(DoorBoardIOPointMap.DOOR_1_READER.getState());
        if (door1Reader != null) {
            this.door1Reader = door1Reader;
            this.door1Reader.setDefaultLabel(door1Reader.getSilkscreen());
        }
        
        
        IOPointTemplateDTO door2Rex = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_REX.getState());
        if (door2Rex != null) {
            this.door2Rex = door2Rex;
            this.door2Rex.setDefaultLabel(door2Rex.getSilkscreen());
        }
        
        IOPointTemplateDTO door2DoorContact = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_DOOR_CONTACT.getState());
        if (door2DoorContact != null) {
            this.door2DoorContact = door2DoorContact;
            this.door2DoorContact.setDefaultLabel(door2DoorContact.getSilkscreen());
        }
        
        IOPointTemplateDTO door2DoorLockRelay = MoreObjects.firstNonNull(ioPointMap.get(DoorBoardIOPointMap.DOOR_2_DOOR_LOCK_RELAY.getState()),
                                                                         ioPointMap.get(DoorBoardIOPointMap.DOOR_2_DOOR_LOCK_RELAY_ALT.getState()));
        if (door2DoorLockRelay != null) {
            this.door2DoorLock = door2DoorLockRelay;
            this.door2DoorLock.setDefaultLabel(door2DoorLockRelay.getSilkscreen());
        }
        
        IOPointTemplateDTO door2AuxRelay1 = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_AUX_RELAY_1.getState());
        if (door2AuxRelay1 != null) {
            this.door2AuxRelay1 = door2AuxRelay1;
            this.door2AuxRelay1.setDefaultLabel(door2AuxRelay1.getSilkscreen());
        }
        
        IOPointTemplateDTO door2AuxInput1 = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_AUX_INPUT_1.getState());
        if (door2AuxInput1 != null) {
            this.door2AuxInput1 = door2AuxInput1;
            this.door2AuxInput1.setDefaultLabel(door2AuxInput1.getSilkscreen());
        }
        
        IOPointTemplateDTO door2AuxInput2 = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_AUX_INPUT_2.getState());
        if (door2AuxInput2 != null) {
            this.door2AuxInput2 = door2AuxInput2;
            this.door2AuxInput2.setDefaultLabel(door2AuxInput2.getSilkscreen());
        }
        
        IOPointTemplateDTO door2AuxRelay2 = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_AUX_RELAY_2.getState());
        if (door2AuxRelay2 != null) {
            this.door2AuxRelay2 = door2AuxRelay2;
            this.door2AuxRelay2.setDefaultLabel(door2AuxRelay2.getSilkscreen());
        }
        
        IOPointTemplateDTO door2Reader = ioPointMap.get(DoorBoardIOPointMap.DOOR_2_READER.getState());
        if (door2Reader != null) {
            this.door2Reader = door2Reader;
            this.door2Reader.setDefaultLabel(door2Reader.getSilkscreen());
        }
        
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
}
