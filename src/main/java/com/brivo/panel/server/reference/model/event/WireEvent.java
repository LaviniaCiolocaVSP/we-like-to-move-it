package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class WireEvent extends Event {
    private Long boardId;
    private Integer pointAddress;
    private Long deviceId;

    @JsonCreator
    public WireEvent(@JsonProperty("eventType") final EventType eventType,
                     @JsonProperty("eventTime") final Instant eventTime,
                     @JsonProperty("boardId") final Long boardId,
                     @JsonProperty("pointAddress") final Integer pointAddress,
                     @JsonProperty("deviceId") final Long deviceId) {
        super(eventType, eventTime);
        this.boardId = boardId;
        this.pointAddress = pointAddress;
        this.deviceId = deviceId;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public Integer getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Integer pointAddress) {
        this.pointAddress = pointAddress;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }
}
