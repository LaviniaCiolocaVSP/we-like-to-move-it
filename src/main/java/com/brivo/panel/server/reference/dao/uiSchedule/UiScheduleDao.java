package com.brivo.panel.server.reference.dao.uiSchedule;

import com.brivo.panel.server.reference.dao.BrivoDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class UiScheduleDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UiScheduleDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteScheduleById(Long scheduleId) {
        LOGGER.info("Deleting schedule with id {}", scheduleId);

        String query = UiScheduleQuery.DELETE_SCHEDULE.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", scheduleId);

        jdbcTemplate.update(query, paramMap);
    }

    public void deleteScheduleDataAndHolidaysByScheduleId(Long scheduleId) {
        LOGGER.info("Deleting schedule data with schedule id {}", scheduleId);

        String query = UiScheduleQuery.DELETE_SCHEDULE_DATA_HOLIDAYS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", scheduleId);

        jdbcTemplate.update(query, paramMap);
    }
}
