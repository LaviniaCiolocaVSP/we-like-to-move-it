package com.brivo.panel.server.reference.domain.common;


import com.brivo.panel.server.reference.domain.entities.DoorDataAntipassback;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IDoorDataAntipassbackRepository extends CrudRepository<DoorDataAntipassback, Long> {

    @Query(
            value = "SELECT dda.* " +
                    "FROM brivo20.door_data_antipassback dda " +
                    "LEFT JOIN brivo20.device d ON dda.device_oid = d.object_id " +
                    "WHERE d.object_id = :deviceObjectId",
            nativeQuery = true
    )
    Optional<DoorDataAntipassback> findByDeviceObjectId(@Param("deviceObjectId") long deviceObjectId);
}
