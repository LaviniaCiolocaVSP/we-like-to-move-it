package com.brivo.panel.server.reference.dao.site;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SiteDao {
    Logger logger = LoggerFactory.getLogger(SiteDao.class);

    @Autowired
    NamedParameterJdbcTemplate template;

    /**
     * Site IDs for board.
     *
     * @param boardObjectId Board object Id.
     * @return List of Security Group IDs.
     */
    public List<Long> getSiteIdsForBoard(Long boardObjectId) {
        logger.trace(">>> GetSiteIdsForBoard");

        String query = SiteQuery.SELECT_SITE_FOR_BOARD.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("boardOID", boardObjectId);

        logger.trace("Query ["
                + SiteQuery.SELECT_SITE_FOR_BOARD.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<Long> list = template.queryForList(query, paramMap, Long.class);

        logger.trace("<<< GetSiteIdsForBoard");

        return list;
    }

    /**
     * Site IDs for device.
     *
     * @param deviceObjectId Device object Id.
     * @return List of Security Group IDs.
     */
    public List<Long> getSiteIdsForDevice(Long deviceObjectId) {
        logger.trace(">>> GetSiteIdsForDevice");

        String query = SiteQuery.SELECT_SITE_FOR_DEVICE.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("memberOID", deviceObjectId);

        logger.trace("Query ["
                + SiteQuery.SELECT_SITE_FOR_DEVICE.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<Long> list = template.queryForList(query, paramMap, Long.class);

        logger.trace("<<< GetSiteIdsForDevice");

        return list;
    }

    /**
     * Site IDs for panel.
     *
     * @param panelObjectId Brain object Id.
     * @return List of Security Group IDs.
     */
    public List<Long> getSiteIdsForPanel(Long panelObjectId) {
        logger.trace(">>> GetSiteIdsForPanel");

        String query = SiteQuery.SELECT_SITE_FOR_PANEL.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelOID", panelObjectId);

        logger.trace("Query ["
                + SiteQuery.SELECT_SITE_FOR_PANEL.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<Long> list = template.queryForList(query, paramMap, Long.class);

        logger.trace("<<< GetSiteIdsForPanel");

        return list;
    }

}