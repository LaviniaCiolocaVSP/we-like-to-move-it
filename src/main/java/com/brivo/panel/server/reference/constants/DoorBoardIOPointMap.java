package com.brivo.panel.server.reference.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum DoorBoardIOPointMap 
{
    NULL_STATE(-1, null),
    DOOR_1_REX(1, "DOOR 1 - REX"),
    DOOR_1_DOOR_CONTACT(2, "DOOR 1 - DOOR CONTACT"),
    DOOR_1_DOOR_LOCK_RELAY(3, "DOOR 1 - DOOR LOCK RELAY"),
    //For the 300 and 6K panels they use the 4 & 17 points for door lock relay
    DOOR_1_DOOR_LOCK_RELAY_ALT(4, "DOOR 1 - DOOR LOCK RELAY"),
    DOOR_1_AUX_RELAY_1(5, "DOOR 1 - AUX RELAY 1"),
    DOOR_1_AUX_INPUT_1(6, "DOOR 1 - AUX INPUT 1"),
    DOOR_1_AUX_INPUT_2(7, "DOOR 1 - AUX INPUT 2"),
    DOOR_1_AUX_RELAY_2(8, "DOOR 1 - AUX RELAY 2"),
    DOOR_1_READER(10, "DOOR 1 - READER"),
    DOOR_2_REX(14, "DOOR 2 - REX"),
    DOOR_2_DOOR_CONTACT(15, "DOOR 2 - DOOR CONTACT"),
    DOOR_2_DOOR_LOCK_RELAY(16, "DOOR 2 - DOOR LOCK RELAY"),
    //For the 300 and 6K panels they use the 4 & 17 points for door lock relay
    DOOR_2_DOOR_LOCK_RELAY_ALT(17, "DOOR 2 - DOOR LOCK RELAY"),
    DOOR_2_AUX_RELAY_1(18, "DOOR 2 - AUX RELAY 1"),
    DOOR_2_AUX_INPUT_1(19, "DOOR 2 - AUX INPUT 1"),
    DOOR_2_AUX_INPUT_2(20, "DOOR 2 - AUX INPUT 2"),
    DOOR_2_AUX_RELAY_2(21, "DOOR 2 - AUX RELAY 2"),
    DOOR_2_READER(23, "DOOR 2 - READER");


    private static Map<Integer,DoorBoardIOPointMap> byStateID =
        new HashMap<Integer,DoorBoardIOPointMap>();

    static
    {
        byStateID.put(1, DOOR_1_REX);
        byStateID.put(2, DOOR_1_DOOR_CONTACT);
        byStateID.put(3, DOOR_1_DOOR_LOCK_RELAY);
        byStateID.put(5, DOOR_1_AUX_RELAY_1);
        byStateID.put(6, DOOR_1_AUX_INPUT_1);
        byStateID.put(7, DOOR_1_AUX_INPUT_2);
        byStateID.put(8, DOOR_1_AUX_RELAY_2);
        byStateID.put(10, DOOR_1_READER);
        byStateID.put(14, DOOR_2_REX);
        byStateID.put(15, DOOR_2_DOOR_CONTACT);
        byStateID.put(16, DOOR_2_DOOR_LOCK_RELAY);
        byStateID.put(18, DOOR_2_AUX_RELAY_1);
        byStateID.put(19, DOOR_2_AUX_INPUT_1);
        byStateID.put(20, DOOR_2_AUX_INPUT_2);
        byStateID.put(21, DOOR_2_AUX_RELAY_2);
        byStateID.put(23, DOOR_2_READER);
    };



    private int state;
    private String description;

    DoorBoardIOPointMap(int state, String description)
    {
        this.state = state;
        this.description = description;
    }

    public int getState()
    {
        return this.state;
    }

    public String getDescription()
    {
        return description;
    }

    @Override
    public String toString()
    {
        return description;
    }

    public String getDisplayText()
    {
        return this.toString();
    }

    /**
     * Convert an int state id value into the
     * corresponding enumeration value.
     *
     * @param stateID
     * @return
     */
    public static DoorBoardIOPointMap getStateByID(int stateID)
    {
        DoorBoardIOPointMap state = byStateID.get(stateID);

        if(state == null)
        {
            state = NULL_STATE;
        }

        return state;
    }

    public static List<DoorBoardIOPointMap> getAllStates()
    {
        List<DoorBoardIOPointMap> states = new ArrayList<DoorBoardIOPointMap>();

        states.add(DOOR_1_REX);
        states.add(DOOR_1_DOOR_CONTACT);
        states.add(DOOR_1_DOOR_LOCK_RELAY);
        states.add(DOOR_1_AUX_RELAY_1);
        states.add(DOOR_1_AUX_INPUT_1);
        states.add(DOOR_1_AUX_INPUT_2);
        states.add(DOOR_1_AUX_RELAY_2);
        states.add(DOOR_1_READER);
        states.add(DOOR_2_REX);
        states.add(DOOR_2_DOOR_CONTACT);
        states.add(DOOR_2_DOOR_LOCK_RELAY);
        states.add(DOOR_2_AUX_RELAY_1);
        states.add(DOOR_2_AUX_INPUT_1);
        states.add(DOOR_2_AUX_INPUT_2);
        states.add(DOOR_2_AUX_RELAY_2);
        states.add(DOOR_2_READER);

        return states;
    }

    public static List<DoorBoardIOPointMap> getIoPointsForADoorNode(int nodeNumber)
    {
        List<DoorBoardIOPointMap> states = new ArrayList<DoorBoardIOPointMap>();

        if(nodeNumber == 1)
        {
            states.add(DOOR_1_REX);
            states.add(DOOR_1_DOOR_CONTACT);
            states.add(DOOR_1_DOOR_LOCK_RELAY);
            states.add(DOOR_1_AUX_RELAY_1);
            states.add(DOOR_1_AUX_INPUT_1);
            states.add(DOOR_1_AUX_INPUT_2);
            states.add(DOOR_1_AUX_RELAY_2);
            states.add(DOOR_1_READER);
        }
        else if(nodeNumber == 2)
        {
            states.add(DOOR_2_REX);
            states.add(DOOR_2_DOOR_CONTACT);
            states.add(DOOR_2_DOOR_LOCK_RELAY);
            states.add(DOOR_2_AUX_RELAY_1);
            states.add(DOOR_2_AUX_INPUT_1);
            states.add(DOOR_2_AUX_INPUT_2);
            states.add(DOOR_2_AUX_RELAY_2);
            states.add(DOOR_2_READER);
        }

        return states;
    }
}
