package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "object_type", schema = "brivo20", catalog = "onair")
public class ObjectType {
    private long objectTypeId;
    private String name;
    private String description;
    private Collection<BrivoObject> objectsByBrivoObjectTypeId;

    @Id
    @Column(name = "object_type_id", nullable = false)
    public long getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(long objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectType that = (ObjectType) o;

        if (objectTypeId != that.objectTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectTypeId ^ (objectTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "objectTypeByObjectTypeId")
    public Collection<BrivoObject> getObjectsByBrivoObjectTypeId() {
        return objectsByBrivoObjectTypeId;
    }

    public void setObjectsByBrivoObjectTypeId(Collection<BrivoObject> objectsByBrivoObjectTypeId) {
        this.objectsByBrivoObjectTypeId = objectsByBrivoObjectTypeId;
    }
}
