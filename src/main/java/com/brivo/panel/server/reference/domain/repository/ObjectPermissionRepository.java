package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ObjectPermissionRepository extends IObjectPermissionRepository {

    Optional<List<ObjectPermission>> findByGroupObjectId(@Param("objectId") Long objectId);

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_permission op " +
                    "WHERE actor_object_id in :groupObjectIds " +
                    "AND op.security_action_id = :securityActionId",
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findAllByGroupObjectId(@Param("groupObjectIds") Collection<Long> groupObjectIds,
                                                            @Param("securityActionId") int securityActionId);

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_permission op " +
                    "WHERE actor_object_id = :groupObjectId " +
                    "AND op.security_action_id = :keypadUnlockSecurityActionId",
            nativeQuery = true
    )
    Optional<ObjectPermission> findKeypadUnlockHoldPrivilege(@Param("groupObjectId") Long groupObjectId,
                                                             @Param("keypadUnlockSecurityActionId") int keypadUnlockSecurityActionId);


    @Query(
            value = "DELETE FROM brivo20.object_permission op " +
                    "WHERE object_id = :deviceObjectId AND actor_object_id = :actorObjectId AND schedule_id = :scheduleId",
            nativeQuery = true
    )
    @Modifying
    void deleteObjectPermission(@Param("deviceObjectId") Long deviceObjectId,
                                @Param("actorObjectId") Long actorObjectId,
                                @Param("scheduleId") Long scheduleId);

    @Query(
            value = "SELECT op.* " +
                    "FROM brivo20.object_permission op " +
                    "WHERE actor_object_id = :groupObjectId " +
                    "AND op.security_action_id = :securityActionId",
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findObjectPermissionsByGroupObjectId(@Param("groupObjectId") Long groupObjectId,
                                                                          @Param("securityActionId") int securityActionId);

    @Query(
            value = "SELECT op.* " +
                    "    FROM security_group sg" +
                    "    JOIN security_group_member sgm" +
                    "    ON (sgm.security_group_id = sg.security_group_id AND sg.object_id = :siteObjectId)" +
                    "    JOIN device d" +
                    "    ON (d.object_id = sgm.object_id)" +
                    "    JOIN brain b" +
                    "    ON (b.brain_id = d.brain_id)" +
                    "    JOIN object_permission op on (d.object_id = op.object_id and op.security_action_id in (:securityActionIds) AND op.actor_object_id = :userGroupObjectId)" +
                    "    JOIN schedule s on (op.schedule_id = s.schedule_id)" +
                    "    LEFT OUTER JOIN door_data dd" +
                    "    ON (d.object_id               = dd.device_oid)" +
                    "    LEFT OUTER JOIN security_group ssg on (s.site_id    = ssg.object_id and ssg.security_group_type_id = 1)" +
                    "    WHERE sg.account_id           = :accountId " +
                    "    AND d.deleted                != 1" +
                    "    AND d.device_type_id         IN (1, 4, 7, 9, 11)" +
                    "    AND sg.security_group_type_id = 1" +
                    "    ORDER BY sg.name," +
                    "    d.device_type_id," +
                    "    d.name",
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findObjectPermissionsByGroupObjectIdAndSiteId(@Param("accountId") Long accountId,
                                                                                   @Param("userGroupObjectId") Long userGroupObjectId,
                                                                                   @Param("siteObjectId") Long siteObjectId,
                                                                                   @Param("securityActionIds") Collection<Integer> securityActionIds);

    @Query(
            value = "SELECT op.* " +
                    "    FROM security_group sg" +
                    "    JOIN security_group_member sgm" +
                    "    ON (sgm.security_group_id = sg.security_group_id)" +
                    "    JOIN device d" +
                    "    ON (d.object_id = sgm.object_id)" +
                    "    JOIN brain b" +
                    "    ON (b.brain_id = d.brain_id)" +
                    "    JOIN object_permission op on (d.object_id = op.object_id and op.security_action_id in (:securityActionIds) AND op.actor_object_id = :userGroupObjectId)" +
                    "    JOIN schedule s on (op.schedule_id = s.schedule_id)" +
                    "    LEFT OUTER JOIN door_data dd" +
                    "    ON (d.object_id               = dd.device_oid)" +
                    "    LEFT OUTER JOIN security_group ssg on (s.site_id    = ssg.object_id and ssg.security_group_type_id = 1)" +
                    "    WHERE sg.account_id           = :accountId " +
                    "    AND d.deleted                != 1" +
                    "    AND d.device_type_id         IN (1, 4, 7, 9, 11)" +
                    "    AND sg.security_group_type_id = 1" +
                    "    ORDER BY sg.name," +
                    "    d.device_type_id," +
                    "    d.name",
            nativeQuery = true
    )
    Optional<List<ObjectPermission>> findCurrentPrivileges(@Param("accountId") Long accountId,
                                                           @Param("userGroupObjectId") Long userGroupObjectId,
                                                           @Param("securityActionIds") Collection<Integer> securityActionIds);

//    //ON AIR -> current_device_group_permissions
//    @Query(
//            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO (sg.name, s.scheduleId, op.securityActionId, op.objectId, d.name)  " +
//                    "FROM SecurityGroup sg " +
//                    "JOIN SecurityGroupMember sgm " +
//                    "ON (sgm.securityGroupId = sg.securityGroupId) " +
//                    "JOIN Device d " +
//                    "ON (d.objectId = sgm.objectId) " +
//                    "JOIN Brain b " +
//                    "ON (b.brainId = d.brainId) " +
//                    "JOIN ObjectPermission op on (d.objectId = op.objectId and op.securityActionId in (:securityActionIds) AND op.actorObjectId = :userGroupObjectId) " +
//                    "JOIN schedule s on (op.scheduleId = s.scheduleId) " +
//                    "LEFT OUTER JOIN DoorData dd " +
//                    "ON (d.objectId               = dd.deviceOid) " +
//                    "LEFT OUTER JOIN SecurityGroup ssg on (s.siteId    = ssg.objectId and ssg.securityGroupTypeId = 1) " +
//                    "WHERE sg.accountId           = :accountId " +
//                    "AND d.deleted                != 1 " +
//                    "AND d.deviceTypeId         IN (1, 4, 7, 9, 11) " +
//                    "AND sg.securityGroupTypeId = 1 " +
//                    "ORDER BY sg.name, " +
//                    "  d.deviceTypeId, " +
//                    "  d.name "
//    )
//    List<UiGroupPermissionDTO> findGroupPermissions(@Param("accountId") Long accountId,
//                                                    @Param("userGroupObjectId") Long userGroupObjectId,
//                                                    @Param("securityActionIds") Collection<Long> securityActionIds);


    // ONAIR - device_group_with_available_permissions.sql
//    @Query(
//            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO (sg.name, s.scheduleId, op.securityActionId, op.objectId, d.name) " +
//                    "FROM SecurityGroup sg" +
//                    "    JOIN SecurityGroupMember sgm where (sgm.securityGroupId = sg.securityGroupId AND sg.objectId = :siteObjectId)" +
//                    "    JOIN Device d where (d.objectId = sgm.objectId)" +
//                    "    JOIN Brain b ON (b.brainId = d.brainId)" +
//                    "    LEFT OUTER JOIN ObjectPermission op on (d.objectId = op.objectId and op.securityActionId in (:securityActionIds) AND op.actorObjectId = :userGroupObjectId)" +
//                    "    LEFT OUTER JOIN schedule s on (op.scheduleId = s.scheduleId)" +
//                    "    LEFT OUTER JOIN DoorData dd ON (d.objectId               = dd.deviceOid)" +
//                    "    LEFT OUTER JOIN SecurityGroup ssg on (s.siteId    = ssg.objectId and ssg.securityGroupTypeId = 1)" +
//                    "    WHERE sg.accountId           = :accountId " +
//                    "    AND d.deleted                != 1" +
//                    "    AND d.deviceTypeId         IN (1, 4, 7, 9, 11)" +
//                    "    AND sg.securityGroupTypeId = 1" +
//                    "    ORDER BY sg.name," +
//                    "    d.deviceTypeId," +
//                    "    d.name"
//    )
//    List<UiGroupPermissionDTO> findUiGroupPermissionDTOForSite(@Param("accountId") Long accountId,
//                                                               @Param("userGroupObjectId") Long userGroupObjectId,
//                                                               @Param("siteObjectId") Long siteObjectId,
//                                                               @Param("securityActionIds") Collection<Long> securityActionIds);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiGroupPermissionDTO (sg.name, op.scheduleId, op.securityActionId, op.objectId, sg.name) " +
                    "    FROM SecurityGroup sg " +
                    " JOIN ObjectPermission op on op.actorObjectId = sg.objectId"

    )
    List<UiGroupPermissionDTO> findTest(@Param("accountId") Long accountId,
                                        @Param("userGroupObjectId") Long userGroupObjectId);
}

