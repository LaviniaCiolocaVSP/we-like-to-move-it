package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class UserTypeRolePK implements Serializable {
    private long userTypeId;
    private String role;

    @Column(name = "user_type_id", nullable = false)
    @Id
    public long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(long userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Column(name = "role", nullable = false, length = 30)
    @Id
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserTypeRolePK that = (UserTypeRolePK) o;

        if (userTypeId != that.userTypeId) {
            return false;
        }
        return role != null ? role.equals(that.role) : that.role == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userTypeId ^ (userTypeId >>> 32));
        result = 31 * result + (role != null ? role.hashCode() : 0);
        return result;
    }
}
