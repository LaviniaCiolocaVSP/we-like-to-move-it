package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.PanelProperty;
import com.brivo.panel.server.reference.model.hardware.PanelPropertyType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Panel Configuration Extractor
 *
 * @author brandon
 */
@Component
public class PanelPropertyExtractor
        implements ResultSetExtractor<List<PanelProperty>> {
    private static final Logger LOGGER = LoggerFactory.getLogger(PanelPropertyExtractor.class);

    @Override
    public List<PanelProperty> extractData(ResultSet rs)
            throws SQLException, DataAccessException {
        List<PanelProperty> list = new ArrayList<>();

        while (rs.next()) {
            PanelProperty prop = new PanelProperty();

            String name = rs.getString("name");
            String value = rs.getString("value");

            LOGGER.debug("Panel Property - name [{}], value [{}].", name, value);

            PanelPropertyType type = PanelPropertyType.getPanelPropertyType(name);

            prop.setPanelPropertyType(type);
            prop.setValue(value);

            list.add(prop);
        }

        return list;
    }
}
