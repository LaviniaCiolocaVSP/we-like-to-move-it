package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.time.Instant;

public class UiPanelDTO implements Serializable {
    private Long brainId;
    private Long objectId;
    private String panelCP;
    private String firmwareVersion;
    private Instant lastPanelHeartbeat;
    private String panelType;
    private Long panelTypeId;
    private String name;
    private Long accountId;
    private Long logPeriod;
    private Long antipassBackResetInterval;
    private Long networkId;
    private String password;
    private String timezone;
    private String buildVersion;
    private String ipAddress;

    public UiPanelDTO() {

    }

    public UiPanelDTO(final Long brainId, final Long objectId, final String panelCP, final String firmwareVersion,
                      final String buildVersion, final String ipAddress,
                      final Instant lastPanelHeartbeat, final Long panelTypeId, final String panelType, final String name,
                      final Long accountId, final Long logPeriod, final Long antipassBackResetInterval,
                      final Long networkId, final String password, final String timezone) {
        this.brainId = brainId;
        this.objectId = objectId;
        this.panelCP = panelCP;
        this.firmwareVersion = firmwareVersion;
        this.lastPanelHeartbeat = lastPanelHeartbeat;
        this.panelType = panelType;
        this.panelTypeId = panelTypeId;
        this.name = name;
        this.accountId = accountId;
        this.logPeriod = logPeriod;
        this.antipassBackResetInterval = antipassBackResetInterval;
        this.networkId = networkId;
        this.password = password;
        this.timezone = timezone;
        this.buildVersion = buildVersion;
        this.ipAddress = ipAddress;
    }

    public Long getBrainId() {
        return brainId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public String getPanelCP() {
        return panelCP;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public Instant getLastPanelHeartbeat() {
        return lastPanelHeartbeat;
    }

    public String getPanelType() {
        return panelType;
    }

    public Long getPanelTypeId() {
        return panelTypeId;
    }

    public String getName() {
        return name;
    }

    public Long getAccountId() {
        return accountId;
    }

    public Long getLogPeriod() {
        return logPeriod;
    }

    public Long getAntipassBackResetInterval() {
        return antipassBackResetInterval;
    }

    public Long getNetworkId() {
        return networkId;
    }

    public String getPassword() {
        return password;
    }

    public String getTimezone() {
        return timezone;
    }

    public String getBuildVersion() {
        return buildVersion;
    }

    public String getIpAddress() {
        return ipAddress;
    }
}
