package com.brivo.panel.server.reference.domain.brivo;


import com.brivo.panel.server.reference.domain.common.IBrainStateRepository;
import com.brivo.panel.server.reference.domain.entities.BrainState;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrivoBrainStateRepository extends IBrainStateRepository {
    Optional<BrainState> findByObjectId(@Param("objectId") long objectId);
}