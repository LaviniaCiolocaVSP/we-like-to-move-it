package com.brivo.panel.server.reference.model.hardware;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class AllegionGateway {
    private Long deviceId;
    private Integer rs485NodeId;
    private Integer gatewayId;
    private List<AllegionLock> locks;
    private Integer boardNum;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }


    public Integer getRs485NodeId() {
        return rs485NodeId;
    }

    public void setRs485NodeId(Integer rs485NodeId) {
        this.rs485NodeId = rs485NodeId;
    }

    public Integer getGatewayId() {
        return gatewayId;
    }

    public void setGatewayId(Integer gatewayId) {
        this.gatewayId = gatewayId;
    }

    public List<AllegionLock> getLocks() {
        return locks;
    }

    public void setLocks(List<AllegionLock> locks) {
        this.locks = locks;
    }

    @JsonIgnore
    public Integer getBoardNum() {
        return boardNum;
    }

    public void setBoardNum(Integer boardNum) {
        this.boardNum = boardNum;
    }
}
