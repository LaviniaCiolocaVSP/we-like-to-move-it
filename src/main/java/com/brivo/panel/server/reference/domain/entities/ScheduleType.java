package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "schedule_type", schema = "brivo20", catalog = "onair")
public class ScheduleType {
    private long scheduleTypeId;
    private String name;
    private String description;
    private Collection<Schedule> schedulesByScheduleTypeId;

    @Id
    @Column(name = "schedule_type_id", nullable = false)
    public long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 512)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleType that = (ScheduleType) o;

        if (scheduleTypeId != that.scheduleTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleTypeId ^ (scheduleTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "scheduleTypeByScheduleTypeId", cascade = CascadeType.ALL)
    public Collection<Schedule> getSchedulesByScheduleTypeId() {
        return schedulesByScheduleTypeId;
    }

    public void setSchedulesByScheduleTypeId(Collection<Schedule> schedulesByScheduleTypeId) {
        this.schedulesByScheduleTypeId = schedulesByScheduleTypeId;
    }
}
