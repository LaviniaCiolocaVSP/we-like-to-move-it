package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "badge_template_reversals", schema = "brivo20", catalog = "onair")
public class BadgeTemplateReversals {
    private long frontId;
    private long backId;
    private BadgeTemplate badgeTemplateByFrontId;
    private BadgeTemplate badgeTemplateByBackId;

    @Id
    @Basic
    @Column(name = "front_id", nullable = false)
    public long getFrontId() {
        return frontId;
    }

    public void setFrontId(long frontId) {
        this.frontId = frontId;
    }

    @Basic
    @Column(name = "back_id", nullable = false)
    public long getBackId() {
        return backId;
    }

    public void setBackId(long backId) {
        this.backId = backId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeTemplateReversals that = (BadgeTemplateReversals) o;

        if (frontId != that.frontId) {
            return false;
        }
        return backId == that.backId;
    }

    @Override
    public int hashCode() {
        int result = (int) (frontId ^ (frontId >>> 32));
        result = 31 * result + (int) (backId ^ (backId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "front_id", referencedColumnName = "badge_template_id", nullable = false, insertable = false, updatable = false)
    public BadgeTemplate getBadgeTemplateByFrontId() {
        return badgeTemplateByFrontId;
    }

    public void setBadgeTemplateByFrontId(BadgeTemplate badgeTemplateByFrontId) {
        this.badgeTemplateByFrontId = badgeTemplateByFrontId;
    }

    @ManyToOne
    @JoinColumn(name = "back_id", referencedColumnName = "badge_template_id", nullable = false, insertable = false, updatable = false)
    public BadgeTemplate getBadgeTemplateByBackId() {
        return badgeTemplateByBackId;
    }

    public void setBadgeTemplateByBackId(BadgeTemplate badgeTemplateByBackId) {
        this.badgeTemplateByBackId = badgeTemplateByBackId;
    }
}
