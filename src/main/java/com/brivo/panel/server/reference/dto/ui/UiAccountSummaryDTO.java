package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiAccountSummaryDTO implements Serializable {
    private final Long accountId;
    private final String name;

    public UiAccountSummaryDTO(Long accountId, String name) {
        this.accountId = accountId;
        this.name = name;
    }

    public Long getAccountId() {
        return accountId;
    }

    public String getName() {
        return name;
    }
}
