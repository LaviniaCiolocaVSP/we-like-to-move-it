package com.brivo.panel.server.reference.model.mocks;

import com.brivo.panel.server.reference.model.schedule.EnablingGroup;
import com.brivo.panel.server.reference.model.schedule.Holiday;
import com.brivo.panel.server.reference.model.schedule.PanelScheduleData;
import com.brivo.panel.server.reference.model.schedule.Schedule;
import com.brivo.panel.server.reference.model.schedule.ScheduleBlock;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionBlock;
import com.brivo.panel.server.reference.model.schedule.ScheduleThreatSettings;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class ScheduleBuilder {

    public static PanelScheduleData getMockSchedule() {
        final PanelScheduleData panelScheduleData = new PanelScheduleData();

        final List<Schedule> schedules = getSchedules();
        panelScheduleData.setSchedules(schedules);

        final List<Holiday> holidays = getHolidays();
        panelScheduleData.setHolidays(holidays);

        panelScheduleData.setLastUpdate(Instant.now());

        return panelScheduleData;
    }

    private static List<Schedule> getSchedules() {
        final List<Schedule> schedules = new ArrayList<>();
        final Schedule schedule = new Schedule();
        schedule.setScheduleId(0L);

        final List<ScheduleBlock> scheduleBlocks = getScheduleBlocks();
        schedule.setScheduleBlocks(scheduleBlocks);

        final List<Long> holidayIds = new ArrayList<>();
        holidayIds.add(0L);

        schedule.setHolidayIds(holidayIds);

        final EnablingGroup enablingGroup = getEnablingGroup();
        schedule.setEnablingGroup(enablingGroup);

        final ScheduleThreatSettings scheduleThreatSettings = getScheduleThreatSettings();
        schedule.setThreatLevel(scheduleThreatSettings);

        final List<ScheduleExceptionBlock> scheduleExceptionBlocks = getScheduleExceptionBlocks();
        schedule.setExceptionBlocks(scheduleExceptionBlocks);
        schedules.add(schedule);
        return schedules;
    }

    private static List<Holiday> getHolidays() {
        final List<Holiday> holidays = new ArrayList<>();
        final Holiday holiday = new Holiday();
        holiday.setHolidayId(0L);
        holiday.setStartDate(Instant.now());
        holiday.setEndDate(Instant.now());
        holidays.add(holiday);

        return holidays;
    }

    private static List<ScheduleExceptionBlock> getScheduleExceptionBlocks() {
        final List<ScheduleExceptionBlock> scheduleExceptionBlocks = new ArrayList<>();
        final ScheduleExceptionBlock scheduleExceptionBlock = new ScheduleExceptionBlock();
        scheduleExceptionBlock.setEnableBlock(false);
        scheduleExceptionBlock.setStartDate(Instant.now());
        scheduleExceptionBlock.setEndDate(Instant.now());

        return scheduleExceptionBlocks;
    }

    private static ScheduleThreatSettings getScheduleThreatSettings() {
        final ScheduleThreatSettings scheduleThreatSettings = new ScheduleThreatSettings();
        scheduleThreatSettings.setSeverityCheckPoint(0);
        scheduleThreatSettings.setTreatmentOperator(0);

        return scheduleThreatSettings;
    }

    private static EnablingGroup getEnablingGroup() {
        final EnablingGroup enablingGroup = new EnablingGroup();
        enablingGroup.setEnablingGroupId(0L);
        enablingGroup.setGracePeriod(0);
        enablingGroup.setAutoDeactivate(false);

        return enablingGroup;
    }

    private static List<ScheduleBlock> getScheduleBlocks() {
        final List<ScheduleBlock> scheduleBlocks = new ArrayList<>();
        final ScheduleBlock scheduleBlock = new ScheduleBlock();
        scheduleBlock.setStartMinute(0);
        scheduleBlock.setEndMinute(0);

        scheduleBlocks.add(scheduleBlock);

        return scheduleBlocks;
    }
}
