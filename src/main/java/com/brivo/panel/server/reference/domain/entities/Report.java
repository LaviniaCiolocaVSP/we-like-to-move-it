package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@IdClass(ReportPK.class)
public class Report {
    private long reportId;
    private long accountId;
    private String name;
    private long typeId;
    private long formatId;
    private short userDisabled;
    private long objectId;
    private Long ownerUserId;
    private Long ownerObjectId;
    private Account accountByAccountId;
    private ReportType reportTypeByTypeId;
    private ReportFormat reportFormatByFormatId;
    private BrivoObject objectByBrivoObjectId;
    private BrivoObject objectByOwnerBrivoObjectId;
    private ReportFilter reportFilterByReportId;
    private ReportSelect reportSelectByReportId;

    @Id
    @Column(name = "report_id", nullable = false)
    public long getReportId() {
        return reportId;
    }

    public void setReportId(long reportId) {
        this.reportId = reportId;
    }

    @Id
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "type_id", nullable = false)
    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "format_id", nullable = false)
    public long getFormatId() {
        return formatId;
    }

    public void setFormatId(long formatId) {
        this.formatId = formatId;
    }

    @Basic
    @Column(name = "user_disabled", nullable = false)
    public short getUserDisabled() {
        return userDisabled;
    }

    public void setUserDisabled(short userDisabled) {
        this.userDisabled = userDisabled;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "owner_user_id", nullable = true)
    public Long getOwnerUserId() {
        return ownerUserId;
    }

    public void setOwnerUserId(Long ownerUserId) {
        this.ownerUserId = ownerUserId;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = true)
    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Report report = (Report) o;

        if (reportId != report.reportId) {
            return false;
        }
        if (accountId != report.accountId) {
            return false;
        }
        if (typeId != report.typeId) {
            return false;
        }
        if (formatId != report.formatId) {
            return false;
        }
        if (userDisabled != report.userDisabled) {
            return false;
        }
        if (objectId != report.objectId) {
            return false;
        }
        if (name != null ? !name.equals(report.name) : report.name != null) {
            return false;
        }
        if (ownerUserId != null ? !ownerUserId.equals(report.ownerUserId) : report.ownerUserId != null) {
            return false;
        }
        return ownerObjectId != null ? ownerObjectId.equals(report.ownerObjectId) : report.ownerObjectId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (reportId ^ (reportId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (typeId ^ (typeId >>> 32));
        result = 31 * result + (int) (formatId ^ (formatId >>> 32));
        result = 31 * result + (int) userDisabled;
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (ownerUserId != null ? ownerUserId.hashCode() : 0);
        result = 31 * result + (ownerObjectId != null ? ownerObjectId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false,
            insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "type_id", referencedColumnName = "type_id", nullable = false,
            insertable = false, updatable = false)
    public ReportType getReportTypeByTypeId() {
        return reportTypeByTypeId;
    }

    public void setReportTypeByTypeId(ReportType reportTypeByTypeId) {
        this.reportTypeByTypeId = reportTypeByTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "format_id", referencedColumnName = "format_id", nullable = false,
            insertable = false, updatable = false)
    public ReportFormat getReportFormatByFormatId() {
        return reportFormatByFormatId;
    }

    public void setReportFormatByFormatId(ReportFormat reportFormatByFormatId) {
        this.reportFormatByFormatId = reportFormatByFormatId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false,
            insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByOwnerBrivoObjectId() {
        return objectByOwnerBrivoObjectId;
    }

    public void setObjectByOwnerBrivoObjectId(BrivoObject objectByOwnerBrivoObjectId) {
        this.objectByOwnerBrivoObjectId = objectByOwnerBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "report_id", referencedColumnName = "report_id", nullable = false,
            insertable = false, updatable = false)
    public ReportFilter getReportFilterByReportId() {
        return reportFilterByReportId;
    }

    public void setReportFilterByReportId(ReportFilter reportFilterByReportId) {
        this.reportFilterByReportId = reportFilterByReportId;
    }

    @ManyToOne
    @JoinColumn(name = "report_id", referencedColumnName = "report_id", nullable = false, insertable = false, updatable = false)
    public ReportSelect getReportSelectByReportId() {
        return reportSelectByReportId;
    }

    public void setReportSelectByReportId(ReportSelect reportSelectByReportId) {
        this.reportSelectByReportId = reportSelectByReportId;
    }
}
