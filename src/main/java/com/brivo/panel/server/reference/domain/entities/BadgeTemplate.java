package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "badge_template", schema = "brivo20", catalog = "onair")
public class BadgeTemplate {
    private long badgeTemplateId;
    private long accountId;
    private String name;
    private BigInteger width;
    private BigInteger height;
    private String backgroundColor;
    private Long bgImageMediaId;
    private Timestamp created;
    private Timestamp updated;
    private Collection<BadgeItem> badgeItemsByBadgeTemplateId;
    private Collection<BadgePrintJob> badgePrintJobsByBadgeTemplateId;
    private Account accountByAccountId;
    private BadgeMedia badgeMediaByBgImageMediaId;
    private Collection<BadgeTemplateReversals> badgeTemplateReversalsByBadgeTemplateId;
    private Collection<BadgeTemplateReversals> badgeTemplateReversalsByBadgeTemplateId_0;
    private Collection<UserPrintStatus> userPrintStatusesByBadgeTemplateId;

    @Id
    @Column(name = "badge_template_id", nullable = false)
    public long getBadgeTemplateId() {
        return badgeTemplateId;
    }

    public void setBadgeTemplateId(long badgeTemplateId) {
        this.badgeTemplateId = badgeTemplateId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "width", nullable = false, precision = 0)
    public BigInteger getWidth() {
        return width;
    }

    public void setWidth(BigInteger width) {
        this.width = width;
    }

    @Basic
    @Column(name = "height", nullable = false, precision = 0)
    public BigInteger getHeight() {
        return height;
    }

    public void setHeight(BigInteger height) {
        this.height = height;
    }

    @Basic
    @Column(name = "background_color", nullable = true, length = 32)
    public String getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    @Basic
    @Column(name = "bg_image_media_id", nullable = true)
    public Long getBgImageMediaId() {
        return bgImageMediaId;
    }

    public void setBgImageMediaId(Long bgImageMediaId) {
        this.bgImageMediaId = bgImageMediaId;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeTemplate that = (BadgeTemplate) o;

        if (badgeTemplateId != that.badgeTemplateId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (width != null ? !width.equals(that.width) : that.width != null) {
            return false;
        }
        if (height != null ? !height.equals(that.height) : that.height != null) {
            return false;
        }
        if (backgroundColor != null ? !backgroundColor.equals(that.backgroundColor) : that.backgroundColor != null) {
            return false;
        }
        if (bgImageMediaId != null ? !bgImageMediaId.equals(that.bgImageMediaId) : that.bgImageMediaId != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeTemplateId ^ (badgeTemplateId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (width != null ? width.hashCode() : 0);
        result = 31 * result + (height != null ? height.hashCode() : 0);
        result = 31 * result + (backgroundColor != null ? backgroundColor.hashCode() : 0);
        result = 31 * result + (bgImageMediaId != null ? bgImageMediaId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeTemplateByBadgeTemplateId")
    public Collection<BadgeItem> getBadgeItemsByBadgeTemplateId() {
        return badgeItemsByBadgeTemplateId;
    }

    public void setBadgeItemsByBadgeTemplateId(Collection<BadgeItem> badgeItemsByBadgeTemplateId) {
        this.badgeItemsByBadgeTemplateId = badgeItemsByBadgeTemplateId;
    }

    @OneToMany(mappedBy = "badgeTemplateByBadgeTemplateId")
    public Collection<BadgePrintJob> getBadgePrintJobsByBadgeTemplateId() {
        return badgePrintJobsByBadgeTemplateId;
    }

    public void setBadgePrintJobsByBadgeTemplateId(Collection<BadgePrintJob> badgePrintJobsByBadgeTemplateId) {
        this.badgePrintJobsByBadgeTemplateId = badgePrintJobsByBadgeTemplateId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "bg_image_media_id", referencedColumnName = "badge_media_id", insertable = false, updatable = false)
    public BadgeMedia getBadgeMediaByBgImageMediaId() {
        return badgeMediaByBgImageMediaId;
    }

    public void setBadgeMediaByBgImageMediaId(BadgeMedia badgeMediaByBgImageMediaId) {
        this.badgeMediaByBgImageMediaId = badgeMediaByBgImageMediaId;
    }

    @OneToMany(mappedBy = "badgeTemplateByFrontId")
    public Collection<BadgeTemplateReversals> getBadgeTemplateReversalsByBadgeTemplateId() {
        return badgeTemplateReversalsByBadgeTemplateId;
    }

    public void setBadgeTemplateReversalsByBadgeTemplateId(Collection<BadgeTemplateReversals> badgeTemplateReversalsByBadgeTemplateId) {
        this.badgeTemplateReversalsByBadgeTemplateId = badgeTemplateReversalsByBadgeTemplateId;
    }

    @OneToMany(mappedBy = "badgeTemplateByBackId")
    public Collection<BadgeTemplateReversals> getBadgeTemplateReversalsByBadgeTemplateId_0() {
        return badgeTemplateReversalsByBadgeTemplateId_0;
    }

    public void setBadgeTemplateReversalsByBadgeTemplateId_0(Collection<BadgeTemplateReversals> badgeTemplateReversalsByBadgeTemplateId_0) {
        this.badgeTemplateReversalsByBadgeTemplateId_0 = badgeTemplateReversalsByBadgeTemplateId_0;
    }

    @OneToMany(mappedBy = "badgeTemplateByBadgeTemplateId")
    public Collection<UserPrintStatus> getUserPrintStatusesByBadgeTemplateId() {
        return userPrintStatusesByBadgeTemplateId;
    }

    public void setUserPrintStatusesByBadgeTemplateId(Collection<UserPrintStatus> userPrintStatusesByBadgeTemplateId) {
        this.userPrintStatusesByBadgeTemplateId = userPrintStatusesByBadgeTemplateId;
    }
}
