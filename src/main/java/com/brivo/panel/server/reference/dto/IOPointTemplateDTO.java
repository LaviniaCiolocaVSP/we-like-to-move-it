package com.brivo.panel.server.reference.dto;

public class IOPointTemplateDTO {
    
    private long boardType;
    private int pointAddress;
    private String silkscreen;
    private String defaultLabel;
    private long type;
    private int eol;
    private int state;
    
    
    public IOPointTemplateDTO(long boardType, int pointAddress, String silkscreen, String defaultLabel, long type, int eol, int state) {
        this.boardType = boardType;
        this.pointAddress = pointAddress;
        this.silkscreen = silkscreen;
        this.defaultLabel = defaultLabel;
        this.type = type;
        this.eol = eol;
        this.state = state;
    }
    
    public long getBoardType() {
        return boardType;
    }
    
    public void setBoardType(long boardType) {
        this.boardType = boardType;
    }
    
    public int getPointAddress() {
        return pointAddress;
    }
    
    public void setPointAddress(int pointAddress) {
        this.pointAddress = pointAddress;
    }
    
    public String getSilkscreen() {
        return silkscreen;
    }
    
    public void setSilkscreen(String silkscreen) {
        this.silkscreen = silkscreen;
    }
    
    public String getDefaultLabel() {
        return defaultLabel;
    }
    
    public void setDefaultLabel(String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }
    
    public long getType() {
        return type;
    }
    
    public void setType(long type) {
        this.type = type;
    }
    
    public int getEol() {
        return eol;
    }
    
    public void setEol(int eol) {
        this.eol = eol;
    }
    
    public int getState() {
        return state;
    }
    
    public void setState(int state) {
        this.state = state;
    }
}
