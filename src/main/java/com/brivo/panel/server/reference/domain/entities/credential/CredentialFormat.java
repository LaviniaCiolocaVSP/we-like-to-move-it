package com.brivo.panel.server.reference.domain.entities.credential;

import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class CredentialFormat {
    
    public static final long UNKNOWN_FORMAT_ID = 110;
    public static final String BRIVO_PHONE_PASS = "Brivo Mobile Pass";
    public static final String PIN_NAME_SUFFIX = "Digit Keycode";
    
    public static final char MASK_CHAR = 'X';
    public static final char EVEN_PARITY = 'E';
    public static final char ODD_PARITY = 'O';
    public static final char IGNORE_BIT = '.';
    
    private long credentialFormatId;
    private String name;
    private String description;
    private int numBits;
    
    private CredentialEncoding encoding;
    
    private boolean supports4k;
    
    private List<CredentialField> fields;
    
    public CredentialFormat() {
    
    }
    
    public CredentialFormat(long credentialFormatId) {
        this.credentialFormatId = credentialFormatId;
    }
    
    public CredentialFormat(CredentialFormat toCopy) {
        setCredentialFormatId(toCopy.getCredentialFormatId());
        setName(toCopy.getName());
        setDescription(toCopy.getDescription());
        setNumBits(toCopy.getNumBits());
        setEncoding(toCopy.getEncoding());
        
        if (toCopy.getFields() != null) {
            fields = new ArrayList<CredentialField>();
            
            for (CredentialField field : toCopy.getFields()) {
                fields.add(field);
                
            }
        }
    }
    
    public boolean isUnknownFormat() {
        return credentialFormatId == UNKNOWN_FORMAT_ID;
    }
    
    public boolean isBrivoPhonePass() {
        if (name == null) return false;
        return name.equals(BRIVO_PHONE_PASS);
    }
    
    public boolean isPin() {
        return StringUtils.endsWith(this.name, PIN_NAME_SUFFIX);
    }
    
    public void setNumBits(int numBits) {
        this.numBits = numBits;
    }
    
    public int getNumBits() {
        return numBits;
    }
    
    public void setFields(List<CredentialField> fields) {
        this.fields = fields;
    }
    
    public List<CredentialField> getFields() {
        return fields;
    }
    
    public void setEncoding(CredentialEncoding encoding) {
        this.encoding = encoding;
    }
    
    public CredentialEncoding getEncoding() {
        return encoding;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return name;
    }
    
    public void setCredentialFormatId(long credentialFormatId) {
        this.credentialFormatId = credentialFormatId;
    }
    
    public long getCredentialFormatId() {
        return credentialFormatId;
    }
    
    public static CredentialFormat formatForPin(String pinValue) {
        CredentialFormat format = new CredentialFormat();
        switch (pinValue.length()) {
            case 4:
                format.setCredentialFormatId(1);
                break;
            case 5:
                format.setCredentialFormatId(2);
                break;
            case 6:
                format.setCredentialFormatId(3);
                break;
            case 7:
                format.setCredentialFormatId(4);
                break;
            case 8:
                format.setCredentialFormatId(5);
                break;
            default:
                return null;
        }
        return format;
    }
    
    public void setSupports4k(boolean supports4k) {
        this.supports4k = supports4k;
    }
    
    public boolean supports4k() {
        return supports4k;
    }
    
    
}
