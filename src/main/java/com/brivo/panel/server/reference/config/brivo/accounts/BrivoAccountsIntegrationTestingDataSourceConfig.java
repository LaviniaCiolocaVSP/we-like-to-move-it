package com.brivo.panel.server.reference.config.brivo.accounts;

import com.brivo.panel.server.reference.RunProfile;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile(RunProfile.INTEGRATION_TESTING)
@PropertySource("classpath:brivo-accounts-database-integration-testing.properties")
public class BrivoAccountsIntegrationTestingDataSourceConfig extends AbstractBrivoAccountsDataSourceConfig {
}
