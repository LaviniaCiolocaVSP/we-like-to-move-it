package com.brivo.panel.server.reference.service.processors.site;

import java.util.List;

/**
 * Processor interface indicating the necessary base operation for obtaining the site ids for a particular event
 */
public interface SiteIdsProcessor {
    List<Long> getSiteIds(Long objectId);
}
