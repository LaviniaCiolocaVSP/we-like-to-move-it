package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleSummaryDTO;
import com.brivo.panel.server.reference.service.UiScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/schedule")
public class UiScheduleController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UiScheduleController.class);
    private final UiScheduleService uiScheduleService;

    @Autowired
    public UiScheduleController(final UiScheduleService uiScheduleService) {
        this.uiScheduleService = uiScheduleService;
    }

    @GetMapping("/{accountId}")
    public Collection<UiScheduleDTO> get(@PathVariable final long accountId) {
        return uiScheduleService.getUiScheduleForAccountId(accountId);
    }

    @GetMapping("/bytype/{accountId}/{siteId}/{scheduleTypeId}")
    public Collection<UiScheduleDTO> get(@PathVariable final long accountId, @PathVariable final long siteId,
                                         @PathVariable final long scheduleTypeId) {
        return uiScheduleService.getUiSchedulesForAccountIdByType(accountId, siteId, scheduleTypeId);
    }

    @GetMapping("/bysite/{accountId}/{siteId}")
    public Collection<UiScheduleSummaryDTO> get(@PathVariable final long accountId, @PathVariable final long siteId) {
        return uiScheduleService.getUiScheduleSummaryDTOSForSiteWithDefault(accountId, siteId);
    }

    @GetMapping("/get/{scheduleId}")
    public UiScheduleDTO getScheduleDetails(@PathVariable final long scheduleId) {
        return uiScheduleService.getUiScheduleByScheduleId(scheduleId);
    }

    @GetMapping("/get/holiday/{holidayId}")
    public Set<UiScheduleDTO> getSchedulesByHolidayId(@PathVariable final long holidayId) {
        LOGGER.debug("getSchedules by holidayId...");
        return uiScheduleService.getUiScheduleByHolidayId(holidayId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final UiScheduleDTO uiScheduleDTO) {
        return uiScheduleService.create(uiScheduleDTO);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO delete(@RequestBody List<Long> scheduleIds) {
        return uiScheduleService.delete(scheduleIds);
    }


    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update(@RequestBody UiScheduleDTO schedule) {
        return uiScheduleService.update(schedule);
    }

}
