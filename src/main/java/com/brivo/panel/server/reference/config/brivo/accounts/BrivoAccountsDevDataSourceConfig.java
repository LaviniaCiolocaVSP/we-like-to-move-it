package com.brivo.panel.server.reference.config.brivo.accounts;

import com.brivo.panel.server.reference.RunProfile;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;

@Configuration
@Profile({
        RunProfile.DEV_MAC,
        RunProfile.DEV_WIN,
        RunProfile.INT
})
@PropertySource("classpath:brivo-accounts-database.properties")
public class BrivoAccountsDevDataSourceConfig extends AbstractBrivoAccountsDataSourceConfig {
}
