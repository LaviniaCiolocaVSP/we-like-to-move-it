package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "report_job", schema = "brivo20", catalog = "onair")
public class ReportJob {
    private long id;
    private long reportConfigurationId;
    private Long reportScheduleId;
    private long userObjectId;
    private long jobStatusId;
    private Timestamp submitted;
    private Timestamp scheduledRun;
    private Timestamp started;
    private Timestamp completed;
    private String runnerInstance;
    private Long executionMillis;
    private long emailOptionId;
    private long languageId;
    private String reportUriExtension;
    private Timestamp created;
    private Timestamp updated;
    private Short deleted;
    private ReportConfiguration reportConfigurationByReportConfigurationId;
    private ReportSchedule reportScheduleByReportScheduleId;
    private ReportJobStatus reportJobStatusByJobStatusId;
    private ReportEmailOption reportEmailOptionByEmailOptionId;
    private Collection<ReportJobOutput> reportJobOutputsById;
    private Collection<ReportJobOutputFormat> reportJobOutputFormatsById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_configuration_id", nullable = false)
    public long getReportConfigurationId() {
        return reportConfigurationId;
    }

    public void setReportConfigurationId(long reportConfigurationId) {
        this.reportConfigurationId = reportConfigurationId;
    }

    @Basic
    @Column(name = "report_schedule_id", nullable = true)
    public Long getReportScheduleId() {
        return reportScheduleId;
    }

    public void setReportScheduleId(Long reportScheduleId) {
        this.reportScheduleId = reportScheduleId;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "job_status_id", nullable = false)
    public long getJobStatusId() {
        return jobStatusId;
    }

    public void setJobStatusId(long jobStatusId) {
        this.jobStatusId = jobStatusId;
    }

    @Basic
    @Column(name = "submitted", nullable = false)
    public Timestamp getSubmitted() {
        return submitted;
    }

    public void setSubmitted(Timestamp submitted) {
        this.submitted = submitted;
    }

    @Basic
    @Column(name = "scheduled_run", nullable = false)
    public Timestamp getScheduledRun() {
        return scheduledRun;
    }

    public void setScheduledRun(Timestamp scheduledRun) {
        this.scheduledRun = scheduledRun;
    }

    @Basic
    @Column(name = "started", nullable = true)
    public Timestamp getStarted() {
        return started;
    }

    public void setStarted(Timestamp started) {
        this.started = started;
    }

    @Basic
    @Column(name = "completed", nullable = true)
    public Timestamp getCompleted() {
        return completed;
    }

    public void setCompleted(Timestamp completed) {
        this.completed = completed;
    }

    @Basic
    @Column(name = "runner_instance", nullable = true, length = 45)
    public String getRunnerInstance() {
        return runnerInstance;
    }

    public void setRunnerInstance(String runnerInstance) {
        this.runnerInstance = runnerInstance;
    }

    @Basic
    @Column(name = "execution_millis", nullable = true)
    public Long getExecutionMillis() {
        return executionMillis;
    }

    public void setExecutionMillis(Long executionMillis) {
        this.executionMillis = executionMillis;
    }

    @Basic
    @Column(name = "email_option_id", nullable = false)
    public long getEmailOptionId() {
        return emailOptionId;
    }

    public void setEmailOptionId(long emailOptionId) {
        this.emailOptionId = emailOptionId;
    }

    @Basic
    @Column(name = "language_id", nullable = false)
    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    @Basic
    @Column(name = "report_uri_extension", nullable = true, length = 10)
    public String getReportUriExtension() {
        return reportUriExtension;
    }

    public void setReportUriExtension(String reportUriExtension) {
        this.reportUriExtension = reportUriExtension;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportJob reportJob = (ReportJob) o;

        if (id != reportJob.id) {
            return false;
        }
        if (reportConfigurationId != reportJob.reportConfigurationId) {
            return false;
        }
        if (userObjectId != reportJob.userObjectId) {
            return false;
        }
        if (jobStatusId != reportJob.jobStatusId) {
            return false;
        }
        if (emailOptionId != reportJob.emailOptionId) {
            return false;
        }
        if (languageId != reportJob.languageId) {
            return false;
        }
        if (reportScheduleId != null ? !reportScheduleId.equals(reportJob.reportScheduleId) : reportJob.reportScheduleId != null) {
            return false;
        }
        if (submitted != null ? !submitted.equals(reportJob.submitted) : reportJob.submitted != null) {
            return false;
        }
        if (scheduledRun != null ? !scheduledRun.equals(reportJob.scheduledRun) : reportJob.scheduledRun != null) {
            return false;
        }
        if (started != null ? !started.equals(reportJob.started) : reportJob.started != null) {
            return false;
        }
        if (completed != null ? !completed.equals(reportJob.completed) : reportJob.completed != null) {
            return false;
        }
        if (runnerInstance != null ? !runnerInstance.equals(reportJob.runnerInstance) : reportJob.runnerInstance != null) {
            return false;
        }
        if (executionMillis != null ? !executionMillis.equals(reportJob.executionMillis) : reportJob.executionMillis != null) {
            return false;
        }
        if (reportUriExtension != null ? !reportUriExtension.equals(reportJob.reportUriExtension) : reportJob.reportUriExtension != null) {
            return false;
        }
        if (created != null ? !created.equals(reportJob.created) : reportJob.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(reportJob.updated) : reportJob.updated != null) {
            return false;
        }
        return deleted != null ? deleted.equals(reportJob.deleted) : reportJob.deleted == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportConfigurationId ^ (reportConfigurationId >>> 32));
        result = 31 * result + (reportScheduleId != null ? reportScheduleId.hashCode() : 0);
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (int) (jobStatusId ^ (jobStatusId >>> 32));
        result = 31 * result + (submitted != null ? submitted.hashCode() : 0);
        result = 31 * result + (scheduledRun != null ? scheduledRun.hashCode() : 0);
        result = 31 * result + (started != null ? started.hashCode() : 0);
        result = 31 * result + (completed != null ? completed.hashCode() : 0);
        result = 31 * result + (runnerInstance != null ? runnerInstance.hashCode() : 0);
        result = 31 * result + (executionMillis != null ? executionMillis.hashCode() : 0);
        result = 31 * result + (int) (emailOptionId ^ (emailOptionId >>> 32));
        result = 31 * result + (int) (languageId ^ (languageId >>> 32));
        result = 31 * result + (reportUriExtension != null ? reportUriExtension.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_configuration_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportConfiguration getReportConfigurationByReportConfigurationId() {
        return reportConfigurationByReportConfigurationId;
    }

    public void setReportConfigurationByReportConfigurationId(ReportConfiguration reportConfigurationByReportConfigurationId) {
        this.reportConfigurationByReportConfigurationId = reportConfigurationByReportConfigurationId;
    }

    @ManyToOne
    @JoinColumn(name = "report_schedule_id", referencedColumnName = "id", insertable = false, updatable = false)
    public ReportSchedule getReportScheduleByReportScheduleId() {
        return reportScheduleByReportScheduleId;
    }

    public void setReportScheduleByReportScheduleId(ReportSchedule reportScheduleByReportScheduleId) {
        this.reportScheduleByReportScheduleId = reportScheduleByReportScheduleId;
    }

    @ManyToOne
    @JoinColumn(name = "job_status_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportJobStatus getReportJobStatusByJobStatusId() {
        return reportJobStatusByJobStatusId;
    }

    public void setReportJobStatusByJobStatusId(ReportJobStatus reportJobStatusByJobStatusId) {
        this.reportJobStatusByJobStatusId = reportJobStatusByJobStatusId;
    }

    @ManyToOne
    @JoinColumn(name = "email_option_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportEmailOption getReportEmailOptionByEmailOptionId() {
        return reportEmailOptionByEmailOptionId;
    }

    public void setReportEmailOptionByEmailOptionId(ReportEmailOption reportEmailOptionByEmailOptionId) {
        this.reportEmailOptionByEmailOptionId = reportEmailOptionByEmailOptionId;
    }

    @OneToMany(mappedBy = "reportJobByReportJobId")
    public Collection<ReportJobOutput> getReportJobOutputsById() {
        return reportJobOutputsById;
    }

    public void setReportJobOutputsById(Collection<ReportJobOutput> reportJobOutputsById) {
        this.reportJobOutputsById = reportJobOutputsById;
    }

    @OneToMany(mappedBy = "reportJobByReportJobId")
    public Collection<ReportJobOutputFormat> getReportJobOutputFormatsById() {
        return reportJobOutputFormatsById;
    }

    public void setReportJobOutputFormatsById(Collection<ReportJobOutputFormat> reportJobOutputFormatsById) {
        this.reportJobOutputFormatsById = reportJobOutputFormatsById;
    }
}
