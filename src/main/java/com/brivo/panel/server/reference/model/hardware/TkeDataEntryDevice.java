package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TkeDataEntryDevice {
    private Long deviceId;
    private Integer inputBoardAddress;
    private Integer inputPointAddress;
    private TwoFactorCredentialSettings twoFactorCredential;
    private Long cardRequiredScheduleId;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getInputBoardAddress() {
        return inputBoardAddress;
    }

    public void setInputBoardAddress(Integer inputBoardAddress) {
        this.inputBoardAddress = inputBoardAddress;
    }

    public Integer getInputPointAddress() {
        return inputPointAddress;
    }

    public void setInputPointAddress(Integer inputPointAddress) {
        this.inputPointAddress = inputPointAddress;
    }

    public TwoFactorCredentialSettings getTwoFactorCredential() {
        return twoFactorCredential;
    }

    public void setTwoFactorCredential(
            TwoFactorCredentialSettings twoFactorCredential) {
        this.twoFactorCredential = twoFactorCredential;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }
}
