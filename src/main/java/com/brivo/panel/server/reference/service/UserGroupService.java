package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.usergroup.UserGroupDao;
import com.brivo.panel.server.reference.model.credential.CardMask;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.usergroup.UserGroup;
import com.brivo.panel.server.reference.model.usergroup.UserGroupData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserGroupService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupService.class);

    @Autowired
    private CredentialService credentialService;

    @Autowired
    private UserGroupDao userGroupDao;

    /**
     * Return UserGroup and CardMask data for this panel.
     *
     * @param panel The panel.
     * @return UserGroupData.
     */
    public UserGroupData getUserGroupData(Panel panel) {
        LOGGER.trace(">>> GetUserGroupData");

        UserGroupData data = new UserGroupData();
        data.setLastUpdate(panel.getPersonsChanged());

        List<CardMask> maskList = credentialService.getCardMasks();
        data.setCardmasks(maskList);

        List<UserGroup> groupList = userGroupDao.getUserGroups(panel);
        data.setUserGroups(groupList);

        LOGGER.trace("<<< GetUserGroupData");

        return data;
    }

}
