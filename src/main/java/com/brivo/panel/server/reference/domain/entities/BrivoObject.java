package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "object")
public class BrivoObject {
    private long objectId;
    private long objectTypeId;
    private Collection<AccessCredential> accessCredentialsByObjectId;
    private Collection<Account> accountsByObjectId;
    private Collection<Address> addressesByObjectId;
    //private Collection<Application> applicationsByObjectId;
    //private Collection<ApplicationAccount> applicationAccountsByObjectId;
    //private Collection<ApplicationKey> applicationKeysByObjectId;
    private Board boardByObjectId;
    private Brain brainByObjectId;
    //private BrainLock brainLockByObjectId;
    private BrainState brainStateByObjectId;
    //private CameraGroup cameraGroupByObjectId;
    //private Collection<CameraGroup> cameraGroupsByObjectId;
    private Device deviceByObjectId;
    //private Collection<Email> emailsByObjectId;
    //private NotifRule notifRuleByObjectId;
    /*
    private Collection<NotifRule> notifRulesByObjectId;
    private Collection<NotifRuleCondition> notifRuleConditionsByObjectId;
    private Collection<NotifRuleCondition> notifRuleConditionsByObjectId_0;
    private Collection<OauthApplication> oauthApplicationsByObjectId;
    */
    private ObjectType objectTypeByObjectTypeId;
    /*
    private Collection<ObjectNotificationType> objectNotificationTypesByObjectId;
    */
    private Collection<ObjectPermission> objectPermissionsByBrivoObjectId;
    private Collection<ObjectPermission> objectPermissionsByActorBrivoObjectId;
    /*
    private Collection<ObjectPermissionTemplate> objectPermissionTemplatesByObjectId;
    private Collection<ObjectProperty> objectPropertiesByObjectId;
    private Collection<OvrCamera> ovrCamerasByObjectId;
    private Collection<Report> reportsByObjectId;
    private Collection<Report> reportsByObjectId_0;
    private Collection<ReportConfiguration> reportConfigurationsByObjectId;
    private Collection<ReportSchedule> reportSchedulesByObjectId;
    private Collection<ReportSchedule> reportSchedulesByObjectId_0;
    private Collection<ReportScheduleRecipient> reportScheduleRecipientsByObjectId;
    private Collection<ReportShortcut> reportShortcutsByObjectId;
    */
    private Collection<SecurityGroup> securityGroupsByObjectId;
    /*
    private Collection<SecurityGroupMember> securityGroupMembersByObjectId;
    private Collection<SiteProximity> siteProximitiesByObjectId;
    private Collection<Telephone> telephonesByObjectId;
    */
    private Users usersByObjectId;
    /*
    private Collection<VideoCamera> videoCamerasByObjectId;
    private Collection<VideoProvider> videoProvidersByObjectId;
    */


    @Override
    public String toString() {
        return "BrivoObject{" +
                "objectId=" + objectId +
                ", deviceByObjectId=" + deviceByObjectId +
                '}';
    }

    public BrivoObject(final BrivoObjectTypes objectType) {
        this.objectTypeId = objectType.getObjectTypeID();
    }

    protected BrivoObject() {
    }

    @Id
    @Column(name = "object_id", insertable = true, updatable = true, unique = true, nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "object_type_id", nullable = false)
    public long getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(long objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrivoObject brivoObject = (BrivoObject) o;

        if (objectId != brivoObject.objectId) {
            return false;
        }
        return objectTypeId == brivoObject.objectTypeId;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (objectTypeId ^ (objectTypeId >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "objectByOwnerObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<AccessCredential> getAccessCredentialsByObjectId() {
        return accessCredentialsByObjectId;
    }

    public void setAccessCredentialsByObjectId(Collection<AccessCredential> accessCredentialsByObjectId) {
        this.accessCredentialsByObjectId = accessCredentialsByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId", cascade = CascadeType.ALL)
    public Collection<Account> getAccountsByObjectId() {
        return accountsByObjectId;
    }

    public void setAccountsByObjectId(Collection<Account> accountsByObjectId) {
        this.accountsByObjectId = accountsByObjectId;
    }

    @OneToMany(mappedBy = "objectByOwnerBrivoObjectId")
    public Collection<Address> getAddressesByObjectId() {
        return addressesByObjectId;
    }

    public void setAddressesByObjectId(Collection<Address> addressesByObjectId) {
        this.addressesByObjectId = addressesByObjectId;
    }

    /*
            @OneToMany(mappedBy = "objectByObjectId")
            public Collection<Application> getApplicationsByObjectId() {
                return applicationsByObjectId;
            }

            public void setApplicationsByObjectId(Collection<Application> applicationsByObjectId) {
                this.applicationsByObjectId = applicationsByObjectId;
            }

            @OneToMany(mappedBy = "objectByObjectId")
            public Collection<ApplicationAccount> getApplicationAccountsByObjectId() {
                return applicationAccountsByObjectId;
            }

            public void setApplicationAccountsByObjectId(Collection<ApplicationAccount> applicationAccountsByObjectId) {
                this.applicationAccountsByObjectId = applicationAccountsByObjectId;
            }

            @OneToMany(mappedBy = "objectByObjectId")
            public Collection<ApplicationKey> getApplicationKeysByObjectId() {
                return applicationKeysByObjectId;
            }

            public void setApplicationKeysByObjectId(Collection<ApplicationKey> applicationKeysByObjectId) {
                this.applicationKeysByObjectId = applicationKeysByObjectId;
            }

            */
    @OneToOne(mappedBy = "objectByBrivoObjectId", cascade = CascadeType.ALL)
    public Board getBoardByObjectId() {
        return boardByObjectId;
    }

    public void setBoardByObjectId(Board boardByObjectId) {
        this.boardByObjectId = boardByObjectId;
    }

    @OneToOne(mappedBy = "objectByObjectId", cascade = CascadeType.ALL)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }


    /*
    @OneToOne(mappedBy = "objectByObjectId")
    public BrainLock getBrainLockByObjectId() {
        return brainLockByObjectId;
    }

    public void setBrainLockByObjectId(BrainLock brainLockByObjectId) {
        this.brainLockByObjectId = brainLockByObjectId;
    }

    */
    @OneToOne(mappedBy = "objectByBrivoObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public BrainState getBrainStateByObjectId() {
        return brainStateByObjectId;
    }

    public void setBrainStateByObjectId(BrainState brainStateByObjectId) {
        this.brainStateByObjectId = brainStateByObjectId;
    }

    /*
        @OneToOne(mappedBy = "objectByObjectId")
        public CameraGroup getCameraGroupByObjectId() {
            return cameraGroupByObjectId;
        }

        public void setCameraGroupByObjectId(CameraGroup cameraGroupByObjectId) {
            this.cameraGroupByObjectId = cameraGroupByObjectId;
        }

        @OneToMany(mappedBy = "objectByUserObjectId")
        public Collection<CameraGroup> getCameraGroupsByObjectId() {
            return cameraGroupsByObjectId;
        }

        public void setCameraGroupsByObjectId(Collection<CameraGroup> cameraGroupsByObjectId) {
            this.cameraGroupsByObjectId = cameraGroupsByObjectId;
        }

        */
    @OneToOne(mappedBy = "objectByBrivoObjectId", cascade = CascadeType.ALL)
    public Device getDeviceByObjectId() {
        return deviceByObjectId;
    }

    public void setDeviceByObjectId(Device deviceByObjectId) {
        this.deviceByObjectId = deviceByObjectId;
    }

    /*
    @OneToMany(mappedBy = "objectByOwnerObjectId")
    public Collection<Email> getEmailsByObjectId() {
        return emailsByObjectId;
    }

    public void setEmailsByObjectId(Collection<Email> emailsByObjectId) {
        this.emailsByObjectId = emailsByObjectId;
    }

    @OneToOne(mappedBy = "objectByObjectId")
    public NotifRule getNotifRuleByObjectId() {
        return notifRuleByObjectId;
    }

    public void setNotifRuleByObjectId(NotifRule notifRuleByObjectId) {
        this.notifRuleByObjectId = notifRuleByObjectId;
    }

    @OneToMany(mappedBy = "objectByOwnerObjectId")
    public Collection<NotifRule> getNotifRulesByObjectId() {
        return notifRulesByObjectId;
    }

    public void setNotifRulesByObjectId(Collection<NotifRule> notifRulesByObjectId) {
        this.notifRulesByObjectId = notifRulesByObjectId;
    }

    @OneToMany(mappedBy = "objectByActorObjectId")
    public Collection<NotifRuleCondition> getNotifRuleConditionsByObjectId() {
        return notifRuleConditionsByObjectId;
    }

    public void setNotifRuleConditionsByObjectId(Collection<NotifRuleCondition> notifRuleConditionsByObjectId) {
        this.notifRuleConditionsByObjectId = notifRuleConditionsByObjectId;
    }

    @OneToMany(mappedBy = "objectByActeeObjectId")
    public Collection<NotifRuleCondition> getNotifRuleConditionsByObjectId_0() {
        return notifRuleConditionsByObjectId_0;
    }

    public void setNotifRuleConditionsByObjectId_0(Collection<NotifRuleCondition> notifRuleConditionsByObjectId_0) {
        this.notifRuleConditionsByObjectId_0 = notifRuleConditionsByObjectId_0;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<OauthApplication> getOauthApplicationsByObjectId() {
        return oauthApplicationsByObjectId;
    }

    public void setOauthApplicationsByObjectId(Collection<OauthApplication> oauthApplicationsByObjectId) {
        this.oauthApplicationsByObjectId = oauthApplicationsByObjectId;
    }
    */

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "object_type_id", referencedColumnName = "object_type_id", nullable = false, insertable = false,
            updatable = false)
    public ObjectType getObjectTypeByObjectTypeId() {
        return objectTypeByObjectTypeId;
    }

    public void setObjectTypeByObjectTypeId(ObjectType objectTypeByObjectTypeId) {
        this.objectTypeByObjectTypeId = objectTypeByObjectTypeId;
    }

    /*
    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<ObjectNotificationType> getObjectNotificationTypesByObjectId() {
        return objectNotificationTypesByObjectId;
    }

    public void setObjectNotificationTypesByObjectId(Collection<ObjectNotificationType> objectNotificationTypesByObjectId) {
        this.objectNotificationTypesByObjectId = objectNotificationTypesByObjectId;
    }

    */
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ObjectPermission> getObjectPermissionsByBrivoObjectId() {
        return objectPermissionsByBrivoObjectId;
    }

    public void setObjectPermissionsByBrivoObjectId(Collection<ObjectPermission> objectPermissionsByBrivoObjectId) {
        this.objectPermissionsByBrivoObjectId = objectPermissionsByBrivoObjectId;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ObjectPermission> getObjectPermissionsByActorBrivoObjectId() {
        return objectPermissionsByActorBrivoObjectId;
    }

    public void setObjectPermissionsByActorBrivoObjectId(Collection<ObjectPermission> objectPermissionsByActorObjectId) {
        this.objectPermissionsByActorBrivoObjectId = objectPermissionsByActorObjectId;
    }

/*
    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<ObjectPermissionTemplate> getObjectPermissionTemplatesByObjectId() {
        return objectPermissionTemplatesByObjectId;
    }

    public void setObjectPermissionTemplatesByObjectId(Collection<ObjectPermissionTemplate> objectPermissionTemplatesByObjectId) {
        this.objectPermissionTemplatesByObjectId = objectPermissionTemplatesByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<ObjectProperty> getObjectPropertiesByObjectId() {
        return objectPropertiesByObjectId;
    }

    public void setObjectPropertiesByObjectId(Collection<ObjectProperty> objectPropertiesByObjectId) {
        this.objectPropertiesByObjectId = objectPropertiesByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<OvrCamera> getOvrCamerasByObjectId() {
        return ovrCamerasByObjectId;
    }

    public void setOvrCamerasByObjectId(Collection<OvrCamera> ovrCamerasByObjectId) {
        this.ovrCamerasByObjectId = ovrCamerasByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<Report> getReportsByObjectId() {
        return reportsByObjectId;
    }

    public void setReportsByObjectId(Collection<Report> reportsByObjectId) {
        this.reportsByObjectId = reportsByObjectId;
    }

    @OneToMany(mappedBy = "objectByOwnerObjectId")
    public Collection<Report> getReportsByObjectId_0() {
        return reportsByObjectId_0;
    }

    public void setReportsByObjectId_0(Collection<Report> reportsByObjectId_0) {
        this.reportsByObjectId_0 = reportsByObjectId_0;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<ReportConfiguration> getReportConfigurationsByObjectId() {
        return reportConfigurationsByObjectId;
    }

    public void setReportConfigurationsByObjectId(Collection<ReportConfiguration> reportConfigurationsByObjectId) {
        this.reportConfigurationsByObjectId = reportConfigurationsByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<ReportSchedule> getReportSchedulesByObjectId() {
        return reportSchedulesByObjectId;
    }

    public void setReportSchedulesByObjectId(Collection<ReportSchedule> reportSchedulesByObjectId) {
        this.reportSchedulesByObjectId = reportSchedulesByObjectId;
    }

    @OneToMany(mappedBy = "objectByUserObjectId")
    public Collection<ReportSchedule> getReportSchedulesByObjectId_0() {
        return reportSchedulesByObjectId_0;
    }

    public void setReportSchedulesByObjectId_0(Collection<ReportSchedule> reportSchedulesByObjectId_0) {
        this.reportSchedulesByObjectId_0 = reportSchedulesByObjectId_0;
    }

    @OneToMany(mappedBy = "objectByUserObjectId")
    public Collection<ReportScheduleRecipient> getReportScheduleRecipientsByObjectId() {
        return reportScheduleRecipientsByObjectId;
    }

    public void setReportScheduleRecipientsByObjectId(Collection<ReportScheduleRecipient> reportScheduleRecipientsByObjectId) {
        this.reportScheduleRecipientsByObjectId = reportScheduleRecipientsByObjectId;
    }

    @OneToMany(mappedBy = "objectByUserObjectId")
    public Collection<ReportShortcut> getReportShortcutsByObjectId() {
        return reportShortcutsByObjectId;
    }

    public void setReportShortcutsByObjectId(Collection<ReportShortcut> reportShortcutsByObjectId) {
        this.reportShortcutsByObjectId = reportShortcutsByObjectId;
    }
    */

    @OneToMany(mappedBy = "objectByObjectId", cascade = CascadeType.ALL)
    public Collection<SecurityGroup> getSecurityGroupsByObjectId() {
        return securityGroupsByObjectId;
    }

    public void setSecurityGroupsByObjectId(Collection<SecurityGroup> securityGroupsByObjectId) {
        this.securityGroupsByObjectId = securityGroupsByObjectId;
    }

    /*
    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<SecurityGroupMember> getSecurityGroupMembersByObjectId() {
        return securityGroupMembersByObjectId;
    }

    public void setSecurityGroupMembersByObjectId(Collection<SecurityGroupMember> securityGroupMembersByObjectId) {
        this.securityGroupMembersByObjectId = securityGroupMembersByObjectId;
    }

    @OneToMany(mappedBy = "objectBySiteObjectId")
    public Collection<SiteProximity> getSiteProximitiesByObjectId() {
        return siteProximitiesByObjectId;
    }

    public void setSiteProximitiesByObjectId(Collection<SiteProximity> siteProximitiesByObjectId) {
        this.siteProximitiesByObjectId = siteProximitiesByObjectId;
    }

    @OneToMany(mappedBy = "objectByOwnerObjectId")
    public Collection<Telephone> getTelephonesByObjectId() {
        return telephonesByObjectId;
    }

    public void setTelephonesByObjectId(Collection<Telephone> telephonesByObjectId) {
        this.telephonesByObjectId = telephonesByObjectId;
    }
    */

    @OneToOne(mappedBy = "objectByBrivoObjectId", cascade = CascadeType.ALL)
    public Users getUsersByObjectId() {
        return usersByObjectId;
    }

    public void setUsersByObjectId(Users usersByObjectId) {
        this.usersByObjectId = usersByObjectId;
    }

    /*
    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<VideoCamera> getVideoCamerasByObjectId() {
        return videoCamerasByObjectId;
    }

    public void setVideoCamerasByObjectId(Collection<VideoCamera> videoCamerasByObjectId) {
        this.videoCamerasByObjectId = videoCamerasByObjectId;
    }

    @OneToMany(mappedBy = "objectByObjectId")
    public Collection<VideoProvider> getVideoProvidersByObjectId() {
        return videoProvidersByObjectId;
    }

    public void setVideoProvidersByObjectId(Collection<VideoProvider> videoProvidersByObjectId) {
        this.videoProvidersByObjectId = videoProvidersByObjectId;
    }
    */
}
