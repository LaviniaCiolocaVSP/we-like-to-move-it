package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.IoPointTemplate;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface IOPointTemplateRepository extends CrudRepository<IoPointTemplate, Long> {
    @Query(
            value = "SELECT * " +
                    "FROM brivo20.io_point_template " +
                    "WHERE board_type = :boardType",
            nativeQuery = true
    )
    Optional<List<IoPointTemplate>> getIoPointTemplateByBoardType(@Param("boardType") long boardType);
    
    
}
