package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Board;
import com.brivo.panel.server.reference.domain.entities.BoardPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.Set;

@Repository
public interface BoardRepository extends CrudRepository<Board, BoardPK> {
    Optional<Set<Board>> findByPanelOid(@Param("panelOid") long panelOid);

    Optional<Board> findByObjectId(@Param("objectId") long objectId);

    @Query(
            value = "SELECT b.* " +
                    "FROM brivo20.board b " +
                    "WHERE b.object_id in :objectIds",
            nativeQuery = true
    )
    List<Board> findAllByObjectId(@Param("objectIds") Iterable<Long> objectIds);

    @Query(
            value = "SELECT b.* " +
                    "FROM brivo20.board b " +
                    "WHERE b.panel_oid = :panelOid " +
                    "AND b.board_number = :boardNumber",
            nativeQuery = true
    )
    Optional<Board> findByPanelOidAndBoardNumber(@Param("panelOid") Long panelOid, @Param("boardNumber") Long boardNumber);

    @Query(
            value = "SELECT DISTINCT board_property.value " +
                    "FROM board, board_property " +
                    "WHERE board_property.value_type_id = 25 " +
                    "AND board_property.board_object_id = board.object_id " +
                    "AND board.object_id = :boardObjectId",
            nativeQuery = true
    )
    Optional<String> getBoardNumForAllegionGateway(@Param("boardObjectId") Long boardObjectId);

    @Query(
            value = "SELECT node_num " +
                    "FROM device de, door_data dd, brain br, board bo " +
                    "WHERE bo.panel_oid = br.object_id " +
                    "AND br.brain_id = de.brain_id " +
                    "AND de.object_id = dd.device_oid " +
                    "AND bo.board_number = dd.board_num " +
                    "AND de.deleted = 0 " +
                    "AND bo.object_id = :boardObjectId",
            nativeQuery = true
    )
    Optional<List<Integer>> getUsedLockIds(@Param("boardObjectId") Long boardObjectId);
}
