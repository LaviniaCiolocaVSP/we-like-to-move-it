package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.constants.Definitions;
import com.brivo.panel.server.reference.dao.uiSchedule.UiScheduleDao;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.Holiday;
import com.brivo.panel.server.reference.domain.entities.Schedule;
import com.brivo.panel.server.reference.domain.entities.ScheduleData;
import com.brivo.panel.server.reference.domain.entities.ScheduleExceptionData;
import com.brivo.panel.server.reference.domain.entities.ScheduleHolidayMap;
import com.brivo.panel.server.reference.domain.entities.ScheduleType;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupTypes;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.HolidayRepository;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.domain.repository.ScheduleRepository;
import com.brivo.panel.server.reference.domain.repository.ScheduleTypeRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupRepository;
import com.brivo.panel.server.reference.dto.CucumberScheduleDataCollection;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ScheduleDTO;
import com.brivo.panel.server.reference.dto.ui.UiHolidayDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleDataDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiSiteDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.brivo.panel.server.reference.dto.CucumberScheduleDataCollection.ERROR_MESSAGE_PREFIX;
import static com.brivo.panel.server.reference.dto.CucumberScheduleDataCollection.SUCCESS_MESSAGE;

/**
 * Service class for handling schedule data.
 */
@Service
public class UiScheduleService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiScheduleService.class);

    private static final int ONE_SECOND_IN_MILLISECONDS = 1000;
    private static final String[] WEEK_DAYS = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Holiday"};
    private static final String ALL_DAY = "All day";
    private static final int ONE_WEEK_IN_DAYS = 7;
    private static final int ONE_DAY_IN_HOURS = 24;
    private static final int ONE_HOUR_IN_MINUTES = 60;
    private static final int ONE_DAY_IN_MINUTES = ONE_DAY_IN_HOURS * ONE_HOUR_IN_MINUTES;
    private static final int ONE_WEEK_IN_MINUTES = ONE_WEEK_IN_DAYS * ONE_DAY_IN_HOURS * ONE_HOUR_IN_MINUTES;
    private static final int END_OF_WEEK_BLOCK_MIN = ONE_WEEK_IN_MINUTES - 1;
    private static final int ONE_MINUTE_IN_SECONDS = 60;
    private static final int MINIMUM_HOLIDAY_BLOCK_MINUTE = ONE_WEEK_IN_MINUTES;
    private static final int MAX_HOLIDAY_BLOCK_MINUTE = END_OF_WEEK_BLOCK_MIN + ONE_DAY_IN_MINUTES;

    private static final long PROGIO_SCHEDULE_TYPE = 3L;
    private static final long CADM_1 = 1L;
    private static final long CADM_255 = 255L;
    private static final long CADM_254 = 254L;

    private final static String UNIVERSAL_SITE_NAME = "Universal";
    private static final Long UNIVERSAL_SITE_ID = 0L;

    private static final Long SCHEDULE_ACCESS_TYPE = 1L;
    private static final Long SCHEDULE_GEN_V_TYPE = 3L;

    //private static final Long SCHEDULE_ALWAYS_ACCESS_ID = -1L;
    private static final Long SCHEDULE_NO_ACCESS_ID = -2L;

    private static final int INCORRECT_WEEK_DAY = -1;

    public static final UiScheduleSummaryDTO NO_ACCESS_SCHEDULE = new UiScheduleSummaryDTO(SCHEDULE_NO_ACCESS_ID, "No access", "", 0L);

    private final UiScheduleDao uiScheduleDao;

    private final ScheduleRepository scheduleRepository;
    private final AccountRepository accountRepository;
    private final SecurityGroupRepository securityGroupRepository;
    private final ScheduleTypeRepository scheduleTypeRepository;
    private final HolidayRepository holidayRepository;

    private final PanelRepository panelRepository;

    public UiScheduleService(ScheduleRepository scheduleRepository, AccountRepository accountRepository,
                             SecurityGroupRepository securityGroupRepository, ScheduleTypeRepository scheduleTypeRepository,
                             HolidayRepository holidayRepository, UiScheduleDao uiScheduleDao,
                             PanelRepository panelRepository) {
        this.scheduleRepository = scheduleRepository;
        this.accountRepository = accountRepository;
        this.securityGroupRepository = securityGroupRepository;
        this.scheduleTypeRepository = scheduleTypeRepository;
        this.holidayRepository = holidayRepository;
        this.uiScheduleDao = uiScheduleDao;
        this.panelRepository = panelRepository;
    }

    public Collection<ScheduleDTO> getScheduleDTOS(final Collection<Schedule> schedules) {
        return schedules.stream()
                        .map(convertSchedule())
                        .collect(Collectors.toSet());
    }

    private Function<Schedule, ScheduleDTO> convertSchedule() {
        return schedule -> {
            final Collection<ScheduleDTO.ScheduledBlock> scheduledBlocks = schedule.getScheduleDataByScheduleId()
                                                                                   .stream()
                                                                                   .map(this::getBlock)
                                                                                   .collect(Collectors.toSet());

            final Collection<ScheduleDTO.ScheduledExceptionBlock> scheduleExceptionBlocks =
                    schedule.getScheduleExceptionDataByScheduleId()
                            .stream()
                            .map(this::getExceptionBlock)
                            .collect(Collectors.toSet());

            final Collection<Long> holidaysIds = schedule.getScheduleHolidayMapsByScheduleId()
                                                         .stream()
                                                         .map(ScheduleHolidayMap::getHolidayOid)
                                                         .collect(Collectors.toSet());

            return new ScheduleDTO(schedule.getScheduleId(), schedule.getName(), schedule.getCadmScheduleId(),
                    schedule.getDescription(),
                    schedule.getScheduleTypeByScheduleTypeId().getScheduleTypeId(), schedule.getSiteId(),
                    schedule.getEnablingGroupId(), schedule.getEnablingGracePeriod(), scheduledBlocks,
                    scheduleExceptionBlocks, holidaysIds);
        };
    }

    private ScheduleDTO.ScheduledBlock getBlock(final ScheduleData scheduleData) {
        //  the number of minutes since Sunday Midnight hour 0. Ex: if start_time is 540 --> it is 9 AM (= 540 / 60 - 0 = 9 hours - hour 0)
        return new ScheduleDTO.ScheduledBlock(scheduleData.getStartTime(), scheduleData.getStopTime());
    }

    private ScheduleDTO.ScheduledExceptionBlock getExceptionBlock(final ScheduleExceptionData scheduleExceptionData) {
        //  the number of minutes since Sunday Midnight hour 0. Ex: if start_time is 540 --> it is 9 AM (= 540 / 60 - 0 = 9 hours - hour 0)
        return new ScheduleDTO.ScheduledExceptionBlock(scheduleExceptionData.getExceptionId(),
                scheduleExceptionData.getStartMin(), scheduleExceptionData.getEndMin(), scheduleExceptionData.getRepeatOrdinal(),
                scheduleExceptionData.getRepeatIndex(), scheduleExceptionData.getRepeatType(), scheduleExceptionData.getIsEnableBlock());
    }


    public Set<UiScheduleDTO> getUiScheduleDTOS(final Collection<Schedule> schedules) {
        return schedules.stream()
                        .filter(schedule -> schedule.getCadmScheduleId() != CADM_1 &&
                                schedule.getCadmScheduleId() != CADM_255 &&
                                schedule.getCadmScheduleId() != CADM_254)
                        .map(convertScheduleUI())
                        .collect(Collectors.toSet());
    }

    public Set<UiScheduleDTO> getUiScheduleDTOSNotFiltered(final Collection<Schedule> schedules) {
        return schedules.stream()
                        .map(convertScheduleUI())
                        .collect(Collectors.toSet());
    }

    private Function<Schedule, UiScheduleDTO> convertScheduleUI() {
        return schedule -> {
            String siteName = UNIVERSAL_SITE_NAME;
            //   String siteName = "";
            final Optional<SecurityGroup> site = securityGroupRepository.findByObjectId(schedule.getSiteId());
            if (site.isPresent())
                siteName = site.get().getName();
            return new UiScheduleDTO(schedule.getScheduleId(), schedule.getName(), schedule.getDescription(),
                    schedule.getScheduleTypeByScheduleTypeId().getScheduleTypeId(),
                    schedule.getAccountId(), schedule.getCadmScheduleId(),
                    schedule.getSiteId(), siteName);
        };
    }

    private UiScheduleDTO getUiScheduleDTO(Schedule schedule) {
        Collection<ScheduleData> scheduleDataByScheduleId = schedule.getScheduleDataByScheduleId();
        // Set<UiScheduleDataDTO> scheduleDataDTOS = convertScheduleDataToDTO(scheduleDataByScheduleId);
        Set<UiScheduleDataDTO> scheduleDataDTOS = scheduleStringToDataBlock(scheduleDataByScheduleId);

        String[] scheduleDataArray = scheduleStringToText(scheduleDataByScheduleId);

        String siteName = "";
        String groupName = "";
        if (schedule.getSiteId() != null && schedule.getSiteId() != 0) {
            Optional<SecurityGroup> byId = securityGroupRepository.findByObjectId(schedule.getSiteId());
            if (byId.isPresent())
                siteName = byId.get().getName();
        }
        Long enablingGroupId = schedule.getEnablingGroupId();
        if (enablingGroupId != null && enablingGroupId != 0) {
            Optional<SecurityGroup> byId = securityGroupRepository.findByObjectId(enablingGroupId);
            if (byId.isPresent())
                groupName = byId.get().getName();
        } else
            enablingGroupId = 0l;

        Long gracePeriod = schedule.getEnablingGracePeriod();
        if (gracePeriod == null) gracePeriod = 0L;

        List<Long> holidayIds = new ArrayList<>();
        for (ScheduleHolidayMap scheduleHolidayMap : schedule.getScheduleHolidayMapsByScheduleId()) {
            holidayIds.add(scheduleHolidayMap.getHolidayOid());
        }

        Iterable<Holiday> holidaysById = holidayRepository.findAllById(holidayIds);

        Set<UiHolidayDTO> uiHolidayDTOS = getUiHolidayDTOS(holidaysById);

        return new UiScheduleDTO(schedule.getScheduleId(), schedule.getName(), schedule.getDescription(),
                schedule.getScheduleTypeByScheduleTypeId().getScheduleTypeId(),
                schedule.getAccountId(), schedule.getCadmScheduleId(),
                schedule.getSiteId(), siteName, enablingGroupId, groupName, gracePeriod,
                scheduleDataDTOS, scheduleDataArray, uiHolidayDTOS);
    }

    private static Set<UiScheduleDataDTO> convertScheduleDataToDTO(Collection<ScheduleData> scheduleDataByScheduleId) {
        Set<UiScheduleDataDTO> scheduleDataDTOS = new HashSet<>();

        if (scheduleDataByScheduleId.size() > 0) {
            scheduleDataByScheduleId.forEach(scheduledBlock -> convertScheduleBlockData(scheduleDataDTOS, scheduledBlock));

        }
        return scheduleDataDTOS;
    }

    private static void convertScheduleBlockData(Set<UiScheduleDataDTO> scheduleDataDTOS, ScheduleData scheduledBlock) {
        UiScheduleDataDTO block = new UiScheduleDataDTO();
        block.setScheduleId(scheduledBlock.getScheduleId());
        long startTime = scheduledBlock.getStartTime();
        block.setStartTime(startTime);
        long stopTime = scheduledBlock.getStopTime();
        block.setStopTime(stopTime);

        String start;
        String stop;

        if ((startTime % ONE_DAY_IN_MINUTES == 0) && (stopTime % ONE_DAY_IN_MINUTES) == (ONE_DAY_IN_MINUTES - 1)
                || (startTime % ONE_DAY_IN_MINUTES == 0) && (stopTime % ONE_DAY_IN_MINUTES) == 0) {
            start = ALL_DAY;
            stop = ALL_DAY;
        } else {
            int day = (int) startTime / ONE_DAY_IN_MINUTES;

            int hoursStart = (int) (startTime % ONE_DAY_IN_MINUTES) / ONE_HOUR_IN_MINUTES;
            int minutesStart = (int) (startTime % ONE_DAY_IN_MINUTES) % ONE_HOUR_IN_MINUTES;

            int hoursStop = (int) (stopTime % ONE_DAY_IN_MINUTES) / ONE_HOUR_IN_MINUTES;
            int minutesStop = (int) (stopTime % ONE_DAY_IN_MINUTES) % ONE_HOUR_IN_MINUTES;


            if (minutesStart < 10) {
                start = WEEK_DAYS[day] + " " + hoursStart + ":0" + minutesStart;
            } else {
                start = WEEK_DAYS[day] + " " + hoursStart + ":" + minutesStart;
            }

            if (minutesStop < 10) {
                stop = WEEK_DAYS[day] + " " + hoursStop + ":0" + minutesStop;
            } else {
                stop = WEEK_DAYS[day] + " " + hoursStop + ":" + minutesStop;
            }
        }

        block.setStart(start);
        block.setStop(stop);

        scheduleDataDTOS.add(block);
    }

    public Set<UiScheduleDTO> getUiScheduleForAccountId(final Long accountId) {
        return getUiScheduleDTOS(scheduleRepository.findByAccountId(accountId));
    }

    public Collection<UiScheduleDTO> getUiSchedulesForAccountIdByType(final Long accountId, final Long siteId,
                                                                      final Long scheduleTypeId) {
        final int minType = Math.toIntExact(panelRepository.getMinBrainTypeIdForAccount(accountId));

        if ((minType >= Definitions.MIN_PROGIO_VERSION) && (scheduleTypeId == 1)) {
            return Collections.emptyList();
        }

        final Collection<Schedule> schedules = scheduleRepository.findByAccountIdScheduleTypeIdSiteId(accountId, siteId, scheduleTypeId);

        return getUiScheduleDTOS(schedules);
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UiScheduleDTO getUiScheduleByScheduleId(final Long scheduleId) {
        Optional<Schedule> scheduleList;
        UiScheduleDTO uiScheduleDTO = new UiScheduleDTO();

        scheduleList = scheduleRepository.findById(scheduleId);
        if (scheduleList.isPresent())
            uiScheduleDTO = getUiScheduleDTO(scheduleList.get());
        return uiScheduleDTO;
    }

    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public MessageDTO create(UiScheduleDTO uiScheduleDTO) {
        LOGGER.debug("Creating new schedule from {}", uiScheduleDTO.toString());
        createAndPersistScheduleToDatabase(uiScheduleDTO);

        return new MessageDTO("Schedule '" + uiScheduleDTO.getScheduleName() + "' was successfully created!");
    }

    public Schedule createAndPersistScheduleToDatabase(UiScheduleDTO uiScheduleDTO) {
        LOGGER.debug("Adding new schedule: {}", uiScheduleDTO.toString());

        validateNewSchedule(uiScheduleDTO);
        LOGGER.debug("New schedule {} successfully validated", uiScheduleDTO.toString());

        final Schedule schedule = createSchedule(uiScheduleDTO);

        final Schedule savedSchedule = scheduleRepository.save(schedule);

        LOGGER.debug("Schedule {} successfully persisted to database...", savedSchedule.getName());


        return savedSchedule;
    }

    private Schedule createSchedule(final UiScheduleDTO uiScheduleDTO) {
        final Schedule schedule = new Schedule();

        final Long nextScheduleId = scheduleRepository.getMaxScheduleId().get() + 1;
        final Long nextCadmScheduleId = scheduleRepository.getMaxCadmScheduleId(uiScheduleDTO.getAccountId()).get() + 1;
        LOGGER.debug("nextScheduleId: {}, nextCadmScheduleId: {}", nextScheduleId, nextCadmScheduleId);

        schedule.setScheduleId(nextScheduleId);
        schedule.setName(uiScheduleDTO.getScheduleName());
        schedule.setDescription(uiScheduleDTO.getDescription());
        schedule.setCadmScheduleId(nextCadmScheduleId);
        final LocalDateTime now = LocalDateTime.now();
        schedule.setCreated(now);
        schedule.setUpdated(now);

        schedule.setSiteId(uiScheduleDTO.getSiteId());
        if (uiScheduleDTO.getSiteId() != 0) {
            schedule.setEnablingGroupId(uiScheduleDTO.getGroupId());
            schedule.setEnablingGracePeriod(uiScheduleDTO.getGracePeriod());
        }

        final Account account = accountRepository.findById(uiScheduleDTO.getAccountId()).get();
        schedule.setAccountByAccountId(account);

        Optional<ScheduleType> scheduleType = scheduleTypeRepository.findById(PROGIO_SCHEDULE_TYPE);
        schedule.setScheduleTypeByScheduleTypeId(scheduleType.get());

        Set<ScheduleData> scheduleData = createScheduleDataSet(schedule, uiScheduleDTO.getScheduleData());
        schedule.setScheduleDataByScheduleId(scheduleData);

        Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId = new ArrayList<>();

        final Collection<UiHolidayDTO> holidayDTOSet = uiScheduleDTO.getHolidayDTOSet();
        if (holidayDTOSet != null && !holidayDTOSet.isEmpty()) {
            holidayDTOSet.forEach(scheduledBlock ->
                    createAndSetScheduleHolidayMap(schedule, scheduleHolidayMapsByScheduleId, scheduledBlock));
            LOGGER.debug("Successfully set holidays {} for schedule {}",  StringUtils.join(holidayDTOSet.toArray(), "; "));
        }
        schedule.setScheduleHolidayMapsByScheduleId(scheduleHolidayMapsByScheduleId);

        return schedule;
    }

    private void createAndSetScheduleHolidayMap(Schedule schedule, Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId, UiHolidayDTO holiday) {
        LOGGER.debug("Adding holiday {} to schedule {}", holiday.toString());

        ScheduleHolidayMap scheduleHolidayMap = new ScheduleHolidayMap();

        scheduleHolidayMap.setHolidayOid(holiday.getHolidayId());
        scheduleHolidayMap.setScheduleOid(schedule.getScheduleId());
        scheduleHolidayMap.setScheduleByScheduleOid(schedule);

        LOGGER.debug("Successfully built schedule holiday {}", scheduleHolidayMap);
        scheduleHolidayMapsByScheduleId.add(scheduleHolidayMap);
    }

    private Set<ScheduleData> createScheduleDataSet(final Schedule schedule,
                                                    final Collection<UiScheduleDataDTO> uiScheduleDataDTOS) {
        LOGGER.debug("Creating schedule data for schedule {}, from UiSchedulaDataDTOS: {}", schedule.getName(), StringUtils.join(uiScheduleDataDTOS.toArray(), "; "));

        Set<ScheduleData> scheduleData = new HashSet<>();
        if (uiScheduleDataDTOS != null && uiScheduleDataDTOS.size() > 0) {

            UiScheduleDataDTO uiScheduleDataDTOSArray[] = new UiScheduleDataDTO[uiScheduleDataDTOS.size()];
            uiScheduleDataDTOSArray = uiScheduleDataDTOS.toArray(uiScheduleDataDTOSArray);

            Set<UiScheduleDataDTO> newUIScheduleData = new HashSet<>();


            Arrays.sort(uiScheduleDataDTOSArray, new UiScheduleDataDTOComparator());

            int i, iLength;
            for (i = 0, iLength = uiScheduleDataDTOSArray.length; i < iLength; i++) {
                UiScheduleDataDTO item = uiScheduleDataDTOSArray[i];
                long start = item.getStartTime();
                long end = item.getStopTime();

                while (i < iLength - 1 && (end + 1) >= uiScheduleDataDTOSArray[i + 1].getStartTime() && end < 8639 && start > 1439) {
                    end = uiScheduleDataDTOSArray[++i].getStopTime();
                }
                newUIScheduleData.add(new UiScheduleDataDTO(start, end));
            }

            newUIScheduleData.forEach(scheduledBlock ->

                    createAndSetScheduleBlocks(schedule, scheduleData, scheduledBlock));
        }

        LOGGER.debug("Successfully created schedule data {} for schedule {}", StringUtils.join(scheduleData.toArray(), "; "), schedule.getName());
        return scheduleData;
    }

    private void createAndSetScheduleBlocks(Schedule schedule, Set<ScheduleData> scheduleData, UiScheduleDataDTO
            scheduledBlock) {
        ScheduleData block = new ScheduleData();

        block.setStartTime(scheduledBlock.getStartTime());
        block.setStopTime(scheduledBlock.getStopTime());
        block.setScheduleId(schedule.getScheduleId());
        block.setScheduleByScheduleId(schedule);

        scheduleData.add(block);
    }

    private void validateNewSchedule(final UiScheduleDTO uiScheduleDTO) {
        validateSchedule(uiScheduleDTO);
    }

    private void validateExistingSchedule(final UiScheduleDTO uiScheduleDTO) {
        validateSchedule(uiScheduleDTO);
        validateScheduleId(uiScheduleDTO.getScheduleId());

    }

    private void validateSchedule(final UiScheduleDTO uiScheduleDTO) {
        final String name = uiScheduleDTO.getScheduleName();
        Assert.hasLength(name, "Schedule must have a name");

        validateSiteId(uiScheduleDTO);

        //TODO add other validations, if needed
    }

    private void validateSiteId(UiScheduleDTO uiScheduleDTO) {
        final long site_id = uiScheduleDTO.getSiteId();
        if (site_id != 0) {
            Optional<SecurityGroup> securityGroup = securityGroupRepository.findByObjectId(site_id);
            Assert.state(securityGroup.isPresent(),
                    "There is no security group with the id " + site_id);
            Assert.state(securityGroup.get().getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId() == SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID(),
                    "Security group type is not DEVICE");
        }
    }

    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public MessageDTO delete(List<Long> scheduleIds) {
        LOGGER.debug("Deleting schedules with ids {}...", StringUtils.join(scheduleIds.toArray(), "; "));
        final Optional<Schedule> schedule = scheduleRepository.findById(scheduleIds.get(0));

        scheduleIds.forEach(scheduleId -> {
            if (isValidScheduleId(scheduleId))
                uiScheduleDao.deleteScheduleById(scheduleId);
        });

        return new MessageDTO("Schedules with ids '" + scheduleIds + "' were successfully deleted from the database");
    }

    private void validateScheduleId(Long schedule_id) {
        Assert.state(scheduleRepository.findById(schedule_id).isPresent(),
                "There is no schedule with the id " + schedule_id);
    }

    private boolean isValidScheduleId(final Long schedule_id) {
        return scheduleRepository.findById(schedule_id).isPresent();
    }

    private static String[] scheduleStringToText(Collection<ScheduleData> scheduleData) {
        String schedData = scheduleDataToString(scheduleData);
        StringTokenizer st = new StringTokenizer(schedData, ",");
        ArrayList points = new ArrayList();

        // Convert scheduleData string to Integers
        while (st.hasMoreElements()) {
            try {
                points.add(Integer.valueOf(st.nextToken()));
            } catch (NumberFormatException nfe) {
                // nfe.printStackTrace();
            }
        }

        String[] week = new String[8];

        for (int i = 0; i < 8; i++) {
            week[i] = "";
        }

        // Fill in missing "points" and distribute into Array
        for (int i = 0; i < (points.size() - 1); i += 2) {
            int point_a = (Integer) points.get(i);
            int point_b = (Integer) points.get(i + 1);

            int day = (int) Math.floor(point_a / 1440);
            int midnight = (day + 1) * 1440;

            if (point_b > midnight) {
                point_b = midnight - 1;
                points.add(i + 1, Integer.valueOf(midnight));
                i--;
            }

            String t1 = schedulePointToText(point_a);
            String t2 = schedulePointToText(point_b);

            if (week[day].length() > 0) {
                week[day] += ", ";
            }

            week[day] += (t1.equals(t2) ? "All Day" : (t1 + "-" + t2));
        }

        return week;
    }

    private static Set<UiScheduleDataDTO> scheduleStringToDataBlock(Collection<ScheduleData> scheduleData) {
        String schedData = scheduleDataToString(scheduleData);
        StringTokenizer st = new StringTokenizer(schedData, ",");
        ArrayList points = new ArrayList();

        Set<UiScheduleDataDTO> scheduleDataDTOSet = new HashSet<>();

        // Convert scheduleData string to Integers
        while (st.hasMoreElements()) try {
            points.add(Integer.valueOf(st.nextToken()));
        } catch (NumberFormatException nfe) {
            LOGGER.error("Convert scheduleData string to Integers error");
        }

        // Fill in missing "points" and distribute into Array
        for (int i = 0; i < (points.size() - 1); i += 2) {
            int point_a = ((Integer) points.get(i)).intValue();
            int point_b = ((Integer) points.get(i + 1)).intValue();

            int day = (int) Math.floor(point_a / 1440);
            int midnight = (day + 1) * 1440;

            if (point_b > midnight) {
                point_b = midnight - 1;
                points.add(i + 1, Integer.valueOf(midnight));
                i--;
            }

            String t1 = schedulePointToText(point_a);
            String t2 = schedulePointToText(point_b);

            scheduleDataDTOSet.add(new UiScheduleDataDTO(day, WEEK_DAYS[day], t1, t2, point_a, point_b));

        }

        return scheduleDataDTOSet;
    }

    private static String scheduleDataToString(Collection<ScheduleData> scheduleData) {
        StringBuilder scheduleDataString = new StringBuilder("");

        if (scheduleData.size() > 0) {
            for (ScheduleData scheduleD : scheduleData) {
                scheduleDataString.append(
                        scheduleD.getStartTime());
                scheduleDataString.append(",");
                scheduleDataString.append(
                        scheduleD.getStopTime());
                scheduleDataString.append(",");
            }
        }

        return (scheduleDataString.length() > 0)
                ? scheduleDataString.toString()
                                    .substring(0, scheduleDataString.length() - 1)
                : scheduleDataString.toString();
    }

    private static String schedulePointToText(int point) {
        int h = (int) Math.floor((((double) point / 60) + 24) % 24);
        String m = String.valueOf(point % 60);

        // Translate a "point" into a time stamp, e.g. 500 = "8:20AM"
        return ((((h % 12) == 0) ? "12" : String.valueOf((h + 12) % 12)) + ":" +
                ((m.length() == 1) ? ("0" + m) : m) +
                (((h < 12) || (h == 24)) ? "AM" : "PM"));
    }


    private static class UiScheduleDataDTOComparator implements Comparator<UiScheduleDataDTO> {

        @Override
        public int compare(UiScheduleDataDTO o1, UiScheduleDataDTO o2) {
            int startO1 = Math.toIntExact(o1.getStartTime());
            int startO2 = Math.toIntExact(o2.getStartTime());
            return startO1 - startO2;
        }
    }

    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public MessageDTO update(UiScheduleDTO uiScheduleDTO) {
        LOGGER.debug("Updating schedule {}", uiScheduleDTO.getScheduleName());

        validateExistingSchedule(uiScheduleDTO);

        final Schedule existingSchedule = scheduleRepository.findById(uiScheduleDTO.getScheduleId()).get();

        uiScheduleDao.deleteScheduleDataAndHolidaysByScheduleId(existingSchedule.getScheduleId());

        Schedule updatedSchedule = updateSchedule(existingSchedule, uiScheduleDTO);

        final Schedule savedSchedule = scheduleRepository.save(updatedSchedule);

        LOGGER.debug("Schedule {} successfully updated...", savedSchedule.getName());

        return new MessageDTO("Schedule '" + uiScheduleDTO.getScheduleName() + "' was successfully created!");
    }

    private Schedule updateSchedule(final Schedule schedule, final UiScheduleDTO uiScheduleDTO) {

        schedule.setName(uiScheduleDTO.getScheduleName());
        schedule.setDescription(uiScheduleDTO.getDescription());

        schedule.setSiteId(uiScheduleDTO.getSiteId());
        if (uiScheduleDTO.getSiteId() != 0) {
            schedule.setEnablingGroupId(uiScheduleDTO.getGroupId());
            schedule.setEnablingGracePeriod(uiScheduleDTO.getGracePeriod());
        } else {
            schedule.setEnablingGroupId(null);
            schedule.setEnablingGracePeriod(null);
        }

        Set<ScheduleData> scheduleData = createScheduleDataSet(schedule, uiScheduleDTO.getScheduleData());
        schedule.setScheduleDataByScheduleId(scheduleData);

        Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId = new ArrayList<>();
        uiScheduleDTO.getHolidayDTOSet()
                     .forEach(scheduledBlock -> createAndSetScheduleHolidayMap(schedule, scheduleHolidayMapsByScheduleId, scheduledBlock));

        schedule.setScheduleHolidayMapsByScheduleId(scheduleHolidayMapsByScheduleId);

        schedule.setScheduleHolidayMapsByScheduleId(scheduleHolidayMapsByScheduleId);
        return schedule;
    }

    private static Set<UiHolidayDTO> getUiHolidayDTOS(final Iterable<Holiday> holidays) {
        return StreamSupport.stream(holidays.spliterator(), false)
                            .map(convertHolidayUI())
                            .collect(Collectors.toSet());
    }

    private static Function<Holiday, UiHolidayDTO> convertHolidayUI() {
        return holiday -> {
            String siteName = UNIVERSAL_SITE_NAME;
            long siteId = UNIVERSAL_SITE_ID;
            if (holiday.getSecurityGroupBySiteId() != null && holiday.getSiteId() != 0) {
                siteName = holiday.getSecurityGroupBySiteId().getName();
                siteId = holiday.getSiteId();
            }
            return new UiHolidayDTO(holiday.getHolidayId(), holiday.getName(), siteId, holiday.getStartDate(),
                    holiday.getStopDate(), holiday.getCadmHolidayId(),
                    holiday.getAccountId(), siteName);
        };
    }

    @Transactional
    public Optional<Schedule> findScheduleById(final long scheduleId) {
        return scheduleRepository.findById(scheduleId);
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Set<UiScheduleDTO> getUiScheduleByHolidayId(final Long holidayId) {
        Collection<Schedule> scheduleList;
        scheduleList = scheduleRepository.findByHolidayId(holidayId);
        return scheduleList.stream()
                           .filter(schedule -> schedule.getCadmScheduleId() != CADM_1 ||
                                   schedule.getCadmScheduleId() != CADM_255 ||
                                   schedule.getCadmScheduleId() != CADM_254)
                           .map(convertScheduleUI())
                           .collect(Collectors.toSet());
    }

    @Transactional
    public Collection<UiScheduleSummaryDTO> getUiScheduleSummaryDTOSForSiteWithNoAccessAndAlwaysAccess(final Long accountId, final Long siteObjectId) {
        List<UiScheduleSummaryDTO> sortedSchedules = (List<UiScheduleSummaryDTO>) getUiScheduleSummaryDTOSForSite(accountId, siteObjectId);

        Long scheduleAlwaysAccessId = scheduleRepository.getIdOfScheduleAlwaysAccess();
        sortedSchedules.add(0, new UiScheduleSummaryDTO(scheduleAlwaysAccessId, "Always access", "", 0L));
        sortedSchedules.add(0, NO_ACCESS_SCHEDULE);

        return sortedSchedules;
    }

    @Transactional
    public Collection<UiScheduleSummaryDTO> getUiScheduleSummaryDTOSForSite(final Long accountId, final Long siteObjectId) {
        Collection<UiScheduleSummaryDTO> schedules = scheduleRepository.getUiSummaryDTOsByTypeIdAndSiteId(accountId,
                siteObjectId, SCHEDULE_GEN_V_TYPE);

        schedules.forEach(schedule -> {
            if (schedule.getSiteId() == UNIVERSAL_SITE_ID)
                schedule.setSiteName(UNIVERSAL_SITE_NAME);
            final String updatedScheduleName = schedule.getSiteName() + " - " + schedule.getScheduleName();
            schedule.setScheduleName(updatedScheduleName);
        });

        return schedules;
    }

    @Transactional
    public Collection<UiScheduleSummaryDTO> getUiScheduleSummaryDTOSForSiteWithDefault(final Long accountId, final Long siteObjectId) {
        List<UiScheduleSummaryDTO> sortedSchedules = (List<UiScheduleSummaryDTO>) getUiScheduleSummaryDTOSForSite(accountId, siteObjectId);

        Schedule defaultSchedule = scheduleRepository.getDefaultSchedule();
        sortedSchedules.add(0, new UiScheduleSummaryDTO(defaultSchedule.getScheduleId(), defaultSchedule.getName(), "", defaultSchedule.getSiteId()));

        return sortedSchedules;
    }

    public Map<Long, Collection<UiScheduleSummaryDTO>> getAvailableSchedulesMapForAllSites(final Long accountId,
                                                                                           final Collection<UiSiteDTO> allSites) {
        Map<Long, Collection<UiScheduleSummaryDTO>> schedulesForSitesMap = new HashMap<>();

        allSites.forEach(uiSiteDTO -> {
            final Long siteObjectId = uiSiteDTO.getSiteObjectId();

            if (siteObjectId > 0) {
                Collection<UiScheduleSummaryDTO> schedulesForSite = getUiScheduleSummaryDTOSForSiteWithNoAccessAndAlwaysAccess(accountId, siteObjectId);
                schedulesForSitesMap.put(siteObjectId, schedulesForSite);
            }
        });

        return schedulesForSitesMap;
    }

    public Long getIdOfDefaultSchedule() {
        return scheduleRepository.getIdOfDefaultSchedule();
    }

    private int getIdOfWeekDay(String dayName) {
        for (int i = 0; i < WEEK_DAYS.length; i++) {
            if (WEEK_DAYS[i].toLowerCase().equals(dayName.toLowerCase())) {
                return i;
            }
        }

        return INCORRECT_WEEK_DAY;
    }

    public CucumberScheduleDataCollection convertScheduleBlocks(final String scheduleBlocksString) {
        CucumberScheduleDataCollection cucumberScheduleDataCollection = new CucumberScheduleDataCollection();
        Collection<UiScheduleDataDTO> scheduleDataDTOS = new ArrayList<>();

        try {
            if (scheduleBlocksString.isEmpty()) {
                LOGGER.debug("Schedule blocks are not set, collection will be empty");
                cucumberScheduleDataCollection = new CucumberScheduleDataCollection(scheduleDataDTOS, SUCCESS_MESSAGE);
                return cucumberScheduleDataCollection;

            } else {
                String[] scheduleBlockParts = scheduleBlocksString.split(",");
                LOGGER.debug("Successfully split scheduleBlocksString to {} blocks: {}", scheduleBlockParts.length, Arrays.toString(scheduleBlockParts));

                if (scheduleBlockParts != null && scheduleBlockParts.length > 0) {
                    for (String scheduleBlockPart : scheduleBlockParts) {
                        UiScheduleDataDTO uiScheduleDataDTO = new UiScheduleDataDTO();

                        if (!scheduleBlockPart.contains(" ")) {
                            LOGGER.error("The format of the schedule block {} is incorrect, there has to be a white space( ) between the day name and time", scheduleBlockPart);
                            return new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + "The format of the schedule block " + scheduleBlockPart + " is incorrect, there has to be a white space( ) between the day name and time");
                        }

                        String[] dayAndTime = scheduleBlockPart.split(" ");
                        final String dayName = dayAndTime[0];
                        LOGGER.debug("Day: {}, time: {}", dayName, dayAndTime[1]);

                        uiScheduleDataDTO.setDayName(dayName);
                        int dayId = getIdOfWeekDay(dayName);
                        if (dayId == INCORRECT_WEEK_DAY) {
                            LOGGER.error("The day {} is incorrect, couldn't be found in WEEK_DAYS list", dayName);
                            return new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + " the day -" + dayName + "- specified in the schedule block -" + scheduleBlockPart + "- is not correct");
                        }
                        uiScheduleDataDTO.setDayId(dayId);

                        String[] startAndEndTime = dayAndTime[1].split("-");
                        final String startTimeString = startAndEndTime[0];
                        final String stopTimeString = startAndEndTime[1];
                        uiScheduleDataDTO.setStart(startTimeString);
                        uiScheduleDataDTO.setStop(stopTimeString);

                        String timePattern = "[1-9][0-9]:[0-9][0-9][A|P]M";
                        if (!startTimeString.toUpperCase().matches(timePattern)) {
                            LOGGER.debug("There was an error while parsing start time {}, it does not match the needed pattern", startTimeString);
                            return  new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + " the time " + startTimeString + " does not match the pattern hh:miAM or hh:miPM");
                        }
                        if (!stopTimeString.toUpperCase().matches(timePattern)) {
                            LOGGER.debug("There was an error while parsing stop time {}, it does not match the needed pattern", stopTimeString);
                            return  new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + " the time " + stopTimeString + " does not match the pattern hh:miAM or hh:miPM");
                        }

                        Long startTime = convertScheduleBlockTimeStringToLong(startTimeString, dayId);
                        Long stopTime = convertScheduleBlockTimeStringToLong(stopTimeString, dayId);

                        uiScheduleDataDTO.setStartTime(startTime);
                        uiScheduleDataDTO.setStopTime(stopTime);

                        LOGGER.debug("Successfully computed uiScheduleDataDTO: {}", uiScheduleDataDTO.toString());
                        scheduleDataDTOS.add(uiScheduleDataDTO);
                    }
                } else {
                    LOGGER.error("There was an error parsing the schedule blocks string to blocks: the scheduleBlocksString is not empty, but the blocks are not separated by comma");
                    return new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + "The schedule blocks separator must be a comma(,), without any white spaces");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("There was an error while parsing the schedule blocks string {} received from cucumber test input: {}", scheduleBlocksString, ex.getMessage());
            return new CucumberScheduleDataCollection(Collections.emptyList(), ERROR_MESSAGE_PREFIX + ex.getMessage());
        }

        cucumberScheduleDataCollection.setUiScheduleDataDTOS(scheduleDataDTOS);
        cucumberScheduleDataCollection.setMessage(SUCCESS_MESSAGE);

        return cucumberScheduleDataCollection;
    }

    //timeString has the format 10:00AM, 12:00PM
    private Long convertScheduleBlockTimeStringToLong(final String timeString, int dayId) {
        LOGGER.debug("Converting time={} with dayId={} to minutes", timeString, dayId);
        final String amPmOption = timeString.substring(timeString.length() - 2).toUpperCase();
        LOGGER.debug("ampmOption: {} for timeString: {}", amPmOption, timeString);
        if (!amPmOption.equals("AM") && !amPmOption.equals("PM")) {
            LOGGER.error("The schedule block's time should end in either AM or PM");
        }

        final String hourAndMinuteString = timeString.substring(0, timeString.length() - 2);
        LOGGER.debug("hourAndMinuteString: {} for timeString: {}", hourAndMinuteString, timeString);
        String[] hourMinutes = hourAndMinuteString.split(":");

        int hourOfDay = Integer.parseInt(hourMinutes[0]);
        int minutes = Integer.parseInt(hourMinutes[1]);

        if (amPmOption.equals("PM")) {
            hourOfDay += 12;
        }

        final int totalHoursNumber = (24 * dayId) + hourOfDay;
        Long scheduleBlockTime = Long.valueOf(totalHoursNumber * 60 + minutes);

        LOGGER.debug("Time in minutes for time {}, dayId {} = {}", timeString, dayId, scheduleBlockTime);

        return scheduleBlockTime;
    }
}
