package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "video_provider_type_property", schema = "brivo20", catalog = "onair")
public class VideoProviderTypeProperty {
    private long videoProvTypePropId;
    private String defaultValue;
    private long videoProviderTypeId;
    private Short oneTimeEditable;
    private Short forSetup;
    private Short displayable;
    private Long valueTypeId;
    private Long minimumValue;
    private Long maximumValue;
    private String regexPattern;
    private Short isRequired;
    private String name;
    private long validationTypeId;
    private Collection<VideoProviderPropertyValue> videoProviderPropertyValuesByVideoProvTypePropId;
    private VideoProviderType videoProviderTypeByVideoProviderTypeId;
    private ValueType valueTypeByValueTypeId;
    private ValidationType validationTypeByValidationTypeId;

    @Id
    @Column(name = "video_prov_type_prop_id", nullable = false)
    public long getVideoProvTypePropId() {
        return videoProvTypePropId;
    }

    public void setVideoProvTypePropId(long videoProvTypePropId) {
        this.videoProvTypePropId = videoProvTypePropId;
    }

    @Basic
    @Column(name = "default_value", nullable = true, length = 512)
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Basic
    @Column(name = "video_provider_type_id", nullable = false)
    public long getVideoProviderTypeId() {
        return videoProviderTypeId;
    }

    public void setVideoProviderTypeId(long videoProviderTypeId) {
        this.videoProviderTypeId = videoProviderTypeId;
    }

    @Basic
    @Column(name = "one_time_editable", nullable = true)
    public Short getOneTimeEditable() {
        return oneTimeEditable;
    }

    public void setOneTimeEditable(Short oneTimeEditable) {
        this.oneTimeEditable = oneTimeEditable;
    }

    @Basic
    @Column(name = "for_setup", nullable = true)
    public Short getForSetup() {
        return forSetup;
    }

    public void setForSetup(Short forSetup) {
        this.forSetup = forSetup;
    }

    @Basic
    @Column(name = "displayable", nullable = true)
    public Short getDisplayable() {
        return displayable;
    }

    public void setDisplayable(Short displayable) {
        this.displayable = displayable;
    }

    @Basic
    @Column(name = "value_type_id", nullable = true)
    public Long getValueTypeId() {
        return valueTypeId;
    }

    public void setValueTypeId(Long valueTypeId) {
        this.valueTypeId = valueTypeId;
    }

    @Basic
    @Column(name = "minimum_value", nullable = true)
    public Long getMinimumValue() {
        return minimumValue;
    }

    public void setMinimumValue(Long minimumValue) {
        this.minimumValue = minimumValue;
    }

    @Basic
    @Column(name = "maximum_value", nullable = true)
    public Long getMaximumValue() {
        return maximumValue;
    }

    public void setMaximumValue(Long maximumValue) {
        this.maximumValue = maximumValue;
    }

    @Basic
    @Column(name = "regex_pattern", nullable = true, length = 512)
    public String getRegexPattern() {
        return regexPattern;
    }

    public void setRegexPattern(String regexPattern) {
        this.regexPattern = regexPattern;
    }

    @Basic
    @Column(name = "is_required", nullable = true)
    public Short getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Short isRequired) {
        this.isRequired = isRequired;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "validation_type_id", nullable = false)
    public long getValidationTypeId() {
        return validationTypeId;
    }

    public void setValidationTypeId(long validationTypeId) {
        this.validationTypeId = validationTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoProviderTypeProperty that = (VideoProviderTypeProperty) o;

        if (videoProvTypePropId != that.videoProvTypePropId) {
            return false;
        }
        if (videoProviderTypeId != that.videoProviderTypeId) {
            return false;
        }
        if (validationTypeId != that.validationTypeId) {
            return false;
        }
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null) {
            return false;
        }
        if (oneTimeEditable != null ? !oneTimeEditable.equals(that.oneTimeEditable) : that.oneTimeEditable != null) {
            return false;
        }
        if (forSetup != null ? !forSetup.equals(that.forSetup) : that.forSetup != null) {
            return false;
        }
        if (displayable != null ? !displayable.equals(that.displayable) : that.displayable != null) {
            return false;
        }
        if (valueTypeId != null ? !valueTypeId.equals(that.valueTypeId) : that.valueTypeId != null) {
            return false;
        }
        if (minimumValue != null ? !minimumValue.equals(that.minimumValue) : that.minimumValue != null) {
            return false;
        }
        if (maximumValue != null ? !maximumValue.equals(that.maximumValue) : that.maximumValue != null) {
            return false;
        }
        if (regexPattern != null ? !regexPattern.equals(that.regexPattern) : that.regexPattern != null) {
            return false;
        }
        if (isRequired != null ? !isRequired.equals(that.isRequired) : that.isRequired != null) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoProvTypePropId ^ (videoProvTypePropId >>> 32));
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (int) (videoProviderTypeId ^ (videoProviderTypeId >>> 32));
        result = 31 * result + (oneTimeEditable != null ? oneTimeEditable.hashCode() : 0);
        result = 31 * result + (forSetup != null ? forSetup.hashCode() : 0);
        result = 31 * result + (displayable != null ? displayable.hashCode() : 0);
        result = 31 * result + (valueTypeId != null ? valueTypeId.hashCode() : 0);
        result = 31 * result + (minimumValue != null ? minimumValue.hashCode() : 0);
        result = 31 * result + (maximumValue != null ? maximumValue.hashCode() : 0);
        result = 31 * result + (regexPattern != null ? regexPattern.hashCode() : 0);
        result = 31 * result + (isRequired != null ? isRequired.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (validationTypeId ^ (validationTypeId >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "videoProviderTypePropertyByVideoProvTypePropId")
    public Collection<VideoProviderPropertyValue> getVideoProviderPropertyValuesByVideoProvTypePropId() {
        return videoProviderPropertyValuesByVideoProvTypePropId;
    }

    public void setVideoProviderPropertyValuesByVideoProvTypePropId(Collection<VideoProviderPropertyValue> videoProviderPropertyValuesByVideoProvTypePropId) {
        this.videoProviderPropertyValuesByVideoProvTypePropId = videoProviderPropertyValuesByVideoProvTypePropId;
    }

    @ManyToOne
    @JoinColumn(name = "video_provider_type_id", referencedColumnName = "video_provider_type_id", nullable = false, insertable = false, updatable = false)
    public VideoProviderType getVideoProviderTypeByVideoProviderTypeId() {
        return videoProviderTypeByVideoProviderTypeId;
    }

    public void setVideoProviderTypeByVideoProviderTypeId(VideoProviderType videoProviderTypeByVideoProviderTypeId) {
        this.videoProviderTypeByVideoProviderTypeId = videoProviderTypeByVideoProviderTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "value_type_id", referencedColumnName = "value_type_id", insertable = false, updatable = false)
    public ValueType getValueTypeByValueTypeId() {
        return valueTypeByValueTypeId;
    }

    public void setValueTypeByValueTypeId(ValueType valueTypeByValueTypeId) {
        this.valueTypeByValueTypeId = valueTypeByValueTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "validation_type_id", referencedColumnName = "validation_type_id", nullable = false, insertable = false, updatable = false)
    public ValidationType getValidationTypeByValidationTypeId() {
        return validationTypeByValidationTypeId;
    }

    public void setValidationTypeByValidationTypeId(ValidationType validationTypeByValidationTypeId) {
        this.validationTypeByValidationTypeId = validationTypeByValidationTypeId;
    }
}
