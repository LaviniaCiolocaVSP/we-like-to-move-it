package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "credential_encoding", schema = "brivo20", catalog = "onair")
public class CredentialEncoding {
    private long credentialEncodingId;
    private String name;
    private Collection<AccessCredentialType> accessCredentialTypesByCredentialEncodingId;

    @Id
    @Column(name = "credential_encoding_id", nullable = false)
    public long getCredentialEncodingId() {
        return credentialEncodingId;
    }

    public void setCredentialEncodingId(long credentialEncodingId) {
        this.credentialEncodingId = credentialEncodingId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialEncoding that = (CredentialEncoding) o;

        if (credentialEncodingId != that.credentialEncodingId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (credentialEncodingId ^ (credentialEncodingId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "credentialEncodingByCredentialEncodingId")
    public Collection<AccessCredentialType> getAccessCredentialTypesByCredentialEncodingId() {
        return accessCredentialTypesByCredentialEncodingId;
    }

    public void setAccessCredentialTypesByCredentialEncodingId(Collection<AccessCredentialType> accessCredentialTypesByCredentialEncodingId) {
        this.accessCredentialTypesByCredentialEncodingId = accessCredentialTypesByCredentialEncodingId;
    }
}
