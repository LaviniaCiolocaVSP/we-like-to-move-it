package com.brivo.panel.server.reference.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;
import java.util.stream.Stream;

public enum ExecuteCommandType implements Serializable {
    REBOOT_PANEL(1),
    RESET_PANEL_DATA(2),
    RESET_CAN_BUS(3);

    private int executeCommandTypeKey;

    ExecuteCommandType(int executeCommandTypeKey) {
        this.executeCommandTypeKey = executeCommandTypeKey;
    }

    @JsonCreator
    public static ExecuteCommandType getExecuteCommandTypeByKey(Integer key) {
        return Stream.of(ExecuteCommandType.values())
                .filter(executeCommandType -> executeCommandType.executeCommandTypeKey == key)
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public Integer getExecuteCommandTypeKey() {
        return this.executeCommandTypeKey;
    }
}
