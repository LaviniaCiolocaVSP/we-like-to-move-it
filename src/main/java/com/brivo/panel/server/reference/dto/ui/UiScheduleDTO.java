package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Set;

public class UiScheduleDTO implements Serializable {
    private long scheduleId;

    private String scheduleName;
    private String description;
    private long scheduleTypeId;
    private String scheduleType;
    private long accountId;
    private long cadmScheduleId;
    private long siteId;
    private String siteName;
    private long groupId;
    private String groupName;
    private long gracePeriod;

    private Collection<UiScheduleDataDTO> scheduleData;
    private String[] scheduleDataArray;
    private Collection<UiHolidayDTO> holidayDTOSet;


    public UiScheduleDTO() {

    }

    public UiScheduleDTO(long scheduleId, String scheduleName, String description, long scheduleTypeId,
                         long accountId, long cadmScheduleId, long siteId, String siteName) {
        this.scheduleId = scheduleId;
        this.scheduleName = scheduleName;
        this.description = description;
        this.scheduleTypeId = scheduleTypeId;
        this.accountId = accountId;
        this.cadmScheduleId = cadmScheduleId;
        this.siteId = siteId;
        this.siteName = siteName;
    }

    public UiScheduleDTO(long scheduleId, String scheduleName, String description, long scheduleTypeId,
                         long accountId, long cadmScheduleId, long siteId, String siteName, long groupId, String groupName, long gracePeriod,
                         Collection<UiScheduleDataDTO> scheduleData, String[] scheduleDataArray, Collection<UiHolidayDTO> holidayDTOSet) {
        this.scheduleId = scheduleId;
        this.scheduleName = scheduleName;
        this.description = description;
        this.scheduleTypeId = scheduleTypeId;
        this.accountId = accountId;
        this.cadmScheduleId = cadmScheduleId;
        this.siteId = siteId;
        this.siteName = siteName;
        this.scheduleData = scheduleData;
        this.scheduleDataArray = scheduleDataArray;
        this.groupId = groupId;
        this.groupName = groupName;
        this.gracePeriod = gracePeriod;
        this.holidayDTOSet = holidayDTOSet;
    }

    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getCadmScheduleId() {
        return cadmScheduleId;
    }

    public void setCadmScheduleId(long cadmScheduleId) {
        this.cadmScheduleId = cadmScheduleId;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public String getScheduleType() {
        return scheduleType;
    }

    public void setScheduleType(String scheduleType) {
        this.scheduleType = scheduleType;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(long gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public Collection<UiScheduleDataDTO> getScheduleData() {
        return scheduleData;
    }

    public void setScheduleData(Collection<UiScheduleDataDTO> scheduleData) {
        this.scheduleData = scheduleData;
    }

    public String[] getScheduleDataArray() {
        return scheduleDataArray;
    }

    public void setScheduleDataArray(String[] scheduleDataArray) {
        this.scheduleDataArray = scheduleDataArray;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Collection<UiHolidayDTO> getHolidayDTOSet() {
        return holidayDTOSet;
    }

    public void setHolidayDTOSet(Collection<UiHolidayDTO> holidayDTOSet) {
        this.holidayDTOSet = holidayDTOSet;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Override
    public String toString() {
        return "UiScheduleDTO{" +
                "scheduleId=" + scheduleId +
                ", scheduleName='" + scheduleName + '\'' +
                ", scheduleTypeId=" + scheduleTypeId +
                ", accountId=" + accountId +
                ", cadmScheduleId=" + cadmScheduleId +
                ", siteId=" + siteId +
                ", groupId=" + groupId +
                ", gracePeriod=" + gracePeriod +
                ", scheduleData=" + scheduleData +
                ", scheduleDataArray=" + Arrays.toString(scheduleDataArray) +
                '}';
    }
}
