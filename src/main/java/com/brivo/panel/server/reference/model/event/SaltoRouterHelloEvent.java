package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class SaltoRouterHelloEvent extends Event {
    private Long deviceId;
    private Integer firmwareNumber;
    private Integer firmwareVersion;
    private Instant manufactureTime;

    @JsonCreator
    public SaltoRouterHelloEvent(@JsonProperty("eventType") final EventType eventType,
                                 @JsonProperty("eventTime") final Instant eventTime,
                                 @JsonProperty("deviceId") final Long deviceId,
                                 @JsonProperty("firmwareNumber") final Integer firmwareNumber,
                                 @JsonProperty("firmwareVersion") final Integer firmwareVersion,
                                 @JsonProperty("manufactureTime") final Instant manufactureTime) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.firmwareNumber = firmwareNumber;
        this.firmwareVersion = firmwareVersion;
        this.manufactureTime = manufactureTime;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getFirmwareNumber() {
        return firmwareNumber;
    }

    public void setFirmwareNumber(Integer firmwareNumber) {
        this.firmwareNumber = firmwareNumber;
    }

    public Integer getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(Integer firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public Instant getManufactureTime() {
        return manufactureTime;
    }

    public void setManufactureTime(Instant manufactureTime) {
        this.manufactureTime = manufactureTime;
    }
}
