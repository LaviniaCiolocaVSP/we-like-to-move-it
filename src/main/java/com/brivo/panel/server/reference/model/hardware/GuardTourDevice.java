package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class GuardTourDevice {
    private Long deviceId;
    private List<ProgrammableDeviceOutput> outputs;
    private List<EnterPermission> enterPermissions;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public List<ProgrammableDeviceOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<ProgrammableDeviceOutput> outputs) {
        this.outputs = outputs;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }
}
