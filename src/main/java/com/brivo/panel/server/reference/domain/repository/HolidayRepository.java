package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Holiday;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface HolidayRepository extends CrudRepository<Holiday, Long> {

    List<Holiday> findByAccountId(@Param("accountId") long accountId);

    @Query(
            value = "SELECT coalesce(max(h.holiday_id),0) FROM brivo20.holiday h",
            nativeQuery = true
    )
    Optional<Long> getMaxHolidayId();

    @Query(
            value = "SELECT coalesce(max(h.cadm_holiday_id),0) FROM brivo20.holiday h",
            nativeQuery = true
    )
    Optional<Long> getMaxCadmHolidayId();

    @Query(
            value = "SELECT h.* FROM brivo20.holiday h WHERE h.holiday_id = :holidayId",
            nativeQuery = true
    )
    Optional<Holiday> findByHolidayId(@Param("holidayId") long holidayId);

    @Query(
            value = "select count(*) from holiday h where h.account_id = :account_id",
            nativeQuery = true
    )
    long getHolidayCountForAccount(@Param("account_id") long accountId);
}
