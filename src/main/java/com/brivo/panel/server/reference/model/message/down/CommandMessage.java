package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class CommandMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private String messageName;

    public CommandMessage() {
        super(DownstreamMessageType.Request_Panel_Log);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public String getMessageName() {
        return messageName;
    }

    public void setMessageName(String messageName) {
        this.messageName = messageName;
    }
}
