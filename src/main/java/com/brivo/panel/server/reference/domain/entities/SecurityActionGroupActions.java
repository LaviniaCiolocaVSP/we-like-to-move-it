package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "security_action_group_actions", schema = "brivo20", catalog = "onair")
public class SecurityActionGroupActions {
    private long actionGroupActionsId;
    private long securityActionGroupId;
    private long securityActionId;
    private SecurityActionGroup securityActionGroupBySecurityActionGroupId;
    private SecurityAction securityActionBySecurityActionId;

    @Id
    @Column(name = "action_group_actions_id", nullable = false)
    public long getActionGroupActionsId() {
        return actionGroupActionsId;
    }

    public void setActionGroupActionsId(long actionGroupActionsId) {
        this.actionGroupActionsId = actionGroupActionsId;
    }

    @Basic
    @Column(name = "security_action_group_id", nullable = false)
    public long getSecurityActionGroupId() {
        return securityActionGroupId;
    }

    public void setSecurityActionGroupId(long securityActionGroupId) {
        this.securityActionGroupId = securityActionGroupId;
    }

    @Basic
    @Column(name = "security_action_id", nullable = false)
    public long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityActionGroupActions that = (SecurityActionGroupActions) o;

        if (actionGroupActionsId != that.actionGroupActionsId) {
            return false;
        }
        if (securityActionGroupId != that.securityActionGroupId) {
            return false;
        }
        return securityActionId == that.securityActionId;
    }

    @Override
    public int hashCode() {
        int result = (int) (actionGroupActionsId ^ (actionGroupActionsId >>> 32));
        result = 31 * result + (int) (securityActionGroupId ^ (securityActionGroupId >>> 32));
        result = 31 * result + (int) (securityActionId ^ (securityActionId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "security_action_group_id", referencedColumnName = "security_action_group_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityActionGroup getSecurityActionGroupBySecurityActionGroupId() {
        return securityActionGroupBySecurityActionGroupId;
    }

    public void setSecurityActionGroupBySecurityActionGroupId(SecurityActionGroup securityActionGroupBySecurityActionGroupId) {
        this.securityActionGroupBySecurityActionGroupId = securityActionGroupBySecurityActionGroupId;
    }

    @ManyToOne
    @JoinColumn(name = "security_action_id", referencedColumnName = "security_action_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityAction getSecurityActionBySecurityActionId() {
        return securityActionBySecurityActionId;
    }

    public void setSecurityActionBySecurityActionId(SecurityAction securityActionBySecurityActionId) {
        this.securityActionBySecurityActionId = securityActionBySecurityActionId;
    }
}
