package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.ISecurityGroupAntipassbackRepository;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupAntipassback;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SecurityGroupAntipassbackRepository extends ISecurityGroupAntipassbackRepository {

    Optional<SecurityGroupAntipassback> findBySecurityGroupId(@Param("securityGroupId") long securityGroupId);
}
