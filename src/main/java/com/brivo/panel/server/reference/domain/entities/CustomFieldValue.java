package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigInteger;
import java.sql.Timestamp;

@Entity
@Table(name = "custom_field_value", schema = "brivo20", catalog = "onair")
@IdClass(CustomFieldValuePK.class)
public class CustomFieldValue {
    private long userId;
    private long customFieldId;
    private Long enumId;
    private String textValue;
    private BigInteger numericValue;
    private Timestamp dateValue;
    //private Collection<CustomFieldDefinition> customFieldValueByCustomFieldId;
    private EnumeratedField enumeratedFieldByEnumId;

    @Id
    @Column(name = "user_id", nullable = false)
    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Id
    @Column(name = "custom_field_id", nullable = false)
    public long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Basic
    @Column(name = "enum_id", nullable = true)
    public Long getEnumId() {
        return enumId;
    }

    public void setEnumId(Long enumId) {
        this.enumId = enumId;
    }

    @Basic
    @Column(name = "text_value", nullable = true, length = 128)
    public String getTextValue() {
        return textValue;
    }

    public void setTextValue(String textValue) {
        this.textValue = textValue;
    }

    @Basic
    @Column(name = "numeric_value", nullable = true, precision = 0)
    public BigInteger getNumericValue() {
        return numericValue;
    }

    public void setNumericValue(BigInteger numericValue) {
        this.numericValue = numericValue;
    }

    @Basic
    @Column(name = "date_value", nullable = true)
    public Timestamp getDateValue() {
        return dateValue;
    }

    public void setDateValue(Timestamp dateValue) {
        this.dateValue = dateValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomFieldValue that = (CustomFieldValue) o;

        if (userId != that.userId) {
            return false;
        }
        if (customFieldId != that.customFieldId) {
            return false;
        }
        if (enumId != null ? !enumId.equals(that.enumId) : that.enumId != null) {
            return false;
        }
        if (textValue != null ? !textValue.equals(that.textValue) : that.textValue != null) {
            return false;
        }
        if (numericValue != null ? !numericValue.equals(that.numericValue) : that.numericValue != null) {
            return false;
        }
        return dateValue != null ? dateValue.equals(that.dateValue) : that.dateValue == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + (int) (customFieldId ^ (customFieldId >>> 32));
        result = 31 * result + (enumId != null ? enumId.hashCode() : 0);
        result = 31 * result + (textValue != null ? textValue.hashCode() : 0);
        result = 31 * result + (numericValue != null ? numericValue.hashCode() : 0);
        result = 31 * result + (dateValue != null ? dateValue.hashCode() : 0);
        return result;
    }

    /*
    @OneToMany(mappedBy = "customFieldValueByCustomFieldId")
    public Collection<CustomFieldDefinition> getCustomFieldValueByCustomFieldId() {
        return customFieldValueByCustomFieldId;
    }

    public void setCustomFieldValueByCustomFieldId(Collection<CustomFieldDefinition> customFieldValueByCustomFieldId) {
        this.customFieldValueByCustomFieldId = customFieldValueByCustomFieldId;
    }
    */

    @ManyToOne
    @JoinColumn(name = "enum_id", referencedColumnName = "enum_id", insertable = false, updatable = false)
    public EnumeratedField getEnumeratedFieldByEnumId() {
        return enumeratedFieldByEnumId;
    }

    public void setEnumeratedFieldByEnumId(EnumeratedField enumeratedFieldByEnumId) {
        this.enumeratedFieldByEnumId = enumeratedFieldByEnumId;
    }
}
