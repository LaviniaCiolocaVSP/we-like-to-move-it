package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "security_policy", schema = "brivo20", catalog = "onair")
public class SecurityPolicy {
    private long policyId;
    private String name;
    private String strategyClass;
    private String loginUrl;
    private Collection<SecurityPolicyAccount> securityPolicyAccountsByPolicyId;
    private Collection<SecurityPolicyParam> securityPolicyParamsByPolicyId;

    @Id
    @Column(name = "policy_id", nullable = false)
    public long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "strategy_class", nullable = false, length = 256)
    public String getStrategyClass() {
        return strategyClass;
    }

    public void setStrategyClass(String strategyClass) {
        this.strategyClass = strategyClass;
    }

    @Basic
    @Column(name = "login_url", nullable = true, length = 256)
    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityPolicy that = (SecurityPolicy) o;

        if (policyId != that.policyId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (strategyClass != null ? !strategyClass.equals(that.strategyClass) : that.strategyClass != null) {
            return false;
        }
        return loginUrl != null ? loginUrl.equals(that.loginUrl) : that.loginUrl == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (policyId ^ (policyId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (strategyClass != null ? strategyClass.hashCode() : 0);
        result = 31 * result + (loginUrl != null ? loginUrl.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "securityPolicyByPolicyId")
    public Collection<SecurityPolicyAccount> getSecurityPolicyAccountsByPolicyId() {
        return securityPolicyAccountsByPolicyId;
    }

    public void setSecurityPolicyAccountsByPolicyId(Collection<SecurityPolicyAccount> securityPolicyAccountsByPolicyId) {
        this.securityPolicyAccountsByPolicyId = securityPolicyAccountsByPolicyId;
    }

    @OneToMany(mappedBy = "securityPolicyByPolicyId")
    public Collection<SecurityPolicyParam> getSecurityPolicyParamsByPolicyId() {
        return securityPolicyParamsByPolicyId;
    }

    public void setSecurityPolicyParamsByPolicyId(Collection<SecurityPolicyParam> securityPolicyParamsByPolicyId) {
        this.securityPolicyParamsByPolicyId = securityPolicyParamsByPolicyId;
    }
}
