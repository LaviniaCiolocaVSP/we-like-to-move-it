package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum ScheduleQuery implements FileQuery {
    SELECT_ACCOUNT_SCHEDULES,
    SELECT_ACCOUNT_HOLIDAYS,
    SELECT_SCHEDULE_BLOCKS,
    SELECT_SCHEDULE_EXCEPTION_BLOCKS,
    SELECT_SCHEDULE_HOLIDAYS;

    private String query;

    ScheduleQuery() {
        this.query = QueryUtils.getQueryText(ScheduleQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
