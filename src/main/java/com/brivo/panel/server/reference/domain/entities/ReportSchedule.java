package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "report_schedule", schema = "brivo20", catalog = "onair")
public class ReportSchedule {
    private long id;
    private long objectId;
    private long reportConfigurationId;
    private String cronSchedule;
    private long userObjectId;
    private Timestamp effective;
    private Timestamp expires;
    private String name;
    private long emailOptionId;
    private long languageId;
    private short deleted;
    private Timestamp created;
    private Timestamp updated;
    private String timeZone;
    private Collection<ReportJob> reportJobsById;
    private BrivoObject objectByBrivoObjectId;
    private ReportConfiguration reportConfigurationByReportConfigurationId;
    private BrivoObject objectByUserBrivoObjectId;
    private ReportEmailOption reportEmailOptionByEmailOptionId;
    private Collection<ReportScheduleFormat> reportScheduleFormatsById;
    private Collection<ReportScheduleRecipient> reportScheduleRecipientsById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "report_configuration_id", nullable = false)
    public long getReportConfigurationId() {
        return reportConfigurationId;
    }

    public void setReportConfigurationId(long reportConfigurationId) {
        this.reportConfigurationId = reportConfigurationId;
    }

    @Basic
    @Column(name = "cron_schedule", nullable = false, length = 100)
    public String getCronSchedule() {
        return cronSchedule;
    }

    public void setCronSchedule(String cronSchedule) {
        this.cronSchedule = cronSchedule;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "effective", nullable = false)
    public Timestamp getEffective() {
        return effective;
    }

    public void setEffective(Timestamp effective) {
        this.effective = effective;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "email_option_id", nullable = false)
    public long getEmailOptionId() {
        return emailOptionId;
    }

    public void setEmailOptionId(long emailOptionId) {
        this.emailOptionId = emailOptionId;
    }

    @Basic
    @Column(name = "language_id", nullable = false)
    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportSchedule that = (ReportSchedule) o;

        if (id != that.id) {
            return false;
        }
        if (objectId != that.objectId) {
            return false;
        }
        if (reportConfigurationId != that.reportConfigurationId) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        if (emailOptionId != that.emailOptionId) {
            return false;
        }
        if (languageId != that.languageId) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (cronSchedule != null ? !cronSchedule.equals(that.cronSchedule) : that.cronSchedule != null) {
            return false;
        }
        if (effective != null ? !effective.equals(that.effective) : that.effective != null) {
            return false;
        }
        if (expires != null ? !expires.equals(that.expires) : that.expires != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        return timeZone != null ? timeZone.equals(that.timeZone) : that.timeZone == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (reportConfigurationId ^ (reportConfigurationId >>> 32));
        result = 31 * result + (cronSchedule != null ? cronSchedule.hashCode() : 0);
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (effective != null ? effective.hashCode() : 0);
        result = 31 * result + (expires != null ? expires.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (emailOptionId ^ (emailOptionId >>> 32));
        result = 31 * result + (int) (languageId ^ (languageId >>> 32));
        result = 31 * result + (int) deleted;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (timeZone != null ? timeZone.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportScheduleByReportScheduleId")
    public Collection<ReportJob> getReportJobsById() {
        return reportJobsById;
    }

    public void setReportJobsById(Collection<ReportJob> reportJobsById) {
        this.reportJobsById = reportJobsById;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "report_configuration_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportConfiguration getReportConfigurationByReportConfigurationId() {
        return reportConfigurationByReportConfigurationId;
    }

    public void setReportConfigurationByReportConfigurationId(ReportConfiguration reportConfigurationByReportConfigurationId) {
        this.reportConfigurationByReportConfigurationId = reportConfigurationByReportConfigurationId;
    }

    @ManyToOne
    @JoinColumn(name = "user_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByUserBrivoObjectId() {
        return objectByUserBrivoObjectId;
    }

    public void setObjectByUserBrivoObjectId(BrivoObject objectByUserBrivoObjectId) {
        this.objectByUserBrivoObjectId = objectByUserBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "email_option_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportEmailOption getReportEmailOptionByEmailOptionId() {
        return reportEmailOptionByEmailOptionId;
    }

    public void setReportEmailOptionByEmailOptionId(ReportEmailOption reportEmailOptionByEmailOptionId) {
        this.reportEmailOptionByEmailOptionId = reportEmailOptionByEmailOptionId;
    }

    @OneToMany(mappedBy = "reportScheduleByReportScheduleId")
    public Collection<ReportScheduleFormat> getReportScheduleFormatsById() {
        return reportScheduleFormatsById;
    }

    public void setReportScheduleFormatsById(Collection<ReportScheduleFormat> reportScheduleFormatsById) {
        this.reportScheduleFormatsById = reportScheduleFormatsById;
    }

    @OneToMany(mappedBy = "reportScheduleByReportScheduleId")
    public Collection<ReportScheduleRecipient> getReportScheduleRecipientsById() {
        return reportScheduleRecipientsById;
    }

    public void setReportScheduleRecipientsById(Collection<ReportScheduleRecipient> reportScheduleRecipientsById) {
        this.reportScheduleRecipientsById = reportScheduleRecipientsById;
    }
}
