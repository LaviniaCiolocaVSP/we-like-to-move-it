package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Collection;

public class ThreatLevelProgrammableDeviceOutput extends ProgrammableDeviceOutput {
    Collection<Long> onDeviceIds;

    public ThreatLevelProgrammableDeviceOutput() {
        super();
    }

    public ThreatLevelProgrammableDeviceOutput(ProgrammableDeviceOutput programmableDeviceOutput) {
        super(programmableDeviceOutput.getBoardAddress(), programmableDeviceOutput.getPointAddress(), programmableDeviceOutput.getBehavior(),
                programmableDeviceOutput.getBehaviorArgument(), programmableDeviceOutput.getOutputType(), programmableDeviceOutput.getObjectId());
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Collection<Long> getOnDeviceIds() {
        return onDeviceIds;
    }

    public void setOnDeviceIds(Collection<Long> onDeviceIds) {
        this.onDeviceIds = onDeviceIds;
    }
}
