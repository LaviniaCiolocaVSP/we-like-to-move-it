package com.brivo.panel.server.reference.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class CloudService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CloudService.class);

    @Value("${upload-folder.root}") //TODO replace with the Spring Boot temp upload folder
    private String uploadFolder;

    private Path uploadFolderPath;

    @PostConstruct
    public void init() {
        uploadFolderPath = Paths.get(uploadFolder);
        try {
            if (uploadFolderPath.toFile().exists()) {
                return;
            }
            Files.createDirectory(uploadFolderPath);
        } catch (IOException e) {
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public String upload(final MultipartFile file) {
        final String fileName = file.getOriginalFilename();
        try {
            Files.copy(file.getInputStream(), uploadFolderPath.resolve(fileName));
            return "You have successfully uploaded " + fileName + "!";
        } catch (final Exception e) {
            LOGGER.error(e.getMessage(), e);
            throw new IllegalStateException("There was an error while uploading the file '" + fileName + "'");
        }
    }

    public Resource loadFile(final String filename) {
        try {
            Path file = uploadFolderPath.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteAll() {
        FileSystemUtils.deleteRecursively(uploadFolderPath.toFile());
    }
}
