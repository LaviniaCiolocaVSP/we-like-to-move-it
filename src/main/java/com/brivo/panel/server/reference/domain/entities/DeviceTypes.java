package com.brivo.panel.server.reference.domain.entities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum DeviceTypes {
    DOOR_KEYPAD(Long.valueOf(1)),
    AUX_RELAY_DEVICE(Long.valueOf(2)),
    TIMER(Long.valueOf(3)),
    WIEGAND(Long.valueOf(4)),
    SWITCH(Long.valueOf(5)),
    EVENT(Long.valueOf(6)),
    FLOOR(Long.valueOf(7)),
    ELEVATOR(Long.valueOf(8)),
    SALTO_LOCK(Long.valueOf(9)),
    IPAC(Long.valueOf(10)),
    ALLEGION_LOCK(Long.valueOf(11));

    private static final Map<Long, DeviceTypes> BY_TYPE_ID;

    static {
        final Map<Long, DeviceTypes> byTypeID = new HashMap<>();
        for (DeviceTypes ot : DeviceTypes.values()) {
            byTypeID.put(ot.deviceTypeId, ot);
        }

        BY_TYPE_ID = Collections.unmodifiableMap(byTypeID);
    }

    private Long deviceTypeId;

    DeviceTypes(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public static DeviceTypes valueOf(Long objectTypeID) {
        final DeviceTypes ot = BY_TYPE_ID.get(objectTypeID);
        return ot;
    }

    public Long getDeviceTypeID() {
        return deviceTypeId;
    }
}

