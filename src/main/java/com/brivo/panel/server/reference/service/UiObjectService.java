package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.repository.BrivoObjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UiObjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UiObjectService.class);

    private final BrivoObjectRepository objectRepository;

    @Autowired
    public UiObjectService(final BrivoObjectRepository objectRepository) {
        this.objectRepository = objectRepository;
    }

    public Long getNextObjectId() {
        final Long maxObjectId = objectRepository.getMaxObjectId()
                                                 .orElseThrow(() -> new IllegalArgumentException(
                                                         "An error occured while getting max(object_id) from object table"));

        return maxObjectId + 1;
    }

    public BrivoObject findByObjectId(final Long objectId) {
        return objectRepository.findByObjectId(objectId)
                               .orElseThrow(() -> new IllegalArgumentException(
                                       "There is not object with id " + objectId + " in the database"));
    }
}
