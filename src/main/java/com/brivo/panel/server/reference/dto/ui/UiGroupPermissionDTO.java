package com.brivo.panel.server.reference.dto.ui;

import java.util.Collection;

public class UiGroupPermissionDTO {
    private String siteName;
    private Long scheduleId;
    private Long securityActionId;
    private String deviceType;
    private Long deviceId;
    private Long deviceObjectId;
    private String deviceName;
    private UiScheduleSummaryDTO selectedSchedule;
    private Collection<UiScheduleSummaryDTO> availableSchedules;

    public UiGroupPermissionDTO() {

    }

    public UiGroupPermissionDTO(String siteName, Long scheduleId, Long securityActionId, Long deviceId, Long deviceObjectId,
                                String deviceName, String deviceType,
                                UiScheduleSummaryDTO uiScheduleDTO, Collection<UiScheduleSummaryDTO> availableSchedules) {
        this.siteName = siteName;
        this.scheduleId = scheduleId;
        this.securityActionId = securityActionId;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
        this.selectedSchedule = uiScheduleDTO;
        this.availableSchedules = availableSchedules;
        this.deviceObjectId = deviceObjectId;
        this.deviceType = deviceType;
    }

    public UiGroupPermissionDTO(String siteName, Long scheduleId, Long securityActionId, Long deviceId, String deviceName) {
        this.siteName = siteName;
        this.scheduleId = scheduleId;
        this.securityActionId = securityActionId;
        this.deviceId = deviceId;
        this.deviceName = deviceName;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public UiScheduleSummaryDTO getSelectedSchedule() {
        return selectedSchedule;
    }

    public void setSelectedSchedule(UiScheduleSummaryDTO selectedSchedule) {
        this.selectedSchedule = selectedSchedule;
    }

    public Collection<UiScheduleSummaryDTO> getAvailableSchedules() {
        return availableSchedules;
    }

    public void setAvailableSchedules(Collection<UiScheduleSummaryDTO> availableSchedules) {
        this.availableSchedules = availableSchedules;
    }

    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public void setDeviceObjectId(Long deviceObjectId) {
        this.deviceObjectId = deviceObjectId;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Override
    public String toString() {
        return "UiGroupPermissionDTO{" +
                "scheduleId=" + scheduleId +
                ", securityActionId=" + securityActionId +
                ", deviceObjectId=" + deviceObjectId +
                ", selectedSchedule=" + selectedSchedule +
                '}';
    }
}