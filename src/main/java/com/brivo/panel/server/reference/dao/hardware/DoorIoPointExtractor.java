package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.DoorIoPoint;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Component
public class DoorIoPointExtractor implements ResultSetExtractor<List<DoorIoPoint>> {

    @Override
    public List<DoorIoPoint> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<DoorIoPoint> doorIoPoints = new ArrayList<>();
        while (rs.next()) {
            DoorIoPoint ioPoint = new DoorIoPoint();
            ioPoint.setName(rs.getString("name"));
            ioPoint.setIoPointAddress(rs.getInt("ioPointAddress"));
            doorIoPoints.add(ioPoint);
        }
        return doorIoPoints;
    }

}
