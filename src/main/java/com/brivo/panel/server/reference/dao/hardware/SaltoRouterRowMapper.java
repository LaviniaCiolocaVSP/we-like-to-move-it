package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.SaltoRouter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;


@Component
public class SaltoRouterRowMapper implements RowMapper<SaltoRouter> {
    @Override
    public SaltoRouter mapRow(ResultSet rs, int rowNum) throws SQLException {
        SaltoRouter router = new SaltoRouter();
        router.setDeviceId(rs.getLong("board_object_id"));
        router.setAddress(StringUtils.removeAll(rs.getString("mac_address"), "[:|-]"));
        router.setPort(rs.getInt("port_number"));

        return router;
    }
}
