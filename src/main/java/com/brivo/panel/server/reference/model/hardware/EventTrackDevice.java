package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class EventTrackDevice {
    private Long deviceId;
    private Long monitoredDoorId;
    private Integer monitoredEventTypeId;
    private Boolean reportEngage;
    private Long activeScheduleId;
    private List<ProgrammableDeviceOutput> outputs;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getMonitoredDoorId() {
        return monitoredDoorId;
    }

    public void setMonitoredDoorId(Long monitoredDoorId) {
        this.monitoredDoorId = monitoredDoorId;
    }

    public Integer getMonitoredEventTypeId() {
        return monitoredEventTypeId;
    }

    public void setMonitoredEventTypeId(Integer monitoredEventTypeId) {
        this.monitoredEventTypeId = monitoredEventTypeId;
    }

    public Boolean getReportEngage() {
        return reportEngage;
    }

    public void setReportEngage(Boolean reportEngage) {
        this.reportEngage = reportEngage;
    }

    public Long getActiveScheduleId() {
        return activeScheduleId;
    }

    public void setActiveScheduleId(Long activeScheduleId) {
        this.activeScheduleId = activeScheduleId;
    }

    public List<ProgrammableDeviceOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<ProgrammableDeviceOutput> outputs) {
        this.outputs = outputs;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }
}
