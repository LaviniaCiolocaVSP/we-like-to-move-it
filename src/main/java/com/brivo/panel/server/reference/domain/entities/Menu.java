package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Menu {
    private long menuId;
    private long ordering;
    private Long parentId;
    private String screenId;
    private String labelkey;
    private String url;
    private String iconurl;
    private String requiredPermission;
    private Long featureId;
    private Feature featureByFeatureId;

    @Id
    @Column(name = "menu_id", nullable = false)
    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    @Basic
    @Column(name = "ordering", nullable = false)
    public long getOrdering() {
        return ordering;
    }

    public void setOrdering(long ordering) {
        this.ordering = ordering;
    }

    @Basic
    @Column(name = "parent_id", nullable = true)
    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "screen_id", nullable = true, length = 100)
    public String getScreenId() {
        return screenId;
    }

    public void setScreenId(String screenId) {
        this.screenId = screenId;
    }

    @Basic
    @Column(name = "labelkey", nullable = false, length = 100)
    public String getLabelkey() {
        return labelkey;
    }

    public void setLabelkey(String labelkey) {
        this.labelkey = labelkey;
    }

    @Basic
    @Column(name = "url", nullable = true, length = 100)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "iconurl", nullable = true, length = 100)
    public String getIconurl() {
        return iconurl;
    }

    public void setIconurl(String iconurl) {
        this.iconurl = iconurl;
    }

    @Basic
    @Column(name = "required_permission", nullable = true, length = 100)
    public String getRequiredPermission() {
        return requiredPermission;
    }

    public void setRequiredPermission(String requiredPermission) {
        this.requiredPermission = requiredPermission;
    }

    @Basic
    @Column(name = "feature_id", nullable = true)
    public Long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(Long featureId) {
        this.featureId = featureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Menu menu = (Menu) o;

        if (menuId != menu.menuId) {
            return false;
        }
        if (ordering != menu.ordering) {
            return false;
        }
        if (parentId != null ? !parentId.equals(menu.parentId) : menu.parentId != null) {
            return false;
        }
        if (screenId != null ? !screenId.equals(menu.screenId) : menu.screenId != null) {
            return false;
        }
        if (labelkey != null ? !labelkey.equals(menu.labelkey) : menu.labelkey != null) {
            return false;
        }
        if (url != null ? !url.equals(menu.url) : menu.url != null) {
            return false;
        }
        if (iconurl != null ? !iconurl.equals(menu.iconurl) : menu.iconurl != null) {
            return false;
        }
        if (requiredPermission != null ? !requiredPermission.equals(menu.requiredPermission) : menu.requiredPermission != null) {
            return false;
        }
        return featureId != null ? featureId.equals(menu.featureId) : menu.featureId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (menuId ^ (menuId >>> 32));
        result = 31 * result + (int) (ordering ^ (ordering >>> 32));
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (screenId != null ? screenId.hashCode() : 0);
        result = 31 * result + (labelkey != null ? labelkey.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (iconurl != null ? iconurl.hashCode() : 0);
        result = 31 * result + (requiredPermission != null ? requiredPermission.hashCode() : 0);
        result = 31 * result + (featureId != null ? featureId.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "feature_id", referencedColumnName = "feature_id", insertable = false, updatable = false)
    public Feature getFeatureByFeatureId() {
        return featureByFeatureId;
    }

    public void setFeatureByFeatureId(Feature featureByFeatureId) {
        this.featureByFeatureId = featureByFeatureId;
    }
}
