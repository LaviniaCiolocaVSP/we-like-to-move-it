package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supported_languages", schema = "brivo20", catalog = "onair")
public class SupportedLanguages {
    private long languageId;
    private String language;
    private String languageCode;

    @Id
    @Basic
    @Column(name = "language_id", nullable = false)
    public long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(long languageId) {
        this.languageId = languageId;
    }

    @Basic
    @Column(name = "language", nullable = false, length = 32)
    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    @Basic
    @Column(name = "language_code", nullable = true, length = 5)
    public String getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SupportedLanguages that = (SupportedLanguages) o;

        if (languageId != that.languageId) {
            return false;
        }
        if (language != null ? !language.equals(that.language) : that.language != null) {
            return false;
        }
        return languageCode != null ? languageCode.equals(that.languageCode) : that.languageCode == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (languageId ^ (languageId >>> 32));
        result = 31 * result + (language != null ? language.hashCode() : 0);
        result = 31 * result + (languageCode != null ? languageCode.hashCode() : 0);
        return result;
    }
}
