package com.brivo.panel.server.reference.dto.ui;

public class UiScheduleSummaryDTO {
    private Long scheduleId;
    private String scheduleName;
    private String siteName;
    private Long siteId;

    public UiScheduleSummaryDTO() {

    }

    public UiScheduleSummaryDTO(Long scheduleId, String scheduleName, String siteName, Long siteId) {
        this.scheduleId = scheduleId;
        this.scheduleName = scheduleName;
        this.siteName = siteName;
        this.siteId = siteId;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    public String getSiteName() {
        return siteName;
    }

    public void setSiteName(String siteName) {
        this.siteName = siteName;
    }

    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Override
    public String toString() {
        return "UiScheduleSummaryDTO{" +
                "scheduleId=" + scheduleId +
                ", scheduleName='" + scheduleName + '\'' +
                ", siteName='" + siteName + '\'' +
                ", siteId=" + siteId +
                '}';
    }
}
