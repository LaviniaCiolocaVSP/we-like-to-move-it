package com.brivo.panel.server.reference.dao.usergroup;

import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.usergroup.UserGroup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

@BrivoDao
public class UserGroupDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserGroupDao.class);
    private final int ONE_MINUTE_AFTER_MIDNIGHT = 1;

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private UserGroupExtractor userGroupExtractor;

    /**
     * Get user groups.
     *
     * @param panel The associated panel.
     * @return List of UserGroup.
     */
    public List<UserGroup> getUserGroups(Panel panel) {
        LOGGER.trace(">>> GetUserGroups");

        String query = UserGroupQuery.SELECT_USER_GROUPS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] \n{}", UserGroupQuery.SELECT_USER_GROUPS.name(), paramMap, query);

        List<UserGroup> groups = jdbcTemplate.query(query, paramMap, userGroupExtractor);

        TimeZone timeZone = TimeZone.getTimeZone(panel.getTimezone());
        for (UserGroup group : groups) {
            int resetTime = group.getAntiPassbackResetTime();
            if (resetTime != 0) {
                int timeZoneOffsetMins = timeZone.getOffset(System.currentTimeMillis()) / 60000;//millis to minutes
                group.setAntiPassbackResetTime(resetTime - timeZoneOffsetMins);
                if (group.getAntiPassbackResetTime() == 0) {
                    group.setAntiPassbackResetTime(ONE_MINUTE_AFTER_MIDNIGHT);
                }

            }
        }

        LOGGER.trace("<<< GetUserGroups");

        return groups;
    }

}
