package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class PulseRequestMessage extends DeviceAdministratorMessage {

    public PulseRequestMessage() {
        super(DownstreamMessageType.Pulse_Device);
    }

    public String toString() {
        return "PulseDeviceRequest : {objectId:" + deviceId + ", adminId:" + administratorId + "}";
    }

}
