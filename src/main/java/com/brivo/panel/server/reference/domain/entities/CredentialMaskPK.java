package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CredentialMaskPK implements Serializable {
    private long accessCredentialType;
    private long useOrder;

    @Column(name = "access_credential_type", nullable = false)
    @Id
    public long getAccessCredentialType() {
        return accessCredentialType;
    }

    public void setAccessCredentialType(long accessCredentialType) {
        this.accessCredentialType = accessCredentialType;
    }

    @Column(name = "use_order", nullable = false)
    @Id
    public long getUseOrder() {
        return useOrder;
    }

    public void setUseOrder(long useOrder) {
        this.useOrder = useOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialMaskPK that = (CredentialMaskPK) o;

        if (accessCredentialType != that.accessCredentialType) {
            return false;
        }
        return useOrder == that.useOrder;
    }

    @Override
    public int hashCode() {
        int result = (int) (accessCredentialType ^ (accessCredentialType >>> 32));
        result = 31 * result + (int) (useOrder ^ (useOrder >>> 32));
        return result;
    }
}
