package com.brivo.panel.server.reference.model;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;


public class ConfigurationStatus {
    private String credential;
    private String schedule;
    private String hardware;
    private Integer credentialPageSize;
    private Boolean registered;
    private Integer bleAuthTimeFrame;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public String getSchedule() {
        return schedule;
    }

    public void setSchedule(String schedule) {
        this.schedule = schedule;
    }

    public String getHardware() {
        return hardware;
    }

    public void setHardware(String hardware) {
        this.hardware = hardware;
    }

    public Integer getCredentialPageSize() {
        return credentialPageSize;
    }

    public void setCredentialPageSize(Integer credentialPageSize) {
        this.credentialPageSize = credentialPageSize;
    }

    public Boolean getRegistered() {
        return registered;
    }

    public void setRegistered(Boolean registered) {
        this.registered = registered;
    }

    public Integer getBleAuthTimeFrame() {
        return bleAuthTimeFrame;
    }

    public void setBleAuthTimeFrame(Integer bleAuthTimeFrame) {
        this.bleAuthTimeFrame = bleAuthTimeFrame;
    }
}
