package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_parameter", schema = "brivo20", catalog = "onair")
public class ReportParameter {
    private long id;
    private long reportDefinitionId;
    private long reportParameterType;
    private String labelKey;
    private String name;
    private Short isRequired;
    private Short allowMultiple;
    private long ordering;
    private ReportDefinition reportDefinitionByReportDefinitionId;
    private ReportParameterType reportParameterTypeByReportParameterType;
    private Collection<ReportParameterOption> reportParameterOptionsById;
    private Collection<ReportParameterValue> reportParameterValuesById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_definition_id", nullable = false)
    public long getReportDefinitionId() {
        return reportDefinitionId;
    }

    public void setReportDefinitionId(long reportDefinitionId) {
        this.reportDefinitionId = reportDefinitionId;
    }

    @Basic
    @Column(name = "report_parameter_type", nullable = false)
    public long getReportParameterType() {
        return reportParameterType;
    }

    public void setReportParameterType(long reportParameterType) {
        this.reportParameterType = reportParameterType;
    }

    @Basic
    @Column(name = "label_key", nullable = true, length = 100)
    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(String labelKey) {
        this.labelKey = labelKey;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "is_required", nullable = true)
    public Short getIsRequired() {
        return isRequired;
    }

    public void setIsRequired(Short isRequired) {
        this.isRequired = isRequired;
    }

    @Basic
    @Column(name = "allow_multiple", nullable = true)
    public Short getAllowMultiple() {
        return allowMultiple;
    }

    public void setAllowMultiple(Short allowMultiple) {
        this.allowMultiple = allowMultiple;
    }

    @Basic
    @Column(name = "ordering", nullable = false)
    public long getOrdering() {
        return ordering;
    }

    public void setOrdering(long ordering) {
        this.ordering = ordering;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportParameter that = (ReportParameter) o;

        if (id != that.id) {
            return false;
        }
        if (reportDefinitionId != that.reportDefinitionId) {
            return false;
        }
        if (reportParameterType != that.reportParameterType) {
            return false;
        }
        if (ordering != that.ordering) {
            return false;
        }
        if (labelKey != null ? !labelKey.equals(that.labelKey) : that.labelKey != null) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (isRequired != null ? !isRequired.equals(that.isRequired) : that.isRequired != null) {
            return false;
        }
        return allowMultiple != null ? allowMultiple.equals(that.allowMultiple) : that.allowMultiple == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportDefinitionId ^ (reportDefinitionId >>> 32));
        result = 31 * result + (int) (reportParameterType ^ (reportParameterType >>> 32));
        result = 31 * result + (labelKey != null ? labelKey.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (isRequired != null ? isRequired.hashCode() : 0);
        result = 31 * result + (allowMultiple != null ? allowMultiple.hashCode() : 0);
        result = 31 * result + (int) (ordering ^ (ordering >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_definition_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportDefinition getReportDefinitionByReportDefinitionId() {
        return reportDefinitionByReportDefinitionId;
    }

    public void setReportDefinitionByReportDefinitionId(ReportDefinition reportDefinitionByReportDefinitionId) {
        this.reportDefinitionByReportDefinitionId = reportDefinitionByReportDefinitionId;
    }

    @ManyToOne
    @JoinColumn(name = "report_parameter_type", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportParameterType getReportParameterTypeByReportParameterType() {
        return reportParameterTypeByReportParameterType;
    }

    public void setReportParameterTypeByReportParameterType(ReportParameterType reportParameterTypeByReportParameterType) {
        this.reportParameterTypeByReportParameterType = reportParameterTypeByReportParameterType;
    }

    @OneToMany(mappedBy = "reportParameterByReportParameterId")
    public Collection<ReportParameterOption> getReportParameterOptionsById() {
        return reportParameterOptionsById;
    }

    public void setReportParameterOptionsById(Collection<ReportParameterOption> reportParameterOptionsById) {
        this.reportParameterOptionsById = reportParameterOptionsById;
    }

    @OneToMany(mappedBy = "reportParameterByReportParameterId")
    public Collection<ReportParameterValue> getReportParameterValuesById() {
        return reportParameterValuesById;
    }

    public void setReportParameterValuesById(Collection<ReportParameterValue> reportParameterValuesById) {
        this.reportParameterValuesById = reportParameterValuesById;
    }
}
