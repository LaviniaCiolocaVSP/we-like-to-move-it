package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiCredentialFormatDTO implements Serializable {
    private final Long credentialFormatId;
    private final String credentialFormatDescription;

    public UiCredentialFormatDTO(Long credentialFormatId, String credentialFormatDescription) {
        this.credentialFormatId = credentialFormatId;
        this.credentialFormatDescription = credentialFormatDescription;
    }

    public Long getCredentialFormatId() {
        return credentialFormatId;
    }

    public String getCredentialFormatDescription() {
        return credentialFormatDescription;
    }
}
