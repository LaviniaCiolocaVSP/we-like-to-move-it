package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Identity {
    private long identityId;
    private long loginId;
    private long groupId;
    private String username;
    private String credential;
    private short remoteLogin;
    private String loginUrl;
    private short admin;
    private long externalId;
    private long externalTypeId;
    private Short owner;
    private short remoteService;
    private short remoteXmlrpc;
    private String providerUrl;
    private Short disableDealerGroupView;
    private Short disableDeviceEdit;
    private Login loginByLoginId;
    private LoginGroup loginGroupByGroupId;

    @Id
    @Column(name = "identity_id", nullable = false)
    public long getIdentityId() {
        return identityId;
    }

    public void setIdentityId(long identityId) {
        this.identityId = identityId;
    }

    @Basic
    @Column(name = "login_id", nullable = false)
    public long getLoginId() {
        return loginId;
    }

    public void setLoginId(long loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "group_id", nullable = false)
    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 36)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "credential", nullable = true, length = 256)
    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    @Basic
    @Column(name = "remote_login", nullable = false)
    public short getRemoteLogin() {
        return remoteLogin;
    }

    public void setRemoteLogin(short remoteLogin) {
        this.remoteLogin = remoteLogin;
    }

    @Basic
    @Column(name = "login_url", nullable = true, length = 512)
    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    @Basic
    @Column(name = "admin", nullable = false)
    public short getAdmin() {
        return admin;
    }

    public void setAdmin(short admin) {
        this.admin = admin;
    }

    @Basic
    @Column(name = "external_id", nullable = false)
    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    @Basic
    @Column(name = "external_type_id", nullable = false)
    public long getExternalTypeId() {
        return externalTypeId;
    }

    public void setExternalTypeId(long externalTypeId) {
        this.externalTypeId = externalTypeId;
    }

    @Basic
    @Column(name = "owner", nullable = true)
    public Short getOwner() {
        return owner;
    }

    public void setOwner(Short owner) {
        this.owner = owner;
    }

    @Basic
    @Column(name = "remote_service", nullable = false)
    public short getRemoteService() {
        return remoteService;
    }

    public void setRemoteService(short remoteService) {
        this.remoteService = remoteService;
    }

    @Basic
    @Column(name = "remote_xmlrpc", nullable = false)
    public short getRemoteXmlrpc() {
        return remoteXmlrpc;
    }

    public void setRemoteXmlrpc(short remoteXmlrpc) {
        this.remoteXmlrpc = remoteXmlrpc;
    }

    @Basic
    @Column(name = "provider_url", nullable = true, length = 512)
    public String getProviderUrl() {
        return providerUrl;
    }

    public void setProviderUrl(String providerUrl) {
        this.providerUrl = providerUrl;
    }

    @Basic
    @Column(name = "disable_dealer_group_view", nullable = true)
    public Short getDisableDealerGroupView() {
        return disableDealerGroupView;
    }

    public void setDisableDealerGroupView(Short disableDealerGroupView) {
        this.disableDealerGroupView = disableDealerGroupView;
    }

    @Basic
    @Column(name = "disable_device_edit", nullable = true)
    public Short getDisableDeviceEdit() {
        return disableDeviceEdit;
    }

    public void setDisableDeviceEdit(Short disableDeviceEdit) {
        this.disableDeviceEdit = disableDeviceEdit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Identity identity = (Identity) o;

        if (identityId != identity.identityId) {
            return false;
        }
        if (loginId != identity.loginId) {
            return false;
        }
        if (groupId != identity.groupId) {
            return false;
        }
        if (remoteLogin != identity.remoteLogin) {
            return false;
        }
        if (admin != identity.admin) {
            return false;
        }
        if (externalId != identity.externalId) {
            return false;
        }
        if (externalTypeId != identity.externalTypeId) {
            return false;
        }
        if (remoteService != identity.remoteService) {
            return false;
        }
        if (remoteXmlrpc != identity.remoteXmlrpc) {
            return false;
        }
        if (username != null ? !username.equals(identity.username) : identity.username != null) {
            return false;
        }
        if (credential != null ? !credential.equals(identity.credential) : identity.credential != null) {
            return false;
        }
        if (loginUrl != null ? !loginUrl.equals(identity.loginUrl) : identity.loginUrl != null) {
            return false;
        }
        if (owner != null ? !owner.equals(identity.owner) : identity.owner != null) {
            return false;
        }
        if (providerUrl != null ? !providerUrl.equals(identity.providerUrl) : identity.providerUrl != null) {
            return false;
        }
        if (disableDealerGroupView != null ? !disableDealerGroupView.equals(identity.disableDealerGroupView) : identity.disableDealerGroupView != null) {
            return false;
        }
        return disableDeviceEdit != null ? disableDeviceEdit.equals(identity.disableDeviceEdit) : identity.disableDeviceEdit == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (identityId ^ (identityId >>> 32));
        result = 31 * result + (int) (loginId ^ (loginId >>> 32));
        result = 31 * result + (int) (groupId ^ (groupId >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (credential != null ? credential.hashCode() : 0);
        result = 31 * result + (int) remoteLogin;
        result = 31 * result + (loginUrl != null ? loginUrl.hashCode() : 0);
        result = 31 * result + (int) admin;
        result = 31 * result + (int) (externalId ^ (externalId >>> 32));
        result = 31 * result + (int) (externalTypeId ^ (externalTypeId >>> 32));
        result = 31 * result + (owner != null ? owner.hashCode() : 0);
        result = 31 * result + (int) remoteService;
        result = 31 * result + (int) remoteXmlrpc;
        result = 31 * result + (providerUrl != null ? providerUrl.hashCode() : 0);
        result = 31 * result + (disableDealerGroupView != null ? disableDealerGroupView.hashCode() : 0);
        result = 31 * result + (disableDeviceEdit != null ? disableDeviceEdit.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "login_id", referencedColumnName = "login_id", nullable = false, insertable = false, updatable = false)
    public Login getLoginByLoginId() {
        return loginByLoginId;
    }

    public void setLoginByLoginId(Login loginByLoginId) {
        this.loginByLoginId = loginByLoginId;
    }

    @ManyToOne
    @JoinColumn(name = "group_id", referencedColumnName = "group_id", nullable = false, insertable = false, updatable = false)
    public LoginGroup getLoginGroupByGroupId() {
        return loginGroupByGroupId;
    }

    public void setLoginGroupByGroupId(LoginGroup loginGroupByGroupId) {
        this.loginGroupByGroupId = loginGroupByGroupId;
    }
}
