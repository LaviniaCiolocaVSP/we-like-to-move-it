package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ObjectNotificationTypePK implements Serializable {
    private long notificationTypeId;
    private long objectId;

    @Column(name = "notification_type_id", nullable = false)
    @Id
    public long getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(long notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    @Column(name = "object_id", nullable = false)
    @Id
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectNotificationTypePK that = (ObjectNotificationTypePK) o;

        if (notificationTypeId != that.notificationTypeId) {
            return false;
        }
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        int result = (int) (notificationTypeId ^ (notificationTypeId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }
}
