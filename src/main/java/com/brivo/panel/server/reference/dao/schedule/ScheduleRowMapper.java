package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.model.schedule.EnablingGroup;
import com.brivo.panel.server.reference.model.schedule.Schedule;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ScheduleRowMapper implements RowMapper<Schedule> {

    @Override
    public Schedule mapRow(ResultSet rs, int rowNum) throws SQLException {
        Schedule schedule = new Schedule();

        Long scheduleId = rs.getLong("scheduleId");
        Long enablingGroupId = rs.getLong("enablingGroupId");
        Integer enablingGracePeriod = rs.getInt("enablingGracePeriod");

        schedule.setScheduleId(scheduleId);

        if (enablingGroupId > 0) {
            EnablingGroup group = new EnablingGroup();

            group.setEnablingGroupId(enablingGroupId);
            group.setGracePeriod(enablingGracePeriod);
            group.setAutoDeactivate(true);

            schedule.setEnablingGroup(group);
        }

        return schedule;
    }

}
