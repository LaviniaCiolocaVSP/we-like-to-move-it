package com.brivo.panel.server.reference.model.hardware;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A class that allows comparison of firmware versions.
 */
public class Firmware implements Comparable<Firmware> {
    private static final Pattern DIGITS_ONLY = Pattern.compile("[0-9]*");

    private final String rawValue;
    private final int majorVersion;
    private final int minorVersion;
    private final int patchVersion;


    public Firmware(String raw) {
        this.rawValue = raw;

        String[] tokens = raw.split("\\.");

        this.majorVersion = Integer.parseInt(scrub(tokens[0]));

        if (tokens.length > 1) {
            this.minorVersion = Integer.parseInt(scrub(tokens[1]));
        } else {
            this.minorVersion = 0;
        }

        if (tokens.length > 2) {
            this.patchVersion = Integer.parseInt(scrub(tokens[2]));
        } else {
            this.patchVersion = 0;
        }
    }

    public Firmware(int major, int minor, int patch) {
        this.majorVersion = major;
        this.minorVersion = minor;
        this.patchVersion = patch;
        this.rawValue = String.format("%d.%d.%d", major, minor, patch);
    }

    public String getRawValue() {
        return rawValue;
    }

    public int getMajorVersion() {
        return majorVersion;
    }

    public int getMinorVersion() {
        return minorVersion;
    }

    public int getPatchVersion() {
        return patchVersion;
    }

    @Override
    public int compareTo(Firmware other) {
        int result = 0;

        if (this.majorVersion == other.majorVersion) {
            if (this.minorVersion == other.minorVersion) {
                result = this.patchVersion - other.patchVersion;
            } else {
                result = this.minorVersion - other.minorVersion;
            }
        } else {
            result = this.majorVersion - other.majorVersion;
        }

        return result;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + majorVersion;
        result = prime * result + minorVersion;
        result = prime * result + patchVersion;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Firmware other = (Firmware) obj;
        if (majorVersion != other.majorVersion) {
            return false;
        }
        if (minorVersion != other.minorVersion) {
            return false;
        }
        return patchVersion == other.patchVersion;
    }

    @Override
    public String toString() {
        return String.format("major=%d,minor=%d,patch=%d,raw=%s",
                this.majorVersion, this.minorVersion, this.patchVersion, this.rawValue);
    }

    /**
     * If the input is digits only then this is returned. Otherwise
     * "0" is returned.
     */
    private String scrub(String input) {
        Matcher matcher = DIGITS_ONLY.matcher(input);

        if (matcher.find()) {
            return matcher.group();
        } else {
            return "0";
        }
    }
}
