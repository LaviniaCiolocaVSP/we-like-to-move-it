package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface BrivoObjectPermissionRepository extends IObjectPermissionRepository {

    Optional<List<ObjectPermission>> findByGroupObjectId(@Param("objectId") Long objectId);
}