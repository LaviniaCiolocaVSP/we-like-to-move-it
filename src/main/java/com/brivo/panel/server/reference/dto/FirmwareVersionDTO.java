package com.brivo.panel.server.reference.dto;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FirmwareVersionDTO implements Comparable, Serializable {
    
    public static final String MERCURY_PANEL_FIRMWARE_VERSION = "5.1.5";
    
    private static final long serialVersionUID = 6081067679491287494L;
    
    private static final Pattern DIGITS_ONLY = Pattern.compile("[0-9]*");
    
    private final String raw;
    
    private final int major;
    
    private final int minor;
    
    private final int patch;
    
    
    public FirmwareVersionDTO(String rawVersion) {
        this.raw = rawVersion;
        
        String[] tokens = rawVersion.split("\\.");
        
        major = Integer.parseInt(scrub(tokens[0]));
        
        if (tokens.length > 1) {
            minor = Integer.parseInt(scrub(tokens[1]));
        } else {
            minor = 0;
        }
        
        if (tokens.length > 2) {
            patch = Integer.parseInt(scrub(tokens[2]));
        } else {
            patch = 0;
        }
    }
    
    /**
     * @return Returns the major.
     */
    public int getMajor() {
        return major;
    }
    
    /**
     * @return Returns the minor.
     */
    public int getMinor() {
        return minor;
    }
    
    /**
     * @return Returns the patch.
     */
    public int getPatch() {
        return patch;
    }
    
    /**
     * @return the raw string representation
     */
    public String getRaw() {
        return raw;
    }
    
    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        
        if (!(obj instanceof FirmwareVersionDTO)) {
            return false;
        }
        
        FirmwareVersionDTO other = (FirmwareVersionDTO) obj;
        
        return major == other.major && minor == other.minor
               && patch == other.patch && raw == other.raw;
    }
    
    @Override
    public int hashCode() {
        int result = 17;
        
        result = 37 * result + major;
        result = 37 * result + minor;
        result = 37 * result + patch;
        
        return result;
    }
    
    @Override
    public int compareTo(Object obj) {
        FirmwareVersionDTO other = (FirmwareVersionDTO) obj;
        
        if (major == other.major) {
            if (minor == other.minor && patch == other.patch) {
                return raw.compareTo(other.raw);
            }
            
            return (minor == other.minor) ? patch - other.patch : minor
                                                                  - other.minor;
        } else {
            return major - other.major;
        }
    }
    
    @Override
    public String toString() {
        return this.raw;
    }
    
    private String scrub(String input) {
        Matcher matcher = DIGITS_ONLY.matcher(input);
        
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "0";
        }
    }
}
