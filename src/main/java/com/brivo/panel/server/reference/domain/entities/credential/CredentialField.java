package com.brivo.panel.server.reference.domain.entities.credential;


public class CredentialField
{
    private long credentialFieldId;
    private boolean isForReference;
    private String name;
    private String xmlName;
    private boolean display;
    
    private String formatValue;
    private int length;
    
    private int order;
    
    private CredentialFieldType credentialFieldType;
    
    public CredentialField() {
        
    }
    
    public CredentialField(CredentialField toCopy) {
        setCredentialFieldId(toCopy.getCredentialFieldId());
        setForReference(toCopy.isForReference());
        setName(toCopy.getName());
        setXmlName(toCopy.getXmlName());
        setDisplay(toCopy.display());
        setFormatValue(toCopy.getFormatValue());
        setLength(toCopy.getLength());
        setOrder(toCopy.getOrder());
        setCredentialFieldType(toCopy.getCredentialFieldType());
    }
    
    public void setCredentialFieldType(CredentialFieldType credentialFieldType)
    {
        this.credentialFieldType = credentialFieldType;
    }
    public CredentialFieldType getCredentialFieldType()
    {
        return credentialFieldType;
    }
    
    public void setFormatValue(String formatValue)
    {
        this.formatValue = formatValue;
    }
    public String getFormatValue()
    {
        return formatValue;
    }
    public void setName(String name)
    {
        this.name = name;
    }
    public String getName()
    {
        return name;
    }
    public void setLength(int length)
    {
        this.length = length;
    }
    public int getLength()
    {
        if (getCredentialFieldType() == null) {
            return length;
        }
        switch (getCredentialFieldType()) {
        case ES:
        case FS:
        case LRC:
        case SS:
            return 1;
        default: 
            return length;
        }
    }
    
    public int getOrder() {
        return order;
    }
    
    public void setOrder(int order) {
        this.order = order;
    }
    public void setCredentialFieldId(long credentialFieldId)
    {
        this.credentialFieldId = credentialFieldId;
    }
    public long getCredentialFieldId()
    {
        return credentialFieldId;
    }
    public void setForReference(boolean isForReference)
    {
        this.isForReference = isForReference;
    }
    public boolean isForReference()
    {
        return isForReference;
    }
    public void setXmlName(String xmlName)
    {
        this.xmlName = xmlName;
    }
    public String getXmlName()
    {
        return xmlName;
    }
    public void setDisplay(boolean display)
    {
        this.display = display;
    }
    public boolean display()
    {
        return display;
    }
   

}
