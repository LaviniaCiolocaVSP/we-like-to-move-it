package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class ActorScheduleEvent extends Event {
    private Long scheduleId;
    private Long userId;
    private Long deviceId;
    private Instant deactivationTime;

    @JsonCreator
    public ActorScheduleEvent(@JsonProperty("eventType") final EventType eventType,
                              @JsonProperty("eventTime") final Instant eventTime,
                              @JsonProperty("scheduleId") final Long scheduleId,
                              @JsonProperty("userId") final Long userId,
                              @JsonProperty("deviceId") final Long deviceId,
                              @JsonProperty("deactivationTime") final Instant deactivationTime) {
        super(eventType, eventTime);
        this.scheduleId = scheduleId;
        this.userId = userId;
        this.deviceId = deviceId;
        this.deactivationTime = deactivationTime;
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Instant getDeactivationTime() {
        return deactivationTime;
    }

    public void setDeactivationTime(Instant deactivationTime) {
        this.deactivationTime = deactivationTime;
    }

}
