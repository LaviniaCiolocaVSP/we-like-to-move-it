package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.hardware.HardwareDao;
import com.brivo.panel.server.reference.dao.hardware.PanelHardwareData;
import com.brivo.panel.server.reference.dao.panel.PanelDao;
import com.brivo.panel.server.reference.model.hardware.AllegionGateway;
import com.brivo.panel.server.reference.model.hardware.Board;
import com.brivo.panel.server.reference.model.hardware.Door;
import com.brivo.panel.server.reference.model.hardware.DoorIoPoint;
import com.brivo.panel.server.reference.model.hardware.Elevator;
import com.brivo.panel.server.reference.model.hardware.EnterPermission;
import com.brivo.panel.server.reference.model.hardware.EventTrackDevice;
import com.brivo.panel.server.reference.model.hardware.Floor;
import com.brivo.panel.server.reference.model.hardware.LockPermission;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.hardware.PanelConfiguration;
import com.brivo.panel.server.reference.model.hardware.Permission;
import com.brivo.panel.server.reference.model.hardware.ProgrammableDeviceOutput;
import com.brivo.panel.server.reference.model.hardware.SaltoLock;
import com.brivo.panel.server.reference.model.hardware.SaltoRouter;
import com.brivo.panel.server.reference.model.hardware.SwitchDevice;
import com.brivo.panel.server.reference.model.hardware.TimerDevice;
import com.brivo.panel.server.reference.model.hardware.ValidCredentialDevice;
import com.brivo.panel.server.reference.service.util.ConverterUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class HardwareService {

    private final static Logger LOGGER = LoggerFactory.getLogger(HardwareService.class);

    private static Integer PING_PORT_DEFAULT = 0;
    private static Integer FLUSH_INTERVAL_DEFAULT = 0;
    private static Boolean SHOULD_POLL_DEFAULT = false;
    private static Boolean SHOULD_CACHE_DEFAULT = false;
    private static Boolean ENABLE_COMMAND_CHANNEL = true;
    // private static final String ZONE_INFO_PATH = "/usr/share/zoneinfo/";
    private static Boolean ENABLE_FIPS_MODE = false;
    @Autowired
    HardwareDao hardwareDao;

    // private static final String ZONE_INFO_PATH = "timezones/";
    @Autowired
    PanelService panelService;

    @Autowired
    PanelDao panelDao;

    @Value("${timezone-path}")
    private String ZONE_INFO_PATH;

    @Value("${disable.server.authorization.checkfirst:true}")
    private Boolean disableServerAuthCheckFirst;

    /**
     * Return all hardware configuration data.
     * <p>
     * Not currently supported:
     * - Guard Tour Device
     * - Keypad Device
     * - Mustering Device
     * - TkeDataEntryDevice
     *
     * @param panel The panel we are working will.
     * @return PanelHardwareData
     */
    public PanelHardwareData getHardware(Panel panel) {
        LOGGER.trace(">>> GetHardware");

        PanelHardwareData data = new PanelHardwareData();

        data.setLastUpdate(panel.getPanelChanged());

        data.setPanelConfig(getPanelConfiguration(panel));
        data.setAllegionGateways(getAllegionGateways(panel));
        data.setBoards(getBoards(panel));
        data.setDoors(getDoors(panel));
        data.setElevators(getElevators(panel));
        data.setEventTrackDevices(getEventTrackDevices(panel));
        data.setFloors(getFloors(panel));
        data.setSaltoRouters(getSaltoRouters(panel));
        data.setSwitchDevices(getSwitchDevices(panel));
        data.setTimerDevices(getTimerDevices(panel));
        data.setValidCredentialDevices(getValidCredentialDevices(panel));

        LOGGER.trace("<<< GetHardware");

        return data;
    }

    /**
     * Panel Configuration.
     * <p>
     * Set network id to TCP/IP
     * Get panel's object properties
     * Run select_default_panel_props.sql
     * Write out listening port, nonce, flush interval, polling interval,
     * cache flag, time zone, and server version
     *
     * @param panel
     * @return
     */
    PanelConfiguration getPanelConfiguration(Panel panel) {
        LOGGER.trace(">>> GetPanelConfiguration");

        PanelConfiguration data = hardwareDao.getPanelConfiguration(panel);

        if (data.getPingPort() == null) {
            data.setPingPort(PING_PORT_DEFAULT);
        }

        if (data.getFlushInterval() == null) {
            data.setFlushInterval(FLUSH_INTERVAL_DEFAULT);
        }

        if (data.getShouldPollForData() == null) {
            data.setShouldPollForData(SHOULD_POLL_DEFAULT);
        }

        if (data.getShouldCacheEvents() == null) {
            data.setShouldCacheEvents(SHOULD_CACHE_DEFAULT);
        }

        // Do we need to generate Nonce value? See ByteEncoder.generateNonce().

        data.setEnableCommandChannel(ENABLE_COMMAND_CHANNEL);
        data.setPanelLogPeriod(panel.getLogPeriod());
        String panelTimeZone = hardwareDao.getPanelTimeZone(panel);

        data.setTimeZoneData(Base64Encode(String.format("%s%s", ZONE_INFO_PATH, panelTimeZone)));
        data.setTimeZone(panelTimeZone);
        data.setFirmwareVersion(panel.getFirmware().getRawValue());
        data.setEnableFipsMode(ENABLE_FIPS_MODE);
        data.setLastUpdate(panel.getPanelChanged());
        data.setDisableServerAuthCheckFirst(disableServerAuthCheckFirst);

        LOGGER.trace("<<< GetPanelConfiguration");
        return data;
    }

    private String Base64Encode(String filename) {

        String encoded = null;
        try {
            LOGGER.info("Filename: " + filename);

            InputStream in = this.getClass().getClassLoader().getResourceAsStream(filename);
            byte[] content = IOUtils.toByteArray(in);

            // byte[] content = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource(filename).toURI()));
            // byte[] fileContents = Files.readAllBytes(path);
            encoded = Base64.getEncoder().encodeToString(content);
        } catch (IOException e) {
            LOGGER.error(e.getStackTrace().toString());
        }
        return encoded;
    }

    /**
     * Boards
     *
     * @param panel
     * @return
     */
    List<Board> getBoards(Panel panel) {
        LOGGER.trace(">>> GetBoards");

        List<Board> boards = hardwareDao.getBoards(panel);

        LOGGER.trace("<<< GetBoards");

        return boards;
    }

    /**
     * Doors
     * <p>
     * Run select_doors.sql to load doors
     * <p>
     * For each door found
     * Create Door object
     * If scheduleEnabled,
     * Run device_schedule query to retrieve device schedule. See
     * Gen5DeviceBuilder.progDevicesData. Set UnlockScheduleId.
     * Retrieve device permissions (enter, lock, and privacy override permissions)
     * Add two factor settings to add two factor data if applicable
     * Add antipassback settings to add antipassback data if applicable
     * Add card required to add card required schedule if applicable
     * Add door debounce / lock on open to add door debounce and lock-on-open
     * parameters if applicable
     * Add Door object to Collection to be returned
     *
     * @param panel The Panel.
     * @return List of Door.
     */
    List<Door> getDoors(Panel panel) {
        LOGGER.trace(">>> GetDoors");

        List<Door> doors = hardwareDao.getDoors(panel);

        for (Door door : doors) {
            //taken from db - enable_live_control column. Changed select_doors.sql
            door.setReportLiveStatus(ConverterUtils.convertIntegerToBoolean(door.getEnableLiveControl())); // Not currently supported.

            getDoorPermissions(door, panel);

            updateDoorIOPoints(door.getDeviceId(), door);

        }

        LOGGER.trace("<<< GetDoors");

        return doors;
    }

    /**
     * Set Door IOPoints based on the prog_io_points in the database
     *
     * @param deviceId
     * @param door
     */
    void updateDoorIOPoints(Long deviceId, Door door) {
        List<DoorIoPoint> ioPointsForBrain = hardwareDao.getDoorIoPoints(deviceId);

        int boardAddress = door.getBoardNumber();
        String doorName = "Door 1 - ";
        if (door.getNodeNumber() == 2) {
            doorName = "Door 2 - ";
        }
        final String reader = doorName + "Reader";
        final String lockRelay = doorName + "Door Lock Relay";
        final String auxRelay = doorName + "Aux Relay 1";
        final String contact = doorName + "Door Contact";
        final String rex = doorName + "REX";


        for (DoorIoPoint ioPoint : ioPointsForBrain) {
            if (ioPoint.getName().equalsIgnoreCase(reader)) {
                door.setReaderBoardAddress(boardAddress);
                door.setReaderPointAddress(ioPoint.getIoPointAddress());
            } else if (ioPoint.getName().equalsIgnoreCase(lockRelay)) {
                door.setDoorLatchRelayBoardAddress(boardAddress);
                door.setDoorLatchRelayPointAddress(ioPoint.getIoPointAddress());
            } else if (ioPoint.getName().equalsIgnoreCase(auxRelay)) {
                if (door.getUseAlarmShuntRelayBehavior()) {
                    door.setShuntRelayBoardAddress(boardAddress);
                    door.setShuntRelayPointAddress(ioPoint.getIoPointAddress());
                }
            } else if (ioPoint.getName().equalsIgnoreCase(contact)) {
                door.setDoorSwitchInputBoardAddress(boardAddress);
                door.setDoorSwitchInputPointAddress(ioPoint.getIoPointAddress());
            } else if (ioPoint.getName().equalsIgnoreCase(rex)) {
                door.setRexSwitchInputBoardAddress(boardAddress);
                door.setRexSwitchInputPointAddress(ioPoint.getIoPointAddress());
            }
        }
    }

    /**
     * Fetch enter
     * and lock
     * permissions .
     *
     * @param door  For this door .
     * @param panel The
     *              panel .
     */
    void getDoorPermissions(Door door, Panel panel) {
        List<Permission> permList =
                hardwareDao.getPermissions(door.getDeviceId(),
                        panel.getElectronicSerialNumber());

        List<EnterPermission> enterList = new ArrayList<>();
        List<LockPermission> lockList = new ArrayList<>();

        for (Permission perm : permList) {
            if (perm instanceof EnterPermission) {
                enterList.add((EnterPermission) perm);
            } else if (perm instanceof LockPermission) {
                lockList.add((LockPermission) perm);
            }
        }

        door.setEnterPermissions(enterList);
        door.setLockPermissions(lockList);
    }

    /**
     * TimerDevice
     * <p>
     * <p>
     * Run select_timer_devices.
     * sql to
     * load timer
     * devices
     * <p>
     * <p>
     * For each
     * timer device
     * found
     * <p>
     * Create TimerDevice
     * object
     * <p>
     * Add programmable
     * output data
     * <p>
     * Add TimerDevice
     * object to
     * Collection to
     * be returned     *
     *
     * @param panel
     * @return
     */
    List<TimerDevice> getTimerDevices(Panel panel) {
        LOGGER.trace(">>> GetTimerDevices");

        List<TimerDevice> timers = hardwareDao.getTimerDevices(panel);

        for (TimerDevice timer : timers) {
            timer.setReportLiveStatus(false); // Not currently supported.

            timer.setOutputs(getProgrammableDeviceOutputForDevice(timer.getDeviceId()));
        }

        LOGGER.trace("<<< GetTimerDevices");

        return timers;
    }

    /**
     * ValidCredentialDevice
     * <p>
     * <p>
     * Run select_valid_credential_devices.
     * sql to
     * load valid
     * credential devices
     * <p>
     * <p>
     * For each
     * valid credential
     * device found
     * <p>
     * Create ValidCredentialDevice
     * object
     * <p>
     * Retrieve device
     * <p>
     * permissions(enter, lock, and privacy override permissions)
     * <p>
     * Add programmable
     * output data
     * <p>
     * Add two
     * factor settings
     * to add
     * two factor
     * data if applicable
     * <p>
     * Add card
     * required to
     * add card
     * required schedule if applicable
     * <p>
     * Add ValidCredentialDevice
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -inputBoardId
     * -AntipassbackSettings
     * -reportLiveStatus
     * -KeypadCommand
     *
     * @param panel The
     *              panel .
     * @return List of
     * ValidCredentialDevice .
     */
    List<ValidCredentialDevice> getValidCredentialDevices(Panel panel) {
        LOGGER.trace(">>> GetValidCredentialDevices");

        List<ValidCredentialDevice> creds = hardwareDao.getValidCredentialDevices(panel);

        for (ValidCredentialDevice device : creds) {
            device.setReportLiveStatus(false); // Not currently supported.

            device.setEnterPermissions(
                    getEnterPermissionsForDevice(device.getDeviceId(),
                            panel.getElectronicSerialNumber()));

            device.setOutputs(getProgrammableDeviceOutputForDevice(device.getDeviceId()));
        }

        LOGGER.trace("<<< GetValidCredentialDevices");

        return creds;
    }

    /**
     * Fetch the
     * enter permissions for this device .
     *
     * @param deviceId          The
     *                          device .
     * @param panelSerialNumber The
     *                          panel .
     * @return The permissions.
     */
    List<EnterPermission> getEnterPermissionsForDevice(Long deviceId, String panelSerialNumber) {
        LOGGER.trace(">>> GetEnterPermissionsForDevice");

        List<Permission> permList =
                hardwareDao.getPermissions(deviceId, panelSerialNumber);

        List<EnterPermission> enterList = permList.stream()
                                                  .filter(permission -> permission instanceof EnterPermission)
                                                  .map(permission -> (EnterPermission) permission)
                                                  .collect(Collectors.toList());

        LOGGER.trace("<<< GetEnterPermissionsForDevice");

        return enterList;
    }

    /**
     * SwitchDevice
     * <p>
     * <p>
     * Run select_switch_devices.
     * sql to
     * load switch devices
     * <p>
     * <p>
     * For each switch
     * device found
     * <p>
     * Create SwitchDevice
     * object
     * <p>
     * Add programmable
     * output data
     * <p>
     * Add SwitchDevice
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -inputBoardId
     * -inputDelay
     * -maxActivation
     * -reportLiveStatus
     *
     * @param panel
     * @return
     */
    List<SwitchDevice> getSwitchDevices(Panel panel) {
        LOGGER.trace(">>> GetSwitchDevices");

        List<SwitchDevice> switches = hardwareDao.getSwitchDevices(panel);
//        List<PanelDataHardwareTagsExtraFields> switchDevicesExtraFields = paneldataHardwareTagsExtraFieldsRepository.findByJsonTag(SWITCH_DEVICES_JSON_TAG);
//
//        if (switchDevicesExtraFields == null || switchDevicesExtraFields.isEmpty()) {
//            LOGGER.debug("There are no switchDevicesExtraFields set in the database");
//        } else {
//            LOGGER.debug("Successfully retrieved {} switchDevicesExtraFields: {}", switchDevicesExtraFields.size(),
//                    Arrays.toString(switchDevicesExtraFields.toArray()));
//        }

        for (SwitchDevice device : switches) {
            device.setReportLiveStatus(false); // Not supported.

            final Long deviceObjectId = device.getDeviceId();
            List<ProgrammableDeviceOutput> outputs = getProgrammableDeviceOutputForDevice(deviceObjectId);
//            List<ProgrammableDeviceOutput> convertedOutputs = outputs;
//
//            if (switchDevicesExtraFields != null && !switchDevicesExtraFields.isEmpty()) {
//                Optional<PanelDataHardwareTagsExtraFields> switchExtraField = findSwitchDeviceExtraFields(switchDevicesExtraFields, deviceObjectId);
//
//                if (switchExtraField.isPresent()) {
//                    LOGGER.debug("Found extra field {} for device with object id {}", switchExtraField.get().toString(), deviceObjectId);
//
//                    convertedOutputs = outputs.stream()
//                                              .map(convertProgrammableDeviceOutputToThreatLevel(switchExtraField.get()))
//                                              .collect(Collectors.toList());
//
//                    LOGGER.debug("Threat level outputs for device with object id = {} are {}", deviceObjectId,
//                            Arrays.toString(convertedOutputs.toArray()));
//                } else {
//                    LOGGER.debug("No extra field found for switch device with object id {}", deviceObjectId);
//                }
//            }
//
//            device.setOutputs(convertedOutputs);

            device.setOutputs(outputs);

        }

        LOGGER.trace("<<< GetSwitchDevices");

        return switches;
    }

//    private Optional<PanelDataHardwareTagsExtraFields> findSwitchDeviceExtraFields(List<PanelDataHardwareTagsExtraFields> switchDevicesExtraFields,
//                                                                                   Long deviceObjectId) {
//
//        return switchDevicesExtraFields.stream()
//                                       .filter(panelDataHardwareTagsExtraFields -> panelDataHardwareTagsExtraFields.getDeviceOid().equals(deviceObjectId))
//                                       .findFirst();
//    }
//
//    private Function<ProgrammableDeviceOutput, ThreatLevelProgrammableDeviceOutput> convertProgrammableDeviceOutputToThreatLevel(
//            PanelDataHardwareTagsExtraFields panelDataHardwareTagsExtraFields) {
//        return programmableDeviceOutput -> {
//            ThreatLevelProgrammableDeviceOutput threatLevelProgrammableDeviceOutput = new ThreatLevelProgrammableDeviceOutput(programmableDeviceOutput);
//
//            Collection<Long> onDeviceIds = parsePaneldataHardwareValueToOnDeviceIds(panelDataHardwareTagsExtraFields.getValue());
//            threatLevelProgrammableDeviceOutput.setOnDeviceIds(onDeviceIds);
//
//            return threatLevelProgrammableDeviceOutput;
//        };
//    }

    private Collection<Long> parsePaneldataHardwareValueToOnDeviceIds(final String value) {
        LOGGER.debug("Parsing value {} to get onDeviceIds", value);

        Collection<Long> onDeviceIds = new ArrayList<>();
        try {

            String[] deviceIdsString = value.split(",");

            for (String s : deviceIdsString) {
                String trimmedDeviceId = s.trim();
                Long deviceOid = Long.parseLong(trimmedDeviceId);
                onDeviceIds.add(deviceOid);
            }

            LOGGER.debug("Successfully parsed {} value to onDeviceIds: {}", value, Arrays.toString(onDeviceIds.toArray()));
        } catch (Exception ex) {
            LOGGER.error("An error occurred while parsing value {} to list on long values, separated by comma", value);
        }

        return onDeviceIds;
    }

    /**
     * EventTrackDevice
     * <p>
     * <p>
     * Run select_event_track_devices.
     * sql to
     * load event
     * track devices
     * <p>
     * <p>
     * For each
     * event track
     * device found
     * <p>
     * Create EventTrackDevice
     * object
     * <p>
     * Add programmable
     * output data
     * <p>
     * Add EventTrackDevice
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -reportLiveStatus
     *
     * @param panel
     * @return
     */
    List<EventTrackDevice> getEventTrackDevices(Panel panel) {
        LOGGER.trace(">>> GetEventTrackDevices");

        List<EventTrackDevice> eventTracks = hardwareDao.getEventTrackDevices(panel);

        for (EventTrackDevice device : eventTracks) {
            device.setReportLiveStatus(false); // Not supported.

            device.setOutputs(getProgrammableDeviceOutputForDevice(device.getDeviceId()));
        }

        LOGGER.trace("<<< GetEventTrackDevices");

        return eventTracks;
    }

    /**
     * Floor
     * <p>
     * <p>
     * Run select_floors.
     * sql to
     * load floors
     * <p>
     * <p>
     * For each
     * floor found
     * <p>
     * Create Floor
     * object
     * <p>
     * Run query
     * to retrieve
     * device schedule
     * <p>
     * Retrieve device
     * <p>
     * permissions(enter, lock, and privacy override permissions)
     * <p>
     * Add programmable
     * output data
     * <p>
     * Retrieve FloorElevator
     * data .
     * <p>
     * Add Floor
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -reportLiveStatus
     *
     * @param panel
     * @return
     */
    List<Floor> getFloors(Panel panel) {
        LOGGER.trace(">>> GetFloors");

        List<Floor> floors = hardwareDao.getFloors(panel);

        for (Floor floor : floors) {
            floor.setReportLiveStatus(false); // Not supported.

            floor.setEnterPermissions(
                    getEnterPermissionsForDevice(
                            floor.getDeviceId(), panel.getElectronicSerialNumber()));
        }

        LOGGER.trace("<<< GetFloors");

        return floors;
    }

    /**
     * Elevator
     * <p>
     * <p>
     * Run select_elevators.
     * sql to
     * load elevators
     * <p>
     * <p>
     * For each
     * elevator found
     * <p>
     * Create Elevator
     * object
     * <p>
     * Add programmable
     * output data
     * <p>
     * Add two
     * factor settings
     * to add
     * two factor
     * data if applicable
     * <p>
     * Add card
     * required to
     * add card
     * required schedule if applicable
     * <p>
     * Add Elevator
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -AntipassbackSettings
     * -reportLiveStatus
     * -KeypadCommand
     *
     * @param panel
     * @return
     */
    List<Elevator> getElevators(Panel panel) {
        LOGGER.trace(">>> GetElevators");

        List<Elevator> elevators = hardwareDao.getElevators(panel);

        // Does this have Programmable Device Output?

        LOGGER.trace("<<< GetElevators");

        return elevators;
    }

    /**
     * SaltoRouter
     * <p>
     * <p>
     * Run select_salto_router_board_properties.
     * sql to
     * load Salto
     * router data
     * <p>
     * <p>
     * For each
     * Salto router
     * found
     * <p>
     * Create SaltoRouter
     * object
     * <p>
     * Run select_salto_locks.
     * sql to
     * load locks
     * related to
     * the router
     * <p>
     * For each
     * Salto lock
     * found
     * <p>
     * Create SaltoLock
     * object
     * <p>
     * Set up
     * enablePrivacyMode appropriately
     * depending whether
     * lockdown is
     * going on
     * or not
     * <p>
     * Run query
     * to retrieve
     * device schedule
     * <p>
     * Retrieve device
     * <p>
     * permissions(enter, lock, and privacy override permissions)
     * <p>
     * Add SaltoLock
     * object to
     * Collection
     * <p>
     * Add Collection
     * of SaltoLock
     * objects to
     * the Salto
     * router
     * <p>
     * Add SaltoRouter
     * object to
     * Collection to
     * be returned
     * <p>
     * <p>
     * OnAir does
     * not support:
     * -reportLiveStatus
     *
     * @param panel
     * @return
     */
    List<SaltoRouter> getSaltoRouters(Panel panel) {
        LOGGER.trace(">>> GetSaltoRouter");

        List<SaltoRouter> routers = hardwareDao.getSaltoRouters(panel);

        if (routers.size() == 1) // Should be at most one.
        {
            SaltoRouter router = routers.get(0);
            router.setIsIPV6(false); // Not currently supported.
            router.setLocks(getSaltoLocks(panel));
        }

        LOGGER.trace("<<< GetSaltoRouter");

        return routers;
    }

    /**
     * Get Salto
     * locks .
     *
     * @param panel The
     *              panel .
     * @return The locks.
     */
    List<SaltoLock> getSaltoLocks(Panel panel) {
        List<SaltoLock> locks = hardwareDao.getSaltoLocks(panel);

        for (SaltoLock lock : locks) {
            lock.setReportLiveStatus(false); // Not supported.
            lock.setEnterPermissions(
                    getEnterPermissionsForDevice(
                            lock.getDeviceId(), panel.getElectronicSerialNumber()));
        }

        return locks;
    }

    /**
     * AllegionGateway
     * <p>
     * <p>
     * Run select_allegion_gateway_board_properties.
     * sql to
     * load Allegion
     * gateway data
     * <p>
     * <p>
     * For each
     * Allegion gateway
     * found
     * <p>
     * Create AllegionGateway
     * object
     * <p>
     * Run select_allegion_locks.
     * sql to
     * load locks
     * related to
     * the gateway
     * <p>
     * For each
     * Allegion lock
     * found
     * <p>
     * Create AllegionLock
     * object
     * <p>
     * Run query
     * to retrieve
     * device schedule
     * <p>
     * Retrieve device
     * <p>
     * permissions(enter, lock, and privacy override permissions)
     * <p>
     * Add AllegionLock
     * object to
     * Collection
     * <p>
     * Add Collection
     * of AllegionLock
     * objects to
     * the Allegion
     * gateway
     * <p>
     * Add AllegionGateway
     * object to
     * Collection to
     * be returned     *
     * <p>
     * <p>
     * TODO Allegion
     * implementation needs
     * to be
     * reconsidered .
     *
     * @param panel
     * @return
     */
    List<AllegionGateway> getAllegionGateways(Panel panel) {
        LOGGER.trace(">>> GetAllegionGateways");

        List<AllegionGateway> gateways = hardwareDao.getAllegionGateways(panel);

        LOGGER.trace("<<< GetAllegionGateways");

        return gateways;
    }

    /**
     * Fetch programmable
     * device outputs for
     * the specified
     * device .
     *
     * @param deviceId The
     *                 device .
     * @return The outputs
     */
    List<ProgrammableDeviceOutput> getProgrammableDeviceOutputForDevice(Long deviceId) {
        List<ProgrammableDeviceOutput> outputList = hardwareDao.getDeviceProgrammableOutputs(deviceId);

        return outputList;
    }

}
