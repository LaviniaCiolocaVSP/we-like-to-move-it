package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "device_group_permission_v", schema = "brivo20", catalog = "onair")
public class DeviceGroupPermissionV {
    private Long securityActionId;
    private Long actorObjectId;
    private Long securityGroupId;
    private Long accountId;
    private Long groupObjectId;
    private String groupName;
    private String groupDescription;
    private Long deviceId;
    private String deviceName;
    private String deviceDescription;
    private Long scheduleId;
    private String scheduleName;
    private String scheduleDescription;
    private Long panelId;
    private Long opsFlag;

    @Id
    @Basic
    @Column(name = "security_action_id", nullable = true)
    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "group_object_id", nullable = true)
    public Long getGroupObjectId() {
        return groupObjectId;
    }

    public void setGroupObjectId(Long groupObjectId) {
        this.groupObjectId = groupObjectId;
    }

    @Basic
    @Column(name = "group_name", nullable = true, length = 35)
    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    @Basic
    @Column(name = "group_description", nullable = true, length = 256)
    public String getGroupDescription() {
        return groupDescription;
    }

    public void setGroupDescription(String groupDescription) {
        this.groupDescription = groupDescription;
    }

    @Basic
    @Column(name = "device_id", nullable = true)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "device_name", nullable = true, length = 100)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Basic
    @Column(name = "device_description", nullable = true, length = 256)
    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    @Basic
    @Column(name = "schedule_id", nullable = true)
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "schedule_name", nullable = true, length = 32)
    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    @Basic
    @Column(name = "schedule_description", nullable = true, length = 256)
    public String getScheduleDescription() {
        return scheduleDescription;
    }

    public void setScheduleDescription(String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }

    @Basic
    @Column(name = "panel_id", nullable = true)
    public Long getPanelId() {
        return panelId;
    }

    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }

    @Basic
    @Column(name = "ops_flag", nullable = true)
    public Long getOpsFlag() {
        return opsFlag;
    }

    public void setOpsFlag(Long opsFlag) {
        this.opsFlag = opsFlag;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceGroupPermissionV that = (DeviceGroupPermissionV) o;

        if (securityActionId != null ? !securityActionId.equals(that.securityActionId) : that.securityActionId != null) {
            return false;
        }
        if (actorObjectId != null ? !actorObjectId.equals(that.actorObjectId) : that.actorObjectId != null) {
            return false;
        }
        if (securityGroupId != null ? !securityGroupId.equals(that.securityGroupId) : that.securityGroupId != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (groupObjectId != null ? !groupObjectId.equals(that.groupObjectId) : that.groupObjectId != null) {
            return false;
        }
        if (groupName != null ? !groupName.equals(that.groupName) : that.groupName != null) {
            return false;
        }
        if (groupDescription != null ? !groupDescription.equals(that.groupDescription) : that.groupDescription != null) {
            return false;
        }
        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) {
            return false;
        }
        if (deviceName != null ? !deviceName.equals(that.deviceName) : that.deviceName != null) {
            return false;
        }
        if (deviceDescription != null ? !deviceDescription.equals(that.deviceDescription) : that.deviceDescription != null) {
            return false;
        }
        if (scheduleId != null ? !scheduleId.equals(that.scheduleId) : that.scheduleId != null) {
            return false;
        }
        if (scheduleName != null ? !scheduleName.equals(that.scheduleName) : that.scheduleName != null) {
            return false;
        }
        if (scheduleDescription != null ? !scheduleDescription.equals(that.scheduleDescription) : that.scheduleDescription != null) {
            return false;
        }
        if (panelId != null ? !panelId.equals(that.panelId) : that.panelId != null) {
            return false;
        }
        return opsFlag != null ? opsFlag.equals(that.opsFlag) : that.opsFlag == null;
    }

    @Override
    public int hashCode() {
        int result = securityActionId != null ? securityActionId.hashCode() : 0;
        result = 31 * result + (actorObjectId != null ? actorObjectId.hashCode() : 0);
        result = 31 * result + (securityGroupId != null ? securityGroupId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (groupObjectId != null ? groupObjectId.hashCode() : 0);
        result = 31 * result + (groupName != null ? groupName.hashCode() : 0);
        result = 31 * result + (groupDescription != null ? groupDescription.hashCode() : 0);
        result = 31 * result + (deviceId != null ? deviceId.hashCode() : 0);
        result = 31 * result + (deviceName != null ? deviceName.hashCode() : 0);
        result = 31 * result + (deviceDescription != null ? deviceDescription.hashCode() : 0);
        result = 31 * result + (scheduleId != null ? scheduleId.hashCode() : 0);
        result = 31 * result + (scheduleName != null ? scheduleName.hashCode() : 0);
        result = 31 * result + (scheduleDescription != null ? scheduleDescription.hashCode() : 0);
        result = 31 * result + (panelId != null ? panelId.hashCode() : 0);
        result = 31 * result + (opsFlag != null ? opsFlag.hashCode() : 0);
        return result;
    }
}
