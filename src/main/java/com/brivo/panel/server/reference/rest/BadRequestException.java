package com.brivo.panel.server.reference.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to be thrown if the request does not have all of the
 * required content.
 *
 * @author brandon
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request")
public class BadRequestException extends RuntimeException {
    private static final long serialVersionUID = 5430716896972483519L;

    public BadRequestException() {
    }

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(String message, Throwable t) {
        super(message, t);
    }

}
