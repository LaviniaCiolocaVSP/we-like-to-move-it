package com.brivo.panel.server.reference.dto.ui;

public class UiPhysicalPointDTO {
    private String name;
    private Long boardNumber;
    private Short pointAddress;
    private String description;
    private Long type;

    public UiPhysicalPointDTO() {
    }

    public UiPhysicalPointDTO(String name, Long boardNumber, Short pointAddress, Long type) {
        this.name = name;
        this.boardNumber = boardNumber;
        this.pointAddress = pointAddress;
        this.type = type;
        this.description = this.name + " (Board " + boardNumber + ")";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(Long boardNumber) {
        this.boardNumber = boardNumber;
    }

    public Short getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Short pointAddress) {
        this.pointAddress = pointAddress;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getType() {
        return type;
    }

    public void setType(Long type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "UiPhysicalPointDTO{" +
                "name='" + name + '\'' +
                ", boardNumber=" + boardNumber +
                ", pointAddress=" + pointAddress +
                ", description='" + description + '\'' +
                ", type=" + type +
                '}';
    }
}
