package com.brivo.panel.server.reference.model.event;

import org.springframework.web.socket.TextMessage;

import java.time.Instant;

public class UnknownEvent {
    private String eventType;
    private Instant eventTime;
    private TextMessage message;

    public String getEventType() {
        return eventType;
    }

    public void setEventType(final String eventType) {
        this.eventType = eventType;
    }

    public Instant getEventTime() {
        return eventTime;
    }

    public void setEventTime(final Instant eventTime) {
        this.eventTime = eventTime;
    }

    public TextMessage getMessage() {
        return message;
    }

    public void setMessage(final TextMessage message) {
        this.message = message;
    }
}
