package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Objects;

public class ScheduleBlock implements Comparable<ScheduleBlock> {
    private Integer startMinute;
    private Integer endMinute;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getStartMinute() {
        return startMinute;
    }

    public void setStartMinute(Integer startMinute) {
        this.startMinute = startMinute;
    }

    public Integer getEndMinute() {
        return endMinute;
    }

    public void setEndMinute(Integer endMinute) {
        this.endMinute = endMinute;
    }

    @Override
    public int compareTo(ScheduleBlock block2) {
        if (block2 == null) {
            throw new NullPointerException();
        }
        return (Objects.equals(getStartMinute(), block2.getStartMinute())) ? getEndMinute() - block2.getEndMinute()
                : getStartMinute() - block2.getStartMinute();
    }
}
