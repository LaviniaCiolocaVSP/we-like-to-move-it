package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.AccessCredential;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface CredentialRepository extends CrudRepository<AccessCredential, Long> {
    Optional<List<AccessCredential>> findByAccountId(long accountId);

    @Query(
            value = "SELECT coalesce(max(ac.access_credential_id),0) FROM brivo20.access_credential ac",
            nativeQuery = true
    )
    Optional<Long> getMaxAccessCredentialId();

    @Query(
            value = "select count(*) from access_credential c where c.account_id = :account_id",
            nativeQuery = true
    )
    long getAccessCredentialCountForAccount(@Param("account_id") long accountId);

    @Query(
            value = "select ts.* from ( select a.* from brivo20.access_credential a " +
                    "join brivo20.security_group_member s on s.object_id=a.owner_object_id join brivo20.security_group sg on sg.security_group_id=s.security_group_id " +
                    "join brivo20.object_permission o on sg.object_id= o.actor_object_id join brivo20.device d on d.object_id = o.object_id " +
                    "join brivo20.brain b on b.brain_id=d.brain_id where b.object_id = :panelOid group by a.access_credential_id ) ts",
            nativeQuery = true)
    Optional<List<AccessCredential>> findByPanelOid(@Param("panelOid") long panelOid);

}
