package com.brivo.panel.server.reference.dto;

import com.brivo.panel.server.reference.dto.ui.UiScheduleDataDTO;

import java.util.Collection;

public class CucumberScheduleDataCollection {

    private Collection<UiScheduleDataDTO> uiScheduleDataDTOS;
    private String message;

    public final static String ERROR_MESSAGE_PREFIX = "Error: ";
    public final static String SUCCESS_MESSAGE = "Sucessfully parsed schedule blocks string from cucumber test input to schedule blocks";

    public CucumberScheduleDataCollection() {
    }

    public CucumberScheduleDataCollection(Collection<UiScheduleDataDTO> uiScheduleDataDTOS, String message) {
        this.uiScheduleDataDTOS = uiScheduleDataDTOS;
        this.message = message;
    }

    public Collection<UiScheduleDataDTO> getUiScheduleDataDTOS() {
        return uiScheduleDataDTOS;
    }

    public void setUiScheduleDataDTOS(Collection<UiScheduleDataDTO> uiScheduleDataDTOS) {
        this.uiScheduleDataDTOS = uiScheduleDataDTOS;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "CucumberScheduleDataCollection{" +
                "uiScheduleDataDTOS=" + uiScheduleDataDTOS +
                ", message='" + message + '\'' +
                '}';
    }
}
