package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IAccessCredentialTypeRepository;

public interface BrivoAccessCredentialTypeRepository extends IAccessCredentialTypeRepository {
}
