package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Resolve Two Factor Event
 *
 * @see TwoFactorProcessor
 * <p>
 * The old processor did some work looking up credential reference IDs that is not done here.
 * They don't appear to be necessary anymore. If I'm wrong this will have to be enhanced.
 */
@Service
public class TwoFactorEventDataProcessor implements EventDataProcessor {
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private EventResolutionDao eventResolutionDao;

    @Autowired
    private ActorEventDataProcessor actorEventDataProcessor;

    /*@Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveTwoFactorEvent");

        if(EventType.INVALID_SECOND_FACTOR_UNKNOWN_CARD.equals(eventType))
        {
            handleUnknownSecondCard(event, panel);
        }

        Boolean result =  actorEventDataProcessor.resolveEvent(eventType, event, panel);

        logger.trace("<<< ResolveTwoFactorEvent");

        return result;
    }


    *//**
     * See if we can find the unknown second card. If
     * we do find it change the security action id.
     *//*
    private void handleUnknownSecondCard(SecurityLogEvent event, Panel panel)
    {
        List<EventResolutionData> list =
                this.eventResolutionDao.getCredentialByValueEvent(
                        event.getEventData().getUnknownWeigand(),
                        event.getEventData().getCredentialBitLength(),
                        panel);

        if(list.size() == 1)
        {
            event.setSecurityActionId((long) SecurityAction.ACTION_INVALID_SECOND_FACTOR_INVALID_CARD
                                                     .getSecurityActionValue());
        }
    }*/
}
