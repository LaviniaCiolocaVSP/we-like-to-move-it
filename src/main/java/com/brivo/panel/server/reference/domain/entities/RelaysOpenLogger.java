package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "relays_open_logger", schema = "brivo20", catalog = "onair")
public class RelaysOpenLogger {
    private long relaysOpenLoggerId;
    private String relayName;
    private Timestamp insertDate;
    private String status;

    @Id
    @Column(name = "relays_open_logger_id", nullable = false)
    public long getRelaysOpenLoggerId() {
        return relaysOpenLoggerId;
    }

    public void setRelaysOpenLoggerId(long relaysOpenLoggerId) {
        this.relaysOpenLoggerId = relaysOpenLoggerId;
    }

    @Column(name = "relay_name", length = 30)
    public String getRelayName() {
        return relayName;
    }

    public void setRelayName(String relayName) {
        this.relayName = relayName;
    }

    @Column(name = "insert_date", nullable = false)
    public Timestamp getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(Timestamp insertDate) {
        this.insertDate = insertDate;
    }

    @Column(name = "status", length = 10)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
