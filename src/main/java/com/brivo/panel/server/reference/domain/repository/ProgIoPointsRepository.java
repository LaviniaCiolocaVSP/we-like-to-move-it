package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface ProgIoPointsRepository extends CrudRepository<ProgIoPoints, Long> {
    @Query(
            value = "SELECT * " +
                    "FROM brivo20.prog_io_points " +
                    "WHERE panel_oid = :panelOid " +
                    "AND board_number = :boardNumber " +
                    "AND point_address in (:pointAddresses)",
            nativeQuery = true
    )
    Optional<List<ProgIoPoints>> getProgIoPointsForBoard(@Param("panelOid") long panelOid,
                                                         @Param("boardNumber") long boardNumber,
                                                         @Param("pointAddresses") List<Integer> pointAddresses);


    @Query(
            value = "UPDATE brivo20.prog_io_points " +
                    "SET inuse = :ioPointDoor " +
                    "WHERE panel_oid = :panelOid " +
                    "AND board_number = :boardNumber " +
                    "AND point_address in :pointAddresses " +
                    "AND inuse = :ioPointFree",
            nativeQuery = true
    )
    @Modifying
    void useIoPointsForDoor(@Param("pointAddresses") int[] pointAddresses,
                            @Param("panelOid") Long panelOid, @Param("boardNumber") Long boardNumber,
                            @Param("ioPointDoor") int ioPointDoor, @Param("ioPointFree") int ioPointFree);

    @Query(
            value = "UPDATE brivo20.prog_io_points " +
                    "SET inuse = :ioPointDevice " +
                    "WHERE panel_oid = :panelOid " +
                    "AND board_number = :boardNumber " +
                    "AND point_address = :pointAddress " +
                    "AND inuse = :ioPointFree",
            nativeQuery = true
    )
    @Modifying
    void useProgIoPointForDevice(@Param("panelOid") Long panelOid, @Param("boardNumber") Long boardNumber,
                                 @Param("pointAddress") Short pointAddress,
                                 @Param("ioPointDevice") int ioPointDevice, @Param("ioPointFree") int ioPointFree);


    @Query(
            value = "SELECT * " +
                    "FROM brivo20.prog_io_points " +
                    "WHERE panel_oid = :panelOid " +
                    "AND board_number = :boardNumber",
            nativeQuery = true
    )
    Optional<List<ProgIoPoints>> getProgIoPointsForBoardByPanelOidAndBoardNumber(@Param("panelOid") long panelOid,
                                                                                 @Param("boardNumber") long boardNumber);

    @Query(
            value = "SELECT pip.* " +
                    "FROM brivo20.prog_io_points pip, brivo20.io_point_template ipt,  brivo20.board b " +
                    "WHERE pip.panel_oid = :panelOid " +
                    "AND pip.type = :progIoPointType " +
                    "AND pip.inuse IN ( :ioPointFree, :ioPointDevice) " +
                    "AND b.panel_oid = pip.panel_oid " +
                    "AND b.board_number    = pip.board_number " +
                    "AND ipt.board_type    = b.board_type " +
                    "AND ipt.point_address = pip.point_address " +
                    "ORDER by pip.point_address",
            nativeQuery = true
    )
    Optional<List<ProgIoPoints>> getPhysicalPointsByPanelOid(@Param("panelOid") long panelOid,
                                                             @Param("progIoPointType") int progIoPointType,
                                                             @Param("ioPointDevice") int ioPointDevice,
                                                             @Param("ioPointFree") int ioPointFree);

    @Query(
            value = "SELECT pip.* " +
                    "FROM brivo20.prog_io_points pip " +
                    "WHERE pip.panel_oid = :panelOid " +
                    "AND pip.board_number = :boardNumber " +
                    "AND pip.point_address = :pointAddress",
            nativeQuery = true
    )
    ProgIoPoints findByPanelOidAndBoardNumberAndPointAddress(@Param("panelOid") long panelOid,
                                                             @Param("boardNumber") long boardNumber,
                                                             @Param("pointAddress") long pointAddress);

}
