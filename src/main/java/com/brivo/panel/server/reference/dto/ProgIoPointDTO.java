package com.brivo.panel.server.reference.dto;

import com.brivo.panel.server.reference.domain.entities.Board;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProgIoPointDTO {
    private final String name;
    private final Long boardNumber;
    private final Short pointAddress;
    private final Long type;
    private final Short eol;
    private final Short state;
    private final Short inuse;

    @JsonCreator
    public ProgIoPointDTO(@JsonProperty("name") final String name, @JsonProperty("boardNumber") final Long boardNumber,
                          @JsonProperty("pointAddress") final Short pointAddress, @JsonProperty("type") final Long type,
                          @JsonProperty("eol") final Short eol, @JsonProperty("state") final Short state,
                          @JsonProperty("inuse") final Short inuse) {
        this.name = name;
        this.boardNumber = boardNumber;
        this.pointAddress = pointAddress;
        this.type = type;
        this.eol = eol;
        this.state = state;
        this.inuse = inuse;
    }

    public String getName() {
        return name;
    }

    public Long getBoardNumber() {
        return boardNumber;
    }

    public Short getPointAddress() {
        return pointAddress;
    }

    public Long getType() {
        return type;
    }

    public Short getEol() {
        return eol;
    }

    public Short getState() {
        return state;
    }

    public Short getInuse() {
        return inuse;
    }
}
