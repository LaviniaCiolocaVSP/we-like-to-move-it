package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "report_parameter_value", schema = "brivo20", catalog = "onair")
public class ReportParameterValue {
    private long id;
    private long reportConfigurationId;
    private long reportParameterId;
    private String value;
    private Timestamp created;
    private Timestamp updated;
    private Long entityId;
    private String filterOperand;
    private String filterValue;
    private Boolean includeInReport;
    private ReportConfiguration reportConfigurationByReportConfigurationId;
    private ReportParameter reportParameterByReportParameterId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_configuration_id", nullable = false)
    public long getReportConfigurationId() {
        return reportConfigurationId;
    }

    public void setReportConfigurationId(long reportConfigurationId) {
        this.reportConfigurationId = reportConfigurationId;
    }

    @Basic
    @Column(name = "report_parameter_id", nullable = false)
    public long getReportParameterId() {
        return reportParameterId;
    }

    public void setReportParameterId(long reportParameterId) {
        this.reportParameterId = reportParameterId;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 27)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "entity_id", nullable = true)
    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    @Basic
    @Column(name = "filter_operand", nullable = true, length = 10)
    public String getFilterOperand() {
        return filterOperand;
    }

    public void setFilterOperand(String filterOperand) {
        this.filterOperand = filterOperand;
    }

    @Basic
    @Column(name = "filter_value", nullable = true, length = 256)
    public String getFilterValue() {
        return filterValue;
    }

    public void setFilterValue(String filterValue) {
        this.filterValue = filterValue;
    }

    @Basic
    @Column(name = "include_in_report", nullable = true)
    public Boolean getIncludeInReport() {
        return includeInReport;
    }

    public void setIncludeInReport(Boolean includeInReport) {
        this.includeInReport = includeInReport;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportParameterValue that = (ReportParameterValue) o;

        if (id != that.id) {
            return false;
        }
        if (reportConfigurationId != that.reportConfigurationId) {
            return false;
        }
        if (reportParameterId != that.reportParameterId) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (entityId != null ? !entityId.equals(that.entityId) : that.entityId != null) {
            return false;
        }
        if (filterOperand != null ? !filterOperand.equals(that.filterOperand) : that.filterOperand != null) {
            return false;
        }
        if (filterValue != null ? !filterValue.equals(that.filterValue) : that.filterValue != null) {
            return false;
        }
        return includeInReport != null ? includeInReport.equals(that.includeInReport) : that.includeInReport == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportConfigurationId ^ (reportConfigurationId >>> 32));
        result = 31 * result + (int) (reportParameterId ^ (reportParameterId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (entityId != null ? entityId.hashCode() : 0);
        result = 31 * result + (filterOperand != null ? filterOperand.hashCode() : 0);
        result = 31 * result + (filterValue != null ? filterValue.hashCode() : 0);
        result = 31 * result + (includeInReport != null ? includeInReport.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_configuration_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportConfiguration getReportConfigurationByReportConfigurationId() {
        return reportConfigurationByReportConfigurationId;
    }

    public void setReportConfigurationByReportConfigurationId(ReportConfiguration reportConfigurationByReportConfigurationId) {
        this.reportConfigurationByReportConfigurationId = reportConfigurationByReportConfigurationId;
    }

    @ManyToOne
    @JoinColumn(name = "report_parameter_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportParameter getReportParameterByReportParameterId() {
        return reportParameterByReportParameterId;
    }

    public void setReportParameterByReportParameterId(ReportParameter reportParameterByReportParameterId) {
        this.reportParameterByReportParameterId = reportParameterByReportParameterId;
    }
}
