package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "camera_param", schema = "brivo20", catalog = "onair")
public class CameraParam {
    private long cameraParamId;
    private long cameraId;
    private long cameraModelParamId;
    private String paramValue;
    private OvrCamera ovrCameraByCameraId;
    private CameraModelParam cameraModelParamByCameraModelParamId;

    @Id
    @Column(name = "camera_param_id", nullable = false)
    public long getCameraParamId() {
        return cameraParamId;
    }

    public void setCameraParamId(long cameraParamId) {
        this.cameraParamId = cameraParamId;
    }

    @Basic
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "camera_model_param_id", nullable = false)
    public long getCameraModelParamId() {
        return cameraModelParamId;
    }

    public void setCameraModelParamId(long cameraModelParamId) {
        this.cameraModelParamId = cameraModelParamId;
    }

    @Basic
    @Column(name = "param_value", nullable = true, length = 512)
    public String getParamValue() {
        return paramValue;
    }

    public void setParamValue(String paramValue) {
        this.paramValue = paramValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraParam that = (CameraParam) o;

        if (cameraParamId != that.cameraParamId) {
            return false;
        }
        if (cameraId != that.cameraId) {
            return false;
        }
        if (cameraModelParamId != that.cameraModelParamId) {
            return false;
        }
        return paramValue != null ? paramValue.equals(that.paramValue) : that.paramValue == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraParamId ^ (cameraParamId >>> 32));
        result = 31 * result + (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (int) (cameraModelParamId ^ (cameraModelParamId >>> 32));
        result = 31 * result + (paramValue != null ? paramValue.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id", nullable = false, insertable = false, updatable = false)
    public OvrCamera getOvrCameraByCameraId() {
        return ovrCameraByCameraId;
    }

    public void setOvrCameraByCameraId(OvrCamera ovrCameraByCameraId) {
        this.ovrCameraByCameraId = ovrCameraByCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "camera_model_param_id", referencedColumnName = "camera_model_param_id", nullable = false, insertable = false, updatable = false)
    public CameraModelParam getCameraModelParamByCameraModelParamId() {
        return cameraModelParamByCameraModelParamId;
    }

    public void setCameraModelParamByCameraModelParamId(CameraModelParam cameraModelParamByCameraModelParamId) {
        this.cameraModelParamByCameraModelParamId = cameraModelParamByCameraModelParamId;
    }
}
