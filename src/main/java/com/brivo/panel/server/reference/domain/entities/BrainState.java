package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "brain_state", schema = "brivo20", catalog = "onair")
public class BrainState {
    private long objectId;
    private String ipAddress;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Long nonce;
    private String firmwareVersion;
    private LocalDateTime panelChecked;
    private LocalDateTime personsChecked;
    private LocalDateTime schedulesChecked;
    private BrivoObject objectByBrivoObjectId;

    @Id
    @Column(name = "object_id", nullable = false, insertable = false, updatable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "ip_address", nullable = true, length = 20)
    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "nonce", nullable = true)
    public Long getNonce() {
        return nonce;
    }

    public void setNonce(Long nonce) {
        this.nonce = nonce;
    }

    @Basic
    @Column(name = "firmware_version", nullable = true, length = 32)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Basic
    @Column(name = "panel_checked", nullable = true)
    public LocalDateTime getPanelChecked() {
        return panelChecked;
    }

    public void setPanelChecked(LocalDateTime panelChecked) {
        this.panelChecked = panelChecked;
    }

    @Basic
    @Column(name = "persons_checked", nullable = true)
    public LocalDateTime getPersonsChecked() {
        return personsChecked;
    }

    public void setPersonsChecked(LocalDateTime personsChecked) {
        this.personsChecked = personsChecked;
    }

    @Basic
    @Column(name = "schedules_checked", nullable = true)
    public LocalDateTime getSchedulesChecked() {
        return schedulesChecked;
    }

    public void setSchedulesChecked(LocalDateTime schedulesChecked) {
        this.schedulesChecked = schedulesChecked;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrainState that = (BrainState) o;

        if (objectId != that.objectId) {
            return false;
        }
        if (ipAddress != null ? !ipAddress.equals(that.ipAddress) : that.ipAddress != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (nonce != null ? !nonce.equals(that.nonce) : that.nonce != null) {
            return false;
        }
        if (firmwareVersion != null ? !firmwareVersion.equals(that.firmwareVersion) : that.firmwareVersion != null) {
            return false;
        }
        if (panelChecked != null ? !panelChecked.equals(that.panelChecked) : that.panelChecked != null) {
            return false;
        }
        if (personsChecked != null ? !personsChecked.equals(that.personsChecked) : that.personsChecked != null) {
            return false;
        }
        return schedulesChecked != null ? schedulesChecked.equals(that.schedulesChecked) : that.schedulesChecked == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (ipAddress != null ? ipAddress.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (nonce != null ? nonce.hashCode() : 0);
        result = 31 * result + (firmwareVersion != null ? firmwareVersion.hashCode() : 0);
        result = 31 * result + (panelChecked != null ? panelChecked.hashCode() : 0);
        result = 31 * result + (personsChecked != null ? personsChecked.hashCode() : 0);
        result = 31 * result + (schedulesChecked != null ? schedulesChecked.hashCode() : 0);
        return result;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }
}
