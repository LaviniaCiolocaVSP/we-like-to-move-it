package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ProgrammableDeviceOutput {
    protected Integer boardAddress;
    protected Integer pointAddress;
    protected Integer behavior;
    protected Integer behaviorArgument;
    protected Integer outputType;
    protected Long objectId;

    public ProgrammableDeviceOutput() {

    }

    public ProgrammableDeviceOutput(Integer boardAddress, Integer pointAddress, Integer behavior, Integer behaviorArgument,
                                    Integer outputType, Long objectId) {
        this.boardAddress = boardAddress;
        this.pointAddress = pointAddress;
        this.behavior = behavior;
        this.behaviorArgument = behaviorArgument;
        this.outputType = outputType;
        this.objectId = objectId;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getBoardAddress() {
        return boardAddress;
    }

    public void setBoardAddress(Integer boardAddress) {
        this.boardAddress = boardAddress;
    }

    public Integer getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Integer pointAddress) {
        this.pointAddress = pointAddress;
    }

    public Integer getBehavior() {
        return behavior;
    }

    public void setBehavior(Integer behavior) {
        this.behavior = behavior;
    }

    public Integer getBehaviorArgument() {
        return behaviorArgument;
    }

    public void setBehaviorArgument(Integer behaviorArgument) {
        this.behaviorArgument = behaviorArgument;
    }

    public Integer getOutputType() {
        return outputType;
    }

    public void setOutputType(Integer outputType) {
        this.outputType = outputType;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }
}
