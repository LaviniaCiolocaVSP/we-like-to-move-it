package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "login_session", schema = "brivo20", catalog = "onair")
public class LoginSession {
    private long usersId;
    private String sessionId;
    private Timestamp lastAccess;
    private Long proxyUsersId;
    private String remoteAddress;
    private long applicationId;
    private Users usersByUsersId;
    private Users usersByProxyUsersId;
    private Application applicationByApplicationId;
    private Collection<LoginSessionAttribute> loginSessionAttributesBySessionId;

    @Basic
    @Column(name = "users_id", nullable = false)
    public long getUsersId() {
        return usersId;
    }

    public void setUsersId(long usersId) {
        this.usersId = usersId;
    }

    @Id
    @Column(name = "session_id", nullable = false, length = 40)
    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Basic
    @Column(name = "last_access", nullable = false)
    public Timestamp getLastAccess() {
        return lastAccess;
    }

    public void setLastAccess(Timestamp lastAccess) {
        this.lastAccess = lastAccess;
    }

    @Basic
    @Column(name = "proxy_users_id", nullable = true)
    public Long getProxyUsersId() {
        return proxyUsersId;
    }

    public void setProxyUsersId(Long proxyUsersId) {
        this.proxyUsersId = proxyUsersId;
    }

    @Basic
    @Column(name = "remote_address", nullable = true, length = 40)
    public String getRemoteAddress() {
        return remoteAddress;
    }

    public void setRemoteAddress(String remoteAddress) {
        this.remoteAddress = remoteAddress;
    }

    @Basic
    @Column(name = "application_id", nullable = false)
    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoginSession that = (LoginSession) o;

        if (usersId != that.usersId) {
            return false;
        }
        if (applicationId != that.applicationId) {
            return false;
        }
        if (sessionId != null ? !sessionId.equals(that.sessionId) : that.sessionId != null) {
            return false;
        }
        if (lastAccess != null ? !lastAccess.equals(that.lastAccess) : that.lastAccess != null) {
            return false;
        }
        if (proxyUsersId != null ? !proxyUsersId.equals(that.proxyUsersId) : that.proxyUsersId != null) {
            return false;
        }
        return remoteAddress != null ? remoteAddress.equals(that.remoteAddress) : that.remoteAddress == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (usersId ^ (usersId >>> 32));
        result = 31 * result + (sessionId != null ? sessionId.hashCode() : 0);
        result = 31 * result + (lastAccess != null ? lastAccess.hashCode() : 0);
        result = 31 * result + (proxyUsersId != null ? proxyUsersId.hashCode() : 0);
        result = 31 * result + (remoteAddress != null ? remoteAddress.hashCode() : 0);
        result = 31 * result + (int) (applicationId ^ (applicationId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "users_id", referencedColumnName = "users_id", nullable = false, insertable = false, updatable = false)
    public Users getUsersByUsersId() {
        return usersByUsersId;
    }

    public void setUsersByUsersId(Users usersByUsersId) {
        this.usersByUsersId = usersByUsersId;
    }

    @ManyToOne
    @JoinColumn(name = "proxy_users_id", referencedColumnName = "users_id", insertable = false, updatable = false)
    public Users getUsersByProxyUsersId() {
        return usersByProxyUsersId;
    }

    public void setUsersByProxyUsersId(Users usersByProxyUsersId) {
        this.usersByProxyUsersId = usersByProxyUsersId;
    }

    @ManyToOne
    @JoinColumn(name = "application_id", referencedColumnName = "application_id", nullable = false, insertable = false, updatable = false)
    public Application getApplicationByApplicationId() {
        return applicationByApplicationId;
    }

    public void setApplicationByApplicationId(Application applicationByApplicationId) {
        this.applicationByApplicationId = applicationByApplicationId;
    }

    @OneToMany(mappedBy = "loginSessionBySessionId")
    public Collection<LoginSessionAttribute> getLoginSessionAttributesBySessionId() {
        return loginSessionAttributesBySessionId;
    }

    public void setLoginSessionAttributesBySessionId(Collection<LoginSessionAttribute> loginSessionAttributesBySessionId) {
        this.loginSessionAttributesBySessionId = loginSessionAttributesBySessionId;
    }
}
