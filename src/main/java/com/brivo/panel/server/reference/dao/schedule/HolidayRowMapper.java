package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.model.schedule.Holiday;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.Date;


@Component
public class HolidayRowMapper implements RowMapper<Holiday> {
    @Override
    public Holiday mapRow(ResultSet rs, int rowNum) throws SQLException {
        Long holidayId = rs.getLong("holidayId");
        Date startDate = rs.getTimestamp("startDate");
        Date stopDate = rs.getTimestamp("stopDate");

        Instant start = (startDate == null) ? null : startDate.toInstant();
        Instant end = (stopDate == null) ? null : stopDate.toInstant();

        Holiday holiday = new Holiday();
        holiday.setHolidayId(holidayId);
        holiday.setStartDate(start);
        holiday.setEndDate(end);

        return holiday;
    }
}
