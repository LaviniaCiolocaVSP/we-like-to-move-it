package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.Floor;
import com.brivo.panel.server.reference.model.hardware.FloorElevator;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class FloorExtractor implements ResultSetExtractor<List<Floor>> {
    @Override
    public List<Floor> extractData(ResultSet rs) throws SQLException, DataAccessException {
        Map<Long, Floor> floorMap = new HashMap<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID"); // Floor Id
            Long elevatorId = rs.getLong("elevatorOID");
            Integer boardNumber = rs.getInt("boardNumber");
            Integer pointAddress = rs.getInt("pointAddress");

            Floor floor = floorMap.get(deviceId);
            if (floor == null) {
                floor = new Floor();
                floor.setDeviceId(deviceId);
                floor.setFloorElevators(new ArrayList<FloorElevator>());

                long unlockScheduleId = 0;
                rs.getLong("lockedDown");
                if (rs.wasNull()) {
                    unlockScheduleId = rs.getLong("unlockScheduleId");
                }
                floor.setUnlockScheduleId(unlockScheduleId);

                floorMap.put(deviceId, floor);
            }

            FloorElevator floorElevator = new FloorElevator();

            floorElevator.setElevatorId(elevatorId);
            floorElevator.setBoardAddress(boardNumber);
            floorElevator.setPointAddress(pointAddress);

            floor.getFloorElevators().add(floorElevator);

        }

        return new ArrayList<>(floorMap.values());
    }

}

