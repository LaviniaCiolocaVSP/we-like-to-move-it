package com.brivo.panel.server.reference.service.util;

import org.springframework.util.Assert;

import java.io.File;

public final class FolderUtils {

    public static void checkFolderExistence(final String folderPath, final String folderType) {
        final File folder = new File(folderPath);
        if (folder.exists()) {
            return;
        }

        Assert.isTrue(folder.mkdirs(), "Cannot create the " + folderType + " folder");
    }
}
