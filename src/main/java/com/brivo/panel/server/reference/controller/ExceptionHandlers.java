package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class ExceptionHandlers {

    @ExceptionHandler({
            IllegalArgumentException.class,
            IllegalStateException.class,
    })
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public @ResponseBody
    MessageDTO badRequest(final RuntimeException e) {
        e.printStackTrace();
        return new MessageDTO(e.getMessage());
    }

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public @ResponseBody
    MessageDTO ise(final Exception e) {
        e.printStackTrace();
        return new MessageDTO(e.getMessage());
    }
}
