package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;

@Entity
public class Telephone {
    private long telephoneId;
    private Long ownerObjectId;
    private String type;
    private String value;
    private String extension;
    private Timestamp created;
    private Timestamp updated;
    private BrivoObject objectByOwnerBrivoObjectId;

    @Id
    @Column(name = "telephone_id", nullable = false)
    public long getTelephoneId() {
        return telephoneId;
    }

    public void setTelephoneId(long telephoneId) {
        this.telephoneId = telephoneId;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = true)
    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Basic
    @Column(name = "type", nullable = true, length = 32)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Basic
    @Column(name = "value", nullable = true, length = 512)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "extension", nullable = true, length = 10)
    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Telephone telephone = (Telephone) o;

        if (telephoneId != telephone.telephoneId) {
            return false;
        }
        if (ownerObjectId != null ? !ownerObjectId.equals(telephone.ownerObjectId) : telephone.ownerObjectId != null) {
            return false;
        }
        if (type != null ? !type.equals(telephone.type) : telephone.type != null) {
            return false;
        }
        if (value != null ? !value.equals(telephone.value) : telephone.value != null) {
            return false;
        }
        if (extension != null ? !extension.equals(telephone.extension) : telephone.extension != null) {
            return false;
        }
        if (created != null ? !created.equals(telephone.created) : telephone.created != null) {
            return false;
        }
        return updated != null ? updated.equals(telephone.updated) : telephone.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (telephoneId ^ (telephoneId >>> 32));
        result = 31 * result + (ownerObjectId != null ? ownerObjectId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (extension != null ? extension.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id", insertable = false, updatable = false)
    public BrivoObject getObjectByOwnerBrivoObjectId() {
        return objectByOwnerBrivoObjectId;
    }

    public void setObjectByOwnerBrivoObjectId(BrivoObject objectByOwnerBrivoObjectId) {
        this.objectByOwnerBrivoObjectId = objectByOwnerBrivoObjectId;
    }
}
