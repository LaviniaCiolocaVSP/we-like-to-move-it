package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "security_group_antipassback", schema = "brivo20", catalog = "onair")
public class SecurityGroupAntipassback {
    private long securityGroupId;
    private Short immunity;
    private Long resetTime;
    private SecurityGroup securityGroupBySecurityGroupId;

    @Id
    @Column(name = "security_group_id", nullable = false, insertable = false, updatable = false)
    public long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "immunity", nullable = true)
    public Short getImmunity() {
        return immunity;
    }

    public void setImmunity(Short immunity) {
        this.immunity = immunity;
    }

    @Basic
    @Column(name = "reset_time", nullable = true)
    public Long getResetTime() {
        return resetTime;
    }

    public void setResetTime(Long resetTime) {
        this.resetTime = resetTime;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = SecurityGroup.class, optional = false)
    @JoinColumn(name = "security_group_id", referencedColumnName = "security_group_id", insertable = true, updatable = true)
    public SecurityGroup getSecurityGroupBySecurityGroupId() {
        return securityGroupBySecurityGroupId;
    }

    public void setSecurityGroupBySecurityGroupId(SecurityGroup securityGroupBySecurityGroupId) {
        this.securityGroupBySecurityGroupId = securityGroupBySecurityGroupId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityGroupAntipassback that = (SecurityGroupAntipassback) o;

        if (securityGroupBySecurityGroupId.getSecurityGroupId() != that.getSecurityGroupBySecurityGroupId().getSecurityGroupId()) {
            return false;
        }
        if (immunity != null ? !immunity.equals(that.immunity) : that.immunity != null) {
            return false;
        }
        return resetTime != null ? resetTime.equals(that.resetTime) : that.resetTime == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityGroupBySecurityGroupId.getSecurityGroupId() ^ (securityGroupBySecurityGroupId.getSecurityGroupId() >>> 32));
        result = 31 * result + (immunity != null ? immunity.hashCode() : 0);
        result = 31 * result + (resetTime != null ? resetTime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SecurityGroupAntipassback{" +
                "securityGroupId=" + securityGroupId +
                ", immunity=" + immunity +
                ", resetTime=" + resetTime +
                '}';
    }
}
