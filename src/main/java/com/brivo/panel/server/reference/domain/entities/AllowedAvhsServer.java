package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "allowed_avhs_server", schema = "brivo20", catalog = "onair")
public class AllowedAvhsServer {
    private long allowedAvhsServerId;
    private Long accountId;
    private Long dealerAccountId;
    private String cameraSerialNumber;
    private long avhsServerId;
    private Account accountByAccountId;
    private Account accountByDealerAccountId;

    @Id
    @Column(name = "allowed_avhs_server_id", nullable = false)
    public long getAllowedAvhsServerId() {
        return allowedAvhsServerId;
    }

    public void setAllowedAvhsServerId(long allowedAvhsServerId) {
        this.allowedAvhsServerId = allowedAvhsServerId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Basic
    @Column(name = "camera_serial_number", nullable = true, length = 12)
    public String getCameraSerialNumber() {
        return cameraSerialNumber;
    }

    public void setCameraSerialNumber(String cameraSerialNumber) {
        this.cameraSerialNumber = cameraSerialNumber;
    }

    @Basic
    @Column(name = "avhs_server_id", nullable = false)
    public long getAvhsServerId() {
        return avhsServerId;
    }

    public void setAvhsServerId(long avhsServerId) {
        this.avhsServerId = avhsServerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AllowedAvhsServer that = (AllowedAvhsServer) o;

        if (allowedAvhsServerId != that.allowedAvhsServerId) {
            return false;
        }
        if (avhsServerId != that.avhsServerId) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (dealerAccountId != null ? !dealerAccountId.equals(that.dealerAccountId) : that.dealerAccountId != null) {
            return false;
        }
        return cameraSerialNumber != null ? cameraSerialNumber.equals(that.cameraSerialNumber) : that.cameraSerialNumber == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (allowedAvhsServerId ^ (allowedAvhsServerId >>> 32));
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (dealerAccountId != null ? dealerAccountId.hashCode() : 0);
        result = 31 * result + (cameraSerialNumber != null ? cameraSerialNumber.hashCode() : 0);
        result = 31 * result + (int) (avhsServerId ^ (avhsServerId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "dealer_account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByDealerAccountId() {
        return accountByDealerAccountId;
    }

    public void setAccountByDealerAccountId(Account accountByDealerAccountId) {
        this.accountByDealerAccountId = accountByDealerAccountId;
    }
}
