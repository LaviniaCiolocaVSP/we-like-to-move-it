package com.brivo.panel.server.reference.model.hardware;

import java.util.List;

public class OsdpSettings {
    private Integer errorDetectionMethod = 1;
    private List<Integer> pdAddress;
    private List<PdAddressMap> pdAddressMaps;

    public OsdpSettings() {
    }

    public Integer getErrorDetectionMethod() {
        return errorDetectionMethod;
    }

    public void setErrorDetectionMethod(Integer errorDetectionMethod) {
        this.errorDetectionMethod = errorDetectionMethod;
    }

    public List<Integer> getPdAddress() {
        return pdAddress;
    }

    public void setPdAddress(List<Integer> array) {
        this.pdAddress = array;
    }

    public List<PdAddressMap> getPdAddressMaps() {
        return pdAddressMaps;
    }

    public void setPdAddressMaps(List<PdAddressMap> pdAddressMaps) {
        this.pdAddressMaps = pdAddressMaps;
    }
}
