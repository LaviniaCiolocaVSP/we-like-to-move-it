package com.brivo.panel.server.reference.dao.panel;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum BrainConnectionQuery implements FileQuery {
    CLEAR_BRAIN_CONNECTION_STATUS,
    UPDATE_BRAIN_CONNECTION_STATUS,
    CLEAR_ALL_SERVER_BRAIN_CONNECTION_STATUS,
    INSERT_BRAIN_CONNECTION_STATUS,
    UPDATE_BRAIN_STATE,
    INSERT_BRAIN_STATE,
    GET_REPORTED_FIRMWARE,
    GET_BRAIN_CONNECTION_STATUS;

    private String query;

    BrainConnectionQuery() {
        this.query = QueryUtils.getQueryText(this.getClass(), this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
