package com.brivo.panel.server.reference.dao.panel;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum PanelQuery implements FileQuery {
    SELECT_PANEL_BY_SERIAL,
    SELECT_PANEL_BY_OBJECT_ID,
    SELECT_PANEL_BY_DEVICE,
    SELECT_FLUSHABLE_PANELS_BY_ACCOUNT_ID,
    SELECT_FLUSHABLE_PANEL_IDS_BY_ACCOUNT_ID,
    RETRIEVE_BRAINS_BY_SCHEDULES,
    RETRIEVE_BRAINS_BY_DEVICES,
    RETRIEVE_BRAINS_BY_USERS,
    RETRIEVE_BRAINS_BY_GROUPS,
    RETRIEVE_BRAINS_BY_DEVICE_SCHEDULES,
    UPDATE_PERSONS_CHANGED_FOR_BRAIN_IDS,
    UPDATE_PANEL_CHANGED_FOR_BRAIN_IDS,
    UPDATE_SCHEDULES_CHANGED_FOR_BRAIN_IDS,
    RETRIEVE_FLUSHABLE_PANELS_FROM_PANEL_LIST,
    UPDATE_PANEL_CHANGED_COLUMNS_FOR_BRAIN_IDS,
    RETRIEVE_PANELS_BY_ACCOUNT_ID,
    RETRIEVE_USER_ID_BY_CREDENTIAL_OBJECT_ID,
    SELECT_DEVICE_OBJECT_ID_BY_DEVICE_ID,
    DELETE_PANEL;

    private String query;

    PanelQuery() {
        this.query = QueryUtils.getQueryText(PanelQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
