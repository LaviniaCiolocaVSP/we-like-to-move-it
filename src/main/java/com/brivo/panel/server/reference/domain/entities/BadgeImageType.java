package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_image_type", schema = "brivo20", catalog = "onair")
public class BadgeImageType {
    private long badgeImageTypeId;
    private String name;
    private Collection<BadgeImageItem> badgeImageItemsByBadgeImageTypeId;

    @Id
    @Column(name = "badge_image_type_id", nullable = false)
    public long getBadgeImageTypeId() {
        return badgeImageTypeId;
    }

    public void setBadgeImageTypeId(long badgeImageTypeId) {
        this.badgeImageTypeId = badgeImageTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeImageType that = (BadgeImageType) o;

        if (badgeImageTypeId != that.badgeImageTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeImageTypeId ^ (badgeImageTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeImageTypeByBadgeImageTypeId")
    public Collection<BadgeImageItem> getBadgeImageItemsByBadgeImageTypeId() {
        return badgeImageItemsByBadgeImageTypeId;
    }

    public void setBadgeImageItemsByBadgeImageTypeId(Collection<BadgeImageItem> badgeImageItemsByBadgeImageTypeId) {
        this.badgeImageItemsByBadgeImageTypeId = badgeImageItemsByBadgeImageTypeId;
    }
}
