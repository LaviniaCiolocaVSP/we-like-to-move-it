package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.auth.PanelPrincipal;
import com.brivo.panel.server.reference.model.credential.CredentialData;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.service.CredentialService;
import org.apache.commons.lang.time.StopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
public class CredentialController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CredentialController.class);
    @Autowired
    private CredentialService credentialService;

    /**
     * Get Credentials. This version returns credentials page by page.
     */
    @RequestMapping(path = "/paneldata/credentials",
            method = RequestMethod.GET,
            produces = "application/json")
    public CredentialData getCredentials(
            @AuthenticationPrincipal PanelPrincipal panelPrincipal,
            @RequestParam(required = false, defaultValue = "0") Long offset,
            @RequestParam(required = false, defaultValue = "2000") Integer pageSize) {

        LOGGER.debug("Credentials request: offset: {} pageSize: {}", offset, pageSize);
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();

        Panel panel = panelPrincipal.getPanel();
        CredentialData data = credentialService.getAllCredentials(panel, offset, pageSize);
        data.setLastUpdate(Instant.now());

        stopWatch.stop();

        return data;
    }

}
