package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.ISecurityGroupRepository;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface SecurityGroupRepository extends ISecurityGroupRepository {
    @Query(
            value = "select count(*) from security_group g where g.security_group_type_id != 1 and g.account_id = account_id",
            nativeQuery = true
    )
    long getSecurityGroupCountForAccount(@Param("account_id") long accountId);

    @Query(
            value = "SELECT sg.* " +
                    "FROM brivo20.security_group sg " +
                    "WHERE sg.object_id = :objectId",
            nativeQuery = true
    )
    Optional<SecurityGroup> findByObjectId(@Param("objectId") Long objectId);
}
