package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.holiday.HolidayDao;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.Holiday;
import com.brivo.panel.server.reference.domain.entities.ScheduleHolidayMap;
import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupTypes;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.HolidayRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupRepository;
import com.brivo.panel.server.reference.dto.HolidayDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiHolidayDTO;
import com.brivo.panel.server.reference.dto.ui.UiScheduleDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


@Service
public class HolidayService extends CommonService {
    private static final Logger LOGGER = LoggerFactory.getLogger(HolidayService.class);
    private final static String UNIVERSAL_SITE_NAME = "Universal";
    private static final Long UNIVERSAL_SITE_ID = 0L;
    private final HolidayRepository holidayRepository;
    private final SecurityGroupRepository securityGroupRepository;
    private final AccountRepository accountRepository;
    private final HolidayDao holidayDao;

    @Autowired
    public HolidayService(HolidayRepository holidayRepository, SecurityGroupRepository securityGroupRepository,
                          AccountRepository accountRepository, HolidayDao holidayDao) {
        this.holidayRepository = holidayRepository;
        this.securityGroupRepository = securityGroupRepository;
        this.accountRepository = accountRepository;
        this.holidayDao = holidayDao;
    }

    public Collection<HolidayDTO> getHolidayDTOS(final Collection<Holiday> holidays) {
        return holidays.stream()
                       .map(convertHoliday())
                       .collect(Collectors.toSet());
    }

    private Function<Holiday, HolidayDTO> convertHoliday() {
        return holiday -> new HolidayDTO(getOptionalLongValue(holiday.getHolidayId()), holiday.getName(),
                                         holiday.getSiteId(), holiday.getStartDate(), holiday.getStopDate(), holiday.getCadmHolidayId());
    }

    public Set<UiHolidayDTO> getUiHolidayDTOS(final Collection<Holiday> holidays) {
        return holidays.stream()
                       .map(convertHolidayUI())
                       .collect(Collectors.toSet());
    }

    private Function<Holiday, UiHolidayDTO> convertHolidayUI() {
        return holiday -> {
            String siteName = UNIVERSAL_SITE_NAME;
            long siteId = UNIVERSAL_SITE_ID;
            if (holiday.getSecurityGroupBySiteId() != null && holiday.getSiteId() != 0) {
                siteName = holiday.getSecurityGroupBySiteId().getName();
                siteId = holiday.getSiteId();
            }
            return new UiHolidayDTO(holiday.getHolidayId(), holiday.getName(), siteId, holiday.getStartDate(),
                                    holiday.getStopDate(), holiday.getCadmHolidayId(),
                                    holiday.getAccountId(), siteName);
        };
    }


    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Set<UiHolidayDTO> getUiHolidaysForAccountId(final Long accountId) {
        final List<Holiday> holidayList;
        final Set<UiHolidayDTO> uiHolidayDTOSet;

        holidayList = holidayRepository.findByAccountId(accountId);
        final Account account = accountRepository.findById(accountId).get();
        LOGGER.info(account.getName() + " " + account.getAccountId());
        uiHolidayDTOSet = getUiHolidayDTOS(holidayList);
        return uiHolidayDTOSet;
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO create(UiHolidayDTO uiHolidayDTO) {

        Holiday holiday = createHolidayAndPersistToDatabase(uiHolidayDTO);

        return new MessageDTO("Holiday '" + holiday.getName() + "' was successfully created!");
    }

    public Holiday createHolidayAndPersistToDatabase(UiHolidayDTO uiHolidayDTO) {
        final Optional<Long> holiday_id = holidayRepository.getMaxHolidayId();
        uiHolidayDTO.setHolidayId(holiday_id.get() + 1);

        final Optional<Long> cadmHoliday_id = holidayRepository.getMaxCadmHolidayId();
        uiHolidayDTO.setCadmHolidayId(cadmHoliday_id.get() + 1);

        validateNewHoliday(uiHolidayDTO);

        final Holiday holiday = createHoliday(uiHolidayDTO);
        final Account account = accountRepository.findById(uiHolidayDTO.getAccountId()).get();
        holiday.setAccountByAccountId(account);

        Collection<ScheduleHolidayMap> scheduleHolidayMapsByHolidayId = new ArrayList<>();
        uiHolidayDTO.getScheduleDTOSet()
                    .forEach(scheduledBlock -> createAndSetScheduleHolidayMap(holiday, scheduleHolidayMapsByHolidayId, scheduledBlock));

        holiday.setScheduleHolidayMapsByHolidayId(scheduleHolidayMapsByHolidayId);

        final Holiday savedHoliday = holidayRepository.save(holiday);

        LOGGER.debug("Holiday " + savedHoliday.getName() + " successfully persisted to database...");
        return savedHoliday;
    }

    private void createAndSetScheduleHolidayMap(Holiday holiday, Collection<ScheduleHolidayMap> scheduleHolidayMapsByScheduleId, UiScheduleDTO schedule) {
        ScheduleHolidayMap scheduleHolidayMap = new ScheduleHolidayMap();
        scheduleHolidayMap.setHolidayOid(holiday.getHolidayId());
        scheduleHolidayMap.setScheduleOid(schedule.getScheduleId());
        scheduleHolidayMap.setHolidayByHolidayOid(holiday);
        scheduleHolidayMapsByScheduleId.add(scheduleHolidayMap);
    }

    private Holiday createHoliday(final UiHolidayDTO uiHolidayDTO) {
        Holiday holiday = new Holiday();

        holiday.setHolidayId(uiHolidayDTO.getHolidayId());
        holiday.setName(uiHolidayDTO.getName());
        holiday.setStartDate(uiHolidayDTO.getFrom());
        holiday.setStopDate(uiHolidayDTO.getTo());
        if (uiHolidayDTO.getSiteId() != 0) {
            final SecurityGroup securityGroup = securityGroupRepository.findByObjectId(uiHolidayDTO.getSiteId()).get();
            holiday.setSiteId(uiHolidayDTO.getSiteId());
            holiday.setSecurityGroupBySiteId(securityGroup);
        }
        return holiday;
    }


    private void validateNewHoliday(final UiHolidayDTO uiHolidayDTO) {
        validateHoliday(uiHolidayDTO);
    }

    private void validateHoliday(final UiHolidayDTO uiHolidayDTO) {
        final String name = uiHolidayDTO.getName();
        Assert.hasLength(name, "Holiday must have a name");

        final LocalDateTime from = uiHolidayDTO.getFrom();
        Assert.notNull(from, "Holiday must have a start date");

        final LocalDateTime to = uiHolidayDTO.getTo();
        Assert.notNull(from, "Holiday must have an end date");

        validateSiteId(uiHolidayDTO);
        validateHolidayAccount(uiHolidayDTO);
        //TODO add other validations, if needed
    }

    private void validateSiteId(UiHolidayDTO uiHolidayDTO) {
        final long site_id = uiHolidayDTO.getSiteId();
        if (site_id != 0) {
            Optional<SecurityGroup> securityGroup = securityGroupRepository.findByObjectId(site_id);
            Assert.state(securityGroup.isPresent(),
                         "There is no security group with the id " + site_id);
            Assert.state(securityGroup.get().getSecurityGroupTypeBySecurityGroupTypeId().getSecurityGroupTypeId() == SecurityGroupTypes.DEVICE_GROUP.getSecurityGroupTypeID(),
                         "Security group type is not DEVICE");
        }
    }

    private void validateHolidayAccount(UiHolidayDTO uiHolidayDTO) {
        final long accountId = uiHolidayDTO.getAccountId();
        Assert.state(accountRepository.findById(accountId).isPresent(),
                     "There is no account with the id " + accountId);
    }


    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO delete(List<Long> holidayIds) {
        LOGGER.debug("Deleting holidays with ids {}...", StringUtils.join(holidayIds.toArray(), "; "));
        final Optional<Holiday> holiday = holidayRepository.findByHolidayId(holidayIds.get(0));

        for (Long id : holidayIds) {
            validateHolidayId(id);
            // holidayRepository.deleteById(id);
            holidayDao.deleteHolidayById(id);
            LOGGER.debug("Holiday {} successfully deleted from database...", id);
        }

        return new MessageDTO("Holidays with ids '" + StringUtils.join(holidayIds.toArray(), "; ") + "' were successfully deleted!");
    }

    public void validateHolidayId(long holiday_id) {
        Assert.state(holidayRepository.findById(holiday_id).isPresent(),
                     "There is no holiday with the id " + holiday_id);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO update(UiHolidayDTO uiHolidayDTO) {

        validateExistingHoliday(uiHolidayDTO);

        Holiday existingHoliday = holidayRepository.findById(uiHolidayDTO.getHolidayId()).get();

        holidayDao.deleteHolidayScheduleMapById(existingHoliday.getHolidayId());

        existingHoliday.setName(uiHolidayDTO.getName());
        existingHoliday.setStartDate(uiHolidayDTO.getFrom());
        existingHoliday.setStopDate(uiHolidayDTO.getTo());
        existingHoliday.setSiteId(uiHolidayDTO.getSiteId());

        Collection<ScheduleHolidayMap> scheduleHolidayMapsByHolidayId = new ArrayList<>();
        uiHolidayDTO.getScheduleDTOSet()
                    .forEach(scheduledBlock -> createAndSetScheduleHolidayMap(existingHoliday, scheduleHolidayMapsByHolidayId, scheduledBlock));

        existingHoliday.setScheduleHolidayMapsByHolidayId(scheduleHolidayMapsByHolidayId);

        final Holiday updatedHoliday = holidayRepository.save(existingHoliday);

        LOGGER.debug("Holiday {} successfully updated...", updatedHoliday.getName());

        return new MessageDTO("Holiday with id '" + updatedHoliday.getHolidayId() + "' was successfully updated!");
    }

    private void validateExistingHoliday(final UiHolidayDTO uiHolidayDTO) {
        validateHoliday(uiHolidayDTO);
        validateHolidayId(uiHolidayDTO.getHolidayId());
    }

}
