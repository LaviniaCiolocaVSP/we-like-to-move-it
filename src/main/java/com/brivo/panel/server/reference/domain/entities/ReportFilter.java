package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_filter", schema = "brivo20", catalog = "onair")
public class ReportFilter {
    private long reportId;
    private long filterId;
    private long target;
    private long type;
    private String leftValue;
    private String rightValue;
    private Long fieldId;
    private Collection<Report> reportFilterByReportId;

    @Id
    @Column(name = "report_id", nullable = false)
    public long getReportId() {
        return reportId;
    }

    public void setReportId(long reportId) {
        this.reportId = reportId;
    }

    @Basic
    @Column(name = "filter_id", nullable = false)
    public long getFilterId() {
        return filterId;
    }

    public void setFilterId(long filterId) {
        this.filterId = filterId;
    }

    @Basic
    @Column(name = "target", nullable = false)
    public long getTarget() {
        return target;
    }

    public void setTarget(long target) {
        this.target = target;
    }

    @Basic
    @Column(name = "type", nullable = false)
    public long getType() {
        return type;
    }

    public void setType(long type) {
        this.type = type;
    }

    @Basic
    @Column(name = "left_value", nullable = false, length = 4000)
    public String getLeftValue() {
        return leftValue;
    }

    public void setLeftValue(String leftValue) {
        this.leftValue = leftValue;
    }

    @Basic
    @Column(name = "right_value", nullable = true, length = 32)
    public String getRightValue() {
        return rightValue;
    }

    public void setRightValue(String rightValue) {
        this.rightValue = rightValue;
    }

    @Basic
    @Column(name = "field_id", nullable = true)
    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportFilter that = (ReportFilter) o;

        if (reportId != that.reportId) {
            return false;
        }
        if (filterId != that.filterId) {
            return false;
        }
        if (target != that.target) {
            return false;
        }
        if (type != that.type) {
            return false;
        }
        if (leftValue != null ? !leftValue.equals(that.leftValue) : that.leftValue != null) {
            return false;
        }
        if (rightValue != null ? !rightValue.equals(that.rightValue) : that.rightValue != null) {
            return false;
        }
        return fieldId != null ? fieldId.equals(that.fieldId) : that.fieldId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (reportId ^ (reportId >>> 32));
        result = 31 * result + (int) (filterId ^ (filterId >>> 32));
        result = 31 * result + (int) (target ^ (target >>> 32));
        result = 31 * result + (int) (type ^ (type >>> 32));
        result = 31 * result + (leftValue != null ? leftValue.hashCode() : 0);
        result = 31 * result + (rightValue != null ? rightValue.hashCode() : 0);
        result = 31 * result + (fieldId != null ? fieldId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportFilterByReportId")
    public Collection<Report> getReportFilterByReportId() {
        return reportFilterByReportId;
    }

    public void setReportFilterByReportId(Collection<Report> reportFilterByReportId) {
        this.reportFilterByReportId = reportFilterByReportId;
    }
}
