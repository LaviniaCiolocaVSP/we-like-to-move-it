package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.BoardType;
import org.springframework.data.repository.CrudRepository;

public interface BoardTypeRepository extends CrudRepository<BoardType, Long> {
}
