package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessCredentialTypeDTO implements Serializable {
    private Long id;
    private String name;
    private String description;
    private Long numBits;
    private String format;
    private Short supportedBy4000;
    private Long credentialEncodingId;
    private Timestamp newCardEngineDate;

    @JsonCreator
    public AccessCredentialTypeDTO(@JsonProperty("id") final Long id,
                                   @JsonProperty("name") final String name,
                                   @JsonProperty("description") final String description,
                                   @JsonProperty("numBits") final Long numBits,
                                   @JsonProperty("format") final String format,
                                   @JsonProperty("supportedBy4000") final Short supportedBy4000,
                                   @JsonProperty("credentialEncodingId") final Long credentialEncodingId,
                                   @JsonProperty("newCardEngineDate") final Timestamp newCardEngineDate) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.numBits = numBits;
        this.format = format;
        this.supportedBy4000 = supportedBy4000;
        this.credentialEncodingId = credentialEncodingId;
        this.newCardEngineDate = newCardEngineDate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getNumBits() {
        return numBits;
    }

    public void setNumBits(Long numBits) {
        this.numBits = numBits;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Long getCredentialEncodingId() {
        return credentialEncodingId;
    }

    public void setCredentialEncodingId(Long credentialEncodingId) {
        this.credentialEncodingId = credentialEncodingId;
    }

    public Timestamp getNewCardEngineDate() {
        return newCardEngineDate;
    }

    public void setNewCardEngineDate(Timestamp newCardEngineDate) {
        this.newCardEngineDate = newCardEngineDate;
    }

    public Short getSupportedBy4000() {
        return supportedBy4000;
    }

    public void setSupportedBy4000(Short supportedBy4000) {
        this.supportedBy4000 = supportedBy4000;
    }
}
