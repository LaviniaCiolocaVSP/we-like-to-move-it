package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_item_type", schema = "brivo20", catalog = "onair")
public class BadgeItemType {
    private long badgeItemTypeId;
    private String name;
    private Collection<BadgeItem> badgeItemsByBadgeItemTypeId;

    @Id
    @Column(name = "badge_item_type_id", nullable = false)
    public long getBadgeItemTypeId() {
        return badgeItemTypeId;
    }

    public void setBadgeItemTypeId(long badgeItemTypeId) {
        this.badgeItemTypeId = badgeItemTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeItemType that = (BadgeItemType) o;

        if (badgeItemTypeId != that.badgeItemTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeItemTypeId ^ (badgeItemTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeItemTypeByBadgeItemTypeId")
    public Collection<BadgeItem> getBadgeItemsByBadgeItemTypeId() {
        return badgeItemsByBadgeItemTypeId;
    }

    public void setBadgeItemsByBadgeItemTypeId(Collection<BadgeItem> badgeItemsByBadgeItemTypeId) {
        this.badgeItemsByBadgeItemTypeId = badgeItemsByBadgeItemTypeId;
    }
}
