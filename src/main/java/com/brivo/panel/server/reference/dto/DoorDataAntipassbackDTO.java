package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class DoorDataAntipassbackDTO {
    private final Long doorDataAntipassbackId;
    private final Long zone;
    private final Long altReaderPanelOid;
    private final Long altReaderBoardNum;
    private final Long altReaderPointAddress;
    private final Long altZone;

    @JsonCreator
    public DoorDataAntipassbackDTO(@JsonProperty("doorDataAntipassbackId") final Long doorDataAntipassbackId,
                                   @JsonProperty("zone") final Long zone,
                                   @JsonProperty("altReaderPanelOid") final Long altReaderPanelOid,
                                   @JsonProperty("altReaderBoardNum") final Long altReaderBoardNum,
                                   @JsonProperty("altReaderPointAddress") final Long altReaderPointAddress,
                                   @JsonProperty("altZone") final Long altZone) {
        this.doorDataAntipassbackId = doorDataAntipassbackId;
        this.zone = zone;
        this.altReaderBoardNum = altReaderBoardNum;
        this.altReaderPanelOid = altReaderPanelOid;
        this.altReaderPointAddress = altReaderPointAddress;
        this.altZone = altZone;
    }

    public Long getDoorDataAntipassbackId() {
        return doorDataAntipassbackId;
    }

    public Long getZone() {
        return zone;
    }

    public Long getAltReaderPanelOid() {
        return altReaderPanelOid;
    }

    public Long getAltReaderBoardNum() {
        return altReaderBoardNum;
    }

    public Long getAltReaderPointAddress() {
        return altReaderPointAddress;
    }

    public Long getAltZone() {
        return altZone;
    }
}
