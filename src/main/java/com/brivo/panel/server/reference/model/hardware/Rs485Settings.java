package com.brivo.panel.server.reference.model.hardware;

import com.google.common.collect.Lists;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;

public class Rs485Settings implements Serializable {

    public static final int OSDP_OPERATION_MODE = 0;
    public static final int ALLEGION_OPERATION_MODE = 1;
    private Integer portNum = 0;
    private Integer operationMode = 0;
    private Integer baudRate = 9600;
    private OsdpSettings osdpSettings;

    public Rs485Settings() {

    }

    /**
     * Sets up a default Rs485Settings object on a port for either of the modes
     *
     * @param operationMode
     */
    public Rs485Settings(int portNum, int operationMode) {
        this.portNum = portNum;
        this.operationMode = 1;
        this.setBaudRate(9600);
        if (operationMode != 1) {
            this.operationMode = 0;
            this.osdpSettings = new OsdpSettings();
            this.osdpSettings.setPdAddress(Lists.newArrayList(0));
            this.osdpSettings.setErrorDetectionMethod(1);
        }
    }

    public Integer getPortNum() {
        return portNum;
    }

    public void setPortNum(Integer portNum) {
        this.portNum = portNum;
    }

    public Integer getOperationMode() {
        return operationMode;
    }

    public void setOperationMode(Integer operationMode) {
        this.operationMode = operationMode;
    }

    public Integer getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(Integer baudRate) {
        this.baudRate = baudRate;
    }

    public OsdpSettings getOsdpSettings() {
        return osdpSettings;
    }

    public void setOsdpSettings(OsdpSettings osdpSettings) {
        this.osdpSettings = osdpSettings;
    }

    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

}
