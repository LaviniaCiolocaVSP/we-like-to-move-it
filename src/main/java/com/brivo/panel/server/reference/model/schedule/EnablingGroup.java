package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class EnablingGroup {
    private Long enablingGroupId;
    private Integer gracePeriod;
    private Boolean autoDeactivate;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getEnablingGroupId() {
        return enablingGroupId;
    }

    public void setEnablingGroupId(Long enablingGroupId) {
        this.enablingGroupId = enablingGroupId;
    }

    public Integer getGracePeriod() {
        return gracePeriod;
    }

    public void setGracePeriod(Integer gracePeriod) {
        this.gracePeriod = gracePeriod;
    }

    public Boolean getAutoDeactivate() {
        return autoDeactivate;
    }

    public void setAutoDeactivate(Boolean autoDeactivate) {
        this.autoDeactivate = autoDeactivate;
    }

}
