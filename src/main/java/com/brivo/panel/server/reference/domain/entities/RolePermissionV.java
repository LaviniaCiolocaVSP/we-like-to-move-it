package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;

public class RolePermissionV {
    private Long accountId;
    private String username;
    private Long actorObjectId;
    private Long securityGroupId;
    private Long objectId;
    private String objectName;
    private Long securityGroupTypeId;
    private Integer read;
    private Integer write;
    private Integer append;
    private Integer activateDevices;
    private Integer run;

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "username", nullable = true, length = -1)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "security_group_id", nullable = true)
    public Long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(Long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "object_id", nullable = true)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "object_name", nullable = true, length = -1)
    public String getObjectName() {
        return objectName;
    }

    public void setObjectName(String objectName) {
        this.objectName = objectName;
    }

    @Basic
    @Column(name = "security_group_type_id", nullable = true)
    public Long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public void setSecurityGroupTypeId(Long securityGroupTypeId) {
        this.securityGroupTypeId = securityGroupTypeId;
    }

    @Basic
    @Column(name = "read", nullable = true)
    public Integer getRead() {
        return read;
    }

    public void setRead(Integer read) {
        this.read = read;
    }

    @Basic
    @Column(name = "write", nullable = true)
    public Integer getWrite() {
        return write;
    }

    public void setWrite(Integer write) {
        this.write = write;
    }

    @Basic
    @Column(name = "append", nullable = true)
    public Integer getAppend() {
        return append;
    }

    public void setAppend(Integer append) {
        this.append = append;
    }

    @Basic
    @Column(name = "activate_devices", nullable = true)
    public Integer getActivateDevices() {
        return activateDevices;
    }

    public void setActivateDevices(Integer activateDevices) {
        this.activateDevices = activateDevices;
    }

    @Basic
    @Column(name = "run", nullable = true)
    public Integer getRun() {
        return run;
    }

    public void setRun(Integer run) {
        this.run = run;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        RolePermissionV that = (RolePermissionV) o;

        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null) {
            return false;
        }
        if (actorObjectId != null ? !actorObjectId.equals(that.actorObjectId) : that.actorObjectId != null) {
            return false;
        }
        if (securityGroupId != null ? !securityGroupId.equals(that.securityGroupId) : that.securityGroupId != null) {
            return false;
        }
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) {
            return false;
        }
        if (objectName != null ? !objectName.equals(that.objectName) : that.objectName != null) {
            return false;
        }
        if (securityGroupTypeId != null ? !securityGroupTypeId.equals(that.securityGroupTypeId) : that.securityGroupTypeId != null) {
            return false;
        }
        if (read != null ? !read.equals(that.read) : that.read != null) {
            return false;
        }
        if (write != null ? !write.equals(that.write) : that.write != null) {
            return false;
        }
        if (append != null ? !append.equals(that.append) : that.append != null) {
            return false;
        }
        if (activateDevices != null ? !activateDevices.equals(that.activateDevices) : that.activateDevices != null) {
            return false;
        }
        return run != null ? run.equals(that.run) : that.run == null;
    }

    @Override
    public int hashCode() {
        int result = accountId != null ? accountId.hashCode() : 0;
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (actorObjectId != null ? actorObjectId.hashCode() : 0);
        result = 31 * result + (securityGroupId != null ? securityGroupId.hashCode() : 0);
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (objectName != null ? objectName.hashCode() : 0);
        result = 31 * result + (securityGroupTypeId != null ? securityGroupTypeId.hashCode() : 0);
        result = 31 * result + (read != null ? read.hashCode() : 0);
        result = 31 * result + (write != null ? write.hashCode() : 0);
        result = 31 * result + (append != null ? append.hashCode() : 0);
        result = 31 * result + (activateDevices != null ? activateDevices.hashCode() : 0);
        result = 31 * result + (run != null ? run.hashCode() : 0);
        return result;
    }
}
