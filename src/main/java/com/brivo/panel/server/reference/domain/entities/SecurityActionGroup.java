package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "security_action_group", schema = "brivo20", catalog = "onair")
public class SecurityActionGroup {
    private long securityActionGroupId;
    private String nameKey;
    private String description;
    private Timestamp created;
    private Timestamp updated;
    private Collection<SecurityActionGroupActions> securityActionGroupActionsBySecurityActionGroupId;

    @Id
    @Column(name = "security_action_group_id", nullable = false)
    public long getSecurityActionGroupId() {
        return securityActionGroupId;
    }

    public void setSecurityActionGroupId(long securityActionGroupId) {
        this.securityActionGroupId = securityActionGroupId;
    }

    @Basic
    @Column(name = "name_key", nullable = false, length = 55)
    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 255)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityActionGroup that = (SecurityActionGroup) o;

        if (securityActionGroupId != that.securityActionGroupId) {
            return false;
        }
        if (nameKey != null ? !nameKey.equals(that.nameKey) : that.nameKey != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityActionGroupId ^ (securityActionGroupId >>> 32));
        result = 31 * result + (nameKey != null ? nameKey.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "securityActionGroupBySecurityActionGroupId")
    public Collection<SecurityActionGroupActions> getSecurityActionGroupActionsBySecurityActionGroupId() {
        return securityActionGroupActionsBySecurityActionGroupId;
    }

    public void setSecurityActionGroupActionsBySecurityActionGroupId(Collection<SecurityActionGroupActions> securityActionGroupActionsBySecurityActionGroupId) {
        this.securityActionGroupActionsBySecurityActionGroupId = securityActionGroupActionsBySecurityActionGroupId;
    }
}
