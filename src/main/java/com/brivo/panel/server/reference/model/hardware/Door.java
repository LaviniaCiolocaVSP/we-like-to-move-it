package com.brivo.panel.server.reference.model.hardware;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class Door {
    private Long deviceId;
    private Integer readerBoardAddress;
    private Integer readerPointAddress;
    private Integer doorLatchRelayBoardAddress;
    private Integer doorLatchRelayPointAddress;
    private Integer shuntRelayBoardAddress;
    private Integer shuntRelayPointAddress;
    private Integer doorSwitchInputBoardAddress;
    private Integer doorSwitchInputPointAddress;
    private Integer rexSwitchInputBoardAddress;
    private Integer rexSwitchInputPointAddress;
    private Integer alternateReaderBoardAddress;
    private Integer alternateReaderPointAddress;
    private Long unlockScheduleId;
    private Integer maxPinAttempts;
    private Integer maxPinAttemptsLockDuration;
    private Integer passThroughDelay;
    private Boolean useAlarmShuntRelayBehavior;
    private Integer alarmShuntDelay;
    private Integer doorAjarDuration;
    private Integer invalidPinResetDuration;
    private Boolean reportDoorAjar;
    private Boolean reportDoorForced;
    private Boolean rexBehavior;
    private Integer maxRexTriggerExtension;
    private AntipassbackSettings antipassback;
    private TwoFactorCredentialSettings twoFactorCredential;
    private Long cardRequiredScheduleId;
    private List<EnterPermission> enterPermissions;
    private List<LockPermission> lockPermissions;

    private Boolean reportLiveStatus;
    private Integer debouncePeriod;
    private LockOnOpenSettings lockOnOpen;
    private KeypadCommand keypadCommand;
    private Boolean scheduleEnabled;

    private Integer enableLiveControl;

    @JsonIgnore
    private int nodeNumber;
    @JsonIgnore
    private int boardNumber;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getReaderBoardAddress() {
        return readerBoardAddress;
    }

    public void setReaderBoardAddress(Integer readerBoardAddress) {
        this.readerBoardAddress = readerBoardAddress;
    }

    public Integer getReaderPointAddress() {
        return readerPointAddress;
    }

    public void setReaderPointAddress(Integer readerPointAddress) {
        this.readerPointAddress = readerPointAddress;
    }

    public Integer getDoorLatchRelayBoardAddress() {
        return doorLatchRelayBoardAddress;
    }

    public void setDoorLatchRelayBoardAddress(Integer doorLatchRelayBoardAddress) {
        this.doorLatchRelayBoardAddress = doorLatchRelayBoardAddress;
    }

    public Integer getDoorLatchRelayPointAddress() {
        return doorLatchRelayPointAddress;
    }

    public void setDoorLatchRelayPointAddress(Integer doorLatchRelayPointAddress) {
        this.doorLatchRelayPointAddress = doorLatchRelayPointAddress;
    }

    public Integer getShuntRelayBoardAddress() {
        return shuntRelayBoardAddress;
    }

    public void setShuntRelayBoardAddress(Integer shuntRelayBoardAddress) {
        this.shuntRelayBoardAddress = shuntRelayBoardAddress;
    }

    public Integer getShuntRelayPointAddress() {
        return shuntRelayPointAddress;
    }

    public void setShuntRelayPointAddress(Integer shuntRelayPointAddress) {
        this.shuntRelayPointAddress = shuntRelayPointAddress;
    }

    public Integer getDoorSwitchInputBoardAddress() {
        return doorSwitchInputBoardAddress;
    }

    public void setDoorSwitchInputBoardAddress(Integer doorSwitchInputBoardAddress) {
        this.doorSwitchInputBoardAddress = doorSwitchInputBoardAddress;
    }

    public Integer getDoorSwitchInputPointAddress() {
        return doorSwitchInputPointAddress;
    }

    public void setDoorSwitchInputPointAddress(Integer doorSwitchInputPointAddress) {
        this.doorSwitchInputPointAddress = doorSwitchInputPointAddress;
    }

    public Integer getRexSwitchInputBoardAddress() {
        return rexSwitchInputBoardAddress;
    }

    public void setRexSwitchInputBoardAddress(Integer rexSwitchInputBoardAddress) {
        this.rexSwitchInputBoardAddress = rexSwitchInputBoardAddress;
    }

    public Integer getRexSwitchInputPointAddress() {
        return rexSwitchInputPointAddress;
    }

    public void setRexSwitchInputPointAddress(Integer rexSwitchInputPointAddress) {
        this.rexSwitchInputPointAddress = rexSwitchInputPointAddress;
    }

    public Long getUnlockScheduleId() {
        return unlockScheduleId;
    }

    public void setUnlockScheduleId(Long unlockScheduleId) {
        this.unlockScheduleId = unlockScheduleId;
    }

    public Integer getMaxPinAttempts() {
        return maxPinAttempts;
    }

    public void setMaxPinAttempts(Integer maxPinAttempts) {
        this.maxPinAttempts = maxPinAttempts;
    }

    public Integer getMaxPinAttemptsLockDuration() {
        return maxPinAttemptsLockDuration;
    }

    public void setMaxPinAttemptsLockDuration(Integer maxPinAttemptsLockDuration) {
        this.maxPinAttemptsLockDuration = maxPinAttemptsLockDuration;
    }

    public Integer getPassThroughDelay() {
        return passThroughDelay;
    }

    public void setPassThroughDelay(Integer passThroughDelay) {
        this.passThroughDelay = passThroughDelay;
    }

    public Boolean getUseAlarmShuntRelayBehavior() {
        return useAlarmShuntRelayBehavior;
    }

    public void setUseAlarmShuntRelayBehavior(Boolean useAlarmShuntRelayBehavior) {
        this.useAlarmShuntRelayBehavior = useAlarmShuntRelayBehavior;
    }

    public Integer getAlarmShuntDelay() {
        return alarmShuntDelay;
    }

    public void setAlarmShuntDelay(Integer alarmShuntDelay) {
        this.alarmShuntDelay = alarmShuntDelay;
    }

    public Integer getDoorAjarDuration() {
        return doorAjarDuration;
    }

    public void setDoorAjarDuration(Integer doorAjarDuration) {
        this.doorAjarDuration = doorAjarDuration;
    }

    public Integer getInvalidPinResetDuration() {
        return invalidPinResetDuration;
    }

    public void setInvalidPinResetDuration(Integer invalidPinResetDuration) {
        this.invalidPinResetDuration = invalidPinResetDuration;
    }

    public Boolean getReportDoorAjar() {
        return reportDoorAjar;
    }

    public void setReportDoorAjar(Boolean reportDoorAjar) {
        this.reportDoorAjar = reportDoorAjar;
    }

    public Boolean getReportDoorForced() {
        return reportDoorForced;
    }

    public void setReportDoorForced(Boolean reportDoorForced) {
        this.reportDoorForced = reportDoorForced;
    }

    public Boolean getRexBehavior() {
        return rexBehavior;
    }

    public void setRexBehavior(Boolean rexBehavior) {
        this.rexBehavior = rexBehavior;
    }

    public Integer getMaxRexTriggerExtension() {
        return maxRexTriggerExtension;
    }

    public void setMaxRexTriggerExtension(Integer maxRexTriggerExtension) {
        this.maxRexTriggerExtension = maxRexTriggerExtension;
    }

    public AntipassbackSettings getAntipassback() {
        return antipassback;
    }

    public void setAntipassback(AntipassbackSettings antipassback) {
        this.antipassback = antipassback;
    }

    public TwoFactorCredentialSettings getTwoFactorCredential() {
        return twoFactorCredential;
    }

    public void setTwoFactorCredential(
            TwoFactorCredentialSettings twoFactorCredential) {
        this.twoFactorCredential = twoFactorCredential;
    }

    public Long getCardRequiredScheduleId() {
        return cardRequiredScheduleId;
    }

    public void setCardRequiredScheduleId(Long cardRequiredScheduleId) {
        this.cardRequiredScheduleId = cardRequiredScheduleId;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public List<LockPermission> getLockPermissions() {
        return lockPermissions;
    }

    public void setLockPermissions(List<LockPermission> lockPermissions) {
        this.lockPermissions = lockPermissions;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

    public Integer getDebouncePeriod() {
        return debouncePeriod;
    }

    public void setDebouncePeriod(Integer debouncePeriod) {
        this.debouncePeriod = debouncePeriod;
    }

    public LockOnOpenSettings getLockOnOpen() {
        return lockOnOpen;
    }

    public void setLockOnOpen(LockOnOpenSettings lockOnOpen) {
        this.lockOnOpen = lockOnOpen;
    }

    public KeypadCommand getKeypadCommand() {
        return keypadCommand;
    }

    public void setKeypadCommand(KeypadCommand keypadCommand) {
        this.keypadCommand = keypadCommand;
    }

    public Integer getAlternateReaderBoardAddress() {
        return alternateReaderBoardAddress;
    }

    public void setAlternateReaderBoardAddress(Integer alternateReaderBoardAddress) {
        this.alternateReaderBoardAddress = alternateReaderBoardAddress;
    }

    public Integer getAlternateReaderPointAddress() {
        return alternateReaderPointAddress;
    }

    public void setAlternateReaderPointAddress(Integer alternateReaderPointAddress) {
        this.alternateReaderPointAddress = alternateReaderPointAddress;
    }

    public Integer getEnableLiveControl() {
        return enableLiveControl;
    }

    public void setEnableLiveControl(Integer enableLiveControl) {
        this.enableLiveControl = enableLiveControl;
    }

    @JsonIgnore
    public Boolean getScheduleEnabled() {
        return scheduleEnabled;
    }

    public void setScheduleEnabled(Boolean scheduleEnabled) {
        this.scheduleEnabled = scheduleEnabled;
    }

    public int getNodeNumber() {
        return nodeNumber;
    }

    public void setNodeNumber(int nodeNumber) {
        this.nodeNumber = nodeNumber;
    }

    public int getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(int boardNumber) {
        this.boardNumber = boardNumber;
    }

}
