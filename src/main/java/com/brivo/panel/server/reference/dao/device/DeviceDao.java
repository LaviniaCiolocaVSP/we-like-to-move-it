package com.brivo.panel.server.reference.dao.device;

import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@BrivoDao
public class DeviceDao {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceDao.class);
    
    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;
    
    public void deleteDeviceById(final Long deviceId, final Long deviceObjectId) {
        LOGGER.info("Deleting device with id {}", deviceId);
        
        String query = DeviceQuery.DELETE_DEVICE.getQuery();
        
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceId", deviceId);
        paramMap.put("deviceObjectId", deviceObjectId);
        
        jdbcTemplate.update(query, paramMap);
    }
    
    public void freeProgIoPointsForDevice(final Long deviceObjectId, final Collection<ProgDevicePuts> progDevicePutsCollection,
                                          final int pointStatus) {
        LOGGER.info("Marking io points free for device with object id {}", deviceObjectId);
        
        String query = DeviceQuery.UPDATE_PROG_IO_POINTS.getQuery();
        
        progDevicePutsCollection.forEach(progDevicePuts -> {
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("panelOid", progDevicePuts.getPanelOid());
            paramMap.put("boardNumber", progDevicePuts.getBoardNumber());
            paramMap.put("pointAddress", progDevicePuts.getPointAddress());
            paramMap.put("inuse", pointStatus);
            
            jdbcTemplate.update(query, paramMap);
        });
    }
    
    public void deleteDoorById(final Long deviceId, final Long deviceObjectId, Long boardNumber, Long brainObjectId,
                               final Collection<Short> pointAddresses) {
        LOGGER.info("Deleting door with id {}", deviceId);
        
        String query = DeviceQuery.DELETE_DOOR.getQuery();
        
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceId", deviceId);
        paramMap.put("deviceObjectId", deviceObjectId);
        paramMap.put("boardNumber", boardNumber);
        paramMap.put("brainObjectId", brainObjectId);
        paramMap.put("pointAddresses", pointAddresses);
        
        jdbcTemplate.update(query, paramMap);
    }
}
