package com.brivo.panel.server.reference.model.credential;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

import java.io.Serializable;
import java.util.stream.Stream;

public enum CredentialType implements Serializable {
    CARD(0),
    PIN(1),
    DIGITAL(2);

    private int credentialTypeKey;

    CredentialType(int credentialTypeKey) {
        this.credentialTypeKey = credentialTypeKey;
    }

    // @JsonCreator
    public static CredentialType getCredentialTypeByName(String typeName) {
        return Stream.of(CredentialType.values())
                     .filter(credentialType -> credentialType.name().equalsIgnoreCase(typeName))
                     .findFirst()
                     .orElse(null);
    }

    @JsonCreator
    public static CredentialType getCredentialTypeByKey(Integer key) {
        return Stream.of(CredentialType.values())
                .filter(credentialType -> credentialType.credentialTypeKey == key)
                .findFirst()
                .orElse(null);
    }

    @JsonValue
    public Integer getCredentialTypeKey() {
        return this.credentialTypeKey;
    }
}
