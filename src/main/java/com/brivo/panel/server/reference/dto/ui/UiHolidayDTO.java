package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;

public class UiHolidayDTO implements Serializable {
    private long holidayId;
    private String name;
    private long siteId;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
    private LocalDateTime from;

    //@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "MM/dd/yyyy HH:mm:ss")
    private LocalDateTime to;

    private long cadmHolidayId;
    private long accountId;
    private String site;


    private Set<UiScheduleDTO> scheduleDTOSet;

    public UiHolidayDTO() {

    }

    public UiHolidayDTO(long holidayId, String name, long siteId, LocalDateTime from,
                        LocalDateTime to, long cadmHolidayId, long accountId, String site) {
        this.name = name;
        this.siteId = siteId;
        this.from = from;
        this.to = to;
        this.holidayId = holidayId;
        this.cadmHolidayId = cadmHolidayId;
        this.accountId = accountId;
        this.site = site;
    }

    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getSiteId() {
        return siteId;
    }

    public void setSiteId(long siteId) {
        this.siteId = siteId;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }

    public long getCadmHolidayId() {
        return cadmHolidayId;
    }

    public void setCadmHolidayId(long cadmHolidayId) {
        this.cadmHolidayId = cadmHolidayId;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public String getSite() {
        return site;
    }

    public void setSite(String site) {
        this.site = site;
    }

    public Set<UiScheduleDTO> getScheduleDTOSet() {
        return scheduleDTOSet;
    }

    public void setScheduleDTOSet(Set<UiScheduleDTO> scheduleDTOSet) {
        this.scheduleDTOSet = scheduleDTOSet;
    }

    @Override
    public String toString() {
        return "UiHolidayDTO{" +
                "holidayId=" + holidayId +
                ", name='" + name + '\'' +
                ", siteId=" + siteId +
                ", from=" + from +
                ", to=" + to +
                ", cadmHolidayId=" + cadmHolidayId +
                ", accountId=" + accountId +
                ", site='" + site + '\'' +
                ", scheduleDTOSet=" + scheduleDTOSet +
                '}';
    }
}
