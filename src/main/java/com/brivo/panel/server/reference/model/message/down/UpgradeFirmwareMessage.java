package com.brivo.panel.server.reference.model.message.down;


import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class UpgradeFirmwareMessage extends DownstreamMessage {

    private final String firmware;

    public UpgradeFirmwareMessage(String firmware) {
        super(DownstreamMessageType.Upgrade_Firmware);
        this.firmware = firmware;
    }

    public String getFirmware() {
        return firmware;
    }
}
