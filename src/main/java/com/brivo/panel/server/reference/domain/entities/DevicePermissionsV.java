package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "device_permissions_v", schema = "brivo20", catalog = "onair")
public class DevicePermissionsV {
    private Long accountId;
    private Long brainId;
    private Long deviceId;
    private Long panelId;
    private Long deviceObjectId;
    private Long deviceTypeId;
    private String deviceName;
    private String deviceDescription;
    private Short deviceIsLockedDown;
    private Timestamp deviceCreated;
    private Timestamp deviceUpdated;
    private Long deviceOpsFlag;
    private Long accessCredentialId;
    private Long accessCredentialTypeId;
    private Long credentialOwnerObjectId;
    private String credential;
    private String credentialComments;
    private Timestamp credentialCreated;
    private Timestamp credentialUpdated;
    private Timestamp credentialEnableOn;
    private Timestamp credentialExpires;
    private Long credentialTotalUses;
    private Long credentialRemainingUses;
    private String credentialReferenceId;
    private Long securityActionId;
    private Long scheduleId;
    private Long permActorObjectId;
    private Long permObjectId;
    private Short ignoreLockdown;
    private Timestamp permissionCreated;
    private Timestamp permissionUpdated;

    @Id
    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "brain_id", nullable = true)
    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "device_id", nullable = true)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "panel_id", nullable = true)
    public Long getPanelId() {
        return panelId;
    }

    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }

    @Basic
    @Column(name = "device_object_id", nullable = true)
    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public void setDeviceObjectId(Long deviceObjectId) {
        this.deviceObjectId = deviceObjectId;
    }

    @Basic
    @Column(name = "device_type_id", nullable = true)
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    @Basic
    @Column(name = "device_name", nullable = true, length = 100)
    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Basic
    @Column(name = "device_description", nullable = true, length = 256)
    public String getDeviceDescription() {
        return deviceDescription;
    }

    public void setDeviceDescription(String deviceDescription) {
        this.deviceDescription = deviceDescription;
    }

    @Basic
    @Column(name = "device_is_locked_down", nullable = true)
    public Short getDeviceIsLockedDown() {
        return deviceIsLockedDown;
    }

    public void setDeviceIsLockedDown(Short deviceIsLockedDown) {
        this.deviceIsLockedDown = deviceIsLockedDown;
    }

    @Basic
    @Column(name = "device_created", nullable = true)
    public Timestamp getDeviceCreated() {
        return deviceCreated;
    }

    public void setDeviceCreated(Timestamp deviceCreated) {
        this.deviceCreated = deviceCreated;
    }

    @Basic
    @Column(name = "device_updated", nullable = true)
    public Timestamp getDeviceUpdated() {
        return deviceUpdated;
    }

    public void setDeviceUpdated(Timestamp deviceUpdated) {
        this.deviceUpdated = deviceUpdated;
    }

    @Basic
    @Column(name = "device_ops_flag", nullable = true)
    public Long getDeviceOpsFlag() {
        return deviceOpsFlag;
    }

    public void setDeviceOpsFlag(Long deviceOpsFlag) {
        this.deviceOpsFlag = deviceOpsFlag;
    }

    @Basic
    @Column(name = "access_credential_id", nullable = true)
    public Long getAccessCredentialId() {
        return accessCredentialId;
    }

    public void setAccessCredentialId(Long accessCredentialId) {
        this.accessCredentialId = accessCredentialId;
    }

    @Basic
    @Column(name = "access_credential_type_id", nullable = true)
    public Long getAccessCredentialTypeId() {
        return accessCredentialTypeId;
    }

    public void setAccessCredentialTypeId(Long accessCredentialTypeId) {
        this.accessCredentialTypeId = accessCredentialTypeId;
    }

    @Basic
    @Column(name = "credential_owner_object_id", nullable = true)
    public Long getCredentialOwnerObjectId() {
        return credentialOwnerObjectId;
    }

    public void setCredentialOwnerObjectId(Long credentialOwnerObjectId) {
        this.credentialOwnerObjectId = credentialOwnerObjectId;
    }

    @Basic
    @Column(name = "credential", nullable = true, length = 128)
    public String getCredential() {
        return credential;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    @Basic
    @Column(name = "credential_comments", nullable = true, length = 256)
    public String getCredentialComments() {
        return credentialComments;
    }

    public void setCredentialComments(String credentialComments) {
        this.credentialComments = credentialComments;
    }

    @Basic
    @Column(name = "credential_created", nullable = true)
    public Timestamp getCredentialCreated() {
        return credentialCreated;
    }

    public void setCredentialCreated(Timestamp credentialCreated) {
        this.credentialCreated = credentialCreated;
    }

    @Basic
    @Column(name = "credential_updated", nullable = true)
    public Timestamp getCredentialUpdated() {
        return credentialUpdated;
    }

    public void setCredentialUpdated(Timestamp credentialUpdated) {
        this.credentialUpdated = credentialUpdated;
    }

    @Basic
    @Column(name = "credential_enable_on", nullable = true)
    public Timestamp getCredentialEnableOn() {
        return credentialEnableOn;
    }

    public void setCredentialEnableOn(Timestamp credentialEnableOn) {
        this.credentialEnableOn = credentialEnableOn;
    }

    @Basic
    @Column(name = "credential_expires", nullable = true)
    public Timestamp getCredentialExpires() {
        return credentialExpires;
    }

    public void setCredentialExpires(Timestamp credentialExpires) {
        this.credentialExpires = credentialExpires;
    }

    @Basic
    @Column(name = "credential_total_uses", nullable = true)
    public Long getCredentialTotalUses() {
        return credentialTotalUses;
    }

    public void setCredentialTotalUses(Long credentialTotalUses) {
        this.credentialTotalUses = credentialTotalUses;
    }

    @Basic
    @Column(name = "credential_remaining_uses", nullable = true)
    public Long getCredentialRemainingUses() {
        return credentialRemainingUses;
    }

    public void setCredentialRemainingUses(Long credentialRemainingUses) {
        this.credentialRemainingUses = credentialRemainingUses;
    }

    @Basic
    @Column(name = "credential_reference_id", nullable = true, length = 128)
    public String getCredentialReferenceId() {
        return credentialReferenceId;
    }

    public void setCredentialReferenceId(String credentialReferenceId) {
        this.credentialReferenceId = credentialReferenceId;
    }

    @Basic
    @Column(name = "security_action_id", nullable = true)
    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "schedule_id", nullable = true)
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "perm_actor_object_id", nullable = true)
    public Long getPermActorObjectId() {
        return permActorObjectId;
    }

    public void setPermActorObjectId(Long permActorObjectId) {
        this.permActorObjectId = permActorObjectId;
    }

    @Basic
    @Column(name = "perm_object_id", nullable = true)
    public Long getPermObjectId() {
        return permObjectId;
    }

    public void setPermObjectId(Long permObjectId) {
        this.permObjectId = permObjectId;
    }

    @Basic
    @Column(name = "ignore_lockdown", nullable = true)
    public Short getIgnoreLockdown() {
        return ignoreLockdown;
    }

    public void setIgnoreLockdown(Short ignoreLockdown) {
        this.ignoreLockdown = ignoreLockdown;
    }

    @Basic
    @Column(name = "permission_created", nullable = true)
    public Timestamp getPermissionCreated() {
        return permissionCreated;
    }

    public void setPermissionCreated(Timestamp permissionCreated) {
        this.permissionCreated = permissionCreated;
    }

    @Basic
    @Column(name = "permission_updated", nullable = true)
    public Timestamp getPermissionUpdated() {
        return permissionUpdated;
    }

    public void setPermissionUpdated(Timestamp permissionUpdated) {
        this.permissionUpdated = permissionUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DevicePermissionsV that = (DevicePermissionsV) o;
        return Objects.equals(accountId, that.accountId) &&
                Objects.equals(brainId, that.brainId) &&
                Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(panelId, that.panelId) &&
                Objects.equals(deviceObjectId, that.deviceObjectId) &&
                Objects.equals(deviceTypeId, that.deviceTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(accountId, brainId, deviceId, panelId, deviceObjectId, deviceTypeId);
    }
}
