package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.dto.ui.UiSiteDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface DeviceRepository extends CrudRepository<Device, Long> {

    Optional<List<Device>> findByAccountId(@Param("accountId") long accountId);

    Optional<List<Device>> findByBrainId(@Param("brainId") long brainId);

    Optional<Device> findByObjectId(@Param("objectId") long objectId);

    @Query(
            value = "SELECT * " +
                    "FROM brivo20.device d " +
                    "JOIN brivo20.door_data dd ON d.object_id = dd.device_oid " +
                    "WHERE d.brain_id = :brainId " +
                    "AND dd.board_num = :boardNumber",
            nativeQuery = true
    )
    Optional<List<Device>> findDoorsByBoardAndPanel(@Param("brainId") long brainId, @Param("boardNumber") long boardNumber);

    @Query(
            value = "select count(*) from device d where d.device_type_id = 1 and d.deleted != 1 and d.account_id = :account_id",
            nativeQuery = true
    )
    Long getDoorCountForAccount(@Param("account_id") long accountId);

    @Query(
            value = "select count(*) from device d where d.device_type_id != 1 and d.deleted != 1 and d.account_id = :account_id",
            nativeQuery = true
    )
    Long getDeviceCountForAccount(@Param("account_id") long accountId);

    @Query(
            value = "SELECT coalesce(max(d.device_id),0) FROM brivo20.device d",
            nativeQuery = true
    )
    Optional<Long> getMaxDeviceId();

    @Query(
            value = "SELECT coalesce(max(d.panel_id),0) FROM brivo20.device d WHERE d.brain_id = :brainId AND d.device_type_id = :deviceTypeId",
            nativeQuery = true
    )
    Optional<Long> getMaxPanelId(@Param("brainId") long brainId, @Param("deviceTypeId") long deviceTypeId);

    @Query(
            value = "SELECT * " +
                    "FROM brivo20.device d " +
                    "JOIN brivo20.brain b ON d.brain_id = b.brain_id " +
                    "WHERE d.device_type_id in (1, 9, 11) " +
                    "AND d.deleted = 0 " +
                    "AND b.object_id = :panelOid",
            nativeQuery = true
    )
    Optional<List<Device>> findValidDoorsByPanelOid(@Param("panelOid") long panelOid);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiSiteDTO(sg.securityGroupId, sg.objectId, sg.name) " +
                    "FROM SecurityGroup sg " +
                    "JOIN SecurityGroupMember sgm ON sgm.securityGroupId = sg.securityGroupId " +
                    "WHERE sgm.objectId = :deviceOid " +
                    "AND sg.securityGroupTypeId = 1"
    )
    UiSiteDTO getUiSiteDTOForDevice(@Param("deviceOid") long deviceOid);

    @Query(
            value = "SELECT d.* " +
                    "FROM brivo20.device d " +
                    "JOIN brivo20.security_group_member sgm ON sgm.object_id = d.object_id " +
                    "JOIN brivo20.security_group sg on sg.security_group_id = sgm.security_group_id " +
                    "WHERE d.device_type_id in (1, 4, 7, 9, 11) " +
                    "AND d.deleted = 0 " +
                    "AND sg.object_id = :siteObjectId " +
                    "AND sgm.object_id not in (:deviceObjectIds)",
            nativeQuery = true
    )
    List<Device> findDoorsFromSiteExceptTheOnesWithPermissions(@Param("siteObjectId") long siteObjectId,
                                                               @Param("deviceObjectIds") Collection<Long> deviceObjectIds);

    @Query(
            value = "SELECT d.name " +
                    "FROM brivo20.device_type d " +
                    "WHERE d.device_type_id = :deviceTypeId ",
            nativeQuery = true
    )
    String findDeviceTypeDescriptionById(@Param("deviceTypeId") long deviceTypeId);
}
