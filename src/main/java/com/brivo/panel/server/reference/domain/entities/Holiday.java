package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
public class Holiday {
    private long holidayId;
    private long cadmHolidayId;
    private String name;
    private String description;
    private LocalDateTime startDate;
    private LocalDateTime stopDate;
    private LocalDateTime created;
    private LocalDateTime updated;
    private Long siteId;
    private Collection<DeviceHoliday> deviceHolidaysByHolidayId;
    private Account accountByAccountId;
    private long accountId;
    private SecurityGroup securityGroupBySiteId;

    private Collection<ScheduleHolidayMap> scheduleHolidayMapsByHolidayId;

    @Id
    @Column(name = "holiday_id", nullable = false)
    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    @Basic
    @Column(name = "cadm_holiday_id", nullable = false)
    public long getCadmHolidayId() {
        return cadmHolidayId;
    }

    public void setCadmHolidayId(long cadmHolidayId) {
        this.cadmHolidayId = cadmHolidayId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 4000)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "start_date", nullable = true)
    public LocalDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateTime startDate) {
        this.startDate = startDate;
    }

    @Basic
    @Column(name = "stop_date", nullable = true)
    public LocalDateTime getStopDate() {
        return stopDate;
    }

    public void setStopDate(LocalDateTime stopDate) {
        this.stopDate = stopDate;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "site_id", nullable = true)
    public Long getSiteId() {
        return siteId;
    }

    public void setSiteId(Long siteId) {
        this.siteId = siteId;
    }

    @Basic
    @Column(name = "account_id", nullable = true, insertable = false, updatable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Holiday holiday = (Holiday) o;

        if (holidayId != holiday.holidayId) {
            return false;
        }
        if (accountByAccountId.getAccountId() != holiday.accountByAccountId.getAccountId()) {
            return false;
        }
        if (cadmHolidayId != holiday.cadmHolidayId) {
            return false;
        }
        if (name != null ? !name.equals(holiday.name) : holiday.name != null) {
            return false;
        }
        if (description != null ? !description.equals(holiday.description) : holiday.description != null) {
            return false;
        }
        if (startDate != null ? !startDate.equals(holiday.startDate) : holiday.startDate != null) {
            return false;
        }
        if (stopDate != null ? !stopDate.equals(holiday.stopDate) : holiday.stopDate != null) {
            return false;
        }
        if (created != null ? !created.equals(holiday.created) : holiday.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(holiday.updated) : holiday.updated != null) {
            return false;
        }
        return siteId != null ? siteId.equals(holiday.siteId) : holiday.siteId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (holidayId ^ (holidayId >>> 32));
        result = 31 * result + (int) (accountByAccountId.getAccountId() ^ (accountByAccountId.getAccountId() >>> 32));
        result = 31 * result + (int) (cadmHolidayId ^ (cadmHolidayId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
        result = 31 * result + (stopDate != null ? stopDate.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (siteId != null ? siteId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "holidayByHolidayId")
    public Collection<DeviceHoliday> getDeviceHolidaysByHolidayId() {
        return deviceHolidaysByHolidayId;
    }

    public void setDeviceHolidaysByHolidayId(Collection<DeviceHoliday> deviceHolidaysByHolidayId) {
        this.deviceHolidaysByHolidayId = deviceHolidaysByHolidayId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "site_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public SecurityGroup getSecurityGroupBySiteId() {
        return securityGroupBySiteId;
    }

    public void setSecurityGroupBySiteId(SecurityGroup securityGroupBySiteId) {
        this.securityGroupBySiteId = securityGroupBySiteId;
    }

    @OneToMany(mappedBy = "holidayByHolidayOid", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ScheduleHolidayMap> getScheduleHolidayMapsByHolidayId() {
        return scheduleHolidayMapsByHolidayId;
    }

    public void setScheduleHolidayMapsByHolidayId(Collection<ScheduleHolidayMap> scheduleHolidayMapsByHolidayId) {
        this.scheduleHolidayMapsByHolidayId = scheduleHolidayMapsByHolidayId;
    }
}
