package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

public class AccountDTO extends AbstractDTO {

    private long id;
    private long accountTypeId;
    private long objectId;
    private String name;
    private LocalDateTime downloadTime;
    private Collection<SiteDTO> sites;
    private Collection<PanelDTO> panels;
    private Collection<DeviceDTO> devices;
    private Collection<DoorDTO> doors;
    private Collection<HolidayDTO> holidays;
    private Collection<ScheduleDTO> schedules;
    private Collection<GroupDTO> groups;
    private Collection<UserDTO> users;
    private Collection<CredentialDTO> credentials;
    private Collection<CredentialFieldDTO> credentialFields;
    private Collection<ObjectPropertyDTO> objectProperties;
    private Collection<AccessCredentialTypeDTO> accessCredentialTypeDTOS;

    public AccountDTO(final Long accountId, final String name) {
        this.id = accountId;
        this.name = name;
    }

    @JsonCreator
    public AccountDTO(@JsonProperty("id") final long id, @JsonProperty("accountTypeId") final long accountTypeId,
                      @JsonProperty("downloadTime") final LocalDateTime downloadTime,
                      @JsonProperty("objectId") final long objectId, @JsonProperty("name") final String name,
                      @JsonProperty("sites") final Collection<SiteDTO> sites,
                      @JsonProperty("panels") final Collection<PanelDTO> panels,
                      @JsonProperty("devices") final Collection<DeviceDTO> devices,
                      @JsonProperty("doors") final Collection<DoorDTO> doors,
                      @JsonProperty("users") final Collection<UserDTO> users,
                      @JsonProperty("groups") final Collection<GroupDTO> groups,
                      @JsonProperty("credentials") final Collection<CredentialDTO> credentials,
                      @JsonProperty("holidays") final Collection<HolidayDTO> holidays,
                      @JsonProperty("schedules") final Collection<ScheduleDTO> schedules,
                      @JsonProperty("credentialFields") final Collection<CredentialFieldDTO> credentialFields,
                      @JsonProperty("objectProperties") final Collection<ObjectPropertyDTO> objectProperties,
                      @JsonProperty("accessCredentialTypeDTOS") final Collection<AccessCredentialTypeDTO> accessCredentialTypeDTOS) {
        this.id = id;
        this.accountTypeId = accountTypeId;
        this.objectId = objectId;
        this.name = name;
        this.sites = sites;
        this.panels = panels;
        this.devices = devices;
        this.holidays = holidays;
        this.schedules = schedules;
        this.groups = groups;
        this.users = users;
        this.credentials = credentials;
        this.doors = doors;
        this.credentialFields = credentialFields;
        this.objectProperties = objectProperties;
        this.downloadTime = downloadTime;
        this.accessCredentialTypeDTOS = accessCredentialTypeDTOS;
    }

    @Override
    public Long getId() {
        return id;
    }

    public long getAccountTypeId() {
        return accountTypeId;
    }

    public long getObjectId() {
        return objectId;
    }

    public String getName() {
        return name;
    }

    public Collection<SiteDTO> getSites() {
        return sites;
    }

    public Collection<DeviceDTO> getDevices() {
        return devices;
    }

    public Collection<UserDTO> getUsers() {
        return users;
    }

    public Collection<GroupDTO> getGroups() {
        return groups;
    }

    public Collection<PanelDTO> getPanels() {
        return panels;
    }

    public Collection<HolidayDTO> getHolidays() {
        return holidays;
    }

    public Collection<ScheduleDTO> getSchedules() {
        return schedules;
    }

    public Collection<CredentialDTO> getCredentials() {
        return credentials;
    }

    public Collection<DoorDTO> getDoors() {
        return doors;
    }

    public Collection<CredentialFieldDTO> getCredentialFields() {
        return credentialFields;
    }

    public Collection<ObjectPropertyDTO> getObjectProperties() {
        return objectProperties;
    }

    public LocalDateTime getDownloadTime() {
        return downloadTime;
    }

    public Collection<AccessCredentialTypeDTO> getAccessCredentialTypeDTOS() {
        return accessCredentialTypeDTOS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountDTO that = (AccountDTO) o;
        return id == that.id &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
