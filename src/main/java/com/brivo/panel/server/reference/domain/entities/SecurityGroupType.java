package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "security_group_type", schema = "brivo20", catalog = "onair")
public class SecurityGroupType {
    private long securityGroupTypeId;
    private String name;
    private String description;
    private Collection<SecurityGroup> securityGroupsBySecurityGroupTypeId;

    @Id
    @Column(name = "security_group_type_id", nullable = false)
    public long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public void setSecurityGroupTypeId(long securityGroupTypeId) {
        this.securityGroupTypeId = securityGroupTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityGroupType that = (SecurityGroupType) o;

        if (securityGroupTypeId != that.securityGroupTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityGroupTypeId ^ (securityGroupTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "securityGroupTypeBySecurityGroupTypeId", cascade = CascadeType.ALL)
    public Collection<SecurityGroup> getSecurityGroupsBySecurityGroupTypeId() {
        return securityGroupsBySecurityGroupTypeId;
    }

    public void setSecurityGroupsBySecurityGroupTypeId(Collection<SecurityGroup> securityGroupsBySecurityGroupTypeId) {
        this.securityGroupsBySecurityGroupTypeId = securityGroupsBySecurityGroupTypeId;
    }
}
