package com.brivo.panel.server.reference.model;

import java.io.Serializable;
import java.util.Date;

public class FlushMessage implements Serializable {

    private static final long serialVersionUID = 696650244825978549L;

    private Long panelId;
    private boolean isDelayed;
    private Date messageCreationTime;

    // For serialization
    public FlushMessage() {

    }

    public FlushMessage(Long panelId, boolean isDelayed) {
        this(panelId, isDelayed, new Date());
    }

    public FlushMessage(Long panelId, boolean isDelayed, Date messageCreationTime) {
        this.panelId = panelId;
        this.isDelayed = isDelayed;
        this.messageCreationTime = messageCreationTime;
    }

    public Long getPanelId() {
        return this.panelId;
    }

    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }

    public boolean isDelayed() {
        return this.isDelayed;
    }

    public void setDelayed(boolean delayed) {
        this.isDelayed = delayed;
    }

    public Date getMessageCreationTime() {
        return this.messageCreationTime;
    }

    public void setMessageCreationTime(Date messageCreationTime) {
        this.messageCreationTime = messageCreationTime;
    }
}
