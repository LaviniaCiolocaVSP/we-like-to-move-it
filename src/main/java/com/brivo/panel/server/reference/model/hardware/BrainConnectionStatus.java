package com.brivo.panel.server.reference.model.hardware;

import java.util.Date;

/**
 *
 */
public class BrainConnectionStatus {
    private long brainId;
    private String messageQueue;
    private Date connectTime;
    private String ipAddress;

    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    public String getMessageQueue() {
        return messageQueue;
    }

    public void setMessageQueue(String messageQueue) {
        this.messageQueue = messageQueue;
    }

    public Date getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(Date connectTime) {
        this.connectTime = connectTime;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
