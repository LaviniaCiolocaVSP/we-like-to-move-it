package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DeviceSchedulePK implements Serializable {
    private long deviceId;
    private Long scheduleId;

    @Column(name = "device_id", nullable = false)
    @Id
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "schedule_id", nullable = false)
    @Id
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceSchedulePK that = (DeviceSchedulePK) o;

        if (deviceId != that.deviceId) {
            return false;
        }
        return scheduleId == that.scheduleId;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceId ^ (deviceId >>> 32));
        result = 31 * result + (int) (scheduleId ^ (scheduleId >>> 32));
        return result;
    }
}
