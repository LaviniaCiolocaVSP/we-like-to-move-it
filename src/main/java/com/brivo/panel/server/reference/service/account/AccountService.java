package com.brivo.panel.server.reference.service.account;

import com.brivo.panel.server.reference.dto.AccountDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountDashboardSummaryDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.Collection;

@Service
@SuppressWarnings("unused")
public class AccountService {

    public final static String BRIVO_REPOSITORY_IDENTIFIER = "BRIVO";
    private final static String RPS_REPOSITORY_IDENTIFIER = "RPS";

    private final CreateAccount createAccount;
    private final ReadAccount readAccount;
    private final DeleteAccount deleteAccount;

    @Autowired
    public AccountService(final CreateAccount createAccount, final ReadAccount readAccount,
                          final DeleteAccount deleteAccount) {
        this.createAccount = createAccount; this.readAccount = readAccount; this.deleteAccount = deleteAccount;
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UiAccountDashboardSummaryDTO getCurrentUiAccountDashboardSummary() {
        return readAccount.getCurrentAccountDashboardSummary();
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO create(AccountDTO accountDTO) {
        return createAccount.create(accountDTO);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO setCurrentAccount(final String accountName) {
        deleteAccount.deleteAccounts();

        return createAccount.create(accountName);
    }


    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO setBOLAccount(final String accountName) {
        deleteAccount.deleteBOLAccounts();

        return createAccount.create(accountName);
    }

    @Transactional(
            readOnly = true,
            transactionManager = "brivoAccountsTransactionManager",
            propagation = Propagation.REQUIRED
    )
    public Collection<UiAccountSummaryDTO> getAccountNamesAndIds(String nameFilter, int pageIndex, int pageSize) {
        return readAccount.getAccountNamesAndIds(nameFilter, pageIndex, pageSize);
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public UiAccountSummaryDTO getCurrentAccount() {
        return readAccount.getCurrentAccount();
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public ResponseEntity<StreamingResponseBody> downloadCurrentAccount(final long accountId) {
        return readAccount.download(accountId, RPS_REPOSITORY_IDENTIFIER);
    }

    @Transactional(
            readOnly = true,
            transactionManager = "brivoAccountsTransactionManager",
            propagation = Propagation.REQUIRED
    )
    public ResponseEntity<StreamingResponseBody> downloadBrivoAccount(final long accountId) {
        return readAccount.download(accountId, BRIVO_REPOSITORY_IDENTIFIER);
    }
}
