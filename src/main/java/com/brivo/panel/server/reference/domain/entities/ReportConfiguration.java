package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "report_configuration", schema = "brivo20", catalog = "onair")
public class ReportConfiguration {
    private long id;
    private long objectId;
    private long reportDefinitionId;
    private long accountId;
    private String name;
    private Short deleted;
    private Timestamp created;
    private Timestamp updated;
    private long ownerObjectId;
    private BrivoObject objectByBrivoObjectId;
    private ReportDefinition reportDefinitionByReportDefinitionId;
    private Account accountByAccountId;
    private Collection<ReportJob> reportJobsById;
    private Collection<ReportParameterValue> reportParameterValuesById;
    private Collection<ReportSchedule> reportSchedulesById;
    private Collection<ReportShortcut> reportShortcutsById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "report_definition_id", nullable = false)
    public long getReportDefinitionId() {
        return reportDefinitionId;
    }

    public void setReportDefinitionId(long reportDefinitionId) {
        this.reportDefinitionId = reportDefinitionId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = false)
    public long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportConfiguration that = (ReportConfiguration) o;

        if (id != that.id) {
            return false;
        }
        if (objectId != that.objectId) {
            return false;
        }
        if (reportDefinitionId != that.reportDefinitionId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (ownerObjectId != that.ownerObjectId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (reportDefinitionId ^ (reportDefinitionId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (ownerObjectId ^ (ownerObjectId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "report_definition_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportDefinition getReportDefinitionByReportDefinitionId() {
        return reportDefinitionByReportDefinitionId;
    }

    public void setReportDefinitionByReportDefinitionId(ReportDefinition reportDefinitionByReportDefinitionId) {
        this.reportDefinitionByReportDefinitionId = reportDefinitionByReportDefinitionId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "reportConfigurationByReportConfigurationId")
    public Collection<ReportJob> getReportJobsById() {
        return reportJobsById;
    }

    public void setReportJobsById(Collection<ReportJob> reportJobsById) {
        this.reportJobsById = reportJobsById;
    }

    @OneToMany(mappedBy = "reportConfigurationByReportConfigurationId")
    public Collection<ReportParameterValue> getReportParameterValuesById() {
        return reportParameterValuesById;
    }

    public void setReportParameterValuesById(Collection<ReportParameterValue> reportParameterValuesById) {
        this.reportParameterValuesById = reportParameterValuesById;
    }

    @OneToMany(mappedBy = "reportConfigurationByReportConfigurationId")
    public Collection<ReportSchedule> getReportSchedulesById() {
        return reportSchedulesById;
    }

    public void setReportSchedulesById(Collection<ReportSchedule> reportSchedulesById) {
        this.reportSchedulesById = reportSchedulesById;
    }

    @OneToMany(mappedBy = "reportConfigurationByReportConfigurationId")
    public Collection<ReportShortcut> getReportShortcutsById() {
        return reportShortcutsById;
    }

    public void setReportShortcutsById(Collection<ReportShortcut> reportShortcutsById) {
        this.reportShortcutsById = reportShortcutsById;
    }
}
