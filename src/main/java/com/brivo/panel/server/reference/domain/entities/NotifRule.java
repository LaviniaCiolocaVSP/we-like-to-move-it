package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "notif_rule", schema = "brivo20", catalog = "onair")
public class NotifRule {
    private long objectId;
    private long ownerObjectId;
    private long accountId;
    private String name;
    private short wantsDailyReport;
    private short wantsWeeklyReport;
    private Timestamp created;
    private Timestamp updated;
    private long creatorObjectId;
    private Short wantsPanelCommReport;
    private Long scheduleId;
    private Long languageId;
    private BrivoObject objectByBrivoObjectId;
    private BrivoObject objectByOwnerBrivoObjectId;
    private Account accountByAccountId;
    private Schedule scheduleByScheduleId;
    private Collection<NotifRuleCondition> notifRuleConditionsByObjectId;

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = false)
    public long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 60)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "wants_daily_report", nullable = false)
    public short getWantsDailyReport() {
        return wantsDailyReport;
    }

    public void setWantsDailyReport(short wantsDailyReport) {
        this.wantsDailyReport = wantsDailyReport;
    }

    @Basic
    @Column(name = "wants_weekly_report", nullable = false)
    public short getWantsWeeklyReport() {
        return wantsWeeklyReport;
    }

    public void setWantsWeeklyReport(short wantsWeeklyReport) {
        this.wantsWeeklyReport = wantsWeeklyReport;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "creator_object_id", nullable = false)
    public long getCreatorObjectId() {
        return creatorObjectId;
    }

    public void setCreatorObjectId(long creatorObjectId) {
        this.creatorObjectId = creatorObjectId;
    }

    @Basic
    @Column(name = "wants_panel_comm_report", nullable = true)
    public Short getWantsPanelCommReport() {
        return wantsPanelCommReport;
    }

    public void setWantsPanelCommReport(Short wantsPanelCommReport) {
        this.wantsPanelCommReport = wantsPanelCommReport;
    }

    @Basic
    @Column(name = "schedule_id", nullable = true)
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "language_id", nullable = true)
    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        NotifRule notifRule = (NotifRule) o;

        if (objectId != notifRule.objectId) {
            return false;
        }
        if (ownerObjectId != notifRule.ownerObjectId) {
            return false;
        }
        if (accountId != notifRule.accountId) {
            return false;
        }
        if (wantsDailyReport != notifRule.wantsDailyReport) {
            return false;
        }
        if (wantsWeeklyReport != notifRule.wantsWeeklyReport) {
            return false;
        }
        if (creatorObjectId != notifRule.creatorObjectId) {
            return false;
        }
        if (name != null ? !name.equals(notifRule.name) : notifRule.name != null) {
            return false;
        }
        if (created != null ? !created.equals(notifRule.created) : notifRule.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(notifRule.updated) : notifRule.updated != null) {
            return false;
        }
        if (wantsPanelCommReport != null ? !wantsPanelCommReport.equals(notifRule.wantsPanelCommReport) : notifRule.wantsPanelCommReport != null) {
            return false;
        }
        if (scheduleId != null ? !scheduleId.equals(notifRule.scheduleId) : notifRule.scheduleId != null) {
            return false;
        }
        return languageId != null ? languageId.equals(notifRule.languageId) : notifRule.languageId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (ownerObjectId ^ (ownerObjectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) wantsDailyReport;
        result = 31 * result + (int) wantsWeeklyReport;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (creatorObjectId ^ (creatorObjectId >>> 32));
        result = 31 * result + (wantsPanelCommReport != null ? wantsPanelCommReport.hashCode() : 0);
        result = 31 * result + (scheduleId != null ? scheduleId.hashCode() : 0);
        result = 31 * result + (languageId != null ? languageId.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "owner_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByOwnerBrivoObjectId() {
        return objectByOwnerBrivoObjectId;
    }

    public void setObjectByOwnerBrivoObjectId(BrivoObject objectByOwnerBrivoObjectId) {
        this.objectByOwnerBrivoObjectId = objectByOwnerBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", insertable = false, updatable = false)
    public Schedule getScheduleByScheduleId() {
        return scheduleByScheduleId;
    }

    public void setScheduleByScheduleId(Schedule scheduleByScheduleId) {
        this.scheduleByScheduleId = scheduleByScheduleId;
    }

    @OneToMany(mappedBy = "notifRuleByNotifRuleObjectId")
    public Collection<NotifRuleCondition> getNotifRuleConditionsByObjectId() {
        return notifRuleConditionsByObjectId;
    }

    public void setNotifRuleConditionsByObjectId(Collection<NotifRuleCondition> notifRuleConditionsByObjectId) {
        this.notifRuleConditionsByObjectId = notifRuleConditionsByObjectId;
    }
}
