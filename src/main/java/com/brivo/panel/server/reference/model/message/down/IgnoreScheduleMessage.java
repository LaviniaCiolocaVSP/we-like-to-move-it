package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class IgnoreScheduleMessage extends DeviceAdministratorMessage {

    public IgnoreScheduleMessage() {
        super(DownstreamMessageType.IgnoreScheduleMessage);
    }

    public String toString() {
        return "IgnoreScheduleMessage : {objectId:" + this.deviceId + ", adminId:" + administratorId + "}";
    }
}
