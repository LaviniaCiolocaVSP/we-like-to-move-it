package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "account_settings_value", schema = "brivo20", catalog = "onair")
public class AccountSettingsValue {
    private long accountSettingsId;
    private long accountId;
    private String value;
    private Timestamp updated;
    private Timestamp created;
    private AccountSettings accountSettingsByAccountSettingsId;
    private Account accountByAccountId;

    @Id
    @Basic
    @Column(name = "account_settings_id", nullable = false)
    public long getAccountSettingsId() {
        return accountSettingsId;
    }

    public void setAccountSettingsId(long accountSettingsId) {
        this.accountSettingsId = accountSettingsId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 250)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountSettingsValue that = (AccountSettingsValue) o;

        if (accountSettingsId != that.accountSettingsId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountSettingsId ^ (accountSettingsId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "account_settings_id", referencedColumnName = "account_settings_id", nullable = false,
            insertable = false, updatable = false)
    public AccountSettings getAccountSettingsByAccountSettingsId() {
        return accountSettingsByAccountSettingsId;
    }

    public void setAccountSettingsByAccountSettingsId(AccountSettings accountSettingsByAccountSettingsId) {
        this.accountSettingsByAccountSettingsId = accountSettingsByAccountSettingsId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false,
            updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
