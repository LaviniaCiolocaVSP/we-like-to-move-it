package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class EventResolutionData {
    private Long accountId;
    private Long objectId;
    private String fullName;
    private Long deviceOid;
    private Long objectTypeId;
    private String deviceName;
    private Long siteOid;
    private String securityGroupName;
    private Long deviceTypeId;
    private String engageMessage;
    private String disengageMessage;
    private Long ownerId;
    private Long lastOwnerId;
    private Instant enableOn;
    private Instant expires;
    private Boolean disabled;
    private String referenceId;

    public EventResolutionData() {

    }

    @JsonCreator
    public EventResolutionData(@JsonProperty("accountId") final Long accountId,
                               @JsonProperty("objectId") final Long objectId,
                               @JsonProperty("fullName") final String fullName,
                               @JsonProperty("deviceOid") final Long deviceOid,
                               @JsonProperty("objectTypeId") final Long objectTypeId,
                               @JsonProperty("deviceName") final String deviceName,
                               @JsonProperty("siteOid") final Long siteOid,
                               @JsonProperty("securityGroupName") final String securityGroupName,
                               @JsonProperty("deviceTypeId") final Long deviceTypeId,
                               @JsonProperty("engageMessage") final String engageMessage,
                               @JsonProperty("disengageMessage") final String disengageMessage,
                               @JsonProperty("ownerId") final Long ownerId,
                               @JsonProperty("lastOwnerId") final Long lastOwnerId,
                               @JsonProperty("eventType") final Instant enableOn,
                               @JsonProperty("eventType") final Instant expires,
                               @JsonProperty("eventType") final Boolean disabled,
                               @JsonProperty("eventType") final String referenceId) {
        this.accountId = accountId;
        this.objectId = objectId;
        this.fullName = fullName;
        this.deviceOid = deviceOid;
        this.objectTypeId = objectTypeId;
        this.deviceName = deviceName;
        this.siteOid = siteOid;
        this.securityGroupName = securityGroupName;
        this.deviceTypeId = deviceTypeId;
        this.engageMessage = engageMessage;
        this.disengageMessage = disengageMessage;
        this.ownerId = ownerId;
        this.lastOwnerId = lastOwnerId;
        this.enableOn = enableOn;
        this.expires = expires;
        this.disabled = disabled;
        this.referenceId = referenceId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(Long deviceOid) {
        this.deviceOid = deviceOid;
    }

    public Long getObjectTypeId() {
        return objectTypeId;
    }

    public void setObjectTypeId(Long objectTypeId) {
        this.objectTypeId = objectTypeId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Long getSiteOid() {
        return siteOid;
    }

    public void setSiteOid(Long siteOid) {
        this.siteOid = siteOid;
    }

    public String getSecurityGroupName() {
        return securityGroupName;
    }

    public void setSecurityGroupName(String securityGroupName) {
        this.securityGroupName = securityGroupName;
    }

    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    public String getEngageMessage() {
        return engageMessage;
    }

    public void setEngageMessage(String engageMessage) {
        this.engageMessage = engageMessage;
    }

    public String getDisengageMessage() {
        return disengageMessage;
    }

    public void setDisengageMessage(String disengageMessage) {
        this.disengageMessage = disengageMessage;
    }

    public Long getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(Long ownerId) {
        this.ownerId = ownerId;
    }

    public Long getLastOwnerId() {
        return lastOwnerId;
    }

    public void setLastOwnerId(Long lastOwnerId) {
        this.lastOwnerId = lastOwnerId;
    }

    public Instant getEnableOn() {
        return enableOn;
    }

    public void setEnableOn(Instant enableOn) {
        this.enableOn = enableOn;
    }

    public Instant getExpires() {
        return expires;
    }

    public void setExpires(Instant expires) {
        this.expires = expires;
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
}
