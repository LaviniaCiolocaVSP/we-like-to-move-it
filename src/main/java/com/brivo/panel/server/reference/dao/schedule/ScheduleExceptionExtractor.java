package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.model.schedule.ScheduleException;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatDay;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatType;
import com.brivo.panel.server.reference.model.schedule.ScheduleExceptionRepeatWeek;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * Schedule Exception Extractor.
 * <p>
 * ---------------------------------------------------------
 * Database Column - Usage for One Time Schedule Exceptions
 * ---------------------------------------------------------
 * exception_id    - Primary Key
 * schedule_id     - Parent Schedule Id foreign key.
 * repeat_ordinal  - Epoch time in seconds of 12:00 AM of the day of the exception.
 * repeat_index    - -1.
 * start_min       - Minutes from midnight of the start of the exception.
 * end_min         - Minutes from midnight of the end of the exception.
 * is_enable_block - Is 1 if an disabling exception. Is 0 if a enabling exception. REVERESED!
 * repeat_type     - 0.
 * ---------------------------------------------------------
 * Database Column - Usage for Repeating Schedule Exceptions
 * ---------------------------------------------------------
 * exception_id    - Primary Key
 * schedule_id     - Parent Schedule Id foreign key.
 * repeat_ordinal  - The week of the month where 0=1st, 1=2nd, 2=3rd, 3=4th, and 4=5th.
 * repeat_index    - The day of the week where 0=Sun, 1=Mon, 2=Tue, 3=Wed, 4=Thur, 5=Fri, and 6=Sat.
 * start_min       - Minutes from midnight of the start of the exception.
 * end_min         - Minutes from midnight of the end of the exception.
 * is_enable_block - Is 1 if an disabling exception. Is 0 if a enabling exception. REVERESED!
 * repeat_type     - 1.
 */
@Component
public class ScheduleExceptionExtractor
        implements ResultSetExtractor<List<ScheduleException>> {
    @Override
    public List<ScheduleException> extractData(ResultSet rs)
            throws SQLException, DataAccessException {
        List<ScheduleException> list = new ArrayList<>();

        while (rs.next()) {
            Integer isEnable = rs.getInt("isEnableBlock");
            Integer repeatType = rs.getInt("repeatType");
            Long repeatOrdinal = rs.getLong("repeatOrdinal");
            Integer repeatIndex = rs.getInt("repeatIndex");
            Integer startTime = rs.getInt("startTime");
            Integer endTime = rs.getInt("endTime");
            Long scheduleId = rs.getLong("scheduleId");

            ScheduleException exception = new ScheduleException();

            exception.setScheduleId(scheduleId);
            exception.setIsEnable(isEnable != 1); //Data in DB uses reversed values.
            exception.setRepeatType(
                    ScheduleExceptionRepeatType.getScheduleExceptionRepeatType(repeatType));

            if (ScheduleExceptionRepeatType.ONE_TIME_EXCEPTION.equals(exception.getRepeatType())) {
                exception.setStartDateSeconds(repeatOrdinal);
                exception.setStartMinute(startTime);
                exception.setEndMinute(endTime);
            } else if (ScheduleExceptionRepeatType.RECURRING_EXCEPTION.equals(exception.getRepeatType())) {
                exception.setRepeatWeek(
                        ScheduleExceptionRepeatWeek.getScheduleExceptionRepeatWeek(Math.toIntExact(repeatOrdinal)));
                exception.setRepeatDay(
                        ScheduleExceptionRepeatDay.getScheduleExceptionRepeatDay(repeatIndex));
                exception.setStartMinute(startTime);
                exception.setEndMinute(endTime);
            }

            list.add(exception);
        }

        return list;
    }
}
