package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "tes_resident", schema = "brivo20", catalog = "onair")
public class TesResident {
    private long residentId;
    private long objectId;
    private long directoryId;
    private long accountId;
    private String displayText;
    private String firstName;
    private String lastName;
    private String directoryCode;
    private String primaryPhone;
    private String alternatePhone;
    private Long dndScheduleId;
    private short isHidden;
    private Timestamp created;
    private Timestamp enabled;
    private Timestamp expires;
    private Timestamp updated;
    private short deleted;
    private TesDirectory tesDirectoryByDirectoryId;
    private Account accountByAccountId;
    private Schedule scheduleByDndScheduleId;

    @Id
    @Column(name = "resident_id", nullable = false)
    public long getResidentId() {
        return residentId;
    }

    public void setResidentId(long residentId) {
        this.residentId = residentId;
    }

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "directory_id", nullable = false)
    public long getDirectoryId() {
        return directoryId;
    }

    public void setDirectoryId(long directoryId) {
        this.directoryId = directoryId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "display_text", nullable = false, length = 40)
    public String getDisplayText() {
        return displayText;
    }

    public void setDisplayText(String displayText) {
        this.displayText = displayText;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 40)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 40)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "directory_code", nullable = false, length = 10)
    public String getDirectoryCode() {
        return directoryCode;
    }

    public void setDirectoryCode(String directoryCode) {
        this.directoryCode = directoryCode;
    }

    @Basic
    @Column(name = "primary_phone", nullable = false, length = 15)
    public String getPrimaryPhone() {
        return primaryPhone;
    }

    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }

    @Basic
    @Column(name = "alternate_phone", nullable = true, length = 15)
    public String getAlternatePhone() {
        return alternatePhone;
    }

    public void setAlternatePhone(String alternatePhone) {
        this.alternatePhone = alternatePhone;
    }

    @Basic
    @Column(name = "dnd_schedule_id", nullable = true)
    public Long getDndScheduleId() {
        return dndScheduleId;
    }

    public void setDndScheduleId(Long dndScheduleId) {
        this.dndScheduleId = dndScheduleId;
    }

    @Basic
    @Column(name = "is_hidden", nullable = false)
    public short getIsHidden() {
        return isHidden;
    }

    public void setIsHidden(short isHidden) {
        this.isHidden = isHidden;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "enabled", nullable = false)
    public Timestamp getEnabled() {
        return enabled;
    }

    public void setEnabled(Timestamp enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TesResident that = (TesResident) o;

        if (residentId != that.residentId) {
            return false;
        }
        if (objectId != that.objectId) {
            return false;
        }
        if (directoryId != that.directoryId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (isHidden != that.isHidden) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (displayText != null ? !displayText.equals(that.displayText) : that.displayText != null) {
            return false;
        }
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) {
            return false;
        }
        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) {
            return false;
        }
        if (directoryCode != null ? !directoryCode.equals(that.directoryCode) : that.directoryCode != null) {
            return false;
        }
        if (primaryPhone != null ? !primaryPhone.equals(that.primaryPhone) : that.primaryPhone != null) {
            return false;
        }
        if (alternatePhone != null ? !alternatePhone.equals(that.alternatePhone) : that.alternatePhone != null) {
            return false;
        }
        if (dndScheduleId != null ? !dndScheduleId.equals(that.dndScheduleId) : that.dndScheduleId != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (enabled != null ? !enabled.equals(that.enabled) : that.enabled != null) {
            return false;
        }
        if (expires != null ? !expires.equals(that.expires) : that.expires != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (residentId ^ (residentId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (directoryId ^ (directoryId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (displayText != null ? displayText.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (directoryCode != null ? directoryCode.hashCode() : 0);
        result = 31 * result + (primaryPhone != null ? primaryPhone.hashCode() : 0);
        result = 31 * result + (alternatePhone != null ? alternatePhone.hashCode() : 0);
        result = 31 * result + (dndScheduleId != null ? dndScheduleId.hashCode() : 0);
        result = 31 * result + (int) isHidden;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        result = 31 * result + (expires != null ? expires.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) deleted;
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "directory_id", referencedColumnName = "directory_id", nullable = false, insertable = false, updatable = false)
    public TesDirectory getTesDirectoryByDirectoryId() {
        return tesDirectoryByDirectoryId;
    }

    public void setTesDirectoryByDirectoryId(TesDirectory tesDirectoryByDirectoryId) {
        this.tesDirectoryByDirectoryId = tesDirectoryByDirectoryId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "dnd_schedule_id", referencedColumnName = "schedule_id", insertable = false, updatable = false)
    public Schedule getScheduleByDndScheduleId() {
        return scheduleByDndScheduleId;
    }

    public void setScheduleByDndScheduleId(Schedule scheduleByDndScheduleId) {
        this.scheduleByDndScheduleId = scheduleByDndScheduleId;
    }
}
