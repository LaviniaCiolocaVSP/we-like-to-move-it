package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiExtraFieldDTO implements Serializable {
    private String rootJsonTag;
    private String matchField;
    private String existingTags;
    private String tagToBeAddedOrUpdated;
    private String originalJsonText;
    private Long id;
    private String values;

    public UiExtraFieldDTO() {
    }

    public UiExtraFieldDTO(String rootJsonTag, String matchField, String existingTags, String tagToBeAddedOrUpdated,
                           String originalJsonText, Long id, String values) {
        this.rootJsonTag = rootJsonTag;
        this.matchField = matchField;
        this.existingTags = existingTags;
        this.tagToBeAddedOrUpdated = tagToBeAddedOrUpdated;
        this.originalJsonText = originalJsonText;
        this.id = id;
        this.values = values;
    }

    @Override
    public String toString() {
        return "UiExtraFieldDTO{" +
                "rootJsonTag='" + rootJsonTag + '\'' +
                ", matchField='" + matchField + '\'' +
                ", existingTags='" + existingTags + '\'' +
                ", tagToBeAddedOrUpdated='" + tagToBeAddedOrUpdated + '\'' +
                ", originalJsonText='" + originalJsonText + '\'' +
                ", id=" + id +
                ", values='" + values + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalJsonText() {
        return originalJsonText;
    }

    public void setOriginalJsonText(String originalJsonText) {
        this.originalJsonText = originalJsonText;
    }

    public String getRootJsonTag() {
        return rootJsonTag;
    }

    public void setRootJsonTag(String rootJsonTag) {
        this.rootJsonTag = rootJsonTag;
    }

    public String getMatchField() {
        return matchField;
    }

    public void setMatchField(String matchField) {
        this.matchField = matchField;
    }

    public String getExistingTags() {
        return existingTags;
    }

    public void setExistingTags(String existingTags) {
        this.existingTags = existingTags;
    }

    public String getTagToBeAddedOrUpdated() {
        return tagToBeAddedOrUpdated;
    }

    public void setTagToBeAddedOrUpdated(String tagToBeAddedOrUpdated) {
        this.tagToBeAddedOrUpdated = tagToBeAddedOrUpdated;
    }

    public String getValues() {
        return values;
    }

    public void setValues(String values) {
        this.values = values;
    }
}
