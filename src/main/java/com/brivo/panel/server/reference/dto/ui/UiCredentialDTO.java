
package com.brivo.panel.server.reference.dto.ui;


import java.io.Serializable;
import java.time.LocalDateTime;

public class UiCredentialDTO implements Serializable {
    private Long accountId;
    private Long credentialId;
    private String credentialType;
    private Long credentialTypeId;
    private String ownerFullName;
    private Long ownerObjectId;

    private LocalDateTime enableOn;

    private LocalDateTime expires;

    private String referenceId;
    private String credential;
    private Long numBits;

    private String facility_code;

    private String enableOnString;
    private String expiresString;

    public UiCredentialDTO() {
    }

    public UiCredentialDTO(final Long credentialId, final Long credentialTypeId, final String credentialType,
                           final String ownerFullName, final LocalDateTime enableOn, final LocalDateTime expires,
                           final String referenceId, final Long ownerObjectId, final String credential,
                           final Long numBits, final Long accountId) {
        this.credentialId = credentialId;
        this.credentialTypeId = credentialTypeId;
        this.credentialType = credentialType;
        this.ownerFullName = ownerFullName;
        this.enableOn = enableOn;
        this.expires = expires;
        this.referenceId = referenceId;
        this.ownerObjectId = ownerObjectId;
        this.credential = credential;
        this.numBits = numBits;
        this.accountId = accountId;
    }

    public UiCredentialDTO(final Long credentialId, final Long credentialTypeId, final String credentialType,
                           final String ownerFullName, final LocalDateTime enableOn, final LocalDateTime expires,
                           final String referenceId, final Long ownerObjectId, final String credential,
                           final Long numBits, final Long accountId, final String facility_code) {
        this.credentialId = credentialId;
        this.credentialTypeId = credentialTypeId;
        this.credentialType = credentialType;
        this.ownerFullName = ownerFullName;
        this.enableOn = enableOn;
        this.expires = expires;
        this.referenceId = referenceId;
        this.ownerObjectId = ownerObjectId;
        this.credential = credential;
        this.numBits = numBits;
        this.accountId = accountId;
        this.facility_code = facility_code;
    }

    public Long getAccountId() {
        return accountId;
    }

    public Long getCredentialId() {
        return credentialId;
    }

    public String getCredentialType() {
        return credentialType;
    }

    public String getOwnerFullName() {
        return ownerFullName;
    }

    public LocalDateTime getEnableOn() {
        return enableOn;
    }

    public LocalDateTime getExpires() {
        return expires;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public Long getCredentialTypeId() {
        return credentialTypeId;
    }

    public String getCredential() {
        return credential;
    }

    public Long getNumBits() {
        return numBits;
    }

    @Override
    public String toString() {
        return "UiCredentialDTO{" +
               "accountId=" + accountId +
               ", credentialId=" + credentialId +
               ", credentialType='" + credentialType + '\'' +
               ", credentialTypeId=" + credentialTypeId +
               ", ownerFullName='" + ownerFullName + '\'' +
               ", ownerObjectId=" + ownerObjectId +
               ", enableOn=" + enableOn +
               ", expires=" + expires +
               ", referenceId='" + referenceId + '\'' +
               ", credential='" + credential + '\'' +
               ", numBits=" + numBits +
               '}';
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public void setCredentialId(Long credentialId) {
        this.credentialId = credentialId;
    }

    public void setCredentialType(String credentialType) {
        this.credentialType = credentialType;
    }

    public void setCredentialTypeId(Long credentialTypeId) {
        this.credentialTypeId = credentialTypeId;
    }

    public void setOwnerFullName(String ownerFullName) {
        this.ownerFullName = ownerFullName;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    public void setEnableOn(LocalDateTime enableOn) {
        this.enableOn = enableOn;
    }

    public void setExpires(LocalDateTime expires) {
        this.expires = expires;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public void setCredential(String credential) {
        this.credential = credential;
    }

    public void setNumBits(Long numBits) {
        this.numBits = numBits;
    }

    public String getFacility_code() {
        return facility_code;
    }

    public void setFacility_code(String facility_code) {
        this.facility_code = facility_code;
    }

    public String getEnableOnString() {
        return enableOnString;
    }

    public void setEnableOnString(String enableOnString) {
        this.enableOnString = enableOnString;
    }

    public String getExpiresString() {
        return expiresString;
    }

    public void setExpiresString(String expiresString) {
        this.expiresString = expiresString;
    }
}

