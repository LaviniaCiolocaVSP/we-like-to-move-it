package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Collection;

public class UiSwitchDTO extends UiProgDeviceDTO implements Serializable {
    private Collection<UiPhysicalPointDTO> availableInputs;
    private UiPhysicalPointDTO input;

    public UiSwitchDTO() {
    }

    public UiSwitchDTO(final UiProgDeviceDTO uiProgDeviceDTO) {
        super(uiProgDeviceDTO, uiProgDeviceDTO.getDoorId(), uiProgDeviceDTO.getEventTypeId(), uiProgDeviceDTO.getAvailableOutputs(),
                uiProgDeviceDTO.getDisengageDelay(), uiProgDeviceDTO.getReportEngage(), uiProgDeviceDTO.getReportDisengage(),
                uiProgDeviceDTO.getEngageMessage(), uiProgDeviceDTO.getDisengageMessage(), uiProgDeviceDTO.getBehavior(),
                uiProgDeviceDTO.getOutputs());
    }

    public UiSwitchDTO(final Long disengageDelay, final Boolean reportEngage, final Boolean reportDisengage,
                       final String engageMessage, final String disengageMessage) {
        this.disengageDelay = disengageDelay;
        this.reportDisengage = reportDisengage;
        this.reportEngage = reportEngage;
        this.engageMessage = engageMessage;
        this.disengageMessage = disengageMessage;
    }

    public UiPhysicalPointDTO getInput() {
        return input;
    }

    public void setInput(UiPhysicalPointDTO input) {
        this.input = input;
    }

    public Collection<UiPhysicalPointDTO> getAvailableInputs() {
        return availableInputs;
    }

    public void setAvailableInputs(Collection<UiPhysicalPointDTO> availableInputs) {
        this.availableInputs = availableInputs;
    }

}
