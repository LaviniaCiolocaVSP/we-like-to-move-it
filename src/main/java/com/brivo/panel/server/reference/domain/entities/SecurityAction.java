package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "security_action", schema = "brivo20", catalog = "onair")
public class SecurityAction {
    private long securityActionId;
    private String name;
    private String description;
    private Timestamp created;
    private Timestamp updated;
    private long securityEventTypeId;
    private short exception;
    private String priority;
    private Collection<NotifRuleCondition> notifRuleConditionsBySecurityActionId;
    private Collection<ObjectPermission> objectPermissionsBySecurityActionId;
    private SecurityEventType securityEventTypeBySecurityEventTypeId;
    private Collection<SecurityActionGroupActions> securityActionGroupActionsBySecurityActionId;

    @Id
    @Column(name = "security_action_id", nullable = false, insertable = false, updatable = false)
    public long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "security_event_type_id", nullable = false)
    public long getSecurityEventTypeId() {
        return securityEventTypeId;
    }

    public void setSecurityEventTypeId(long securityEventTypeId) {
        this.securityEventTypeId = securityEventTypeId;
    }

    @Basic
    @Column(name = "exception", nullable = false)
    public short getException() {
        return exception;
    }

    public void setException(short exception) {
        this.exception = exception;
    }

    @Basic
    @Column(name = "priority", nullable = false, length = 32)
    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityAction that = (SecurityAction) o;

        if (securityActionId != that.securityActionId) {
            return false;
        }
        if (securityEventTypeId != that.securityEventTypeId) {
            return false;
        }
        if (exception != that.exception) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        return priority != null ? priority.equals(that.priority) : that.priority == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityActionId ^ (securityActionId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (securityEventTypeId ^ (securityEventTypeId >>> 32));
        result = 31 * result + (int) exception;
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "securityActionBySecurityActionId")
    public Collection<NotifRuleCondition> getNotifRuleConditionsBySecurityActionId() {
        return notifRuleConditionsBySecurityActionId;
    }

    public void setNotifRuleConditionsBySecurityActionId(Collection<NotifRuleCondition> notifRuleConditionsBySecurityActionId) {
        this.notifRuleConditionsBySecurityActionId = notifRuleConditionsBySecurityActionId;
    }

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ObjectPermission> getObjectPermissionsBySecurityActionId() {
        return objectPermissionsBySecurityActionId;
    }

    public void setObjectPermissionsBySecurityActionId(Collection<ObjectPermission> objectPermissionsBySecurityActionId) {
        this.objectPermissionsBySecurityActionId = objectPermissionsBySecurityActionId;
    }

    @ManyToOne
    @JoinColumn(name = "security_event_type_id", referencedColumnName = "security_event_type_id", nullable = false,
            insertable = false, updatable = false)
    public SecurityEventType getSecurityEventTypeBySecurityEventTypeId() {
        return securityEventTypeBySecurityEventTypeId;
    }

    public void setSecurityEventTypeBySecurityEventTypeId(SecurityEventType securityEventTypeBySecurityEventTypeId) {
        this.securityEventTypeBySecurityEventTypeId = securityEventTypeBySecurityEventTypeId;
    }

    @OneToMany(mappedBy = "securityActionBySecurityActionId")
    public Collection<SecurityActionGroupActions> getSecurityActionGroupActionsBySecurityActionId() {
        return securityActionGroupActionsBySecurityActionId;
    }

    public void setSecurityActionGroupActionsBySecurityActionId(
            Collection<SecurityActionGroupActions> securityActionGroupActionsBySecurityActionId) {
        this.securityActionGroupActionsBySecurityActionId = securityActionGroupActionsBySecurityActionId;
    }
}
