package com.brivo.panel.server.reference.service.util;

public class ConverterUtils {

    public static Boolean convertShortToBoolean(final Short value) {
        return value != null && value == 1;
    }

    public static Boolean convertIntegerToBoolean(final Integer value) {
        return value != null && value == 1;
    }

    public static  Short convertBooleanToShort(final Boolean value) {
        if (!value)
            return 0;
        else
            return 1;
    }
}
