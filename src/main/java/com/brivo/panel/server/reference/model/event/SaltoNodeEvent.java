package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class SaltoNodeEvent extends Event {
    private Long deviceId;
    private String nodeMacAddress;

    @JsonCreator
    public SaltoNodeEvent(@JsonProperty("eventType") final EventType eventType,
                          @JsonProperty("eventTime") final Instant eventTime,
                          @JsonProperty("deviceId") final Long deviceId,
                          @JsonProperty("nodeMacAddress") final String nodeMacAddress) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.nodeMacAddress = nodeMacAddress;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public String getNodeMacAddress() {
        return nodeMacAddress;
    }

    public void setNodeMacAddress(String nodeMacAddress) {
        this.nodeMacAddress = nodeMacAddress;
    }
}
