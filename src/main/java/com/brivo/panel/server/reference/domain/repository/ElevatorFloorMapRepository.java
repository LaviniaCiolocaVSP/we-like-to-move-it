package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.ElevatorFloorMap;
import org.springframework.data.repository.CrudRepository;

public interface ElevatorFloorMapRepository extends CrudRepository<ElevatorFloorMap, Long> {
}
