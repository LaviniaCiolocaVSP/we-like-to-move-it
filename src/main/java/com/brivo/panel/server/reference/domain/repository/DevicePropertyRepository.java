package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IDevicePropertyRepository;
import com.brivo.panel.server.reference.domain.entities.DeviceProperty;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface DevicePropertyRepository extends IDevicePropertyRepository {

    Optional<DeviceProperty> findByDeviceId(@Param("deviceId") Long deviceId, @Param("id") String id);

    @Query(
            value = "DELETE " +
                    "FROM brivo20.device_property dp " +
                    "WHERE dp.device_id = :deviceId " +
                    "AND dp.id = :id",
            nativeQuery = true
    )
    @Modifying
    void deleteById(@Param("deviceId") Long deviceId, @Param("id") String id);
}
