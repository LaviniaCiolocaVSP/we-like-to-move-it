package com.brivo.panel.server.reference.dao;

import org.springframework.stereotype.Repository;

/**
 * Custom Brivo DAO stereotype for a Data Access Object.
 *
 * @author glloyd
 */
@Repository
public @interface BrivoDao {

}
