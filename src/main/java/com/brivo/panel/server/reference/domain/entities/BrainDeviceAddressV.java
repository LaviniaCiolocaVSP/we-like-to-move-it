package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "brain_device_address_v", schema = "brivo20", catalog = "onair")
public class BrainDeviceAddressV {
    private Long deviceId;
    private Long deviceObjectId;
    private Long deviceTypeId;
    private Short isIngress;
    private Short isEgress;
    private Long accountId;
    private Long panelId;
    private String name;
    private String description;
    private Timestamp deviceCreated;
    private Timestamp deviceUpdated;
    private Short enableLiveControl;
    private Long brainId;
    private Long brainObjectId;
    private Long brainTypeId;
    private Long networkId;
    private Long messageSpecId;
    private String electronicSerialNumber;
    private String hardwareControllerVersion;
    private String firmwareVersion;
    private String physicalAddress;
    private String password;
    private Short isRegistered;
    private String timeZone;
    private Timestamp brainCreated;
    private Timestamp brainUpdated;
    private Long brainOpsFlag;
    private Long deviceOpsFlag;
    private Timestamp brainActivated;
    private Timestamp brainDeactivated;
    private Timestamp panelChanged;
    private Timestamp personsChanged;
    private Timestamp schedulesChanged;
    private Timestamp panelChecked;
    private Timestamp personsChecked;
    private Timestamp schedulesChecked;
    private Long addressId;
    private String addressType;
    private String firstName;
    private String lastName;
    private String address1;
    private String address2;
    private String city;
    private String state;
    private String zip;
    private String poBox;
    private String country;
    private String addressTimeZone;
    private String notes;
    private Timestamp addressCreated;
    private Timestamp addressUpdated;
    private String brainName;

    @Id
    @Basic
    @Column(name = "device_id", nullable = true)
    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    @Basic
    @Column(name = "device_object_id", nullable = true)
    public Long getDeviceObjectId() {
        return deviceObjectId;
    }

    public void setDeviceObjectId(Long deviceObjectId) {
        this.deviceObjectId = deviceObjectId;
    }

    @Basic
    @Column(name = "device_type_id", nullable = true)
    public Long getDeviceTypeId() {
        return deviceTypeId;
    }

    public void setDeviceTypeId(Long deviceTypeId) {
        this.deviceTypeId = deviceTypeId;
    }

    @Basic
    @Column(name = "is_ingress", nullable = true)
    public Short getIsIngress() {
        return isIngress;
    }

    public void setIsIngress(Short isIngress) {
        this.isIngress = isIngress;
    }

    @Basic
    @Column(name = "is_egress", nullable = true)
    public Short getIsEgress() {
        return isEgress;
    }

    public void setIsEgress(Short isEgress) {
        this.isEgress = isEgress;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "panel_id", nullable = true)
    public Long getPanelId() {
        return panelId;
    }

    public void setPanelId(Long panelId) {
        this.panelId = panelId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "device_created", nullable = true)
    public Timestamp getDeviceCreated() {
        return deviceCreated;
    }

    public void setDeviceCreated(Timestamp deviceCreated) {
        this.deviceCreated = deviceCreated;
    }

    @Basic
    @Column(name = "device_updated", nullable = true)
    public Timestamp getDeviceUpdated() {
        return deviceUpdated;
    }

    public void setDeviceUpdated(Timestamp deviceUpdated) {
        this.deviceUpdated = deviceUpdated;
    }

    @Basic
    @Column(name = "enable_live_control", nullable = true)
    public Short getEnableLiveControl() {
        return enableLiveControl;
    }

    public void setEnableLiveControl(Short enableLiveControl) {
        this.enableLiveControl = enableLiveControl;
    }

    @Basic
    @Column(name = "brain_id", nullable = true)
    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "brain_object_id", nullable = true)
    public Long getBrainObjectId() {
        return brainObjectId;
    }

    public void setBrainObjectId(Long brainObjectId) {
        this.brainObjectId = brainObjectId;
    }

    @Basic
    @Column(name = "brain_type_id", nullable = true)
    public Long getBrainTypeId() {
        return brainTypeId;
    }

    public void setBrainTypeId(Long brainTypeId) {
        this.brainTypeId = brainTypeId;
    }

    @Basic
    @Column(name = "network_id", nullable = true)
    public Long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }

    @Basic
    @Column(name = "message_spec_id", nullable = true)
    public Long getMessageSpecId() {
        return messageSpecId;
    }

    public void setMessageSpecId(Long messageSpecId) {
        this.messageSpecId = messageSpecId;
    }

    @Basic
    @Column(name = "electronic_serial_number", nullable = true, length = 32)
    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    @Basic
    @Column(name = "hardware_controller_version", nullable = true, length = 10)
    public String getHardwareControllerVersion() {
        return hardwareControllerVersion;
    }

    public void setHardwareControllerVersion(String hardwareControllerVersion) {
        this.hardwareControllerVersion = hardwareControllerVersion;
    }

    @Basic
    @Column(name = "firmware_version", nullable = true, length = 10)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Basic
    @Column(name = "physical_address", nullable = true, length = 128)
    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 30)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "is_registered", nullable = true)
    public Short getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Short isRegistered) {
        this.isRegistered = isRegistered;
    }

    @Basic
    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "brain_created", nullable = true)
    public Timestamp getBrainCreated() {
        return brainCreated;
    }

    public void setBrainCreated(Timestamp brainCreated) {
        this.brainCreated = brainCreated;
    }

    @Basic
    @Column(name = "brain_updated", nullable = true)
    public Timestamp getBrainUpdated() {
        return brainUpdated;
    }

    public void setBrainUpdated(Timestamp brainUpdated) {
        this.brainUpdated = brainUpdated;
    }

    @Basic
    @Column(name = "brain_ops_flag", nullable = true)
    public Long getBrainOpsFlag() {
        return brainOpsFlag;
    }

    public void setBrainOpsFlag(Long brainOpsFlag) {
        this.brainOpsFlag = brainOpsFlag;
    }

    @Basic
    @Column(name = "device_ops_flag", nullable = true)
    public Long getDeviceOpsFlag() {
        return deviceOpsFlag;
    }

    public void setDeviceOpsFlag(Long deviceOpsFlag) {
        this.deviceOpsFlag = deviceOpsFlag;
    }

    @Basic
    @Column(name = "brain_activated", nullable = true)
    public Timestamp getBrainActivated() {
        return brainActivated;
    }

    public void setBrainActivated(Timestamp brainActivated) {
        this.brainActivated = brainActivated;
    }

    @Basic
    @Column(name = "brain_deactivated", nullable = true)
    public Timestamp getBrainDeactivated() {
        return brainDeactivated;
    }

    public void setBrainDeactivated(Timestamp brainDeactivated) {
        this.brainDeactivated = brainDeactivated;
    }

    @Basic
    @Column(name = "panel_changed", nullable = true)
    public Timestamp getPanelChanged() {
        return panelChanged;
    }

    public void setPanelChanged(Timestamp panelChanged) {
        this.panelChanged = panelChanged;
    }

    @Basic
    @Column(name = "persons_changed", nullable = true)
    public Timestamp getPersonsChanged() {
        return personsChanged;
    }

    public void setPersonsChanged(Timestamp personsChanged) {
        this.personsChanged = personsChanged;
    }

    @Basic
    @Column(name = "schedules_changed", nullable = true)
    public Timestamp getSchedulesChanged() {
        return schedulesChanged;
    }

    public void setSchedulesChanged(Timestamp schedulesChanged) {
        this.schedulesChanged = schedulesChanged;
    }

    @Basic
    @Column(name = "panel_checked", nullable = true)
    public Timestamp getPanelChecked() {
        return panelChecked;
    }

    public void setPanelChecked(Timestamp panelChecked) {
        this.panelChecked = panelChecked;
    }

    @Basic
    @Column(name = "persons_checked", nullable = true)
    public Timestamp getPersonsChecked() {
        return personsChecked;
    }

    public void setPersonsChecked(Timestamp personsChecked) {
        this.personsChecked = personsChecked;
    }

    @Basic
    @Column(name = "schedules_checked", nullable = true)
    public Timestamp getSchedulesChecked() {
        return schedulesChecked;
    }

    public void setSchedulesChecked(Timestamp schedulesChecked) {
        this.schedulesChecked = schedulesChecked;
    }

    @Basic
    @Column(name = "address_id", nullable = true)
    public Long getAddressId() {
        return addressId;
    }

    public void setAddressId(Long addressId) {
        this.addressId = addressId;
    }

    @Basic
    @Column(name = "address_type", nullable = true, length = 20)
    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    @Basic
    @Column(name = "first_name", nullable = true, length = 30)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = true, length = 30)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "address1", nullable = true, length = 64)
    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Basic
    @Column(name = "address2", nullable = true, length = 64)
    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @Basic
    @Column(name = "city", nullable = true, length = 64)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "state", nullable = true, length = 32)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Basic
    @Column(name = "zip", nullable = true, length = 10)
    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    @Basic
    @Column(name = "po_box", nullable = true, length = 10)
    public String getPoBox() {
        return poBox;
    }

    public void setPoBox(String poBox) {
        this.poBox = poBox;
    }

    @Basic
    @Column(name = "country", nullable = true, length = 32)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "address_time_zone", nullable = true, length = 32)
    public String getAddressTimeZone() {
        return addressTimeZone;
    }

    public void setAddressTimeZone(String addressTimeZone) {
        this.addressTimeZone = addressTimeZone;
    }

    @Basic
    @Column(name = "notes", nullable = true, length = 4000)
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    @Basic
    @Column(name = "address_created", nullable = true)
    public Timestamp getAddressCreated() {
        return addressCreated;
    }

    public void setAddressCreated(Timestamp addressCreated) {
        this.addressCreated = addressCreated;
    }

    @Basic
    @Column(name = "address_updated", nullable = true)
    public Timestamp getAddressUpdated() {
        return addressUpdated;
    }

    public void setAddressUpdated(Timestamp addressUpdated) {
        this.addressUpdated = addressUpdated;
    }

    @Basic
    @Column(name = "brain_name", nullable = true, length = 32)
    public String getBrainName() {
        return brainName;
    }

    public void setBrainName(String brainName) {
        this.brainName = brainName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        BrainDeviceAddressV that = (BrainDeviceAddressV) o;
        return Objects.equals(deviceId, that.deviceId) &&
                Objects.equals(deviceObjectId, that.deviceObjectId) &&
                Objects.equals(deviceTypeId, that.deviceTypeId) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(panelId, that.panelId) &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, deviceObjectId, deviceTypeId, accountId, panelId, name);
    }
}
