package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "sso_application", schema = "brivo20", catalog = "onair")
public class SsoApplication {
    private long applicationId;
    private String name;
    private String loginUrl;
    private String loginProxy;
    private String providerUrl;
    private short remoteLogin;
    private Short remoteService;
    private short remoteXmlrpc;
    private long accountTypeId;
    private Collection<LoginGroup> loginGroupsByApplicationId;
    private AccountType accountTypeByAccountTypeId;

    @Id
    @Column(name = "application_id", nullable = false)
    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "login_url", nullable = true, length = 512)
    public String getLoginUrl() {
        return loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }

    @Basic
    @Column(name = "login_proxy", nullable = true, length = 512)
    public String getLoginProxy() {
        return loginProxy;
    }

    public void setLoginProxy(String loginProxy) {
        this.loginProxy = loginProxy;
    }

    @Basic
    @Column(name = "provider_url", nullable = true, length = 512)
    public String getProviderUrl() {
        return providerUrl;
    }

    public void setProviderUrl(String providerUrl) {
        this.providerUrl = providerUrl;
    }

    @Basic
    @Column(name = "remote_login", nullable = false)
    public short getRemoteLogin() {
        return remoteLogin;
    }

    public void setRemoteLogin(short remoteLogin) {
        this.remoteLogin = remoteLogin;
    }

    @Basic
    @Column(name = "remote_service", nullable = true)
    public Short getRemoteService() {
        return remoteService;
    }

    public void setRemoteService(Short remoteService) {
        this.remoteService = remoteService;
    }

    @Basic
    @Column(name = "remote_xmlrpc", nullable = false)
    public short getRemoteXmlrpc() {
        return remoteXmlrpc;
    }

    public void setRemoteXmlrpc(short remoteXmlrpc) {
        this.remoteXmlrpc = remoteXmlrpc;
    }

    @Basic
    @Column(name = "account_type_id", nullable = false)
    public long getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(long accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SsoApplication that = (SsoApplication) o;

        if (applicationId != that.applicationId) {
            return false;
        }
        if (remoteLogin != that.remoteLogin) {
            return false;
        }
        if (remoteXmlrpc != that.remoteXmlrpc) {
            return false;
        }
        if (accountTypeId != that.accountTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (loginUrl != null ? !loginUrl.equals(that.loginUrl) : that.loginUrl != null) {
            return false;
        }
        if (loginProxy != null ? !loginProxy.equals(that.loginProxy) : that.loginProxy != null) {
            return false;
        }
        if (providerUrl != null ? !providerUrl.equals(that.providerUrl) : that.providerUrl != null) {
            return false;
        }
        return remoteService != null ? remoteService.equals(that.remoteService) : that.remoteService == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (applicationId ^ (applicationId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (loginUrl != null ? loginUrl.hashCode() : 0);
        result = 31 * result + (loginProxy != null ? loginProxy.hashCode() : 0);
        result = 31 * result + (providerUrl != null ? providerUrl.hashCode() : 0);
        result = 31 * result + (int) remoteLogin;
        result = 31 * result + (remoteService != null ? remoteService.hashCode() : 0);
        result = 31 * result + (int) remoteXmlrpc;
        result = 31 * result + (int) (accountTypeId ^ (accountTypeId >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "ssoApplicationByApplicationId")
    public Collection<LoginGroup> getLoginGroupsByApplicationId() {
        return loginGroupsByApplicationId;
    }

    public void setLoginGroupsByApplicationId(Collection<LoginGroup> loginGroupsByApplicationId) {
        this.loginGroupsByApplicationId = loginGroupsByApplicationId;
    }

    @ManyToOne
    @JoinColumn(name = "account_type_id", referencedColumnName = "account_type_id", nullable = false, insertable = false,
            updatable = false)
    public AccountType getAccountTypeByAccountTypeId() {
        return accountTypeByAccountTypeId;
    }

    public void setAccountTypeByAccountTypeId(AccountType accountTypeByAccountTypeId) {
        this.accountTypeByAccountTypeId = accountTypeByAccountTypeId;
    }
}
