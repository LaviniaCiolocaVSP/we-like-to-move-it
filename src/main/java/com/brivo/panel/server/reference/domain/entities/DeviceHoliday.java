package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "device_holiday", schema = "brivo20", catalog = "onair")
@IdClass(DeviceHolidayPK.class)
public class DeviceHoliday {
    private long deviceId;
    private long holidayId;
    private long status;
    //    private Device deviceByDeviceId;
    private Holiday holidayByHolidayId;

    @Id
    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Id
    @Column(name = "holiday_id", nullable = false)
    public long getHolidayId() {
        return holidayId;
    }

    public void setHolidayId(long holidayId) {
        this.holidayId = holidayId;
    }

    @Basic
    @Column(name = "status", nullable = false)
    public long getStatus() {
        return status;
    }

    public void setStatus(long status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DeviceHoliday that = (DeviceHoliday) o;

        if (deviceId != that.deviceId) {
            return false;
        }
        if (holidayId != that.holidayId) {
            return false;
        }
        return status == that.status;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceId ^ (deviceId >>> 32));
        result = 31 * result + (int) (holidayId ^ (holidayId >>> 32));
        result = 31 * result + (int) (status ^ (status >>> 32));
        return result;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "device_id", referencedColumnName = "device_id", nullable = false, insertable = false,
            updatable = false)
    public Device getDeviceByDeviceId() {
        return deviceByDeviceId;
    }

    public void setDeviceByDeviceId(Device deviceByDeviceId) {
        this.deviceByDeviceId = deviceByDeviceId;
    }
    */

    @ManyToOne
    @JoinColumn(name = "holiday_id", referencedColumnName = "holiday_id", nullable = false,
            insertable = false, updatable = false)
    public Holiday getHolidayByHolidayId() {
        return holidayByHolidayId;
    }

    public void setHolidayByHolidayId(Holiday holidayByHolidayId) {
        this.holidayByHolidayId = holidayByHolidayId;
    }
}
