package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dao.panel.PanelDao;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.message.down.RequestPanelLogMessage;
import com.brivo.panel.server.reference.rest.NotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.socket.WebSocketSession;

@Service
public class PanelService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PanelService.class);

    private final PanelDao panelDao;
    private final PanelRepository panelRepository;
    private final SessionRegistryService sessionRegistryService;

    @Autowired
    public PanelService(final PanelDao panelDao,
                        final PanelRepository panelRepository,
                        final SessionRegistryService sessionRegistryService) {
        this.panelDao = panelDao;
        this.panelRepository = panelRepository;
        this.sessionRegistryService = sessionRegistryService;
    }

    /**
     * Look up the specified panel. Throw an exception if zero or
     * two are found.
     *
     * @param panelSerialNumber The panel to look up.
     * @return The Panel.
     */
    public Panel getPanel(String panelSerialNumber) {
        LOGGER.trace(">>> GetPanel");

        Panel panel = panelDao.getPanelByElectronicSerialNumber(panelSerialNumber);

        if (panel == null) {
            throw new NotFoundException("Error finding single instance of panel serial ["
                    + panelSerialNumber + "].");
        }

        LOGGER.trace("<<< GetPanel");

        return panel;
    }

    @Transactional
    public Panel findPanelForDeviceObjectId(Long deviceObjectId) {
        return panelDao.getPanelByDeviceObjectId(deviceObjectId);
    }

    @Transactional
    public Long findDeviceObjectIdForDeviceId(Long deviceId) {
        return panelDao.getDeviceObjectIdByDeviceId(deviceId);
    }

}
