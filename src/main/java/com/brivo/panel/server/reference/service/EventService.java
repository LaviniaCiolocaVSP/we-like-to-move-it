package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.Events;
import com.brivo.panel.server.reference.domain.repository.EventRepository;
import com.brivo.panel.server.reference.model.event.Event;
import com.brivo.panel.server.reference.model.event.PanelEventData;
import com.brivo.panel.server.reference.model.event.UnknownEvent;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

/**
 * Event Service.
 */
@Service
public class EventService {
    private static final Logger LOGGER = LoggerFactory.getLogger(EventService.class);

    /*
    @Autowired
    ConvertPojoService pojoService;

    @Autowired
    SiteDao siteDao;

    @Autowired
    PanelService panelService;

    @Autowired
    private LegacyEventHandler eventHandler;

    @Autowired
    private ApplicationContext context;

    @Autowired
    private EventNotHandledDataProcessor eventNotHandledDataProcessor;

    @Autowired
    private Set<Long> eventIdsToLimit;

    @Autowired
    private EventMonitoringService eventMonitoringService;

    @Value("${event.limit.seconds}")
    private Long eventLimitSeconds;

    private ConcurrentHashMap<String, EventCounter> securityActionIdsByBrainIdsWithEventCounterCache = new ConcurrentHashMap<>();
    */

    /**
     * Handle all event types from the panel.
     *
     * @param panel           The Panel.
     * @param events          The events.
     * @param eventRepository
     */
    @Transactional
    public void handleEvents(Panel panel, PanelEventData events, final EventRepository eventRepository) {
        LOGGER.info(">>> HandleEvents");
        final ObjectMapper objectMapper = new ObjectMapper();

        if (events.getActorDeviceEvents() != null) {
            LOGGER.debug("ActorDeviceEvents");

            events.getActorDeviceEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getActorDeviceEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleActorDeviceEvent(panel, event));
                  */
        }

        if (events.getAlarmEvents() != null) {
            LOGGER.debug("AlarmEvents");

            events.getAlarmEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getAlarmEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleAlarmEvent(panel, event));
                  */
        }


        if (events.getBoardEvents() != null) {
            LOGGER.debug("BoardEvents");

            events.getBoardEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getBoardEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .filter(event -> !checkEventForLimiting(panel, event))
                  .forEach(event -> handleBoardEvent(panel, event));
                  */
        }

        if (events.getDeviceEvents() != null) {
            LOGGER.debug("DeviceEvents");

            events.getDeviceEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getDeviceEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .filter(event -> !checkEventForLimiting(panel, event))
                  .forEach(event -> handleDeviceEvent(panel, event));
                  */
        }

        if (events.getDeviceStatusEvents() != null) {
            LOGGER.debug("DeviceStatusEvents");

            events.getDeviceStatusEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getDeviceStatusEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleDeviceStatusEvent(panel, event));
                  */
        }

        if (events.getCredentialEvents() != null) {
            LOGGER.debug("CredentialEvents");

            events.getCredentialEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getCredentialEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleCredentialEvent(panel, event));
                  */
        }

        if (events.getPanelReportEvents() != null) {
            LOGGER.debug("PanelReportEvents");

            events.getPanelReportEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getPanelReportEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handlePanelReportEvent(panel, event));
                  */
        }

        if (events.getSaltoNodeEvents() != null) {
            LOGGER.debug("SaltoNodeEvents");

            events.getSaltoNodeEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getSaltoNodeEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleSaltoNodeEvent(panel, event));
                  */
        }

        if (events.getSaltoRouterHelloEvents() != null) {
            LOGGER.debug("SaltoRouterHelloEvents");

            events.getSaltoRouterHelloEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getSaltoRouterHelloEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleSaltoRouterHelloEvent(panel, event));
                  */
        }

        if (events.getActorScheduleEvents() != null) {
            LOGGER.debug("ActorScheduleEvents");

            events.getActorScheduleEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getActorScheduleEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleActorScheduleEvent(panel, event));
                  */
        }

        if (events.getStartUpEvents() != null) {
            LOGGER.debug("StartUpEvents");

            events.getStartUpEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getStartUpEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .filter(event -> !checkEventForLimiting(panel, event))
                  .forEach(event -> handleStartupEvent(panel, event));
                  */
        }

        if (events.getWireEvents() != null) {
            LOGGER.debug("WireEvents");

            events.getWireEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getWireEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .filter(event -> !checkEventForLimiting(panel, event))
                  .forEach(event -> handleWireEvent(panel, event));
                  */
        }

        if (events.getEntranceEvents() != null) {
            LOGGER.debug("EntranceEvents");

            events.getEntranceEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getEntranceEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .forEach(event -> handleEntranceEvent(panel, event));
                  */
        }

        if (events.getUnauthorizedIpAccessEvents() != null) {
            LOGGER.debug("UnauthorizedIpAccessEvents");

            events.getUnauthorizedIpAccessEvents().forEach(event ->
                    setAndSaveEventInTheDatabase(panel, eventRepository, objectMapper, event)
            );
            /*
            events.getUnauthorizedIpAccessEvents().stream()
                  .filter(event -> checkForHandledEventType(panel, event))
                  .filter(event -> !checkEventForLimiting(panel, event))
                  .forEach(event -> handleUnauthorizedIpAccessEvent(panel, event));
                  */
        }

    }

    private void setAndSaveEventInTheDatabase(final Panel panel,
                                              final EventRepository eventRepository,
                                              final ObjectMapper objectMapper,
                                              final Event event) {
        try {
            final Events storableEvent = new Events();
            storableEvent.setEventType(event.getEventType().toString());

            LocalDateTime updatedDate = LocalDateTime.ofInstant(event.getEventTime(), panel.getTimezone());
            storableEvent.setEventTime(updatedDate);
            storableEvent.setPanelObjectId(panel.getObjectId());

            final String originalEventData = objectMapper.writeValueAsString(event);
            String updatedEventData = originalEventData;
            // event_data column length is 1000
            if (originalEventData.length() >= 1000) {
                updatedEventData = originalEventData.substring(0, 999);
            }

            storableEvent.setEventData(updatedEventData);

            eventRepository.save(storableEvent);

            LOGGER.debug("The event was saved in the database");
        } catch (JsonProcessingException e) {
            LOGGER.error(e.getStackTrace().toString());
        }
    }

    public void setAndSaveUnknownEventInTheDatabase(final Panel panel,
                                                    final EventRepository eventRepository,
                                                    final UnknownEvent event) {
        final Events storableEvent = new Events();
        storableEvent.setEventType(event.getEventType());

        LocalDateTime updatedDate = LocalDateTime.ofInstant(event.getEventTime(), panel.getTimezone());
        storableEvent.setEventTime(updatedDate);
        storableEvent.setPanelObjectId(panel.getObjectId());

        JSONObject eventAsJSON = new JSONObject();
        try {
            eventAsJSON = new JSONObject(event.getMessage().getPayload());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        storableEvent.setEventData(eventAsJSON.toString());

        eventRepository.save(storableEvent);

        LOGGER.debug("The unknown event was saved in the database");
    }

    /*
    private boolean checkForHandledEventType(Panel panel, Event event) {
        if (event.getEventType() == EventType.NONE) {
            // eventMonitoringService.meterUnhandledEvent();
            LOGGER.warn("Unhandled Event Type encountered for panel " + panel.getElectronicSerialNumber() +
                    "\n with event data " + event);
            return false;
        }

        return true;
    }
    */

    /* *//**
     * Actor Device Event.
     * <p>
     * Alarm Event.
     * <p>
     * Board Event.
     * <p>
     * Device Event.
     * <p>
     * Device Status Event. We do not handle this event.
     * <p>
     * Credential Event.
     * <p>
     * Panel Report Event.
     * <p>
     * Salto Node Event.
     * <p>
     * Salto Router Hello Event.
     * <p>
     * Actor Schedule Event.
     * <p>
     * Startup Event.
     * <p>
     * Wire Event.
     * <p>
     * Construct a
     * string describing
     * the event.
     * <p>
     * Handle Event.
     *
     * @param eventType        EventType
     * of the
     * event .
     * @param objectId         Object
     * Id used
     * to derive
     * site information.
     * @param securityLogEvent Event
     * to process.
     * <p>
     * Return the
     * site IDs for
     * the supplied
     * board/device/
     * panel object
     * <p>
     * Id.We figure
     * out which
     * based on
     * the EventType.
     * These queries
     * <p>
     * are expected
     * to return
     * one site
     * Id most
     * of the
     * time.We do
     * this
     * on the
     * off chance
     * that the
     * panel is
     * associated with
     * more
     * <p>
     * than one
     * site .
     * @param eventType EventType
     * @param objectId  Object
     * Id .
     * @return List of
     * site IDs.
     * <p>
     * Resolve the
     * event.Query for
     * any additional
     * data that
     * <p>
     * will be
     * needed downstream.
     * @param eventType EventType.
     * @param logEvent  The
     * event .
     * @return True if
     * the resolution
     * succeeded .
     * <p>
     * Send event
     * to the
     * event queue.
     * @param securityLogEvent The
     * Event .
     *//*
    private void handleActorDeviceEvent(Panel panel, ActorDeviceEvent event) {
        LOGGER.debug(">>> HandleActorDeviceEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertActorDeviceEventToLogEvent(panel, event);

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleActorDeviceEvent");
        // eventMonitoringService.meterHandledEvent("actorDeviceEventsHandled");
    }

    *//**
     * Alarm Event.
     *//*
    private void handleAlarmEvent(Panel panel, AlarmEvent event) {
        LOGGER.debug(">>> HandleAlarmEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertAlarmEventToLogEvent(panel, event);

        // handleEvent(eventType, panel.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleAlarmEvent");
        // eventMonitoringService.meterHandledEvent("alarmEventsHandled");
    }

    *//**
     * Board Event.
     *//*
    private void handleBoardEvent(Panel panel, BoardEvent event) {
        LOGGER.debug(">>> HandleBoardEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertBoardEventToLogEvent(panel, event);

        // handleEvent(eventType, panel.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleBoardEvent");
        // eventMonitoringService.meterHandledEvent("boardEventHandled");
    }

    *//**
     * Device Event.
     *//*
    private void handleDeviceEvent(Panel panel, DeviceEvent event) {
        LOGGER.debug(">>> HandleDeviceEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertDeviceEventToLogEvent(panel, event);

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleDeviceEvent");
        // eventMonitoringService.meterHandledEvent("deviceEventHandled");
    }

    *//**
     * Device Status Event. We do not handle this event.
     *//*
    private void handleDeviceStatusEvent(Panel panel, DeviceStatusEvent event) {
        LOGGER.debug(">>> HandleDeviceStatusEvent");
        LOGGER.debug("<<< HandleDeviceStatusEvent");
    }

    *//**
     * Credential Event.
     *//*
    private void handleCredentialEvent(Panel panel, CredentialEvent event) {
        LOGGER.debug(">>> HandleCredentialEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertCredentialEventToLogEvent(panel, event);

        if (LOGGER.isDebugEnabled()) {
            // LOGGER.debug("CredentialEvent converted to // SecurityLogEvent " + securityLogEvent);
        }

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleCredentialEvent");
        // eventMonitoringService.meterHandledEvent("credentialEventHandled");
    }

    *//**
     * Panel Report Event.
     *//*
    private void handlePanelReportEvent(Panel panel, PanelReportEvent event) {
        LOGGER.debug(">>> HandleCredentialEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertPanelReportEventToLogEvent(panel, event);

        // handleEvent(eventType, panel.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleCredentialEvent");
        // eventMonitoringService.meterHandledEvent("panelReportEventHandled");
    }

    *//**
     * Salto Node Event.
     *//*
    private void handleSaltoNodeEvent(Panel panel, SaltoNodeEvent event) {
        LOGGER.debug(">>> HandleSaltoNodeEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertSaltoNodeEventToLogEvent(panel, event);

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleSaltoNodeEvent");
        // eventMonitoringService.meterHandledEvent("saltoNodeEventHandled");
    }

    *//**
     * Salto Router Hello Event.
     *//*
    private void handleSaltoRouterHelloEvent(Panel panel, SaltoRouterHelloEvent event) {
        LOGGER.debug(">>> HandleSaltoRouterHelloEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertSaltoRouterHelloToLogEvent(panel, event);

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleSaltoRouterHelloEvent");
        // eventMonitoringService.meterHandledEvent("saltoRouterHelloEventHandled");
    }

    *//**
     * Actor Schedule Event.
     *//*
    private void handleActorScheduleEvent(Panel panel, ActorScheduleEvent event) {
        LOGGER.debug(">>> HandleActorScheduleEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertActorScheduleEventToLogEvent(panel, event);

        // handleEvent(eventType, event.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleActorScheduleEvent");
        // eventMonitoringService.meterHandledEvent("actorScheduleEventHandled");
    }

    *//**
     * Startup Event.
     *//*

    private void handleStartupEvent(Panel panel, StartupEvent event) {
        LOGGER.debug(">>> HandleStartupEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertStartupEventToLogEvent(panel, event);

        // handleEvent(eventType, panel.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleStartupEvent");
        // eventMonitoringService.meterHandledEvent("startupEventHandled");
    }

    *//**
     * Wire Event.
     *//*
    private void handleWireEvent(Panel panel, WireEvent event) {
        LOGGER.debug(">>> HandleWireEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertWireEventToLogEvent(panel, event);

        // handleEvent(eventType, panel.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleWireEvent");
        // eventMonitoringService.meterHandledEvent("wireEventHandled");
    }

    private void handleEntranceEvent(Panel panel, EntranceEvent event) {
        LOGGER.debug(">>> HandleEntranceEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertEntranceEventToLogEvent(panel, event);

        // handleEvent(eventType, securityLogEvent.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< HandleEntranceEvent");
        // eventMonitoringService.meterHandledEvent("entranceEventHandled");
    }


    private void handleUnauthorizedIpAccessEvent(Panel panel, UnauthorizedIpAccessEvent event) {
        LOGGER.debug(">>> handleUnauthorizedIpAccessEvent");

        EventType eventType = event.getEventType();

        // SecurityLogEvent securityLogEvent = pojoService.convertUnauthorizedIpAccessEventToLogEvent(panel, event);

        // handleEvent(eventType, securityLogEvent.getDeviceId(), securityLogEvent, panel);

        LOGGER.debug("<<< handleUnauthorizedIpAccessEvent");
        // eventMonitoringService.meterHandledEvent("unauthorizedIpAccessEventHandled");
    }
*/

/**
 * Construct a
 * string describing
 * the event.
 */
    /*
    private String constructEventDump(String msg, EventType eventType, SecurityLogEvent securityLogEvent) {
        return
                "\n============================================================"
                        + "\n" + msg + ". \nEvent Type ["
                        + eventType + "]. \nEvent Data \n["
                        + ToStringBuilder.reflectionToString(securityLogEvent) + "]."
                        + "\n============================================================";

    }
    */

    /*
    boolean isWirelessLockEvent(SecurityLogEvent securityLogEvent) {
        return securityLogEvent.getSecurityActionId() !=null
            &&(securityLogEvent.getSecurityActionId().

    equals(Long.valueOf(SecurityAction.WIRELESS_LOCK_DISCONNECTED.getSecurityActionValue()))
            ||securityLogEvent.getSecurityActionId().

    equals(Long.valueOf(SecurityAction.WIRELESS_LOCK_RECONNECTED.getSecurityActionValue())));
}
*/

/**
 * Handle Event.
 *
 * @param eventType        EventType
 *                         of the
 *                         event .
 * @param objectId         Object
 *                         Id used
 *                         to derive
 *                         site information.
 * @param securityLogEvent Event
 *                         to process.
 */

    /*
    void handleEvent(EventType eventType, Long objectId, SecurityLogEvent securityLogEvent, Panel panel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug(constructEventDump("Handling Input Event", eventType, securityLogEvent));
        }

        List<Long> siteIds;

        if (

                isWirelessLockEvent(securityLogEvent)) {
            siteIds = getLockSiteIds(securityLogEvent.getActorObjectId());
        } else {
            siteIds = getSiteIds(eventType, objectId);
        }


        if (siteIds.size() == 0) {
            LOGGER.warn(constructEventDump("No Sites for Event", eventType, securityLogEvent));

            return;
        }

        List<SecurityLogEvent> eventList = pojoService.copyEventForSites(securityLogEvent, siteIds);
        EventDataProcessor processor = context.getBean(eventType.getEventDataProcessor());

        for (// SecurityLogEvent logEvent : eventList) {

                eventMonitoringService.incrementResolvingEvents();
        if (

                resolveEvent(eventType, logEvent, panel)) {
            eventMonitoringService.decrementResolvingEvents();

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(constructEventDump(
                        "Sending Resolved Event to Queue", eventType, securityLogEvent));
            }


            processor.doAfterResolve(eventType, logEvent, panel);

            sendToEventQueue(logEvent, eventType);

        } else {
            eventMonitoringService.meterFailedEventResolution();
            eventMonitoringService.decrementResolvingEvents();
            LOGGER.warn(constructEventDump(
                    "Unable to Resolve Event", eventType, securityLogEvent));
        }
    }

        LOGGER.debug("<<< HandleEvent");
}
*/

    /*
    private List<Long> getLockSiteIds(Long objectId) {
        List<Long> sites = siteDao.getSiteIdsForDevice(objectId);
        return sites;
    }
    */

/**
 * Return the
 * site IDs for
 * the supplied
 * board/device/
 * panel object
 * <p>
 * Id.We figure
 * out which
 * based on
 * the EventType.
 * These queries
 * <p>
 * are expected
 * to return
 * one site
 * Id most
 * of the
 * time.We do
 * this
 * on the
 * off chance
 * that the
 * panel is
 * associated with
 * more
 * <p>
 * than one
 * site .
 *
 * @param eventType EventType
 * @param objectId  Object
 *                  Id .
 * @return List of
 * site IDs.
 */

    /*
    private List<Long> getSiteIds(EventType eventType, Long objectId) {
        LOGGER.debug(">>> GetSiteIds");
        List<Long> siteIds;

        if ((objectId == null) || eventType.getSiteIdsProcessor() == null) {
            siteIds = eventNotHandledDataProcessor.getSiteIds(objectId);
        } else {
            siteIds = context.getBean(eventType.getSiteIdsProcessor()).getSiteIds(objectId);
        }

        LOGGER.debug("<<< GetSiteIds");

        return siteIds;
    }
    */

/**
 * Resolve the
 * event.Query for
 * any additional
 * data that
 * <p>
 * will be
 * needed downstream.
 *
 * @param eventType EventType.
 * @param logEvent  The
 *                  event .
 * @return True if
 * the resolution
 * succeeded .
 */
    /*
    private Boolean resolveEvent(EventType eventType, SecurityLogEvent logEvent, Panel panel) {
                                 LOGGER.debug(">>> ResolveEvent");

    Boolean result = false;

        if(eventType.getEventDataProcessor()!=null)

    {
        result = context.getBean(eventType.getEventDataProcessor()).resolveEvent(eventType, logEvent, panel);
    }else

    {
        LOGGER.warn("No event resolution for event [" + logEvent + "].");
        eventNotHandledDataProcessor.resolveEvent(eventType, logEvent, panel);
    }

                LOGGER.debug("<<< ResolveEvent");

                return result;
}
*/

/**
 * Send event
 * to the
 * event queue.
 *
 * @param securityLogEvent The
 *                         Event .
 */

    /*
    private void sendToEventQueue(SecurityLogEvent securityLogEvent, EventType eventType) {
        LOGGER.debug(">>> SendToEventQueue");
        eventMonitoringService.incrementSendingEvents();

        try {
            eventHandler.send(securityLogEvent);
            eventMonitoringService.decrementSendingEvents();
            eventMonitoringService.meterSuccessfulSentEvent();
        } catch (
                Exception e) {
            eventMonitoringService.decrementSendingEvents();
            eventMonitoringService.meterFailedSentEvent();
            throw e;
        }
        LOGGER.debug("<<< SendToEventQueue");
    }
    */

    /*
    private boolean checkEventForLimiting(Panel panel, Event eventToCheck) {
        if (eventIdsToLimit.contains(eventToCheck.getSecurityActionId())) {

            Long securityActionId = eventToCheck.getSecurityActionId();
            String key = panel.getElectronicSerialNumber() + "." + securityActionId;
            Instant eventOccured = eventToCheck.getEventTime();
            EventCounter counter = securityActionIdsByBrainIdsWithEventCounterCache.get(key);
            if (counter == null) {
                EventCounter newCounter = new EventCounter(eventOccured, securityActionId);
                securityActionIdsByBrainIdsWithEventCounterCache.put(key, newCounter);
                return false;
            }
            if (counter.isInThrottleTime(eventOccured, eventLimitSeconds)) {
                counter.incrementCount(eventOccured);
                counter.setSecurityActionId(securityActionId);

                String eventName = String.valueOf(eventToCheck.getSecurityActionId());
                LOGGER.debug(String.format("Ignoring event %s", eventName));

                eventMonitoringService.meterIgnoredEvent(eventToCheck.getSecurityActionId());
                return true;
            }
            counter.setFirstEventTime(eventOccured);
            counter.setEventCountToOne();
            counter.setSecurityActionId(securityActionId);

            return false;
        }
        return false;
    }
    */
}
