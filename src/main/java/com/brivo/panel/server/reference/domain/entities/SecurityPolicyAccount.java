package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "security_policy_account", schema = "brivo20", catalog = "onair")
public class SecurityPolicyAccount {
    private long policyId;
    private long accountId;
    private SecurityPolicy securityPolicyByPolicyId;
    private Account accountByAccountId;

    @Id
    @Basic
    @Column(name = "policy_id", nullable = false)
    public long getPolicyId() {
        return policyId;
    }

    public void setPolicyId(long policyId) {
        this.policyId = policyId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityPolicyAccount that = (SecurityPolicyAccount) o;

        if (policyId != that.policyId) {
            return false;
        }
        return accountId == that.accountId;
    }

    @Override
    public int hashCode() {
        int result = (int) (policyId ^ (policyId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "policy_id", referencedColumnName = "policy_id", nullable = false, insertable = false, updatable = false)
    public SecurityPolicy getSecurityPolicyByPolicyId() {
        return securityPolicyByPolicyId;
    }

    public void setSecurityPolicyByPolicyId(SecurityPolicy securityPolicyByPolicyId) {
        this.securityPolicyByPolicyId = securityPolicyByPolicyId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
