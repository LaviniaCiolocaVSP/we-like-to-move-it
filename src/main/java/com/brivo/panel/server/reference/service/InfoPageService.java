package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.dto.ui.UiInfoDTO;
import com.brivo.panel.server.reference.service.util.FolderUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class InfoPageService {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(FileService.class);
    
    private static final String PATH_SEPARATOR = File.separator;
    
    @Value("${info-folder}")
    private String infoFolder;
    
    @Value("${info-document}")
    private String infoFileName;
    
    
    @PostConstruct
    public void initialize() {
        FolderUtils.checkFolderExistence(infoFolder, "info");
    }
    
    public UiInfoDTO getInfoFileContents() {
        UiInfoDTO uiInfoDTO = null;
        
        try {
            
            InputStream in = this.getClass().getClassLoader().getResourceAsStream(infoFolder + PATH_SEPARATOR + infoFileName);
            byte[] jsonData = IOUtils.toByteArray(in);
            
            ObjectMapper objectMapper = new ObjectMapper();
            uiInfoDTO = objectMapper.readValue(jsonData, UiInfoDTO.class);
        } catch (IOException e) {
            LOGGER.error("Info file not found!");
            e.printStackTrace();
        }
        
        return uiInfoDTO;
    }
}
