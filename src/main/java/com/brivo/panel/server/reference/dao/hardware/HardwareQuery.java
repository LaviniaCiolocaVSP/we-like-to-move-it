package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum HardwareQuery implements FileQuery {
    SELECT_ALLEGION_GATEWAY_BOARDS,
    SELECT_ALLEGION_LOCKS,
    SELECT_BOARDS,
    SELECT_DEFAULT_PANEL_PROPS,
    SELECT_DOORS,
    SELECT_ELEVATORS,
    SELECT_EVENT_TRACK_DEVICES,
    SELECT_FLOORS,
    SELECT_GROUP_DEVICE_PERMISSION,
    SELECT_PROGRAMMABLE_OUTPUT_POINTS,
    SELECT_SALTO_LOCKS,
    SELECT_SALTO_ROUTER,
    SELECT_SWITCH_DEVICES,
    SELECT_TIMER_DEVICES,
    SELECT_VALID_CREDENTIAL_DEVICES,
    SELECT_RS485_SETTINGS,
    SELECT_BOARD_BY_ID,
    SELECT_IO_POINTS_BY_DEVICE,
    SELECT_OSDP_POINT_ADDRESSES,
    SELECT_PANEL_TIMEZONE,
    GET_ALLEGION_LOCK_BY_OBJECT_ID;

    private String query;

    HardwareQuery() {
        this.query = QueryUtils.getQueryText(HardwareQuery.class, this.name());
    }

    public String getQuery() {
        return this.query;
    }

    public String toString() {
        return query;
    }
}
