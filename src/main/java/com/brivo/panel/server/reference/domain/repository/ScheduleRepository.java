package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Schedule;
import com.brivo.panel.server.reference.dto.ui.UiScheduleSummaryDTO;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

public interface ScheduleRepository extends CrudRepository<Schedule, Long> {

    List<Schedule> findByAccountId(@Param("accountId") long accountId);

    @Query(
            value = "SELECT coalesce(max(s.schedule_id),0) FROM brivo20.schedule s",
            nativeQuery = true
    )
    Optional<Long> getMaxScheduleId();

    @Query(
            value = "SELECT coalesce(max(s.cadm_schedule_id),255) FROM brivo20.schedule s WHERE account_id = :accountId AND cadm_schedule_id > 255 ",
            nativeQuery = true
    )
    Optional<Long> getMaxCadmScheduleId(@Param("accountId") long objectId);

    @Query(
            value = "select sc.* from brivo20.schedule sc join brivo20.schedule_holiday_map shm on shm.schedule_oid = sc.schedule_id where shm.holiday_oid=:holidayId",
            nativeQuery = true
    )
    Collection<Schedule> findByHolidayId(@Param("holidayId") long holidayId);

    @Query(
            value = "select count(*) from schedule s where s.account_id = :account_id and s.cadm_schedule_id not in (1,254,255)",
            nativeQuery = true
    )
    long getScheduleCountForAccount(@Param("account_id") long accountId);


    @Query(
            value = "SELECT s.* " +
                    "FROM brivo20.schedule s " +
                    "WHERE account_id = :accountId " +
                    "AND s.schedule_type_id = :scheduleTypeId " +
                    "AND s.site_id in (0, :siteId)",
            nativeQuery = true
    )
    List<Schedule> findByAccountIdScheduleTypeIdSiteId(@Param("accountId") long accountId,
                                                       @Param("siteId") long siteId,
                                                       @Param("scheduleTypeId") long scheduleTypeId);

    @Query(
            value = "SELECT s.* " +
                    "FROM brivo20.schedule s " +
                    "WHERE account_id = :accountId " +
                    "AND s.site_id in (0, :siteId)",
            nativeQuery = true
    )
    List<Schedule> findByAccountIdSiteId(@Param("accountId") long accountId,
                                         @Param("siteId") long siteId);

    @Query(
            value = "SELECT new com.brivo.panel.server.reference.dto.ui.UiScheduleSummaryDTO(s.scheduleId, s.name, sg.name, s.siteId) " +
                    "FROM Schedule s " +
                    "LEFT JOIN SecurityGroup sg ON sg.objectId = s.siteId " +
                    "WHERE s.accountId = :accountId " +
                    "AND s.scheduleTypeId = :scheduleTypeId " +
                    "AND s.siteId in (0, :siteId) " +
                    "AND s.cadmScheduleId != 254 " +
                    "ORDER BY s.siteId, s.name"
    )
    List<UiScheduleSummaryDTO> getUiSummaryDTOsByTypeIdAndSiteId(@Param("accountId") long accountId,
                                                                 @Param("siteId") long siteId,
                                                                 @Param("scheduleTypeId") long scheduleTypeId);

    @Query(
            value = "SELECT s.schedule_id FROM brivo20.schedule s WHERE s.cadm_schedule_id = 254",
            nativeQuery = true
    )
    Long getIdOfScheduleAlwaysAccess();

    @Query(
            value = "SELECT s.schedule_id FROM brivo20.schedule s WHERE s.cadm_schedule_id = 255",
            nativeQuery = true
    )
    Long getIdOfDefaultSchedule();

    @Query(
            value = "SELECT s.* FROM brivo20.schedule s WHERE s.cadm_schedule_id = 255",
            nativeQuery = true
    )
    Schedule getDefaultSchedule();
}
