package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Arrays;

@Entity
@Table(name = "object_permission_schedule_v", schema = "brivo20", catalog = "onair")
public class ObjectPermissionScheduleV {
    private Long securityActionId;
    private Long actorObjectId;
    private Long objectId;
    private Short ignoreLockdown;
    private Timestamp objectPermissionCreated;
    private Timestamp objectPermissionUpdated;
    private Long scheduleId;
    private Long accountId;
    private Long scheduleTypeId;
    private String scheduleName;
    private String scheduleDescription;
    private byte[] schedule;
    private Long cadmScheduleId;
    private Timestamp scheduleCreated;
    private Timestamp scheduleUpdated;

    @Id
    @Basic
    @Column(name = "security_action_id", nullable = true)
    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "object_id", nullable = true)
    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    @Basic
    @Column(name = "ignore_lockdown", nullable = true)
    public Short getIgnoreLockdown() {
        return ignoreLockdown;
    }

    public void setIgnoreLockdown(Short ignoreLockdown) {
        this.ignoreLockdown = ignoreLockdown;
    }

    @Basic
    @Column(name = "object_permission_created", nullable = true)
    public Timestamp getObjectPermissionCreated() {
        return objectPermissionCreated;
    }

    public void setObjectPermissionCreated(Timestamp objectPermissionCreated) {
        this.objectPermissionCreated = objectPermissionCreated;
    }

    @Basic
    @Column(name = "object_permission_updated", nullable = true)
    public Timestamp getObjectPermissionUpdated() {
        return objectPermissionUpdated;
    }

    public void setObjectPermissionUpdated(Timestamp objectPermissionUpdated) {
        this.objectPermissionUpdated = objectPermissionUpdated;
    }

    @Basic
    @Column(name = "schedule_id", nullable = true)
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "schedule_type_id", nullable = true)
    public Long getScheduleTypeId() {
        return scheduleTypeId;
    }

    public void setScheduleTypeId(Long scheduleTypeId) {
        this.scheduleTypeId = scheduleTypeId;
    }

    @Basic
    @Column(name = "schedule_name", nullable = true, length = 32)
    public String getScheduleName() {
        return scheduleName;
    }

    public void setScheduleName(String scheduleName) {
        this.scheduleName = scheduleName;
    }

    @Basic
    @Column(name = "schedule_description", nullable = true, length = 256)
    public String getScheduleDescription() {
        return scheduleDescription;
    }

    public void setScheduleDescription(String scheduleDescription) {
        this.scheduleDescription = scheduleDescription;
    }

    @Basic
    @Column(name = "schedule", nullable = true)
    public byte[] getSchedule() {
        return schedule;
    }

    public void setSchedule(byte[] schedule) {
        this.schedule = schedule;
    }

    @Basic
    @Column(name = "cadm_schedule_id", nullable = true)
    public Long getCadmScheduleId() {
        return cadmScheduleId;
    }

    public void setCadmScheduleId(Long cadmScheduleId) {
        this.cadmScheduleId = cadmScheduleId;
    }

    @Basic
    @Column(name = "schedule_created", nullable = true)
    public Timestamp getScheduleCreated() {
        return scheduleCreated;
    }

    public void setScheduleCreated(Timestamp scheduleCreated) {
        this.scheduleCreated = scheduleCreated;
    }

    @Basic
    @Column(name = "schedule_updated", nullable = true)
    public Timestamp getScheduleUpdated() {
        return scheduleUpdated;
    }

    public void setScheduleUpdated(Timestamp scheduleUpdated) {
        this.scheduleUpdated = scheduleUpdated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectPermissionScheduleV that = (ObjectPermissionScheduleV) o;

        if (securityActionId != null ? !securityActionId.equals(that.securityActionId) : that.securityActionId != null) {
            return false;
        }
        if (actorObjectId != null ? !actorObjectId.equals(that.actorObjectId) : that.actorObjectId != null) {
            return false;
        }
        if (objectId != null ? !objectId.equals(that.objectId) : that.objectId != null) {
            return false;
        }
        if (ignoreLockdown != null ? !ignoreLockdown.equals(that.ignoreLockdown) : that.ignoreLockdown != null) {
            return false;
        }
        if (objectPermissionCreated != null ? !objectPermissionCreated.equals(that.objectPermissionCreated) : that.objectPermissionCreated != null) {
            return false;
        }
        if (objectPermissionUpdated != null ? !objectPermissionUpdated.equals(that.objectPermissionUpdated) : that.objectPermissionUpdated != null) {
            return false;
        }
        if (scheduleId != null ? !scheduleId.equals(that.scheduleId) : that.scheduleId != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (scheduleTypeId != null ? !scheduleTypeId.equals(that.scheduleTypeId) : that.scheduleTypeId != null) {
            return false;
        }
        if (scheduleName != null ? !scheduleName.equals(that.scheduleName) : that.scheduleName != null) {
            return false;
        }
        if (scheduleDescription != null ? !scheduleDescription.equals(that.scheduleDescription) : that.scheduleDescription != null) {
            return false;
        }
        if (!Arrays.equals(schedule, that.schedule)) {
            return false;
        }
        if (cadmScheduleId != null ? !cadmScheduleId.equals(that.cadmScheduleId) : that.cadmScheduleId != null) {
            return false;
        }
        if (scheduleCreated != null ? !scheduleCreated.equals(that.scheduleCreated) : that.scheduleCreated != null) {
            return false;
        }
        return scheduleUpdated != null ? scheduleUpdated.equals(that.scheduleUpdated) : that.scheduleUpdated == null;
    }

    @Override
    public int hashCode() {
        int result = securityActionId != null ? securityActionId.hashCode() : 0;
        result = 31 * result + (actorObjectId != null ? actorObjectId.hashCode() : 0);
        result = 31 * result + (objectId != null ? objectId.hashCode() : 0);
        result = 31 * result + (ignoreLockdown != null ? ignoreLockdown.hashCode() : 0);
        result = 31 * result + (objectPermissionCreated != null ? objectPermissionCreated.hashCode() : 0);
        result = 31 * result + (objectPermissionUpdated != null ? objectPermissionUpdated.hashCode() : 0);
        result = 31 * result + (scheduleId != null ? scheduleId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (scheduleTypeId != null ? scheduleTypeId.hashCode() : 0);
        result = 31 * result + (scheduleName != null ? scheduleName.hashCode() : 0);
        result = 31 * result + (scheduleDescription != null ? scheduleDescription.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(schedule);
        result = 31 * result + (cadmScheduleId != null ? cadmScheduleId.hashCode() : 0);
        result = 31 * result + (scheduleCreated != null ? scheduleCreated.hashCode() : 0);
        result = 31 * result + (scheduleUpdated != null ? scheduleUpdated.hashCode() : 0);
        return result;
    }
}
