package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "login_group", schema = "brivo20", catalog = "onair")
public class LoginGroup {
    private long groupId;
    private long applicationId;
    private String name;
    private short legacy;
    private long externalId;
    private long externalTypeId;
    private short deleted;
    private Short canResetPassword;
    private Short allowSoftLockout;
    private Long softLockoutTimeout;
    private short initiallyDeactivated;
    private Collection<Identity> identitiesByGroupId;
    private SsoApplication ssoApplicationByApplicationId;
    private ExternalIdType externalIdTypeByExternalTypeId;

    @Id
    @Column(name = "group_id", nullable = false)
    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Basic
    @Column(name = "application_id", nullable = false)
    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "legacy", nullable = false)
    public short getLegacy() {
        return legacy;
    }

    public void setLegacy(short legacy) {
        this.legacy = legacy;
    }

    @Basic
    @Column(name = "external_id", nullable = false)
    public long getExternalId() {
        return externalId;
    }

    public void setExternalId(long externalId) {
        this.externalId = externalId;
    }

    @Basic
    @Column(name = "external_type_id", nullable = false)
    public long getExternalTypeId() {
        return externalTypeId;
    }

    public void setExternalTypeId(long externalTypeId) {
        this.externalTypeId = externalTypeId;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "can_reset_password", nullable = true)
    public Short getCanResetPassword() {
        return canResetPassword;
    }

    public void setCanResetPassword(Short canResetPassword) {
        this.canResetPassword = canResetPassword;
    }

    @Basic
    @Column(name = "allow_soft_lockout", nullable = true)
    public Short getAllowSoftLockout() {
        return allowSoftLockout;
    }

    public void setAllowSoftLockout(Short allowSoftLockout) {
        this.allowSoftLockout = allowSoftLockout;
    }

    @Basic
    @Column(name = "soft_lockout_timeout", nullable = true)
    public Long getSoftLockoutTimeout() {
        return softLockoutTimeout;
    }

    public void setSoftLockoutTimeout(Long softLockoutTimeout) {
        this.softLockoutTimeout = softLockoutTimeout;
    }

    @Basic
    @Column(name = "initially_deactivated", nullable = false)
    public short getInitiallyDeactivated() {
        return initiallyDeactivated;
    }

    public void setInitiallyDeactivated(short initiallyDeactivated) {
        this.initiallyDeactivated = initiallyDeactivated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LoginGroup that = (LoginGroup) o;

        if (groupId != that.groupId) {
            return false;
        }
        if (applicationId != that.applicationId) {
            return false;
        }
        if (legacy != that.legacy) {
            return false;
        }
        if (externalId != that.externalId) {
            return false;
        }
        if (externalTypeId != that.externalTypeId) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (initiallyDeactivated != that.initiallyDeactivated) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (canResetPassword != null ? !canResetPassword.equals(that.canResetPassword) : that.canResetPassword != null) {
            return false;
        }
        if (allowSoftLockout != null ? !allowSoftLockout.equals(that.allowSoftLockout) : that.allowSoftLockout != null) {
            return false;
        }
        return softLockoutTimeout != null ? softLockoutTimeout.equals(that.softLockoutTimeout) : that.softLockoutTimeout == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (groupId ^ (groupId >>> 32));
        result = 31 * result + (int) (applicationId ^ (applicationId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) legacy;
        result = 31 * result + (int) (externalId ^ (externalId >>> 32));
        result = 31 * result + (int) (externalTypeId ^ (externalTypeId >>> 32));
        result = 31 * result + (int) deleted;
        result = 31 * result + (canResetPassword != null ? canResetPassword.hashCode() : 0);
        result = 31 * result + (allowSoftLockout != null ? allowSoftLockout.hashCode() : 0);
        result = 31 * result + (softLockoutTimeout != null ? softLockoutTimeout.hashCode() : 0);
        result = 31 * result + (int) initiallyDeactivated;
        return result;
    }

    @OneToMany(mappedBy = "loginGroupByGroupId")
    public Collection<Identity> getIdentitiesByGroupId() {
        return identitiesByGroupId;
    }

    public void setIdentitiesByGroupId(Collection<Identity> identitiesByGroupId) {
        this.identitiesByGroupId = identitiesByGroupId;
    }

    @ManyToOne
    @JoinColumn(name = "application_id", referencedColumnName = "application_id", nullable = false, insertable = false, updatable = false)
    public SsoApplication getSsoApplicationByApplicationId() {
        return ssoApplicationByApplicationId;
    }

    public void setSsoApplicationByApplicationId(SsoApplication ssoApplicationByApplicationId) {
        this.ssoApplicationByApplicationId = ssoApplicationByApplicationId;
    }

    @ManyToOne
    @JoinColumn(name = "external_type_id", referencedColumnName = "external_type_id", nullable = false, insertable = false, updatable = false)
    public ExternalIdType getExternalIdTypeByExternalTypeId() {
        return externalIdTypeByExternalTypeId;
    }

    public void setExternalIdTypeByExternalTypeId(ExternalIdType externalIdTypeByExternalTypeId) {
        this.externalIdTypeByExternalTypeId = externalIdTypeByExternalTypeId;
    }
}
