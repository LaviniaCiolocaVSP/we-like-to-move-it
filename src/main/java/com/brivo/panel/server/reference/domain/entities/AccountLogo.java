package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Arrays;

@Entity
@Table(name = "account_logo", schema = "brivo20", catalog = "onair")
public class AccountLogo {
    private long accountId;
    private byte[] image;
    private String imageContenttype;
    private Long imageMediaByteSize;
    private String imageFilename;
    private Timestamp created;
    private Timestamp updated;
//    private Account accountByAccountId;

    @Id
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "image", nullable = true)
    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Basic
    @Column(name = "image_contenttype", nullable = true, length = 128)
    public String getImageContenttype() {
        return imageContenttype;
    }

    public void setImageContenttype(String imageContenttype) {
        this.imageContenttype = imageContenttype;
    }

    @Basic
    @Column(name = "image_media_byte_size", nullable = true)
    public Long getImageMediaByteSize() {
        return imageMediaByteSize;
    }

    public void setImageMediaByteSize(Long imageMediaByteSize) {
        this.imageMediaByteSize = imageMediaByteSize;
    }

    @Basic
    @Column(name = "image_filename", nullable = true, length = 256)
    public String getImageFilename() {
        return imageFilename;
    }

    public void setImageFilename(String imageFilename) {
        this.imageFilename = imageFilename;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountLogo that = (AccountLogo) o;

        if (accountId != that.accountId) {
            return false;
        }
        if (!Arrays.equals(image, that.image)) {
            return false;
        }
        if (imageContenttype != null ? !imageContenttype.equals(that.imageContenttype) : that.imageContenttype != null) {
            return false;
        }
        if (imageMediaByteSize != null ? !imageMediaByteSize.equals(that.imageMediaByteSize) : that.imageMediaByteSize != null) {
            return false;
        }
        if (imageFilename != null ? !imageFilename.equals(that.imageFilename) : that.imageFilename != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + Arrays.hashCode(image);
        result = 31 * result + (imageContenttype != null ? imageContenttype.hashCode() : 0);
        result = 31 * result + (imageMediaByteSize != null ? imageMediaByteSize.hashCode() : 0);
        result = 31 * result + (imageFilename != null ? imageFilename.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

//    @OneToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false)
//    public Account getAccountByAccountId() {
//        return accountByAccountId;
//    }
//
//    public void setAccountByAccountId(Account accountByAccountId) {
//        this.accountByAccountId = accountByAccountId;
//    }
}
