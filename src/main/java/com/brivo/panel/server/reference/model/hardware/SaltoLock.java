package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class SaltoLock {
    private Long deviceId;
    private Integer lockId;
    private Long unlockScheduleId;
    private Integer passthroughDelay;
    private Boolean privacyModeOn;
    private Integer credentialCachePeriod;
    private List<EnterPermission> enterPermissions;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getLockId() {
        return lockId;
    }

    public void setLockId(Integer lockId) {
        this.lockId = lockId;
    }

    public Long getUnlockScheduleId() {
        return unlockScheduleId;
    }

    public void setUnlockScheduleId(Long unlockScheduleId) {
        this.unlockScheduleId = unlockScheduleId;
    }

    public Integer getPassthroughDelay() {
        return passthroughDelay;
    }

    public void setPassthroughDelay(Integer passthroughDelay) {
        this.passthroughDelay = passthroughDelay;
    }

    public Boolean getPrivacyModeOn() {
        return privacyModeOn;
    }

    public void setPrivacyModeOn(Boolean privacyModeOn) {
        this.privacyModeOn = privacyModeOn;
    }

    public Integer getCredentialCachePeriod() {
        return credentialCachePeriod;
    }

    public void setCredentialCachePeriod(Integer credentialCachePeriod) {
        this.credentialCachePeriod = credentialCachePeriod;
    }

    public List<EnterPermission> getEnterPermissions() {
        return enterPermissions;
    }

    public void setEnterPermissions(List<EnterPermission> enterPermissions) {
        this.enterPermissions = enterPermissions;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }
}
