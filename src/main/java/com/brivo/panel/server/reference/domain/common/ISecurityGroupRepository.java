package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.SecurityGroup;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

@Repository
public interface ISecurityGroupRepository extends CrudRepository<SecurityGroup, Long> {

    @Query(
            value = "SELECT sgm.security_group_id " +
                    "FROM brivo20.security_group sg " +
                    "JOIN brivo20.security_group_member sgm on sg.security_group_id = sgm.security_group_id " +
                    "JOIN brivo20.security_group_type sgt on sgt.security_group_type_id = sg.security_group_type_id " +
                    "WHERE sgt.security_group_type_id = 1 " +
                    "AND sgm.object_id = :deviceObjectId",
            nativeQuery = true
    )
    Optional<List<BigInteger>> findByDeviceObjectId(@Param("deviceObjectId") long deviceObjectId);

    @Query(
            value = "SELECT sgm.security_group_id " +
                    "FROM brivo20.security_group_member sgm " +
                    "WHERE sgm.object_id = :userObjectId",
            nativeQuery = true
    )
    Optional<List<BigInteger>> findByUserObjectId(@Param("userObjectId") Long userObjectId);
}