package com.brivo.panel.server.reference.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public enum IOBoardIOPointMap 
{
    NULL_STATE(-1),
    INPUT_1(1)
    {
        public String toString()
        {
            return "INPUT 1";
        }
    },
    INPUT_2(2)
    {
        public String toString()
        {
            return "INPUT 2";
        }
    },
    INPUT_3(3)
    {
        public String toString()
        {
            return "INPUT 3";
        }
    },
    INPUT_4(4)
    {
        public String toString()
        {
            return "INPUT 4";
        }
    },
    INPUT_5(5)
    {
        public String toString()
        {
            return "INPUT 5";
        }
    },
    INPUT_6(6)
    {
        public String toString()
        {
            return "INPUT 6";
        }
    },
    INPUT_7(7)
    {
        public String toString()
        {
            return "INPUT 7";
        }
    },
    INPUT_8(8)
    {
        public String toString()
        {
            return "INPUT 8";
        }
    },
    OUTPUT_1(9)
    {
        public String toString()
        {
            return "OUTPUT 1";
        }
    },
    OUTPUT_2(10)
    {
        public String toString()
        {
            return "OUTPUT 2";
        }
    },
    OUTPUT_3(11)
    {
        public String toString()
        {
            return "OUTPUT 3";
        }
    },
    OUTPUT_4(12)
    {
        public String toString()
        {
            return "OUTPUT 4";
        }
    },
    OUTPUT_5(13)
    {
        public String toString()
        {
            return "OUTPUT 5";
        }
    },
    OUTPUT_6(14)
    {
        public String toString()
        {
            return "OUTPUT 6";
        }
    },
    OUTPUT_7(15)
    {
        public String toString()
        {
            return "OUTPUT 7";
        }
    },
    OUTPUT_8(16)
    {
        public String toString()
        {
            return "OUTPUT 8";
        }
    };

    private static Map<Integer,IOBoardIOPointMap> byStateID =
        new HashMap<Integer,IOBoardIOPointMap>();

    static
    {
        byStateID.put(1, INPUT_1);
        byStateID.put(2, INPUT_2);
        byStateID.put(3, INPUT_3);
        byStateID.put(5, INPUT_4);
        byStateID.put(6, INPUT_5);
        byStateID.put(7, INPUT_6);
        byStateID.put(8, INPUT_7);
        byStateID.put(10, INPUT_8);
        byStateID.put(14, OUTPUT_1);
        byStateID.put(15, OUTPUT_2);
        byStateID.put(16, OUTPUT_3);
        byStateID.put(18, OUTPUT_4);
        byStateID.put(19, OUTPUT_5);
        byStateID.put(20, OUTPUT_6);
        byStateID.put(21, OUTPUT_7);
        byStateID.put(23, OUTPUT_8);
    };



    private int state;

    private IOBoardIOPointMap(int state)
    {
        this.state = state;
    }

    public int getState()
    {
        return this.state;
    }

    public String getDisplayText()
    {
        return this.toString();
    }

    /**
     * Convert an int state id value into the
     * corresponding enumeration value.
     *
     * @param stateID
     * @return
     */
    public static IOBoardIOPointMap getStateByID(int stateID)
    {
        IOBoardIOPointMap state = byStateID.get(stateID);

        if(state == null)
        {
            state = NULL_STATE;
        }

        return state;
    }

    public static List<IOBoardIOPointMap> getAllStates()
    {
        List<IOBoardIOPointMap> states = new ArrayList<IOBoardIOPointMap>();

        states.add(INPUT_1);
        states.add(INPUT_2);
        states.add(INPUT_3);
        states.add(INPUT_4);
        states.add(INPUT_5);
        states.add(INPUT_6);
        states.add(INPUT_7);
        states.add(INPUT_8);
        states.add(OUTPUT_1);
        states.add(OUTPUT_2);
        states.add(OUTPUT_3);
        states.add(OUTPUT_4);
        states.add(OUTPUT_5);
        states.add(OUTPUT_6);
        states.add(OUTPUT_7);
        states.add(OUTPUT_8);

        return states;
    }
}
