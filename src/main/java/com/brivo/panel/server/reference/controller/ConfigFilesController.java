package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.service.FileService;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.Set;

@RestController
@RequestMapping("/configFiles")
public class ConfigFilesController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ConfigFilesController.class);

    private final FileService fileService;

    @Autowired
    public ConfigFilesController(final FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping
    public Set<String> getConfigFilesNames() {
        LOGGER.info("Returning names of config files");

        return fileService.getConfigFilesNames();
    }

    @GetMapping(
            path = "/{fileName}",
            produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
    )
    public ResponseEntity<StreamingResponseBody> getConfigFileByName(@PathVariable final String fileName) {
        LOGGER.info("Downloading config files");

        return fileService.downloadConfigFile(fileName);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public MessageDTO uploadNewConfigFile(@RequestParam("file") final MultipartFile file) {
        return fileService.uploadNewConfigFile(file);
    }

    @PutMapping(
            path = "/{fileName}",
            consumes = MediaType.TEXT_PLAIN_VALUE
    )
    public MessageDTO saveUpdatedConfigFile(@PathVariable final String fileName,
                                            @RequestBody final String fileContent) {
        LOGGER.info("Will save the updated configFile {}", fileName);

        return fileService.saveUpdatedConfigFile(fileName, fileContent);
    }

    @DeleteMapping(path = "/{fileName}")
    public MessageDTO deleteConfigurationFile(@PathVariable final String fileName) {
        return fileService.deleteConfigurationFile(fileName);
    }

    @PutMapping(path = "/generateConfigurationFiles")
    public MessageDTO generateConfigurationFilesBasedOnIP() {
        LOGGER.info("Will generate the configuration files for panel");

        return fileService.generateConfigurationFilesBasedOnIP();
    }
}
