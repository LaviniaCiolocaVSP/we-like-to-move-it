package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "user_image", schema = "brivo20", catalog = "onair")
public class UserImage {
    private long userImageId;
    private long accountId;
    private String contentType;
    private Long mediaByteSize;
    private String filename;
    private Timestamp created;
    private Timestamp expires;
    private Account accountByAccountId;
    private Collection<Users> usersByUserImageId;

    @Id
    @Column(name = "user_image_id", nullable = false)
    public long getUserImageId() {
        return userImageId;
    }

    public void setUserImageId(long userImageId) {
        this.userImageId = userImageId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "content_type", nullable = true, length = 128)
    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    @Basic
    @Column(name = "media_byte_size", nullable = true)
    public Long getMediaByteSize() {
        return mediaByteSize;
    }

    public void setMediaByteSize(Long mediaByteSize) {
        this.mediaByteSize = mediaByteSize;
    }

    @Basic
    @Column(name = "filename", nullable = true, length = 256)
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserImage userImage = (UserImage) o;

        if (userImageId != userImage.userImageId) {
            return false;
        }
        if (accountId != userImage.accountId) {
            return false;
        }
        if (contentType != null ? !contentType.equals(userImage.contentType) : userImage.contentType != null) {
            return false;
        }
        if (mediaByteSize != null ? !mediaByteSize.equals(userImage.mediaByteSize) : userImage.mediaByteSize != null) {
            return false;
        }
        if (filename != null ? !filename.equals(userImage.filename) : userImage.filename != null) {
            return false;
        }
        if (created != null ? !created.equals(userImage.created) : userImage.created != null) {
            return false;
        }
        return expires != null ? expires.equals(userImage.expires) : userImage.expires == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userImageId ^ (userImageId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (contentType != null ? contentType.hashCode() : 0);
        result = 31 * result + (mediaByteSize != null ? mediaByteSize.hashCode() : 0);
        result = 31 * result + (filename != null ? filename.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (expires != null ? expires.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "userImageByUserImageId")
    public Collection<Users> getUsersByUserImageId() {
        return usersByUserImageId;
    }

    public void setUsersByUserImageId(Collection<Users> usersByUserImageId) {
        this.usersByUserImageId = usersByUserImageId;
    }
}
