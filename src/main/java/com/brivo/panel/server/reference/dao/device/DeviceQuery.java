package com.brivo.panel.server.reference.dao.device;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum DeviceQuery implements FileQuery {
    DELETE_DEVICE,
    DELETE_DOOR,
    UPDATE_PROG_IO_POINTS;

    private String query;

    DeviceQuery() {
        this.query = QueryUtils.getQueryText(DeviceQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}