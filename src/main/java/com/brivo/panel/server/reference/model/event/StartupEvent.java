package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class StartupEvent extends Event {
    private String version;
    private Integer firmwareProtocolVersion;

    @JsonCreator
    public StartupEvent(@JsonProperty("eventType") final EventType eventType,
                        @JsonProperty("eventTime") final Instant eventTime,
                        @JsonProperty("version") final String version,
                        @JsonProperty("firmwareProtocolVersion") final Integer firmwareProtocolVersion) {
        super(eventType, eventTime);
        this.version = version;
        this.firmwareProtocolVersion = firmwareProtocolVersion;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public Integer getFirmwareProtocolVersion() {
        return firmwareProtocolVersion;
    }

    public void setFirmwareProtocolVersion(Integer firmwareProtocolVersion) {
        this.firmwareProtocolVersion = firmwareProtocolVersion;
    }

}
