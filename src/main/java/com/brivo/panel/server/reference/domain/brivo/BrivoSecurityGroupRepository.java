package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.ISecurityGroupRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrivoSecurityGroupRepository extends ISecurityGroupRepository {

}
