package com.brivo.panel.server.reference.dao.user;

import com.brivo.panel.server.reference.dao.BrivoDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class UserDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteUserByUserObjectId(Long userObjectId) {
        LOGGER.info("Deleting user with id {}", userObjectId);

        String query = UserQuery.DELETE_USER.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userObjectId", userObjectId);

        jdbcTemplate.update(query, paramMap);
    }
}
