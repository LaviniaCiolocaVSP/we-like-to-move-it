package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "custom_field_definition", schema = "brivo20", catalog = "onair")
@IdClass(CustomFieldDefinitionPK.class)
public class CustomFieldDefinition {
    private long customFieldId;
    private long accountId;
    private String name;
    private long typeId;
    private String pattern;
    private Long displayOrder;
    private BadgeBarcodeItem badgeBarcodeItemByCustomFieldId;
    private BadgeTextItem badgeTextItemByCustomFieldId;
    private Account accountByAccountId;
    private CustomFieldType customFieldTypeByTypeId;
    //private CustomFieldValue customFieldValueByCustomFieldId;
    private EnumeratedField enumeratedFieldByCustomFieldId;

    @Id
    @Column(name = "custom_field_id", nullable = false)
    public long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Id
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "type_id", nullable = false)
    public long getTypeId() {
        return typeId;
    }

    public void setTypeId(long typeId) {
        this.typeId = typeId;
    }

    @Basic
    @Column(name = "pattern", nullable = true, length = 32)
    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Basic
    @Column(name = "display_order", nullable = true)
    public Long getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(Long displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CustomFieldDefinition that = (CustomFieldDefinition) o;

        if (customFieldId != that.customFieldId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (typeId != that.typeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (pattern != null ? !pattern.equals(that.pattern) : that.pattern != null) {
            return false;
        }
        return displayOrder != null ? displayOrder.equals(that.displayOrder) : that.displayOrder == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (customFieldId ^ (customFieldId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (int) (typeId ^ (typeId >>> 32));
        result = 31 * result + (pattern != null ? pattern.hashCode() : 0);
        result = 31 * result + (displayOrder != null ? displayOrder.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "custom_field_id", referencedColumnName = "custom_field_id", nullable = false, insertable = false, updatable = false)
    public BadgeBarcodeItem getBadgeBarcodeItemByCustomFieldId() {
        return badgeBarcodeItemByCustomFieldId;
    }

    public void setBadgeBarcodeItemByCustomFieldId(BadgeBarcodeItem badgeBarcodeItemByCustomFieldId) {
        this.badgeBarcodeItemByCustomFieldId = badgeBarcodeItemByCustomFieldId;
    }

    @ManyToOne
    @JoinColumn(name = "custom_field_id", referencedColumnName = "custom_field_id", nullable = false, insertable = false, updatable = false)
    public BadgeTextItem getBadgeTextItemByCustomFieldId() {
        return badgeTextItemByCustomFieldId;
    }

    public void setBadgeTextItemByCustomFieldId(BadgeTextItem badgeTextItemByCustomFieldId) {
        this.badgeTextItemByCustomFieldId = badgeTextItemByCustomFieldId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "type_id", referencedColumnName = "type_id", nullable = false, insertable = false, updatable = false)
    public CustomFieldType getCustomFieldTypeByTypeId() {
        return customFieldTypeByTypeId;
    }

    public void setCustomFieldTypeByTypeId(CustomFieldType customFieldTypeByTypeId) {
        this.customFieldTypeByTypeId = customFieldTypeByTypeId;
    }

    /*
    @ManyToOne
    @JoinColumn(name = "custom_field_id", referencedColumnName = "custom_field_id", nullable = false)
    public CustomFieldValue getCustomFieldValueByCustomFieldId() {
        return customFieldValueByCustomFieldId;
    }

    public void setCustomFieldValueByCustomFieldId(CustomFieldValue customFieldValueByCustomFieldId) {
        this.customFieldValueByCustomFieldId = customFieldValueByCustomFieldId;
    }
    */

    @ManyToOne
    @JoinColumn(name = "custom_field_id", referencedColumnName = "custom_field_id", nullable = false, insertable = false, updatable = false)
    public EnumeratedField getEnumeratedFieldByCustomFieldId() {
        return enumeratedFieldByCustomFieldId;
    }

    public void setEnumeratedFieldByCustomFieldId(EnumeratedField enumeratedFieldByCustomFieldId) {
        this.enumeratedFieldByCustomFieldId = enumeratedFieldByCustomFieldId;
    }
}
