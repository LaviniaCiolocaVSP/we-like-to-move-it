package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "rs485_settings", schema = "brivo20", catalog = "onair")
@IdClass(Rs485SettingsPK.class)
public class Rs485Settings {
    private long brainId;
    private long port;
    private long operationMode;
    private long baudRate;
    private Long errorDetectionMethod;
    private Brain brainByBrainId;

    @Id
    @Column(name = "brain_id", nullable = false, insertable = false, updatable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Id
    @Column(name = "port", nullable = false)
    public long getPort() {
        return port;
    }

    public void setPort(long port) {
        this.port = port;
    }

    @Basic
    @Column(name = "operation_mode", nullable = false)
    public long getOperationMode() {
        return operationMode;
    }

    public void setOperationMode(long operationMode) {
        this.operationMode = operationMode;
    }

    @Basic
    @Column(name = "baud_rate", nullable = false)
    public long getBaudRate() {
        return baudRate;
    }

    public void setBaudRate(long baudRate) {
        this.baudRate = baudRate;
    }

    @Basic
    @Column(name = "error_detection_method", nullable = true)
    public Long getErrorDetectionMethod() {
        return errorDetectionMethod;
    }

    public void setErrorDetectionMethod(Long errorDetectionMethod) {
        this.errorDetectionMethod = errorDetectionMethod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Rs485Settings that = (Rs485Settings) o;

        if (brainId != that.brainId) {
            return false;
        }
        if (port != that.port) {
            return false;
        }
        if (operationMode != that.operationMode) {
            return false;
        }
        if (baudRate != that.baudRate) {
            return false;
        }
        return errorDetectionMethod != null ? errorDetectionMethod.equals(that.errorDetectionMethod) : that.errorDetectionMethod == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainId ^ (brainId >>> 32));
        result = 31 * result + (int) (port ^ (port >>> 32));
        result = 31 * result + (int) (operationMode ^ (operationMode >>> 32));
        result = 31 * result + (int) (baudRate ^ (baudRate >>> 32));
        result = 31 * result + (errorDetectionMethod != null ? errorDetectionMethod.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByBrainId() {
        return brainByBrainId;
    }

    public void setBrainByBrainId(Brain brainByBrainId) {
        this.brainByBrainId = brainByBrainId;
    }
}
