package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class FloorElevator {
    private Long elevatorId;
    private Integer boardAddress;
    private Integer pointAddress;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getElevatorId() {
        return elevatorId;
    }

    public void setElevatorId(Long elevatorId) {
        this.elevatorId = elevatorId;
    }

    public Integer getBoardAddress() {
        return boardAddress;
    }

    public void setBoardAddress(Integer boardAddress) {
        this.boardAddress = boardAddress;
    }

    public Integer getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Integer pointAddress) {
        this.pointAddress = pointAddress;
    }
}
