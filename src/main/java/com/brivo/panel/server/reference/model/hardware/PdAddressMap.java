package com.brivo.panel.server.reference.model.hardware;

public class PdAddressMap {
    private Integer pdAddress;
    private Integer boardAddress;
    private Integer pointAddress;

    public Integer getPdAddress() {
        return pdAddress;
    }

    public void setPdAddress(Integer pdAddress) {
        this.pdAddress = pdAddress;
    }

    public Integer getBoardAddress() {
        return boardAddress;
    }

    public void setBoardAddress(Integer boardAddress) {
        this.boardAddress = boardAddress;
    }

    public Integer getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Integer pointAddress) {
        this.pointAddress = pointAddress;
    }
}
