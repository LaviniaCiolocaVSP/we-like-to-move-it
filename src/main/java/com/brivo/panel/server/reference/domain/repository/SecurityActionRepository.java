package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.SecurityAction;
import org.springframework.data.repository.CrudRepository;

public interface SecurityActionRepository extends CrudRepository<SecurityAction, Long> {
}
