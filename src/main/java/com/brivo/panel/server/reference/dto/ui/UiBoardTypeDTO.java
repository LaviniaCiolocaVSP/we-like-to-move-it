package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiBoardTypeDTO implements Serializable {
    private final Long boardTypeId;
    private final String boardType;

    public UiBoardTypeDTO(Long boardTypeId, String boardType) {
        this.boardTypeId = boardTypeId;
        this.boardType = boardType;
    }

    public Long getBoardTypeId() {
        return boardTypeId;
    }

    public String getBoardType() {
        return boardType;
    }
}
