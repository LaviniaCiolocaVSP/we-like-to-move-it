package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ObjectPropertyDTO {
    private final Long objectId;
    private final String id;
    private final String value;

    @JsonCreator
    public ObjectPropertyDTO(@JsonProperty("objectId") final Long objectId,
                             @JsonProperty("id") final String id,
                             @JsonProperty("value") final String value) {
        this.objectId = objectId;
        this.id = id;
        this.value = value;
    }

    public Long getObjectId() {
        return objectId;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
