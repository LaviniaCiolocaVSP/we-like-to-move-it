package com.brivo.panel.server.reference.service.processors.site;

import com.brivo.panel.server.reference.dao.site.SiteDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Processor interface indicating the necessary base operation for obtaining the site ids for a particular event
 */
@Service
public class SiteIdsByBoardProcessor implements SiteIdsProcessor {
    @Autowired
    private SiteDao siteDao;

    public List<Long> getSiteIds(Long objectId) {
        return siteDao.getSiteIdsForBoard(objectId);
    }
}
