package com.brivo.panel.server.reference.model;

import com.brivo.panel.server.reference.model.hardware.Panel;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.io.Serializable;
import java.time.Instant;
import java.time.zone.ZoneOffsetTransition;

public class PanelConnectionStatus implements Serializable {
    private static final long serialVersionUID = -4190025635272384977L;

    private Instant lastPanelHeartbeat;
    private Instant lastServerHeartbeat;
    private Instant lastPanelHelloMessage;
    private Instant nextDstTransition;
    private Panel panel;
    private Boolean connected;

    public PanelConnectionStatus(Panel panel) {
        setPanel(panel);
    }

    public boolean transitionDaylightSavings() {
        if (nextDstTransition != null && Instant.now().isAfter(nextDstTransition)) {
            nextDstTransition = nextDstTransition();
            return true;
        }

        return false;
    }

    private Instant nextDstTransition() {
        ZoneOffsetTransition nextTransition = panel.getTimezone().getRules().nextTransition(Instant.now());

        return (nextTransition != null) ? nextTransition.getInstant() : null;
    }

    public Panel getPanel() {
        return panel;
    }

    public void setPanel(Panel panel) {
        if (panel == null || panel.getTimezone() == null) {
            throw new IllegalArgumentException("Panel and panel timezone must be set");
        }
        this.panel = panel;
        this.nextDstTransition = nextDstTransition();
    }

    public Instant getLastPanelHeartbeat() {
        return lastPanelHeartbeat;
    }

    public void setLastPanelHeartbeat(Instant lastPanelHeartbeat) {
        this.lastPanelHeartbeat = lastPanelHeartbeat;
    }

    public Instant getLastServerHeartbeat() {
        return lastServerHeartbeat;
    }

    public void setLastServerHeartbeat(Instant lastServerHeartbeat) {
        this.lastServerHeartbeat = lastServerHeartbeat;
    }

    public Instant getLastPanelHelloMessage() {
        return lastPanelHelloMessage;
    }

    public void setLastPanelHelloMessage(Instant lastPanelHelloMessage) {
        this.lastPanelHelloMessage = lastPanelHelloMessage;
    }

    public Boolean getConnected() {
        return connected;
    }

    public void setConnected(Boolean connected) {
        this.connected = connected;
    }

    public Instant getNextDstTransition() {
        return nextDstTransition;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

}
