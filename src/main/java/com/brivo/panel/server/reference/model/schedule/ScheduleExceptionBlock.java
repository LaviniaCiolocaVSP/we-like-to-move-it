package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.Objects;

public class ScheduleExceptionBlock implements Comparable<ScheduleExceptionBlock> {
    private Boolean enableBlock;
    private Instant startDate;
    private Instant endDate;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Boolean getEnableBlock() {
        return enableBlock;
    }

    public void setEnableBlock(Boolean enableBlock) {
        this.enableBlock = enableBlock;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    @Override
    public int compareTo(ScheduleExceptionBlock block2) {
        if (block2 == null) {
            throw new NullPointerException();
        }

        if (Objects.equals(getStartDate(), block2.getStartDate())) {
            return getEndDate().isBefore(block2.getEndDate()) ? -1 : 1;
        }

        return getStartDate().isBefore(block2.getStartDate()) ? -1 : 1;
    }


}
