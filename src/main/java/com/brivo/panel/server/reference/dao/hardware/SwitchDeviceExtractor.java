package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.SwitchDevice;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Switch Device Extractor
 */
@Component
public class SwitchDeviceExtractor
        implements ResultSetExtractor<List<SwitchDevice>> {
    @Override
    public List<SwitchDevice> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<SwitchDevice> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Integer boardNumber = rs.getInt("boardNumber");
            Integer pointAddress = rs.getInt("pointAddress");
            Long scheduleId = rs.getLong("scheduleID");

            Integer notifyFlagInt = rs.getInt("notifyFlag");
            Boolean notify = false;
            if (notifyFlagInt != null && notifyFlagInt == 1) {
                notify = true;
            }

            Integer notifyDisengageInt = rs.getInt("notifyDisengage");
            Boolean notifyDisengage = false;
            if (notifyDisengageInt != null && notifyDisengageInt == 1) {
                notifyDisengage = true;
            }

            SwitchDevice device = new SwitchDevice();

            device.setDeviceId(deviceId);
            device.setInputBoardAddress(boardNumber);
            device.setInputPointAddress(pointAddress);
            device.setActiveScheduleId(scheduleId);
            device.setReportEngage(notify);
            device.setReportDisengage(notifyDisengage);

            list.add(device);
        }

        return list;
    }
}
