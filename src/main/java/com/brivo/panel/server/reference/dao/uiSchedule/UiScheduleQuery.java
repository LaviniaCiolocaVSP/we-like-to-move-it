package com.brivo.panel.server.reference.dao.uiSchedule;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum UiScheduleQuery implements FileQuery {
    DELETE_SCHEDULE, DELETE_SCHEDULE_DATA_HOLIDAYS;

    private String query;

    UiScheduleQuery() {
        this.query = QueryUtils.getQueryText(UiScheduleQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}