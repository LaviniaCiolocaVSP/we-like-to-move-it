package com.brivo.panel.server.reference.domain.entities.credential;

import java.util.HashMap;
import java.util.Map;

public enum CredentialEncoding
{
    
    BINARY(1, "Binary", 1),
    BCD(2, "Binary Coded Decimal", 4),
    REVERSE_BCD(3,"Reverse BCD", 4),
    BCD_PARITY(4,"BCD + Parity", 5),
    REVERSE_BCD_PARITY(5,"Reverse BCD + Parity", 5);
    
    private long id;
    private String name;
    private int bitsPerDigit;
    private CredentialEncoding(long id, String name, int bitsPerDigit) {
        this.id = id;
        this.name = name;
        this.bitsPerDigit = bitsPerDigit;

    }
    
    public String toString() {
        return name;
    }
    
    public int bitsPerDigit() {
        return bitsPerDigit;    
    }
    
    public long getId() {
        return this.id;
    }
    
    public static final Map<Long, CredentialEncoding> idMap;
    
    static {
        idMap = new HashMap<Long, CredentialEncoding>();
        for (CredentialEncoding encoding : CredentialEncoding.values()) {
            idMap.put(encoding.getId(), encoding);
        }
    }
    
    public static CredentialEncoding byId(long id) {
        return idMap.get(id);
    }

}
