package com.brivo.panel.server.reference.model.hardware;

import java.time.Instant;
import java.time.ZoneId;

/**
 * POJO for brain table.
 */
public class Panel {
    private String electronicSerialNumber;
    private Long brainId;
    private Long objectId;
    private Long accountId;
    private Integer networkId;
    private Firmware firmware;
    private ZoneId timezone;
    private String timeZoneData;
    private Instant panelChanged;
    private Instant personsChanged;
    private Instant schedulesChanged;
    private PanelType panelType;
    private Boolean isRegistered;
    private Integer logPeriod;

    @Override
    public String toString() {
        return "Panel{" +
                "electronicSerialNumber='" + electronicSerialNumber + '\'' +
                ", brainId=" + brainId +
                ", objectId=" + objectId +
                ", accountId=" + accountId +
                ", firmware=" + firmware +
                ", timezone=" + timezone +
                ", panelType=" + panelType +
                ", isRegistered=" + isRegistered +
                ", logPeriod=" + logPeriod +
                '}';
    }

    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    public Long getBrainId() {
        return brainId;
    }

    public void setBrainId(Long brainId) {
        this.brainId = brainId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(Long objectId) {
        this.objectId = objectId;
    }

    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    public Firmware getFirmware() {
        return firmware;
    }

    public void setFirmware(Firmware firmware) {
        this.firmware = firmware;
    }

    public ZoneId getTimezone() {
        return timezone;
    }

    public void setTimezone(ZoneId timezone) {
        this.timezone = timezone;
    }

    public String getTimeZoneData() {
        return timeZoneData;
    }

    public void setTimeZoneData(String timeZoneData) {
        this.timeZoneData = timeZoneData;
    }

    public Instant getPanelChanged() {
        return panelChanged;
    }

    public void setPanelChanged(Instant panelChanged) {
        this.panelChanged = panelChanged;
    }

    public Instant getPersonsChanged() {
        return personsChanged;
    }

    public void setPersonsChanged(Instant personsChanged) {
        this.personsChanged = personsChanged;
    }

    public Instant getSchedulesChanged() {
        return schedulesChanged;
    }

    public void setSchedulesChanged(Instant schedulesChanged) {
        this.schedulesChanged = schedulesChanged;
    }

    public PanelType getPanelType() {
        return panelType;
    }

    public void setPanelType(PanelType panelType) {
        this.panelType = panelType;
    }

    public Boolean getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Boolean isRegistered) {
        this.isRegistered = isRegistered;
    }

    public Integer getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Integer networkId) {
        this.networkId = networkId;
    }

    public Integer getLogPeriod() {
        return logPeriod;
    }

    public void setLogPeriod(Integer logPeriod) {
        this.logPeriod = logPeriod;
    }
}

