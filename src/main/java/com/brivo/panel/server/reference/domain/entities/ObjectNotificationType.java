package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "object_notification_type", schema = "brivo20", catalog = "onair")
@IdClass(ObjectNotificationTypePK.class)
public class ObjectNotificationType {
    private long notificationTypeId;
    private long objectId;
    private NotificationType notificationTypeByNotificationTypeId;
    private BrivoObject objectByBrivoObjectId;

    @Id
    @Column(name = "notification_type_id", nullable = false)
    public long getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(long notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    @Id
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ObjectNotificationType that = (ObjectNotificationType) o;

        if (notificationTypeId != that.notificationTypeId) {
            return false;
        }
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        int result = (int) (notificationTypeId ^ (notificationTypeId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "notification_type_id", referencedColumnName = "notification_type_id", nullable = false,
            insertable = false, updatable = false)
    public NotificationType getNotificationTypeByNotificationTypeId() {
        return notificationTypeByNotificationTypeId;
    }

    public void setNotificationTypeByNotificationTypeId(NotificationType notificationTypeByNotificationTypeId) {
        this.notificationTypeByNotificationTypeId = notificationTypeByNotificationTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }
}
