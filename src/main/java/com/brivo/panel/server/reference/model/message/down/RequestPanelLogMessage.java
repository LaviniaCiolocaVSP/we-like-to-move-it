package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class RequestPanelLogMessage extends DownstreamMessage {

    private final Long objectId;

    public RequestPanelLogMessage(final Long objectId) {
        super(DownstreamMessageType.Request_Panel_Log);
        this.objectId = objectId;
    }

    public Long getObjectId() {
        return objectId;
    }
}

