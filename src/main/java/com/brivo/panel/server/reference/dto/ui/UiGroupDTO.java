package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;
import java.util.Collection;

public class UiGroupDTO implements Serializable {

    private long accountId;
    private long objectId;
    private Long groupId;
    private String name;
    private String description;
    private UiGroupAntipassbackDTO uiGroupAntipassbackDTO;
    private boolean keypadUnlockHoldPrivileges;
    private Long numberOfUsers;

    public UiGroupDTO() {

    }

    public UiGroupDTO(long objectId, String name) {
        this.objectId = objectId;
        this.name = name;
    }

    public UiGroupDTO(long accountId, Long groupId, String name) {
        this.accountId = accountId;
        this.groupId = groupId;
        this.name = name;

    }

    public UiGroupDTO(Long objectId, long groupId, String name, String description, Long numberOfUsers) {
        this.objectId = objectId;
        this.groupId = groupId;
        this.name = name;
        this.description = description;
        this.numberOfUsers = numberOfUsers;
    }

    public UiGroupDTO(long accountId, Long objectId, long groupId, String name, String description,
                      final UiGroupAntipassbackDTO uiGroupAntipassbackDTO, final boolean keypadUnlockHoldPrivileges, Long numberOfUsers) {
        this.accountId = accountId;
        this.objectId = objectId;
        this.groupId = groupId;
        this.name = name;
        this.description = description;
        this.uiGroupAntipassbackDTO = uiGroupAntipassbackDTO;
        this.keypadUnlockHoldPrivileges = keypadUnlockHoldPrivileges;
        this.numberOfUsers = numberOfUsers;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public Long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UiGroupAntipassbackDTO getUiGroupAntipassbackDTO() {
        return uiGroupAntipassbackDTO;
    }

    public void setUiGroupAntipassbackDTO(UiGroupAntipassbackDTO uiGroupAntipassbackDTO) {
        this.uiGroupAntipassbackDTO = uiGroupAntipassbackDTO;
    }

    public boolean isKeypadUnlockHoldPrivileges() {
        return keypadUnlockHoldPrivileges;
    }

    public void setKeypadUnlockHoldPrivileges(boolean keypadUnlockHoldPrivileges) {
        this.keypadUnlockHoldPrivileges = keypadUnlockHoldPrivileges;
    }

    public Long getNumberOfUsers() {
        return numberOfUsers;
    }

    public void setNumberOfUsers(Long numberOfUsers) {
        this.numberOfUsers = numberOfUsers;
    }

    @Override
    public String toString() {
        return "UiGroupDTO{" +
                "objectId=" + objectId +
                ", groupId=" + groupId +
                ", name='" + name + '\'' +
                ", uiGroupAntipassbackDTO=" + uiGroupAntipassbackDTO +
                ", keypadUnlockHoldPrivileges=" + keypadUnlockHoldPrivileges +
                ", numberOfUsers=" + numberOfUsers +
                '}';
    }
}
