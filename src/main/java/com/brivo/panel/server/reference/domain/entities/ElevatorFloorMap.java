package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "elevator_floor_map", schema = "brivo20", catalog = "onair")
@IdClass(ElevatorFloorMapPK.class)
public class ElevatorFloorMap implements Serializable {
    private long elevatorOid;
    private long floorOid;
    private long panelOid;
    private long boardNumber;
    private short pointAddress;
    private Device floorByObjectId;
    private Device elevatorByObjectId;
    private Board board;
    private Brain brainByObjectId;

    @Id
    @Column(name = "elevator_oid", nullable = false)
    public long getElevatorOid() {
        return elevatorOid;
    }

    public void setElevatorOid(long elevatorOid) {
        this.elevatorOid = elevatorOid;
    }

    @Id
    @Column(name = "floor_oid", nullable = false)
    public long getFloorOid() {
        return floorOid;
    }

    public void setFloorOid(long floorOid) {
        this.floorOid = floorOid;
    }

    @Column(name = "panel_oid", nullable = false)
    public long getPanelOid() {
        return panelOid;
    }

    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }

    @Column(name = "board_number", nullable = false)
    public long getBoardNumber() {
        return boardNumber;
    }

    public void setBoardNumber(long boardNumber) {
        this.boardNumber = boardNumber;
    }

    @Column(name = "point_address", nullable = false)
    public short getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(short pointAddress) {
        this.pointAddress = pointAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ElevatorFloorMap that = (ElevatorFloorMap) o;

        if (elevatorOid != that.elevatorOid) {
            return false;
        }
        if (floorOid != that.floorOid) {
            return false;
        }
        if (panelOid != that.panelOid) {
            return false;
        }
        if (boardNumber != that.boardNumber) {
            return false;
        }
        return pointAddress == that.pointAddress;
    }

    @Override
    public int hashCode() {
        int result = (int) (elevatorOid ^ (elevatorOid >>> 32));
        result = 31 * result + (int) (floorOid ^ (floorOid >>> 32));
        result = 31 * result + (int) (panelOid ^ (panelOid >>> 32));
        result = 31 * result + (int) (boardNumber ^ (boardNumber >>> 32));
        result = 31 * result + (int) pointAddress;
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY,  cascade = CascadeType.ALL, targetEntity = Device.class)
    @JoinColumn(name = "floor_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Device getFloorByObjectId() {
        return floorByObjectId;
    }

    public void setFloorByObjectId(Device floorByObjectId) {
        this.floorByObjectId = floorByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Device.class)
    @JoinColumn(name = "elevator_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Device getElevatorByObjectId() {
        return elevatorByObjectId;
    }

    public void setElevatorByObjectId(Device elevatorByObjectId) {
        this.elevatorByObjectId = elevatorByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Brain.class)
    @JoinColumn(name = "panel_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumns({
            @JoinColumn(name = "panel_oid", referencedColumnName = "panel_oid", nullable = false, insertable = false, updatable = false),
            @JoinColumn(name = "board_number", referencedColumnName = "board_number", nullable = false, insertable = false, updatable = false)
    })
    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

}
