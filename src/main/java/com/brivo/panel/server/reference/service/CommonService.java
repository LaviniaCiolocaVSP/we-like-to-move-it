package com.brivo.panel.server.reference.service;

import org.springframework.stereotype.Service;

import java.util.Optional;

import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;

@Service
public class CommonService {

    protected Long getOptionalLongValue(final Long value) {
        return Optional.ofNullable(value)
                       .orElse(NOT_AVAILABLE_LONG_VALUE);
    }

    protected String getOptionalValue(final String value) {
        return Optional.ofNullable(value)
                       .orElse(NOT_AVAILABLE);
    }
}
