package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "default_panel_prop", schema = "brivo20", catalog = "onair")
public class DefaultPanelProp {
    private long defaultId;
    private long brainTypeId;
    private long networkId;
    private long displayOrder;
    private String id;
    private long propertyTypeId;
    private String value;
    private BrainType brainTypeByBrainTypeId;
    private PropertyType propertyTypeByPropertyTypeId;

    @Id
    @Column(name = "default_id", nullable = false)
    public long getDefaultId() {
        return defaultId;
    }

    public void setDefaultId(long defaultId) {
        this.defaultId = defaultId;
    }

    @Basic
    @Column(name = "brain_type_id", nullable = false)
    public long getBrainTypeId() {
        return brainTypeId;
    }

    public void setBrainTypeId(long brainTypeId) {
        this.brainTypeId = brainTypeId;
    }

    @Basic
    @Column(name = "network_id", nullable = false)
    public long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(long networkId) {
        this.networkId = networkId;
    }

    @Basic
    @Column(name = "display_order", nullable = false)
    public long getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(long displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Basic
    @Column(name = "id", nullable = false, length = 30)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Basic
    @Column(name = "property_type_id", nullable = false)
    public long getPropertyTypeId() {
        return propertyTypeId;
    }

    public void setPropertyTypeId(long propertyTypeId) {
        this.propertyTypeId = propertyTypeId;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 4000)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DefaultPanelProp that = (DefaultPanelProp) o;

        if (defaultId != that.defaultId) {
            return false;
        }
        if (brainTypeId != that.brainTypeId) {
            return false;
        }
        if (networkId != that.networkId) {
            return false;
        }
        if (displayOrder != that.displayOrder) {
            return false;
        }
        if (propertyTypeId != that.propertyTypeId) {
            return false;
        }
        if (id != null ? !id.equals(that.id) : that.id != null) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (defaultId ^ (defaultId >>> 32));
        result = 31 * result + (int) (brainTypeId ^ (brainTypeId >>> 32));
        result = 31 * result + (int) (networkId ^ (networkId >>> 32));
        result = 31 * result + (int) (displayOrder ^ (displayOrder >>> 32));
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (int) (propertyTypeId ^ (propertyTypeId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "brain_type_id", referencedColumnName = "brain_type_id", nullable = false, insertable = false,
            updatable = false)
    public BrainType getBrainTypeByBrainTypeId() {
        return brainTypeByBrainTypeId;
    }

    public void setBrainTypeByBrainTypeId(BrainType brainTypeByBrainTypeId) {
        this.brainTypeByBrainTypeId = brainTypeByBrainTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "property_type_id", referencedColumnName = "property_type_id", nullable = false, insertable = false,
            updatable = false)
    public PropertyType getPropertyTypeByPropertyTypeId() {
        return propertyTypeByPropertyTypeId;
    }

    public void setPropertyTypeByPropertyTypeId(PropertyType propertyTypeByPropertyTypeId) {
        this.propertyTypeByPropertyTypeId = propertyTypeByPropertyTypeId;
    }
}
