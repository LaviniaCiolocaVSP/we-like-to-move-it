package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;

public abstract class Event {
    private EventType eventType;
    private Instant eventTime;

    public Event() {

    }

    @JsonCreator
    public Event(@JsonProperty("eventType") final EventType eventType,
                 @JsonProperty("eventTime") final Instant eventTime) {
        this.eventType = eventType;
        this.eventTime = eventTime;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Instant getEventTime() {
        return eventTime;
    }

    public void setEventTime(Instant eventTime) {
        this.eventTime = eventTime;
    }

    public Long getSecurityActionId() {
        if (eventType == null) {
            // return EventType.NONE.getSecurityActionId();
            return 0L;
        }
        //return eventType.getSecurityActionId();
        return 1L;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }
}
