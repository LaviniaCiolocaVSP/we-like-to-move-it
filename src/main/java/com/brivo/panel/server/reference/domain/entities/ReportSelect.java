package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_select", schema = "brivo20", catalog = "onair")
public class ReportSelect {
    private long reportId;
    private long selectId;
    private Long fieldId;
    private long displayOrder;
    private Collection<Report> reportSelectByReportId;

    @Id
    @Basic
    @Column(name = "report_id", nullable = false)
    public long getReportId() {
        return reportId;
    }

    public void setReportId(long reportId) {
        this.reportId = reportId;
    }

    @Basic
    @Column(name = "select_id", nullable = false)
    public long getSelectId() {
        return selectId;
    }

    public void setSelectId(long selectId) {
        this.selectId = selectId;
    }

    @Basic
    @Column(name = "field_id", nullable = true)
    public Long getFieldId() {
        return fieldId;
    }

    public void setFieldId(Long fieldId) {
        this.fieldId = fieldId;
    }

    @Basic
    @Column(name = "display_order", nullable = false)
    public long getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(long displayOrder) {
        this.displayOrder = displayOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportSelect that = (ReportSelect) o;

        if (reportId != that.reportId) {
            return false;
        }
        if (selectId != that.selectId) {
            return false;
        }
        if (displayOrder != that.displayOrder) {
            return false;
        }
        return fieldId != null ? fieldId.equals(that.fieldId) : that.fieldId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (reportId ^ (reportId >>> 32));
        result = 31 * result + (int) (selectId ^ (selectId >>> 32));
        result = 31 * result + (fieldId != null ? fieldId.hashCode() : 0);
        result = 31 * result + (int) (displayOrder ^ (displayOrder >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "reportSelectByReportId")
    public Collection<Report> getReportSelectByReportId() {
        return reportSelectByReportId;
    }

    public void setReportSelectByReportId(Collection<Report> reportSelectByReportId) {
        this.reportSelectByReportId = reportSelectByReportId;
    }
}
