package com.brivo.panel.server.reference.domain.entities.credential;

import java.io.Serializable;
import java.util.Comparator;


public class CredentialFieldValue
{
    
    private CredentialField credentialField;
    private String value;

    public CredentialFieldValue() {
        
    }
    
    public CredentialFieldValue(CredentialFieldValue toCopy) {
        setValue(toCopy.getValue());
        setCredentialField(new CredentialField(toCopy.getCredentialField()));
    }
    
    public void setCredentialField(CredentialField credentialField)
    {
        this.credentialField = credentialField;
    }
    
  
    public CredentialField getCredentialField()
    {
        return credentialField;
    }
    public void setValue(String value)
    {
        this.value = value;
    }
    
 
    public String getValue()
    {
        return value;
    }


    public static final class ByFieldOrderComparator implements Comparator<CredentialFieldValue>, Serializable
    {
        private static final long serialVersionUID = -7630329155509813828L;

        @Override
        public int compare(CredentialFieldValue o1, CredentialFieldValue o2)
        {
            return o1.credentialField.getOrder() - o2.credentialField.getOrder();
        }           
    }



}
