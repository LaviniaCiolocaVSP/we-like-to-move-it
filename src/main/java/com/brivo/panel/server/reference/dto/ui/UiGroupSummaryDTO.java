package com.brivo.panel.server.reference.dto.ui;

public class UiGroupSummaryDTO {
    private Long groupId;
    private String name;
    private Long groupObjectId;

    public UiGroupSummaryDTO() {
    }

    public UiGroupSummaryDTO(Long groupId, String name, Long groupObjectId) {
        this.groupId = groupId;
        this.name = name;
        this.groupObjectId = groupObjectId;
    }

    public Long getGroupId() {
        return groupId;
    }

    public void setGroupId(Long groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGroupObjectId() {
        return groupObjectId;
    }

    public void setGroupObjectId(Long groupObjectId) {
        this.groupObjectId = groupObjectId;
    }
}
