package com.brivo.panel.server.reference.domain.entities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum BrivoObjectTypes {
    UNKNOWN(0),
    ACCOUNT(1),
    DEVICE(2),
    USER(3),
    SECURITY_GROUP(4),
    CREDENTIAL(5),
    SCHEDULE(6),
    HOLIDAY(7),
    DEVICE_PARAMETER(8),
    NOTIF_RULE(9),
    DVR(11),
    CAMERA(12),
    BADGING(13),
    BADGE(14),
    BRAIN(10),
    REPORT(15),
    PERMISSION_TEMPLATE(16),
    REPORT_CONFIGURATION(21),
    REPORT_SCHEDULE(22),
    CAMERA_GROUP(23),
    APPLICATION(24),
    BOARD(25),
    APPLICATION_KEY(26),
    APPLICATION_ACCOUNT(27),
    VIDEO_PROVIDER(28),
    TES_DIRECTORY(29),
    TES_RESIDENT(30);

    private static final Map<Integer, BrivoObjectTypes> BY_TYPE_ID;

    static {
        final Map<Integer, BrivoObjectTypes> byTypeID = new HashMap<>();
        for (BrivoObjectTypes ot : BrivoObjectTypes.values()) {
            byTypeID.put(ot.objectTypeID, ot);
        }

        BY_TYPE_ID = Collections.unmodifiableMap(byTypeID);
    }

    private int objectTypeID;

    BrivoObjectTypes(int objectTypeID) {
        this.objectTypeID = objectTypeID;
    }

    public static BrivoObjectTypes valueOf(int objectTypeID) {
        final BrivoObjectTypes ot = BY_TYPE_ID.get(objectTypeID);

        if (null == ot) {
            return UNKNOWN;
        }

        return ot;
    }

    public int getObjectTypeID() {
        return objectTypeID;
    }
}
