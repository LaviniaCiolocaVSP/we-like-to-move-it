package com.brivo.panel.server.reference.model.event;


import com.brivo.panel.server.reference.service.processors.event.ActorEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.BoardPointEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.DeviceEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.DeviceMessageEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.EventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.PanelEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.SaltoEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.ScheduleActivationEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.TwoFactorEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.event.UnknownCredentialEventDataProcessor;
import com.brivo.panel.server.reference.service.processors.site.SiteIdsByBoardProcessor;
import com.brivo.panel.server.reference.service.processors.site.SiteIdsByDeviceProcessor;
import com.brivo.panel.server.reference.service.processors.site.SiteIdsByPanelProcessor;
import com.brivo.panel.server.reference.service.processors.site.SiteIdsProcessor;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Classification of events coming from panel. The
 * Event Code is the value that the panel knows. The
 * Security Action Id is the value the OnAir servers
 * know. I'm a little unclear what a zero value for
 * Security action Id means in the old server, but
 * it looks like either the event is discarded or
 * is never expected to be received.
 */
public enum EventType {
    // An invalid event.
    NONE(-1, -1, null, null),

    // Successful credential presentation.
    CREDENTIAL_PRESENTED(0, 2004, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Unknown card.
    UNKNOWN_CARD_CREDENTIAL(1, 5012, SiteIdsByDeviceProcessor.class, UnknownCredentialEventDataProcessor.class),

    // Unknown Pin.
    UNKNOWN_PIN_CREDENTIAL(2, 5012, SiteIdsByDeviceProcessor.class, UnknownCredentialEventDataProcessor.class),

    // Credential not valid for current schedule.
    CREDENTIAL_NOT_IN_SCHEDULE(3, 5016, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Credential not yet valid or expired.
    CREDENTIAL_OUTSIDE_DATE_RANGE(4, 5017, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Credential does not have permission to device.
    CREDENTIAL_NO_PERMISSION(5, 5018, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Event track event for REX event.
    EXIT(6, 0, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Device has unlocked on automatic schedule.
    AUTO_UNLOCK(9, 5002, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Device has locked on automatic schedule.
    AUTO_LOCK(10, 5003, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Door forced open.
    DOOR_FORCED_OPEN(11, 5008, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Door ajar detected.
    DOOR_AJAR_SET(19, 5001, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Door ajar cleared.
    DOOR_AJAR_CLR(20, 5011, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Panel has low disk space.
    LOW_DISK_SPACE_SET(22, 5026, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Panel low disk space cleared.
    LOW_DISK_SPACE_CLR(23, 5032, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Panel has low memory.
    LOW_MEMORY_SET(24, 5027, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Panel low memory cleared.
    LOW_MEMORY_CLR(25, 5033, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Too many invalid pin attempts.
    TOO_MANY_INVALID_PINS(32, 5006, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    // Device status.
    DEVICE_STATUS(36, 0, null, null),

    // Sent by panel at start of main firmware daemon.    
    APP_STARTUP(38, 5035, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Programmable device engaged.
    DEVICE_ENGAGED(39, 5037, SiteIdsByDeviceProcessor.class, DeviceMessageEventDataProcessor.class),

    // Programmable device disengaged.
    DEVICE_DISENGAGED(40, 5038, SiteIdsByDeviceProcessor.class, DeviceMessageEventDataProcessor.class),

    // Supervised input detected a wiring cut.
    WIRING_CUT_SET(41, 5039, SiteIdsByPanelProcessor.class, BoardPointEventDataProcessor.class),

    // Supervised input detected a wiring cut cleared.
    WIRING_CUT_CLR(42, 5040, SiteIdsByPanelProcessor.class, BoardPointEventDataProcessor.class),

    // Communication with board on can bus failed.
    BOARD_COMMS_FAILURE_SET(43, 5041, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Communication failure cleared.
    BOARD_COMMS_FAILURE_CLR(44, 5042, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Board went on battery.
    BOARD_BATTERY_SET(45, 5043, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Board no longer on battery.
    BOARD_BATTERY_CLR(46, 5044, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Tamper activated on board.
    BOARD_TAMPER_SET(47, 5045, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Tamper cleared.
    BOARD_TAMPER_CLR(48, 5046, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Board micro controller was reset.
    BOARD_CHIP_RESET(49, 5047, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Supervised input detected a wiring short.
    WIRING_SHORT_SET(50, 5048, SiteIdsByPanelProcessor.class, BoardPointEventDataProcessor.class),

    // Supervised input detected wiring short cleared.
    WIRING_SHORT_CLR(51, 5049, SiteIdsByPanelProcessor.class, BoardPointEventDataProcessor.class),

    // Manual unlock schedule override set.
    MAN_UNLOCK_SCHED_OVRD_SET(52, 5050, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Override cleared.
    MAN_UNLOCK_SCHED_OVRD_CLR(53, 5051, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Board with address zero on can bus.
    CAN_BUS_UNCONFIGURED_ERROR(54, 5052, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Two boards with same address on can bus.
    CAN_BUS_COLLISION_ERROR(55, 5053, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Text string from panel.
    PANEL_REPORT(56, 5054, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class),

    // Schedule activated by a member of a group or device.
    SCHEDULE_ACTIVATED(57, 5055, SiteIdsByDeviceProcessor.class, ScheduleActivationEventDataProcessor.class),

    // Second credential is unrecognized card.
    INVALID_SECOND_FACTOR_UNKNOWN_CARD(58, 5056, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // Second credential is unrecognized pin.
    INVALID_SECOND_FACTOR_UNKNOWN_PIN(59, 5057, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // Second credential is known but bad card.
    INVALID_SECOND_FACTOR_INVALID_CARD(60, 5058, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // Both credentials cannot be cards.
    INVALID_SECOND_FACTOR_TWO_CARDS(61, 5059, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // Both credentials cannot be pins.
    INVALID_SECOND_FACTOR_TWO_PINS(62, 5060, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // Second credential not given.
    SECOND_FACTOR_NOT_GIVEN(63, 5061, SiteIdsByDeviceProcessor.class, TwoFactorEventDataProcessor.class),

    // 64-126 Not in use.
    PANEL_LOG(64, 0, null, null),

    TEST_EVENT(127, 0, null, null),

    // 128-164 Not in use.

    // Administrator pulsed output from GUI.
    ADMIN_PULSE_OUTPUT(165, 5064, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Administrator locked early from GUI.
    ADMIN_LOCKED_EARLY(166, 0, null, null),

    // Administrator unlocked early from GUI.
    ADMIN_UNLOCKED_EARLY(167, 0, null, null),

    // Administrator restored unlock schedule from GUI.
    ADMIN_FOLLOW_SCHEDULE(168, 0, null, null),

    // Administrator latched output from GUI.
    ADMIN_LATCH_OUTPUT(169, 0, null, null),

    // Administrator unlatched output from GUI.
    ADMIN_UNLATCH_OUTPUT(170, 0, null, null),

    // Administrator invoked lockdown from GUI.
    ADMIN_LOCKDOWN_DEVICE(171, 0, null, null),

    // Administrator cleared lockdown from GUI.
    ADMIN_RELEASE_DEVICE(172, 0, null, null),

    // Administrator not in a group with lockdown access.
    NO_LOCKDOWN_ACCESS(173, 0, null, null),

    // Antipassback failure.
    ANTIPASSBACK_FAILURE(174, 5063, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // Schedule deactivated by administrator action.
    SCHEDULE_DEACTIVATED(180, 0, null, null),

    // Administrator toggled output from GUI.
    ADMIN_TOGGLE_OUTPUT(192, 0, null, null),

    // Card credential required, got pin only.
    INVALID_CREDENTIAL_TYPE(193, 5069, SiteIdsByDeviceProcessor.class, UnknownCredentialEventDataProcessor.class),

    // Indicated device input activated
    DEVICE_INPUT_ACTIVATED(201, 0, null, null),

    // Initial panel connection to Salto router.
    SALTO_HELLO(202, 5202, SiteIdsByBoardProcessor.class, SaltoEventDataProcessor.class),

    // Salto open with key.
    SALTO_METALLIC_KEY(204, 5204, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Salto open with PPD (Portable Programming Device).
    SALTO_OPEN_BY_PPD(205, 5205, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Salto open with code.
    SALTO_OPEN_BY_MEMORIZED_CODE(206, 5206, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Intrusion alarm set.
    SALTO_INTRUSION_ALARM(207, 5207, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Intrusion alarm cleared.
    SALTO_INTRUSION_CLEAR(208, 5208, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Credential rejected due to low battery.
    SALTO_REJECT_LOW_BATTERY(209, 5209, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Low battery.
    DEV_LOW_BATTERY(210, 5210, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Salto router-node communication lost.
    SALTO_SYSTEM_COMMS_FAILURE_SET(211, 5211, SiteIdsByBoardProcessor.class, SaltoEventDataProcessor.class),

    // Salto router-node communication regained.
    SALTO_SYSTEM_COMMS_FAILURE_CLR(212, 5212, SiteIdsByBoardProcessor.class, SaltoEventDataProcessor.class),

    // Cannot reach Salto router.
    SALTO_ROUTER_COMMS_FAILURE(213, 5213, SiteIdsByBoardProcessor.class, SaltoEventDataProcessor.class),

    // Failed credential, Salto router offline.
    FAILED_ACCESS_OFFLINE(214, 5214, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // privacy override.
    PRIVACY_OVERRIDE(230, 5231, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    // privacy mode.
    PRIVACY_MODE(231, 5232, SiteIdsByDeviceProcessor.class, DeviceEventDataProcessor.class),

    //salto privacy override.
    SALTO_PRIVACY_OVERRIDE(230, 5231, SiteIdsByDeviceProcessor.class, ActorEventDataProcessor.class),

    //salto privacy mode.
    SALTO_PRIVACY_MODE(231, 5232, SiteIdsByDeviceProcessor.class, SaltoEventDataProcessor.class),

    // Unauthorized IP access (Bot detection)
    UNAUTHORIZED_IP_ACCESS(240, 5081, SiteIdsByPanelProcessor.class, PanelEventDataProcessor.class);

    private int eventCode;          // Panel Event Code.
    private long securityActionId;   // OnAir Security Action Id.
    private Class<? extends SiteIdsProcessor> siteIdsProcessor;
    private Class<? extends EventDataProcessor> eventDataProcessor;

    EventType(int code,
              long actionId,
              Class<? extends SiteIdsProcessor> siteIdsProcessor,
              Class<? extends EventDataProcessor> eventDataProcessor) {
        this.eventCode = code;
        this.securityActionId = actionId;
        this.siteIdsProcessor = siteIdsProcessor;
        this.eventDataProcessor = eventDataProcessor;
    }

    /**
     * Return the
     * enumeration value
     * that corresponds
     * to the
     * input
     * <p>
     * parameter.Case is
     * not important.
     * If the
     * parameter is
     * not
     * <p>
     * found or null,
     * NONE is
     * returned .
     *
     * @param value
     * @return
     */
    @JsonCreator
    public static EventType getEventType(String value) {
        if (value == null) {
            return NONE;
        }

        for (EventType type : values()) {
            if (type.name().equalsIgnoreCase(value)) {
                return type;
            }
        }

        return NONE;
    }

    /**
     * Return the
     * Panel Event
     * Code .
     */
    public int getEventCode() {
        return this.eventCode;
    }

    /**
     * Return the
     * OnAir Security
     * Action Id.
     */
    public long getSecurityActionId() {
        return this.securityActionId;
    }

    public Class<? extends SiteIdsProcessor> getSiteIdsProcessor() {
        return siteIdsProcessor;
    }

    public Class<? extends EventDataProcessor> getEventDataProcessor() {
        return eventDataProcessor;
    }

    @JsonValue
    public String getEventTypeName() {
        return name();
    }
}
