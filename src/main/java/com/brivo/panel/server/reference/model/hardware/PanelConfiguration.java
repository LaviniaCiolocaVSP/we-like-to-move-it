package com.brivo.panel.server.reference.model.hardware;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.List;

public class PanelConfiguration {
    private Boolean enableCommandChannel;
    private Integer panelLogPeriod;
    private Integer pingPort;
    private Integer nonce;
    private Integer flushInterval;
    private Boolean shouldPollForData;
    private Boolean shouldCacheEvents;
    private String timeZone;
    private String timeZoneData;
    private String firmwareVersion;
    private Boolean enableFipsMode;
    private Boolean disableServerAuthCheckFirst;
    private Instant lastUpdate;
    private Integer firmwareProtocolNumber;
    private List<Rs485Settings> rs485Settings;


    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Boolean getEnableCommandChannel() {
        return enableCommandChannel;
    }

    public void setEnableCommandChannel(Boolean enableCommandChannel) {
        this.enableCommandChannel = enableCommandChannel;
    }

    public Integer getPingPort() {
        return pingPort;
    }

    public void setPingPort(Integer pingPort) {
        this.pingPort = pingPort;
    }

    public Integer getNonce() {
        return nonce;
    }

    public void setNonce(Integer nonce) {
        this.nonce = nonce;
    }

    public Integer getFlushInterval() {
        return flushInterval;
    }

    public void setFlushInterval(Integer flushInterval) {
        this.flushInterval = flushInterval;
    }

    public Boolean getShouldPollForData() {
        return shouldPollForData;
    }

    public void setShouldPollForData(Boolean shouldPollForData) {
        this.shouldPollForData = shouldPollForData;
    }

    public Boolean getShouldCacheEvents() {
        return shouldCacheEvents;
    }

    public void setShouldCacheEvents(Boolean shouldCacheEvents) {
        this.shouldCacheEvents = shouldCacheEvents;
    }

    @JsonIgnore
    public String getTimeZone() {
        return timeZone;
    }

    @JsonIgnore
    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    public Boolean getEnableFipsMode() {
        return enableFipsMode;
    }

    public void setEnableFipsMode(Boolean enableFipsMode) {
        this.enableFipsMode = enableFipsMode;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Integer getFirmwareProtocolNumber() {
        return firmwareProtocolNumber;
    }

    public void setFirmwareProtocolNumber(Integer firmwareProtocolNumber) {
        this.firmwareProtocolNumber = firmwareProtocolNumber;
    }

    public Integer getPanelLogPeriod() {
        return panelLogPeriod;
    }

    public void setPanelLogPeriod(Integer panelLogPeriod) {
        this.panelLogPeriod = panelLogPeriod;
    }

    public String getTimeZoneData() {
        return timeZoneData;
    }

    public void setTimeZoneData(String timeZoneData) {
        this.timeZoneData = timeZoneData;
    }

    public List<Rs485Settings> getRs485Settings() {
        return rs485Settings;
    }

    public void setRs485Settings(List<Rs485Settings> rs485Settings) {
        this.rs485Settings = rs485Settings;
    }

    public Boolean getDisableServerAuthCheckFirst() {
        return disableServerAuthCheckFirst;
    }

    public void setDisableServerAuthCheckFirst(Boolean disableServerAuthCheckFirst) {
        this.disableServerAuthCheckFirst = disableServerAuthCheckFirst;
    }
}
