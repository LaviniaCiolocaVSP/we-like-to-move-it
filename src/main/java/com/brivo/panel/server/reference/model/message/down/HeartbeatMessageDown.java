package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class HeartbeatMessageDown extends DownstreamMessage {
    public HeartbeatMessageDown() {
        super(DownstreamMessageType.Heartbeat);
    }

}
