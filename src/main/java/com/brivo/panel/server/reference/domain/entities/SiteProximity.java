package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "site_proximity", schema = "brivo20", catalog = "onair")
public class SiteProximity {
    private long siteObjectId;
    private long proximityId;
    private Timestamp updated;
    private BrivoObject objectBySiteBrivoObjectId;
    private Proximity proximityByProximityId;

    @Id
    @Basic
    @Column(name = "site_object_id", nullable = false)
    public long getSiteObjectId() {
        return siteObjectId;
    }

    public void setSiteObjectId(long siteObjectId) {
        this.siteObjectId = siteObjectId;
    }

    @Basic
    @Column(name = "proximity_id", nullable = false)
    public long getProximityId() {
        return proximityId;
    }

    public void setProximityId(long proximityId) {
        this.proximityId = proximityId;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SiteProximity that = (SiteProximity) o;

        if (siteObjectId != that.siteObjectId) {
            return false;
        }
        if (proximityId != that.proximityId) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (siteObjectId ^ (siteObjectId >>> 32));
        result = 31 * result + (int) (proximityId ^ (proximityId >>> 32));
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "site_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectBySiteBrivoObjectId() {
        return objectBySiteBrivoObjectId;
    }

    public void setObjectBySiteBrivoObjectId(BrivoObject objectBySiteBrivoObjectId) {
        this.objectBySiteBrivoObjectId = objectBySiteBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "proximity_id", referencedColumnName = "proximity_id", nullable = false, insertable = false, updatable = false)
    public Proximity getProximityByProximityId() {
        return proximityByProximityId;
    }

    public void setProximityByProximityId(Proximity proximityByProximityId) {
        this.proximityByProximityId = proximityByProximityId;
    }
}
