package com.brivo.panel.server.reference.service.processors.event;

import com.brivo.panel.server.reference.dao.event.EventResolutionDao;
import com.brivo.panel.server.reference.dao.event.PostEventResolutionDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ActorEventDataProcessor implements EventDataProcessor {
    Logger logger = LoggerFactory.getLogger(EventDataProcessor.class);

    @Autowired
    private PostEventResolutionDao postEventResolutionDao;

    @Autowired
    private EventResolutionDao eventResolutionDao;

    /*
    @Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> ResolveActorEvent");

        Boolean result = false;

        if(EventType.ADMIN_PULSE_OUTPUT == eventType)
        {
            convertMobilePass(event);
        }

        List<EventResolutionData> dataList = eventResolutionDao.getEntranceEvent(event);

        if(dataList.size() == 1)
        {
            event.setAccountId(dataList.get(0).getAccountId());
            event.getEventData().setActorName(dataList.get(0).getFullName());
            event.getEventData().setObjectTypeId(dataList.get(0).getObjectTypeId());
            event.getEventData().setObjectName(dataList.get(0).getDeviceName());
            event.setObjectGroupObjectId(dataList.get(0).getSiteOid());
            event.getEventData().setObjectGroupName(dataList.get(0).getSecurityGroupName());
            event.getEventData().setDeviceTypeId(dataList.get(0).getDeviceTypeId());
            event.getEventData().setActionAllowed(false);

            result = true;
        }
        logger.trace("<<< ResolveActorEvent");

        return result;
    }

    *//**
     * Convert for Mobile Pass. If the input event
     * was ADMIN_PULSE_OUTPUT and was a mobile pass
     * convert it to CREDENTIAL_PRESENTED.
     *
     * @param event
     * @return
     *//*
    void convertMobilePass(SecurityLogEvent event)
    {
        Long mobilePassId = eventResolutionDao.getMobileCredentialId(event.getActorObjectId());

        if(mobilePassId != null)
        {
            event.setSecurityActionId(EventType.CREDENTIAL_PRESENTED.getSecurityActionId());
            event.getEventData().setCredentialObjectId(mobilePassId);
        }
    }

    @Override
    public void doAfterResolve(EventType eventType, SecurityLogEvent event, Panel panel)
    {
        logger.trace(">>> HandlePostResolutionEvent");

        if(EventType.CREDENTIAL_PRESENTED.equals(eventType))
        {
            handleSiteOccupant(event);
        }

        logger.trace("<<< HandlePostResolutionEvent");
    }

    *//**
     * Update the site_occupant table.
     *
     * @see PresenceManager
     *
     * @param event
     *//*
    void handleSiteOccupant(SecurityLogEvent event)
    {
        logger.trace(">>> HandleSiteOccupant");

        if(!postEventResolutionDao.isValidForSiteOccupant(event.getObjectId()))
        {
            return;
        }

        SiteOccupant siteOccupant = postEventResolutionDao.getSiteOccupantDoorInfo(event.getObjectId());
        if(siteOccupant == null)
        {
            logger.warn("Invalid site occupant door information for event " + event);
            return;
        }

        if( (siteOccupant.getIsIngress() && siteOccupant.getIsEgress() )
                || (!siteOccupant.getIsIngress() && !siteOccupant.getIsEgress()) )
        {
            logger.warn(String.format("Unable to handle site occupant for event. Door must be marked as either "
                                      + "ingress or egress, but no both. SiteOccupant=[%s]  Event=[%s]",
                                      siteOccupant,
                                      event));
            return;
        }

        siteOccupant.setUserId(event.getActorObjectId());

        if(siteOccupant.getIsIngress())
        {
            siteOccupant.setEntryDoorId(siteOccupant.getDeviceId());
            siteOccupant.setEntered(event.getOccurred().toInstant());
            siteOccupant.setExitDoorId(null);
            siteOccupant.setExited(null);

            postEventResolutionDao.upsertSiteOccupantForIngress(siteOccupant);
        }
        else if(siteOccupant.getIsEgress())
        {
            siteOccupant.setExitDoorId(siteOccupant.getDeviceId());
            siteOccupant.setExited(event.getOccurred().toInstant());

            postEventResolutionDao.updateSiteOccupantForEgress(siteOccupant);
        }

        logger.trace("<<< HandleSiteOccupant");
    }*/

}
