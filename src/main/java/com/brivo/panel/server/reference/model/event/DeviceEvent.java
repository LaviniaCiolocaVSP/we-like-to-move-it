package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class DeviceEvent extends Event {
    private Long deviceId;

    @JsonCreator
    public DeviceEvent(@JsonProperty("eventType") final EventType eventType,
                       @JsonProperty("eventTime") final Instant eventTime,
                       @JsonProperty("deviceId") final Long deviceId) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

}
