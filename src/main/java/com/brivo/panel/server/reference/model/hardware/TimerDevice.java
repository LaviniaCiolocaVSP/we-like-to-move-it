package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

public class TimerDevice {
    private Long deviceId;
    private Long inputScheduleId;
    private Boolean reportEngage;
    private Boolean reportDisengage;
    private List<ProgrammableDeviceOutput> outputs;
    private Boolean reportLiveStatus;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getInputScheduleId() {
        return inputScheduleId;
    }

    public void setInputScheduleId(Long inputScheduleId) {
        this.inputScheduleId = inputScheduleId;
    }

    public List<ProgrammableDeviceOutput> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<ProgrammableDeviceOutput> outputs) {
        this.outputs = outputs;
    }

    public Boolean getReportLiveStatus() {
        return reportLiveStatus;
    }

    public void setReportLiveStatus(Boolean reportLiveStatus) {
        this.reportLiveStatus = reportLiveStatus;
    }

    public Boolean getReportEngage() {
        return reportEngage;
    }

    public void setReportEngage(Boolean reportEngage) {
        this.reportEngage = reportEngage;
    }

    public Boolean getReportDisengage() {
        return reportDisengage;
    }

    public void setReportDisengage(Boolean reportDisengage) {
        this.reportDisengage = reportDisengage;
    }
}
