package com.brivo.panel.server.reference.model.hardware;

public class PanelProperty {
    private PanelPropertyType panelPropertyType;
    private String value;


    public Integer getValueAsInteger() {
        if (value == null) {
            return null;
        }

        return Integer.valueOf(this.value);
    }

    public Boolean getValueAsBoolean() {
        if (value == null) {
            return null;
        }

        return Boolean.valueOf(this.value);
    }

    public PanelPropertyType getPanelPropertyType() {
        return panelPropertyType;
    }

    public void setPanelPropertyType(PanelPropertyType panelPropertyType) {
        this.panelPropertyType = panelPropertyType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
