package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_job_status", schema = "brivo20", catalog = "onair")
public class ReportJobStatus {
    private long id;
    private String jobStatus;
    private Collection<ReportJob> reportJobsById;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "job_status", nullable = true, length = 50)
    public String getJobStatus() {
        return jobStatus;
    }

    public void setJobStatus(String jobStatus) {
        this.jobStatus = jobStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportJobStatus that = (ReportJobStatus) o;

        if (id != that.id) {
            return false;
        }
        return jobStatus != null ? jobStatus.equals(that.jobStatus) : that.jobStatus == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (jobStatus != null ? jobStatus.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportJobStatusByJobStatusId")
    public Collection<ReportJob> getReportJobsById() {
        return reportJobsById;
    }

    public void setReportJobsById(Collection<ReportJob> reportJobsById) {
        this.reportJobsById = reportJobsById;
    }
}
