package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "oauth_service_token", schema = "brivo20", catalog = "onair")
public class OauthServiceToken {
    private long ownerObjectId;
    private long accountId;
    private String serviceToken;
    private Timestamp updated;
    private Collection<Users> oauthServiceTokenByObjectId;

    @Id
    @Column(name = "owner_object_id", nullable = false)
    public long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "service_token", nullable = false, length = 40)
    public String getServiceToken() {
        return serviceToken;
    }

    public void setServiceToken(String serviceToken) {
        this.serviceToken = serviceToken;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OauthServiceToken that = (OauthServiceToken) o;

        if (ownerObjectId != that.ownerObjectId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (serviceToken != null ? !serviceToken.equals(that.serviceToken) : that.serviceToken != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (ownerObjectId ^ (ownerObjectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (serviceToken != null ? serviceToken.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "oauthServiceTokenByObjectId")
    public Collection<Users> getOauthServiceTokenByObjectId() {
        return oauthServiceTokenByObjectId;
    }

    public void setOauthServiceTokenByObjectId(Collection<Users> oauthServiceTokenByObjectId) {
        this.oauthServiceTokenByObjectId = oauthServiceTokenByObjectId;
    }
}
