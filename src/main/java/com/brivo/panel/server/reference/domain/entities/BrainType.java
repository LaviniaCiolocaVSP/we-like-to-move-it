package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "brain_type", schema = "brivo20", catalog = "onair")
public class BrainType {
    private long brainTypeId;
    private String name;
    private String description;
    private Long firmwareProtocol;
    private Long rs485Ports;
    private Collection<Brain> brainsByBrainTypeId;
    private Collection<DefaultPanelProp> defaultPanelPropsByBrainTypeId;
    private FirmwareUpgradeConfig firmwareUpgradeConfigByBrainTypeId;

    @Id
    @Column(name = "brain_type_id", nullable = false)
    public long getBrainTypeId() {
        return brainTypeId;
    }

    public void setBrainTypeId(long brainTypeId) {
        this.brainTypeId = brainTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "firmware_protocol", nullable = true)
    public Long getFirmwareProtocol() {
        return firmwareProtocol;
    }

    public void setFirmwareProtocol(Long firmwareProtocol) {
        this.firmwareProtocol = firmwareProtocol;
    }

    @Basic
    @Column(name = "rs485_ports", nullable = true)
    public Long getRs485Ports() {
        return rs485Ports;
    }

    public void setRs485Ports(Long rs485Ports) {
        this.rs485Ports = rs485Ports;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BrainType brainType = (BrainType) o;

        if (brainTypeId != brainType.brainTypeId) {
            return false;
        }
        if (name != null ? !name.equals(brainType.name) : brainType.name != null) {
            return false;
        }
        if (description != null ? !description.equals(brainType.description) : brainType.description != null) {
            return false;
        }
        if (firmwareProtocol != null ? !firmwareProtocol.equals(brainType.firmwareProtocol) : brainType.firmwareProtocol != null) {
            return false;
        }
        return rs485Ports != null ? rs485Ports.equals(brainType.rs485Ports) : brainType.rs485Ports == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (brainTypeId ^ (brainTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (firmwareProtocol != null ? firmwareProtocol.hashCode() : 0);
        result = 31 * result + (rs485Ports != null ? rs485Ports.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "brainTypeByBrainTypeId")
    public Collection<Brain> getBrainsByBrainTypeId() {
        return brainsByBrainTypeId;
    }

    public void setBrainsByBrainTypeId(Collection<Brain> brainsByBrainTypeId) {
        this.brainsByBrainTypeId = brainsByBrainTypeId;
    }

    @OneToMany(mappedBy = "brainTypeByBrainTypeId")
    public Collection<DefaultPanelProp> getDefaultPanelPropsByBrainTypeId() {
        return defaultPanelPropsByBrainTypeId;
    }

    public void setDefaultPanelPropsByBrainTypeId(Collection<DefaultPanelProp> defaultPanelPropsByBrainTypeId) {
        this.defaultPanelPropsByBrainTypeId = defaultPanelPropsByBrainTypeId;
    }

    @OneToOne(mappedBy = "brainTypeByBrainTypeId")
    public FirmwareUpgradeConfig getFirmwareUpgradeConfigByBrainTypeId() {
        return firmwareUpgradeConfigByBrainTypeId;
    }

    public void setFirmwareUpgradeConfigByBrainTypeId(FirmwareUpgradeConfig firmwareUpgradeConfigByBrainTypeId) {
        this.firmwareUpgradeConfigByBrainTypeId = firmwareUpgradeConfigByBrainTypeId;
    }
}
