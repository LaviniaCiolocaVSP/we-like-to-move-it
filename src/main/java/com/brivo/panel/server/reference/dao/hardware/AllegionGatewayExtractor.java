package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.AllegionGateway;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Allegion Gateway Extractor
 */
@Component
public class AllegionGatewayExtractor implements ResultSetExtractor<List<AllegionGateway>> {
    @Override
    public List<AllegionGateway> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<AllegionGateway> list = new ArrayList<>();

        while (rs.next()) {
            AllegionGateway gateway = new AllegionGateway();
            gateway.setDeviceId(rs.getLong("boardObjectId"));
            gateway.setGatewayId(rs.getInt("gatewayId"));
            gateway.setBoardNum(rs.getInt("boardNum"));
            gateway.setRs485NodeId(0);
            list.add(gateway);
        }

        return list;
    }
}
