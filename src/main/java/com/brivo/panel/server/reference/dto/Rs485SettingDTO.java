package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Rs485SettingDTO {
    private final Long port;
    private final Long operationMode;
    private final Long baudRate;
    private final Long errorDetectionMethod;

    @JsonCreator
    public Rs485SettingDTO(@JsonProperty("port") final Long port,
                           @JsonProperty("operationMode") final Long operationMode,
                           @JsonProperty("baudRate") final Long baudRate,
                           @JsonProperty("errorDetectionMethod") final Long errorDetectionMethod) {
        this.port = port;
        this.operationMode = operationMode;
        this.baudRate = baudRate;
        this.errorDetectionMethod = errorDetectionMethod;
    }

    public Long getPort() {
        return port;
    }

    public Long getOperationMode() {
        return operationMode;
    }

    public Long getBaudRate() {
        return baudRate;
    }

    public Long getErrorDetectionMethod() {
        return errorDetectionMethod;
    }

    public enum OperationMode
    {
        OSDP(0),
        ALLEGION(1);

        private final int val;

        OperationMode(int val)
        {
            this.val = val;
        }

        public int getVal()
        {
            return val;
        }

        public static OperationMode get(int operationModeNum)
        {
            for (OperationMode mode : values())
            {
                if (mode.val == operationModeNum) {
                    return mode;
                }
            }
            return OSDP;
        }
    }
}
