package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.sql.Timestamp;

@Entity
public class Firmware {
    private long firmwareId;
    private String name;
    private String description;
    private Timestamp created;
    private Timestamp updated;
    private Long version;

    @Id
    @Column(name = "firmware_id", nullable = false)
    public long getFirmwareId() {
        return firmwareId;
    }

    public void setFirmwareId(long firmwareId) {
        this.firmwareId = firmwareId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "version", nullable = true)
    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Firmware firmware = (Firmware) o;

        if (firmwareId != firmware.firmwareId) {
            return false;
        }
        if (name != null ? !name.equals(firmware.name) : firmware.name != null) {
            return false;
        }
        if (description != null ? !description.equals(firmware.description) : firmware.description != null) {
            return false;
        }
        if (created != null ? !created.equals(firmware.created) : firmware.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(firmware.updated) : firmware.updated != null) {
            return false;
        }
        return version != null ? version.equals(firmware.version) : firmware.version == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (firmwareId ^ (firmwareId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        return result;
    }
}
