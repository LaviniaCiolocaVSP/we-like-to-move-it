package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrivoObjectRepository extends CrudRepository<BrivoObject, Long> {

    @Query(
            value = "SELECT coalesce(max(o.object_id),0) FROM brivo20.object o ",
            nativeQuery = true
    )
    Optional<Long> getMaxObjectId();

    Optional<BrivoObject> findByObjectId(@Param("objectId") long objectId);
}
