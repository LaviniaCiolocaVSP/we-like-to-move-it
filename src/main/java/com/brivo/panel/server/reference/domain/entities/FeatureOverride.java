package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "feature_override", schema = "brivo20", catalog = "onair")
public class FeatureOverride {
    private long featureOverrideId;
    private long featureId;
    private Long accountId;
    private Long dealerAccountId;
    private Short enabled;
    private Feature featureByFeatureId;
    private Account accountByAccountId;
    private Account accountByDealerAccountId;

    @Id
    @Column(name = "feature_override_id", nullable = false)
    public long getFeatureOverrideId() {
        return featureOverrideId;
    }

    public void setFeatureOverrideId(long featureOverrideId) {
        this.featureOverrideId = featureOverrideId;
    }

    @Basic
    @Column(name = "feature_id", nullable = false)
    public long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(long featureId) {
        this.featureId = featureId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Basic
    @Column(name = "enabled", nullable = true)
    public Short getEnabled() {
        return enabled;
    }

    public void setEnabled(Short enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FeatureOverride that = (FeatureOverride) o;

        if (featureOverrideId != that.featureOverrideId) {
            return false;
        }
        if (featureId != that.featureId) {
            return false;
        }
        if (accountId != null ? !accountId.equals(that.accountId) : that.accountId != null) {
            return false;
        }
        if (dealerAccountId != null ? !dealerAccountId.equals(that.dealerAccountId) : that.dealerAccountId != null) {
            return false;
        }
        return enabled != null ? enabled.equals(that.enabled) : that.enabled == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (featureOverrideId ^ (featureOverrideId >>> 32));
        result = 31 * result + (int) (featureId ^ (featureId >>> 32));
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (dealerAccountId != null ? dealerAccountId.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "feature_id", referencedColumnName = "feature_id", nullable = false, insertable = false, updatable = false)
    public Feature getFeatureByFeatureId() {
        return featureByFeatureId;
    }

    public void setFeatureByFeatureId(Feature featureByFeatureId) {
        this.featureByFeatureId = featureByFeatureId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @ManyToOne
    @JoinColumn(name = "dealer_account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByDealerAccountId() {
        return accountByDealerAccountId;
    }

    public void setAccountByDealerAccountId(Account accountByDealerAccountId) {
        this.accountByDealerAccountId = accountByDealerAccountId;
    }
}
