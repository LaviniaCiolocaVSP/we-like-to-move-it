package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;

@Entity
@Table(name = "security_group", schema = "brivo20", catalog = "onair")
public class SecurityGroup implements Serializable {
    private long securityGroupId;
    private long parentId;
    private long objectId;
    private long accountId;
    private String name;
    private String description;
    private short disabled;
    private LocalDateTime created;
    private LocalDateTime updated;
    private short lockdown;
    private short deleted;
    private Long securityGroupTypeId;
    private SecurityGroupAntipassback securityGroupAntipassbackBySecurityGroupId;
    private Collection<OvrCamera> ovrCamerasBySecurityGroupId;
    private BrivoObject objectByObjectId;
    private SecurityGroupType securityGroupTypeBySecurityGroupTypeId;
    private Account accountByAccountId;
    private Collection<SecurityGroupMember> securityGroupMembersBySecurityGroupId;
    private Collection<VideoCamera> videoCamerasBySecurityGroupId;
    private Collection<VideoProvider> videoProvidersBySecurityGroupId;

    @Id
    @Column(name = "security_group_id", nullable = false)
    public long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Basic
    @Column(name = "parent_id", nullable = false)
    public long getParentId() {
        return parentId;
    }

    public void setParentId(long parentId) {
        this.parentId = parentId;
    }

    @Basic
    @Column(name = "security_group_type_id", nullable = false, insertable = false, updatable = false)
    public Long getSecurityGroupTypeId() {
        return securityGroupTypeId;
    }

    public void setSecurityGroupTypeId(Long securityGroupTypeId) {
        this.securityGroupTypeId = securityGroupTypeId;
    }

    @Basic
    @Column(name = "object_id", nullable = false, insertable = false, updatable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Column(name = "account_id", nullable = true, insertable = false, updatable = false)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "name", nullable = true, length = 35)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Column(name = "disabled", nullable = false)
    public short getDisabled() {
        return disabled;
    }

    public void setDisabled(short disabled) {
        this.disabled = disabled;
    }

    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Column(name = "lockdown", nullable = false)
    public short getLockdown() {
        return lockdown;
    }

    public void setLockdown(short lockdown) {
        this.lockdown = lockdown;
    }

    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityGroup that = (SecurityGroup) o;

        if (securityGroupId != that.securityGroupId) {
            return false;
        }
        if (parentId != that.parentId) {
            return false;
        }
        if (disabled != that.disabled) {
            return false;
        }
        if (lockdown != that.lockdown) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (description != null ? !description.equals(that.description) : that.description != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityGroupId ^ (securityGroupId >>> 32));
        result = 31 * result + (int) (parentId ^ (parentId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (int) disabled;
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) lockdown;
        result = 31 * result + (int) deleted;
        return result;
    }

    @OneToOne(mappedBy = "securityGroupBySecurityGroupId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public SecurityGroupAntipassback getSecurityGroupAntipassbackBySecurityGroupId() {
        return securityGroupAntipassbackBySecurityGroupId;
    }

    public void setSecurityGroupAntipassbackBySecurityGroupId(SecurityGroupAntipassback securityGroupAntipassbackBySecurityGroupId) {
        this.securityGroupAntipassbackBySecurityGroupId = securityGroupAntipassbackBySecurityGroupId;
    }

    @OneToMany(mappedBy = "securityGroupBySiteId", fetch = FetchType.LAZY)
    public Collection<OvrCamera> getOvrCamerasBySecurityGroupId() {
        return ovrCamerasBySecurityGroupId;
    }

    public void setOvrCamerasBySecurityGroupId(Collection<OvrCamera> ovrCamerasBySecurityGroupId) {
        this.ovrCamerasBySecurityGroupId = ovrCamerasBySecurityGroupId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByObjectId() {
        return objectByObjectId;
    }

    public void setObjectByObjectId(BrivoObject objectByObjectId) {
        this.objectByObjectId = objectByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "security_group_type_id", referencedColumnName = "security_group_type_id", nullable = false)
    public SecurityGroupType getSecurityGroupTypeBySecurityGroupTypeId() {
        return securityGroupTypeBySecurityGroupTypeId;
    }

    public void setSecurityGroupTypeBySecurityGroupTypeId(SecurityGroupType securityGroupTypeBySecurityGroupTypeId) {
        this.securityGroupTypeBySecurityGroupTypeId = securityGroupTypeBySecurityGroupTypeId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToMany(mappedBy = "securityGroupBySecurityGroupId")
    public Collection<SecurityGroupMember> getSecurityGroupMembersBySecurityGroupId() {
        return securityGroupMembersBySecurityGroupId;
    }

    public void setSecurityGroupMembersBySecurityGroupId(Collection<SecurityGroupMember> securityGroupMembersBySecurityGroupId) {
        this.securityGroupMembersBySecurityGroupId = securityGroupMembersBySecurityGroupId;
    }

    @OneToMany(mappedBy = "securityGroupBySecurityGroupId")
    public Collection<VideoCamera> getVideoCamerasBySecurityGroupId() {
        return videoCamerasBySecurityGroupId;
    }

    public void setVideoCamerasBySecurityGroupId(Collection<VideoCamera> videoCamerasBySecurityGroupId) {
        this.videoCamerasBySecurityGroupId = videoCamerasBySecurityGroupId;
    }

    @OneToMany(mappedBy = "securityGroupBySecurityGroupId")
    public Collection<VideoProvider> getVideoProvidersBySecurityGroupId() {
        return videoProvidersBySecurityGroupId;
    }

    public void setVideoProvidersBySecurityGroupId(Collection<VideoProvider> videoProvidersBySecurityGroupId) {
        this.videoProvidersBySecurityGroupId = videoProvidersBySecurityGroupId;
    }

    @Override
    public String toString() {
        return "SecurityGroup{" +
                "securityGroupId=" + securityGroupId +
                ", objectId=" + objectId +
                ", name='" + name + '\'' +
                ", disabled=" + disabled +
                ", deleted=" + deleted +
                ", securityGroupTypeId=" + securityGroupTypeId +
                ", securityGroupAntipassbackBySecurityGroupId=" + securityGroupAntipassbackBySecurityGroupId +
                '}';
    }
}
