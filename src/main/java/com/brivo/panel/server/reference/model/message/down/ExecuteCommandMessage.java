package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.ExecuteCommandType;
import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class ExecuteCommandMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long administratorId;
    private ExecuteCommandType command;
    private Long argument;

    public ExecuteCommandMessage() {
        super(DownstreamMessageType.ExecuteCommandMessage);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getAdministratorId() {
        return administratorId;
    }

    public void setAdministratorId(Long administratorId) {
        this.administratorId = administratorId;
    }

    public ExecuteCommandType getCommand() {
        return command;
    }

    public void setCommand(ExecuteCommandType command) {
        this.command = command;
    }

    public Long getArgument() {
        return argument;
    }

    public void setArgument(Long argument) {
        this.argument = argument;
    }
}
