package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Collection;
import java.util.Objects;

public class SiteDTO extends AbstractDTO {
    private final long id;
    private final long objectId;
    private final String siteName;
    private final Collection<AddressDTO> addresses;
    private final Short lockdown;
    private final Short deleted;
    private final Short disabled;

    @JsonCreator
    public SiteDTO(@JsonProperty("id") final long id, @JsonProperty("objectId") final long objectId,
                   @JsonProperty("siteName") final String siteName, @JsonProperty("addresses") final Collection<AddressDTO> addresses,
                   @JsonProperty("lockdown") final Short lockdown, @JsonProperty("deleted") final Short deleted,
                   @JsonProperty("disabled") final Short disabled) {
        this.id = id;
        this.siteName = siteName;
        this.addresses = addresses;
        this.objectId = objectId;
        this.lockdown = lockdown;
        this.deleted = deleted;
        this.disabled = disabled;
    }

    @Override
    public Long getId() {
        return id;
    }

    public long getObjectId() {
        return objectId;
    }

    public String getSiteName() {
        return siteName;
    }

    public Collection<AddressDTO> getAddresses() {
        return addresses;
    }

    public Short getLockdown() {
        return lockdown;
    }

    public Short getDeleted() {
        return deleted;
    }

    public Short getDisabled() {
        return disabled;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SiteDTO siteDTO = (SiteDTO) o;
        return id == siteDTO.id &&
                objectId == siteDTO.objectId &&
                Objects.equals(siteName, siteDTO.siteName) &&
                Objects.equals(addresses, siteDTO.addresses) &&
                Objects.equals(lockdown, siteDTO.lockdown) &&
                Objects.equals(deleted, siteDTO.deleted);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, objectId, siteName, addresses, lockdown, deleted);
    }
}
