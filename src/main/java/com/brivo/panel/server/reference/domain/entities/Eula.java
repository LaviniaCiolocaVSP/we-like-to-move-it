package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
public class Eula {
    private long eulaId;
    private String name;
    private String version;
    private Timestamp created;
    private Timestamp updated;
    private String description;
    private String url;
    private Long dealerAccountId;
    private Collection<UserAcceptedEula> userAcceptedEulasByEulaId;

    @Id
    @Column(name = "eula_id", nullable = false)
    public long getEulaId() {
        return eulaId;
    }

    public void setEulaId(long eulaId) {
        this.eulaId = eulaId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "version", nullable = false, length = 32)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "url", nullable = false, length = 256)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "dealer_account_id", nullable = true)
    public Long getDealerAccountId() {
        return dealerAccountId;
    }

    public void setDealerAccountId(Long dealerAccountId) {
        this.dealerAccountId = dealerAccountId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Eula eula = (Eula) o;

        if (eulaId != eula.eulaId) {
            return false;
        }
        if (name != null ? !name.equals(eula.name) : eula.name != null) {
            return false;
        }
        if (version != null ? !version.equals(eula.version) : eula.version != null) {
            return false;
        }
        if (created != null ? !created.equals(eula.created) : eula.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(eula.updated) : eula.updated != null) {
            return false;
        }
        if (description != null ? !description.equals(eula.description) : eula.description != null) {
            return false;
        }
        if (url != null ? !url.equals(eula.url) : eula.url != null) {
            return false;
        }
        return dealerAccountId != null ? dealerAccountId.equals(eula.dealerAccountId) : eula.dealerAccountId == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (eulaId ^ (eulaId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (dealerAccountId != null ? dealerAccountId.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "eulaByEulaId")
    public Collection<UserAcceptedEula> getUserAcceptedEulasByEulaId() {
        return userAcceptedEulasByEulaId;
    }

    public void setUserAcceptedEulasByEulaId(Collection<UserAcceptedEula> userAcceptedEulasByEulaId) {
        this.userAcceptedEulasByEulaId = userAcceptedEulasByEulaId;
    }
}
