package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class SetDeviceThreatLevelMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long deviceId;
    private Long threatLevel;

    public SetDeviceThreatLevelMessage() {
        super(DownstreamMessageType.SetDeviceThreatLevelMessage);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getThreatLevel() {
        return threatLevel;
    }

    public void setThreatLevel(Long threatLevel) {
        this.threatLevel = threatLevel;
    }
}
