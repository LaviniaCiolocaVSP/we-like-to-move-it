package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "events", schema = "brivo20", catalog = "onair")
public class Events {
    private long eventId;
    private long panelObjectId;
    private String eventType;
    private LocalDateTime eventTime;
    private String eventData;
    private Brain brainByObjectId;

    @Id
    @Column(name = "event_id", nullable = false)
    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    @Basic
    @Column(name = "panel_object_id", nullable = false)
    public long getPanelObjectId() {
        return panelObjectId;
    }

    public void setPanelObjectId(long panelObjectId) {
        this.panelObjectId = panelObjectId;
    }

    @Basic
    @Column(name = "event_type", nullable = false)
    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Basic
    @Column(name = "event_time", nullable = true)
    public LocalDateTime getEventTime() {
        return eventTime;
    }

    public void setEventTime(LocalDateTime eventTime) {
        this.eventTime = eventTime;
    }

    @Basic
    @Column(name = "event_data", nullable = false)
    public String getEventData() {
        return eventData;
    }

    public void setEventData(String eventData) {
        this.eventData = eventData;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Brain.class)
    @JoinColumn(name = "panel_object_id", referencedColumnName = "object_id", nullable = false, insertable = false,
            updatable = false)
    public Brain getBrainByObjectId() {
        return brainByObjectId;
    }

    public void setBrainByObjectId(Brain brainByObjectId) {
        this.brainByObjectId = brainByObjectId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final Events events = (Events) o;
        return eventId == events.eventId &&
                panelObjectId == events.panelObjectId &&
                Objects.equals(eventType, events.eventType) &&
                Objects.equals(eventTime, events.eventTime) &&
                Objects.equals(eventData, events.eventData) &&
                Objects.equals(brainByObjectId, events.brainByObjectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(eventId, panelObjectId, eventType, eventTime, eventData, brainByObjectId);
    }

    @Override
    public String toString() {
        return "Events{" +
                "eventId=" + eventId +
                ", panelObjectId=" + panelObjectId +
                ", eventType='" + eventType + '\'' +
                ", eventTime=" + eventTime +
                ", eventData='" + eventData + '\'' +
                ", brainByObjectId=" + brainByObjectId +
                '}';
    }
}
