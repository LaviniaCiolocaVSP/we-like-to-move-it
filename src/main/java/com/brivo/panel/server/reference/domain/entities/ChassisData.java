package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "chassis_data", schema = "brivo20", catalog = "onair")
public class ChassisData {
    private long chassisId;
    private String chassisType;
    private String mainSerial;
    private String powerSerial;
    private Timestamp created;
    private Timestamp updated;

    @Id
    @Column(name = "chassis_id", nullable = false)
    public long getChassisId() {
        return chassisId;
    }

    public void setChassisId(long chassisId) {
        this.chassisId = chassisId;
    }

    @Basic
    @Column(name = "chassis_type", nullable = true, length = 10)
    public String getChassisType() {
        return chassisType;
    }

    public void setChassisType(String chassisType) {
        this.chassisType = chassisType;
    }

    @Basic
    @Column(name = "main_serial", nullable = true, length = 30)
    public String getMainSerial() {
        return mainSerial;
    }

    public void setMainSerial(String mainSerial) {
        this.mainSerial = mainSerial;
    }

    @Basic
    @Column(name = "power_serial", nullable = true, length = 30)
    public String getPowerSerial() {
        return powerSerial;
    }

    public void setPowerSerial(String powerSerial) {
        this.powerSerial = powerSerial;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ChassisData that = (ChassisData) o;

        if (chassisId != that.chassisId) {
            return false;
        }
        if (chassisType != null ? !chassisType.equals(that.chassisType) : that.chassisType != null) {
            return false;
        }
        if (mainSerial != null ? !mainSerial.equals(that.mainSerial) : that.mainSerial != null) {
            return false;
        }
        if (powerSerial != null ? !powerSerial.equals(that.powerSerial) : that.powerSerial != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (chassisId ^ (chassisId >>> 32));
        result = 31 * result + (chassisType != null ? chassisType.hashCode() : 0);
        result = 31 * result + (mainSerial != null ? mainSerial.hashCode() : 0);
        result = 31 * result + (powerSerial != null ? powerSerial.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }
}
