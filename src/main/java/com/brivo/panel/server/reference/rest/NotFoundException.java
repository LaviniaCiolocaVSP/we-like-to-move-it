package com.brivo.panel.server.reference.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception to be thrown if an operation is requested on an object
 * that does not exist.
 *
 * @author brandon
 */
@ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "Requested object not found")
public class NotFoundException extends RuntimeException {
    private static final long serialVersionUID = -1825006553320249846L;

    public NotFoundException(String string) {
        super(string);
    }

}
