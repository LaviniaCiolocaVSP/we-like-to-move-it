package com.brivo.panel.server.reference.dao.event;

import com.brivo.panel.server.reference.dao.credential.CredentialEventExtractor;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Event Resolution DAO.
 */
@Component
public class EventResolutionDao {
    Logger logger = LoggerFactory.getLogger(EventResolutionDao.class);

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private EventResolutionExtractor resolutionExtractor;

    @Autowired
    private CredentialEventExtractor credentialEventExtractor;

    /**
     * Get Mobile Credential.
     * <p>
     * Run select_mobile_credential.sql
     */
    public Long getMobileCredentialId(Long userObjectId) {
        logger.trace(">>> GetMobileCredential");

        String query = EventQuery.SELECT_MOBILE_CREDENTIAL.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectId", userObjectId);

        logger.trace("Query ["
                + EventQuery.SELECT_MOBILE_CREDENTIAL.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<Long> list =
                template.queryForList(query, paramMap, Long.class);

        logger.trace("<<< GetMobileCredential");

        return Iterables.getFirst(list, null);
    }


    /**
     * Get Entrance Event.
     * <p>
     * Run select_entrance_event.sql
     *//*
    public List<EventResolutionData> getEntranceEvent(SecurityLogEvent event) {
        logger.trace(">>> GetEntranceEvent");

        String query = EventQuery.SELECT_ENTRANCE_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectId", event.getObjectId());
        paramMap.put("userObjectId", event.getActorObjectId());
        paramMap.put("securityGroupId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_ENTRANCE_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetEntranceEvent");

        return list;
    }

    *//**
     * Get Panel Event.
     * <p>
     * Run select_panel_event.sql
     *//*
    public List<EventResolutionData> getPanelEvent(SecurityLogEvent event) {
        logger.trace(">>> GetPanelEvent");


        String query = EventQuery.SELECT_PANEL_BY_OBJECT_ID.getQuery();

        Map<String, Object> paramMap = Collections.singletonMap("panelObjectId", event.getObjectId());

        logger.trace(
                "Query [" + EventQuery.SELECT_PANEL_BY_OBJECT_ID.name() + "]:\n Params [" + paramMap + "] \n" + query);

        EventResolutionData data = template.queryForObject(query, paramMap, (rs, rowNum) ->
        {
            EventResolutionData eventResolutionData = new EventResolutionData();
            eventResolutionData.setAccountId(rs.getLong("account_id"));
            eventResolutionData.setObjectId(rs.getLong("object_id"));
            eventResolutionData.setObjectTypeId(rs.getLong("object_type_id"));
            eventResolutionData.setDeviceName(rs.getString("device_name"));

            return eventResolutionData;
        });

        paramMap = Maps.newHashMap();
        paramMap.put("siteId", event.getObjectGroupObjectId());
        paramMap.put("panelObjectId", event.getObjectId());

        template.query(EventQuery.SELECT_SITE_BY_SECURITY_GROUP_AND_PANEL.getQuery(), paramMap, rs -> {
            data.setSecurityGroupName(rs.getString("site_name"));
            data.setSiteOid(rs.getLong("site_object_id"));
        });

        logger.trace("<<< GetPanelEvent");

        return Lists.newArrayList(data);
    }

    *//**
     * Get Device Event.
     * <p>
     * Run select_device_event.sql
     *//*
    public List<EventResolutionData> getDeviceEvent(SecurityLogEvent event) {
        logger.trace(">>> GetDeviceEvent");

        String query = EventQuery.SELECT_DEVICE_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceObjectId", event.getObjectId());
        paramMap.put("securityGroupId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_DEVICE_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetDeviceEvent");

        return list;
    }

    *//**
     * Get Device Message Event.
     * <p>
     * Run select_device_message_event.sql and update input event.
     *//*
    public List<EventResolutionData> getDeviceMessageEvent(SecurityLogEvent event) {
        logger.trace(">>> GetDeviceMessageEvent");

        String query = EventQuery.SELECT_DEVICE_MESSAGE_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceObjectId", event.getObjectId());
        paramMap.put("siteId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_DEVICE_MESSAGE_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetDeviceMessageEvent");

        return list;
    }

    *//**
     * Get Salto Router Event.
     * <p>
     * Run select_router_event.sql
     *//*
    public List<EventResolutionData> getSaltoRouterEvent(SecurityLogEvent event) {
        logger.trace(">>> GetSaltoRouterEvent");

        String query = EventQuery.SELECT_ROUTER_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("boardObjectId", event.getObjectId());
        paramMap.put("securityGroupId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_ROUTER_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetSaltoRouterEvent");

        return list;
    }

    *//**
     * Get Board Number.
     * <p>
     * Run select_board_number.sql
     *//*
    public Integer getBoardNumber(Long objectId) {
        logger.trace(">>> GetBoardNumber");

        String query = EventQuery.SELECT_BOARD_NUMBER.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectId", objectId);

        logger.trace("Query ["
                + EventQuery.SELECT_BOARD_NUMBER.name()
                + "]:\n Params [" + paramMap + "] \n" + query);
        Integer boardNumber = template.queryForObject(query, paramMap, Integer.class);

        logger.trace("<<< GetBoardNumber");

        return boardNumber;
    }

    *//**
     * Get Board Point Event.
     * <p>
     * Run select_board_point_event.sql
     *//*
    public List<EventResolutionData> getBoardPointEvent(SecurityLogEvent event) {
        logger.trace(">>> GetBoardPointEvent");

        String query = EventQuery.SELECT_BOARD_POINT_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceObjectId", event.getObjectId());
        paramMap.put("siteId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_BOARD_POINT_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetBoardPointEvent");

        return list;
    }

    *//**
     * Get Schedule Activated Event.
     * <p>
     * Run select_schedule_activated_event.sql
     *//*
    public List<EventResolutionData> getScheduleActivationEvent(SecurityLogEvent event) {
        logger.trace(">>> GetBoardPointEvent");

        String query = EventQuery.SELECT_SCHEDULE_ACTIVATED_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceObjectId", event.getObjectId());
        paramMap.put("userObjectId", event.getActorObjectId());
        paramMap.put("securityGroupId", event.getObjectGroupObjectId());

        logger.trace("Query ["
                + EventQuery.SELECT_SCHEDULE_ACTIVATED_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, resolutionExtractor);

        logger.trace("<<< GetBoardPointEvent");

        return list;
    }

    *//**
     * Get Credential By Value Event.
     *//*
    public List<EventResolutionData> getCredentialByValueEvent(
            String credentialValue, Integer credentialBitCount, Panel panel) {
        logger.trace(">>> GetCredentialByValueEvent");

        String query = EventQuery.SELECT_CREDENTIAL_BY_VALUE_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectID", panel.getObjectId());
        paramMap.put("credentialValue", credentialValue);
        paramMap.put("credentialBits", credentialBitCount);

        logger.trace("Query ["
                + EventQuery.SELECT_CREDENTIAL_BY_VALUE_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, credentialEventExtractor);

        logger.trace("<<< GetCredentialByValueEvent");

        return list;
    }

    *//**
     * Get Credential By Value 255 Mask Event.
     *//*
    public List<EventResolutionData> getCredentialByValue255MaskEvent(
            String credentialValue, Panel panel) {
        logger.trace(">>> GetCredentialByValue255MaskEvent");

        String query = EventQuery.SELECT_CREDENTIAL_BY_VALUE_255_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());
        paramMap.put("credentialValue", credentialValue);

        logger.trace("Query ["
                + EventQuery.SELECT_CREDENTIAL_BY_VALUE_255_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, credentialEventExtractor);

        logger.trace("<<< GetCredentialByValue255MaskEvent");

        return list;
    }

    *//**
     * Get Pin By Value Event.
     *//*
    public List<EventResolutionData> getPinByValueEvent(SecurityLogEvent event, Panel panel) {
        logger.trace(">>> GetPinByValueEvent");

        String query = EventQuery.SELECT_PIN_BY_VALUE_EVENT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("objectID", panel.getObjectId());
        paramMap.put("credentialValue", event.getEventData().getUnknownWeigand());

        logger.trace("Query ["
                + EventQuery.SELECT_PIN_BY_VALUE_EVENT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<EventResolutionData> list =
                template.query(query, paramMap, credentialEventExtractor);

        logger.trace("<<< GetPinByValueEvent");
        return list;
    }

    *//**
     * Run select_user_by_object_id.sql
     *//*
    public User getUserByObjectId(Long userObjectId) {
        logger.trace(">>> GetUserByObjectId");

        String query = EventQuery.SELECT_USER_BY_OBJECT_ID.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userObjectId", userObjectId);

        logger.trace("Query ["
                + EventQuery.SELECT_USER_BY_OBJECT_ID.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        List<User> users = template.query(query, paramMap, (rs, rowNum) ->
        {
            Date deactivated = rs.getTimestamp("deactivated");

            Integer disabledInt = rs.getInt("disabled");
            Boolean disabled = false;
            if (disabledInt != null && disabledInt == 1) {
                disabled = true;
            }

            User user = new User();
            user.setFirstName(rs.getString("first_name"));
            user.setLastName(rs.getString("last_name"));
            user.setDeactivated((deactivated == null) ? null : deactivated.toInstant());
            user.setDisabled(disabled);
            return user;
        });

        return Iterables.getFirst(users, null);
    }

    *//**
     * Run select_permission_count.sql
     *//*
    public int getPermissionCount(Long deviceObjectId, Long userObjectId) {
        logger.trace(">>> GetPermissionCount");

        String query = EventQuery.SELECT_PERMISSION_COUNT.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceObjectId", deviceObjectId);
        paramMap.put("userObjectId", userObjectId);

        logger.trace("Query ["
                + EventQuery.SELECT_PERMISSION_COUNT.name()
                + "]:\n Params [" + paramMap + "] \n" + query);

        int count = template.queryForObject(query, paramMap, Integer.class);

        logger.trace("<<< GetPermissionCount");

        return count;
    }

    public String getReferenceId(Long credentialId, Long panelObjectId) {
        String query = EventQuery.SELECT_UNKNOWN_CARD_ID.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panelObjectId);
        paramMap.put("credentialId", credentialId);

        logger.trace("Query ["
                + EventQuery.SELECT_UNKNOWN_CARD_ID.name()
                + "]:\n Params [" + paramMap + "] \n" + query);
        List<String> referenceIds = template.queryForList(query, paramMap, String.class);

        return Iterables.getFirst(referenceIds, "");
    }


    public String getScheduleName(Long scheduleId) {
        String query = EventQuery.SELECT_SCHEDULE_NAME.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", scheduleId);

        logger.trace("Query ["
                + EventQuery.SELECT_SCHEDULE_NAME.name()
                + "]:\n Params [" + paramMap + "] \n" + query);
        String name = template.queryForObject(query, paramMap, String.class);

        return name;
    }*/

}


