package com.brivo.panel.server.reference.service.credential.utils;

import com.brivo.panel.server.reference.domain.entities.credential.Credential;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialField;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldType;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldValue;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFormat;
import org.apache.commons.lang.StringUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Class for encoding and decoding credentials that are of binary type. The
 * pupose of the encoder is to turn a list of number values into a properly
 * formatted binary string. Conversely, the decoding will take a properly
 * formatted binary string, and extract those number values.
 *
 * <p>
 * For encoding, the fields of a credential format are iterated in order (all
 * value fields, then parities, then masks). The
 * {@link CredentialField#getFormatValue()} is used in conjunction with
 * {@link CredentialField#getCredentialFieldType()} to determine the appropriate
 * method for encoding. For binary encodings, the length of each
 * {@link CredentialField#getFormatValue()} must be equal to
 * {@link CredentialFormat#getNumBits()}. The values and their use by the
 * encoder are described below
 * </p>
 *
 * <ul>
 * <li>
 * {@link CredentialFieldType VALUE} - The formatValue for this specifies where
 * each value bit should be inserted into the final string. An 'X' indicates
 * that that bit in the final string is meant to hold a value for this field.
 * All other characters are ignored. For example, with a 10 bit encoding
 * <p>
 * <code>
 * ..X.XX.X..
 * </code>
 * </p>
 * may be a given formatValue. Because there are 4 X's, our max value for this
 * field is 2^4-1, or 15. After inserting a value of 9 (101 in binary), our string
 * would look like:
 * <p>camera_group
 * <code>
 * ..0.10.1..
 * </code>
 * </p>
 * </li>
 * <li>
 * {@link CredentialFieldType PRESET_VALUE} - The formatValue for this specifies
 * where each bit of a preset value should be inserted into the final string. 1
 * and 0 represent values to be inserted, while '.' represents a bit to be
 * ignored. For example, again with a 10 bit format
 * <p>
 * <code>
 * .1.0..0.1.
 * </code>
 * </p>
 * <p>
 * Would specify that in the final encoded value, bits 2, 4, 7 and 9 would be 1,
 * 0, 0 and 1 respectively
 * </p>
 * </li>
 * <li>
 * {@link CredentialFieldType PARITY_BIT} - The formatValue for this specifies
 * whether a parity calculation should be even or odd, and also which bits
 * should be considered for parity. Proper ordering of the parity bits is
 * important, as some parity calcuations may rely on the results of other parity
 * calcuations. In this format, 'E' or 'O' represent the bit that is to be
 * replaced with the parity, and also whether it is even or odd, respectively.
 * 'X' represents bits which are to be considered for parity. All other
 * characters are ignored. An example, again with a 10 bit format:
 * <p>
 * <code>
 * EXX.X.X...
 * </code>
 * </p>
 * This specifies an even parity in the first space calculated ovr the 2nd, 3rd,
 * 5th and 7th digits. This means the number of 1s across those 4 digits along
 * with the parity bit should be even
 * <p></li>
 * <li>
 * {@link CredentialFieldType BIT_MASK}</li>
 * <li>
 * All other {@code CredentialFieldType}s are invalid for this encoder</li>
 * </ul>
 *
 * <p>
 * The following is an example of the standard 26 bit wiegand format definition,
 * and a rundown of the encoding process.
 * </p>
 *
 * <p>
 * <b> Format Definition</b> 26 bits<br/>
 * <ul>
 * <li>Card Number <code> .........XXXXXXXXXXXXXXXX.</code> 16 bits for value</li>
 * <li>Facility Code <code> .XXXXXXXX.................</code> 8 bits for facility code</li>
 * <li>Parity Bit <code> EXXXXXXXXXXXX.............</code></li>
 * <li>Parity Bit <code> .............XXXXXXXXXXXXO</code></li>
 * </ul>
 * </p>
 *
 * <p>
 * <b> Sample Card Values </b>
 * <ul>
 * <li>Card Number <code> 35143 </code></li>
 * <li>Facility Code <code> 154   </code></li>
 * </ul>
 * </p>
 *
 * <p>
 * Steps
 * <ol>
 * <li>Create a string of 0s the size of the credential (26 bits). Our
 * credential looks like <code>00000000000000000000000000</code></li>
 * <li>Process each field value in order
 * <ol>
 * <li>Card Number - The number 35143 should be converted to binary first
 * (1000100101000111) and then the 'X's specified by the format Value should be
 * replaced, starting from the right. Our credential now looks like
 * <code>00000000010001001010001110</code></li>
 * <li>Facility Code - The number 154 should be converted to binary first
 * (10011010) and then the 'X's specified by the format Value should be
 * replaced, starting from the right. Our credential now looks like
 * <code>01001101010001001010001110</code></li>
 * </ol>
 * <li>Now process the parity fields
 * <ol>
 * <li>First parity bit - The first parity bit is even and across the first 13
 * digits. The first 13 digits now are 0100110101000. There are 5 1s, and an
 * even parity bit, so our parity bit should be 1 to bring the total number to 6
 * Our credential now looks like
 * <code>11001101010001001010001110</code></li>
 * <li>Second parity bit - The second parity bit is odd and across the last 13
 * digits. The last 13 digits now are 1001010001110. There are 6 1s, and an
 * odd parity bit, so our parity bit should be 1 to bring the total number to 7
 * Our credential now looks like
 * <code>11001101010001001010001111</code></li>
 * </ol>
 * </li>
 * <li> Done with fields, now just turn the value into hex.  Our hex-encoded credential is
 * <code>335128f </code>
 * </li>
 * </ol>
 * </p>
 */
public class BinaryCredentialEncoder implements CredentialEncoder {
    @Override
    public void encode(Credential toEncode) {
        List<CredentialFieldValue> valueCredentialFields = toEncode.getFieldValues();
        Collections.sort(valueCredentialFields, new CredentialFieldValue.ByFieldOrderComparator());
        
        String s = StringUtils.rightPad("",
                                        toEncode.getNumBits(), '0');
        // create a string of all 0s of the correct length to start with
        StringBuilder finalValue = new StringBuilder(s);
        
        // Insert values and apply masks
        for (CredentialFieldValue value : valueCredentialFields) {
            switch (value.getCredentialField().getCredentialFieldType()) {
                case VALUE:
                    insertValueField(finalValue, value);
                    break;
                case PRESET_VALUE:
                    insertPresetValueField(finalValue, value);
                    break;
                case PARITY_BIT:
                    String parityMask = value.getCredentialField().getFormatValue();
                    applyParity(finalValue, parityMask);
                    break;
                case BIT_MASK:
                    String bitMask = value.getCredentialField().getFormatValue();
                    // store the finalValue premask in our credential
                    String unmaskedCredential = RadixUtils.convertBase(finalValue
                                                                               .toString(), 2, 16);
                    toEncode.setUnmaskedCredential(unmaskedCredential);
                    applyMask(finalValue, bitMask);
                    break;
                default:
                    throw new IllegalStateException(
                            "Unexpected field type for a Binary encoded credential");
            }
        }
        toEncode.setEncodedCredential(RadixUtils.convertBase(finalValue
                                                                     .toString(), 2, 16));
    }
    
    private void insertValueField(StringBuilder binaryCardValue,
                                  CredentialFieldValue fieldValue) {
        
        String templateValue = fieldValue.getCredentialField().getFormatValue();
        
        String valueInBinary = RadixUtils.convertBase(fieldValue.getValue(), 2);
        
        // Replace each MASK_CHAR with one digit from our value.
        // start at the least significant digit and work forward, that way we
        // don't have to pad in useless 0s
        int lastReplaced = templateValue.length();
        for (int i = valueInBinary.length() - 1; i >= 0; i--) {
            lastReplaced = templateValue.lastIndexOf(CredentialFormat.MASK_CHAR, lastReplaced - 1);
            // If lastReplaced is -1 that means we have more binary digits
            // than our mask allowed for.  We need to error our, that is an
            // illegal argument
            if (lastReplaced == -1) {
                throw new IllegalStateException(fieldValue.getValue()
                                                + " was too large for the value template ["
                                                + fieldValue.getCredentialField().getFormatValue() + "]");
            }
            
            binaryCardValue.setCharAt(lastReplaced, valueInBinary.charAt(i));
        }
        
    }
    
    private void insertPresetValueField(StringBuilder binaryCardValue, CredentialFieldValue fieldValue) {
        String presetValueTemplate = fieldValue.getCredentialField().getFormatValue();
        
        // anything that isn't the IGNORE_BIT should be overwritten by the
        // preset value's value at that digit
        for (int i = 0; i < presetValueTemplate.length(); i++) {
            if (presetValueTemplate.charAt(i) != CredentialFormat.IGNORE_BIT) {
                binaryCardValue.setCharAt(i, presetValueTemplate.charAt(i));
            }
        }
    }
    
    private void applyParity(StringBuilder binaryCardValue, String parityMask) {
        char parityBit = '0'; // start with an even parity, if we find out it's odd later, flip it
        
        int parityBitLocation = -1;
        boolean evenParityBit = true;
        
        for (int i = 0; i < parityMask.length(); i++) {
            switch (parityMask.charAt(i)) {
                case CredentialFormat.EVEN_PARITY:
                    parityBitLocation = i;
                    break;
                case CredentialFormat.ODD_PARITY:
                    parityBitLocation = i;
                    evenParityBit = false;
                    break;
                case CredentialFormat.MASK_CHAR:
                    char valueAt = binaryCardValue.charAt(i);
                    parityBit = parityBit == valueAt ? '0' : '1';
                    break;
                default:
                    // ignore all other characters
                    break;
            }
        }
        
        // swap the parity bit, since we calculated assuming even, but it's odd
        if (!evenParityBit) {
            parityBit = parityBit == '1' ? '0' : '1';
        }
        
        binaryCardValue.setCharAt(parityBitLocation, parityBit);
    }
    
    private void applyMask(StringBuilder binaryCardValue, String bitMask) {
        
        BigInteger binaryCardNumber = new BigInteger(binaryCardValue.toString(), 2);
        BigInteger bitMaskNumber = new BigInteger(bitMask, 2);
        
        String maskedValueString = binaryCardNumber.and(bitMaskNumber).toString(2);
        // pad the string
        maskedValueString = StringUtils.leftPad(maskedValueString, bitMask.length(), '0');
        binaryCardValue.replace(0, maskedValueString.length(), maskedValueString);
    }
    
    @Override
    public Credential decode(String encodedCredential, CredentialFormat credentialFormat) {
        // Reverse the order of our fields, as we'll handle them in the opposite
        // way we did the encoding
        List<CredentialField> credentialFields = new ArrayList<CredentialField>(credentialFormat.getFields());
        Collections.reverse(credentialFields);
        
        // Convert the string from hex to base 2 and pad it to the appropriate length
        String encodedBinary = RadixUtils.convertBase(encodedCredential, 16, 2);
        encodedBinary = StringUtils.leftPad(encodedBinary, credentialFormat.getNumBits(), '0');
        
        // Build the start of our credential
        Credential decoded = new Credential();
        List<CredentialFieldValue> decodedFields = new ArrayList<CredentialFieldValue>();
        decoded.setFieldValues(decodedFields);
        decoded.setFormat(credentialFormat);
        
        // loop through our fields, processing in reverse order than the encoding
        for (CredentialField field : credentialFields) {
            
            switch (field.getCredentialFieldType()) {
                case PARITY_BIT:
                    StringBuilder parityCheck = new StringBuilder(encodedBinary);
                    applyParity(parityCheck, field.getFormatValue());
                    
                    if (!parityCheck.toString().equals(encodedBinary)) {
                        return null;
                    }
                    break;
                case PRESET_VALUE:
                    if (!checkPresetEqual(encodedBinary, field.getFormatValue())) {
                        return null;
                    }
                    break;
                case VALUE:
                    String value = extractValueFromMask(encodedBinary, field.getFormatValue());
                    CredentialFieldValue cf = new CredentialFieldValue();
                    cf.setCredentialField(field);
                    cf.setValue(RadixUtils.convertBase(value, 2, 10));
                    decodedFields.add(cf);
                    break;
                case BIT_MASK:
                    // We'll ignore bit masks. Decode is basically an undo of an
                    // encode operation, and we can't undo a mask. Also, the hex
                    // value from unknown cards will be the unmasked hex value, so
                    // we'll be able to decode 75 bit PIVs read off the panel
                    // effectively.
                    break;
                default:
                    throw new IllegalStateException(
                            "Unexpected field type for a Binary encoded credential");
            }
        }
        
        return decoded;
    }
    
    
    private String extractValueFromMask(String source, String mask) {
        StringBuilder value = new StringBuilder();
        
        for (int i = 0; i < mask.length(); i++) {
            if (mask.charAt(i) == CredentialFormat.MASK_CHAR) {
                value.append(source.charAt(i));
            }
        }
        return value.toString();
        
    }
    
    private boolean checkPresetEqual(String encodedBinary, String formatValue) {
        char expectedCharAt;
        char actualCharAt;
        for (int i = 0; i < formatValue.length(); i++) {
            expectedCharAt = formatValue.charAt(i);
            actualCharAt = encodedBinary.charAt(i);
            if (expectedCharAt != CredentialFormat.IGNORE_BIT) {
                if (actualCharAt != expectedCharAt) {
                    return false;
                }
            }
        }
        return true;
    }
    
    
    public void encodeToBinaryBase(Credential toEncode) {
        List<CredentialFieldValue> valueCredentialFields = toEncode.getFieldValues();
        Collections.sort(valueCredentialFields, new CredentialFieldValue.ByFieldOrderComparator());
        
        String s = StringUtils.rightPad("",
                                        toEncode.getNumBits(), '0');
        // create a string of all 0s of the correct length to start with
        StringBuilder finalValue = new StringBuilder(s);
        
        // Insert values and apply masks
        for (CredentialFieldValue value : valueCredentialFields) {
            switch (value.getCredentialField().getCredentialFieldType()) {
                case VALUE:
                    insertValueField(finalValue, value);
                    break;
                case PRESET_VALUE:
                    insertPresetValueField(finalValue, value);
                    break;
                case PARITY_BIT:
                    String parityMask = value.getCredentialField().getFormatValue();
                    applyParity(finalValue, parityMask);
                    break;
                case BIT_MASK:
                    String bitMask = value.getCredentialField().getFormatValue();
                    // store the finalValue premask in our credential
                    String unmaskedCredential = RadixUtils.convertBase(finalValue
                                                                               .toString(), 2, 16);
                    toEncode.setUnmaskedCredential(unmaskedCredential);
                    applyMask(finalValue, bitMask);
                    break;
                default:
                    throw new IllegalStateException(
                            "Unexpected field type for a Binary encoded credential");
            }
        }
        toEncode.setEncodedCredential(finalValue.toString());
    }
}
