package com.brivo.panel.server.reference.dto.ui;

import com.brivo.panel.server.reference.constants.IOBoardIOPointMap;
import com.brivo.panel.server.reference.dto.FirmwareVersionDTO;
import com.brivo.panel.server.reference.dto.IOPointTemplateDTO;
import com.google.common.base.MoreObjects;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class UiIOBoardDTO {
    
    public static final FirmwareVersionDTO FAILSAFE_FIRMWARE_VERSION = new FirmwareVersionDTO("5.1.2");
    
    private IOPointTemplateDTO input1;
    private IOPointTemplateDTO input2;
    private IOPointTemplateDTO input3;
    private IOPointTemplateDTO input4;
    private IOPointTemplateDTO input5;
    private IOPointTemplateDTO input6;
    private IOPointTemplateDTO input7;
    private IOPointTemplateDTO input8;
    
    private IOPointTemplateDTO output1;
    private IOPointTemplateDTO output2;
    private IOPointTemplateDTO output3;
    private IOPointTemplateDTO output4;
    private IOPointTemplateDTO output5;
    private IOPointTemplateDTO output6;
    private IOPointTemplateDTO output7;
    private IOPointTemplateDTO output8;
    
    private boolean failSafeStateAllowed;
    
    public UiIOBoardDTO() {
    }
    
    public UiIOBoardDTO(IOPointTemplateDTO input1, IOPointTemplateDTO input2, IOPointTemplateDTO input3,
                        IOPointTemplateDTO input4, IOPointTemplateDTO input5, IOPointTemplateDTO input6,
                        IOPointTemplateDTO input7, IOPointTemplateDTO input8, IOPointTemplateDTO output1,
                        IOPointTemplateDTO output2, IOPointTemplateDTO output3, IOPointTemplateDTO output4,
                        IOPointTemplateDTO output5, IOPointTemplateDTO output6, IOPointTemplateDTO output7,
                        IOPointTemplateDTO output8, boolean failSafeStateAllowed) {
        this.input1 = input1;
        this.input2 = input2;
        this.input3 = input3;
        this.input4 = input4;
        this.input5 = input5;
        this.input6 = input6;
        this.input7 = input7;
        this.input8 = input8;
        this.output1 = output1;
        this.output2 = output2;
        this.output3 = output3;
        this.output4 = output4;
        this.output5 = output5;
        this.output6 = output6;
        this.output7 = output7;
        this.output8 = output8;
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
    
    
    public IOPointTemplateDTO getInput1() {
        return input1;
    }
    
    public void setInput1(IOPointTemplateDTO input1) {
        this.input1 = input1;
    }
    
    public IOPointTemplateDTO getInput2() {
        return input2;
    }
    
    public void setInput2(IOPointTemplateDTO input2) {
        this.input2 = input2;
    }
    
    public IOPointTemplateDTO getInput3() {
        return input3;
    }
    
    public void setInput3(IOPointTemplateDTO input3) {
        this.input3 = input3;
    }
    
    public IOPointTemplateDTO getInput4() {
        return input4;
    }
    
    public void setInput4(IOPointTemplateDTO input4) {
        this.input4 = input4;
    }
    
    public IOPointTemplateDTO getInput5() {
        return input5;
    }
    
    public void setInput5(IOPointTemplateDTO input5) {
        this.input5 = input5;
    }
    
    public IOPointTemplateDTO getInput6() {
        return input6;
    }
    
    public void setInput6(IOPointTemplateDTO input6) {
        this.input6 = input6;
    }
    
    public IOPointTemplateDTO getInput7() {
        return input7;
    }
    
    public void setInput7(IOPointTemplateDTO input7) {
        this.input7 = input7;
    }
    
    public IOPointTemplateDTO getInput8() {
        return input8;
    }
    
    public void setInput8(IOPointTemplateDTO input8) {
        this.input8 = input8;
    }
    
    public IOPointTemplateDTO getOutput1() {
        return output1;
    }
    
    public void setOutput1(IOPointTemplateDTO output1) {
        this.output1 = output1;
    }
    
    public IOPointTemplateDTO getOutput2() {
        return output2;
    }
    
    public void setOutput2(IOPointTemplateDTO output2) {
        this.output2 = output2;
    }
    
    public IOPointTemplateDTO getOutput3() {
        return output3;
    }
    
    public void setOutput3(IOPointTemplateDTO output3) {
        this.output3 = output3;
    }
    
    public IOPointTemplateDTO getOutput4() {
        return output4;
    }
    
    public void setOutput4(IOPointTemplateDTO output4) {
        this.output4 = output4;
    }
    
    public IOPointTemplateDTO getOutput5() {
        return output5;
    }
    
    public void setOutput5(IOPointTemplateDTO output5) {
        this.output5 = output5;
    }
    
    public IOPointTemplateDTO getOutput6() {
        return output6;
    }
    
    public void setOutput6(IOPointTemplateDTO output6) {
        this.output6 = output6;
    }
    
    public IOPointTemplateDTO getOutput7() {
        return output7;
    }
    
    public void setOutput7(IOPointTemplateDTO output7) {
        this.output7 = output7;
    }
    
    public IOPointTemplateDTO getOutput8() {
        return output8;
    }
    
    public void setOutput8(IOPointTemplateDTO output8) {
        this.output8 = output8;
    }
    
    public boolean isFailSafeStateAllowed() {
        return failSafeStateAllowed;
    }
    
    public void setFailSafeStateAllowed(boolean failSafeStateAllowed) {
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
    
    public void setIOPoints(Collection<IOPointTemplateDTO> ioPoints, boolean failSafeStateAllowed) {
        if (ioPoints == null) return;
        
        Map<Integer, IOPointTemplateDTO> ioPointMap = new HashMap<Integer, IOPointTemplateDTO>();
        
        for (Iterator<IOPointTemplateDTO> i = ioPoints.iterator(); i.hasNext(); ) {
            IOPointTemplateDTO ioPoint = i.next();
            
            Integer pointAddress = ioPoint.getPointAddress();
            
            ioPointMap.put(pointAddress, ioPoint);
        }
        
        IOPointTemplateDTO input1 = ioPointMap.get(IOBoardIOPointMap.INPUT_1.getState());
        if (input1 != null) {
            this.input1 = input1;
            this.input1.setDefaultLabel(input1.getSilkscreen());
        }
        
        IOPointTemplateDTO input2 = ioPointMap.get(IOBoardIOPointMap.INPUT_2.getState());
        if (input2 != null) {
            this.input2 = input2;
            this.input2.setDefaultLabel(input2.getSilkscreen());
        }
        
        IOPointTemplateDTO input3 = ioPointMap.get(IOBoardIOPointMap.INPUT_3.getState());
        if (input3 != null) {
            this.input3 = input3;
            this.input3.setDefaultLabel(input3.getSilkscreen());
        }
        
        IOPointTemplateDTO input4 = ioPointMap.get(IOBoardIOPointMap.INPUT_4.getState());
        if (input4 != null) {
            this.input4 = input4;
            this.input4.setDefaultLabel(input4.getSilkscreen());
        }
        
        IOPointTemplateDTO input5 = ioPointMap.get(IOBoardIOPointMap.INPUT_5.getState());
        if (input5 != null) {
            this.input5 = input5;
            this.input5.setDefaultLabel(input5.getSilkscreen());
        }
        
        IOPointTemplateDTO input6 = ioPointMap.get(IOBoardIOPointMap.INPUT_6.getState());
        if (input6 != null) {
            this.input6 = input6;
            this.input6.setDefaultLabel(input6.getSilkscreen());
        }
        
        IOPointTemplateDTO input7 = ioPointMap.get(IOBoardIOPointMap.INPUT_7.getState());
        if (input7 != null) {
            this.input7 = input7;
            this.input7.setDefaultLabel(input7.getSilkscreen());
        }
        
        
        IOPointTemplateDTO input8 = ioPointMap.get(IOBoardIOPointMap.INPUT_8.getState());
        if (input8 != null) {
            this.input8 = input8;
            this.input8.setDefaultLabel(input8.getSilkscreen());
        }
        
        
        IOPointTemplateDTO output1 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_1.getState());
        if (output1 != null) {
            this.output1 = output1;
            this.output1.setDefaultLabel(output1.getSilkscreen());
        }
        
        IOPointTemplateDTO output2 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_2.getState());
        if (output2 != null) {
            this.output2 = output2;
            this.output2.setDefaultLabel(output2.getSilkscreen());
        }
        
        IOPointTemplateDTO output3 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_3.getState());
        if (output3 != null) {
            this.output3 = output3;
            this.output3.setDefaultLabel(output3.getSilkscreen());
        }
        
        IOPointTemplateDTO output4 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_4.getState());
        if (output4 != null) {
            this.output4 = output4;
            this.output4.setDefaultLabel(output4.getSilkscreen());
        }
        
        IOPointTemplateDTO output6 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_6.getState());
        if (output6 != null) {
            this.output6 = output6;
            this.output6.setDefaultLabel(output6.getSilkscreen());
        }
        
        IOPointTemplateDTO output7 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_7.getState());
        if (output7 != null) {
            this.output7 = output7;
            this.output7.setDefaultLabel(output7.getSilkscreen());
        }
        
        IOPointTemplateDTO output5 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_5.getState());
        if (output5 != null) {
            this.output5 = output5;
            this.output5.setDefaultLabel(output5.getSilkscreen());
        }
        
        IOPointTemplateDTO output8 = ioPointMap.get(IOBoardIOPointMap.OUTPUT_8.getState());
        if (output8 != null) {
            this.output8 = output8;
            this.output8.setDefaultLabel(output8.getSilkscreen());
        }
        
        this.failSafeStateAllowed = failSafeStateAllowed;
    }
}
