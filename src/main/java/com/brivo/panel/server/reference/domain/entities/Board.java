package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.io.Serializable;
import java.util.Collection;

@Entity
@IdClass(BoardPK.class)
public class Board implements Serializable {
    private long panelOid;
    private String boardLocation;
    private short boardNumber;
    private long objectId;
    private Brain brainByPanelOid;
    private long boardType;
    private BoardType boardTypeByBoardType;
    private BrivoObject objectByBrivoObjectId;
    private Collection<BoardProperty> boardPropertiesByObjectId;
    private Collection<ElevatorFloorMap> elevatorFloorMaps;
    private Collection<ProgDevicePuts> progDevicePuts;
    private Collection<ProgIoPoints> progIoPoints;
    private DoorData doorData;
    
    @Id
    @Column(name = "panel_oid", nullable = false, insertable = false, updatable = false)
    public long getPanelOid() {
        return panelOid;
    }
    
    public void setPanelOid(long panelOid) {
        this.panelOid = panelOid;
    }
    
    @Column(name = "board_location", nullable = true, length = 75)
    public String getBoardLocation() {
        return boardLocation;
    }
    
    public void setBoardLocation(String boardLocation) {
        this.boardLocation = boardLocation;
    }
    
    @Column(name = "board_type", nullable = false, insertable = false, updatable = false)
    public long getBoardType() {
        return boardType;
    }
    
    public void setBoardType(long boardType) {
        this.boardType = boardType;
    }
    
    @Id
    @Column(name = "board_number", nullable = false)
    public short getBoardNumber() {
        return boardNumber;
    }
    
    public void setBoardNumber(short boardNumber) {
        this.boardNumber = boardNumber;
    }
    
    @Column(name = "object_id", nullable = false, insertable = false, updatable = false)
    public long getObjectId() {
        return objectId;
    }
    
    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Brain.class)
    @JoinColumn(name = "panel_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByPanelOid() {
        return brainByPanelOid;
    }
    
    public void setBrainByPanelOid(Brain brainByPanelOid) {
        this.brainByPanelOid = brainByPanelOid;
    }
    
    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "board_type", referencedColumnName = "board_type_id")
    public BoardType getBoardTypeByBoardType() {
        return boardTypeByBoardType;
    }
    
    public void setBoardTypeByBoardType(BoardType boardTypeByBoardType) {
        this.boardTypeByBoardType = boardTypeByBoardType;
    }
    
    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }
    
    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }
    
    @OneToMany(mappedBy = "boardByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<BoardProperty> getBoardPropertiesByObjectId() {
        return boardPropertiesByObjectId;
    }
    
    public void setBoardPropertiesByObjectId(Collection<BoardProperty> boardPropertiesByObjectId) {
        this.boardPropertiesByObjectId = boardPropertiesByObjectId;
    }
    
    @OneToMany(mappedBy = "board", fetch = FetchType.LAZY)
    public Collection<ElevatorFloorMap> getElevatorFloorMaps() {
        return elevatorFloorMaps;
    }
    
    public void setElevatorFloorMaps(Collection<ElevatorFloorMap> elevatorFloorMaps) {
        this.elevatorFloorMaps = elevatorFloorMaps;
    }

//    @OneToOne(mappedBy = "board", cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = DoorData.class, optional = false)
//    public DoorData getDoorData() {
//        return doorData;
//    }
//
//    public void setDoorData(DoorData doorData) {
//        this.doorData = doorData;
//    }
    
    @OneToMany(mappedBy = "board", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<ProgDevicePuts> getProgDevicePuts() {
        return progDevicePuts;
    }
    
    public void setProgDevicePuts(Collection<ProgDevicePuts> progDevicePuts) {
        this.progDevicePuts = progDevicePuts;
    }
    
    @OneToMany(mappedBy = "board", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
   /* @JoinColumn(name = "board_number", referencedColumnName = "board_number")*/
    public Collection<ProgIoPoints> getProgIoPoints() {
        return progIoPoints;
    }
    
    public void setProgIoPoints(Collection<ProgIoPoints> progIoPoints) {
        this.progIoPoints = progIoPoints;
    }
    
}
