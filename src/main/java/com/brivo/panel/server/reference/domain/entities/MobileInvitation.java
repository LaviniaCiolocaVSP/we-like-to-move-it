package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "mobile_invitation", schema = "brivo20", catalog = "onair")
public class MobileInvitation implements Serializable {
    private long mobileInvitationId;
    private String email;
    private long userObjectId;
    private long accountId;
    private long accessCredentialId;
    private short redeemed;
    private Timestamp redemptionDate;
    private Timestamp expirationDate;
    private short cancelled;
    private Timestamp cancelDate;
    private String accessCode;
    private Timestamp created;
    //    private Collection<Users> mobileInvitationByObjectId;
    private Account accountByAccountId;

    @Id
    @Column(name = "mobile_invitation_id", nullable = false)
    public long getMobileInvitationId() {
        return mobileInvitationId;
    }

    public void setMobileInvitationId(long mobileInvitationId) {
        this.mobileInvitationId = mobileInvitationId;
    }

    @Column(name = "email", nullable = false, length = 512)
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "access_credential_id", nullable = false)
    public long getAccessCredentialId() {
        return accessCredentialId;
    }

    public void setAccessCredentialId(long accessCredentialId) {
        this.accessCredentialId = accessCredentialId;
    }

    @Column(name = "redeemed", nullable = false)
    public short getRedeemed() {
        return redeemed;
    }

    public void setRedeemed(short redeemed) {
        this.redeemed = redeemed;
    }

    @Column(name = "redemption_date", nullable = true)
    public Timestamp getRedemptionDate() {
        return redemptionDate;
    }

    public void setRedemptionDate(Timestamp redemptionDate) {
        this.redemptionDate = redemptionDate;
    }

    @Column(name = "expiration_date", nullable = false)
    public Timestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Timestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    @Column(name = "cancelled", nullable = false)
    public short getCancelled() {
        return cancelled;
    }

    public void setCancelled(short cancelled) {
        this.cancelled = cancelled;
    }

    @Column(name = "cancel_date", nullable = true)
    public Timestamp getCancelDate() {
        return cancelDate;
    }

    public void setCancelDate(Timestamp cancelDate) {
        this.cancelDate = cancelDate;
    }

    @Column(name = "access_code", nullable = false, length = 55)
    public String getAccessCode() {
        return accessCode;
    }

    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MobileInvitation that = (MobileInvitation) o;

        if (mobileInvitationId != that.mobileInvitationId) {
            return false;
        }
//        if (userObjectId != that.userObjectId) return false;
        if (accountId != that.accountId) {
            return false;
        }
        if (accessCredentialId != that.accessCredentialId) {
            return false;
        }
        if (redeemed != that.redeemed) {
            return false;
        }
        if (cancelled != that.cancelled) {
            return false;
        }
        if (email != null ? !email.equals(that.email) : that.email != null) {
            return false;
        }
        if (redemptionDate != null ? !redemptionDate.equals(that.redemptionDate) : that.redemptionDate != null) {
            return false;
        }
        if (expirationDate != null ? !expirationDate.equals(that.expirationDate) : that.expirationDate != null) {
            return false;
        }
        if (cancelDate != null ? !cancelDate.equals(that.cancelDate) : that.cancelDate != null) {
            return false;
        }
        if (accessCode != null ? !accessCode.equals(that.accessCode) : that.accessCode != null) {
            return false;
        }
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (mobileInvitationId ^ (mobileInvitationId >>> 32));
        result = 31 * result + (email != null ? email.hashCode() : 0);
//        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (accessCredentialId ^ (accessCredentialId >>> 32));
        result = 31 * result + (int) redeemed;
        result = 31 * result + (redemptionDate != null ? redemptionDate.hashCode() : 0);
        result = 31 * result + (expirationDate != null ? expirationDate.hashCode() : 0);
        result = 31 * result + (int) cancelled;
        result = 31 * result + (cancelDate != null ? cancelDate.hashCode() : 0);
        result = 31 * result + (accessCode != null ? accessCode.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

//    @OneToMany(mappedBy = "mobileInvitationByObjectId")
//    public Collection<Users> getMobileInvitationByObjectId() {
//        return mobileInvitationByObjectId;
//    }
//
//    public void setMobileInvitationByObjectId(Collection<Users> mobileInvitationByObjectId) {
//        this.mobileInvitationByObjectId = mobileInvitationByObjectId;
//    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
