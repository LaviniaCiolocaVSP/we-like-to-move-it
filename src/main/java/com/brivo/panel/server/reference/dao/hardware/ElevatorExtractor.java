package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.Elevator;
import com.brivo.panel.server.reference.model.hardware.TwoFactorCredentialSettings;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Elevator Extractor
 */
@Component
public class ElevatorExtractor implements ResultSetExtractor<List<Elevator>> {
    @Override
    public List<Elevator> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<Elevator> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Integer boardNumber = rs.getInt("boardNumber");
            Integer pointAddress = rs.getInt("pointAddress");
            Integer argument = rs.getInt("argument");
            Long twoFactorScheduleId = rs.getLong("twoFactorScheduleID");
            Integer twoFactorInterval = rs.getInt("twoFactorInterval");
            Long cardRequiredScheduleId = rs.getLong("cardRequiredScheduleID");

            TwoFactorCredentialSettings twoFactor = new TwoFactorCredentialSettings();
            twoFactor.setScheduleId(twoFactorScheduleId);
            twoFactor.setInterval(twoFactorInterval);

            Elevator elevator = new Elevator();
            elevator.setDeviceId(deviceId);
            elevator.setInputBoardAddress(boardNumber);
            elevator.setInputPointAddress(pointAddress);
            elevator.setDelayTime(argument);
            elevator.setTwoFactorCredential(twoFactor);
            elevator.setCardRequiredScheduleId(cardRequiredScheduleId);

            list.add(elevator);
        }

        return list;
    }
}
