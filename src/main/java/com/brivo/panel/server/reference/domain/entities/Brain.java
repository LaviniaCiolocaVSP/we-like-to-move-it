package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Objects;

@Entity
@Table(name = "brain")
public class Brain implements Serializable {
    private long brainId;
    private long objectId;
    private long networkId;
    private long messageSpecId;
    private String electronicSerialNumber;
    private String hardwareControllerVersion;
    private String firmwareVersion;
    private String physicalAddress;
    private Short isRegistered;
    private String timeZone;
    private LocalDateTime created;
    private LocalDateTime updated;
    private long opsFlag;
    private LocalDateTime activated;
    private LocalDateTime deactivated;
    private Long accountId;
    private LocalDateTime panelChanged;
    private LocalDateTime personsChanged;
    private LocalDateTime schedulesChanged;
    private String password;
    private LocalDateTime panelChecked;
    private LocalDateTime personsChecked;
    private LocalDateTime schedulesChecked;
    private String certificate;
    private String privateKey;
    private String hash;
    private String name;
    private String note;
    private String macAddress;
    private Long antipassbackResetInterval;
    private Long logPeriod;
    private Collection<Board> boardsByObjectId;
    private BrainType brainTypeByBrainTypeId;
    private BrivoObject objectByObjectId;
    private Account accountByAccountId;
    private BrainConnectionStatus brainConnectionStatusByBrainId;
    private Collection<Device> devicesByBrainId;
    private Collection<ElevatorFloorMap> elevatorFloorMapsByObjectId;
    //    private Collection<FirmwareUpgrade> firmwareUpgradesByBrainId;
    private Collection<ProgDevicePuts> progDevicePutsByObjectId;
    private Collection<ProgIoPoints> progIoPointsByObjectId;
    private Collection<Rs485Settings> rs485SettingsByBrainId;
    //    private Collection<SwapHistory> swapHistoriesByBrainId;
//    private Collection<SwapHistory> swapHistoriesByBrainId_0;
    private Collection<Events> eventsByEventId;

    @Id
    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Column(name = "object_id", nullable = false, insertable = false, updatable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Column(name = "network_id", nullable = false)
    public long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(long networkId) {
        this.networkId = networkId;
    }

    @Column(name = "message_spec_id", nullable = false)
    public long getMessageSpecId() {
        return messageSpecId;
    }

    public void setMessageSpecId(long messageSpecId) {
        this.messageSpecId = messageSpecId;
    }

    @Column(name = "electronic_serial_number", nullable = true, length = 32)
    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public void setElectronicSerialNumber(String electronicSerialNumber) {
        this.electronicSerialNumber = electronicSerialNumber;
    }

    @Column(name = "hardware_controller_version", nullable = true, length = 10)
    public String getHardwareControllerVersion() {
        return hardwareControllerVersion;
    }

    public void setHardwareControllerVersion(String hardwareControllerVersion) {
        this.hardwareControllerVersion = hardwareControllerVersion;
    }

    @Column(name = "firmware_version", nullable = true, length = 10)
    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public void setFirmwareVersion(String firmwareVersion) {
        this.firmwareVersion = firmwareVersion;
    }

    @Column(name = "physical_address", nullable = true, length = 128)
    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public void setPhysicalAddress(String physicalAddress) {
        this.physicalAddress = physicalAddress;
    }

    @Column(name = "is_registered", nullable = false)
    public Short getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(Short isRegistered) {
        this.isRegistered = isRegistered;
    }

    @Column(name = "time_zone", nullable = true, length = 32)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public LocalDateTime getUpdated() {
        return updated;
    }

    public void setUpdated(LocalDateTime updated) {
        this.updated = updated;
    }

    @Column(name = "ops_flag", nullable = false)
    public long getOpsFlag() {
        return opsFlag;
    }

    public void setOpsFlag(long opsFlag) {
        this.opsFlag = opsFlag;
    }

    @Column(name = "activated", nullable = true)
    public LocalDateTime getActivated() {
        return activated;
    }

    public void setActivated(LocalDateTime activated) {
        this.activated = activated;
    }

    @Column(name = "deactivated", nullable = true)
    public LocalDateTime getDeactivated() {
        return deactivated;
    }

    public void setDeactivated(LocalDateTime deactivated) {
        this.deactivated = deactivated;
    }

    /*
    @Column(name = "object_id", nullable = false)
    public long getDeviceId() {
        return objectId;
    }

    public void setDeviceId(long objectId) {
        this.objectId = objectId;
    }
    */

    @Column(name = "account_id", nullable = true, insertable = false, updatable = false)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "panel_changed", nullable = true)
    public LocalDateTime getPanelChanged() {
        return panelChanged;
    }

    public void setPanelChanged(LocalDateTime panelChanged) {
        this.panelChanged = panelChanged;
    }

    @Column(name = "persons_changed", nullable = true)
    public LocalDateTime getPersonsChanged() {
        return personsChanged;
    }

    public void setPersonsChanged(LocalDateTime personsChanged) {
        this.personsChanged = personsChanged;
    }

    @Column(name = "schedules_changed", nullable = true)
    public LocalDateTime getSchedulesChanged() {
        return schedulesChanged;
    }

    public void setSchedulesChanged(LocalDateTime schedulesChanged) {
        this.schedulesChanged = schedulesChanged;
    }

    @Column(name = "password", nullable = true, length = 30)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Column(name = "panel_checked", nullable = true)
    public LocalDateTime getPanelChecked() {
        return panelChecked;
    }

    public void setPanelChecked(LocalDateTime panelChecked) {
        this.panelChecked = panelChecked;
    }

    @Column(name = "persons_checked", nullable = true)
    public LocalDateTime getPersonsChecked() {
        return personsChecked;
    }

    public void setPersonsChecked(LocalDateTime personsChecked) {
        this.personsChecked = personsChecked;
    }

    @Column(name = "schedules_checked", nullable = true)
    public LocalDateTime getSchedulesChecked() {
        return schedulesChecked;
    }

    public void setSchedulesChecked(LocalDateTime schedulesChecked) {
        this.schedulesChecked = schedulesChecked;
    }

    @Column(name = "certificate", nullable = true, length = 3500)
    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    @Basic
    @Column(name = "private_key", nullable = true, length = 3500)
    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    @Column(name = "hash", nullable = true, length = 50)
    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "note", nullable = true, length = 60)
    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Column(name = "mac_address", nullable = true, length = 12)
    public String getMacAddress() {
        return macAddress;
    }

    public void setMacAddress(String macAddress) {
        this.macAddress = macAddress;
    }

    @Column(name = "antipassback_reset_interval", nullable = true)
    public Long getAntipassbackResetInterval() {
        return antipassbackResetInterval;
    }

    public void setAntipassbackResetInterval(Long antipassbackResetInterval) {
        this.antipassbackResetInterval = antipassbackResetInterval;
    }

    @Column(name = "log_period", nullable = true)
    public Long getLogPeriod() {
        return logPeriod;
    }

    public void setLogPeriod(Long logPeriod) {
        this.logPeriod = logPeriod;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Brain brain = (Brain) o;
        return brainId == brain.brainId &&
                networkId == brain.networkId &&
                messageSpecId == brain.messageSpecId &&
                Objects.equals(electronicSerialNumber, brain.electronicSerialNumber) &&
                Objects.equals(hardwareControllerVersion, brain.hardwareControllerVersion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(brainId, networkId, messageSpecId, electronicSerialNumber, hardwareControllerVersion);
    }

    @OneToMany(mappedBy = "brainByPanelOid", fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = Board.class)
    public Collection<Board> getBoardsByObjectId() {
        return boardsByObjectId;
    }

    public void setBoardsByObjectId(Collection<Board> boardsByObjectId) {
        this.boardsByObjectId = boardsByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = BrainType.class)
    @JoinColumn(name = "brain_type_id", referencedColumnName = "brain_type_id")
    public BrainType getBrainTypeByBrainTypeId() {
        return brainTypeByBrainTypeId;
    }

    public void setBrainTypeByBrainTypeId(BrainType brainTypeByBrainTypeId) {
        this.brainTypeByBrainTypeId = brainTypeByBrainTypeId;
    }

    @OneToOne(fetch = FetchType.LAZY, targetEntity = BrivoObject.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false)
    public BrivoObject getObjectByObjectId() {
        return objectByObjectId;
    }

    public void setObjectByObjectId(BrivoObject objectByObjectId) {
        this.objectByObjectId = objectByObjectId;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id")
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

    @OneToOne(mappedBy = "brainByBrainId", fetch = FetchType.LAZY)
    public BrainConnectionStatus getBrainConnectionStatusByBrainId() {
        return brainConnectionStatusByBrainId;
    }

    public void setBrainConnectionStatusByBrainId(BrainConnectionStatus brainConnectionStatusByBrainId) {
        this.brainConnectionStatusByBrainId = brainConnectionStatusByBrainId;
    }

    @OneToMany(mappedBy = "brainByBrainId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Device> getDevicesByBrainId() {
        return devicesByBrainId;
    }

    public void setDevicesByBrainId(Collection<Device> devicesByBrainId) {
        this.devicesByBrainId = devicesByBrainId;
    }

    @OneToMany(mappedBy = "brainByObjectId", fetch = FetchType.LAZY, targetEntity = ElevatorFloorMap.class)
    public Collection<ElevatorFloorMap> getElevatorFloorMapsByObjectId() {
        return elevatorFloorMapsByObjectId;
    }

    public void setElevatorFloorMapsByObjectId(Collection<ElevatorFloorMap> elevatorFloorMapsByObjectId) {
        this.elevatorFloorMapsByObjectId = elevatorFloorMapsByObjectId;
    }

    /*
    @OneToMany(mappedBy = "brainByBrainId", fetch = FetchType.LAZY)
    public Collection<FirmwareUpgrade> getFirmwareUpgradesByBrainId() {
        return firmwareUpgradesByBrainId;
    }

    public void setFirmwareUpgradesByBrainId(Collection<FirmwareUpgrade> firmwareUpgradesByBrainId) {
        this.firmwareUpgradesByBrainId = firmwareUpgradesByBrainId;
    }
    */

    @OneToMany(mappedBy = "brainByObjectId", fetch = FetchType.LAZY, targetEntity = ProgDevicePuts.class)
    public Collection<ProgDevicePuts> getProgDevicePutsByObjectId() {
        return progDevicePutsByObjectId;
    }

    public void setProgDevicePutsByObjectId(Collection<ProgDevicePuts> progDevicePutsByObjectId) {
        this.progDevicePutsByObjectId = progDevicePutsByObjectId;
    }

    @OneToMany(mappedBy = "brainByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL, targetEntity = ProgIoPoints.class)
    public Collection<ProgIoPoints> getProgIoPointsByObjectId() {
        return progIoPointsByObjectId;
    }

    public void setProgIoPointsByObjectId(Collection<ProgIoPoints> progIoPointsByObjectId) {
        this.progIoPointsByObjectId = progIoPointsByObjectId;
    }

    @OneToMany(mappedBy = "brainByBrainId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Rs485Settings> getRs485SettingsByBrainId() {
        return rs485SettingsByBrainId;
    }

    public void setRs485SettingsByBrainId(Collection<Rs485Settings> rs485SettingsByBrainId) {
        this.rs485SettingsByBrainId = rs485SettingsByBrainId;
    }

    /*
    @OneToMany(mappedBy = "brainByBrainId", fetch = FetchType.LAZY)
    public Collection<SwapHistory> getSwapHistoriesByBrainId() {
        return swapHistoriesByBrainId;
    }

    public void setSwapHistoriesByBrainId(Collection<SwapHistory> swapHistoriesByBrainId) {
        this.swapHistoriesByBrainId = swapHistoriesByBrainId;
    }

    @OneToMany(mappedBy = "brainByReplacesBrainId", fetch = FetchType.LAZY)
    public Collection<SwapHistory> getSwapHistoriesByBrainId_0() {
        return swapHistoriesByBrainId_0;
    }

    public void setSwapHistoriesByBrainId_0(Collection<SwapHistory> swapHistoriesByBrainId_0) {
        this.swapHistoriesByBrainId_0 = swapHistoriesByBrainId_0;
    }
    */

    @OneToMany(mappedBy = "brainByObjectId", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Events> getEventsByEventId() {
        return eventsByEventId;
    }

    public void setEventsByEventId(final Collection<Events> eventsByEventId) {
        this.eventsByEventId = eventsByEventId;
    }
}
