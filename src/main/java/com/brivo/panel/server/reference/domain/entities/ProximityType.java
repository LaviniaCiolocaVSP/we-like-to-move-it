package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "proximity_type", schema = "brivo20", catalog = "onair")
public class ProximityType {
    private long proximityTypeId;
    private String name;
    private String description;
    private Collection<Proximity> proximitiesByProximityTypeId;

    @Id
    @Column(name = "proximity_type_id", nullable = false)
    public long getProximityTypeId() {
        return proximityTypeId;
    }

    public void setProximityTypeId(long proximityTypeId) {
        this.proximityTypeId = proximityTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProximityType that = (ProximityType) o;

        if (proximityTypeId != that.proximityTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (proximityTypeId ^ (proximityTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "proximityTypeByProximityTypeId")
    public Collection<Proximity> getProximitiesByProximityTypeId() {
        return proximitiesByProximityTypeId;
    }

    public void setProximitiesByProximityTypeId(Collection<Proximity> proximitiesByProximityTypeId) {
        this.proximitiesByProximityTypeId = proximitiesByProximityTypeId;
    }
}
