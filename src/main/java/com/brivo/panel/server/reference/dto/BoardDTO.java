package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.Collection;

public class BoardDTO implements Serializable {
    private final String boardLocation;
    private final Long boardTypeId;
    private final Short boardNumber;
    private final Long objectId;
    private final Collection<BoardPropertyDTO> boardProperties;
    
    @JsonCreator
    public BoardDTO(@JsonProperty("boardLocation") final String boardLocation,
                    @JsonProperty("boardTypeId") final Long boardTypeId,
                    @JsonProperty("boardNumber") final Short boardNumber,
                    @JsonProperty("objectId") final Long objectId,
                    @JsonProperty("boardProperties") final Collection<BoardPropertyDTO> boardProperties) {
        this.boardLocation = boardLocation;
        this.boardTypeId = boardTypeId;
        this.boardNumber = boardNumber;
        this.objectId = objectId;
        this.boardProperties = boardProperties;
    }

    public String getBoardLocation() {
        return boardLocation;
    }

    public Long getBoardTypeId() {
        return boardTypeId;
    }

    public Short getBoardNumber() {
        return boardNumber;
    }

    public Long getObjectId() {
        return objectId;
    }

    public Collection<BoardPropertyDTO> getBoardProperties() {
        return boardProperties;
    }

    public static class BoardPropertyDTO {
        private final Long valueTypeId;
        private final String key;
        private final String value;

        @JsonCreator
        public BoardPropertyDTO(@JsonProperty("valueTypeId") final Long valueTypeId,
                                @JsonProperty("key") final String key,
                                @JsonProperty("value") final String value) {
            this.valueTypeId = valueTypeId;
            this.key = key;
            this.value = value;
        }

        public Long getValueTypeId() {
            return valueTypeId;
        }

        public String getKey() {
            return key;
        }

        public String getValue() {
            return value;
        }

    }
}
