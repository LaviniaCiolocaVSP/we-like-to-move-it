package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.auth.PanelPrincipal;
import com.brivo.panel.server.reference.domain.repository.EventRepository;
import com.brivo.panel.server.reference.model.event.PanelEventData;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.service.EventService;
import com.brivo.panel.server.reference.service.PanelService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/paneldata/events")
public class PanelEventsController {
    private static final Logger LOGGER = LoggerFactory.getLogger(PanelEventsController.class);
    @Autowired
    PanelService panelService;

    @Autowired
    private EventService eventService;

    @Autowired
    private EventRepository eventRepository;

    /**
     * Post Events data.
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    // @PreAuthorize("principal.panel.isRegistered")
    public void sendEvents(@AuthenticationPrincipal PanelPrincipal panelPrincipal,
                           HttpServletRequest request,
                           @RequestBody PanelEventData events) {
        Panel panel = panelPrincipal.getPanel();
        // LOGGER.info("Events from the panel: {}", events.toString());

        // Panel panel = PanelBuilder.getMockPanel();

        logRequest(panel, events);

        eventService.handleEvents(panel, events, eventRepository);

//        eventRepository.findAll().forEach(event -> LOGGER.info("Event {}", event));
    }

    private void logRequest(Panel panel, PanelEventData events) {
        LOGGER.info("Panel {} sending event data", panel.getElectronicSerialNumber());

        LOGGER.info("Panel {} event data: {}", panel.getElectronicSerialNumber(), events);
    }
}
