package com.brivo.panel.server.reference.dto.ui;

import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.dto.ProgIoPointDTO;

import java.io.Serializable;
import java.util.Collection;

public class UiBoardDTO implements Serializable {
    private String boardLocation;
    private String boardType;
    private Long boardTypeId;
    private Short boardNumber;
    private Long objectId;
    private Long panelOid;
    private String boardDescription;
    
    private Collection<ProgIoPointDTO> progIoPoints;
    
    public UiBoardDTO() {
    }
    
    public UiBoardDTO(String boardLocation, String boardType, Long boardTypeId, Short boardNumber, Long objectId,
                      Long panelOid) {
        this.boardLocation = boardLocation;
        this.boardType = boardType;
        this.boardTypeId = boardTypeId;
        this.boardNumber = boardNumber;
        this.objectId = objectId;
        this.panelOid = panelOid;
        this.boardDescription = boardType + " " + boardNumber + ": " + boardLocation;
    }
    
    public String getBoardLocation() {
        return boardLocation;
    }
    
    public String getBoardType() {
        return boardType;
    }
    
    public Long getBoardTypeId() {
        return boardTypeId;
    }
    
    public Short getBoardNumber() {
        return boardNumber;
    }
    
    public Long getObjectId() {
        return objectId;
    }
    
    public Long getPanelOid() {
        return panelOid;
    }
    
    public String getBoardDescription() {
        return boardDescription;
    }
    
    public Collection<ProgIoPointDTO> getProgIoPoints() {
        return progIoPoints;
    }
    
    public void setProgIoPoints(Collection<ProgIoPointDTO> progIoPoints) {
        this.progIoPoints = progIoPoints;
    }
    
    public static class UiNodeDTO {
        private final int nodeNumber;
        private final String nodeDescription;
        
        public UiNodeDTO(int nodeNumber, String nodeDescription) {
            this.nodeNumber = nodeNumber;
            this.nodeDescription = nodeDescription;
        }
        
        public int getNodeNumber() {
            return nodeNumber;
        }
        
        public String getNodeDescription() {
            return nodeDescription;
        }
    }
}
