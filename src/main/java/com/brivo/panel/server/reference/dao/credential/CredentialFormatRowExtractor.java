package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.domain.entities.credential.CredentialEncoding;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialField;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldType;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFormat;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CredentialField Extractor.
 */
@Component
public class CredentialFormatRowExtractor implements ResultSetExtractor<List<CredentialFormat>> {
    
    @Override
    public List<CredentialFormat> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<CredentialFormat> list = new ArrayList<>();
        
        while (rs.next()) {
            CredentialFormat field = new CredentialFormat();
            field.setCredentialFormatId(rs.getLong("access_credential_type_id"));
            field.setName(rs.getString("name"));
            field.setDescription(rs.getString("description"));
            
            
            field.setNumBits(rs.getInt("num_bits"));
            
            Integer supportedBy4000Int = rs.getInt("supported_by_4000");
            Boolean supportedBy4000 = false;
            if (supportedBy4000Int != null && supportedBy4000Int == 1) {
                supportedBy4000 = true;
            }
            field.setSupports4k(supportedBy4000);
            
            field.setEncoding(CredentialEncoding.byId(rs.getInt("credential_encoding_id")));
            
            list.add(field);
        }
        
        return list;
    }
    
}
