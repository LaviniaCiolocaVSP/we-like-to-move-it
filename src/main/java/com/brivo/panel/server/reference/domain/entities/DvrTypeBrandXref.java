package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dvr_type_brand_xref", schema = "brivo20", catalog = "onair")
@IdClass(DvrTypeBrandXrefPK.class)
public class DvrTypeBrandXref {
    private long dvrTypeId;
    private long brandId;
    private DvrType dvrTypeByDvrTypeId;

    @Id
    @Column(name = "dvr_type_id", nullable = false)
    public long getDvrTypeId() {
        return dvrTypeId;
    }

    public void setDvrTypeId(long dvrTypeId) {
        this.dvrTypeId = dvrTypeId;
    }

    @Id
    @Column(name = "brand_id", nullable = false)
    public long getBrandId() {
        return brandId;
    }

    public void setBrandId(long brandId) {
        this.brandId = brandId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DvrTypeBrandXref that = (DvrTypeBrandXref) o;

        if (dvrTypeId != that.dvrTypeId) {
            return false;
        }
        return brandId == that.brandId;
    }

    @Override
    public int hashCode() {
        int result = (int) (dvrTypeId ^ (dvrTypeId >>> 32));
        result = 31 * result + (int) (brandId ^ (brandId >>> 32));
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "dvr_type_id", referencedColumnName = "dvr_type_id", nullable = false, insertable = false, updatable = false)
    public DvrType getDvrTypeByDvrTypeId() {
        return dvrTypeByDvrTypeId;
    }

    public void setDvrTypeByDvrTypeId(DvrType dvrTypeByDvrTypeId) {
        this.dvrTypeByDvrTypeId = dvrTypeByDvrTypeId;
    }
}
