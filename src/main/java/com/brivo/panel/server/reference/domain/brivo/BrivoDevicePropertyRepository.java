package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IDevicePropertyRepository;
import com.brivo.panel.server.reference.domain.entities.DeviceProperty;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrivoDevicePropertyRepository extends IDevicePropertyRepository {

    Optional<DeviceProperty> findByDeviceId(@Param("deviceId") Long deviceId, @Param("id") String id);
}
