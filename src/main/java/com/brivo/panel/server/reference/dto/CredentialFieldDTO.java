package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Objects;

public class CredentialFieldDTO {
    private Long credentialFieldId;
    private Long accessCredentialTypeId;
    private Long credentialFieldTypeId;
    private String format;
    private String name;
    private Short forReference;
    private Short display;
    private Long ordering;

    public CredentialFieldDTO() {

    }

    @JsonCreator
    public CredentialFieldDTO(@JsonProperty("credentialFieldId") final Long credentialFieldId,
                              @JsonProperty("accessCredentialTypeId") final Long accessCredentialTypeId,
                              @JsonProperty("credentialFieldTypeId") final Long credentialFieldTypeId,
                              @JsonProperty("format") final String format,
                              @JsonProperty("name") final String name,
                              @JsonProperty("forReference") final Short forReference,
                              @JsonProperty("display") final Short display,
                              @JsonProperty("ordering") final Long ordering) {
        this.credentialFieldId = credentialFieldId;
        this.accessCredentialTypeId = accessCredentialTypeId;
        this.credentialFieldTypeId = credentialFieldTypeId;
        this.format = format;
        this.name = name;
        this.forReference = forReference;
        this.display = display;
        this.ordering = ordering;
    }

    public Long getCredentialFieldId() {
        return credentialFieldId;
    }

    public Long getAccessCredentialTypeId() {
        return accessCredentialTypeId;
    }

    public Long getCredentialFieldTypeId() {
        return credentialFieldTypeId;
    }

    public String getFormat() {
        return format;
    }

    public String getName() {
        return name;
    }

    public Short getForReference() {
        return forReference;
    }

    public Short getDisplay() {
        return display;
    }

    public Long getOrdering() {
        return ordering;
    }

    public void setCredentialFieldId(Long credentialFieldId) {
        this.credentialFieldId = credentialFieldId;
    }

    public void setAccessCredentialTypeId(Long accessCredentialTypeId) {
        this.accessCredentialTypeId = accessCredentialTypeId;
    }

    public void setCredentialFieldTypeId(Long credentialFieldTypeId) {
        this.credentialFieldTypeId = credentialFieldTypeId;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setForReference(Short forReference) {
        this.forReference = forReference;
    }

    public void setDisplay(Short display) {
        this.display = display;
    }

    public void setOrdering(Long ordering) {
        this.ordering = ordering;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        CredentialFieldDTO that = (CredentialFieldDTO) o;
        return Objects.equals(credentialFieldId, that.credentialFieldId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(credentialFieldId);
    }
}
