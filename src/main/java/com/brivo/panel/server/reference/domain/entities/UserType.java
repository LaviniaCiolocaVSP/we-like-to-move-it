package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "user_type", schema = "brivo20", catalog = "onair")
public class UserType {
    private long userTypeId;
    private String name;
    private String description;
    private Collection<AccountApiPermission> accountApiPermissionsByUserTypeId;
    private Collection<UserTypeRole> userTypeRolesByUserTypeId;
    private Collection<Users> usersByUserTypeId;

    @Id
    @Column(name = "user_type_id", nullable = false)
    public long getUserTypeId() {
        return userTypeId;
    }

    public void setUserTypeId(long userTypeId) {
        this.userTypeId = userTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserType userType = (UserType) o;

        if (userTypeId != userType.userTypeId) {
            return false;
        }
        if (name != null ? !name.equals(userType.name) : userType.name != null) {
            return false;
        }
        return description != null ? description.equals(userType.description) : userType.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userTypeId ^ (userTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "userTypeByUserTypeId")
    public Collection<AccountApiPermission> getAccountApiPermissionsByUserTypeId() {
        return accountApiPermissionsByUserTypeId;
    }

    public void setAccountApiPermissionsByUserTypeId(Collection<AccountApiPermission> accountApiPermissionsByUserTypeId) {
        this.accountApiPermissionsByUserTypeId = accountApiPermissionsByUserTypeId;
    }

    @OneToMany(mappedBy = "userTypeByUserTypeId")
    public Collection<UserTypeRole> getUserTypeRolesByUserTypeId() {
        return userTypeRolesByUserTypeId;
    }

    public void setUserTypeRolesByUserTypeId(Collection<UserTypeRole> userTypeRolesByUserTypeId) {
        this.userTypeRolesByUserTypeId = userTypeRolesByUserTypeId;
    }

    @OneToMany(mappedBy = "userTypeByUserTypeId", cascade = CascadeType.ALL)
    public Collection<Users> getUsersByUserTypeId() {
        return usersByUserTypeId;
    }

    public void setUsersByUserTypeId(Collection<Users> usersByUserTypeId) {
        this.usersByUserTypeId = usersByUserTypeId;
    }
}
