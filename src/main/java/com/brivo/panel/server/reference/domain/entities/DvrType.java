package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "dvr_type", schema = "brivo20", catalog = "onair")
public class DvrType {
    private long dvrTypeId;
    private String className;
    private String modelName;
    private String vendorName;
    private Collection<Dvr> dvrsByDvrTypeId;
    private Collection<DvrTypeBrandXref> dvrTypeBrandXrefsByDvrTypeId;

    @Id
    @Column(name = "dvr_type_id", nullable = false)
    public long getDvrTypeId() {
        return dvrTypeId;
    }

    public void setDvrTypeId(long dvrTypeId) {
        this.dvrTypeId = dvrTypeId;
    }

    @Basic
    @Column(name = "class_name", nullable = true, length = 2048)
    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    @Basic
    @Column(name = "model_name", nullable = true, length = 64)
    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    @Basic
    @Column(name = "vendor_name", nullable = true, length = 64)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        DvrType dvrType = (DvrType) o;

        if (dvrTypeId != dvrType.dvrTypeId) {
            return false;
        }
        if (className != null ? !className.equals(dvrType.className) : dvrType.className != null) {
            return false;
        }
        if (modelName != null ? !modelName.equals(dvrType.modelName) : dvrType.modelName != null) {
            return false;
        }
        return vendorName != null ? vendorName.equals(dvrType.vendorName) : dvrType.vendorName == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (dvrTypeId ^ (dvrTypeId >>> 32));
        result = 31 * result + (className != null ? className.hashCode() : 0);
        result = 31 * result + (modelName != null ? modelName.hashCode() : 0);
        result = 31 * result + (vendorName != null ? vendorName.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "dvrTypeByDvrTypeId")
    public Collection<Dvr> getDvrsByDvrTypeId() {
        return dvrsByDvrTypeId;
    }

    public void setDvrsByDvrTypeId(Collection<Dvr> dvrsByDvrTypeId) {
        this.dvrsByDvrTypeId = dvrsByDvrTypeId;
    }

    @OneToMany(mappedBy = "dvrTypeByDvrTypeId")
    public Collection<DvrTypeBrandXref> getDvrTypeBrandXrefsByDvrTypeId() {
        return dvrTypeBrandXrefsByDvrTypeId;
    }

    public void setDvrTypeBrandXrefsByDvrTypeId(Collection<DvrTypeBrandXref> dvrTypeBrandXrefsByDvrTypeId) {
        this.dvrTypeBrandXrefsByDvrTypeId = dvrTypeBrandXrefsByDvrTypeId;
    }
}
