package com.brivo.panel.server.reference.model.credential;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class CardMask {
    private Integer numberBits;
    private String mask;

    public CardMask() {
    }

    public CardMask(Integer numberBits, String mask) {
        this.numberBits = numberBits;
        this.mask = mask;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getNumberBits() {
        return numberBits;
    }

    public void setNumberBits(Integer numberBits) {
        this.numberBits = numberBits;
    }

    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
    }
}
