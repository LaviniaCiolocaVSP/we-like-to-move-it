package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class IOPoint {
    private Integer address;
    private Boolean eolWiring;
    private Integer defaultState;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getAddress() {
        return address;
    }

    public void setAddress(Integer address) {
        this.address = address;
    }

    public Boolean getEolWiring() {
        return eolWiring;
    }

    public void setEolWiring(Boolean eolWiring) {
        this.eolWiring = eolWiring;
    }

    public Integer getDefaultState() {
        return defaultState;
    }

    public void setDefaultState(Integer defaultState) {
        this.defaultState = defaultState;
    }
}
