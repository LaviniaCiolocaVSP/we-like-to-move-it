package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class FlushDataMessage extends DownstreamMessage {
    private final String panelSerialNumber;
    private final Long panelObjectId;

    public FlushDataMessage() {
        super(DownstreamMessageType.Flush_Data_Channel);
        this.panelObjectId = -1L;
        this.panelSerialNumber = "N/A";
    }

    public FlushDataMessage(String panelSerialNumber, Long panelObjectId) {
        super(DownstreamMessageType.Flush_Data_Channel);
        this.panelObjectId = panelObjectId;
        this.panelSerialNumber = panelSerialNumber;
    }

    public FlushDataMessage(FlushDataMessage msg) {
        super(DownstreamMessageType.Flush_Data_Channel);
        this.panelObjectId = msg.getPanelObjectId();
        this.panelSerialNumber = msg.getPanelSerialNumber();
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public Long getPanelObjectId() {
        return panelObjectId;
    }

    public String toString() {
        return "flushDataRequest : { panelObjectId: " + panelObjectId + ", panelSerialNumber: " + panelSerialNumber + "}";
    }
}
