package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_text_item", schema = "brivo20", catalog = "onair")
public class BadgeTextItem {
    private long badgeTextItemId;
    private long badgeItemId;
    private long badgeItemFieldTypeId;
    private Long customFieldId;
    private String value;
    private String fontFamily;
    private Long fontSize;
    private String fontColor;
    private String fontStyle;
    private String fontWeight;
    private String hAlign;
    private String vAlign;
    private Short enableWrapping;
    private Short enableScaling;
    private BadgeItem badgeItemByBadgeItemId;
    private BadgeItemFieldType badgeItemFieldTypeByBadgeItemFieldTypeId;
    private Collection<CustomFieldDefinition> badgeTextItemByCustomFieldId;

    @Id
    @Column(name = "badge_text_item_id", nullable = false)
    public long getBadgeTextItemId() {
        return badgeTextItemId;
    }

    public void setBadgeTextItemId(long badgeTextItemId) {
        this.badgeTextItemId = badgeTextItemId;
    }

    @Basic
    @Column(name = "badge_item_id", nullable = false)
    public long getBadgeItemId() {
        return badgeItemId;
    }

    public void setBadgeItemId(long badgeItemId) {
        this.badgeItemId = badgeItemId;
    }

    @Basic
    @Column(name = "badge_item_field_type_id", nullable = false)
    public long getBadgeItemFieldTypeId() {
        return badgeItemFieldTypeId;
    }

    public void setBadgeItemFieldTypeId(long badgeItemFieldTypeId) {
        this.badgeItemFieldTypeId = badgeItemFieldTypeId;
    }

    @Basic
    @Column(name = "custom_field_id", nullable = true)
    public Long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(Long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Basic
    @Column(name = "value", nullable = true, length = 128)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "font_family", nullable = true, length = 32)
    public String getFontFamily() {
        return fontFamily;
    }

    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    @Basic
    @Column(name = "font_size", nullable = true)
    public Long getFontSize() {
        return fontSize;
    }

    public void setFontSize(Long fontSize) {
        this.fontSize = fontSize;
    }

    @Basic
    @Column(name = "font_color", nullable = true, length = 32)
    public String getFontColor() {
        return fontColor;
    }

    public void setFontColor(String fontColor) {
        this.fontColor = fontColor;
    }

    @Basic
    @Column(name = "font_style", nullable = true, length = 32)
    public String getFontStyle() {
        return fontStyle;
    }

    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    @Basic
    @Column(name = "font_weight", nullable = true, length = 32)
    public String getFontWeight() {
        return fontWeight;
    }

    public void setFontWeight(String fontWeight) {
        this.fontWeight = fontWeight;
    }

    @Basic
    @Column(name = "h_align", nullable = true, length = 32)
    public String gethAlign() {
        return hAlign;
    }

    public void sethAlign(String hAlign) {
        this.hAlign = hAlign;
    }

    @Basic
    @Column(name = "v_align", nullable = true, length = 32)
    public String getvAlign() {
        return vAlign;
    }

    public void setvAlign(String vAlign) {
        this.vAlign = vAlign;
    }

    @Basic
    @Column(name = "enable_wrapping", nullable = true)
    public Short getEnableWrapping() {
        return enableWrapping;
    }

    public void setEnableWrapping(Short enableWrapping) {
        this.enableWrapping = enableWrapping;
    }

    @Basic
    @Column(name = "enable_scaling", nullable = true)
    public Short getEnableScaling() {
        return enableScaling;
    }

    public void setEnableScaling(Short enableScaling) {
        this.enableScaling = enableScaling;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeTextItem that = (BadgeTextItem) o;

        if (badgeTextItemId != that.badgeTextItemId) {
            return false;
        }
        if (badgeItemId != that.badgeItemId) {
            return false;
        }
        if (badgeItemFieldTypeId != that.badgeItemFieldTypeId) {
            return false;
        }
        if (customFieldId != null ? !customFieldId.equals(that.customFieldId) : that.customFieldId != null) {
            return false;
        }
        if (value != null ? !value.equals(that.value) : that.value != null) {
            return false;
        }
        if (fontFamily != null ? !fontFamily.equals(that.fontFamily) : that.fontFamily != null) {
            return false;
        }
        if (fontSize != null ? !fontSize.equals(that.fontSize) : that.fontSize != null) {
            return false;
        }
        if (fontColor != null ? !fontColor.equals(that.fontColor) : that.fontColor != null) {
            return false;
        }
        if (fontStyle != null ? !fontStyle.equals(that.fontStyle) : that.fontStyle != null) {
            return false;
        }
        if (fontWeight != null ? !fontWeight.equals(that.fontWeight) : that.fontWeight != null) {
            return false;
        }
        if (hAlign != null ? !hAlign.equals(that.hAlign) : that.hAlign != null) {
            return false;
        }
        if (vAlign != null ? !vAlign.equals(that.vAlign) : that.vAlign != null) {
            return false;
        }
        if (enableWrapping != null ? !enableWrapping.equals(that.enableWrapping) : that.enableWrapping != null) {
            return false;
        }
        return enableScaling != null ? enableScaling.equals(that.enableScaling) : that.enableScaling == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeTextItemId ^ (badgeTextItemId >>> 32));
        result = 31 * result + (int) (badgeItemId ^ (badgeItemId >>> 32));
        result = 31 * result + (int) (badgeItemFieldTypeId ^ (badgeItemFieldTypeId >>> 32));
        result = 31 * result + (customFieldId != null ? customFieldId.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (fontFamily != null ? fontFamily.hashCode() : 0);
        result = 31 * result + (fontSize != null ? fontSize.hashCode() : 0);
        result = 31 * result + (fontColor != null ? fontColor.hashCode() : 0);
        result = 31 * result + (fontStyle != null ? fontStyle.hashCode() : 0);
        result = 31 * result + (fontWeight != null ? fontWeight.hashCode() : 0);
        result = 31 * result + (hAlign != null ? hAlign.hashCode() : 0);
        result = 31 * result + (vAlign != null ? vAlign.hashCode() : 0);
        result = 31 * result + (enableWrapping != null ? enableWrapping.hashCode() : 0);
        result = 31 * result + (enableScaling != null ? enableScaling.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_id", referencedColumnName = "badge_item_id", nullable = false, insertable = false, updatable = false)
    public BadgeItem getBadgeItemByBadgeItemId() {
        return badgeItemByBadgeItemId;
    }

    public void setBadgeItemByBadgeItemId(BadgeItem badgeItemByBadgeItemId) {
        this.badgeItemByBadgeItemId = badgeItemByBadgeItemId;
    }

    @ManyToOne
    @JoinColumn(name = "badge_item_field_type_id", referencedColumnName = "badge_item_field_type_id", nullable = false,
            insertable = false, updatable = false)
    public BadgeItemFieldType getBadgeItemFieldTypeByBadgeItemFieldTypeId() {
        return badgeItemFieldTypeByBadgeItemFieldTypeId;
    }

    public void setBadgeItemFieldTypeByBadgeItemFieldTypeId(BadgeItemFieldType badgeItemFieldTypeByBadgeItemFieldTypeId) {
        this.badgeItemFieldTypeByBadgeItemFieldTypeId = badgeItemFieldTypeByBadgeItemFieldTypeId;
    }

    @OneToMany(mappedBy = "badgeTextItemByCustomFieldId")
    public Collection<CustomFieldDefinition> getBadgeTextItemByCustomFieldId() {
        return badgeTextItemByCustomFieldId;
    }

    public void setBadgeTextItemByCustomFieldId(Collection<CustomFieldDefinition> badgeTextItemByCustomFieldId) {
        this.badgeTextItemByCustomFieldId = badgeTextItemByCustomFieldId;
    }
}
