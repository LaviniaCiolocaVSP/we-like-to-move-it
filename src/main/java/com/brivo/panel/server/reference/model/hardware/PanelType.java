package com.brivo.panel.server.reference.model.hardware;


import java.io.Serializable;
import java.util.Objects;
import java.util.stream.Stream;

public enum PanelType {
    
    B_6000(28, "6000"),
    
    B_300(33, "300 2-Door"),
    
    NONE(0, "None"); //None means the type is unknown.
    
    
    private final int panelTypeId;
    private final String description;
    
    PanelType(int panelTypeId, String description) {
        this.panelTypeId = panelTypeId;
        this.description = description;
    }
    
    public static PanelType getPanelType(int typeId) {
        for (PanelType type : PanelType.values()) {
            if (type.getPanelTypeId() == typeId) {
                return type;
            }
        }
        
        return NONE;
    }
    
    public int getPanelTypeId() {
        return panelTypeId;
    }
    
    public String getDescription() {
        return description;
    }
    
    public FirmwareProtocol getFirmwareProtocol() {
        return FirmwareProtocol.SIX_THOUSAND;
    }
    
    public int getRs485Ports() {
        if (this == B_6000) {
            return 2;
        } else if (this == B_300) {
            return 1;
        }
        return 0;
    }
    
    public enum FirmwareProtocol implements Serializable {
        NONE(),
        FOUR(4),
        FIVE(5),
        NINE(9),
        SIX_THOUSAND(6000);
        
        private final Integer protocolVal;
        
        FirmwareProtocol() {
            protocolVal = null;
        }
        
        FirmwareProtocol(int protocolVal) {
            this.protocolVal = protocolVal;
        }
        
        public static FirmwareProtocol getProtocolByVal(Integer protocolVal) {
            return Stream.of(FirmwareProtocol.values())
                         .filter(protocol -> Objects.equals(protocol.protocolVal, protocolVal))
                         .findFirst().orElse(NONE);
        }
        
        public Integer getProtocolVal() {
            return protocolVal;
        }
    }
}
