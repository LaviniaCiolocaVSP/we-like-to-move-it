package com.brivo.panel.server.reference.dao.schedule;

import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.schedule.Holiday;
import com.brivo.panel.server.reference.model.schedule.Schedule;
import com.brivo.panel.server.reference.model.schedule.ScheduleBlock;
import com.brivo.panel.server.reference.model.schedule.ScheduleException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ScheduleDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleDao.class);

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private HolidayRowMapper holidayRowMapper;

    @Autowired
    private ScheduleBlockExtractor scheduleBlockExtractor;

    @Autowired
    private ScheduleRowMapper scheduleRowMapper;

    @Autowired
    private ScheduleExceptionExtractor scheduleExceptionExtractor;

    /**
     * Get schedules for the account
     *
     * @param panel
     * @return List of Schedules
     */
    public List<Schedule> getAccountSchedules(Panel panel) {
        Map<String, Long> params = Collections.singletonMap("accountId", panel.getAccountId());

        String query = ScheduleQuery.SELECT_ACCOUNT_SCHEDULES.getQuery();

        return template.query(query, params, scheduleRowMapper);
    }


    public List<Holiday> getAccountHolidays(Panel panel) {
        Map<String, Long> params = Collections.singletonMap("accountId", panel.getAccountId());

        String query = ScheduleQuery.SELECT_ACCOUNT_HOLIDAYS.getQuery();

        return template.query(query, params, holidayRowMapper);
    }

    /**
     * Get schedule blocks.
     * <p>
     * Run select_schedule_blocks.sql to load schedule data.
     *
     * @param schedule
     * @return
     */
    public List<ScheduleBlock> getScheduleBlocks(Schedule schedule) {
        LOGGER.trace(">>> GetScheduleBlocks");

        String query = ScheduleQuery.SELECT_SCHEDULE_BLOCKS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", schedule.getScheduleId());

        LOGGER.trace("Query [{}]:\n Params [{}] \n{}", ScheduleQuery.SELECT_SCHEDULE_BLOCKS.name(), paramMap, query);

        List<ScheduleBlock> list = template.query(query, paramMap, scheduleBlockExtractor);

        LOGGER.trace("<<< GetScheduleBlocks");
        return list;
    }

    /**
     * Get schedule holidays.
     * <p>
     * Run select_schedule_holidays.sql to load holidays associated w/ the schedule.
     *
     * @param schedule
     * @return
     */
    public List<Long> getScheduleHolidays(Schedule schedule) {
        LOGGER.trace(">>> GetScheduleHolidays");

        String query = ScheduleQuery.SELECT_SCHEDULE_HOLIDAYS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", schedule.getScheduleId());

        LOGGER.trace("Query [{}]:\n Params [{}] \n{}", ScheduleQuery.SELECT_SCHEDULE_HOLIDAYS.name(), paramMap, query);

        return template.queryForList(query, paramMap, Long.class);
    }

    /**
     * Get schedule exceptions.
     *
     * @param schedule
     * @return
     */
    public List<ScheduleException> getScheduleExceptions(Schedule schedule) {
        LOGGER.trace(">>> GetScheduleExceptions");

        String query = ScheduleQuery.SELECT_SCHEDULE_EXCEPTION_BLOCKS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("scheduleId", schedule.getScheduleId());

        LOGGER.trace("Query [{}]:\n Params [{}] \n{}", ScheduleQuery.SELECT_SCHEDULE_EXCEPTION_BLOCKS.name(), paramMap, query);

        List<ScheduleException> list =
                template.query(query, paramMap, scheduleExceptionExtractor);

        LOGGER.trace("<<< GetScheduleExceptions");

        return list;
    }
}

