package com.brivo.panel.server.reference.dto.ui;

public class UiCucumberStepsDTO {
    private String keyword;
    private String step;
    private String datatableHeader;
    private String category;

    public UiCucumberStepsDTO() {
    }

    public UiCucumberStepsDTO(String keyword, String step, String datatableHeader, String category) {
        this.keyword = keyword;
        this.step = step;
        this.category = category;
        this.datatableHeader = datatableHeader;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getDatatableHeader() {
        return datatableHeader;
    }

    public void setDatatableHeader(String datatableHeader) {
        this.datatableHeader = datatableHeader;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
