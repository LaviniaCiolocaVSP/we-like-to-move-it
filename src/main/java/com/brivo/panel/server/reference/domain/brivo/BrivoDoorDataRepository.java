package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IDoorDataRepository;
import com.brivo.panel.server.reference.domain.entities.DoorData;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BrivoDoorDataRepository extends IDoorDataRepository {

    Optional<DoorData> findByObjectId(@Param("objectId") long objectId);
}