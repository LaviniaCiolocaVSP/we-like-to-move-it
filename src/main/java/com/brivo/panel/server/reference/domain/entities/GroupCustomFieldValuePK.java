package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class GroupCustomFieldValuePK implements Serializable {
    private long groupId;
    private long groupCustomFieldDefinitionId;

    @Column(name = "group_id", nullable = false)
    @Id
    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Column(name = "group_custom_field_definition_id", nullable = false)
    @Id
    public long getGroupCustomFieldDefinitionId() {
        return groupCustomFieldDefinitionId;
    }

    public void setGroupCustomFieldDefinitionId(long groupCustomFieldDefinitionId) {
        this.groupCustomFieldDefinitionId = groupCustomFieldDefinitionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        GroupCustomFieldValuePK that = (GroupCustomFieldValuePK) o;

        if (groupId != that.groupId) {
            return false;
        }
        return groupCustomFieldDefinitionId == that.groupCustomFieldDefinitionId;
    }

    @Override
    public int hashCode() {
        int result = (int) (groupId ^ (groupId >>> 32));
        result = 31 * result + (int) (groupCustomFieldDefinitionId ^ (groupCustomFieldDefinitionId >>> 32));
        return result;
    }
}
