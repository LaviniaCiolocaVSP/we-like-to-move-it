package com.brivo.panel.server.reference.domain.entities.credential;


import java.util.ArrayList;
import java.util.List;

public class CredentialInput {
    private long credentialId;
    private String referenceId;
    private long credentialFormatId;
    private String encodedCredential;
    private int numBits;
    private boolean unknown = false;
    private List<CredentialFieldValueInput> fieldValues;
    
    public Credential toCredential() {
        Credential credential = new Credential();
        credential.setReferenceId(getReferenceId());
        credential.setFormat(new CredentialFormat(getCredentialFormatId()));
        credential.setEncodedCredential(getEncodedCredential());
        credential.setNumBits(getNumBits());
        credential.setFieldValues(CredentialFieldValueInput.toList(getFieldValues()));
        
        return credential;
        
    }
    
    public String getReferenceId() {
        return referenceId;
    }
    
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
    
    
    public String getEncodedCredential() {
        return encodedCredential;
    }
    
    public void setEncodedCredential(String encodedCredential) {
        this.encodedCredential = encodedCredential;
    }
    
    public int getNumBits() {
        return numBits;
    }
    
    public void setNumBits(int numBits) {
        this.numBits = numBits;
    }
    
    public List<CredentialFieldValueInput> getFieldValues() {
        return fieldValues;
    }
    
    public void setFieldValues(List<CredentialFieldValueInput> fieldValues) {
        this.fieldValues = fieldValues;
    }
    
    public void setCredentialFormatId(long credentialFormatId) {
        this.credentialFormatId = credentialFormatId;
    }
    
    public long getCredentialFormatId() {
        return credentialFormatId;
    }
    
    public boolean isUnknown() {
        return unknown;
    }
    
    public void setUnknown(boolean unknown) {
        this.unknown = unknown;
    }
    
    public long getCredentialId() {
        return credentialId;
    }
    
    public void setCredentialId(long credentialId) {
        this.credentialId = credentialId;
    }
    
    public static final class CredentialFormatInput {
        private long credentialFormatId;
        
        public long getCredentialFormatId() {
            return credentialFormatId;
        }
        
        public CredentialFormat toFormat() {
            return new CredentialFormat(getCredentialFormatId());
        }
        
        public void setCredentialFormatId(long credentialFormatId) {
            this.credentialFormatId = credentialFormatId;
        }
        
        
    }
    
    public static final class CredentialFieldValueInput {
        private String value;
        private long credentialFieldId;
        private String name;
        
        public String getValue() {
            return value;
        }
        
        public static List<CredentialFieldValue> toList(
                List<CredentialFieldValueInput> fieldValues) {
            List<CredentialFieldValue> list = new ArrayList<CredentialFieldValue>();
            if (fieldValues != null) {
                for (CredentialFieldValueInput fieldValue : fieldValues) {
                    list.add(fieldValue.toValue());
                }
            }
            return list;
        }
        
        public CredentialFieldValue toValue() {
            CredentialFieldValue fieldValue = new CredentialFieldValue();
            CredentialField field = new CredentialField();
            fieldValue.setValue(getValue());
            field.setCredentialFieldId(getCredentialFieldId());
            field.setName(getName());
            fieldValue.setCredentialField(field);
            return fieldValue;
            
        }
        
        public void setValue(String value) {
            this.value = value;
        }
        
        public void setCredentialFieldId(long credentialFieldId) {
            this.credentialFieldId = credentialFieldId;
        }
        
        public long getCredentialFieldId() {
            return credentialFieldId;
        }
        
        public String getName() {
            return name;
        }
        
        public void setName(String name) {
            this.name = name;
        }
        
    }
    
}
