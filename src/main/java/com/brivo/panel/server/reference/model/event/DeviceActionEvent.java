package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class DeviceActionEvent extends Event {
    private Long deviceId;
    private Integer actionType;
    private Long argument;

    @JsonCreator
    public DeviceActionEvent(@JsonProperty("eventType") final EventType eventType,
                             @JsonProperty("eventTime") final Instant eventTime,
                             @JsonProperty("deviceId") final Long deviceId,
                             @JsonProperty("actionType") final Integer actionType,
                             @JsonProperty("argument") final Long argument) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.actionType = actionType;
        this.argument = argument;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getActionType() {
        return actionType;
    }

    public void setActionType(Integer actionType) {
        this.actionType = actionType;
    }

    public Long getArgument() {
        return argument;
    }

    public void setArgument(Long argument) {
        this.argument = argument;
    }
}
