package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "firmware_upgrade", schema = "brivo20", catalog = "onair")
public class FirmwareUpgrade {
    private long upgradeId;
    private long brainId;
    private String currentVersion;
    private String nextVersion;
    private Timestamp upgradeTime;
    private Long downloadDuration;
    private String status;
    private Short downloaded;
    private Brain brainByBrainId;

    @Id
    @Column(name = "upgrade_id", nullable = false)
    public long getUpgradeId() {
        return upgradeId;
    }

    public void setUpgradeId(long upgradeId) {
        this.upgradeId = upgradeId;
    }

    @Basic
    @Column(name = "brain_id", nullable = false)
    public long getBrainId() {
        return brainId;
    }

    public void setBrainId(long brainId) {
        this.brainId = brainId;
    }

    @Basic
    @Column(name = "current_version", nullable = false, length = 32)
    public String getCurrentVersion() {
        return currentVersion;
    }

    public void setCurrentVersion(String currentVersion) {
        this.currentVersion = currentVersion;
    }

    @Basic
    @Column(name = "next_version", nullable = false, length = 32)
    public String getNextVersion() {
        return nextVersion;
    }

    public void setNextVersion(String nextVersion) {
        this.nextVersion = nextVersion;
    }

    @Basic
    @Column(name = "upgrade_time", nullable = false)
    public Timestamp getUpgradeTime() {
        return upgradeTime;
    }

    public void setUpgradeTime(Timestamp upgradeTime) {
        this.upgradeTime = upgradeTime;
    }

    @Basic
    @Column(name = "download_duration", nullable = true)
    public Long getDownloadDuration() {
        return downloadDuration;
    }

    public void setDownloadDuration(Long downloadDuration) {
        this.downloadDuration = downloadDuration;
    }

    @Basic
    @Column(name = "status", nullable = false, length = 20)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Basic
    @Column(name = "downloaded", nullable = true)
    public Short getDownloaded() {
        return downloaded;
    }

    public void setDownloaded(Short downloaded) {
        this.downloaded = downloaded;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        FirmwareUpgrade that = (FirmwareUpgrade) o;

        if (upgradeId != that.upgradeId) {
            return false;
        }
        if (brainId != that.brainId) {
            return false;
        }
        if (currentVersion != null ? !currentVersion.equals(that.currentVersion) : that.currentVersion != null) {
            return false;
        }
        if (nextVersion != null ? !nextVersion.equals(that.nextVersion) : that.nextVersion != null) {
            return false;
        }
        if (upgradeTime != null ? !upgradeTime.equals(that.upgradeTime) : that.upgradeTime != null) {
            return false;
        }
        if (downloadDuration != null ? !downloadDuration.equals(that.downloadDuration) : that.downloadDuration != null) {
            return false;
        }
        if (status != null ? !status.equals(that.status) : that.status != null) {
            return false;
        }
        return downloaded != null ? downloaded.equals(that.downloaded) : that.downloaded == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (upgradeId ^ (upgradeId >>> 32));
        result = 31 * result + (int) (brainId ^ (brainId >>> 32));
        result = 31 * result + (currentVersion != null ? currentVersion.hashCode() : 0);
        result = 31 * result + (nextVersion != null ? nextVersion.hashCode() : 0);
        result = 31 * result + (upgradeTime != null ? upgradeTime.hashCode() : 0);
        result = 31 * result + (downloadDuration != null ? downloadDuration.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (downloaded != null ? downloaded.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "brain_id", referencedColumnName = "brain_id", nullable = false, insertable = false, updatable = false)
    public Brain getBrainByBrainId() {
        return brainByBrainId;
    }

    public void setBrainByBrainId(Brain brainByBrainId) {
        this.brainByBrainId = brainByBrainId;
    }
}
