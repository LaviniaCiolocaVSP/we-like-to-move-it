package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePutsPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface IProgDevicePutsRepository extends CrudRepository<ProgDevicePuts, ProgDevicePutsPK> {
    @Query(
            value = "SELECT * " +
                    "FROM brivo20.prog_device_puts " +
                    "WHERE device_oid = :deviceOid",
            nativeQuery = true
    )
    Collection<ProgDevicePuts> findByDeviceOid(@Param("deviceOid") long deviceOid);
}
