package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "report_parameter_option", schema = "brivo20", catalog = "onair")
public class ReportParameterOption {
    private long reportParameterOptionId;
    private long reportParameterId;
    private String optionName;
    private String value;
    private ReportParameter reportParameterByReportParameterId;

    @Id
    @Column(name = "report_parameter_option_id", nullable = false)
    public long getReportParameterOptionId() {
        return reportParameterOptionId;
    }

    public void setReportParameterOptionId(long reportParameterOptionId) {
        this.reportParameterOptionId = reportParameterOptionId;
    }

    @Basic
    @Column(name = "report_parameter_id", nullable = false)
    public long getReportParameterId() {
        return reportParameterId;
    }

    public void setReportParameterId(long reportParameterId) {
        this.reportParameterId = reportParameterId;
    }

    @Basic
    @Column(name = "option_name", nullable = false, length = 55)
    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 55)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportParameterOption that = (ReportParameterOption) o;

        if (reportParameterOptionId != that.reportParameterOptionId) {
            return false;
        }
        if (reportParameterId != that.reportParameterId) {
            return false;
        }
        if (optionName != null ? !optionName.equals(that.optionName) : that.optionName != null) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (reportParameterOptionId ^ (reportParameterOptionId >>> 32));
        result = 31 * result + (int) (reportParameterId ^ (reportParameterId >>> 32));
        result = 31 * result + (optionName != null ? optionName.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_parameter_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportParameter getReportParameterByReportParameterId() {
        return reportParameterByReportParameterId;
    }

    public void setReportParameterByReportParameterId(ReportParameter reportParameterByReportParameterId) {
        this.reportParameterByReportParameterId = reportParameterByReportParameterId;
    }
}
