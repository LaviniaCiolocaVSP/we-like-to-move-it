package com.brivo.panel.server.reference.domain.entities;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public enum SecurityGroupTypes {
    ACCOUNT_ROOT_GROUP(Long.valueOf(0)),
    DEVICE_GROUP(Long.valueOf(1)),
    USER_GROUP(Long.valueOf(2));

    private static final Map<Long, SecurityGroupTypes> BY_TYPE_ID;

    static {
        final Map<Long, SecurityGroupTypes> byTypeID = new HashMap<>();
        for (SecurityGroupTypes ot : SecurityGroupTypes.values()) {
            byTypeID.put(ot.securityGroupTypeID, ot);
        }

        BY_TYPE_ID = Collections.unmodifiableMap(byTypeID);
    }

    private Long securityGroupTypeID;

    SecurityGroupTypes(Long securityGroupTypeID) {
        this.securityGroupTypeID = securityGroupTypeID;
    }

    public static SecurityGroupTypes valueOf(Long objectTypeID) {
        final SecurityGroupTypes ot = BY_TYPE_ID.get(objectTypeID);
        return ot;
    }

    public Long getSecurityGroupTypeID() {
        return securityGroupTypeID;
    }
}

