package com.brivo.panel.server.reference.dto.ui;

import java.io.Serializable;

public class UiAccountDashboardSummaryDTO implements Serializable {

    private long accountId;
    private String accountName;
    private long panelsCount;
    private long doorsCount;
    private long devicesCount;
    private long holidaysCount;
    private long schedulesCount;
    private long groupsCount;
    private long usersCount;
    private long credentialsCount;

    public UiAccountDashboardSummaryDTO(long accountId, String accountName, long panelsCount, long doorsCount, long devicesCount,
                                        long holidaysCount, long schedulesCount, long groupsCount, long usersCount, long credentialsCount) {
        this.accountId = accountId;
        this.accountName = accountName;
        this.panelsCount = panelsCount;
        this.doorsCount = doorsCount;
        this.devicesCount = devicesCount;
        this.holidaysCount = holidaysCount;
        this.schedulesCount = schedulesCount;
        this.groupsCount = groupsCount;
        this.usersCount = usersCount;
        this.credentialsCount = credentialsCount;
    }

    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    public long getPanelsCount() {
        return panelsCount;
    }

    public void setPanelsCount(long panelsCount) {
        this.panelsCount = panelsCount;
    }

    public long getDoorsCount() {
        return doorsCount;
    }

    public void setDoorsCount(long doorsCount) {
        this.doorsCount = doorsCount;
    }

    public long getDevicesCount() {
        return devicesCount;
    }

    public void setDevicesCount(long devicesCount) {
        this.devicesCount = devicesCount;
    }

    public long getHolidaysCount() {
        return holidaysCount;
    }

    public void setHolidaysCount(long holidaysCount) {
        this.holidaysCount = holidaysCount;
    }

    public long getSchedulesCount() {
        return schedulesCount;
    }

    public void setSchedulesCount(long schedulesCount) {
        this.schedulesCount = schedulesCount;
    }

    public long getGroupsCount() {
        return groupsCount;
    }

    public void setGroupsCount(long groupsCount) {
        this.groupsCount = groupsCount;
    }

    public long getUsersCount() {
        return usersCount;
    }

    public void setUsersCount(long usersCount) {
        this.usersCount = usersCount;
    }

    public long getCredentialsCount() {
        return credentialsCount;
    }

    public void setCredentialsCount(long credentialsCount) {
        this.credentialsCount = credentialsCount;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
