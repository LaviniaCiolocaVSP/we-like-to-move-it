package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class UnauthorizedIpAccessEvent extends Event {
    private Long boardId;
    private String processName;
    private String remoteIp;

    @JsonCreator
    public UnauthorizedIpAccessEvent(@JsonProperty("eventType") final EventType eventType,
                                     @JsonProperty("eventTime") final Instant eventTime,
                                     @JsonProperty("boardId") final Long boardId,
                                     @JsonProperty("processName") final String processName,
                                     @JsonProperty("remoteIp") final String remoteIp) {
        super(eventType, eventTime);
        this.boardId = boardId;
        this.processName = processName;
        this.remoteIp = remoteIp;
    }

    public Long getBoardId() {
        return boardId;
    }

    public void setBoardId(Long boardId) {
        this.boardId = boardId;
    }

    public String getProcessName() {
        return processName;
    }

    public void setProcessName(String processName) {
        this.processName = processName;
    }

    public String getRemoteIp() {
        return remoteIp;
    }

    public void setRemoteIp(String remoteIp) {
        this.remoteIp = remoteIp;
    }
}
