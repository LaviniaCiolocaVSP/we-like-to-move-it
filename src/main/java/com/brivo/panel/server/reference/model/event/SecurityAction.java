package com.brivo.panel.server.reference.model.event;

/**
 * This is the number that the OnAir servers know
 * an event type by.
 *
 * @author brandon
 */
public enum SecurityAction {
    NONE(0),

    ACTION_DEVICE_OPEN(2004),
    ACTION_LOCK_KEYPAD(2005),

    ACTION_DOOR_OPEN_TOO_LONG(5001),

    ACTION_DOOR_UNLOCKED_VIA_SCHED(5002),

    ACTION_DOOR_LOCKED_VIA_SCHED(5003),

    ACTION_DOOR_UNLOCKED_VIA_KEYPAD(5004),

    ACTION_DOOR_LOCKED_VIA_KEYPAD(5005),

    ACTION_DOOR_INVALID_CRED_THRESHOLD(5006),

    ACTION_DOOR_EXIT_SWITCH_PRESSED(5007),

    ACTION_DOOR_UNAUTHORIZED_OPEN(5008),

    ACTION_DOOR_OPEN(5009),

    ACTION_DOOR_CLOSED(5010),

    ACTION_DOOR_AJAR_RESOLVED(5011),

    ACTION_ACCESS_FAIL_UNKNOWN_CRED(5012),

    ACTION_ACCESS_FAIL_UNASSIGNED(5013),

    ACTION_ACCESS_FAIL_DELETED_USER(5014),

    ACTION_ACCESS_FAIL_OLD_CRED(5015),

    ACTION_ACCESS_FAIL_OUT_OF_SCHED(5016),

    ACTION_ACCESS_FAIL_EXPIRED(5017),

    ACTION_ACCESS_FAIL_WRONG_DOOR(5018),

    ACTION_AUX_RELAY_ACTIVE(5019),

    ACTION_AUX_RELAY_DEACTIVE(5020),

    ACTION_CP_SWITCH_BATTERY(5021),

    ACTION_CP_AC_POWER_RESTORED(5022),

    ACTION_CP_BATTERY_LEVEL(5023),

    ACTION_CP_UNIT_OPEN(5024),

    ACTION_CP_UNIT_CLOSED(5025),

    ACTION_CP_DISK_SPACE_LOW(5026),

    ACTION_CP_MEMORY_LOW(5027),

    ACTION_CP_HW_INTERFACE_DISCONNECTED(5028),

    ACTION_CP_HW_INTERFACE_RECONNECTED(5029),

    ACTION_CP_HW_MASTER_PIC_RESET(5030),

    ACTION_CP_HW_DOOR_PIC_RESET(5031),

    ACTION_CP_DISK_SPACE_LOW_CLEARED(5032),

    ACTION_CP_MEMORY_LOW_CLEARED(5033),

    ACTION_PANEL_STARTUP(5035),

    ACTION_UNCONFIGURED_DEVICE(5036),

    /**
     * Didn't get heartbeat response from board
     */
    ACTION_BOARD_COMMS_FAILURE_SET(5041),

    /**
     * Heartbeat responses restored
     */
    ACTION_BOARD_COMMS_FAILURE_CLR(5042),

    /**
     * We have a board that went on battery
     */
    ACTION_BOARD_BATTERY_SET(5043),

    /**
     * We have a board that went off battery
     */
    ACTION_BOARD_BATTERY_CLR(5044),

    /**
     * Tamper activated on a board
     */
    ACTION_BOARD_TAMPER_SET(5045),

    /**
     * Tamper cleared on a board
     */
    ACTION_BOARD_TAMPER_CLR(5046),

    /**
     * Board micro-controller reset
     */
    ACTION_BOARD_CHIP_RESET(5047),

    /**
     * Supervised input detected a wiring short
     */
    ACTION_WIRE_SHORT_SET(5048),

    /**
     * Supervised input detected a wiring short cleared
     */
    ACTION_WIRE_SHORT_CLR(5049),

    /**
     * There is a board with no jumper settings
     */
    ACTION_CAN_BUS_UNCONFIGURED_ERROR(5052),

    /**
     * Two boards with the same address
     */
    ACTION_CAN_BUS_COLLISION_ERROR(5053),

    /**
     * CP is sending up its modem number
     */
    ACTION_CP_MODEM_NUMBER(5054),

    ACTION_ACCESS_FAIL_AUTHORIZATION_PENDING(5034),

    /**
     * Programmable device engaged
     */
    ACTION_DEVICE_ENGAGED(5037),

    /**
     * Programmable device disengaged
     */
    ACTION_DEVICE_DISENGAGED(5038),

    /**
     * Supervised input detected a wiring cut
     */
    ACTION_WIRE_CUT_SET(5039),

    /**
     * Supervised input detected a wiring cut cleared
     */
    ACTION_WIRE_CUT_CLR(5040),

    /**
     * An unlock schedule was overridden
     */
    ACTION_SCD_UNLOCK_OVERRIDE_BEGIN(5050),

    /**
     * An unlock schedule was restored
     */
    ACTION_SCD_UNLOCK_OVERRIDE_END(5051),
    /**
     * A schedule was activated by an enabling group.
     */
    ACTION_SCHEDULE_ACTIVATED(5055),
    ACTION_INVALID_SECOND_FACTOR_UNKNOWN_CARD(5056),
    ACTION_INVALID_SECOND_FACTOR_UNKNOWN_PIN(5057),
    ACTION_INVALID_SECOND_FACTOR_INVALID_CARD(5058),
    ACTION_INVALID_SECOND_FACTOR_TWO_CARDS(5059),
    ACTION_INVALID_SECOND_FACTOR_TWO_PINS(5060),
    ACTION_SECOND_FACTOR_NOT_GIVEN(5061),
    ACTION_ANTIPASSBACK_FAILURE(5063),

    ACTION_ADMIN_PULSE_OUTPUT(5064),

    ACTION_ACCESS_FAIL_DEACTIVATED(5065),

    ACTION_INVALID_CREDENTIAL_TYPE(5069),

    ACTION_INVALID_CREDENTIAL_TYPE_UNKNOWN_USER(5070),

    ACTION_UNAUTHORIZED_IP_ACCESS(5081),

    ACTION_SALTO_ROUTER_HELLO(5202),
    ACTION_SALTO_DOOR_LOCK_REX(5203),
    ACTION_SALTO_DOOR_LOCK_OPEN_METALLIC_KEY(5204),
    ACTION_SALTO_DOOR_LOCK_OPEN_PPD(5205),
    ACTION_SALTO_DOOR_LOCK_OPEN_MEMORIZED_CODE(5206),
    ACTION_SALTO_DOOR_LOCK_INTRUSION_ALARM(5207),
    ACTION_SALTO_DOOR_LOCK_INTRUSION_ALARM_CLEAR(5208),
    ACTION_SALTO_DOOR_LOCK_FAILED_ENTRANCE_LOW_BATTERY(5209),
    ACTION_SALTO_DOOR_LOCK_LOW_BATTERY(5210),
    ACTION_SALTO_NODE_COMMUNICATION_LOST(5211),
    ACTION_SALTO_NODE_COMMUNICATION_RESTORED(5212),
    ACTION_SALTO_ROUTER_CONNECTION_DROPPED(5213),
    ACTION_SALTO_ROUTER_FAILED_ENTRANCE_ROUTER_OFFLINE(5214),

    ACTION_DOOR_LOCK_OPEN_PRIVACY_OVERRIDE(5231),
    ACTION_DOOR_LOCK_FAILED_ENTRANCE_PRIVACY_MODE(5232),

    ACTION_IPAC_PULSE_OUTPUT(5218),
    ACTION_IPAC_RESIDENT_CALL_CALLED(5219),
    ACTION_IPAC_RESIDENT_CALL_CONNECTED(5220),
    ACTION_IPAC_RESIDENT_CALL_TERMINATED(5221),

    GATEWAY_DISCONNECTED(7708),
    GATEWAY_RECONNECTED(7709),
    WIRELESS_LOCK_DISCONNECTED(7710),
    WIRELESS_LOCK_RECONNECTED(7711);

    private int id;

    SecurityAction(int id) {
        this.id = id;
    }

    public static SecurityAction getSecurityAction(int actionId) {
        for (SecurityAction action : values()) {
            if (action.getSecurityActionValue() == actionId) {
                return action;
            }
        }
        return NONE;
    }

    public int getSecurityActionValue() {
        return this.id;
    }
}
