package com.brivo.panel.server.reference.dao.event;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum EventQuery implements FileQuery {
    SELECT_BOARD_POINT_EVENT,
    SELECT_CREDENTIAL_BY_VALUE_255_EVENT,
    SELECT_CREDENTIAL_BY_VALUE_EVENT,
    SELECT_DEVICE_EVENT,
    SELECT_DEVICE_MESSAGE_EVENT,
    SELECT_SITE_OCCUPANT_DOOR_INFO,
    SELECT_ENTRANCE_EVENT,
    SELECT_MOBILE_CREDENTIAL,
    SELECT_PANEL_BY_OBJECT_ID,
    SELECT_PERMISSION_COUNT,
    SELECT_PIN_BY_VALUE_EVENT,
    SELECT_ROUTER_EVENT,
    SELECT_SCHEDULE_ACTIVATED_EVENT,
    SELECT_USER_BY_OBJECT_ID,
    SELECT_UNKNOWN_CARD_ID,
    SELECT_VALID_FOR_PRESENCE,
    SELECT_SITE_BY_SECURITY_GROUP_AND_PANEL,
    INSERT_SITE_OCCUPANT_FOR_INGRESS,
    UPDATE_SITE_OCCUPANT_FOR_INGRESS,
    UPDATE_SITE_OCCUPANT_FOR_EGRESS,
    UPDATE_UNKNOWN_CREDENTIAL,
    SELECT_SCHEDULE_NAME,
    SELECT_BOARD_NUMBER,
    DELETE_ALL_EVENTS;

    private String query;

    EventQuery() {
        this.query = QueryUtils.getQueryText(EventQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }
}
