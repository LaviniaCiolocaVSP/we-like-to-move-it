package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.model.PanelInfoBean;
import com.brivo.panel.server.reference.dao.credential.CredentialDao;
import com.brivo.panel.server.reference.domain.common.ICredentialFieldRepository;
import com.brivo.panel.server.reference.domain.entities.AccessCredential;
import com.brivo.panel.server.reference.domain.entities.AccessCredentialType;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.CredentialField;
import com.brivo.panel.server.reference.domain.entities.CredentialFieldValue;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.ObjectPermission;
import com.brivo.panel.server.reference.domain.entities.SecurityGroupMember;
import com.brivo.panel.server.reference.domain.entities.Users;
import com.brivo.panel.server.reference.domain.repository.AccessCredentialTypeRepository;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.BrivoObjectRepository;
import com.brivo.panel.server.reference.domain.repository.CredentialRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPermissionRepository;
import com.brivo.panel.server.reference.domain.repository.SecurityGroupMemberRepository;
import com.brivo.panel.server.reference.dto.AccessCredentialTypeDTO;
import com.brivo.panel.server.reference.dto.CredentialDTO;
import com.brivo.panel.server.reference.dto.CredentialFieldDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiCredentialDTO;
import com.brivo.panel.server.reference.dto.ui.UiCredentialFormatDTO;
import com.brivo.panel.server.reference.model.event.SecurityAction;
import com.brivo.panel.server.reference.model.message.down.DeleteCredentialMessage;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;

@Service
public class UiCredentialService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiCredentialService.class);
    private static final String DEFAULT_REF_ID = "20eb23d7-7b5e-463a-b608-2cc4098bb541";

    private final CredentialRepository credentialRepository;
    private final BrivoObjectRepository objectRepository;
    private final AccessCredentialTypeRepository accessCredentialTypeRepository;
    private final AccountRepository accountRepository;
    private final SecurityGroupMemberRepository securityGroupMemberRepository;
    private final ObjectPermissionRepository objectPermissionRepository;
    private final CredentialDao credentialDao;

    @Autowired
    public UiCredentialService(final CredentialRepository credentialRepository, final BrivoObjectRepository objectRepository,
                               final AccessCredentialTypeRepository accessCredentialTypeRepository,
                               final AccountRepository accountRepository, final CredentialDao credentialDao,
                               final SecurityGroupMemberRepository securityGroupMemberRepository,
                               final ObjectPermissionRepository objectPermissionRepository) {
        this.credentialRepository = credentialRepository;
        this.objectRepository = objectRepository;
        this.accessCredentialTypeRepository = accessCredentialTypeRepository;
        this.accountRepository = accountRepository;
        this.credentialDao = credentialDao;
        this.securityGroupMemberRepository = securityGroupMemberRepository;
        this.objectPermissionRepository = objectPermissionRepository;
    }

    public Collection<CredentialFieldDTO> getAllCredentialFieldDTOS(final ICredentialFieldRepository credentialFieldRepository) {
        return StreamSupport.stream(credentialFieldRepository.findAll().spliterator(), false)
                            .map(convertCredentialField())
                            .collect(Collectors.toList());
    }

    private Function<CredentialField, CredentialFieldDTO> convertCredentialField() {
        return credentialField -> new CredentialFieldDTO(credentialField.getCredentialFieldId(),
                                                         credentialField.getAccessCredentialTypeId(), credentialField.getCredentialFieldTypeId(),
                                                         credentialField.getFormat(), credentialField.getName(), credentialField.getForReference(),
                                                         credentialField.getDisplay(), credentialField.getOrdering());
    }

    public Collection<CredentialDTO> getCredentialDTOS(final Collection<AccessCredential> credentials) {
        return credentials.stream()
                          .map(convertCredential())
                          .collect(Collectors.toSet());
    }

    private Function<AccessCredential, CredentialDTO> convertCredential() {
        return credential -> {
            Long ownerObjectId = NOT_AVAILABLE_LONG_VALUE;
            Optional<BrivoObject> credentialObject = Optional.ofNullable(credential.getObjectByOwnerObjectId());
            if (credentialObject.isPresent()) {
                ownerObjectId = credentialObject.get().getObjectId();
            }

            final Collection<CredentialDTO.CredentialFieldValueDTO> credentialFieldValues = getCredentialFieldValuesOfCredential(credential);

            return new CredentialDTO(ownerObjectId, credential.getAccessCredentialId(),
                                     credential.getAccessCredentialTypeByAccessCredentialTypeId().getAccessCredentialTypeId(),
                                     credential.getCredential(), credential.getReferenceId(),
                                     credential.getEnableOn(), credential.getExpires(), credential.getDisabled(), credential.getNumBits(),
                                     credential.getLastOwnerObjectId(), credentialFieldValues);
        };
    }

    private Collection<CredentialDTO.CredentialFieldValueDTO> getCredentialFieldValuesOfCredential(final AccessCredential credential) {
        return credential.getCredentialFieldValuesByAccessCredentialId()
                         .stream()
                         .map(convertCredentialFieldValue())
                         .collect(Collectors.toList());
    }

    private Function<CredentialFieldValue, CredentialDTO.CredentialFieldValueDTO> convertCredentialFieldValue() {
        return credentialFieldValue -> new CredentialDTO.CredentialFieldValueDTO(credentialFieldValue.getCredentialFieldId(),
                                                                                 credentialFieldValue.getValue());
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiCredentialFormatDTO> getUiCredentialFormats() {
        final Collection<AccessCredentialType> accessCredentialTypes = accessCredentialTypeRepository.findCardCredentialFormats()
                                                                                                     .orElse(Collections.emptyList());

        return accessCredentialTypes.stream()
                                    .map(convertCredentialFormatUI())
                                    .collect(Collectors.toSet());
    }

    private Function<AccessCredentialType, UiCredentialFormatDTO> convertCredentialFormatUI() {
        return accessCredentialType -> new UiCredentialFormatDTO(accessCredentialType.getAccessCredentialTypeId(),
                                                                 accessCredentialType.getName());
    }

    public Collection<UiCredentialDTO> getUiCredentialDTOS(final Collection<AccessCredential> credentials) {
        return credentials.stream()
                          .map(convertCredentialUI())
                          .sorted(uiCredentialDTOComparator)
                          .collect(Collectors.toList());
    }

    Comparator<UiCredentialDTO> uiCredentialDTOComparator = new Comparator<UiCredentialDTO>() {
        @Override
        public int compare(UiCredentialDTO o1, UiCredentialDTO o2) {
            return o2.getCredentialId().compareTo(o1.getCredentialId());
        }
    };

    private Function<AccessCredential, UiCredentialDTO> convertCredentialUI() {
        return credential -> {
            final AccessCredentialType accessCredentialType = credential.getAccessCredentialTypeByAccessCredentialTypeId();

            Long ownerObjectId = NOT_AVAILABLE_LONG_VALUE;
            String ownerFullName = "";
            Optional<BrivoObject> credentialObject = Optional.ofNullable(credential.getObjectByOwnerObjectId());
            if (credentialObject.isPresent()) {
                ownerObjectId = credentialObject.get().getObjectId();
                final Users user = credentialObject.get().getUsersByObjectId();
                ownerFullName = user.getFirstName() + " " + user.getLastName();
            }
            String facility_code = "";
            if (accessCredentialType.getAccessCredentialTypeId() == 100) {
                CredentialFieldValue credentialFieldValue1 = credential.getCredentialFieldValuesByAccessCredentialId().stream()
                                                                       .filter(credentialFieldValue -> credentialFieldValue.getCredentialFieldId() == 142)
                                                                       .findFirst()
                                                                       .orElse(null);
                if (credentialFieldValue1 != null)
                    facility_code = credentialFieldValue1.getValue();
            }
            return new UiCredentialDTO(credential.getAccessCredentialId(),
                                       accessCredentialType.getAccessCredentialTypeId(), accessCredentialType.getName(), ownerFullName,
                                       credential.getEnableOn(), credential.getExpires(), credential.getReferenceId(), ownerObjectId,
                                       credential.getCredential(), credential.getNumBits(), credential.getAccountId(), facility_code);
        };
    }

    private Function<UiCredentialDTO, AccessCredential> convertCredentialUIDTO() {
        return credentialDTO -> {
            final LocalDateTime now = LocalDateTime.now();
            AccessCredential credential = new AccessCredential();

            final Long credentialId = getNewCredentialId();
            credential.setAccessCredentialId(credentialId);
            credential.setExpires(credentialDTO.getExpires());
            credential.setEnableOn(credentialDTO.getEnableOn());
            credential.setReferenceId(credentialDTO.getReferenceId());
            credential.setCreated(now);
            credential.setUpdated(now);
            credential.setCredential(credentialDTO.getCredential());
            credential.setNumBits(credentialDTO.getNumBits());

            final Account account = getAccountOrThrow(credentialDTO.getAccountId());
            credential.setAccountByAccountId(account);
            credential.setAccountId(credentialDTO.getAccountId());

            final BrivoObject owner = objectRepository.findById(credentialDTO.getOwnerObjectId())
                                                      .orElse(null);

            if (owner != null) {
                credential.setLastOwnerObjectId(owner.getObjectId());
            }

            credential.setObjectByOwnerObjectId(owner);

            final AccessCredentialType accessCredentialType = getAccessCredentialTypeOrThrow(credentialDTO.getCredentialTypeId());
            credential.setAccessCredentialTypeByAccessCredentialTypeId(accessCredentialType);

            return credential;
        };
    }

    private Account getAccountOrThrow(final Long accountId) {
        return accountRepository.findById(accountId)
                                .orElseThrow(() -> new IllegalArgumentException(
                                        "Account with id " + accountId + " not found in database"));
    }

    private AccessCredentialType getAccessCredentialTypeOrThrow(Long accessCredentialTypeId) {
        return accessCredentialTypeRepository.findById(accessCredentialTypeId)
                                             .orElseThrow(() -> new IllegalArgumentException(
                                                     "Access credential type with id " + accessCredentialTypeId + " not found in database"));
    }

    private Long getNewCredentialId() {
        final Long maxCredentialId = credentialRepository.getMaxAccessCredentialId()
                                                         .orElseThrow(() -> new IllegalArgumentException(
                                                                 "An error occured while getting max(access_credential_id)"));
        return maxCredentialId + 1;
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiCredentialDTO> getUiCredentialsForAccount(final long accountId) {
        final Collection<AccessCredential> accessCredentials = credentialRepository.findByAccountId(accountId)
                                                                                   .orElse(Collections.emptyList());

        return getUiCredentialDTOS(accessCredentials);
    }


    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiCredentialDTO> getUiCredentialsForPanel(final long panelOid) {
        final Collection<AccessCredential> accessCredentials = credentialRepository.findByPanelOid(panelOid)
                                                                                   .orElse(Collections.emptyList());

        return getUiCredentialDTOS(accessCredentials);
    }


    @Transactional
    public MessageDTO create(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Adding a new credential for account {}", credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTO().apply(credentialDTO);
        credentialRepository.save(credential);

        LOGGER.debug("Successfully added credential to account {}", credentialDTO.getAccountId());

        return new MessageDTO("The credential was successfully added");
    }

    private Collection<Device> getDevicesForCredential(final AccessCredential credential) {
        final long accessCredentialId = credential.getAccessCredentialId();
        final Collection<Device> notAvailablePanelsForCredential = Collections.emptyList();

        LOGGER.debug("Getting brainIds for credential with id {}", accessCredentialId);

        final BrivoObject owner = credential.getObjectByOwnerObjectId();

        if (owner == null || owner.getUsersByObjectId() == null) {
            LOGGER.debug("The credential {} doesn't have an owner, therefore it isn't associated to any panel", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final long userObjectId = owner.getObjectId();
        final Optional<List<SecurityGroupMember>> groupMembers = securityGroupMemberRepository.findByObjectId(userObjectId);
        if (!groupMembers.isPresent()) {
            LOGGER.debug("The owner of the credential with id {} is user with object id {}. which is not associated to " +
                         "any groups, therefore the credential isn't associated to any panel", accessCredentialId, userObjectId);
            return notAvailablePanelsForCredential;
        }

        final Collection<Long> groupObjectIds = groupMembers.get()
                                                            .stream()
                                                            .map(securityGroupMember -> securityGroupMember.getSecurityGroupBySecurityGroupId()
                                                                                                           .getObjectByObjectId()
                                                                                                           .getObjectId())
                                                            .collect(Collectors.toList());
        if (groupObjectIds.isEmpty()) {
            LOGGER.debug("There are no security groups associated to credential with id {}", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final Collection<ObjectPermission> objectPermissions = objectPermissionRepository.findAllByGroupObjectId(groupObjectIds, SecurityAction.ACTION_DEVICE_OPEN.getSecurityActionValue())
                                                                                         .orElse(Collections.emptyList());

        if (objectPermissions.isEmpty()) {
            LOGGER.debug("There are no object permissions associated to credential with id {}", accessCredentialId);
            return notAvailablePanelsForCredential;
        }

        final Collection<Device> devices = objectPermissions.stream()
                                                            .map(objectPermission -> objectPermission.getObjectByBrivoObjectId().getDeviceByObjectId())
                                                            .collect(Collectors.toSet());

        LOGGER.debug("Successfully retrieved devices = {} for credential with id {}",
                     StringUtils.join(devices.toArray(), "; "), accessCredentialId);

        return devices;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO update(final UiCredentialDTO credentialDTO) {
        LOGGER.debug("Updating credential {} for account {}", credentialDTO.toString(), credentialDTO.getAccountId());

        final AccessCredential credential = convertCredentialUIDTO().apply(credentialDTO);
        credential.setAccessCredentialId(credentialDTO.getCredentialId());

        credentialDao.deleteCredentialById(credential.getAccessCredentialId());
        credentialRepository.save(credential);

        LOGGER.debug("Successfully updated credential {} for account {}", credentialDTO.getCredentialId(), credentialDTO.getAccountId());

        return new MessageDTO("The credential with id " + credentialDTO.getCredentialId() + " was successfully updated");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO deleteCredentials(final List<Long> credentialIds) {
        LOGGER.debug("Deleting access credentials with ids {}...", StringUtils.join(credentialIds.toArray(), "; "));
        Collection<Long> differentBrainIds = new HashSet<>();
        Collection<Long> differentBrainObjectIds = new HashSet<>();

        deleteCredentialByIdsAndGetTheirAssociatedBrains(credentialIds, differentBrainIds, differentBrainObjectIds);

        // sendDeleteCredentialMessageToPanels(credentialIds, differentBrainObjectIds);

        LOGGER.debug("Successfully deleted access credentials with ids {}...", StringUtils.join(credentialIds.toArray(), "; "));

        return new MessageDTO("The access credentials were successfully deleted");
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public MessageDTO deleteCredentialsCucumber(final List<Long> credentialIds) {
        LOGGER.debug("Deleting access credentials with ids {}...", StringUtils.join(credentialIds.toArray(), "; "));
        Collection<Long> differentBrainIds = new HashSet<>();
        Collection<Long> differentBrainObjectIds = new HashSet<>();

        deleteCredentialByIdsAndGetTheirAssociatedBrains(credentialIds, differentBrainIds, differentBrainObjectIds);

        //  differentBrainIds.forEach(brainId -> flushMessageUtils.sendFlushMessageToPanel(brainId));

        // sendDeleteCredentialMessageToPanels(credentialIds, differentBrainObjectIds);

        LOGGER.debug("Successfully deleted access credentials with ids {}...", StringUtils.join(credentialIds.toArray(), "; "));
        return new MessageDTO("The access credentials were successfully deleted");
    }

    private void deleteCredentialByIdsAndGetTheirAssociatedBrains(final List<Long> credentialIds, final Collection<Long> differentBrainIds, final Collection<Long> differentBrainObjectIds) {
        credentialIds.forEach(credentialId -> {
            if (isValidCredential(credentialId)) {
                credentialDao.deleteCredentialById(credentialId);
            }

            final AccessCredential currentCredential =
                    credentialRepository.findById(credentialId).get();
            final Collection<Device> devicesForCurrentCredential = getDevicesForCredential(currentCredential);

            final Collection<Long> brainIds = devicesForCurrentCredential.stream()
                                                                         .map(device -> device.getBrainByBrainId().getBrainId())
                                                                         .collect(Collectors.toSet());

            final Collection<Long> brainObjectIds = devicesForCurrentCredential.stream()
                                                                               .map(device -> device.getBrainByBrainId().getObjectId())
                                                                               .collect(Collectors.toSet());

            brainIds.forEach(brainId -> differentBrainIds.add(brainId));
            brainObjectIds.forEach(brainObjectId -> differentBrainObjectIds.add(brainObjectId));
        });
    }

    public boolean isValidCredential(final Long credentialId) {
        return credentialRepository.findById(credentialId).isPresent();
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public List<PanelInfoBean> getPanelsForCredential(Long accessCredentialId) {
        List<PanelInfoBean> panelSerialNoList = new ArrayList<>();

        AccessCredential accessCredential = credentialRepository.findById(accessCredentialId).get();

        Collection<Device> devices = getDevicesForCredential(accessCredential);

        devices.forEach(device -> panelSerialNoList.add(new PanelInfoBean(device.getBrainByBrainId().getElectronicSerialNumber(), device.getBrainId())));
        return panelSerialNoList;
    }

    public Function<AccessCredentialType, AccessCredentialTypeDTO> convertAccessCredentialTypeToDTO() {
        return accessCredentialType -> new AccessCredentialTypeDTO(accessCredentialType.getAccessCredentialTypeId(),
                accessCredentialType.getName(), accessCredentialType.getDescription(), accessCredentialType.getNumBits(),
                accessCredentialType.getFormat(), accessCredentialType.getSupportedBy4000(), accessCredentialType.getCredentialEncodingId(),
                accessCredentialType.getNewCardEngineDate());
    }

    public Function<AccessCredentialTypeDTO, AccessCredentialType> convertAccessCredentialTypeDTO() {
        return accessCredentialTypeDTO -> {
            AccessCredentialType accessCredentialType = new AccessCredentialType();

            accessCredentialType.setAccessCredentialTypeId(accessCredentialTypeDTO.getId());
            accessCredentialType.setName(accessCredentialTypeDTO.getName());
            accessCredentialType.setDescription(accessCredentialTypeDTO.getDescription());
            accessCredentialType.setNumBits(accessCredentialTypeDTO.getNumBits());
            accessCredentialType.setFormat(accessCredentialTypeDTO.getFormat());
            accessCredentialType.setSupportedBy4000(accessCredentialTypeDTO.getSupportedBy4000());
            accessCredentialType.setCredentialEncodingId(accessCredentialTypeDTO.getCredentialEncodingId());
            accessCredentialType.setNewCardEngineDate(accessCredentialTypeDTO.getNewCardEngineDate());

            return accessCredentialType;
        };
    }

}
