package com.brivo.panel.server.reference.dto.ui;

import java.util.List;

public class UiInfoDTO {
    private String version;
    private String releaseDate;
    private List<String> changeLog;
    private List<UiInfoDTO> previousVersions;

    public UiInfoDTO() {
    }

    public UiInfoDTO(String version, List<String> changeLog) {
        this.version = version;
        this.changeLog = changeLog;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(final String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public List<String> getChangeLog() {
        return changeLog;
    }

    public void setChangeLog(List<String> changeLog) {
        this.changeLog = changeLog;
    }

    public List<UiInfoDTO> getPreviousVersions() {
        return previousVersions;
    }

    public void setPreviousVersions(final List<UiInfoDTO> previousVersions) {
        this.previousVersions = previousVersions;
    }
}
