package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "camera_model_param", schema = "brivo20", catalog = "onair")
public class CameraModelParam {
    private long cameraModelParamId;
    private long cameraModelId;
    private long dataType;
    private String messageKey;
    private String validatorClass;
    private Short optional;
    private String defaultValue;
    private Short displayable;
    private CameraModel cameraModelByCameraModelId;
    private Collection<CameraParam> cameraParamsByCameraModelParamId;

    @Id
    @Column(name = "camera_model_param_id", nullable = false)
    public long getCameraModelParamId() {
        return cameraModelParamId;
    }

    public void setCameraModelParamId(long cameraModelParamId) {
        this.cameraModelParamId = cameraModelParamId;
    }

    @Basic
    @Column(name = "camera_model_id", nullable = false)
    public long getCameraModelId() {
        return cameraModelId;
    }

    public void setCameraModelId(long cameraModelId) {
        this.cameraModelId = cameraModelId;
    }

    @Basic
    @Column(name = "data_type", nullable = false)
    public long getDataType() {
        return dataType;
    }

    public void setDataType(long dataType) {
        this.dataType = dataType;
    }

    @Basic
    @Column(name = "message_key", nullable = true, length = 256)
    public String getMessageKey() {
        return messageKey;
    }

    public void setMessageKey(String messageKey) {
        this.messageKey = messageKey;
    }

    @Basic
    @Column(name = "validator_class", nullable = true, length = 256)
    public String getValidatorClass() {
        return validatorClass;
    }

    public void setValidatorClass(String validatorClass) {
        this.validatorClass = validatorClass;
    }

    @Basic
    @Column(name = "optional", nullable = true)
    public Short getOptional() {
        return optional;
    }

    public void setOptional(Short optional) {
        this.optional = optional;
    }

    @Basic
    @Column(name = "default_value", nullable = true, length = 512)
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    @Basic
    @Column(name = "displayable", nullable = true)
    public Short getDisplayable() {
        return displayable;
    }

    public void setDisplayable(Short displayable) {
        this.displayable = displayable;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraModelParam that = (CameraModelParam) o;

        if (cameraModelParamId != that.cameraModelParamId) {
            return false;
        }
        if (cameraModelId != that.cameraModelId) {
            return false;
        }
        if (dataType != that.dataType) {
            return false;
        }
        if (messageKey != null ? !messageKey.equals(that.messageKey) : that.messageKey != null) {
            return false;
        }
        if (validatorClass != null ? !validatorClass.equals(that.validatorClass) : that.validatorClass != null) {
            return false;
        }
        if (optional != null ? !optional.equals(that.optional) : that.optional != null) {
            return false;
        }
        if (defaultValue != null ? !defaultValue.equals(that.defaultValue) : that.defaultValue != null) {
            return false;
        }
        return displayable != null ? displayable.equals(that.displayable) : that.displayable == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraModelParamId ^ (cameraModelParamId >>> 32));
        result = 31 * result + (int) (cameraModelId ^ (cameraModelId >>> 32));
        result = 31 * result + (int) (dataType ^ (dataType >>> 32));
        result = 31 * result + (messageKey != null ? messageKey.hashCode() : 0);
        result = 31 * result + (validatorClass != null ? validatorClass.hashCode() : 0);
        result = 31 * result + (optional != null ? optional.hashCode() : 0);
        result = 31 * result + (defaultValue != null ? defaultValue.hashCode() : 0);
        result = 31 * result + (displayable != null ? displayable.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "camera_model_id", referencedColumnName = "camera_model_id", nullable = false, insertable = false, updatable = false)
    public CameraModel getCameraModelByCameraModelId() {
        return cameraModelByCameraModelId;
    }

    public void setCameraModelByCameraModelId(CameraModel cameraModelByCameraModelId) {
        this.cameraModelByCameraModelId = cameraModelByCameraModelId;
    }

    @OneToMany(mappedBy = "cameraModelParamByCameraModelParamId")
    public Collection<CameraParam> getCameraParamsByCameraModelParamId() {
        return cameraParamsByCameraModelParamId;
    }

    public void setCameraParamsByCameraModelParamId(Collection<CameraParam> cameraParamsByCameraModelParamId) {
        this.cameraParamsByCameraModelParamId = cameraParamsByCameraModelParamId;
    }
}
