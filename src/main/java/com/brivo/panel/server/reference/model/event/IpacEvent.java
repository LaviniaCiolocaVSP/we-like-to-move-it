package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class IpacEvent extends Event {
    private Long tenantId;

    @JsonCreator
    public IpacEvent(@JsonProperty("eventType") final EventType eventType,
                     @JsonProperty("eventTime") final Instant eventTime,
                     @JsonProperty("tenantId") final Long tenantId) {
        super(eventType, eventTime);
        this.tenantId = tenantId;
    }

    public Long getTenantId() {
        return tenantId;
    }

    public void setTenantId(Long tenantId) {
        this.tenantId = tenantId;
    }
}
