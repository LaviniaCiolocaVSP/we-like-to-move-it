package com.brivo.panel.server.reference.dto.ui;

public class UIUserSummaryDTO {
    private final Long objectId;
    private final String firstName;
    private final String lastName;
    private final String fullName;

    public UIUserSummaryDTO(Long objectId, String firstName, String lastName) {
        this.objectId = objectId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.fullName = this.firstName + " " + lastName;
    }

    public Long getObjectId() {
        return objectId;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFullName() {
        return fullName;
    }
}
