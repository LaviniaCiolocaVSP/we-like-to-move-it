package com.brivo.panel.server.reference.domain.brivo;

import com.brivo.panel.server.reference.domain.common.IProgDevicePutsRepository;

public interface BrivoProgDevicePutsRepository extends IProgDevicePutsRepository {
}
