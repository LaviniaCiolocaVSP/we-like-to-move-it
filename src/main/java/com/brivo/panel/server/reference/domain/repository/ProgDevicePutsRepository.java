package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IProgDevicePutsRepository;
import com.brivo.panel.server.reference.domain.entities.ProgDevicePuts;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Collection;

public interface ProgDevicePutsRepository extends IProgDevicePutsRepository {

    @Query(
            value = "DELETE FROM brivo20.prog_device_puts pdp " +
                    "WHERE pdp.device_oid = :deviceOid",
            nativeQuery = true
    )
    @Modifying
    void deleteByDeviceOid(@Param("deviceOid") Long deviceOid);

}
