package com.brivo.panel.server.reference.model.event;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.Instant;

public class EntranceEvent extends Event {
    private Long deviceId;
    private Long userId;
    private Long credentialId;

    @JsonCreator
    public EntranceEvent(@JsonProperty("eventType") final EventType eventType,
                         @JsonProperty("eventTime") final Instant eventTime,
                         @JsonProperty("deviceId") final Long deviceId,
                         @JsonProperty("userId") final Long userId,
                         @JsonProperty("credentialId") final Long credentialId) {
        super(eventType, eventTime);
        this.deviceId = deviceId;
        this.userId = userId;
        this.credentialId = credentialId;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCredentialId() {
        return credentialId;
    }

    public void setCredentialId(Long credentialId) {
        this.credentialId = credentialId;
    }
}
