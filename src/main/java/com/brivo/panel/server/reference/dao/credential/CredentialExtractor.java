package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.model.credential.Credential;
import com.brivo.panel.server.reference.model.credential.CredentialType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Credential Extractor.
 */
@Component
public class CredentialExtractor implements ResultSetExtractor<List<Credential>> {
    private static final String BMP_CRED_TYPE_NAME = "Brivo Mobile Pass";
    private static final int PIN_CREDENTIAL_TYPE_ID_MAGIC_NUMBER_CEILING = 10;

    @Override
    public List<Credential> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<Credential> list = new ArrayList<>();

        while (rs.next()) {
            Credential cred = new Credential();

            Long ownerId = rs.getLong("ownerOID");
            Long credTypeId = rs.getLong("accessCredentialTypeID");
            String credTypeName = rs.getString("accessCredentialTypeName");
            Date enableDate = rs.getTimestamp("enableOn");
            Date expiresDate = rs.getTimestamp("expires");
            String value = rs.getString("credential");
            Long credId = rs.getLong("accessCredentialID");
            Date created = rs.getTimestamp("created");
            Long beforeMagicDate = rs.getLong("createdWith255Magic");

            Integer numBits = rs.getInt("numBits");
            if (numBits == null || numBits == 0) {
                numBits = rs.getInt("numBitsBackup");
            }

            Instant active = (enableDate == null) ? null : enableDate.toInstant();
            Instant expires = (expiresDate == null) ? null : expiresDate.toInstant();

            cred.setUserId(ownerId);
            cred.setCredentialType(resolveCredentialType(credTypeId, credTypeName));

            cred.setNumberBits(numBits);
            cred.setActive(active);
            cred.setExpires(expires);
            cred.setValue(value);
            cred.setCredentialId(credId);
            cred.setCreated(created.toInstant());
            cred.setCreatedWith255Magic(beforeMagicDate < 0);

            list.add(cred);
        }

        return list;
    }

    private CredentialType resolveCredentialType(Long credentialTypeId, String credentialTypeName) {
        if (credentialTypeId < PIN_CREDENTIAL_TYPE_ID_MAGIC_NUMBER_CEILING) {
            return CredentialType.PIN;
        }

        if (BMP_CRED_TYPE_NAME.equals(credentialTypeName)) {
            return CredentialType.DIGITAL;
        }

        return CredentialType.CARD;
    }
}
