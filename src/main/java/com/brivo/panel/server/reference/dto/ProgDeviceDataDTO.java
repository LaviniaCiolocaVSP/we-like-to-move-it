package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ProgDeviceDataDTO {
    private final Short notifyFlag;
    private final Short notifyDiseng;
    private final String engageMsg;
    private final String disengageMsg;
    private final Long doorId;
    private final Short eventType;

    @JsonCreator
    public ProgDeviceDataDTO(@JsonProperty("notifyFlag") final Short notifyFlag,
                             @JsonProperty("notifyDiseng") final Short notifyDiseng,
                             @JsonProperty("engageMsg") final String engageMsg,
                             @JsonProperty("disengageMsg") final String disengageMsg,
                             @JsonProperty("doorId") final Long doorId,
                             @JsonProperty("eventType") final Short eventType) {
        this.notifyFlag = notifyFlag;
        this.notifyDiseng = notifyDiseng;
        this.engageMsg = engageMsg;
        this.disengageMsg = disengageMsg;
        this.doorId = doorId;
        this.eventType = eventType;
    }

    public Short getNotifyFlag() {
        return notifyFlag;
    }

    public Short getNotifyDiseng() {
        return notifyDiseng;
    }

    public String getEngageMsg() {
        return engageMsg;
    }

    public String getDisengageMsg() {
        return disengageMsg;
    }

    public Long getDoorId() {
        return doorId;
    }

    public Short getEventType() {
        return eventType;
    }
}
