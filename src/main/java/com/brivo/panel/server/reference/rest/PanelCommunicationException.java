package com.brivo.panel.server.reference.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.GONE, reason = "Error communicating with the requested panel")
public class PanelCommunicationException extends RuntimeException {
    private static final long serialVersionUID = 8621461764065009777L;


    public PanelCommunicationException(String message) {
        super(message);
    }

}
