package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "ovr_camera_subscription", schema = "brivo20", catalog = "onair")
public class OvrCameraSubscription {
    private long ovrCameraSubscriptionId;
    private String avhsSubscriptionId;
    private String nameKey;
    private Long retentionPeriod;
    private long featureId;
    private Collection<OvrCamera> ovrCamerasByOvrCameraSubscriptionId;
    private Feature featureByFeatureId;

    @Id
    @Column(name = "ovr_camera_subscription_id", nullable = false)
    public long getOvrCameraSubscriptionId() {
        return ovrCameraSubscriptionId;
    }

    public void setOvrCameraSubscriptionId(long ovrCameraSubscriptionId) {
        this.ovrCameraSubscriptionId = ovrCameraSubscriptionId;
    }

    @Basic
    @Column(name = "avhs_subscription_id", nullable = false, length = 32)
    public String getAvhsSubscriptionId() {
        return avhsSubscriptionId;
    }

    public void setAvhsSubscriptionId(String avhsSubscriptionId) {
        this.avhsSubscriptionId = avhsSubscriptionId;
    }

    @Basic
    @Column(name = "name_key", nullable = false, length = 32)
    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    @Basic
    @Column(name = "retention_period", nullable = true)
    public Long getRetentionPeriod() {
        return retentionPeriod;
    }

    public void setRetentionPeriod(Long retentionPeriod) {
        this.retentionPeriod = retentionPeriod;
    }

    @Basic
    @Column(name = "feature_id", nullable = false)
    public long getFeatureId() {
        return featureId;
    }

    public void setFeatureId(long featureId) {
        this.featureId = featureId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        OvrCameraSubscription that = (OvrCameraSubscription) o;

        if (ovrCameraSubscriptionId != that.ovrCameraSubscriptionId) {
            return false;
        }
        if (featureId != that.featureId) {
            return false;
        }
        if (avhsSubscriptionId != null ? !avhsSubscriptionId.equals(that.avhsSubscriptionId) : that.avhsSubscriptionId != null) {
            return false;
        }
        if (nameKey != null ? !nameKey.equals(that.nameKey) : that.nameKey != null) {
            return false;
        }
        return retentionPeriod != null ? retentionPeriod.equals(that.retentionPeriod) : that.retentionPeriod == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (ovrCameraSubscriptionId ^ (ovrCameraSubscriptionId >>> 32));
        result = 31 * result + (avhsSubscriptionId != null ? avhsSubscriptionId.hashCode() : 0);
        result = 31 * result + (nameKey != null ? nameKey.hashCode() : 0);
        result = 31 * result + (retentionPeriod != null ? retentionPeriod.hashCode() : 0);
        result = 31 * result + (int) (featureId ^ (featureId >>> 32));
        return result;
    }

    @OneToMany(mappedBy = "ovrCameraSubscriptionByOvrCameraSubscriptionId")
    public Collection<OvrCamera> getOvrCamerasByOvrCameraSubscriptionId() {
        return ovrCamerasByOvrCameraSubscriptionId;
    }

    public void setOvrCamerasByOvrCameraSubscriptionId(Collection<OvrCamera> ovrCamerasByOvrCameraSubscriptionId) {
        this.ovrCamerasByOvrCameraSubscriptionId = ovrCamerasByOvrCameraSubscriptionId;
    }

    @ManyToOne
    @JoinColumn(name = "feature_id", referencedColumnName = "feature_id", nullable = false, insertable = false, updatable = false)
    public Feature getFeatureByFeatureId() {
        return featureByFeatureId;
    }

    public void setFeatureByFeatureId(Feature featureByFeatureId) {
        this.featureByFeatureId = featureByFeatureId;
    }
}
