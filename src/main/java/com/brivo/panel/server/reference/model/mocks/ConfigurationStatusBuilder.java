package com.brivo.panel.server.reference.model.mocks;

import com.brivo.panel.server.reference.model.ConfigurationStatus;
import org.springframework.beans.factory.annotation.Value;

import java.time.Instant;

public class ConfigurationStatusBuilder {

    @Value("${credential.page.size}")
    private static Integer credentialPageSize;

    @Value("${ble.auth.time.frame}")
    private static Integer bleAuthTimeFrame;

    public static ConfigurationStatus getMockConfigurationStatus() {
        ConfigurationStatus status = new ConfigurationStatus();
        status.setCredential(Instant.now().toString());
        status.setSchedule(Instant.now().toString());
        status.setHardware(Instant.now().toString());
        status.setCredentialPageSize(credentialPageSize);
        status.setRegistered(true);
        status.setBleAuthTimeFrame(bleAuthTimeFrame);

        return status;
    }
}
