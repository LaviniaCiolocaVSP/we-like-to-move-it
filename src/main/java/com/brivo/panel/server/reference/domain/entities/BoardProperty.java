package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "board_property", schema = "brivo20", catalog = "onair")
public class BoardProperty {
    private long boardObjectId;
    private long valueTypeId;
    private String nameKey;
    private String value;
    private Board boardByObjectId;
    private ValueType valueTypeByValueTypeId;

    @Id
    @Column(name = "board_object_id", nullable = false)
    public long getBoardObjectId() {
        return boardObjectId;
    }

    public void setBoardObjectId(long boardObjectId) {
        this.boardObjectId = boardObjectId;
    }

    @Basic
    @Column(name = "value_type_id", nullable = false)
    public long getValueTypeId() {
        return valueTypeId;
    }

    public void setValueTypeId(long valueTypeId) {
        this.valueTypeId = valueTypeId;
    }

    @Basic
    @Column(name = "name_key", nullable = false, length = 32)
    public String getNameKey() {
        return nameKey;
    }

    public void setNameKey(String nameKey) {
        this.nameKey = nameKey;
    }

    @Basic
    @Column(name = "value", nullable = false, length = 32)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardProperty that = (BoardProperty) o;

        if (boardObjectId != that.boardObjectId) {
            return false;
        }
        if (valueTypeId != that.valueTypeId) {
            return false;
        }
        if (nameKey != null ? !nameKey.equals(that.nameKey) : that.nameKey != null) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (boardObjectId ^ (boardObjectId >>> 32));
        result = 31 * result + (int) (valueTypeId ^ (valueTypeId >>> 32));
        result = 31 * result + (nameKey != null ? nameKey.hashCode() : 0);
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "board_object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Board getBoardByObjectId() {
        return boardByObjectId;
    }

    public void setBoardByObjectId(Board boardByObjectId) {
        this.boardByObjectId = boardByObjectId;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "value_type_id", referencedColumnName = "value_type_id", nullable = false, insertable = false, updatable = false)
    public ValueType getValueTypeByValueTypeId() {
        return valueTypeByValueTypeId;
    }

    public void setValueTypeByValueTypeId(ValueType valueTypeByValueTypeId) {
        this.valueTypeByValueTypeId = valueTypeByValueTypeId;
    }
}
