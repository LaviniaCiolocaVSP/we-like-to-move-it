package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
public class Login {
    private long loginId;
    private String firstName;
    private String lastName;
    private String emailAddress;
    private short active;
    private short locked;
    private String username;
    private String password;
    private short temporaryPassword;
    private String securityQuestion;
    private String securityAnswer;
    private String locale;
    private String timeZone;
    private short systemAdmin;
    private short canCreateGroup;
    private short autoLogin;
    private long retryCount;
    private Timestamp lastAttempted;
    private String lastAddress;
    private short deleted;
    private Long ixLoginId;
    private short forIxApi;
    private Timestamp tempPwdExpiration;
    private Timestamp expiration;
    private String resetHash;
    private Timestamp resetHashExpiration;
    private Long pwdResetRetryCount;
    private Timestamp initialLockoutTime;
    private String twoFactorToken;
    private Timestamp twoFactorTokenExpiration;
    private Long twoFactorRetryCount;
    private String twoFactorResetHash;
    private Timestamp twoFactorResetHashExp;
    private Long twoFactorAuthMethodId;
    private Short twoFactorResetToken;
    private Timestamp twoFactorResetTokenExp;
    private Long twoFactorResetRetry;
    private Collection<Identity> identitiesByLoginId;
    private TwoFactorAuthMethod twoFactorAuthMethodByTwoFactorAuthMethodId;

    @Id
    @Column(name = "login_id", nullable = false)
    public long getLoginId() {
        return loginId;
    }

    public void setLoginId(long loginId) {
        this.loginId = loginId;
    }

    @Basic
    @Column(name = "first_name", nullable = false, length = 64)
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Basic
    @Column(name = "last_name", nullable = false, length = 128)
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Basic
    @Column(name = "email_address", nullable = false, length = 512)
    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    @Basic
    @Column(name = "active", nullable = false)
    public short getActive() {
        return active;
    }

    public void setActive(short active) {
        this.active = active;
    }

    @Basic
    @Column(name = "locked", nullable = false)
    public short getLocked() {
        return locked;
    }

    public void setLocked(short locked) {
        this.locked = locked;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 32)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = false, length = 40)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "temporary_password", nullable = false)
    public short getTemporaryPassword() {
        return temporaryPassword;
    }

    public void setTemporaryPassword(short temporaryPassword) {
        this.temporaryPassword = temporaryPassword;
    }

    @Basic
    @Column(name = "security_question", nullable = true, length = 512)
    public String getSecurityQuestion() {
        return securityQuestion;
    }

    public void setSecurityQuestion(String securityQuestion) {
        this.securityQuestion = securityQuestion;
    }

    @Basic
    @Column(name = "security_answer", nullable = true, length = 512)
    public String getSecurityAnswer() {
        return securityAnswer;
    }

    public void setSecurityAnswer(String securityAnswer) {
        this.securityAnswer = securityAnswer;
    }

    @Basic
    @Column(name = "locale", nullable = false, length = 32)
    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    @Basic
    @Column(name = "time_zone", nullable = false, length = 128)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "system_admin", nullable = false)
    public short getSystemAdmin() {
        return systemAdmin;
    }

    public void setSystemAdmin(short systemAdmin) {
        this.systemAdmin = systemAdmin;
    }

    @Basic
    @Column(name = "can_create_group", nullable = false)
    public short getCanCreateGroup() {
        return canCreateGroup;
    }

    public void setCanCreateGroup(short canCreateGroup) {
        this.canCreateGroup = canCreateGroup;
    }

    @Basic
    @Column(name = "auto_login", nullable = false)
    public short getAutoLogin() {
        return autoLogin;
    }

    public void setAutoLogin(short autoLogin) {
        this.autoLogin = autoLogin;
    }

    @Basic
    @Column(name = "retry_count", nullable = false)
    public long getRetryCount() {
        return retryCount;
    }

    public void setRetryCount(long retryCount) {
        this.retryCount = retryCount;
    }

    @Basic
    @Column(name = "last_attempted", nullable = false)
    public Timestamp getLastAttempted() {
        return lastAttempted;
    }

    public void setLastAttempted(Timestamp lastAttempted) {
        this.lastAttempted = lastAttempted;
    }

    @Basic
    @Column(name = "last_address", nullable = false, length = 15)
    public String getLastAddress() {
        return lastAddress;
    }

    public void setLastAddress(String lastAddress) {
        this.lastAddress = lastAddress;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "ix_login_id", nullable = true)
    public Long getIxLoginId() {
        return ixLoginId;
    }

    public void setIxLoginId(Long ixLoginId) {
        this.ixLoginId = ixLoginId;
    }

    @Basic
    @Column(name = "for_ix_api", nullable = false)
    public short getForIxApi() {
        return forIxApi;
    }

    public void setForIxApi(short forIxApi) {
        this.forIxApi = forIxApi;
    }

    @Basic
    @Column(name = "temp_pwd_expiration", nullable = true)
    public Timestamp getTempPwdExpiration() {
        return tempPwdExpiration;
    }

    public void setTempPwdExpiration(Timestamp tempPwdExpiration) {
        this.tempPwdExpiration = tempPwdExpiration;
    }

    @Basic
    @Column(name = "expiration", nullable = true)
    public Timestamp getExpiration() {
        return expiration;
    }

    public void setExpiration(Timestamp expiration) {
        this.expiration = expiration;
    }

    @Basic
    @Column(name = "reset_hash", nullable = true, length = 36)
    public String getResetHash() {
        return resetHash;
    }

    public void setResetHash(String resetHash) {
        this.resetHash = resetHash;
    }

    @Basic
    @Column(name = "reset_hash_expiration", nullable = true)
    public Timestamp getResetHashExpiration() {
        return resetHashExpiration;
    }

    public void setResetHashExpiration(Timestamp resetHashExpiration) {
        this.resetHashExpiration = resetHashExpiration;
    }

    @Basic
    @Column(name = "pwd_reset_retry_count", nullable = true)
    public Long getPwdResetRetryCount() {
        return pwdResetRetryCount;
    }

    public void setPwdResetRetryCount(Long pwdResetRetryCount) {
        this.pwdResetRetryCount = pwdResetRetryCount;
    }

    @Basic
    @Column(name = "initial_lockout_time", nullable = true)
    public Timestamp getInitialLockoutTime() {
        return initialLockoutTime;
    }

    public void setInitialLockoutTime(Timestamp initialLockoutTime) {
        this.initialLockoutTime = initialLockoutTime;
    }

    @Basic
    @Column(name = "two_factor_token", nullable = true, length = 32)
    public String getTwoFactorToken() {
        return twoFactorToken;
    }

    public void setTwoFactorToken(String twoFactorToken) {
        this.twoFactorToken = twoFactorToken;
    }

    @Basic
    @Column(name = "two_factor_token_expiration", nullable = true)
    public Timestamp getTwoFactorTokenExpiration() {
        return twoFactorTokenExpiration;
    }

    public void setTwoFactorTokenExpiration(Timestamp twoFactorTokenExpiration) {
        this.twoFactorTokenExpiration = twoFactorTokenExpiration;
    }

    @Basic
    @Column(name = "two_factor_retry_count", nullable = true)
    public Long getTwoFactorRetryCount() {
        return twoFactorRetryCount;
    }

    public void setTwoFactorRetryCount(Long twoFactorRetryCount) {
        this.twoFactorRetryCount = twoFactorRetryCount;
    }

    @Basic
    @Column(name = "two_factor_reset_hash", nullable = true, length = 36)
    public String getTwoFactorResetHash() {
        return twoFactorResetHash;
    }

    public void setTwoFactorResetHash(String twoFactorResetHash) {
        this.twoFactorResetHash = twoFactorResetHash;
    }

    @Basic
    @Column(name = "two_factor_reset_hash_exp", nullable = true)
    public Timestamp getTwoFactorResetHashExp() {
        return twoFactorResetHashExp;
    }

    public void setTwoFactorResetHashExp(Timestamp twoFactorResetHashExp) {
        this.twoFactorResetHashExp = twoFactorResetHashExp;
    }

    @Basic
    @Column(name = "two_factor_auth_method_id", nullable = true)
    public Long getTwoFactorAuthMethodId() {
        return twoFactorAuthMethodId;
    }

    public void setTwoFactorAuthMethodId(Long twoFactorAuthMethodId) {
        this.twoFactorAuthMethodId = twoFactorAuthMethodId;
    }

    @Basic
    @Column(name = "two_factor_reset_token", nullable = true)
    public Short getTwoFactorResetToken() {
        return twoFactorResetToken;
    }

    public void setTwoFactorResetToken(Short twoFactorResetToken) {
        this.twoFactorResetToken = twoFactorResetToken;
    }

    @Basic
    @Column(name = "two_factor_reset_token_exp", nullable = true)
    public Timestamp getTwoFactorResetTokenExp() {
        return twoFactorResetTokenExp;
    }

    public void setTwoFactorResetTokenExp(Timestamp twoFactorResetTokenExp) {
        this.twoFactorResetTokenExp = twoFactorResetTokenExp;
    }

    @Basic
    @Column(name = "two_factor_reset_retry", nullable = true)
    public Long getTwoFactorResetRetry() {
        return twoFactorResetRetry;
    }

    public void setTwoFactorResetRetry(Long twoFactorResetRetry) {
        this.twoFactorResetRetry = twoFactorResetRetry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Login login = (Login) o;

        if (loginId != login.loginId) {
            return false;
        }
        if (active != login.active) {
            return false;
        }
        return locked == login.locked;
    }

    @Override
    public int hashCode() {
        int result = (int) (loginId ^ (loginId >>> 32));
        result = 31 * result + (int) active;
        result = 31 * result + (int) locked;
        return result;
    }

    @OneToMany(mappedBy = "loginByLoginId")
    public Collection<Identity> getIdentitiesByLoginId() {
        return identitiesByLoginId;
    }

    public void setIdentitiesByLoginId(Collection<Identity> identitiesByLoginId) {
        this.identitiesByLoginId = identitiesByLoginId;
    }

    @ManyToOne
    @JoinColumn(name = "two_factor_auth_method_id", referencedColumnName = "two_factor_auth_method_id", insertable = false, updatable = false)
    public TwoFactorAuthMethod getTwoFactorAuthMethodByTwoFactorAuthMethodId() {
        return twoFactorAuthMethodByTwoFactorAuthMethodId;
    }

    public void setTwoFactorAuthMethodByTwoFactorAuthMethodId(TwoFactorAuthMethod twoFactorAuthMethodByTwoFactorAuthMethodId) {
        this.twoFactorAuthMethodByTwoFactorAuthMethodId = twoFactorAuthMethodByTwoFactorAuthMethodId;
    }
}
