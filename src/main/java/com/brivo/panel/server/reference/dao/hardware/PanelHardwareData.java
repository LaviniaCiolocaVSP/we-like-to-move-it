package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.AllegionGateway;
import com.brivo.panel.server.reference.model.hardware.Board;
import com.brivo.panel.server.reference.model.hardware.Door;
import com.brivo.panel.server.reference.model.hardware.Elevator;
import com.brivo.panel.server.reference.model.hardware.EventTrackDevice;
import com.brivo.panel.server.reference.model.hardware.Floor;
import com.brivo.panel.server.reference.model.hardware.GuardTourDevice;
import com.brivo.panel.server.reference.model.hardware.KeypadDevice;
import com.brivo.panel.server.reference.model.hardware.MusteringDevice;
import com.brivo.panel.server.reference.model.hardware.PanelConfiguration;
import com.brivo.panel.server.reference.model.hardware.SaltoRouter;
import com.brivo.panel.server.reference.model.hardware.SwitchDevice;
import com.brivo.panel.server.reference.model.hardware.TimerDevice;
import com.brivo.panel.server.reference.model.hardware.TkeDataEntryDevice;
import com.brivo.panel.server.reference.model.hardware.ValidCredentialDevice;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.List;


public class PanelHardwareData {
    private PanelConfiguration panelConfig;
    private List<Board> boards;
    private List<Door> doors;
    private List<TimerDevice> timerDevices;
    private List<ValidCredentialDevice> validCredentialDevices;
    private List<SwitchDevice> switchDevices;
    private List<EventTrackDevice> eventTrackDevices;
    private List<Floor> floors;
    private List<Elevator> elevators;
    private List<SaltoRouter> saltoRouters;
    private List<AllegionGateway> allegionGateways;
    private List<MusteringDevice> musteringDevices;
    private List<GuardTourDevice> guardTourDevices;
    private List<KeypadDevice> keypadDevices;
    private List<TkeDataEntryDevice> tkeDataEntryDevices;
    private Instant lastUpdate;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public PanelConfiguration getPanelConfig() {
        return panelConfig;
    }

    public void setPanelConfig(PanelConfiguration panelConfig) {
        this.panelConfig = panelConfig;
    }

    public List<Board> getBoards() {
        return boards;
    }

    public void setBoards(List<Board> boards) {
        this.boards = boards;
    }

    public List<Door> getDoors() {
        return doors;
    }

    public void setDoors(List<Door> doors) {
        this.doors = doors;
    }

    public List<TimerDevice> getTimerDevices() {
        return timerDevices;
    }

    public void setTimerDevices(List<TimerDevice> timerDevices) {
        this.timerDevices = timerDevices;
    }

    public List<ValidCredentialDevice> getValidCredentialDevices() {
        return validCredentialDevices;
    }

    public void setValidCredentialDevices(List<ValidCredentialDevice> validCredentialDevices) {
        this.validCredentialDevices = validCredentialDevices;
    }

    public List<SwitchDevice> getSwitchDevices() {
        return switchDevices;
    }

    public void setSwitchDevices(List<SwitchDevice> switchDevices) {
        this.switchDevices = switchDevices;
    }

    public List<EventTrackDevice> getEventTrackDevices() {
        return eventTrackDevices;
    }

    public void setEventTrackDevices(List<EventTrackDevice> eventTrackDevices) {
        this.eventTrackDevices = eventTrackDevices;
    }

    public List<Floor> getFloors() {
        return floors;
    }

    public void setFloors(List<Floor> floors) {
        this.floors = floors;
    }

    public List<Elevator> getElevators() {
        return elevators;
    }

    public void setElevators(List<Elevator> elevators) {
        this.elevators = elevators;
    }

    public List<SaltoRouter> getSaltoRouters() {
        return saltoRouters;
    }

    public void setSaltoRouters(List<SaltoRouter> saltoRouter) {
        this.saltoRouters = saltoRouter;
    }

    public List<AllegionGateway> getAllegionGateways() {
        return allegionGateways;
    }

    public void setAllegionGateways(List<AllegionGateway> allegionGateways) {
        this.allegionGateways = allegionGateways;
    }

    public List<MusteringDevice> getMusteringDevices() {
        return musteringDevices;
    }

    public void setMusteringDevices(List<MusteringDevice> musteringDevices) {
        this.musteringDevices = musteringDevices;
    }

    public List<GuardTourDevice> getGuardTourDevices() {
        return guardTourDevices;
    }

    public void setGuardTourDevices(List<GuardTourDevice> guardTourDevices) {
        this.guardTourDevices = guardTourDevices;
    }

    public List<KeypadDevice> getKeypadDevices() {
        return keypadDevices;
    }

    public void setKeypadDevices(List<KeypadDevice> keypadDevices) {
        this.keypadDevices = keypadDevices;
    }

    public List<TkeDataEntryDevice> getTkeDataEntryDevices() {
        return tkeDataEntryDevices;
    }

    public void setTkeDataEntryDevices(List<TkeDataEntryDevice> tkeDataEntryDevices) {
        this.tkeDataEntryDevices = tkeDataEntryDevices;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
