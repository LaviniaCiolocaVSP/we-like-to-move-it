package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.FileNameDTO;
import com.brivo.panel.server.reference.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.util.List;

@RestController
@RequestMapping("/json")
public class AccountJSONFilesController {

    private final FileService fileService;

    @Autowired
    public AccountJSONFilesController(final FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<FileNameDTO> getAccountJSONFilesNames() {
        return fileService.getAccountJSONFilesNames();
    }

    @GetMapping(
            path = "/download/{accountName}",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity<StreamingResponseBody> download(@PathVariable final String accountName) {
        return fileService.download(accountName, true);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public MessageDTO create(@RequestParam("file") final MultipartFile file) {
        return fileService.uploadAccountFile(file);
    }

    @PostMapping(
            path = "/uploadAndUse",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public MessageDTO createAndUseAccount(@RequestParam("file") final MultipartFile file) {
        return fileService.uploadAndUseAccountFile(file);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO deleteAccount(@RequestBody final String fileName) {
        return fileService.deleteAccountFile(fileName);
    }
}
