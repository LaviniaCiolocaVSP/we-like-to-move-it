package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.BrainIpAddress;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface BrainIpAddressRepository extends CrudRepository<BrainIpAddress, Long> {
    @Query(
            value = "SELECT b.* " +
                    "FROM brivo20.brain_ip_address b " +
                    "WHERE b.electronic_serial_number = :electronicSerialNumber",
            nativeQuery = true
    )
    Optional<BrainIpAddress> findByElectronicSerialNumber(@Param("electronicSerialNumber") String electronicSerialNumber);

    @Query(
            value = "SELECT b.* " +
                    "FROM brivo20.brain_ip_address b " +
                    "WHERE b.brain_id = :brainId",
            nativeQuery = true
    )
    Optional<BrainIpAddress> findByBrainId(@Param("brainId") Long brainId);

    @Query(
            value = "SELECT coalesce(max(b.brain_ip_address_id),0) FROM brivo20.brain_ip_address b",
            nativeQuery = true
    )
    Long getMaxBrainIpAddress();
}
