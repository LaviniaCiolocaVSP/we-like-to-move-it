package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.dao.BrivoDao;
import com.brivo.panel.server.reference.model.hardware.AllegionGateway;
import com.brivo.panel.server.reference.model.hardware.AllegionLock;
import com.brivo.panel.server.reference.model.hardware.Board;
import com.brivo.panel.server.reference.model.hardware.Door;
import com.brivo.panel.server.reference.model.hardware.DoorIoPoint;
import com.brivo.panel.server.reference.model.hardware.Elevator;
import com.brivo.panel.server.reference.model.hardware.EnterPermission;
import com.brivo.panel.server.reference.model.hardware.EventTrackDevice;
import com.brivo.panel.server.reference.model.hardware.Floor;
import com.brivo.panel.server.reference.model.hardware.LockInfo;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.hardware.PanelConfiguration;
import com.brivo.panel.server.reference.model.hardware.PanelProperty;
import com.brivo.panel.server.reference.model.hardware.PanelPropertyType;
import com.brivo.panel.server.reference.model.hardware.PdAddressMap;
import com.brivo.panel.server.reference.model.hardware.Permission;
import com.brivo.panel.server.reference.model.hardware.ProgrammableDeviceOutput;
import com.brivo.panel.server.reference.model.hardware.Rs485Settings;
import com.brivo.panel.server.reference.model.hardware.SaltoLock;
import com.brivo.panel.server.reference.model.hardware.SaltoRouter;
import com.brivo.panel.server.reference.model.hardware.SwitchDevice;
import com.brivo.panel.server.reference.model.hardware.TimerDevice;
import com.brivo.panel.server.reference.model.hardware.ValidCredentialDevice;
import com.google.common.collect.Iterables;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@BrivoDao
public class HardwareDao {
    private static final Logger LOGGER = LoggerFactory.getLogger(HardwareDao.class);
    @Autowired
    PdAddressMapsExtractor pdAddressMapsExtractor;

    @Autowired
    DoorIoPointExtractor doorIoPointExtractor;

    @Autowired
    private NamedParameterJdbcTemplate template;

    @Autowired
    private BoardExtractor boardExtractor;

    @Autowired
    private TimerDeviceExtractor timerExtractor;

    @Autowired
    private ValidCredentialDeviceExtractor validCredentialExtractor;

    @Autowired
    private SwitchDeviceExtractor switchExtractor;

    @Autowired
    private EventTrackDeviceExtractor eventTrackExtractor;

    @Autowired
    private FloorExtractor floorExtractor;

    @Autowired
    private ElevatorExtractor elevatorExtractor;

    @Autowired
    private AllegionGatewayExtractor allegionGatewayExtractor;

    @Autowired
    private AllegionLockExtractor allegionLockExtractor;

    @Autowired
    private PermissionExtractor permissionExtractor;

    @Autowired
    private ProgrammableDeviceOutputExtractor progOutputExtractor;

    @Autowired
    private SaltoRouterRowMapper saltoRouterRowMapper;

    @Autowired
    private SaltoLockExtractor saltoLockExtractor;

    @Autowired
    private PanelPropertyExtractor panelPropertyExtractor;

    @Autowired
    private Rs485SettingsExtractor rs485SettingsExtractor;

    /**
     * Panel Configuration.
     *
     * @param panel
     * @return
     */
    public PanelConfiguration getPanelConfiguration(Panel panel) {
        LOGGER.trace(">>> GetPanelConfiguration");

        String query = HardwareQuery.SELECT_DEFAULT_PANEL_PROPS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brainTypeID", panel.getPanelType().getPanelTypeId());
        paramMap.put("networkID", panel.getNetworkId());
        paramMap.put("objectID", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_DEFAULT_PANEL_PROPS.name(), paramMap, query);

        List<PanelProperty> propList = template.query(query, paramMap, panelPropertyExtractor);

        PanelConfiguration config = new PanelConfiguration();
        config.setFirmwareProtocolNumber(panel.getPanelType().getFirmwareProtocol().getProtocolVal());
        config.setFirmwareVersion(panel.getFirmware().getRawValue());
        config.setRs485Settings(getRS485Settings(panel));

        for (PanelProperty prop : propList) {
            if (PanelPropertyType.LISTEN_PORT.equals(prop.getPanelPropertyType())) {
                config.setPingPort(prop.getValueAsInteger());
            } else if (PanelPropertyType.FLUSH_INTERVAL.equals(prop.getPanelPropertyType())) {
                config.setFlushInterval(prop.getValueAsInteger());
            } else if (PanelPropertyType.POLLING.equals(prop.getPanelPropertyType())) {
                config.setShouldPollForData(prop.getValueAsBoolean());
            } else if (PanelPropertyType.CACHE_EVENTS.equals(prop.getPanelPropertyType())) {
                config.setShouldCacheEvents(prop.getValueAsBoolean());
            }
        }

        LOGGER.trace("<<< GetPanelConfiguration");

        return config;
    }

    List<Rs485Settings> getRS485Settings(Panel panel) {
        String query = HardwareQuery.SELECT_RS485_SETTINGS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("brainId", panel.getBrainId());

        LOGGER.trace(
                "Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_RS485_SETTINGS.name(), paramMap, query);

        List<Rs485Settings> settings = template.query(query, paramMap, rs485SettingsExtractor);

        for (int i = 0; i < panel.getPanelType().getRs485Ports(); i++) {
            int portnum = i;
            Optional<Rs485Settings> portSettings = settings.stream()
                                                           .filter(rs485Settings -> rs485Settings.getPortNum().equals(portnum)).findFirst();

            if (!portSettings.isPresent()) {
                /**
                 * 300 panels only have 1 rs485 port and so it will be OSDP
                 * only, 6000 panels have 2 ports where the first can be OSDP or
                 * Allegion, but the second is OSDP only
                 */
                int operationMode = (portnum == 0 && panel.getPanelType().getRs485Ports() > 1)
                        ? Rs485Settings.ALLEGION_OPERATION_MODE : Rs485Settings.OSDP_OPERATION_MODE;
                settings.add(new Rs485Settings(portnum, operationMode));
            }

            for (Rs485Settings setting : settings) {
                if (setting.getOperationMode() == Rs485Settings.OSDP_OPERATION_MODE) {
                    List<PdAddressMap> pdAddressMaps = getPdAddresses(panel.getBrainId());
                    if (!pdAddressMaps.isEmpty()) {
                        setting.getOsdpSettings().setPdAddressMaps(pdAddressMaps);
                    }
                }
            }

        }

        return settings;
    }

    private List<PdAddressMap> getPdAddresses(Long brainId) {
        String query = HardwareQuery.SELECT_OSDP_POINT_ADDRESSES.getQuery();
        Map<String, Object> paramMap = Collections.singletonMap("brainId", brainId);
        List<PdAddressMap> maps = template.query(query, paramMap, pdAddressMapsExtractor);

        return maps;
    }

    /**
     * Get Boards.
     */
    public List<Board> getBoards(Panel panel) {
        LOGGER.trace(">>> GetBoards");

        String query = HardwareQuery.SELECT_BOARDS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_BOARDS.name(), paramMap, query);

        List<Board> list = template.query(query, paramMap, boardExtractor);

        LOGGER.trace("<<< GetBoards");

        return list;
    }

    public Board getBoardById(Long boardId) {
        LOGGER.trace(">>> GetBoardById");

        String query = HardwareQuery.SELECT_BOARD_BY_ID.getQuery();

        Map<String, Object> paramMap = Collections.singletonMap("boardId", boardId);

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_BOARD_BY_ID.name(), paramMap, query);

        List<Board> list = template.query(query, paramMap, (rs, rowNum) ->
        {
            Board board = new Board();

            board.setBoardId(rs.getLong("object_id"));
            board.setBoardNumber(rs.getInt("board_number"));
            board.setBoardType(rs.getInt("board_type"));

            return board;

        });

        LOGGER.trace("<<< GetBoardById");

        return Iterables.getFirst(list, null);
    }

    /**
     * Get Doors.
     */
    public List<Door> getDoors(Panel panel) {
        LOGGER.trace(">>> GetDoors");

        String query = HardwareQuery.SELECT_DOORS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_DOORS.name(), paramMap, query);

        List<Door> list = template.query(query, paramMap, new DoorExtractor(panel));

        LOGGER.trace("<<< GetDoors");

        return list;
    }

    /**
     * Return permission records for this device Id.
     */
    public List<Permission> getPermissions(Long deviceId, String panelSerialNumber) {
        LOGGER.trace(">>> GetPermissions");

        String query = HardwareQuery.SELECT_GROUP_DEVICE_PERMISSION.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceOID", deviceId);
        paramMap.put("panelSerialNumber", panelSerialNumber);

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_GROUP_DEVICE_PERMISSION.name(), paramMap, query);

        List<Permission> list = template.query(query, paramMap, permissionExtractor);

        LOGGER.trace("<<< GetPermissions");

        return list;
    }

    /**
     * Get Timer Devices.
     */
    public List<TimerDevice> getTimerDevices(Panel panel) {
        LOGGER.trace(">>> GetTimerDevices");

        String query = HardwareQuery.SELECT_TIMER_DEVICES.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_TIMER_DEVICES.name(), paramMap, query);

        List<TimerDevice> list = template.query(query, paramMap, timerExtractor);

        LOGGER.trace("<<< GetTimerDevices");

        return list;
    }

    /**
     * Return a list of the programmable outputs for the specified device.
     */
    public List<ProgrammableDeviceOutput> getDeviceProgrammableOutputs(Long deviceId) {
        LOGGER.trace(">>> GetDeviceProgrammableOutputs");

        String query = HardwareQuery.SELECT_PROGRAMMABLE_OUTPUT_POINTS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceOID", deviceId);

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_PROGRAMMABLE_OUTPUT_POINTS.name(), paramMap, query);

        List<ProgrammableDeviceOutput> list = template.query(query, paramMap, progOutputExtractor);

        LOGGER.trace("<<< GetDeviceProgrammableOutputs");

        return list;
    }

    /**
     * Get Valid Credential Devices.
     */
    public List<ValidCredentialDevice> getValidCredentialDevices(Panel panel) {
        LOGGER.trace(">>> GetValidCredentialDevices");

        String query = HardwareQuery.SELECT_VALID_CREDENTIAL_DEVICES.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_VALID_CREDENTIAL_DEVICES.name(), paramMap, query);

        List<ValidCredentialDevice> list = template.query(query, paramMap, validCredentialExtractor);

        LOGGER.trace("<<< GetValidCredentialDevices");

        return list;
    }

    /**
     * Get Switch Devices.
     */
    public List<SwitchDevice> getSwitchDevices(Panel panel) {
        LOGGER.trace(">>> GetSwitchDevices");

        String query = HardwareQuery.SELECT_SWITCH_DEVICES.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_SWITCH_DEVICES.name(), paramMap, query);

        List<SwitchDevice> list = template.query(query, paramMap, switchExtractor);

        LOGGER.trace("<<< GetSwitchDevices");

        return list;
    }

    /**
     * Get Event Track Devices.
     */
    public List<EventTrackDevice> getEventTrackDevices(Panel panel) {
        LOGGER.trace(">>> GetEventTrackDevices");

        String query = HardwareQuery.SELECT_EVENT_TRACK_DEVICES.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_EVENT_TRACK_DEVICES.name(), paramMap, query);

        List<EventTrackDevice> list = template.query(query, paramMap, eventTrackExtractor);

        LOGGER.trace("<<< GetEventTrackDevices");

        return list;
    }

    /**
     * Get Floors.
     */
    public List<Floor> getFloors(Panel panel) {
        LOGGER.trace(">>> GetFloors");

        String query = HardwareQuery.SELECT_FLOORS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_FLOORS.name(), paramMap, query);

        List<Floor> list = template.query(query, paramMap, floorExtractor);

        LOGGER.trace("<<< GetFloors");

        return list;
    }

    /**
     * Get Elevators.
     */
    public List<Elevator> getElevators(Panel panel) {
        LOGGER.trace(">>> GetElevators");

        String query = HardwareQuery.SELECT_ELEVATORS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelSerialNumber", panel.getElectronicSerialNumber());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_ELEVATORS.name(), paramMap, query);

        List<Elevator> list = template.query(query, paramMap, elevatorExtractor);

        LOGGER.trace("<<< GetElevators");

        return list;
    }

    public List<SaltoRouter> getSaltoRouters(Panel panel) {
        LOGGER.trace(">>> GetSaltoRouters");

        String query = HardwareQuery.SELECT_SALTO_ROUTER.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_SALTO_ROUTER.name(), paramMap, query);

        List<SaltoRouter> list = template.query(query, paramMap, saltoRouterRowMapper);

        LOGGER.trace("<<< GetSaltoRouters");

        return list;
    }

    public List<SaltoLock> getSaltoLocks(Panel panel) {
        LOGGER.trace(">>> GetSaltoLocks");

        String query = HardwareQuery.SELECT_SALTO_LOCKS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_SALTO_LOCKS.name(), paramMap, query);

        List<SaltoLock> list = template.query(query, paramMap, saltoLockExtractor);

        LOGGER.trace("<<< GetSaltoLocks");

        return list;
    }

    /**
     * Get Allegion Gateways.
     * <p>
     * select_allegion_gateway_board_properties.sql
     * <p>
     * TODO Allegion implementation needs to be reconsidered.
     *
     * @param panel
     * @return
     */
    public List<AllegionGateway> getAllegionGateways(Panel panel) {
        LOGGER.trace(">>> GetAllegionGateways");

        String query = HardwareQuery.SELECT_ALLEGION_GATEWAY_BOARDS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("panelOid", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_ALLEGION_GATEWAY_BOARDS.name(), paramMap, query);

        List<AllegionGateway> gatewayList = template.query(query, paramMap, allegionGatewayExtractor);
        for (AllegionGateway gateway : gatewayList) {
            gateway.setLocks(getAllegionLocks(gateway, panel));
        }

        LOGGER.trace("<<< GetAllegionGateways");

        return gatewayList;
    }

    public List<AllegionLock> getAllegionLocks(AllegionGateway gateway, Panel panel) {
        LOGGER.trace(">>> GetAllegionLocks");

        String query = HardwareQuery.SELECT_ALLEGION_LOCKS.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("boardOid", gateway.getDeviceId());
        paramMap.put("boardNum", gateway.getBoardNum());
        paramMap.put("panelObjectId", panel.getObjectId());

        LOGGER.trace("Query [{}]:\n Params [{}] {}\n", HardwareQuery.SELECT_ALLEGION_LOCKS.name(), paramMap, query);

        List<AllegionLock> lockList = template.query(query, paramMap, allegionLockExtractor);
        for (AllegionLock l : lockList) {
            List<Permission> permList = getPermissions(l.getDeviceId(), panel.getElectronicSerialNumber());
            List<EnterPermission> enterPerms = permList.stream()
                                                       .filter(permission -> permission instanceof EnterPermission)
                                                       .map(permission -> (EnterPermission) permission).collect(Collectors.toList());

            l.setEnterPermissions(enterPerms);
        }

        return lockList;
    }

    public List<DoorIoPoint> getDoorIoPoints(long deviceOid) {
        String query = HardwareQuery.SELECT_IO_POINTS_BY_DEVICE.getQuery();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceOid", deviceOid);
        List<DoorIoPoint> ioPoints = template.query(query, paramMap, doorIoPointExtractor);
        return ioPoints;
    }

    public LockInfo getLockbyObjectId(Long deviceObjectId) {
        String query = HardwareQuery.GET_ALLEGION_LOCK_BY_OBJECT_ID.getQuery();
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("deviceOid", deviceObjectId);
        LockInfo foundLock = template.query(query, paramMap, new ResultSetExtractor<LockInfo>() {

            @Override
            public LockInfo extractData(ResultSet rs) throws SQLException, DataAccessException {
                LockInfo newLockInfo = new LockInfo();
                while (rs.next()) {
                    newLockInfo.setId(deviceObjectId);
                    newLockInfo.setLockId(rs.getLong("lockId"));
                    newLockInfo.setName(rs.getString("name"));
                }
                return newLockInfo;
            }
        });
        return foundLock;
    }

    public String getPanelTimeZone(Panel panel) {
        Map<String, Long> param = Collections.singletonMap("panelOid", panel.getObjectId());
        String SQL = HardwareQuery.SELECT_PANEL_TIMEZONE.getQuery();
        return template.queryForObject(SQL, param, String.class);
    }

}