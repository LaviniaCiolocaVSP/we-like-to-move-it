package com.brivo.panel.server.reference.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.util.Collection;

public class PanelDTO extends AbstractDTO {
    private final long panelId;
    private final long objectId;
    private final long brainTypeId;
    private final String panelName;
    private final String electronicSerialNumber;
    private final String firmwareVersion;
    private final String hardwareControllerVersion;
    private final String physicalAddress;
    private final String password;
    private final Short isRegistered;
    private final String timeZone;
    private final Long networkId;
    private final LocalDateTime panelChanged;
    private final LocalDateTime personsChanged;
    private final LocalDateTime schedulesChanged;
    private final LocalDateTime panelChecked;
    private final Long antipassbackResetInterval;
    private final Long logPeriod;
    private final BrainStateDTO brainState;
    private final Collection<BoardDTO> boards;
    private final Collection<ProgIoPointDTO> progIoPoints;
    private final Collection<Rs485SettingDTO> rs485Settings;
    private final Collection<ElevatorFloorMapDTO> elevatorFloorMaps;

    @JsonCreator
    public PanelDTO(@JsonProperty("id") final long panelId, @JsonProperty("objectId") final long objectId,
                    @JsonProperty("brainTypeId") final long brainTypeId, @JsonProperty("panelName") final String panelName,
                    @JsonProperty("electronicSerialNumber") final String electronicSerialNumber,
                    @JsonProperty("firmwareVersion") final String firmwareVersion,
                    @JsonProperty("hardwareControllerVersion") final String hardwareControllerVersion,
                    @JsonProperty("physicalAddress") final String physicalAddress, @JsonProperty("password") final String password,
                    @JsonProperty("isRegistered") final Short isRegistered, @JsonProperty("timeZone") final String timeZone,
                    @JsonProperty("brainState") final BrainStateDTO brainState, @JsonProperty("networkId") final Long networkId,
                    @JsonProperty("panelChecked") final LocalDateTime panelChecked,
                    @JsonProperty("panelChanged") final LocalDateTime panelChanged,
                    @JsonProperty("personsChanged") final LocalDateTime personsChanged,
                    @JsonProperty("schedulesChanged") final LocalDateTime schedulesChanged,
                    @JsonProperty("antipassbackResetInterval") final Long antipassbackResetInterval,
                    @JsonProperty("logPeriod") final Long logPeriod,
                    @JsonProperty("boards") final Collection<BoardDTO> boards,
                    @JsonProperty("progIoPoints") final Collection<ProgIoPointDTO> progIoPoints,
                    @JsonProperty("rs485Settings") final Collection<Rs485SettingDTO> rs485Settings,
                    @JsonProperty("elevatorFloorMaps") final Collection<ElevatorFloorMapDTO> elevatorFloorMaps) {
        this.panelId = panelId;
        this.objectId = objectId;
        this.brainTypeId = brainTypeId;
        this.panelName = panelName;
        this.electronicSerialNumber = electronicSerialNumber;
        this.firmwareVersion = firmwareVersion;
        this.hardwareControllerVersion = hardwareControllerVersion;
        this.physicalAddress = physicalAddress;
        this.timeZone = timeZone;
        this.brainState = brainState;
        this.networkId = networkId;
        this.personsChanged = personsChanged;
        this.panelChanged = panelChanged;
        this.schedulesChanged = schedulesChanged;
        this.panelChecked = panelChecked;
        this.logPeriod = logPeriod;
        this.antipassbackResetInterval = antipassbackResetInterval;
        this.password = password;
        this.isRegistered = isRegistered;
        this.boards = boards;
        this.progIoPoints = progIoPoints;
        this.rs485Settings = rs485Settings;
        this.elevatorFloorMaps = elevatorFloorMaps;
    }

    @Override
    public Long getId() {
        return panelId;
    }

    public long getObjectId() {
        return objectId;
    }

    public long getBrainTypeId() {
        return brainTypeId;
    }

    public String getPanelName() {
        return panelName;
    }

    public String getElectronicSerialNumber() {
        return electronicSerialNumber;
    }

    public String getFirmwareVersion() {
        return firmwareVersion;
    }

    public String getHardwareControllerVersion() {
        return hardwareControllerVersion;
    }

    public String getPhysicalAddress() {
        return physicalAddress;
    }

    public Short getIsRegistered() {
        return isRegistered;
    }

    public String getPassword() {
        return password;
    }

    public String getTimeZone() {
        return timeZone;
    }

    public BrainStateDTO getBrainState() {
        return brainState;
    }

    public Long getNetworkId() {
        return networkId;
    }

    public LocalDateTime getPanelChanged() {
        return panelChanged;
    }

    public LocalDateTime getPersonsChanged() {
        return personsChanged;
    }

    public LocalDateTime getSchedulesChanged() {
        return schedulesChanged;
    }

    public LocalDateTime getPanelChecked() {
        return panelChecked;
    }

    public Long getAntipassbackResetInterval() {
        return antipassbackResetInterval;
    }

    public Long getLogPeriod() {
        return logPeriod;
    }

    public Collection<BoardDTO> getBoards() {
        return boards;
    }

    public Collection<ProgIoPointDTO> getProgIoPoints() {
        return progIoPoints;
    }

    public Collection<Rs485SettingDTO> getRs485Settings() {
        return rs485Settings;
    }

    public Collection<ElevatorFloorMapDTO> getElevatorFloorMaps() {
        return elevatorFloorMaps;
    }

    public static class BrainStateDTO {
        private final String ipAddress;
        private final LocalDateTime panelChecked;
        private final String firmwareVersion;

        @JsonCreator
        public BrainStateDTO(@JsonProperty("ipAddress") final String ipAddress,
                             @JsonProperty("panelChecked") final LocalDateTime panelChecked,
                             @JsonProperty("firmwareVersion") final String firmwareVersion) {
            this.ipAddress = ipAddress;
            this.panelChecked = panelChecked;
            this.firmwareVersion = firmwareVersion;
        }

        public String getIpAddress() {
            return ipAddress;
        }

        public LocalDateTime getPanelChecked() {
            return panelChecked;
        }

        public String getFirmwareVersion() {
            return firmwareVersion;
        }
    }

}
