package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.constants.Definitions;
import com.brivo.panel.server.reference.dao.board.BoardDao;
import com.brivo.panel.server.reference.dao.device.DeviceDao;
import com.brivo.panel.server.reference.domain.entities.Board;
import com.brivo.panel.server.reference.domain.entities.BoardType;
import com.brivo.panel.server.reference.domain.entities.BoardTypes;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.DoorData;
import com.brivo.panel.server.reference.domain.entities.IoPointTemplate;
import com.brivo.panel.server.reference.domain.entities.PanelType;
import com.brivo.panel.server.reference.domain.entities.ProgIoPoints;
import com.brivo.panel.server.reference.domain.entities.Rs485Settings;
import com.brivo.panel.server.reference.domain.repository.BoardRepository;
import com.brivo.panel.server.reference.domain.repository.BoardTypeRepository;
import com.brivo.panel.server.reference.domain.repository.DeviceRepository;
import com.brivo.panel.server.reference.domain.repository.DoorDataRepository;
import com.brivo.panel.server.reference.domain.repository.IOPointTemplateRepository;
import com.brivo.panel.server.reference.domain.repository.PanelRepository;
import com.brivo.panel.server.reference.domain.repository.ProgIoPointsRepository;
import com.brivo.panel.server.reference.dto.FirmwareVersionDTO;
import com.brivo.panel.server.reference.dto.IOPointTemplateDTO;
import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ProgIoPointDTO;
import com.brivo.panel.server.reference.dto.Rs485SettingDTO;
import com.brivo.panel.server.reference.dto.ui.UiBoardDTO;
import com.brivo.panel.server.reference.dto.ui.UiBoardTypeDTO;
import com.brivo.panel.server.reference.dto.ui.UiDoorBoardDTO;
import com.brivo.panel.server.reference.dto.ui.UiIOBoardDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_LONG_VALUE;
import static com.brivo.panel.server.reference.service.account.AbstractAccountCRUDComponent.NOT_AVAILABLE_SHORT_VALUE;

@Service
public class UiBoardService {
    private static final Logger LOGGER = LoggerFactory.getLogger(UiBoardService.class);

    private final BoardRepository boardRepository;
    private final BoardTypeRepository boardTypeRepository;
    private final ProgIoPointService progIoPointService;
    private final DoorDataRepository doorDataRepository;
    private final DeviceRepository deviceRepository;
    private final PanelRepository panelRepository;
    private final BoardDao boardDao;
    private final DeviceDao deviceDao;
    private final IOPointTemplateRepository ioPointTemplateRepository;

    private final UiObjectService objectService;

    Collection<Long> panelsWhereIoPointForDoorLockIs4 = Arrays.asList(
            BoardTypes.IPDC1_BOARD.getBoardTypeId(),
            BoardTypes.IPDC2_BOARD.getBoardTypeId(),
            BoardTypes.ACS6000_BOARD.getBoardTypeId(),
            BoardTypes.ACS300_BOARD.getBoardTypeId());

    Collection<Long> wirelessRouterTypes = Arrays.asList(
            BoardTypes.ALLEGION_GATEWAY.getBoardTypeId(),
            BoardTypes.SALTO_ROUTER.getBoardTypeId());


    @Autowired
    public UiBoardService(final BoardRepository boardRepository, final BoardDao boardDao, final DeviceDao deviceDao,
                          final BoardTypeRepository boardTypeRepository, final UiObjectService objectService,
                          final ProgIoPointService progIoPointService, final PanelRepository panelRepository,
                          final IOPointTemplateRepository ioPointTemplateRepository,
                          final DoorDataRepository doorDataRepository, final DeviceRepository deviceRepository) {
        this.boardRepository = boardRepository;
        this.boardDao = boardDao;
        this.boardTypeRepository = boardTypeRepository;
        this.objectService = objectService;
        this.progIoPointService = progIoPointService;
        this.panelRepository = panelRepository;
        this.ioPointTemplateRepository = ioPointTemplateRepository;
        this.deviceDao = deviceDao;
        this.doorDataRepository = doorDataRepository;
        this.deviceRepository = deviceRepository;
    }

    @Transactional(
            readOnly = true
    )
    public Collection<Board> getBoardsForPanel(final Long brainObjectId) {
        return boardRepository.findByPanelOid(brainObjectId)
                              .orElse(Collections.emptySet());
    }

    @Transactional(
            readOnly = true
    )
    public Collection<UiBoardDTO> getUiBoardDTOSForPanel(final Long brainObjectId) {
        return getBoardsForPanel(brainObjectId).stream()
                                               .map(convertUiBoardDTO())
                                               .collect(Collectors.toSet());
    }

    private Function<Board, UiBoardDTO> convertUiBoardDTO() {
        return board -> {
            final BoardType boardType = board.getBoardTypeByBoardType();

            return new UiBoardDTO(board.getBoardLocation(), boardType.getName(), boardType.getBoardTypeId(),
                    board.getBoardNumber(), board.getObjectId(), board.getPanelOid());
        };
    }

    @Transactional(
            readOnly = true,
            propagation = Propagation.REQUIRED
    )
    public Collection<UiBoardDTO.UiNodeDTO> getUiNodeDTOSForBoard(final Long boardObjectId) {
        return getNodesForBoard(boardObjectId).stream()
                                              .map(convertUiNodeDTO())
                                              .collect(Collectors.toSet());
    }

    private Collection<Integer> getNodesForBoard(final Long boardObjectId) {
        final Board board = this.getBoardByObjectId(boardObjectId);
        final long boardType = board.getBoardType();
        final BoardTypes selectedBoardType = BoardTypes.valueOf(boardType);

        //LIKE ACS
        Collection<Integer> availableNodes = new HashSet<>();
        if (boardType == BoardTypes.SALTO_ROUTER.getBoardTypeId() || boardType == BoardTypes.ALLEGION_GATEWAY.getBoardTypeId())
            availableNodes = getAvailableWirelessLocksForBoard(board);
        else
            availableNodes = getAvailableNodesForBoardType(board, selectedBoardType);

        if (shouldNodesBeFilteredByPanel(board)) {
            availableNodes = availableNodes.stream()
                                           .filter(node -> node != 1)
                                           .collect(Collectors.toList());
        }

        return availableNodes;
    }

    private boolean shouldNodesBeFilteredByPanel(final Board board) {
        final long panelOid = board.getPanelOid();

        final Brain boardPanel = getBrainByObjectId(panelOid);


        final long boardPanelTypeId = boardPanel.getBrainTypeByBrainTypeId().getBrainTypeId();
        final Collection<Rs485Settings> rs485Settings = boardPanel.getRs485SettingsByBrainId();

        if (boardPanelTypeId == BoardTypes.ACS6000_BOARD.getBoardTypeId() && board.getBoardNumber() == 1 &&

                !rs485Settings.isEmpty()) {
            Rs485Settings first = rs485Settings.iterator().next();
            return first.getOperationMode() == Rs485SettingDTO.OperationMode.ALLEGION.getVal();
        } else {
            return false;
        }
    }

    private Brain getBrainByObjectId(final Long panelOid) {
        return panelRepository.findByObjectId(panelOid)
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "There is no panel with object id " + panelOid + " in the database"));
    }

    private Long getBrainObjectIdByBrainId(final Long brainId) {
        return panelRepository.findById(brainId)
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "There is no panel with id " + brainId + " in the database"))
                              .getObjectByObjectId()
                              .getObjectId();
    }

    private Board getBoardByObjectId(final Long objectId) {
        return boardRepository.findByObjectId(objectId)
                              .orElseThrow(() -> new IllegalArgumentException(
                                      "There is no board with object id " + objectId + " in the database"));
    }

    private Collection<Integer> getAvailableNodesForBoardType(final Board board, final BoardTypes boardType) {
        final List<List<Integer>> nodeArrayList = getNodesArrayList(boardType);
        Collection<Integer> availableNodes = new HashSet<>(nodeArrayList.size());

        for (int i = 1; i <= nodeArrayList.size(); i++) {
            List<Integer> theseNodes = nodeArrayList.get(i - 1);

            Optional<List<ProgIoPoints>> progIoPointsForBoard = progIoPointService.getProgIoPointsForBoard(
                    board.getPanelOid(), board.getBoardNumber(), theseNodes);

            if (progIoPointsForBoard.isPresent()) {
                int shuntPointAddress = (i == 1) ? Definitions.AUX_RELAYA : Definitions.AUX_RELAYB;

                for (ProgIoPoints progIoPoints : progIoPointsForBoard.get()) {
                    boolean nodeAvailable = true;
                    int auxAvailable = 0;

                    Short pointAddress = progIoPoints.getPointAddress();
                    Short pointInUse = progIoPoints.getInuse();

                    if (pointInUse <= 0) {
                        if (pointAddress.intValue() == shuntPointAddress) {
                            auxAvailable = 1;
                        }
                    } else {
                        nodeAvailable = false;
                    }

                    if (nodeAvailable == true) {
                        availableNodes.add(i);
                    }
                }
            }
        }

        return availableNodes;

    }

    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public Collection<Short> getPointAddressesForBoardByNode(final Board board, final long nodeNumber) {
        final BoardTypes boardType = BoardTypes.valueOf(board.getBoardType());

        final List<List<Integer>> nodesArrayList = getNodesArrayList(boardType);
        // index 0 = node 1, index 1 = node 2
        final int nodeIndex = (int) nodeNumber - 1;

        final List<Integer> nodeArrayList = nodesArrayList.get(nodeIndex);

        Optional<List<ProgIoPoints>> progIoPointsForBoard = progIoPointService.getProgIoPointsForBoard(
                board.getPanelOid(), board.getBoardNumber(), nodeArrayList);

        if (!progIoPointsForBoard.isPresent() || progIoPointsForBoard.get().isEmpty()) {
            LOGGER.error("There are no prog io points for board {}, node {}", board.getBoardNumber(), nodeNumber);
            return Collections.emptyList();
        } else {
            return progIoPointsForBoard.get()
                                       .stream()
                                       .map(progIoPoints -> progIoPoints.getPointAddress())
                                       .collect(Collectors.toSet());
        }
    }

    private List<List<Integer>> getNodesArrayList(final BoardTypes boardType) {
        List<List<Integer>> nodeArrayList = new ArrayList<List<Integer>>(2);

        switch (boardType) {
            case DOOR_BOARD:
            case EP4502_BOARD:
            case EP1502_BOARD:
            case MR52_BOARD:
                nodeArrayList.add(convertIntArray(Definitions.NODE_ONE_SHUNT));
                nodeArrayList.add(convertIntArray(Definitions.NODE_TWO_SHUNT));
                break;
            case EDGE_BOARD:
                nodeArrayList.add(convertIntArray(Definitions.NODE_ONE_SHUNT));
                break;
            case IPDC1_BOARD:
                nodeArrayList.add(convertIntArray(Definitions.IPDC_NODE_ONE_SHUNT));
                break;
            case IPDC2_BOARD:
                nodeArrayList.add(convertIntArray(Definitions.IPDC_NODE_ONE_SHUNT));
                nodeArrayList.add(convertIntArray(Definitions.IPDC_NODE_TWO_SHUNT));
                break;
            case IPAC:
                nodeArrayList.add(convertIntArray(Definitions.IPAC_NODE_ONE_SHUNT));
                nodeArrayList.add(convertIntArray(Definitions.IPDC_NODE_TWO_SHUNT));
                break;
            case ACS300_BOARD:
            case ACS6000_BOARD:
                nodeArrayList.add(convertIntArray(Definitions.G4PANEL_NODE_ONE));
                nodeArrayList.add(convertIntArray(Definitions.G4PANEL_NODE_TWO));
                break;
            default:
                break;
        }

        return nodeArrayList;
    }

    public int[] getPointAddressesForBoardAndNode(final DoorData doorData, final Long boardTypeId, final Long nodeNumber) {
        int[] nodes = null;
        boolean isG4OrIPDC = false;
        boolean isIPAC = false;

        if (panelsWhereIoPointForDoorLockIs4.contains(boardTypeId)) {
            isG4OrIPDC = true;
        }

        if ((boardTypeId == BoardTypes.IPAC.getBoardTypeId())) {
            isIPAC = true;
        }

        if (!wirelessRouterTypes.contains(boardTypeId)) {
            if (doorData.getUseAlarmShunt() == 1) {
                if (nodeNumber == 2) {
                    if (isG4OrIPDC) {
                        nodes = Definitions.IPDC_NODE_TWO_SHUNT;
                    } else if (isIPAC) {
                        nodes = Definitions.IPAC_NODE_TWO_SHUNT;
                    } else {
                        nodes = Definitions.NODE_TWO_SHUNT;
                    }
                } else {
                    if (isG4OrIPDC) {
                        nodes = Definitions.IPDC_NODE_ONE_SHUNT;
                    } else if (isIPAC) {
                        nodes = Definitions.IPAC_NODE_ONE_SHUNT;
                    } else {
                        nodes = Definitions.NODE_ONE_SHUNT;
                    }
                }
            } else {
                if (nodeNumber == 2) {
                    if (isG4OrIPDC) {
                        nodes = Definitions.IPDC_NODE_TWO;
                    } else if (isIPAC) {
                        nodes = Definitions.IPAC_NODE_TWO;
                    } else {
                        nodes = Definitions.NODE_TWO;
                    }
                } else {
                    if (isG4OrIPDC) {
                        nodes = Definitions.IPDC_NODE_ONE;
                    } else if (isIPAC) {
                        nodes = Definitions.IPAC_NODE_ONE;
                    } else {
                        nodes = Definitions.NODE_ONE;
                    }
                }
            }
        }

        return nodes;
    }

    public void useProgIoPointsForDoor(final Device door) {
        final DoorData doorData = door.getDoorData();
        final Long boardNumber = doorData.getBoardNum();
        final Long nodeNumber = doorData.getNodeNum();
        final long panelOid = getBrainObjectIdByBrainId(door.getBrainId());
        final Board board = getBoardByPanelOidAndBoardNumber(panelOid, boardNumber);
        final Long boardTypeId = board.getBoardTypeByBoardType().getBoardTypeId();

        LOGGER.debug("Setting io points as used for door {}, on board {}, node {}", door.getName(), boardNumber, nodeNumber);

        final int[] pointAddressesForNode = getPointAddressesForBoardAndNode(doorData, boardTypeId, nodeNumber);
        progIoPointService.useIoPointsForDoor(pointAddressesForNode, panelOid, boardNumber);

        LOGGER.debug("Successfully set io points as used for door {}, on board {}, node {}", door.getName(), boardNumber, nodeNumber);
    }

    private List<Integer> convertIntArray(int[] ints) {
        List<Integer> integerList = new ArrayList<Integer>(ints.length);
        for (int i : ints) {
            integerList.add(i);
        }

        return integerList;
    }

    private Collection<Integer> getAvailableWirelessLocksForBoard(final Board board) {
        final Collection<Integer> addressesForBoard = getAllPossibleLockAddresses(board);

        addressesForBoard.removeAll(getUsedLockIds(board.getObjectId()));

        return addressesForBoard;
    }

    private Collection<Integer> getUsedLockIds(final Long boardObjectId) {
        return boardRepository.getUsedLockIds(boardObjectId)
                              .orElse(Collections.emptyList());
    }

    private Collection<Integer> getAllPossibleLockAddresses(final Board board) {
        final Collection<Integer> addressesForBoard = new HashSet<>();
        final Long boardTypeId = board.getBoardTypeByBoardType().getBoardTypeId();

        if (boardTypeId == BoardTypes.ALLEGION_GATEWAY.getBoardTypeId()) {
            final Optional<String> boardNumValue = boardRepository.getBoardNumForAllegionGateway(board.getObjectId());
            if (boardNumValue.isPresent()) {
                Integer boardNum = Integer.parseInt(boardNumValue.get());
                addressesForBoard.addAll(getAllegionLockAddresses().get(boardNum));
            }
        } else {
            List<Integer> intList = IntStream.range(1, 65).boxed().collect(Collectors.toList());
            for (Integer i : intList) {
                addressesForBoard.add(i);
            }
        }

        return addressesForBoard;
    }


    private List<ArrayList<Integer>> getAllegionLockAddresses() {
        ArrayList<Integer> addressesForBoard;
        List<ArrayList<Integer>> addresses = new ArrayList<ArrayList<Integer>>();

        for (int i = 0; i < 10; i++) {
            addressesForBoard = new ArrayList<Integer>();
            for (int j = 0; j < 10; j++) {
                int lockNum = i * 10 + j;
                addressesForBoard.add(lockNum);
            }
            addresses.add(addressesForBoard);
        }

        return addresses;
    }

    private Function<Integer, UiBoardDTO.UiNodeDTO> convertUiNodeDTO() {
        return nodeNumber -> new UiBoardDTO.UiNodeDTO(nodeNumber, "Node " + nodeNumber);
    }

    @Transactional(
            readOnly = true
    )
    public Collection<UiBoardTypeDTO> getUiBoardTypeDTOSForPanel(final Long panelOid) {
        Long panelTypeId = panelRepository.getBrainTypeIdForPanelOid(panelOid);
        PanelType panelType = PanelType.getPanelType(panelTypeId);

        Collection<BoardTypes> boardTypes = BoardTypes.getAvailableTypes(panelType);

        return boardTypes.stream().filter(boardType -> !boardType.isWirelessRouter())
                         .map(convertUiBoardTypeDTOFromBoardTypes())
                         .collect(Collectors.toSet());
    }

    private Function<BoardTypes, UiBoardTypeDTO> convertUiBoardTypeDTOFromBoardTypes() {

        return boardType -> new UiBoardTypeDTO(boardType.getBoardTypeId(), boardType.getDescription());


    }


    @Transactional(
            readOnly = true
    )
    public Collection<Integer> getAvailableBoardNumbers(final Long panelOid) {
        Collection<Board> boards = getBoardsForPanel(panelOid);

        Map<Integer, Board> usedBoardNumberMap = new HashMap<Integer, Board>();

        for (Iterator<Board> i = boards.iterator(); i.hasNext(); ) {
            Board board = i.next();
            short boardNumber = board.getBoardNumber();
            int bn = boardNumber;
            usedBoardNumberMap.put(bn, board);
        }

        Integer maxBoardNumber = 0;


        Long panelTypeId = panelRepository.getBrainTypeIdForPanelOid(panelOid);
        PanelType panelType = PanelType.getPanelType(panelTypeId);
        maxBoardNumber = panelType.getMaxNumberOfBoards();

        Collection<Integer> availableBoardNumbers = new ArrayList<>();

        for (int i = 1; i <= maxBoardNumber; i++) {
            boolean available = true;

            if ((usedBoardNumberMap.containsKey(i))) {
                // ((this.board.getBoardNumber() != null) && (this.board.getBoardNumber() != i))) {
                available = false;
            }

            if (available) {
                availableBoardNumbers.add(i);
            }
        }

        return availableBoardNumbers;
    }

    @Transactional(
            readOnly = true
    )

    public Collection<UiBoardTypeDTO> getUiBoardTypeDTOS() {
        return StreamSupport.stream(boardTypeRepository.findAll().spliterator(), false)
                            .map(convertUiBoardTypeDTO())
                            .collect(Collectors.toSet());
    }

    private Function<BoardType, UiBoardTypeDTO> convertUiBoardTypeDTO() {
        return boardType -> new UiBoardTypeDTO(boardType.getBoardTypeId(), boardType.getDescription());
    }


    @Transactional(
            propagation = Propagation.REQUIRED
    )
    public Board getBoardByPanelOidAndBoardNumber(final Long panelOid, final Long boardNumber) {
        return boardRepository.findByPanelOidAndBoardNumber(panelOid, boardNumber)
                              .orElse(getNotAvailableBoard());
    }

    private Board getNotAvailableBoard() {
        final Board board = new Board();

        board.setBoardNumber(NOT_AVAILABLE_SHORT_VALUE);

        return board;
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO create(final UiBoardDTO boardDTO) {
        final Long panelOid = boardDTO.getPanelOid();
        LOGGER.debug("Adding a new board for panel {}", panelOid);

        final Board board = createBoard(boardDTO);
        boardRepository.save(board);

        LOGGER.debug("Successfully added board to panel {}", panelOid);

        return new MessageDTO("Successfully added board having board number = " + boardDTO.getBoardNumber()
                + " to panel " + panelOid);
    }

    @Transactional(
            readOnly = false
    )
    public MessageDTO update(final UiBoardDTO boardDTO) {
        final Long objectId = boardDTO.getObjectId();
        LOGGER.debug("Updating board with object id {}", objectId);

        final Board board = updateBoard(boardDTO);
        boardRepository.save(board);

        LOGGER.debug("Successfully updated board with object id {}", objectId);

        return new MessageDTO("Successfullyupdated board with object id " + objectId);
    }

    private Board updateBoard(final UiBoardDTO boardDTO) {
        final Board originalBoard = getBoardByObjectId(boardDTO.getObjectId());

        Collection<ProgIoPointDTO> progIoPointDTOS = boardDTO.getProgIoPoints();

        Board board = convertBoard(originalBoard).apply(boardDTO);

        Set<ProgIoPoints> progIoPointsSet = progIoPointDTOS.stream()
                                                           .map(convertProgIoPoint(board))
                                                           .collect(Collectors.toSet());

        board.setProgIoPoints(progIoPointsSet);
        return board;
    }

    private Board createBoard(final UiBoardDTO boardDTO) {
        Board board = convertBoard(new Board()).apply(boardDTO);

        final Long nextObjectId = objectService.getNextObjectId();
        final BrivoObject brivoObject = new BrivoObject(BrivoObjectTypes.BOARD);
        brivoObject.setObjectId(nextObjectId);
        board.setObjectByBrivoObjectId(brivoObject);

        Collection<ProgIoPointDTO> progIoPointDTOS = boardDTO.getProgIoPoints();

        Set<ProgIoPoints> progIoPointsSet = progIoPointDTOS.stream()
                                                           .map(convertProgIoPoint(board))
                                                           .collect(Collectors.toSet());
        board.setProgIoPoints(progIoPointsSet);
        return board;
    }


    private Function<ProgIoPointDTO, ProgIoPoints> convertProgIoPoint(Board board) {
        return progIoPointDTO -> {
            ProgIoPoints progIoPoints = new ProgIoPoints();
            progIoPoints.setEol(progIoPointDTO.getEol());
            progIoPoints.setInuse(progIoPointDTO.getInuse());
            progIoPoints.setName(progIoPointDTO.getName());
            progIoPoints.setPointAddress(progIoPointDTO.getPointAddress());
            progIoPoints.setState(progIoPointDTO.getState());
            progIoPoints.setType(progIoPointDTO.getType());
            progIoPoints.setBoard(board);
            progIoPoints.setBoardNumber(board.getBoardNumber());
            progIoPoints.setPanelOid(board.getPanelOid());
            return progIoPoints;
        };
    }


    private Function<UiBoardDTO, Board> convertBoard(final Board board) {
        return boardDTO -> {
            board.setPanelOid(boardDTO.getPanelOid());

            board.setBoardLocation(boardDTO.getBoardLocation());
            board.setBoardNumber(boardDTO.getBoardNumber());

            final BoardType boardType = getBoardTypeById(boardDTO.getBoardTypeId());
            board.setBoardTypeByBoardType(boardType);

            return board;
        };
    }

    private BoardType getBoardTypeById(final Long boardTypeId) {
        return boardTypeRepository.findById(boardTypeId)
                                  .orElseThrow(() -> new IllegalArgumentException(
                                          "There is no board type with the id " + boardTypeId + " in the database"));
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteBoardsByIds(final Collection<Long> boardObjectIds) {
        final Collection<Board> boardsCollection = boardRepository.findAllByObjectId(boardObjectIds);

        return deleteBoardsCollection(boardsCollection);
    }

    @Transactional(
            readOnly = false,
            propagation = Propagation.REQUIRED
    )
    public MessageDTO deleteBoardsCollection(final Collection<Board> boards) {
        final Collection<Long> boardObjectIds = boards.stream()
                                                      .map(board -> board.getObjectId())
                                                      .collect(Collectors.toSet());

        LOGGER.debug("Deleting boards with object ids " + StringUtils.join(boardObjectIds.toArray(), "; "));

        boards.forEach(board -> {
            //FIXME delete devices for board too
            deleteDoorsForBoard(board);

            boardDao.deleteBoard(board);
            LOGGER.debug("Successfully deleted board with object id {}", board.getObjectId());
        });

        return new MessageDTO("The boards with object ids " + StringUtils.join(boardObjectIds.toArray(), "; ")
                + " were successfully deleted");
    }

    @Transactional
    public DoorData getDoorDataOrThrow(long deviceObjectId) {
        return doorDataRepository.findByObjectId(deviceObjectId)
                                 .orElseThrow(() -> new IllegalArgumentException(
                                         "Door data with device object id " + deviceObjectId + " not found in the database"));
    }

    @Transactional
    public void deleteDoorsForBoard(final Board currentBoard) {
        final long boardObjectId = currentBoard.getObjectId();
        LOGGER.debug("Deleting doors for board  with object id {}", boardObjectId);

        final Collection<Device> doorsForBoard = deviceRepository.findDoorsByBoardAndPanel(currentBoard.getBrainByPanelOid().getBrainId(), currentBoard.getBoardNumber())
                                                                 .orElse(Collections.emptyList());

        if (doorsForBoard.isEmpty()) {
            LOGGER.debug("There are no doors to delete for board with object id {}", boardObjectId);
            return;
        }

        doorsForBoard.forEach(device -> {
            final long deviceId = device.getDeviceId();
            final DoorData doorData = getDoorDataOrThrow(device.getObjectByBrivoObjectId().getObjectId());
            final Collection<Short> pointAddresses = getPointAddressesForBoardByNode(currentBoard, doorData.getNodeNum());

            deviceDao.deleteDoorById(deviceId, device.getObjectByBrivoObjectId().getObjectId(), doorData.getBoardNum(),
                    device.getBrainByBrainId().getObjectId(), pointAddresses);


            LOGGER.debug("Successfully deleted door with id {}", deviceId);
        });

        LOGGER.debug("Successfully deleted doors for board  with object id {}", boardObjectId);

    }


    private Collection<IOPointTemplateDTO> getIOPointsForBoard(long boardType) {
        List<IoPointTemplate> ioPointTemplates = ioPointTemplateRepository.getIoPointTemplateByBoardType(boardType).get();
        return ioPointTemplates.stream().map(convertIOPointTemplateDTO()).collect(Collectors.toSet());
    }

    private Function<IoPointTemplate, IOPointTemplateDTO> convertIOPointTemplateDTO() {
        return ioPointTemplate -> new IOPointTemplateDTO(ioPointTemplate.getBoardType(), ioPointTemplate.getPointAddress(),
                ioPointTemplate.getSilkscreen(),
                ioPointTemplate.getDefaultLabel(), ioPointTemplate.getType(),
                ioPointTemplate.getEol(), ioPointTemplate.getState());
    }

    public UiDoorBoardDTO getUiDoorBoardIoPointsTemplate(long panelOid, long boardTypeId) {
        String firmwareVersionByObjectId = panelRepository.getFirmwareVersionByObjectId(panelOid);
        FirmwareVersionDTO firmware = new FirmwareVersionDTO(firmwareVersionByObjectId);
        boolean failSafeStateAllowed =
                firmware.compareTo(UiDoorBoardDTO.FAILSAFE_FIRMWARE_VERSION) >= 0;
        Collection<IOPointTemplateDTO> ioPointTemplateDTOS = getIOPointsForBoard(boardTypeId);

        UiDoorBoardDTO uiDoorBoardDTO = new UiDoorBoardDTO();
        uiDoorBoardDTO.setIOPoints(ioPointTemplateDTOS, failSafeStateAllowed);
        return uiDoorBoardDTO;
    }

    public UiDoorBoardDTO getUiDoorBoardProgIoPoints(long panelOid, long boardNumber) {
        String firmwareVersionByObjectId = panelRepository.getFirmwareVersionByObjectId(panelOid);
        FirmwareVersionDTO firmware = new FirmwareVersionDTO(firmwareVersionByObjectId);
        boolean failSafeStateAllowed =
                firmware.compareTo(UiDoorBoardDTO.FAILSAFE_FIRMWARE_VERSION) >= 0;
        Collection<IOPointTemplateDTO> ioPointTemplateDTOS = getProgIOPointsForBoard(panelOid, boardNumber);
        UiDoorBoardDTO uiDoorBoardDTO = new UiDoorBoardDTO();
        uiDoorBoardDTO.setIOPoints(ioPointTemplateDTOS, failSafeStateAllowed);
        return uiDoorBoardDTO;
    }

    private Collection<IOPointTemplateDTO> getProgIOPointsForBoard(long panelOid, long boardNumber) {
        List<ProgIoPoints> progIoPointsForBoardByPanelOidAndBoardNumber =
                progIoPointService.getProgIoPointsForBoardByPanelOidAndBoardNumber(panelOid, boardNumber).get();

        return progIoPointsForBoardByPanelOidAndBoardNumber.stream()
                                                           .map(convertIOPointTemplateDTOFromProgIoPoints())
                                                           .collect(Collectors.toSet());
    }

    private Function<ProgIoPoints, IOPointTemplateDTO> convertIOPointTemplateDTOFromProgIoPoints() {
        return progIoPoints -> new IOPointTemplateDTO(1, progIoPoints.getPointAddress(),
                progIoPoints.getName(),
                progIoPoints.getName(), progIoPoints.getType(),
                progIoPoints.getEol(), progIoPoints.getState());
    }

    public UiIOBoardDTO getUiIOBoardIoPointsTemplate(long panelOid, long boardTypeId) {
        String firmwareVersionByObjectId = panelRepository.getFirmwareVersionByObjectId(panelOid);
        FirmwareVersionDTO firmware = new FirmwareVersionDTO(firmwareVersionByObjectId);
        boolean failSafeStateAllowed =
                firmware.compareTo(UiDoorBoardDTO.FAILSAFE_FIRMWARE_VERSION) >= 0;
        Collection<IOPointTemplateDTO> ioPointTemplateDTOS = getIOPointsForBoard(boardTypeId);
        UiIOBoardDTO uiIOBoardDTO = new UiIOBoardDTO();
        uiIOBoardDTO.setIOPoints(ioPointTemplateDTOS, failSafeStateAllowed);
        return uiIOBoardDTO;
    }

    public UiIOBoardDTO getUiIOBoardProgIoPoints(long panelOid, long boardNumber) {
        String firmwareVersionByObjectId = panelRepository.getFirmwareVersionByObjectId(panelOid);
        FirmwareVersionDTO firmware = new FirmwareVersionDTO(firmwareVersionByObjectId);
        boolean failSafeStateAllowed =
                firmware.compareTo(UiDoorBoardDTO.FAILSAFE_FIRMWARE_VERSION) >= 0;
        Collection<IOPointTemplateDTO> ioPointTemplateDTOS = getProgIOPointsForBoard(panelOid, boardNumber);
        UiIOBoardDTO uiIOBoardDTO = new UiIOBoardDTO();
        uiIOBoardDTO.setIOPoints(ioPointTemplateDTOS, failSafeStateAllowed);
        return uiIOBoardDTO;
    }
}
