package com.brivo.panel.server.reference.domain.entities.credential;

import java.util.HashMap;
import java.util.Map;

public enum CredentialFieldType
{
    VALUE(1, "Value"),
    PRESET_VALUE(4, "Preset Value"),
    SS(5, "Start Sentinel"),
    ES(6, "End Sentinel"),
    FS(7, "Field Separator"),
    PARITY_BIT(2, "Parity Bit"),
    BIT_MASK(3, "Bit Mask"),
    LRC(8, "Longitudinal Redundancy Check");
    
    private long id;
    private String name;
    
    private CredentialFieldType(long id, String name) {
        this.id = id;
        this.name = name;
    }
    
    @Override
    public String toString()
    {
        return name;
    }

    public String getName()
    {
        return name;
    }
    
    public long getId() {
        return id;
    }
    
    public static CredentialFieldType byId(long credentialFieldTypeId) {
        return idMap.get(credentialFieldTypeId);
    }
    
    private static Map<Long, CredentialFieldType> idMap;
    
    static {
        idMap = new HashMap<Long, CredentialFieldType>();
        
        for (CredentialFieldType fieldType : CredentialFieldType.values()) {
            idMap.put(fieldType.id, fieldType);
        }
    }
    
    
   
    
}
