package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class SetDeviceOutputBehaviorMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long deviceId;
    private Long behavior;
    private Long behaviorArgument;
    private Long boardAddress;
    private Long pointAddress;

    public SetDeviceOutputBehaviorMessage() {
        super(DownstreamMessageType.SetDeviceOutputBehaviorMessage);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    public Long getBehavior() {
        return behavior;
    }

    public void setBehavior(Long behavior) {
        this.behavior = behavior;
    }

    public Long getBehaviorArgument() {
        return behaviorArgument;
    }

    public void setBehaviorArgument(Long behaviorArgument) {
        this.behaviorArgument = behaviorArgument;
    }

    public Long getBoardAddress() {
        return boardAddress;
    }

    public void setBoardAddress(Long boardAddress) {
        this.boardAddress = boardAddress;
    }

    public Long getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(Long pointAddress) {
        this.pointAddress = pointAddress;
    }
}
