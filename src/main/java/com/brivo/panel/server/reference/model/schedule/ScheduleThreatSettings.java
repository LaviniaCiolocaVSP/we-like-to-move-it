package com.brivo.panel.server.reference.model.schedule;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ScheduleThreatSettings {
    private Integer severityCheckPoint;
    private Integer treatmentOperator;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getSeverityCheckPoint() {
        return severityCheckPoint;
    }

    public void setSeverityCheckPoint(Integer severityCheckPoint) {
        this.severityCheckPoint = severityCheckPoint;
    }

    public Integer getTreatmentOperator() {
        return treatmentOperator;
    }

    public void setTreatmentOperator(Integer treatmentOperator) {
        this.treatmentOperator = treatmentOperator;
    }
}
