package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "user_accepted_eula", schema = "brivo20", catalog = "onair")
public class UserAcceptedEula {
    private long userAcceptedEulaId;
    private long userObjectId;
    private String username;
    private long accountId;
    private long eulaId;
    private Timestamp acceptedDate;
    private Eula eulaByEulaId;

    @Id
    @Column(name = "user_accepted_eula_id", nullable = false)
    public long getUserAcceptedEulaId() {
        return userAcceptedEulaId;
    }

    public void setUserAcceptedEulaId(long userAcceptedEulaId) {
        this.userAcceptedEulaId = userAcceptedEulaId;
    }

    @Basic
    @Column(name = "user_object_id", nullable = false)
    public long getUserObjectId() {
        return userObjectId;
    }

    public void setUserObjectId(long userObjectId) {
        this.userObjectId = userObjectId;
    }

    @Basic
    @Column(name = "username", nullable = false, length = 36)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "eula_id", nullable = false)
    public long getEulaId() {
        return eulaId;
    }

    public void setEulaId(long eulaId) {
        this.eulaId = eulaId;
    }

    @Basic
    @Column(name = "accepted_date", nullable = false)
    public Timestamp getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Timestamp acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserAcceptedEula that = (UserAcceptedEula) o;

        if (userAcceptedEulaId != that.userAcceptedEulaId) {
            return false;
        }
        if (userObjectId != that.userObjectId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (eulaId != that.eulaId) {
            return false;
        }
        if (username != null ? !username.equals(that.username) : that.username != null) {
            return false;
        }
        return acceptedDate != null ? acceptedDate.equals(that.acceptedDate) : that.acceptedDate == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (userAcceptedEulaId ^ (userAcceptedEulaId >>> 32));
        result = 31 * result + (int) (userObjectId ^ (userObjectId >>> 32));
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (int) (eulaId ^ (eulaId >>> 32));
        result = 31 * result + (acceptedDate != null ? acceptedDate.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "eula_id", referencedColumnName = "eula_id", nullable = false, insertable = false, updatable = false)
    public Eula getEulaByEulaId() {
        return eulaByEulaId;
    }

    public void setEulaByEulaId(Eula eulaByEulaId) {
        this.eulaByEulaId = eulaByEulaId;
    }
}
