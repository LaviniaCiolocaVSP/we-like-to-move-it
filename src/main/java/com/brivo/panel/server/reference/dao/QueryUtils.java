/*
 * Copyright (c) 2001-2006 Brivo Systems, LLC
 * Bethesda, MD 20814
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of Brivo
 * Systems, LLC. ("Confidential Information").  You shall not
 * disclose such Confidential Information and shall use it only in
 * accordance with the terms of the license agreement you entered into
 * with Brivo.
 */
package com.brivo.panel.server.reference.dao;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import java.net.URL;
import java.util.Locale;

/**
 * This class provides utility methods for enums that implement {@code FileQuery}
 */
public class QueryUtils {

    /**
     * This method first generates the file location of the query based on its
     * package, then concatenates the query name to this location to open the
     * sql file. For example, this package (com.brivo.util) would result in a
     * return value of 'com/brivo/util/'.
     *
     * @param queryClass
     * @param queryName
     * @return A String with sql for requested query
     */
    public static String getQueryText(Class<? extends FileQuery> queryClass, String queryName) {
        String query = null;
        String queryFilename = null;
        try {
            String packageName = queryClass.getPackage().getName();
            String packagePath = packageName.replace(".", "/").concat("/");

            queryFilename = queryName.toLowerCase(Locale.getDefault()).concat(".sql");

            URL url = Resources.getResource(packagePath + queryFilename);


            query = Resources.toString(url, Charsets.UTF_8);
        } catch (Exception e) {
            e.printStackTrace();
            throw new FileQueryException("Could not load query file: " + queryFilename, e);
        }
        return query;
    }

}
