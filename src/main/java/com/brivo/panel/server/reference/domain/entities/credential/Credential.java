package com.brivo.panel.server.reference.domain.entities.credential;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Credential {
    private long credentialId;
    private String referenceId;
    
    private CredentialFormat format;
    
    private String owner;
    private long accountId;
    private String encodedCredential;
    private String unmaskedCredential;
    private Date enableOn;
    private Date expires;
    
    private String comments;
    private boolean disabled;
    private Date created;
    private Date updated;
    private boolean assignableByCurrentUser;
    
    private int numBits;
    
    private String lastOwner;
    
    private List<CredentialFieldValue> fieldValues;
    
    private boolean redeemedBPP;
    
    public Credential() {
        this(0);
    }
    
    public Credential(long credentialId) {
        this.credentialId = credentialId;
        fieldValues = new ArrayList<CredentialFieldValue>();
    }
    
    public Credential(Credential toCopy) {
        setCredentialId(toCopy.getCredentialId());
        setReferenceId(toCopy.getReferenceId());
        setFormat(new CredentialFormat(toCopy.getFormat()));
        setOwner(toCopy.getOwner());
        setAccountId(toCopy.getAccountId());
        setEncodedCredential(toCopy.getEncodedCredential());
        setComments(toCopy.getComments());
        setDisabled(toCopy.isDisabled());
        setCreated(toCopy.getCreated());
        setUpdated(toCopy.getUpdated());
        setNumBits(toCopy.getNumBits());
        setLastOwner(toCopy.getLastOwner());
        setEnableOn(toCopy.getEnableOn());
        
        if (toCopy.getFieldValues() != null) {
            fieldValues = new ArrayList<CredentialFieldValue>();
            
            for (CredentialFieldValue cfv : toCopy.getFieldValues()) {
                fieldValues.add(new CredentialFieldValue(cfv));
            }
        }
    }
    
    public void setFieldValues(List<CredentialFieldValue> credentialFieldValues) {
        this.fieldValues = credentialFieldValues;
    }
    
    
    public List<CredentialFieldValue> getFieldValues() {
        return fieldValues;
    }
    
    public void setFormat(CredentialFormat credentialFormat) {
        this.format = credentialFormat;
    }
    
    
    public CredentialFormat getFormat() {
        return format;
    }
    
    public void setLastOwner(String lastOwner) {
        this.lastOwner = lastOwner;
    }
    
    public String getLastOwner() {
        return lastOwner;
    }
    
    public void setCredentialId(long credentialId) {
        this.credentialId = credentialId;
    }
    
    public long getCredentialId() {
        return credentialId;
    }
    
    /**
     * The getCredentialId and setCredentialId methods were added to support
     * credential de-serialization from the API events end point for EB-465
     *
     * @return the credentialId
     */
    public Long getId() {
        return getCredentialId();
    }
    
    public void setId(Long id) {
        setCredentialId(id);
    }
    
    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }
    
    
    public String getReferenceId() {
        return referenceId;
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }
    
    public String getOwner() {
        return owner;
    }
    
    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }
    
    public long getAccountId() {
        return accountId;
    }
    
    public void setEncodedCredential(String encodedCredential) {
        this.encodedCredential = encodedCredential;
    }
    
    public String getEncodedCredential() {
        return encodedCredential;
    }
    
    public void setComments(String comments) {
        this.comments = comments;
    }
    
    public String getComments() {
        return comments;
    }
    
    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }
    
    public boolean isDisabled() {
        return disabled;
    }
    
    public void setCreated(Date created) {
        this.created = created;
    }
    
    public Date getCreated() {
        return created;
    }
    
    public void setUpdated(Date updated) {
        this.updated = updated;
    }
    
    public Date getUpdated() {
        return updated;
    }
    
    public void setNumBits(int numBits) {
        this.numBits = numBits;
    }
    
    public int getNumBits() {
        if (format == null) {
            return 0;
        }
        
        return format.isUnknownFormat() ? numBits : format.getNumBits();
    }
    
    
    public void setUnmaskedCredential(String unmaskedCredential) {
        this.unmaskedCredential = unmaskedCredential;
    }
    
    public String getUnmaskedCredential() {
        return unmaskedCredential == null ? encodedCredential : unmaskedCredential;
    }
    
    public boolean isAssignableByCurrentUser() {
        return assignableByCurrentUser;
    }
    
    public void setAssignableByCurrentUser(boolean assignableByCurrentUser) {
        this.assignableByCurrentUser = assignableByCurrentUser;
    }
    
    public Date getEnableOn() {
        return enableOn;
    }
    
    public void setEnableOn(Date enableOn) {
        this.enableOn = enableOn;
    }
    
    public Date getExpires() {
        return expires;
    }
    
    public void setExpires(Date expires) {
        this.expires = expires;
    }
    
    public Long getEnabledOnMillis() {
        if (this.enableOn != null) {
            return this.enableOn.getTime();
        }
        return null;
    }
    
    public Long getExpiresMillis() {
        if (this.expires != null) {
            return this.expires.getTime();
        }
        return null;
    }
    
    public boolean isRedeemedBPP() {
        return redeemedBPP;
    }
    
    public void setRedeemedBPP(boolean redeemedBPP) {
        this.redeemedBPP = redeemedBPP;
    }
}
