package com.brivo.panel.server.reference.dto;

public class MessageDTO extends AbstractDTO {

    private final String message;

    public MessageDTO(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public Integer getId() {
        return 0;
    }
}
