package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Embeddable
public class ScheduleDataPK implements Serializable {
    private Schedule scheduleByScheduleId;
    private long startTime;
    private long stopTime;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", nullable = false)
    public Schedule getScheduleByScheduleId() {
        return scheduleByScheduleId;
    }

    public void setScheduleByScheduleId(Schedule scheduleByScheduleId) {
        this.scheduleByScheduleId = scheduleByScheduleId;
    }

    @Column(name = "start_time", nullable = false)
    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    @Column(name = "stop_time", nullable = false)
    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleDataPK that = (ScheduleDataPK) o;

        if (scheduleByScheduleId.getScheduleId() != that.scheduleByScheduleId.getScheduleId()) {
            return false;
        }
        if (startTime != that.startTime) {
            return false;
        }
        return stopTime == that.stopTime;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleByScheduleId.getScheduleId() ^ (scheduleByScheduleId.getScheduleId() >>> 32));
        result = 31 * result + (int) (startTime ^ (startTime >>> 32));
        result = 31 * result + (int) (stopTime ^ (stopTime >>> 32));
        return result;
    }
}