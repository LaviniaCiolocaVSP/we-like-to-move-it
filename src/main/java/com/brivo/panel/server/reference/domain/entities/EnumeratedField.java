package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "enumerated_field", schema = "brivo20", catalog = "onair")
public class EnumeratedField {
    private long customFieldId;
    private long enumId;
    private String enumValue;
    private Collection<CustomFieldValue> customFieldValuesByEnumId;
    private Collection<CustomFieldDefinition> enumeratedFieldByCustomFieldId;
    private Collection<GroupCustomFieldValue> groupCustomFieldValuesByEnumId;

    @Basic
    @Column(name = "custom_field_id", nullable = false)
    public long getCustomFieldId() {
        return customFieldId;
    }

    public void setCustomFieldId(long customFieldId) {
        this.customFieldId = customFieldId;
    }

    @Id
    @Column(name = "enum_id", nullable = false)
    public long getEnumId() {
        return enumId;
    }

    public void setEnumId(long enumId) {
        this.enumId = enumId;
    }

    @Basic
    @Column(name = "enum_value", nullable = true, length = 32)
    public String getEnumValue() {
        return enumValue;
    }

    public void setEnumValue(String enumValue) {
        this.enumValue = enumValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EnumeratedField that = (EnumeratedField) o;

        if (customFieldId != that.customFieldId) {
            return false;
        }
        if (enumId != that.enumId) {
            return false;
        }
        return enumValue != null ? enumValue.equals(that.enumValue) : that.enumValue == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (customFieldId ^ (customFieldId >>> 32));
        result = 31 * result + (int) (enumId ^ (enumId >>> 32));
        result = 31 * result + (enumValue != null ? enumValue.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "enumeratedFieldByEnumId")
    public Collection<CustomFieldValue> getCustomFieldValuesByEnumId() {
        return customFieldValuesByEnumId;
    }

    public void setCustomFieldValuesByEnumId(Collection<CustomFieldValue> customFieldValuesByEnumId) {
        this.customFieldValuesByEnumId = customFieldValuesByEnumId;
    }

    @OneToMany(mappedBy = "enumeratedFieldByCustomFieldId")
    public Collection<CustomFieldDefinition> getEnumeratedFieldByCustomFieldId() {
        return enumeratedFieldByCustomFieldId;
    }

    public void setEnumeratedFieldByCustomFieldId(Collection<CustomFieldDefinition> enumeratedFieldByCustomFieldId) {
        this.enumeratedFieldByCustomFieldId = enumeratedFieldByCustomFieldId;
    }

    @OneToMany(mappedBy = "enumeratedFieldByEnumId")
    public Collection<GroupCustomFieldValue> getGroupCustomFieldValuesByEnumId() {
        return groupCustomFieldValuesByEnumId;
    }

    public void setGroupCustomFieldValuesByEnumId(Collection<GroupCustomFieldValue> groupCustomFieldValuesByEnumId) {
        this.groupCustomFieldValuesByEnumId = groupCustomFieldValuesByEnumId;
    }
}
