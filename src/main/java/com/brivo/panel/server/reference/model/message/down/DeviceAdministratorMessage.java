package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class DeviceAdministratorMessage extends DownstreamMessage {
    protected String panelSerialNumber;
    protected Long deviceId;
    protected Long administratorId;

    public DeviceAdministratorMessage(DownstreamMessageType downstreamMessageType) {
        super(downstreamMessageType);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(final String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Long objectId) {
        this.deviceId = objectId;
    }

    public Long getAdministratorId() {
        return administratorId;
    }

    public void setAdministratorId(Long adminId) {
        this.administratorId = adminId;
    }
}
