package com.brivo.panel.server.reference.dto;

import com.brivo.panel.server.reference.domain.entities.DeviceTypes;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Objects;

public class DoorDTO extends DeviceDTO {

    private final DoorDataDTO doorData;
    private final DoorDataAntipassbackDTO doorDataAntipassback;

    @JsonCreator
    public DoorDTO(@JsonProperty("id") final Long deviceId,
                   @JsonProperty("objectId") final Long objectId,
                   @JsonProperty("brainId") final Long brainId,
                   @JsonProperty("siteIds") final Collection<BigInteger> siteIds,
                   @JsonProperty("deviceName") final String deviceName,
                   @JsonProperty("panelId") final Long panelId,
                   @JsonProperty("bluetoothReaderId") final String bluetoothReaderId,
                   @JsonProperty("maximumREXExtension") final Short maximumREXExtension,
                   @JsonProperty("isIngress") final Short isIngress,
                   @JsonProperty("isEgress") final Short isEgress,
                   @JsonProperty("deviceUnlockScheduleIds") final Collection<Long> deviceUnlockScheduleIds,
                   @JsonProperty("twoFactorSchedule") final Long twoFactorSchedule,
                   @JsonProperty("twoFactorInterval") final Long twoFactorInterval,
                   @JsonProperty("cardRequiredSchedule") final Long cardRequiredSchedule,
                   @JsonProperty("controlFromBrowser") final Short controlFromBrowser,
                   @JsonProperty("deleted") final Short deleted,
                   @JsonProperty("progDevicePuts") final Collection<ProgDevicePutDTO> progDevicePuts,
                   @JsonProperty("progDeviceData") final Collection<ProgDeviceDataDTO> progDeviceData,
                   @JsonProperty("doorData") final DoorDataDTO doorData,
                   @JsonProperty("doorDataAntipassback") final DoorDataAntipassbackDTO doorDataAntipassback) {
        super(deviceId, objectId, brainId, DeviceTypes.DOOR_KEYPAD.getDeviceTypeID(), siteIds, deviceName, panelId,
                bluetoothReaderId, maximumREXExtension, isIngress, isEgress, deviceUnlockScheduleIds, twoFactorSchedule, twoFactorInterval,
                cardRequiredSchedule, controlFromBrowser, deleted, progDevicePuts, progDeviceData);
        this.doorData = doorData;
        this.doorDataAntipassback = doorDataAntipassback;
    }

    public DoorDataDTO getDoorData() {
        return doorData;
    }

    public DoorDataAntipassbackDTO getDoorDataAntipassback() {
        return doorDataAntipassback;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DoorDTO doorDTO = (DoorDTO) o;
        return Objects.equals(deviceId, doorDTO.deviceId) &&
                Objects.equals(objectId, doorDTO.objectId) &&
                Objects.equals(deviceName, doorDTO.deviceName) &&
                Objects.equals(brainId, doorDTO.brainId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deviceId, objectId, deviceName, brainId);
    }
}
