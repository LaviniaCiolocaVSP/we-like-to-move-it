package com.brivo.panel.server.reference.service.processors;

import com.brivo.panel.server.reference.service.processors.event.EventDataProcessor;
import com.brivo.panel.server.reference.service.processors.site.SiteIdsProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

/**
 * Handler for logging event types that do not apply to the current server or are not implemented
 */
@Service
public class EventNotHandledDataProcessor implements EventDataProcessor, SiteIdsProcessor {
    Logger logger = LoggerFactory.getLogger(EventNotHandledDataProcessor.class);

    /*@Override
    public boolean resolveEvent(EventType eventType, SecurityLogEvent securityLogEvent, Panel panel)
    {
        logger.warn(">>> " + eventType.name() + " (not handled) <<<");

        return false;
    }
*/

    @Override
    public List<Long> getSiteIds(Long objectId) {
        logger.warn(">>> No Site Id Processor defined for this event <<<");
        return Collections.emptyList();
    }
}
