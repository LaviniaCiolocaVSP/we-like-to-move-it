package com.brivo.panel.server.reference;

public class RunProfile {

    // JAR execution usage: --spring.profiles.active=<profile-name>

    public static final String DEV_MAC = "dev-mac";

    public static final String DEV_WIN = "dev-win";

    public static final String INT = "int";

    public static final String INTEGRATION_TESTING = "integration-testing";

    public static final String PROD = "prod";

    private RunProfile() {
    }
}
