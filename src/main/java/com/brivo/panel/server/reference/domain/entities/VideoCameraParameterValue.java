package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "video_camera_parameter_value", schema = "brivo20", catalog = "onair")
public class VideoCameraParameterValue {
    private long videoCameraParamValueId;
    private String value;
    private long videoCameraId;
    private long videoCameraModelParamId;
    private VideoCamera videoCameraByVideoCameraId;
    private VideoCameraModelParameter videoCameraModelParameterByVideoCameraModelParamId;

    @Id
    @Column(name = "video_camera_param_value_id", nullable = false)
    public long getVideoCameraParamValueId() {
        return videoCameraParamValueId;
    }

    public void setVideoCameraParamValueId(long videoCameraParamValueId) {
        this.videoCameraParamValueId = videoCameraParamValueId;
    }

    @Basic
    @Column(name = "value", nullable = true, length = 512)
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Basic
    @Column(name = "video_camera_id", nullable = false)
    public long getVideoCameraId() {
        return videoCameraId;
    }

    public void setVideoCameraId(long videoCameraId) {
        this.videoCameraId = videoCameraId;
    }

    @Basic
    @Column(name = "video_camera_model_param_id", nullable = false)
    public long getVideoCameraModelParamId() {
        return videoCameraModelParamId;
    }

    public void setVideoCameraModelParamId(long videoCameraModelParamId) {
        this.videoCameraModelParamId = videoCameraModelParamId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        VideoCameraParameterValue that = (VideoCameraParameterValue) o;

        if (videoCameraParamValueId != that.videoCameraParamValueId) {
            return false;
        }
        if (videoCameraId != that.videoCameraId) {
            return false;
        }
        if (videoCameraModelParamId != that.videoCameraModelParamId) {
            return false;
        }
        return value != null ? value.equals(that.value) : that.value == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (videoCameraParamValueId ^ (videoCameraParamValueId >>> 32));
        result = 31 * result + (value != null ? value.hashCode() : 0);
        result = 31 * result + (int) (videoCameraId ^ (videoCameraId >>> 32));
        result = 31 * result + (int) (videoCameraModelParamId ^ (videoCameraModelParamId >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "video_camera_id", referencedColumnName = "video_camera_id", nullable = false, insertable = false,
            updatable = false)
    public VideoCamera getVideoCameraByVideoCameraId() {
        return videoCameraByVideoCameraId;
    }

    public void setVideoCameraByVideoCameraId(VideoCamera videoCameraByVideoCameraId) {
        this.videoCameraByVideoCameraId = videoCameraByVideoCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "video_camera_model_param_id", referencedColumnName = "video_camera_model_param_id",
            nullable = false, insertable = false, updatable = false)
    public VideoCameraModelParameter getVideoCameraModelParameterByVideoCameraModelParamId() {
        return videoCameraModelParameterByVideoCameraModelParamId;
    }

    public void setVideoCameraModelParameterByVideoCameraModelParamId(VideoCameraModelParameter videoCameraModelParameterByVideoCameraModelParamId) {
        this.videoCameraModelParameterByVideoCameraModelParamId = videoCameraModelParameterByVideoCameraModelParamId;
    }
}
