package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "report_format", schema = "brivo20", catalog = "onair")
public class ReportFormat {
    private long formatId;
    private String name;
    private Collection<Report> reportsByFormatId;
    private Collection<ReportJobOutput> reportJobOutputsByFormatId;
    private Collection<ReportJobOutputFormat> reportJobOutputFormatsByFormatId;
    private Collection<ReportScheduleFormat> reportScheduleFormatsByFormatId;

    @Id
    @Column(name = "format_id", nullable = false)
    public long getFormatId() {
        return formatId;
    }

    public void setFormatId(long formatId) {
        this.formatId = formatId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportFormat that = (ReportFormat) o;

        if (formatId != that.formatId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (formatId ^ (formatId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "reportFormatByFormatId")
    public Collection<Report> getReportsByFormatId() {
        return reportsByFormatId;
    }

    public void setReportsByFormatId(Collection<Report> reportsByFormatId) {
        this.reportsByFormatId = reportsByFormatId;
    }

    @OneToMany(mappedBy = "reportFormatByReportFormatId")
    public Collection<ReportJobOutput> getReportJobOutputsByFormatId() {
        return reportJobOutputsByFormatId;
    }

    public void setReportJobOutputsByFormatId(Collection<ReportJobOutput> reportJobOutputsByFormatId) {
        this.reportJobOutputsByFormatId = reportJobOutputsByFormatId;
    }

    @OneToMany(mappedBy = "reportFormatByReportFormatId")
    public Collection<ReportJobOutputFormat> getReportJobOutputFormatsByFormatId() {
        return reportJobOutputFormatsByFormatId;
    }

    public void setReportJobOutputFormatsByFormatId(Collection<ReportJobOutputFormat> reportJobOutputFormatsByFormatId) {
        this.reportJobOutputFormatsByFormatId = reportJobOutputFormatsByFormatId;
    }

    @OneToMany(mappedBy = "reportFormatByFormatType")
    public Collection<ReportScheduleFormat> getReportScheduleFormatsByFormatId() {
        return reportScheduleFormatsByFormatId;
    }

    public void setReportScheduleFormatsByFormatId(Collection<ReportScheduleFormat> reportScheduleFormatsByFormatId) {
        this.reportScheduleFormatsByFormatId = reportScheduleFormatsByFormatId;
    }
}
