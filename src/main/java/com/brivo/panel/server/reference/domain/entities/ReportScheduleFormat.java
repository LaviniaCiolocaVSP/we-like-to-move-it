package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "report_schedule_format", schema = "brivo20", catalog = "onair")
public class ReportScheduleFormat {
    private long id;
    private long reportScheduleId;
    private long formatType;
    private ReportSchedule reportScheduleByReportScheduleId;
    private ReportFormat reportFormatByFormatType;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "report_schedule_id", nullable = false)
    public long getReportScheduleId() {
        return reportScheduleId;
    }

    public void setReportScheduleId(long reportScheduleId) {
        this.reportScheduleId = reportScheduleId;
    }

    @Basic
    @Column(name = "format_type", nullable = false)
    public long getFormatType() {
        return formatType;
    }

    public void setFormatType(long formatType) {
        this.formatType = formatType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ReportScheduleFormat that = (ReportScheduleFormat) o;

        if (id != that.id) {
            return false;
        }
        if (reportScheduleId != that.reportScheduleId) {
            return false;
        }
        return formatType == that.formatType;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (int) (reportScheduleId ^ (reportScheduleId >>> 32));
        result = 31 * result + (int) (formatType ^ (formatType >>> 32));
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "report_schedule_id", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    public ReportSchedule getReportScheduleByReportScheduleId() {
        return reportScheduleByReportScheduleId;
    }

    public void setReportScheduleByReportScheduleId(ReportSchedule reportScheduleByReportScheduleId) {
        this.reportScheduleByReportScheduleId = reportScheduleByReportScheduleId;
    }

    @ManyToOne
    @JoinColumn(name = "format_type", referencedColumnName = "format_id", nullable = false, insertable = false, updatable = false)
    public ReportFormat getReportFormatByFormatType() {
        return reportFormatByFormatType;
    }

    public void setReportFormatByFormatType(ReportFormat reportFormatByFormatType) {
        this.reportFormatByFormatType = reportFormatByFormatType;
    }
}
