package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.Collection;

@Entity
public class Manufacturer {
    private long manufacturerId;
    private String name;
    private String description;
    private Collection<BoardData> boardDataByManufacturerId;

    @Id
    @Column(name = "manufacturer_id", nullable = false)
    public long getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(long manufacturerId) {
        this.manufacturerId = manufacturerId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 50)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 100)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Manufacturer that = (Manufacturer) o;

        if (manufacturerId != that.manufacturerId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (manufacturerId ^ (manufacturerId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "manufacturerByManufacturerId")
    public Collection<BoardData> getBoardDataByManufacturerId() {
        return boardDataByManufacturerId;
    }

    public void setBoardDataByManufacturerId(Collection<BoardData> boardDataByManufacturerId) {
        this.boardDataByManufacturerId = boardDataByManufacturerId;
    }
}
