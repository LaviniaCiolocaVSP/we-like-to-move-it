package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "prog_device_data", schema = "brivo20", catalog = "onair")
public class ProgDeviceData {
    private long deviceOid;
    private Short notifyFlag;
    private Short notifyDiseng;
    private String engageMsg;
    private String disengageMsg;
    private Long doorId;
    private Short eventType;
    private Device deviceByObjectId;

    @Override
    public String toString() {
        return "ProgDeviceData{" +
                "deviceOid=" + deviceOid +
                ", notifyFlag=" + notifyFlag +
                ", notifyDiseng=" + notifyDiseng +
                ", engageMsg='" + engageMsg + '\'' +
                ", disengageMsg='" + disengageMsg + '\'' +
                ", doorId=" + doorId +
                ", eventType=" + eventType +
                ", deviceByObjectId=" + deviceByObjectId +
                '}';
    }

    @Id
    @Column(name = "device_oid", nullable = false)
    public long getDeviceOid() {
        return deviceOid;
    }

    public void setDeviceOid(long deviceOid) {
        this.deviceOid = deviceOid;
    }

    @Column(name = "notify_flag", nullable = true)
    public Short getNotifyFlag() {
        return notifyFlag;
    }

    public void setNotifyFlag(Short notifyFlag) {
        this.notifyFlag = notifyFlag;
    }

    @Column(name = "notify_diseng", nullable = true)
    public Short getNotifyDiseng() {
        return notifyDiseng;
    }

    public void setNotifyDiseng(Short notifyDiseng) {
        this.notifyDiseng = notifyDiseng;
    }

    @Column(name = "engage_msg", nullable = true, length = 30)
    public String getEngageMsg() {
        return engageMsg;
    }

    public void setEngageMsg(String engageMsg) {
        this.engageMsg = engageMsg;
    }

    @Column(name = "disengage_msg", nullable = true, length = 30)
    public String getDisengageMsg() {
        return disengageMsg;
    }

    public void setDisengageMsg(String disengageMsg) {
        this.disengageMsg = disengageMsg;
    }

    @Column(name = "door_id", nullable = true)
    public Long getDoorId() {
        return doorId;
    }

    public void setDoorId(Long doorId) {
        this.doorId = doorId;
    }

    @Column(name = "event_type", nullable = true)
    public Short getEventType() {
        return eventType;
    }

    public void setEventType(Short eventType) {
        this.eventType = eventType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ProgDeviceData that = (ProgDeviceData) o;

        if (deviceOid != that.deviceOid) {
            return false;
        }
        if (notifyFlag != null ? !notifyFlag.equals(that.notifyFlag) : that.notifyFlag != null) {
            return false;
        }
        if (notifyDiseng != null ? !notifyDiseng.equals(that.notifyDiseng) : that.notifyDiseng != null) {
            return false;
        }
        if (engageMsg != null ? !engageMsg.equals(that.engageMsg) : that.engageMsg != null) {
            return false;
        }
        if (disengageMsg != null ? !disengageMsg.equals(that.disengageMsg) : that.disengageMsg != null) {
            return false;
        }
        if (doorId != null ? !doorId.equals(that.doorId) : that.doorId != null) {
            return false;
        }
        return eventType != null ? eventType.equals(that.eventType) : that.eventType == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceOid ^ (deviceOid >>> 32));
        result = 31 * result + (notifyFlag != null ? notifyFlag.hashCode() : 0);
        result = 31 * result + (notifyDiseng != null ? notifyDiseng.hashCode() : 0);
        result = 31 * result + (engageMsg != null ? engageMsg.hashCode() : 0);
        result = 31 * result + (disengageMsg != null ? disengageMsg.hashCode() : 0);
        result = 31 * result + (doorId != null ? doorId.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "device_oid", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public Device getDeviceByObjectId() {
        return deviceByObjectId;
    }

    public void setDeviceByObjectId(Device deviceByObjectId) {
        this.deviceByObjectId = deviceByObjectId;
    }
}
