package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.CredentialFieldValue;
import org.springframework.data.repository.CrudRepository;

public interface CredentialFieldValueRepository extends CrudRepository<CredentialFieldValue, Long> {
}
