package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.model.FlushMessage;
import com.brivo.panel.server.reference.model.PanelConnectionStatus;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.message.down.FlushDataMessage;
import com.brivo.panel.server.reference.model.message.down.HeartbeatMessageDown;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

@BrivoService
public class SessionRegistryService {
    public static final String PANEL_SESSION_KEY = "panelSessionKey";
    private static final Logger LOGGER = LoggerFactory.getLogger(SessionRegistryService.class);

    private static final Integer WAIT_30_SECONDS_BEFORE_CLOSING_PANEL_SESSION = 30;

    private ConcurrentHashMap<Long, WebSocketSession> sessionMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Long, PanelConnectionStatus> panelConnectionStatusMap = new ConcurrentHashMap<>();
    private ConcurrentHashMap<Long, Heartbeat> heartbeatMap = new ConcurrentHashMap<>();

    // Buffered flush messages for panels local to this node
    private ConcurrentHashMap<Long, FlushMessage> bufferedPanelChangesMap = new ConcurrentHashMap<>();

    @Autowired
    private ApplicationContext applicationContext;

    public static Panel getPanelForSession(WebSocketSession session) {
        Object panel = session.getAttributes().get(PANEL_SESSION_KEY);

        if (panel == null) {
            return null; //FIXME do not return null; throw an exception, investigate alternatives
        }

        return (Panel) panel;
    }

    /**
     * Return a count of active sessions.
     */
    public int getSessionCount() {
        return sessionMap.size();
    }

    /**
     * Add a session to our collection.
     *
     * @param panel   The panel serial number associated with the session.
     * @param session The session.
     */
    public void add(Panel panel, WebSocketSession session) {
        sessionMap.put(panel.getObjectId(), session);
        startHeartbeat(panel);
    }

    public boolean isSessionOpen(WebSocketSession session) {
        return session != null && session.isOpen() && session.getAttributes().containsKey(PANEL_SESSION_KEY);
    }

    private void removeSession(Long panelObjectId) {
        WebSocketSession session = sessionMap.remove(panelObjectId);
        if (session != null && session.isOpen()) {
            LOGGER.debug("Attempting to close session for panelId={}", panelObjectId);
            try {
                session.close();
            } catch (IOException e) {
                LOGGER.debug("Exception closing websocket:", e);
            }
        }
    }

    public void removeBufferedPanelChange(Long panelObjectId) {
        FlushMessage bufferedPanelChange = this.bufferedPanelChangesMap.remove(panelObjectId);

        if (LOGGER.isDebugEnabled() && bufferedPanelChange != null) {
            LOGGER.debug("Removed Delayed Flush for panelId: {}", panelObjectId);
        }
    }

    /**
     * Remove a session from our collection.
     *
     * @param panelObjectId
     */
    public void remove(Long panelObjectId) {
        removeSession(panelObjectId);
        removeBufferedPanelChange(panelObjectId);
    }

    /**
     * Remove all elements of the underlying collection.
     */
    public void removeAll() {
        for (Long panelObjectId : sessionMap.keySet()) {
            // killHeartbeat(panelObjectId);
            removeSession(panelObjectId);
        }

        // Log.debug("Removed all web socket connections");
    }

    /**
     * Return an active sessions.
     */
    public WebSocketSession getWebSocketSession(Long panelObjectId) {
        return sessionMap.get(panelObjectId);
    }

    private void startHeartbeat(Panel panel) {
        LOGGER.debug("startHeartbeat - start");
        Long panelId = panel.getObjectId();

        PanelConnectionStatus status = panelConnectionStatusMap.get(panelId);
        if (status == null) {
            status = new PanelConnectionStatus(panel);
            panelConnectionStatusMap.put(panelId, status);
        } else {
            /*
             *  Re-initialize the panel's DST transition and handle any potential
             *  time zone changes to the site/panel that may have occurred since the
             *  panel was last connected to this server node.
             */
            status.setPanel(panel);
        }

        Instant now = Instant.now();
        status.setLastPanelHeartbeat(now);
        status.setLastPanelHelloMessage(now);

        LOGGER.debug("PanelConnectionStatus: {}, panelId: {}", status.toString(), panelId);
        Heartbeat newBeat = applicationContext.getBean(Heartbeat.class, panelId);
        heartbeatMap.put(panel.getObjectId(), newBeat);

        processHeartbeat(panelId);
    }


    public void processHeartbeat(Long panelId) {
        PanelConnectionStatus status = panelConnectionStatusMap.get(panelId);
        WebSocketSession session = sessionMap.get(panelId);

        if (status == null || session == null) {
            remove(panelId);
            return;
        }

        Instant lastHeartbeatSent = status.getLastServerHeartbeat();
        Instant lastHeartbeatReceived = status.getLastPanelHeartbeat();
        if (lastHeartbeatSent == null || lastHeartbeatReceived.isAfter(lastHeartbeatSent)) {
            status.setConnected(true);
            panelConnectionStatusMap.put(panelId, status);
            status.setLastServerHeartbeat(Instant.now());
            if (lastHeartbeatSent != null) {
                long millisecondsUntilResponse = lastHeartbeatSent.until(lastHeartbeatReceived, ChronoUnit.MILLIS);
            }
        } else {
            //Duration betweenHeartbeatSentAndReceived = Duration.between(lastHeartbeatReceived, lastHeartbeatSent);
          //  if (betweenHeartbeatSentAndReceived.getSeconds() > WAIT_30_SECONDS_BEFORE_CLOSING_PANEL_SESSION) {
                Panel panel = getPanelForSession(session);
                LOGGER.info("Panel {} failed to return heartbeat. Removing session artifacts", panel.getElectronicSerialNumber());
                remove(panelId);
          //  }
        }

    }

}
