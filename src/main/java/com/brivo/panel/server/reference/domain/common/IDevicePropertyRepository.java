package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.DeviceProperty;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;

public interface IDevicePropertyRepository extends CrudRepository<DeviceProperty, Long> {

    @Query(
            value = "SELECT dp.* " +
                    "FROM brivo20.device_property dp " +
                    "WHERE dp.device_id = :deviceId " +
                    "AND dp.id = :id",
            nativeQuery = true
    )
    Optional<DeviceProperty> findByDeviceId(@Param("deviceId") Long deviceId, @Param("id") String id);
}