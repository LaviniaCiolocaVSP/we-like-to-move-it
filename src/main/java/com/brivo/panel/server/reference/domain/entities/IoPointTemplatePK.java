package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class IoPointTemplatePK implements Serializable {
    private long boardType;
    private short pointAddress;

    @Column(name = "board_type", nullable = false)
    @Id
    public long getBoardType() {
        return boardType;
    }

    public void setBoardType(long boardType) {
        this.boardType = boardType;
    }

    @Column(name = "point_address", nullable = false)
    @Id
    public short getPointAddress() {
        return pointAddress;
    }

    public void setPointAddress(short pointAddress) {
        this.pointAddress = pointAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IoPointTemplatePK that = (IoPointTemplatePK) o;

        if (boardType != that.boardType) {
            return false;
        }
        return pointAddress == that.pointAddress;
    }

    @Override
    public int hashCode() {
        int result = (int) (boardType ^ (boardType >>> 32));
        result = 31 * result + (int) pointAddress;
        return result;
    }
}
