package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IDoorDataRepository;
import com.brivo.panel.server.reference.domain.entities.DoorData;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DoorDataRepository extends IDoorDataRepository {

    Optional<DoorData> findByObjectId(@Param("objectId") long objectId);
}
