package com.brivo.panel.server.reference.service;

import com.brivo.panel.server.reference.domain.entities.RelaysOpenLogger;
import com.brivo.panel.server.reference.domain.repository.RelayOpenLoggerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RelayResultService {

    private static final Logger LOGGER = LoggerFactory.getLogger(RelayResultService.class);

    private RelayOpenLoggerRepository relayOpenLoggerRepository;

    @Autowired
    public RelayResultService(final RelayOpenLoggerRepository relayOpenLoggerRepository) {
        this.relayOpenLoggerRepository = relayOpenLoggerRepository;
    }

    public void writeRelayStatus(String reader, String status) {
        RelaysOpenLogger relaysOpenLogger = new RelaysOpenLogger();

        relaysOpenLogger.setRelayName(reader);
        relaysOpenLogger.setStatus(status);

        relayOpenLoggerRepository.save(relaysOpenLogger);
    }

    public long getLatestId() {
        return relayOpenLoggerRepository.getMaxRelaysOpenLoggerId()
                .orElseThrow(() -> new IllegalArgumentException(
                "An error occured while getting max(relays_open_logger_id)"));
    }

    public List<RelaysOpenLogger> getRecords(long startId, long endId) {
        return relayOpenLoggerRepository.getAllRecordsBetween(startId, endId).orElse(null);
    }
}
