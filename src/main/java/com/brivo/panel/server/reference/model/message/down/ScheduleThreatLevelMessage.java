package com.brivo.panel.server.reference.model.message.down;

public class ScheduleThreatLevelMessage {
    private Long scheduleId;
    private Long threatLevel;

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Long getThreatLevel() {
        return threatLevel;
    }

    public void setThreatLevel(Long threatLevel) {
        this.threatLevel = threatLevel;
    }
}
