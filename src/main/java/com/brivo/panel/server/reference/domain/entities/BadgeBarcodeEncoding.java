package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "badge_barcode_encoding", schema = "brivo20", catalog = "onair")
public class BadgeBarcodeEncoding {
    private long badgeBarcodeEncodingId;
    private String name;
    private Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeBarcodeEncodingId;

    @Id
    @Column(name = "badge_barcode_encoding_id", nullable = false)
    public long getBadgeBarcodeEncodingId() {
        return badgeBarcodeEncodingId;
    }

    public void setBadgeBarcodeEncodingId(long badgeBarcodeEncodingId) {
        this.badgeBarcodeEncodingId = badgeBarcodeEncodingId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 64)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgeBarcodeEncoding that = (BadgeBarcodeEncoding) o;

        if (badgeBarcodeEncodingId != that.badgeBarcodeEncodingId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgeBarcodeEncodingId ^ (badgeBarcodeEncodingId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "badgeBarcodeEncodingByBadgeBarcodeEncodingId")
    public Collection<BadgeBarcodeItem> getBadgeBarcodeItemsByBadgeBarcodeEncodingId() {
        return badgeBarcodeItemsByBadgeBarcodeEncodingId;
    }

    public void setBadgeBarcodeItemsByBadgeBarcodeEncodingId(Collection<BadgeBarcodeItem> badgeBarcodeItemsByBadgeBarcodeEncodingId) {
        this.badgeBarcodeItemsByBadgeBarcodeEncodingId = badgeBarcodeItemsByBadgeBarcodeEncodingId;
    }
}
