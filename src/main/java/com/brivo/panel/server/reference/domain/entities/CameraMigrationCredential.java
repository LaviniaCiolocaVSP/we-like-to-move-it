package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "camera_migration_credential", schema = "brivo20", catalog = "onair")
public class CameraMigrationCredential {
    private long cameraId;
    private long cameraMigrationId;
    private String rootPassword;
    private OvrCamera ovrCameraByCameraId;
    private CameraMigration cameraMigrationByCameraMigrationId;

    @Id
    @Column(name = "camera_id", nullable = false)
    public long getCameraId() {
        return cameraId;
    }

    public void setCameraId(long cameraId) {
        this.cameraId = cameraId;
    }

    @Basic
    @Column(name = "camera_migration_id", nullable = false)
    public long getCameraMigrationId() {
        return cameraMigrationId;
    }

    public void setCameraMigrationId(long cameraMigrationId) {
        this.cameraMigrationId = cameraMigrationId;
    }

    @Basic
    @Column(name = "root_password", nullable = false, length = 32)
    public String getRootPassword() {
        return rootPassword;
    }

    public void setRootPassword(String rootPassword) {
        this.rootPassword = rootPassword;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraMigrationCredential that = (CameraMigrationCredential) o;

        if (cameraId != that.cameraId) {
            return false;
        }
        if (cameraMigrationId != that.cameraMigrationId) {
            return false;
        }
        return rootPassword != null ? rootPassword.equals(that.rootPassword) : that.rootPassword == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraId ^ (cameraId >>> 32));
        result = 31 * result + (int) (cameraMigrationId ^ (cameraMigrationId >>> 32));
        result = 31 * result + (rootPassword != null ? rootPassword.hashCode() : 0);
        return result;
    }

    @OneToOne
    @JoinColumn(name = "camera_id", referencedColumnName = "camera_id", nullable = false)
    public OvrCamera getOvrCameraByCameraId() {
        return ovrCameraByCameraId;
    }

    public void setOvrCameraByCameraId(OvrCamera ovrCameraByCameraId) {
        this.ovrCameraByCameraId = ovrCameraByCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "camera_migration_id", referencedColumnName = "camera_migration_id", nullable = false,
            insertable = false, updatable = false)
    public CameraMigration getCameraMigrationByCameraMigrationId() {
        return cameraMigrationByCameraMigrationId;
    }

    public void setCameraMigrationByCameraMigrationId(CameraMigration cameraMigrationByCameraMigrationId) {
        this.cameraMigrationByCameraMigrationId = cameraMigrationByCameraMigrationId;
    }
}
