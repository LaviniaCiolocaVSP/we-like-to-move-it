package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.common.IAccessCredentialTypeRepository;
import com.brivo.panel.server.reference.domain.entities.AccessCredentialType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AccessCredentialTypeRepository extends IAccessCredentialTypeRepository {

    @Query(
            value = "SELECT act.* " +
                    "FROM brivo20.access_credential_type act " +
                    "WHERE act.access_credential_type_id >= 100 " +
                    "AND act.access_credential_type_id != 101",
            nativeQuery = true
    )
    Optional<List<AccessCredentialType>> findCardCredentialFormats();
}
