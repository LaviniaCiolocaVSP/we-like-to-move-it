package com.brivo.panel.server.reference.dao.credential;

import com.brivo.panel.server.reference.domain.entities.credential.CredentialField;
import com.brivo.panel.server.reference.domain.entities.credential.CredentialFieldType;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * CredentialField Extractor.
 */
@Component
public class CredentialFieldRowExtractor implements ResultSetExtractor<List<CredentialField>> {
    
    @Override
    public List<CredentialField> extractData(ResultSet rs) throws SQLException, DataAccessException {
        List<CredentialField> list = new ArrayList<>();
        
        while (rs.next()) {
            CredentialField field = new CredentialField();
            field.setCredentialFieldId(rs.getLong("credential_field_id"));
            field.setOrder(rs.getInt("ordering"));
            field.setName(rs.getString("field_name"));
            
            
            Integer forReferenceInt = rs.getInt("for_reference");
            Boolean forReference = false;
            if (forReferenceInt != null && forReferenceInt == 1) {
                forReference = true;
            }
            field.setForReference(forReference);
            
            Integer displayInt = rs.getInt("display");
            Boolean display = false;
            if (displayInt != null && displayInt == 1) {
                display = true;
            }
            field.setDisplay(display);
            
            field.setLength(rs.getInt("bcd_length"));
            field.setFormatValue(rs.getString("format_value"));
            field.setCredentialFieldType(CredentialFieldType.byId(rs
                                                                          .getLong("credential_field_type_id")));
            
            list.add(field);
        }
        
        return list;
    }
    
}
