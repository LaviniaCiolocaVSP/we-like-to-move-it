package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class SecurityGroupMemberPK implements Serializable {
    private long securityGroupId;
    private long objectId;

    @Column(name = "security_group_id", nullable = false)
    @Id
    public long getSecurityGroupId() {
        return securityGroupId;
    }

    public void setSecurityGroupId(long securityGroupId) {
        this.securityGroupId = securityGroupId;
    }

    @Column(name = "object_id", nullable = false)
    @Id
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SecurityGroupMemberPK that = (SecurityGroupMemberPK) o;

        if (securityGroupId != that.securityGroupId) {
            return false;
        }
        return objectId == that.objectId;
    }

    @Override
    public int hashCode() {
        int result = (int) (securityGroupId ^ (securityGroupId >>> 32));
        result = 31 * result + (int) (objectId ^ (objectId >>> 32));
        return result;
    }
}
