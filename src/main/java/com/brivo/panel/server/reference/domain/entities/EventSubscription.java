package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "event_subscription", schema = "brivo20", catalog = "onair")
public class EventSubscription {
    private long eventSubId;
    private long accountId;
    private String name;
    private String url;
    private String errorEmail;
    private Collection<EventSubCriteria> eventSubCriteriaByEventSubId;
    private Account accountByAccountId;

    @Id
    @Column(name = "event_sub_id", nullable = false)
    public long getEventSubId() {
        return eventSubId;
    }

    public void setEventSubId(long eventSubId) {
        this.eventSubId = eventSubId;
    }

    @Basic
    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "url", nullable = false, length = 512)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "error_email", nullable = false, length = 512)
    public String getErrorEmail() {
        return errorEmail;
    }

    public void setErrorEmail(String errorEmail) {
        this.errorEmail = errorEmail;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventSubscription that = (EventSubscription) o;

        if (eventSubId != that.eventSubId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (url != null ? !url.equals(that.url) : that.url != null) {
            return false;
        }
        return errorEmail != null ? errorEmail.equals(that.errorEmail) : that.errorEmail == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (eventSubId ^ (eventSubId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (errorEmail != null ? errorEmail.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "eventSubscriptionByEventSubId")
    public Collection<EventSubCriteria> getEventSubCriteriaByEventSubId() {
        return eventSubCriteriaByEventSubId;
    }

    public void setEventSubCriteriaByEventSubId(Collection<EventSubCriteria> eventSubCriteriaByEventSubId) {
        this.eventSubCriteriaByEventSubId = eventSubCriteriaByEventSubId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
