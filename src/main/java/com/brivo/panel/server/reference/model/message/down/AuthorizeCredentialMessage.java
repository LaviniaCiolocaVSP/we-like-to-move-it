package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.credential.Credential;
import com.brivo.panel.server.reference.model.message.DownstreamMessage;
import com.brivo.panel.server.reference.model.message.DownstreamMessageType;
import com.brivo.panel.server.reference.model.usergroup.UserGroup;

import java.util.List;

public class AuthorizeCredentialMessage extends DownstreamMessage {
    private String panelSerialNumber;
    private Long queryId;
    private Credential credential;
    private List<UserGroup> userGroups;
    private Boolean authorized;
    private Long eventCode;

    public AuthorizeCredentialMessage() {
        super(DownstreamMessageType.AuthorizeCredentialMessage);
    }

    public String getPanelSerialNumber() {
        return panelSerialNumber;
    }

    public void setPanelSerialNumber(String panelSerialNumber) {
        this.panelSerialNumber = panelSerialNumber;
    }

    public Long getQueryId() {
        return queryId;
    }

    public void setQueryId(Long queryId) {
        this.queryId = queryId;
    }

    public Boolean getAuthorized() {
        return authorized;
    }

    public void setAuthorized(Boolean authorized) {
        this.authorized = authorized;
    }

    public Long getEventCode() {
        return eventCode;
    }

    public void setEventCode(Long eventCode) {
        this.eventCode = eventCode;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public List<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }
}
