package com.brivo.panel.server.reference.dao.group;

import com.brivo.panel.server.reference.dao.BrivoDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.HashMap;
import java.util.Map;

@BrivoDao
public class GroupDao {

    private static final Logger LOGGER = LoggerFactory.getLogger(GroupDao.class);

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    public void deleteGroupByObjectId(Long groupObjectId) {
        LOGGER.info("Deleting group with object id {}", groupObjectId);

        String query = GroupQuery.DELETE_GROUP.getQuery();

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("groupObjectId", groupObjectId);

        jdbcTemplate.update(query, paramMap);
        LOGGER.info("Successfully deleted group with object id {}", groupObjectId);
    }
}
