package com.brivo.panel.server.reference.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller method to handle any exception.
 *
 * @author glloyd
 */
@ControllerAdvice
public class GlobalExceptionHandlerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandlerController.class);

    @ExceptionHandler(value = Exception.class)
    public @ResponseBody
    String defaultErrorHandler(HttpServletRequest req, Exception e) {
        LOGGER.error("Exception caught by global exception handler", e);
        return e.getLocalizedMessage();
    }
}
