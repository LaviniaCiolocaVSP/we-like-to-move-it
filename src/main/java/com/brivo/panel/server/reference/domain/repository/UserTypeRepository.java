package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.UserType;
import org.springframework.data.repository.CrudRepository;

public interface UserTypeRepository extends CrudRepository<UserType, Long> {
}
