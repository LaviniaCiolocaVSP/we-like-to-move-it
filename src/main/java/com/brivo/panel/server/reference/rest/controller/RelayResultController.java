package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.service.RelayResultService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RelayResultController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RelayResultController.class);

    @Autowired
    private RelayResultService relayResultService;

    @RequestMapping(
            path = "/relayOpen/{readerId}",
            method = RequestMethod.POST
    )
    public void relayOpen(@PathVariable String readerId) {
        LOGGER.info("Received relay open message from Arduino");
        relayResultService.writeRelayStatus(readerId, "OPEN");
    }

    @RequestMapping(
            path = "/relayClosed/{readerId}",
            method = RequestMethod.POST
    )
    public void relayClosed(@PathVariable String readerId) {
        LOGGER.info("Received relay closed message from Arduino");
        relayResultService.writeRelayStatus(readerId, "CLOSED");
    }
}
