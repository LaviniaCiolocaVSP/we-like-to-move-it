package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
@Table(name = "badge_print_job", schema = "brivo20", catalog = "onair")
public class BadgePrintJob implements Serializable {
    private long badgePrintJobId;
    private long badgeTemplateId;
    private long accountId;
    private Timestamp created;
    private Timestamp updated;
    private long ownerObjectId;
    private BadgeTemplate badgeTemplateByBadgeTemplateId;
    private Account accountByAccountId;
    //    private Collection<Users> badgePrintJobByObjectId;
    private Collection<UserPrintStatus> userPrintStatusesByBadgePrintJobId;

    @Id
    @Column(name = "badge_print_job_id", nullable = false)
    public long getBadgePrintJobId() {
        return badgePrintJobId;
    }

    public void setBadgePrintJobId(long badgePrintJobId) {
        this.badgePrintJobId = badgePrintJobId;
    }

    @Column(name = "badge_template_id", nullable = false)
    public long getBadgeTemplateId() {
        return badgeTemplateId;
    }

    public void setBadgeTemplateId(long badgeTemplateId) {
        this.badgeTemplateId = badgeTemplateId;
    }

    @Column(name = "account_id", nullable = false)
    public long getAccountId() {
        return accountId;
    }

    public void setAccountId(long accountId) {
        this.accountId = accountId;
    }

    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Column(name = "owner_object_id", nullable = false)
    public long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BadgePrintJob that = (BadgePrintJob) o;

        if (badgePrintJobId != that.badgePrintJobId) {
            return false;
        }
        if (badgeTemplateId != that.badgeTemplateId) {
            return false;
        }
        if (accountId != that.accountId) {
            return false;
        }
        if (ownerObjectId != that.ownerObjectId) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        return updated != null ? updated.equals(that.updated) : that.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (badgePrintJobId ^ (badgePrintJobId >>> 32));
        result = 31 * result + (int) (badgeTemplateId ^ (badgeTemplateId >>> 32));
        result = 31 * result + (int) (accountId ^ (accountId >>> 32));
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (int) (ownerObjectId ^ (ownerObjectId >>> 32));
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "badge_template_id", referencedColumnName = "badge_template_id", nullable = false, insertable = false, updatable = false)
    public BadgeTemplate getBadgeTemplateByBadgeTemplateId() {
        return badgeTemplateByBadgeTemplateId;
    }

    public void setBadgeTemplateByBadgeTemplateId(BadgeTemplate badgeTemplateByBadgeTemplateId) {
        this.badgeTemplateByBadgeTemplateId = badgeTemplateByBadgeTemplateId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", nullable = false, insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }

//    @OneToMany(mappedBy = "badgePrintJobByObjectId")
//    public Collection<Users> getBadgePrintJobByObjectId() {
//        return badgePrintJobByObjectId;
//    }
//
//    public void setBadgePrintJobByObjectId(Collection<Users> badgePrintJobByObjectId) {
//        this.badgePrintJobByObjectId = badgePrintJobByObjectId;
//    }

    @OneToMany(mappedBy = "badgePrintJobByBadgePrintJobId")
    public Collection<UserPrintStatus> getUserPrintStatusesByBadgePrintJobId() {
        return userPrintStatusesByBadgePrintJobId;
    }

    public void setUserPrintStatusesByBadgePrintJobId(Collection<UserPrintStatus> userPrintStatusesByBadgePrintJobId) {
        this.userPrintStatusesByBadgePrintJobId = userPrintStatusesByBadgePrintJobId;
    }
}
