package com.brivo.panel.server.reference.dao.event;

import com.brivo.panel.server.reference.model.event.EventResolutionData;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Extractor for event resolution queries
 *
 * @author brandon
 */
@Component
public class EventResolutionExtractor
        implements ResultSetExtractor<List<EventResolutionData>> {
    @Override
    public List<EventResolutionData> extractData(ResultSet rs)
            throws SQLException, DataAccessException {
        List<EventResolutionData> list = new ArrayList<>();

        while (rs.next()) {
            Long accountId = rs.getLong("account_id");
            Long objectId = rs.getLong("object_id");
            String fullName = rs.getString("full_name");
            Long deviceOid = rs.getLong("device_oid");
            Long objectTypeId = rs.getLong("object_type_id");
            String deviceName = rs.getString("device_name");
            Long siteOid = rs.getLong("site_oid");
            String securityGroupName = rs.getString("security_group_name");
            Long deviceTypeId = rs.getLong("device_type_id");
            String engageMsg = rs.getString("engage_msg");
            String disEngageMsg = rs.getString("disengage_msg");

            EventResolutionData data = new EventResolutionData();

            data.setAccountId(accountId);
            data.setObjectId(objectId);
            data.setFullName(fullName);
            data.setDeviceOid(deviceOid);
            data.setObjectTypeId(objectTypeId);
            data.setDeviceName(deviceName);
            data.setSiteOid(siteOid);
            data.setSecurityGroupName(securityGroupName);
            data.setDeviceTypeId(deviceTypeId);
            data.setEngageMessage(engageMsg);
            data.setDisengageMessage(disEngageMsg);


            list.add(data);
        }

        return list;
    }
}
