package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "smartvue_nvr_registration", schema = "brivo20", catalog = "onair")
public class SmartvueNvrRegistration {
    private long registrationId;
    private long smartvueNvrId;
    private long videoProviderId;
    private long deleted;
    private Timestamp updated;
    private Timestamp created;
    private SmartvueNvrManufacture smartvueNvrManufactureBySmartvueNvrId;
    private VideoProvider videoProviderByVideoProviderId;

    @Id
    @Column(name = "registration_id", nullable = false)
    public long getRegistrationId() {
        return registrationId;
    }

    public void setRegistrationId(long registrationId) {
        this.registrationId = registrationId;
    }

    @Basic
    @Column(name = "smartvue_nvr_id", nullable = false)
    public long getSmartvueNvrId() {
        return smartvueNvrId;
    }

    public void setSmartvueNvrId(long smartvueNvrId) {
        this.smartvueNvrId = smartvueNvrId;
    }

    @Basic
    @Column(name = "video_provider_id", nullable = false)
    public long getVideoProviderId() {
        return videoProviderId;
    }

    public void setVideoProviderId(long videoProviderId) {
        this.videoProviderId = videoProviderId;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public long getDeleted() {
        return deleted;
    }

    public void setDeleted(long deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "updated", nullable = false)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SmartvueNvrRegistration that = (SmartvueNvrRegistration) o;

        if (registrationId != that.registrationId) {
            return false;
        }
        if (smartvueNvrId != that.smartvueNvrId) {
            return false;
        }
        if (videoProviderId != that.videoProviderId) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        return created != null ? created.equals(that.created) : that.created == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (registrationId ^ (registrationId >>> 32));
        result = 31 * result + (int) (smartvueNvrId ^ (smartvueNvrId >>> 32));
        result = 31 * result + (int) (videoProviderId ^ (videoProviderId >>> 32));
        result = 31 * result + (int) (deleted ^ (deleted >>> 32));
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "smartvue_nvr_id", referencedColumnName = "smartvue_nvr_id", nullable = false, insertable = false, updatable = false)
    public SmartvueNvrManufacture getSmartvueNvrManufactureBySmartvueNvrId() {
        return smartvueNvrManufactureBySmartvueNvrId;
    }

    public void setSmartvueNvrManufactureBySmartvueNvrId(SmartvueNvrManufacture smartvueNvrManufactureBySmartvueNvrId) {
        this.smartvueNvrManufactureBySmartvueNvrId = smartvueNvrManufactureBySmartvueNvrId;
    }

    @ManyToOne
    @JoinColumn(name = "video_provider_id", referencedColumnName = "video_provider_id", nullable = false, insertable = false, updatable = false)
    public VideoProvider getVideoProviderByVideoProviderId() {
        return videoProviderByVideoProviderId;
    }

    public void setVideoProviderByVideoProviderId(VideoProvider videoProviderByVideoProviderId) {
        this.videoProviderByVideoProviderId = videoProviderByVideoProviderId;
    }
}
