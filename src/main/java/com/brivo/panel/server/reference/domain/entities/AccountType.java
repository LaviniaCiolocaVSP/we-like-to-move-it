package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "account_type", schema = "brivo20", catalog = "onair")
public class AccountType {
    private long accountTypeId;
    private String name;
    private String description;
    private Collection<Account> accountsByAccountTypeId;
    private Collection<SsoApplication> ssoApplicationsByAccountTypeId;

    @Id
    @Column(name = "account_type_id", nullable = false)
    public long getAccountTypeId() {
        return accountTypeId;
    }

    public void setAccountTypeId(long accountTypeId) {
        this.accountTypeId = accountTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AccountType that = (AccountType) o;

        if (accountTypeId != that.accountTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (accountTypeId ^ (accountTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "accountTypeByAccountTypeId")
    public Collection<Account> getAccountsByAccountTypeId() {
        return accountsByAccountTypeId;
    }

    public void setAccountsByAccountTypeId(Collection<Account> accountsByAccountTypeId) {
        this.accountsByAccountTypeId = accountsByAccountTypeId;
    }

    @OneToMany(mappedBy = "accountTypeByAccountTypeId")
    public Collection<SsoApplication> getSsoApplicationsByAccountTypeId() {
        return ssoApplicationsByAccountTypeId;
    }

    public void setSsoApplicationsByAccountTypeId(Collection<SsoApplication> ssoApplicationsByAccountTypeId) {
        this.ssoApplicationsByAccountTypeId = ssoApplicationsByAccountTypeId;
    }
}
