package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "event_sub_key_type", schema = "brivo20", catalog = "onair")
public class EventSubKeyType {
    private long keyTypeId;
    private String name;
    private Collection<EventSubCriteria> eventSubCriteriaByKeyTypeId;

    @Id
    @Column(name = "key_type_id", nullable = false)
    public long getKeyTypeId() {
        return keyTypeId;
    }

    public void setKeyTypeId(long keyTypeId) {
        this.keyTypeId = keyTypeId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        EventSubKeyType that = (EventSubKeyType) o;

        if (keyTypeId != that.keyTypeId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (keyTypeId ^ (keyTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "eventSubKeyTypeByKeyTypeId")
    public Collection<EventSubCriteria> getEventSubCriteriaByKeyTypeId() {
        return eventSubCriteriaByKeyTypeId;
    }

    public void setEventSubCriteriaByKeyTypeId(Collection<EventSubCriteria> eventSubCriteriaByKeyTypeId) {
        this.eventSubCriteriaByKeyTypeId = eventSubCriteriaByKeyTypeId;
    }
}
