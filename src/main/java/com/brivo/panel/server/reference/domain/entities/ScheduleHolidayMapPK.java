package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class ScheduleHolidayMapPK implements Serializable {
    private long scheduleOid;
    private long holidayOid;

    @Column(name = "schedule_oid", nullable = false)
    @Id
    public long getScheduleOid() {
        return scheduleOid;
    }

    public void setScheduleOid(long scheduleOid) {
        this.scheduleOid = scheduleOid;
    }

    @Column(name = "holiday_oid", nullable = false)
    @Id
    public long getHolidayOid() {
        return holidayOid;
    }

    public void setHolidayOid(long holidayOid) {
        this.holidayOid = holidayOid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleHolidayMapPK that = (ScheduleHolidayMapPK) o;

        if (scheduleOid != that.scheduleOid) {
            return false;
        }
        return holidayOid == that.holidayOid;
    }

    @Override
    public int hashCode() {
        int result = (int) (scheduleOid ^ (scheduleOid >>> 32));
        result = 31 * result + (int) (holidayOid ^ (holidayOid >>> 32));
        return result;
    }
}
