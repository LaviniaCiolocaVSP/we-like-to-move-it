package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "board_type", schema = "brivo20", catalog = "onair")
public class BoardType {
    private long boardTypeId;
    private String name;
    private String description;
    private Collection<Board> boardsByBoardTypeId;

    @Id
    @Column(name = "board_type_id", nullable = false)
    public long getBoardTypeId() {
        return boardTypeId;
    }

    public void setBoardTypeId(long boardTypeId) {
        this.boardTypeId = boardTypeId;
    }

    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "description", nullable = false, length = 256)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BoardType boardType = (BoardType) o;

        if (boardTypeId != boardType.boardTypeId) {
            return false;
        }
        if (name != null ? !name.equals(boardType.name) : boardType.name != null) {
            return false;
        }
        return description != null ? description.equals(boardType.description) : boardType.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (boardTypeId ^ (boardTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "boardTypeByBoardType")
    public Collection<Board> getBoardsByBoardTypeId() {
        return boardsByBoardTypeId;
    }

    public void setBoardsByBoardTypeId(Collection<Board> boardsByBoardTypeId) {
        this.boardsByBoardTypeId = boardsByBoardTypeId;
    }
}
