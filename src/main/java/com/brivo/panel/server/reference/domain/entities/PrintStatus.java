package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "print_status", schema = "brivo20", catalog = "onair")
public class PrintStatus {
    private long printStatusId;
    private String name;
    private Collection<UserPrintStatus> userPrintStatusesByPrintStatusId;

    @Id
    @Column(name = "print_status_id", nullable = false)
    public long getPrintStatusId() {
        return printStatusId;
    }

    public void setPrintStatusId(long printStatusId) {
        this.printStatusId = printStatusId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        PrintStatus that = (PrintStatus) o;

        if (printStatusId != that.printStatusId) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (printStatusId ^ (printStatusId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "printStatusByPrintStatusId")
    public Collection<UserPrintStatus> getUserPrintStatusesByPrintStatusId() {
        return userPrintStatusesByPrintStatusId;
    }

    public void setUserPrintStatusesByPrintStatusId(Collection<UserPrintStatus> userPrintStatusesByPrintStatusId) {
        this.userPrintStatusesByPrintStatusId = userPrintStatusesByPrintStatusId;
    }
}
