package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "ipac_data", schema = "brivo20", catalog = "onair")
public class IpacData {
    private long deviceId;
    private String sipDomain;
    private String sipUsername;
    private String sipPassword;
    private String sipAuthId;
    private long sipPort;
    private long maxWaitForCallEstablish;
    private long maxCallTime;
    private String ipacGreetingMsg;
    private String gate1DtmfCode;
    private String gate2DtmfCode;
    private short enableSipDiagnostic;
    private Long gate1DeviceId;
    private Long gate2DeviceId;
    private short gate1AcceptPin;
    private short gate2AcceptPin;
    private String outboundProxy;
    private String stunServer;
    private Long speakerVolume;
    private Long micVolume;
    private Long tesDirectoryId;
    private Timestamp created;
    private Timestamp updated;
    //    private Device deviceByGate1DeviceId;
//    private Device deviceByGate2DeviceId;
    private TesDirectory tesDirectoryByTesDirectoryId;

    @Id
    @Column(name = "device_id", nullable = false)
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "sip_domain", nullable = false, length = 50)
    public String getSipDomain() {
        return sipDomain;
    }

    public void setSipDomain(String sipDomain) {
        this.sipDomain = sipDomain;
    }

    @Column(name = "sip_username", nullable = false, length = 50)
    public String getSipUsername() {
        return sipUsername;
    }

    public void setSipUsername(String sipUsername) {
        this.sipUsername = sipUsername;
    }

    @Column(name = "sip_password", nullable = false, length = 50)
    public String getSipPassword() {
        return sipPassword;
    }

    public void setSipPassword(String sipPassword) {
        this.sipPassword = sipPassword;
    }

    @Column(name = "sip_auth_id", nullable = true, length = 50)
    public String getSipAuthId() {
        return sipAuthId;
    }

    public void setSipAuthId(String sipAuthId) {
        this.sipAuthId = sipAuthId;
    }

    @Column(name = "sip_port", nullable = false)
    public long getSipPort() {
        return sipPort;
    }

    public void setSipPort(long sipPort) {
        this.sipPort = sipPort;
    }

    @Column(name = "max_wait_for_call_establish", nullable = false)
    public long getMaxWaitForCallEstablish() {
        return maxWaitForCallEstablish;
    }

    public void setMaxWaitForCallEstablish(long maxWaitForCallEstablish) {
        this.maxWaitForCallEstablish = maxWaitForCallEstablish;
    }

    @Column(name = "max_call_time", nullable = false)
    public long getMaxCallTime() {
        return maxCallTime;
    }

    public void setMaxCallTime(long maxCallTime) {
        this.maxCallTime = maxCallTime;
    }

    @Column(name = "ipac_greeting_msg", nullable = true, length = 255)
    public String getIpacGreetingMsg() {
        return ipacGreetingMsg;
    }

    public void setIpacGreetingMsg(String ipacGreetingMsg) {
        this.ipacGreetingMsg = ipacGreetingMsg;
    }

    @Column(name = "gate1_dtmf_code", nullable = true, length = -1)
    public String getGate1DtmfCode() {
        return gate1DtmfCode;
    }

    public void setGate1DtmfCode(String gate1DtmfCode) {
        this.gate1DtmfCode = gate1DtmfCode;
    }

    @Column(name = "gate2_dtmf_code", nullable = true, length = -1)
    public String getGate2DtmfCode() {
        return gate2DtmfCode;
    }

    public void setGate2DtmfCode(String gate2DtmfCode) {
        this.gate2DtmfCode = gate2DtmfCode;
    }

    @Basic
    @Column(name = "enable_sip_diagnostic", nullable = false)
    public short getEnableSipDiagnostic() {
        return enableSipDiagnostic;
    }

    public void setEnableSipDiagnostic(short enableSipDiagnostic) {
        this.enableSipDiagnostic = enableSipDiagnostic;
    }

    @Column(name = "gate1_device_id", nullable = true)
    public Long getGate1DeviceId() {
        return gate1DeviceId;
    }

    public void setGate1DeviceId(Long gate1DeviceId) {
        this.gate1DeviceId = gate1DeviceId;
    }

    @Basic
    @Column(name = "gate2_device_id", nullable = true)
    public Long getGate2DeviceId() {
        return gate2DeviceId;
    }

    public void setGate2DeviceId(Long gate2DeviceId) {
        this.gate2DeviceId = gate2DeviceId;
    }

    @Column(name = "gate1_accept_pin", nullable = false)
    public short getGate1AcceptPin() {
        return gate1AcceptPin;
    }

    public void setGate1AcceptPin(short gate1AcceptPin) {
        this.gate1AcceptPin = gate1AcceptPin;
    }

    @Column(name = "gate2_accept_pin", nullable = false)
    public short getGate2AcceptPin() {
        return gate2AcceptPin;
    }

    public void setGate2AcceptPin(short gate2AcceptPin) {
        this.gate2AcceptPin = gate2AcceptPin;
    }

    @Column(name = "outbound_proxy", nullable = true, length = 255)
    public String getOutboundProxy() {
        return outboundProxy;
    }

    public void setOutboundProxy(String outboundProxy) {
        this.outboundProxy = outboundProxy;
    }

    @Column(name = "stun_server", nullable = true, length = 255)
    public String getStunServer() {
        return stunServer;
    }

    public void setStunServer(String stunServer) {
        this.stunServer = stunServer;
    }

    @Column(name = "speaker_volume", nullable = true)
    public Long getSpeakerVolume() {
        return speakerVolume;
    }

    public void setSpeakerVolume(Long speakerVolume) {
        this.speakerVolume = speakerVolume;
    }

    @Column(name = "mic_volume", nullable = true)
    public Long getMicVolume() {
        return micVolume;
    }

    public void setMicVolume(Long micVolume) {
        this.micVolume = micVolume;
    }

    @Column(name = "tes_directory_id", nullable = true)
    public Long getTesDirectoryId() {
        return tesDirectoryId;
    }

    public void setTesDirectoryId(Long tesDirectoryId) {
        this.tesDirectoryId = tesDirectoryId;
    }

    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        IpacData ipacData = (IpacData) o;

        if (deviceId != ipacData.deviceId) {
            return false;
        }
        if (sipPort != ipacData.sipPort) {
            return false;
        }
        if (maxWaitForCallEstablish != ipacData.maxWaitForCallEstablish) {
            return false;
        }
        if (maxCallTime != ipacData.maxCallTime) {
            return false;
        }
        if (enableSipDiagnostic != ipacData.enableSipDiagnostic) {
            return false;
        }
        if (gate1AcceptPin != ipacData.gate1AcceptPin) {
            return false;
        }
        if (gate2AcceptPin != ipacData.gate2AcceptPin) {
            return false;
        }
        if (sipDomain != null ? !sipDomain.equals(ipacData.sipDomain) : ipacData.sipDomain != null) {
            return false;
        }
        if (sipUsername != null ? !sipUsername.equals(ipacData.sipUsername) : ipacData.sipUsername != null) {
            return false;
        }
        if (sipPassword != null ? !sipPassword.equals(ipacData.sipPassword) : ipacData.sipPassword != null) {
            return false;
        }
        if (sipAuthId != null ? !sipAuthId.equals(ipacData.sipAuthId) : ipacData.sipAuthId != null) {
            return false;
        }
        if (ipacGreetingMsg != null ? !ipacGreetingMsg.equals(ipacData.ipacGreetingMsg) : ipacData.ipacGreetingMsg != null) {
            return false;
        }
        if (gate1DtmfCode != null ? !gate1DtmfCode.equals(ipacData.gate1DtmfCode) : ipacData.gate1DtmfCode != null) {
            return false;
        }
        if (gate2DtmfCode != null ? !gate2DtmfCode.equals(ipacData.gate2DtmfCode) : ipacData.gate2DtmfCode != null) {
            return false;
        }
        if (gate1DeviceId != null ? !gate1DeviceId.equals(ipacData.gate1DeviceId) : ipacData.gate1DeviceId != null) {
            return false;
        }
        if (gate2DeviceId != null ? !gate2DeviceId.equals(ipacData.gate2DeviceId) : ipacData.gate2DeviceId != null) {
            return false;
        }
        if (outboundProxy != null ? !outboundProxy.equals(ipacData.outboundProxy) : ipacData.outboundProxy != null) {
            return false;
        }
        if (stunServer != null ? !stunServer.equals(ipacData.stunServer) : ipacData.stunServer != null) {
            return false;
        }
        if (speakerVolume != null ? !speakerVolume.equals(ipacData.speakerVolume) : ipacData.speakerVolume != null) {
            return false;
        }
        if (micVolume != null ? !micVolume.equals(ipacData.micVolume) : ipacData.micVolume != null) {
            return false;
        }
        if (tesDirectoryId != null ? !tesDirectoryId.equals(ipacData.tesDirectoryId) : ipacData.tesDirectoryId != null) {
            return false;
        }
        if (created != null ? !created.equals(ipacData.created) : ipacData.created != null) {
            return false;
        }
        return updated != null ? updated.equals(ipacData.updated) : ipacData.updated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceId ^ (deviceId >>> 32));
        result = 31 * result + (sipDomain != null ? sipDomain.hashCode() : 0);
        result = 31 * result + (sipUsername != null ? sipUsername.hashCode() : 0);
        result = 31 * result + (sipPassword != null ? sipPassword.hashCode() : 0);
        result = 31 * result + (sipAuthId != null ? sipAuthId.hashCode() : 0);
        result = 31 * result + (int) (sipPort ^ (sipPort >>> 32));
        result = 31 * result + (int) (maxWaitForCallEstablish ^ (maxWaitForCallEstablish >>> 32));
        result = 31 * result + (int) (maxCallTime ^ (maxCallTime >>> 32));
        result = 31 * result + (ipacGreetingMsg != null ? ipacGreetingMsg.hashCode() : 0);
        result = 31 * result + (gate1DtmfCode != null ? gate1DtmfCode.hashCode() : 0);
        result = 31 * result + (gate2DtmfCode != null ? gate2DtmfCode.hashCode() : 0);
        result = 31 * result + (int) enableSipDiagnostic;
        result = 31 * result + (gate1DeviceId != null ? gate1DeviceId.hashCode() : 0);
        result = 31 * result + (gate2DeviceId != null ? gate2DeviceId.hashCode() : 0);
        result = 31 * result + (int) gate1AcceptPin;
        result = 31 * result + (int) gate2AcceptPin;
        result = 31 * result + (outboundProxy != null ? outboundProxy.hashCode() : 0);
        result = 31 * result + (stunServer != null ? stunServer.hashCode() : 0);
        result = 31 * result + (speakerVolume != null ? speakerVolume.hashCode() : 0);
        result = 31 * result + (micVolume != null ? micVolume.hashCode() : 0);
        result = 31 * result + (tesDirectoryId != null ? tesDirectoryId.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        return result;
    }

    /*
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gate1_device_id", referencedColumnName = "device_id", insertable = false, updatable = false)
    public Device getDeviceByGate1DeviceId() {
        return deviceByGate1DeviceId;
    }

    public void setDeviceByGate1DeviceId(Device deviceByGate1DeviceId) {
        this.deviceByGate1DeviceId = deviceByGate1DeviceId;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "gate2_device_id", referencedColumnName = "device_id", insertable = false, updatable = false)
    public Device getDeviceByGate2DeviceId() {
        return deviceByGate2DeviceId;
    }

    public void setDeviceByGate2DeviceId(Device deviceByGate2DeviceId) {
        this.deviceByGate2DeviceId = deviceByGate2DeviceId;
    }
    */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tes_directory_id", referencedColumnName = "directory_id", insertable = false, updatable = false)
    public TesDirectory getTesDirectoryByTesDirectoryId() {
        return tesDirectoryByTesDirectoryId;
    }

    public void setTesDirectoryByTesDirectoryId(TesDirectory tesDirectoryByTesDirectoryId) {
        this.tesDirectoryByTesDirectoryId = tesDirectoryByTesDirectoryId;
    }
}
