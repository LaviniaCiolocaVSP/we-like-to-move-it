package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.Board;
import com.brivo.panel.server.reference.model.hardware.IOPoint;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Board Extractor
 *
 * @author brandon
 */
@Component
public class BoardExtractor implements ResultSetExtractor<List<Board>> {
    @Override
    public List<Board> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<Board> list = new ArrayList<>();

        Map<Integer, BoardAccumulator> boardMap = new HashMap<>();

        while (rs.next()) {
            Integer boardNumber = rs.getInt("boardNumber");
            Integer boardId = rs.getInt("boardId");
            Integer boardType = rs.getInt("boardType");
            Integer pointAddress = rs.getInt("pointAddress");

            Integer eolInt = rs.getInt("eol");
            Boolean eol = false;
            if (eolInt != null && eolInt == 1) {
                eol = true;
            }

            Integer state = rs.getInt("state");

            /**
             * Panel thinks acs300 is board type id 8
             * onair database has it as 12
             */
            if (boardType == 12) {
                boardType = 8;
            }
            /**
             * Panel thinks acs6000 is board type id 7
             * onair database has it as 13
             */
            if (boardType == 13) {
                boardType = 7;
            }


            if (boardMap.containsKey(boardNumber)) {
                boardMap.get(boardNumber).addPoint(pointAddress, eol, state);
                boardMap.get(boardNumber).setBoardId(boardId);
            } else {
                boardMap.put(boardNumber, new BoardAccumulator(boardNumber, boardType,
                        pointAddress, eol, state));
            }
        }

        for (BoardAccumulator boardAcc : boardMap.values()) {
            list.add(boardAcc.getBoard());
        }

        return list;
    }

    private class BoardAccumulator {
        public Integer boardAddress;
        public Integer boardType;
        public List<IOPoint> points;
        private long boardId;

        public BoardAccumulator(Integer boardNumber, Integer boardType,
                                Integer pointAddress, Boolean eol, Integer state) {
            this.boardAddress = boardNumber;
            this.boardType = boardType;
            this.points = new ArrayList<>();

            addPoint(pointAddress, eol, state);
        }

        public void addPoint(Integer pointAddress, Boolean eol, Integer state) {
            IOPoint point = new IOPoint();
            point.setAddress(pointAddress);
            point.setEolWiring(eol);
            point.setDefaultState(state);
            points.add(point);
        }

        public Board getBoard() {
            Board board = new Board();
            board.setBoardNumber(boardAddress);
            board.setBoardType(boardType);
            board.setBoardId(boardId);
            List<IOPoint> ioPoints = points;
            board.setIoPoints(ioPoints);

            return board;
        }

        public void setBoardId(long boardId) {
            this.boardId = boardId;
        }
    }
}

