package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.sql.Timestamp;

@Entity
@Table(name = "application_key", schema = "brivo20", catalog = "onair")
public class ApplicationKey {
    private long objectId;
    private long applicationKeyId;
    private long applicationId;
    private String appKey;
    private String clientPrivateSecret;
    private String serverPublicSecret;
    private Timestamp created;
    private Timestamp updated;
    private Timestamp enabled;
    private Timestamp expires;
    private Short deleted;
    private String name;
    private BrivoObject objectByBrivoObjectId;
    private Application applicationByApplicationId;
    private OauthClientDetails oauthClientDetailsByAppKey;

    @Basic
    @Column(name = "object_id", nullable = false)
    public long getObjectId() {
        return objectId;
    }

    public void setObjectId(long objectId) {
        this.objectId = objectId;
    }

    @Id
    @Column(name = "application_key_id", nullable = false)
    public long getApplicationKeyId() {
        return applicationKeyId;
    }

    public void setApplicationKeyId(long applicationKeyId) {
        this.applicationKeyId = applicationKeyId;
    }

    @Basic
    @Column(name = "application_id", nullable = false)
    public long getApplicationId() {
        return applicationId;
    }

    public void setApplicationId(long applicationId) {
        this.applicationId = applicationId;
    }

    @Basic
    @Column(name = "app_key", nullable = false, length = 40)
    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Basic
    @Column(name = "client_private_secret", nullable = false, length = 4000)
    public String getClientPrivateSecret() {
        return clientPrivateSecret;
    }

    public void setClientPrivateSecret(String clientPrivateSecret) {
        this.clientPrivateSecret = clientPrivateSecret;
    }

    @Basic
    @Column(name = "server_public_secret", nullable = false, length = 4000)
    public String getServerPublicSecret() {
        return serverPublicSecret;
    }

    public void setServerPublicSecret(String serverPublicSecret) {
        this.serverPublicSecret = serverPublicSecret;
    }

    @Basic
    @Column(name = "created", nullable = false)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "enabled", nullable = true)
    public Timestamp getEnabled() {
        return enabled;
    }

    public void setEnabled(Timestamp enabled) {
        this.enabled = enabled;
    }

    @Basic
    @Column(name = "expires", nullable = true)
    public Timestamp getExpires() {
        return expires;
    }

    public void setExpires(Timestamp expires) {
        this.expires = expires;
    }

    @Basic
    @Column(name = "deleted", nullable = true)
    public Short getDeleted() {
        return deleted;
    }

    public void setDeleted(Short deleted) {
        this.deleted = deleted;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 32)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ApplicationKey that = (ApplicationKey) o;

        if (objectId != that.objectId) {
            return false;
        }
        if (applicationKeyId != that.applicationKeyId) {
            return false;
        }
        if (applicationId != that.applicationId) {
            return false;
        }
        if (appKey != null ? !appKey.equals(that.appKey) : that.appKey != null) {
            return false;
        }
        if (clientPrivateSecret != null ? !clientPrivateSecret.equals(that.clientPrivateSecret) : that.clientPrivateSecret != null) {
            return false;
        }
        if (serverPublicSecret != null ? !serverPublicSecret.equals(that.serverPublicSecret) : that.serverPublicSecret != null) {
            return false;
        }
        if (created != null ? !created.equals(that.created) : that.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(that.updated) : that.updated != null) {
            return false;
        }
        if (enabled != null ? !enabled.equals(that.enabled) : that.enabled != null) {
            return false;
        }
        if (expires != null ? !expires.equals(that.expires) : that.expires != null) {
            return false;
        }
        if (deleted != null ? !deleted.equals(that.deleted) : that.deleted != null) {
            return false;
        }
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (objectId ^ (objectId >>> 32));
        result = 31 * result + (int) (applicationKeyId ^ (applicationKeyId >>> 32));
        result = 31 * result + (int) (applicationId ^ (applicationId >>> 32));
        result = 31 * result + (appKey != null ? appKey.hashCode() : 0);
        result = 31 * result + (clientPrivateSecret != null ? clientPrivateSecret.hashCode() : 0);
        result = 31 * result + (serverPublicSecret != null ? serverPublicSecret.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (enabled != null ? enabled.hashCode() : 0);
        result = 31 * result + (expires != null ? expires.hashCode() : 0);
        result = 31 * result + (deleted != null ? deleted.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "object_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public BrivoObject getObjectByBrivoObjectId() {
        return objectByBrivoObjectId;
    }

    public void setObjectByBrivoObjectId(BrivoObject objectByBrivoObjectId) {
        this.objectByBrivoObjectId = objectByBrivoObjectId;
    }

    @ManyToOne
    @JoinColumn(name = "application_id", referencedColumnName = "application_id", nullable = false, insertable = false, updatable = false)
    public Application getApplicationByApplicationId() {
        return applicationByApplicationId;
    }

    public void setApplicationByApplicationId(Application applicationByApplicationId) {
        this.applicationByApplicationId = applicationByApplicationId;
    }

    @ManyToOne
    @JoinColumn(name = "app_key", referencedColumnName = "client_id", nullable = false, insertable = false, updatable = false)
    public OauthClientDetails getOauthClientDetailsByAppKey() {
        return oauthClientDetailsByAppKey;
    }

    public void setOauthClientDetailsByAppKey(OauthClientDetails oauthClientDetailsByAppKey) {
        this.oauthClientDetailsByAppKey = oauthClientDetailsByAppKey;
    }
}
