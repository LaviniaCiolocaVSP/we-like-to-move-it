package com.brivo.panel.server.reference.controller;

import com.brivo.panel.server.reference.dto.MessageDTO;
import com.brivo.panel.server.reference.dto.ui.UiHolidayDTO;
import com.brivo.panel.server.reference.service.HolidayService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/holiday")
public class HolidayController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HolidayController.class);
    private final HolidayService holidayService;

    @Autowired
    public HolidayController(final HolidayService holidayService) {
        this.holidayService = holidayService;
    }

    @GetMapping("/{accountId}")
    public Set<UiHolidayDTO> get(@PathVariable final long accountId) {
        LOGGER.debug("getHolidays by accountId...");
        return holidayService.getUiHolidaysForAccountId(accountId);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO create(@RequestBody final UiHolidayDTO uiHolidayDTO) {
        return holidayService.create(uiHolidayDTO);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO delete(@RequestBody List<Long> holidays_ids) {
        return holidayService.delete(holidays_ids);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public MessageDTO update(@RequestBody UiHolidayDTO holiday) {
        return holidayService.update(holiday);
    }
}
