package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class KeypadCommand {
    private Integer optionalInterval;
    private Long[] keypadCommandDeviceIds;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Integer getOptionalInterval() {
        return optionalInterval;
    }

    public void setOptionalInterval(Integer optionalInterval) {
        this.optionalInterval = optionalInterval;
    }

    public Long[] getKeypadCommandDeviceIds() {
        return keypadCommandDeviceIds;
    }

    public void setKeypadCommandDeviceIds(Long[] keypadCommandDeviceIds) {
        this.keypadCommandDeviceIds = keypadCommandDeviceIds;
    }
}
