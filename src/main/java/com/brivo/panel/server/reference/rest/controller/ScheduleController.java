package com.brivo.panel.server.reference.rest.controller;

import com.brivo.panel.server.reference.auth.PanelPrincipal;
import com.brivo.panel.server.reference.model.hardware.Panel;
import com.brivo.panel.server.reference.model.schedule.PanelScheduleData;
import com.brivo.panel.server.reference.service.ScheduleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScheduleController {
    private static final Logger LOGGER = LoggerFactory.getLogger(ScheduleController.class);

    @Autowired
    private ScheduleService scheduleService;

    /**
     * Get Schedules.
     */
    @RequestMapping(
            path = "/paneldata/schedules",
            method = RequestMethod.GET,
            produces = "application/json"
    )
    public PanelScheduleData getSchedules(@AuthenticationPrincipal PanelPrincipal panelPrincipal) {
        Panel panel = panelPrincipal.getPanel();
        // Panel panel = PanelBuilder.getMockPanel();

        LOGGER.info("Panel {} requesting updated schedule data", panel.getElectronicSerialNumber());

        // PanelScheduleData data = ScheduleBuilder.getMockSchedule();
        PanelScheduleData data;
        if (panel.getIsRegistered()) {
            data = scheduleService.getSchedules(panel);
        } else {
            data = new PanelScheduleData();
        }
        LOGGER.debug("Sending panel {} updated schedule data", panel.getElectronicSerialNumber());
        //LOGGER.debug("Sending panel {} updated schedule data [{}]", panel.getElectronicSerialNumber(), data);

        return data;
    }
}
