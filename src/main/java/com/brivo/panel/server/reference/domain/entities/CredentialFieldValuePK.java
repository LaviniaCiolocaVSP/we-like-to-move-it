package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class CredentialFieldValuePK implements Serializable {
    private long accessCredentialId;
    private long credentialFieldId;

    @Column(name = "access_credential_id", nullable = false)
    @Id
    public long getAccessCredentialId() {
        return accessCredentialId;
    }

    public void setAccessCredentialId(long accessCredentialId) {
        this.accessCredentialId = accessCredentialId;
    }

    @Column(name = "credential_field_id", nullable = false)
    @Id
    public long getCredentialFieldId() {
        return credentialFieldId;
    }

    public void setCredentialFieldId(long credentialFieldId) {
        this.credentialFieldId = credentialFieldId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CredentialFieldValuePK that = (CredentialFieldValuePK) o;

        if (accessCredentialId != that.accessCredentialId) {
            return false;
        }
        return credentialFieldId == that.credentialFieldId;
    }

    @Override
    public int hashCode() {
        int result = (int) (accessCredentialId ^ (accessCredentialId >>> 32));
        result = 31 * result + (int) (credentialFieldId ^ (credentialFieldId >>> 32));
        return result;
    }
}
