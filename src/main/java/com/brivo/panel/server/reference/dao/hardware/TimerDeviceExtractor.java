package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.TimerDevice;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Timer Device Extractor
 */
@Component
public class TimerDeviceExtractor
        implements ResultSetExtractor<List<TimerDevice>> {
    @Override
    public List<TimerDevice> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<TimerDevice> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Long scheduleId = rs.getLong("scheduleID");

            Integer notifyFlagInt = rs.getInt("notifyFlag");
            Boolean notify = false;
            if (notifyFlagInt != null && notifyFlagInt == 1) {
                notify = true;
            }

            Integer notifyDisengageInt = rs.getInt("notifyDisengage");
            Boolean notifyDisengage = false;
            if (notifyDisengageInt != null && notifyDisengageInt == 1) {
                notifyDisengage = true;
            }

            TimerDevice timer = new TimerDevice();

            timer.setDeviceId(deviceId);
            timer.setInputScheduleId(scheduleId);
            timer.setReportEngage(notify);
            timer.setReportDisengage(notifyDisengage);

            list.add(timer);
        }

        return list;
    }
}
