package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "camera_type", schema = "brivo20", catalog = "onair")
public class CameraType {
    private long cameraTypeId;
    private String description;
    private Collection<Camera> camerasByCameraTypeId;

    @Id
    @Column(name = "camera_type_id", nullable = false)
    public long getCameraTypeId() {
        return cameraTypeId;
    }

    public void setCameraTypeId(long cameraTypeId) {
        this.cameraTypeId = cameraTypeId;
    }

    @Basic
    @Column(name = "description", nullable = false, length = 32)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraType that = (CameraType) o;

        if (cameraTypeId != that.cameraTypeId) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (cameraTypeId ^ (cameraTypeId >>> 32));
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "cameraTypeByCameraTypeId")
    public Collection<Camera> getCamerasByCameraTypeId() {
        return camerasByCameraTypeId;
    }

    public void setCamerasByCameraTypeId(Collection<Camera> camerasByCameraTypeId) {
        this.camerasByCameraTypeId = camerasByCameraTypeId;
    }
}
