package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "validation_type", schema = "brivo20", catalog = "onair")
public class ValidationType {
    private long validationTypeId;
    private String name;
    private String description;
    private Collection<VideoCameraModelParameter> videoCameraModelParametersByValidationTypeId;
    private Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByValidationTypeId;

    @Id
    @Column(name = "validation_type_id", nullable = false)
    public long getValidationTypeId() {
        return validationTypeId;
    }

    public void setValidationTypeId(long validationTypeId) {
        this.validationTypeId = validationTypeId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 256)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 512)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ValidationType that = (ValidationType) o;

        if (validationTypeId != that.validationTypeId) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (validationTypeId ^ (validationTypeId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "validationTypeByValidationTypeId")
    public Collection<VideoCameraModelParameter> getVideoCameraModelParametersByValidationTypeId() {
        return videoCameraModelParametersByValidationTypeId;
    }

    public void setVideoCameraModelParametersByValidationTypeId(Collection<VideoCameraModelParameter> videoCameraModelParametersByValidationTypeId) {
        this.videoCameraModelParametersByValidationTypeId = videoCameraModelParametersByValidationTypeId;
    }

    @OneToMany(mappedBy = "validationTypeByValidationTypeId")
    public Collection<VideoProviderTypeProperty> getVideoProviderTypePropertiesByValidationTypeId() {
        return videoProviderTypePropertiesByValidationTypeId;
    }

    public void setVideoProviderTypePropertiesByValidationTypeId(Collection<VideoProviderTypeProperty> videoProviderTypePropertiesByValidationTypeId) {
        this.videoProviderTypePropertiesByValidationTypeId = videoProviderTypePropertiesByValidationTypeId;
    }
}
