package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "camera_group_ovr_camera", schema = "brivo20", catalog = "onair")
public class CameraGroupOvrCamera {
    private long id;
    private Long ovrCameraId;
    private long cameraGroupId;
    private long ordering;
    private Long videoCameraId;
    private Boolean isHd;
    private OvrCamera ovrCameraByOvrCameraId;
    private CameraGroup cameraGroupByCameraGroupId;
    private VideoCamera videoCameraByVideoCameraId;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ovr_camera_id", nullable = true)
    public Long getOvrCameraId() {
        return ovrCameraId;
    }

    public void setOvrCameraId(Long ovrCameraId) {
        this.ovrCameraId = ovrCameraId;
    }

    @Basic
    @Column(name = "camera_group_id", nullable = false)
    public long getCameraGroupId() {
        return cameraGroupId;
    }

    public void setCameraGroupId(long cameraGroupId) {
        this.cameraGroupId = cameraGroupId;
    }

    @Basic
    @Column(name = "ordering", nullable = false)
    public long getOrdering() {
        return ordering;
    }

    public void setOrdering(long ordering) {
        this.ordering = ordering;
    }

    @Basic
    @Column(name = "video_camera_id", nullable = true)
    public Long getVideoCameraId() {
        return videoCameraId;
    }

    public void setVideoCameraId(Long videoCameraId) {
        this.videoCameraId = videoCameraId;
    }

    @Basic
    @Column(name = "is_hd", nullable = true)
    public Boolean getHd() {
        return isHd;
    }

    public void setHd(Boolean hd) {
        isHd = hd;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CameraGroupOvrCamera that = (CameraGroupOvrCamera) o;

        if (id != that.id) {
            return false;
        }
        if (cameraGroupId != that.cameraGroupId) {
            return false;
        }
        if (ordering != that.ordering) {
            return false;
        }
        if (ovrCameraId != null ? !ovrCameraId.equals(that.ovrCameraId) : that.ovrCameraId != null) {
            return false;
        }
        if (videoCameraId != null ? !videoCameraId.equals(that.videoCameraId) : that.videoCameraId != null) {
            return false;
        }
        return isHd != null ? isHd.equals(that.isHd) : that.isHd == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (ovrCameraId != null ? ovrCameraId.hashCode() : 0);
        result = 31 * result + (int) (cameraGroupId ^ (cameraGroupId >>> 32));
        result = 31 * result + (int) (ordering ^ (ordering >>> 32));
        result = 31 * result + (videoCameraId != null ? videoCameraId.hashCode() : 0);
        result = 31 * result + (isHd != null ? isHd.hashCode() : 0);
        return result;
    }

    @ManyToOne
    @JoinColumn(name = "ovr_camera_id", referencedColumnName = "camera_id", insertable = false, updatable = false)
    public OvrCamera getOvrCameraByOvrCameraId() {
        return ovrCameraByOvrCameraId;
    }

    public void setOvrCameraByOvrCameraId(OvrCamera ovrCameraByOvrCameraId) {
        this.ovrCameraByOvrCameraId = ovrCameraByOvrCameraId;
    }

    @ManyToOne
    @JoinColumn(name = "camera_group_id", referencedColumnName = "object_id", nullable = false, insertable = false, updatable = false)
    public CameraGroup getCameraGroupByCameraGroupId() {
        return cameraGroupByCameraGroupId;
    }

    public void setCameraGroupByCameraGroupId(CameraGroup cameraGroupByCameraGroupId) {
        this.cameraGroupByCameraGroupId = cameraGroupByCameraGroupId;
    }

    @ManyToOne
    @JoinColumn(name = "video_camera_id", referencedColumnName = "video_camera_id", insertable = false, updatable = false)
    public VideoCamera getVideoCameraByVideoCameraId() {
        return videoCameraByVideoCameraId;
    }

    public void setVideoCameraByVideoCameraId(VideoCamera videoCameraByVideoCameraId) {
        this.videoCameraByVideoCameraId = videoCameraByVideoCameraId;
    }
}
