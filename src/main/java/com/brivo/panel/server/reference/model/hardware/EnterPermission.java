package com.brivo.panel.server.reference.model.hardware;

public class EnterPermission extends Permission {
    private Boolean allowedPrivacyModeOverride;

    public Boolean getAllowedPrivacyModeOverride() {
        return allowedPrivacyModeOverride;
    }

    public void setAllowedPrivacyModeOverride(Boolean allowedPrivacyModeOverride) {
        this.allowedPrivacyModeOverride = allowedPrivacyModeOverride;
    }
}
