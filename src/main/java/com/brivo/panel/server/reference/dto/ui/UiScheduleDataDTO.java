package com.brivo.panel.server.reference.dto.ui;

public class UiScheduleDataDTO {
    private long scheduleId;
    private long startTime;
    private long stopTime;

    private String start;
    private String stop;

    private int dayId;
    private String dayName;

    public UiScheduleDataDTO() {
    }

    public UiScheduleDataDTO(long scheduleId, long startTime, long stopTime, String start, String stop) {
        this.scheduleId = scheduleId;
        this.startTime = startTime;
        this.stopTime = stopTime;
        this.start = start;
        this.stop = stop;
    }

    public UiScheduleDataDTO(long startTime, long stopTime) {
        this.startTime = startTime;
        this.stopTime = stopTime;
    }

    public UiScheduleDataDTO(int dayId, String dayName, String start, String stop, long startTime, long stopTime) {
        this.dayId = dayId;
        this.dayName = dayName;
        this.start = start;
        this.stop = stop;
        this.startTime = startTime;
        this.stopTime = stopTime;
    }

    public long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public long getStopTime() {
        return stopTime;
    }

    public void setStopTime(long stopTime) {
        this.stopTime = stopTime;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public int getDayId() {
        return dayId;
    }

    public void setDayId(int dayId) {
        this.dayId = dayId;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    @Override
    public String toString() {
        return "UiScheduleDataDTO{" +
                "scheduleId=" + scheduleId +
                ", startTime=" + startTime +
                ", stopTime=" + stopTime +
                ", start='" + start + '\'' +
                ", stop='" + stop + '\'' +
                ", dayId=" + dayId +
                ", dayName='" + dayName + '\'' +
                '}';
    }
}
