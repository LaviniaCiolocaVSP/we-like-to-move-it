package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class TwoFactorCredentialSettings {
    private Long scheduleId;
    private Integer interval;
    private TwoFactorCredentialThreatSettings threatLevel;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    public Integer getInterval() {
        return interval;
    }

    public void setInterval(Integer interval) {
        this.interval = interval;
    }

    public TwoFactorCredentialThreatSettings getThreatLevel() {
        return threatLevel;
    }

    public void setThreatLevel(TwoFactorCredentialThreatSettings threatLevel) {
        this.threatLevel = threatLevel;
    }
}
