package com.brivo.panel.server.reference.domain.repository;

import com.brivo.panel.server.reference.domain.entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<Users, Long> {
    
    List<Users> findByAccountId(@Param("accountId") long accountId);
    
    @Query(
            value = "SELECT coalesce(max(u.users_id),0) FROM brivo20.users u",
            nativeQuery = true
    )
    Optional<Long> getMaxUserId();
    
    @Query(
            value = "SELECT u.* FROM brivo20.users u WHERE u.object_id = :objectId",
            nativeQuery = true
    )
    Optional<Users> findByObjectId(@Param("objectId") long objectId);
    
    @Query(
            value = "select count(*) from users where account_id = :account_id",
            nativeQuery = true
    )
    long getUserCountForAccount(@Param("account_id") long accountId);
    
    
    @Query(
            value = "SELECT u.* FROM brivo20.users u WHERE u.object_id in (select object_id from brivo20.security_group_member where security_group_id = :groupId)",
            nativeQuery = true
    )
    List<Users> findByGroupId(@Param("groupId") long groupId);
}
