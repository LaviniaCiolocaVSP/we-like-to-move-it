package com.brivo.panel.server.reference.model.hardware;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class LockOnOpenSettings {
    private Boolean useLockOnOpen;
    private Integer delay;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public Boolean getUseLockOnOpen() {
        return useLockOnOpen;
    }

    public void setUseLockOnOpen(Boolean useLockOnOpen) {
        this.useLockOnOpen = useLockOnOpen;
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }
}
