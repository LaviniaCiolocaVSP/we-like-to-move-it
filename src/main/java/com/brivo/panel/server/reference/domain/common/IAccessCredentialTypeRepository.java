package com.brivo.panel.server.reference.domain.common;

import com.brivo.panel.server.reference.domain.entities.AccessCredentialType;
import org.springframework.data.repository.CrudRepository;

public interface IAccessCredentialTypeRepository extends CrudRepository<AccessCredentialType, Long> {
}
