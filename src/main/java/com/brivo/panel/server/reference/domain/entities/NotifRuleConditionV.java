package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "notif_rule_condition_v", schema = "brivo20", catalog = "onair")
public class NotifRuleConditionV {
    private Long notifRuleObjectId;
    private Long accountId;
    private String name;
    private Long ownerObjectId;
    private Long creatorObjectId;
    private Long creatorUsersId;
    private String creatorUsername;
    private String creatorFirstName;
    private String creatorLastName;
    private Short wantsDailyReport;
    private Short wantsWeeklyReport;
    private Short wantsPanelCommReport;
    private Long scheduleId;
    private Timestamp notifRuleCreated;
    private Timestamp notifRuleUpdated;
    private Long securityActionId;
    private Long actorObjectId;
    private Long acteeObjectId;
    private Long languageId;

    @Id
    @Basic
    @Column(name = "notif_rule_object_id", nullable = true)
    public Long getNotifRuleObjectId() {
        return notifRuleObjectId;
    }

    public void setNotifRuleObjectId(Long notifRuleObjectId) {
        this.notifRuleObjectId = notifRuleObjectId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 60)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "owner_object_id", nullable = true)
    public Long getOwnerObjectId() {
        return ownerObjectId;
    }

    public void setOwnerObjectId(Long ownerObjectId) {
        this.ownerObjectId = ownerObjectId;
    }

    @Basic
    @Column(name = "creator_object_id", nullable = true)
    public Long getCreatorObjectId() {
        return creatorObjectId;
    }

    public void setCreatorObjectId(Long creatorObjectId) {
        this.creatorObjectId = creatorObjectId;
    }

    @Basic
    @Column(name = "creator_users_id", nullable = true)
    public Long getCreatorUsersId() {
        return creatorUsersId;
    }

    public void setCreatorUsersId(Long creatorUsersId) {
        this.creatorUsersId = creatorUsersId;
    }

    @Basic
    @Column(name = "creator_username", nullable = true, length = 36)
    public String getCreatorUsername() {
        return creatorUsername;
    }

    public void setCreatorUsername(String creatorUsername) {
        this.creatorUsername = creatorUsername;
    }

    @Basic
    @Column(name = "creator_first_name", nullable = true, length = 35)
    public String getCreatorFirstName() {
        return creatorFirstName;
    }

    public void setCreatorFirstName(String creatorFirstName) {
        this.creatorFirstName = creatorFirstName;
    }

    @Basic
    @Column(name = "creator_last_name", nullable = true, length = 35)
    public String getCreatorLastName() {
        return creatorLastName;
    }

    public void setCreatorLastName(String creatorLastName) {
        this.creatorLastName = creatorLastName;
    }

    @Basic
    @Column(name = "wants_daily_report", nullable = true)
    public Short getWantsDailyReport() {
        return wantsDailyReport;
    }

    public void setWantsDailyReport(Short wantsDailyReport) {
        this.wantsDailyReport = wantsDailyReport;
    }

    @Basic
    @Column(name = "wants_weekly_report", nullable = true)
    public Short getWantsWeeklyReport() {
        return wantsWeeklyReport;
    }

    public void setWantsWeeklyReport(Short wantsWeeklyReport) {
        this.wantsWeeklyReport = wantsWeeklyReport;
    }

    @Basic
    @Column(name = "wants_panel_comm_report", nullable = true)
    public Short getWantsPanelCommReport() {
        return wantsPanelCommReport;
    }

    public void setWantsPanelCommReport(Short wantsPanelCommReport) {
        this.wantsPanelCommReport = wantsPanelCommReport;
    }

    @Basic
    @Column(name = "schedule_id", nullable = true)
    public Long getScheduleId() {
        return scheduleId;
    }

    public void setScheduleId(Long scheduleId) {
        this.scheduleId = scheduleId;
    }

    @Basic
    @Column(name = "notif_rule_created", nullable = true)
    public Timestamp getNotifRuleCreated() {
        return notifRuleCreated;
    }

    public void setNotifRuleCreated(Timestamp notifRuleCreated) {
        this.notifRuleCreated = notifRuleCreated;
    }

    @Basic
    @Column(name = "notif_rule_updated", nullable = true)
    public Timestamp getNotifRuleUpdated() {
        return notifRuleUpdated;
    }

    public void setNotifRuleUpdated(Timestamp notifRuleUpdated) {
        this.notifRuleUpdated = notifRuleUpdated;
    }

    @Basic
    @Column(name = "security_action_id", nullable = true)
    public Long getSecurityActionId() {
        return securityActionId;
    }

    public void setSecurityActionId(Long securityActionId) {
        this.securityActionId = securityActionId;
    }

    @Basic
    @Column(name = "actor_object_id", nullable = true)
    public Long getActorObjectId() {
        return actorObjectId;
    }

    public void setActorObjectId(Long actorObjectId) {
        this.actorObjectId = actorObjectId;
    }

    @Basic
    @Column(name = "actee_object_id", nullable = true)
    public Long getActeeObjectId() {
        return acteeObjectId;
    }

    public void setActeeObjectId(Long acteeObjectId) {
        this.acteeObjectId = acteeObjectId;
    }

    @Basic
    @Column(name = "language_id", nullable = true)
    public Long getLanguageId() {
        return languageId;
    }

    public void setLanguageId(Long languageId) {
        this.languageId = languageId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        NotifRuleConditionV that = (NotifRuleConditionV) o;
        return Objects.equals(notifRuleObjectId, that.notifRuleObjectId) &&
                Objects.equals(accountId, that.accountId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(ownerObjectId, that.ownerObjectId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(notifRuleObjectId, accountId, name, ownerObjectId);
    }
}
