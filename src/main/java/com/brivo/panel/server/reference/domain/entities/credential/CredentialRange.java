package com.brivo.panel.server.reference.domain.entities.credential;

import java.math.BigInteger;
import java.util.List;

public class CredentialRange {
    
    private CredentialFormat format;
    private BigInteger firstExternalNumber;
    private BigInteger lastExternalNumber;
    private BigInteger startValue;
    
    private List<CredentialFieldValue> fieldValues;
    
    public void setFormat(CredentialFormat format) {
        this.format = format;
    }
    
    
    public CredentialFormat getFormat() {
        return format;
    }
    
    public void setFirstExternalNumber(BigInteger firstExternalNumber) {
        this.firstExternalNumber = firstExternalNumber;
    }
    
    
    public BigInteger getFirstExternalNumber() {
        return firstExternalNumber;
    }
    
    public void setLastExternalNumber(BigInteger lastExternalNumber) {
        this.lastExternalNumber = lastExternalNumber;
    }
    
    
    public BigInteger getLastExternalNumber() {
        return lastExternalNumber;
    }
    
    public void setStartValue(BigInteger startValue) {
        this.startValue = startValue;
    }
    
    
    public BigInteger getStartValue() {
        return startValue;
    }
    
    public void setFieldValues(List<CredentialFieldValue> values) {
        this.fieldValues = values;
    }
    
    
    public List<CredentialFieldValue> getFieldValues() {
        return fieldValues;
    }
}
