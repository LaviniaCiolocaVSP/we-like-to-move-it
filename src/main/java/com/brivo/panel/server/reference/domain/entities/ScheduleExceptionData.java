package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "schedule_exception_data", schema = "brivo20", catalog = "onair")
public class ScheduleExceptionData {
    private long exceptionId;
    private long repeatOrdinal;
    private long repeatIndex;
    private long startMin;
    private long endMin;
    private long isEnableBlock;
    private long repeatType;
    private Schedule scheduleByScheduleId;

    @Id
    @Column(name = "exception_id", nullable = false)
    public long getExceptionId() {
        return exceptionId;
    }

    public void setExceptionId(long exceptionId) {
        this.exceptionId = exceptionId;
    }

    @Basic
    @Column(name = "repeat_ordinal", nullable = false)
    public long getRepeatOrdinal() {
        return repeatOrdinal;
    }

    public void setRepeatOrdinal(long repeatOrdinal) {
        this.repeatOrdinal = repeatOrdinal;
    }

    @Basic
    @Column(name = "repeat_index", nullable = false)
    public long getRepeatIndex() {
        return repeatIndex;
    }

    public void setRepeatIndex(long repeatIndex) {
        this.repeatIndex = repeatIndex;
    }

    @Basic
    @Column(name = "start_min", nullable = false)
    public long getStartMin() {
        return startMin;
    }

    public void setStartMin(long startMin) {
        this.startMin = startMin;
    }

    @Basic
    @Column(name = "end_min", nullable = false)
    public long getEndMin() {
        return endMin;
    }

    public void setEndMin(long endMin) {
        this.endMin = endMin;
    }

    @Basic
    @Column(name = "is_enable_block", nullable = false)
    public long getIsEnableBlock() {
        return isEnableBlock;
    }

    public void setIsEnableBlock(long isEnableBlock) {
        this.isEnableBlock = isEnableBlock;
    }

    @Basic
    @Column(name = "repeat_type", nullable = false)
    public long getRepeatType() {
        return repeatType;
    }

    public void setRepeatType(long repeatType) {
        this.repeatType = repeatType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ScheduleExceptionData that = (ScheduleExceptionData) o;

        if (exceptionId != that.exceptionId) {
            return false;
        }
        if (scheduleByScheduleId.getScheduleId() != that.scheduleByScheduleId.getScheduleId()) {
            return false;
        }
        if (repeatOrdinal != that.repeatOrdinal) {
            return false;
        }
        if (repeatIndex != that.repeatIndex) {
            return false;
        }
        if (startMin != that.startMin) {
            return false;
        }
        if (endMin != that.endMin) {
            return false;
        }
        if (isEnableBlock != that.isEnableBlock) {
            return false;
        }
        return repeatType == that.repeatType;
    }

    @Override
    public int hashCode() {
        int result = (int) (exceptionId ^ (exceptionId >>> 32));
        result = 31 * result + (int) (scheduleByScheduleId.getScheduleId() ^ (scheduleByScheduleId.getScheduleId() >>> 32));
        result = 31 * result + (int) (repeatOrdinal ^ (repeatOrdinal >>> 32));
        result = 31 * result + (int) (repeatIndex ^ (repeatIndex >>> 32));
        result = 31 * result + (int) (startMin ^ (startMin >>> 32));
        result = 31 * result + (int) (endMin ^ (endMin >>> 32));
        result = 31 * result + (int) (isEnableBlock ^ (isEnableBlock >>> 32));
        result = 31 * result + (int) (repeatType ^ (repeatType >>> 32));
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "schedule_id", referencedColumnName = "schedule_id", nullable = false)
    public Schedule getScheduleByScheduleId() {
        return scheduleByScheduleId;
    }

    public void setScheduleByScheduleId(Schedule scheduleByScheduleId) {
        this.scheduleByScheduleId = scheduleByScheduleId;
    }
}
