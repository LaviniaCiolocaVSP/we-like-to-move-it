package com.brivo.panel.server.reference.model.usergroup;

import com.brivo.panel.server.reference.model.credential.CardMask;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.time.Instant;
import java.util.List;

public class UserGroupData {
    private List<CardMask> cardmasks;
    private List<UserGroup> userGroups;
    private Instant lastUpdate;

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.DEFAULT_STYLE);
    }

    public List<CardMask> getCardmasks() {
        return cardmasks;
    }

    public void setCardmasks(List<CardMask> cardmasks) {
        this.cardmasks = cardmasks;
    }

    public List<UserGroup> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<UserGroup> userGroups) {
        this.userGroups = userGroups;
    }

    public Instant getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Instant lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}
