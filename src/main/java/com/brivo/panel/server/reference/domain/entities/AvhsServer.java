package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Collection;

@Entity
@Table(name = "avhs_server", schema = "brivo20", catalog = "onair")
public class AvhsServer {
    private long avhsServerId;
    private String name;
    private String rootUrl;
    private String masterUserId;
    private String masterPassword;
    private String watchdogUserId;
    private String watchdogPassword;
    private String mysqlUrl;
    private String mysqlUserId;
    private String mysqlPassword;
    private String cameraservUrl;
    private String avhsNotificationReplyUrl;
    private String version;
    private short acceptingNewCameras;
    private short supportsRtmp;
    private short deleted;
    private Collection<AxisDispatcherCredential> axisDispatcherCredentialsByAvhsServerId;
    private Collection<CameraMigrationHistory> cameraMigrationHistoriesByAvhsServerId;
    private Collection<OvrCamera> ovrCamerasByAvhsServerId;

    @Id
    @Column(name = "avhs_server_id", nullable = false)
    public long getAvhsServerId() {
        return avhsServerId;
    }

    public void setAvhsServerId(long avhsServerId) {
        this.avhsServerId = avhsServerId;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "root_url", nullable = false, length = 200)
    public String getRootUrl() {
        return rootUrl;
    }

    public void setRootUrl(String rootUrl) {
        this.rootUrl = rootUrl;
    }

    @Basic
    @Column(name = "master_user_id", nullable = false, length = 36)
    public String getMasterUserId() {
        return masterUserId;
    }

    public void setMasterUserId(String masterUserId) {
        this.masterUserId = masterUserId;
    }

    @Basic
    @Column(name = "master_password", nullable = false, length = 128)
    public String getMasterPassword() {
        return masterPassword;
    }

    public void setMasterPassword(String masterPassword) {
        this.masterPassword = masterPassword;
    }

    @Basic
    @Column(name = "watchdog_user_id", nullable = false, length = 36)
    public String getWatchdogUserId() {
        return watchdogUserId;
    }

    public void setWatchdogUserId(String watchdogUserId) {
        this.watchdogUserId = watchdogUserId;
    }

    @Basic
    @Column(name = "watchdog_password", nullable = false, length = 128)
    public String getWatchdogPassword() {
        return watchdogPassword;
    }

    public void setWatchdogPassword(String watchdogPassword) {
        this.watchdogPassword = watchdogPassword;
    }

    @Basic
    @Column(name = "mysql_url", nullable = false, length = 200)
    public String getMysqlUrl() {
        return mysqlUrl;
    }

    public void setMysqlUrl(String mysqlUrl) {
        this.mysqlUrl = mysqlUrl;
    }

    @Basic
    @Column(name = "mysql_user_id", nullable = false, length = 36)
    public String getMysqlUserId() {
        return mysqlUserId;
    }

    public void setMysqlUserId(String mysqlUserId) {
        this.mysqlUserId = mysqlUserId;
    }

    @Basic
    @Column(name = "mysql_password", nullable = false, length = 128)
    public String getMysqlPassword() {
        return mysqlPassword;
    }

    public void setMysqlPassword(String mysqlPassword) {
        this.mysqlPassword = mysqlPassword;
    }

    @Basic
    @Column(name = "cameraserv_url", nullable = false, length = 200)
    public String getCameraservUrl() {
        return cameraservUrl;
    }

    public void setCameraservUrl(String cameraservUrl) {
        this.cameraservUrl = cameraservUrl;
    }

    @Basic
    @Column(name = "avhs_notification_reply_url", nullable = false, length = 200)
    public String getAvhsNotificationReplyUrl() {
        return avhsNotificationReplyUrl;
    }

    public void setAvhsNotificationReplyUrl(String avhsNotificationReplyUrl) {
        this.avhsNotificationReplyUrl = avhsNotificationReplyUrl;
    }

    @Basic
    @Column(name = "version", nullable = false, length = 10)
    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    @Basic
    @Column(name = "accepting_new_cameras", nullable = false)
    public short getAcceptingNewCameras() {
        return acceptingNewCameras;
    }

    public void setAcceptingNewCameras(short acceptingNewCameras) {
        this.acceptingNewCameras = acceptingNewCameras;
    }

    @Basic
    @Column(name = "supports_rtmp", nullable = false)
    public short getSupportsRtmp() {
        return supportsRtmp;
    }

    public void setSupportsRtmp(short supportsRtmp) {
        this.supportsRtmp = supportsRtmp;
    }

    @Basic
    @Column(name = "deleted", nullable = false)
    public short getDeleted() {
        return deleted;
    }

    public void setDeleted(short deleted) {
        this.deleted = deleted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        AvhsServer that = (AvhsServer) o;

        if (avhsServerId != that.avhsServerId) {
            return false;
        }
        if (acceptingNewCameras != that.acceptingNewCameras) {
            return false;
        }
        if (supportsRtmp != that.supportsRtmp) {
            return false;
        }
        if (deleted != that.deleted) {
            return false;
        }
        if (name != null ? !name.equals(that.name) : that.name != null) {
            return false;
        }
        if (rootUrl != null ? !rootUrl.equals(that.rootUrl) : that.rootUrl != null) {
            return false;
        }
        if (masterUserId != null ? !masterUserId.equals(that.masterUserId) : that.masterUserId != null) {
            return false;
        }
        if (masterPassword != null ? !masterPassword.equals(that.masterPassword) : that.masterPassword != null) {
            return false;
        }
        if (watchdogUserId != null ? !watchdogUserId.equals(that.watchdogUserId) : that.watchdogUserId != null) {
            return false;
        }
        if (watchdogPassword != null ? !watchdogPassword.equals(that.watchdogPassword) : that.watchdogPassword != null) {
            return false;
        }
        if (mysqlUrl != null ? !mysqlUrl.equals(that.mysqlUrl) : that.mysqlUrl != null) {
            return false;
        }
        if (mysqlUserId != null ? !mysqlUserId.equals(that.mysqlUserId) : that.mysqlUserId != null) {
            return false;
        }
        if (mysqlPassword != null ? !mysqlPassword.equals(that.mysqlPassword) : that.mysqlPassword != null) {
            return false;
        }
        if (cameraservUrl != null ? !cameraservUrl.equals(that.cameraservUrl) : that.cameraservUrl != null) {
            return false;
        }
        if (avhsNotificationReplyUrl != null ? !avhsNotificationReplyUrl.equals(that.avhsNotificationReplyUrl) : that.avhsNotificationReplyUrl != null) {
            return false;
        }
        return version != null ? version.equals(that.version) : that.version == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (avhsServerId ^ (avhsServerId >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (rootUrl != null ? rootUrl.hashCode() : 0);
        result = 31 * result + (masterUserId != null ? masterUserId.hashCode() : 0);
        result = 31 * result + (masterPassword != null ? masterPassword.hashCode() : 0);
        result = 31 * result + (watchdogUserId != null ? watchdogUserId.hashCode() : 0);
        result = 31 * result + (watchdogPassword != null ? watchdogPassword.hashCode() : 0);
        result = 31 * result + (mysqlUrl != null ? mysqlUrl.hashCode() : 0);
        result = 31 * result + (mysqlUserId != null ? mysqlUserId.hashCode() : 0);
        result = 31 * result + (mysqlPassword != null ? mysqlPassword.hashCode() : 0);
        result = 31 * result + (cameraservUrl != null ? cameraservUrl.hashCode() : 0);
        result = 31 * result + (avhsNotificationReplyUrl != null ? avhsNotificationReplyUrl.hashCode() : 0);
        result = 31 * result + (version != null ? version.hashCode() : 0);
        result = 31 * result + (int) acceptingNewCameras;
        result = 31 * result + (int) supportsRtmp;
        result = 31 * result + (int) deleted;
        return result;
    }

    @OneToMany(mappedBy = "avhsServerByAvhsServerId")
    public Collection<AxisDispatcherCredential> getAxisDispatcherCredentialsByAvhsServerId() {
        return axisDispatcherCredentialsByAvhsServerId;
    }

    public void setAxisDispatcherCredentialsByAvhsServerId(Collection<AxisDispatcherCredential> axisDispatcherCredentialsByAvhsServerId) {
        this.axisDispatcherCredentialsByAvhsServerId = axisDispatcherCredentialsByAvhsServerId;
    }

    @OneToMany(mappedBy = "avhsServerByPreviousAvhsServerId")
    public Collection<CameraMigrationHistory> getCameraMigrationHistoriesByAvhsServerId() {
        return cameraMigrationHistoriesByAvhsServerId;
    }

    public void setCameraMigrationHistoriesByAvhsServerId(Collection<CameraMigrationHistory> cameraMigrationHistoriesByAvhsServerId) {
        this.cameraMigrationHistoriesByAvhsServerId = cameraMigrationHistoriesByAvhsServerId;
    }

    @OneToMany(mappedBy = "avhsServerByAvhsServerId")
    public Collection<OvrCamera> getOvrCamerasByAvhsServerId() {
        return ovrCamerasByAvhsServerId;
    }

    public void setOvrCamerasByAvhsServerId(Collection<OvrCamera> ovrCamerasByAvhsServerId) {
        this.ovrCamerasByAvhsServerId = ovrCamerasByAvhsServerId;
    }
}
