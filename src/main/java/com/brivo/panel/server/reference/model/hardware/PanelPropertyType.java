package com.brivo.panel.server.reference.model.hardware;

/**
 * See default_panel_prop table.
 *
 * @author brandon
 */
public enum PanelPropertyType {
    NONE(0),
    CACHE_EVENTS(1),
    FLUSH_INTERVAL(2),
    LISTEN_PORT(2),
    MODEM_POWER_RELAY(1),
    MODEM_RESET_DELAY(2),
    OUTAGE_RETRY_INTERVAL(2),
    POLLING(1),
    REPORT_NETWORK_OUTAGE(1),
    SEND_PIC_RESETS(1),
    STATUS_INTERVAL(2);

    // 1 = Boolean
    // 2 = Integer
    private int typeId;

    PanelPropertyType(int typeId) {
        this.typeId = typeId;
    }

    public static PanelPropertyType getPanelPropertyType(String str) {
        for (PanelPropertyType type : values()) {
            if (type.name().equals(str)) {
                return type;
            }
        }

        return NONE;
    }

    public int getPanelPropertyTypeId() {
        return this.typeId;
    }
}