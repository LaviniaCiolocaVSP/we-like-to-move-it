package com.brivo.panel.server.reference.model.message.down;

import com.brivo.panel.server.reference.model.message.DownstreamMessageType;

public class ToggleDeviceMessage extends DeviceAdministratorMessage {

    public ToggleDeviceMessage() {
        super(DownstreamMessageType.ToggleDeviceMessage);
    }

    public String toString() {
        return "ToggleDeviceMessage : {objectId:" + this.deviceId + ", adminId:" + administratorId + "}";
    }
}
