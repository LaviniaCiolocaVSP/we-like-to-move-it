package com.brivo.panel.server.reference.dao.group;

import com.brivo.panel.server.reference.dao.FileQuery;
import com.brivo.panel.server.reference.dao.QueryUtils;

public enum GroupQuery implements FileQuery {
    DELETE_GROUP;

    private String query;

    GroupQuery() {
        this.query = QueryUtils.getQueryText(GroupQuery.class, this.name());
    }

    @Override
    public String getQuery() {
        return query;
    }

}