package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class DevicePropertyPK implements Serializable {
    private long deviceId;
    private String id;

    @Column(name = "device_id", nullable = false)
    @Id
    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    @Column(name = "id", nullable = false, length = 30)
    @Id
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DevicePropertyPK that = (DevicePropertyPK) o;

        if (deviceId != that.deviceId) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (deviceId ^ (deviceId >>> 32));
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
