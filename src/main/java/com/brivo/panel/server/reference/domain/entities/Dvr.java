package com.brivo.panel.server.reference.domain.entities;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.sql.Timestamp;
import java.util.Collection;

@Entity
public class Dvr {
    private long dvrId;
    private String url;
    private String name;
    private Long dvrTypeId;
    private Long accountId;
    private Long playbackOffset;
    private String timeZone;
    private Long maxVideoAge;
    private String username;
    private String password;
    private Timestamp created;
    private Timestamp updated;
    private String modelSpecificField;
    private Short useDvrns;
    private Collection<Camera> camerasByDvrId;
    private DvrType dvrTypeByDvrTypeId;
    private Account accountByAccountId;

    @Id
    @Column(name = "dvr_id", nullable = false)
    public long getDvrId() {
        return dvrId;
    }

    public void setDvrId(long dvrId) {
        this.dvrId = dvrId;
    }

    @Basic
    @Column(name = "url", nullable = true, length = 2048)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "name", nullable = true, length = 40)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "dvr_type_id", nullable = true)
    public Long getDvrTypeId() {
        return dvrTypeId;
    }

    public void setDvrTypeId(Long dvrTypeId) {
        this.dvrTypeId = dvrTypeId;
    }

    @Basic
    @Column(name = "account_id", nullable = true)
    public Long getAccountId() {
        return accountId;
    }

    public void setAccountId(Long accountId) {
        this.accountId = accountId;
    }

    @Basic
    @Column(name = "playback_offset", nullable = true)
    public Long getPlaybackOffset() {
        return playbackOffset;
    }

    public void setPlaybackOffset(Long playbackOffset) {
        this.playbackOffset = playbackOffset;
    }

    @Basic
    @Column(name = "time_zone", nullable = true, length = 128)
    public String getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(String timeZone) {
        this.timeZone = timeZone;
    }

    @Basic
    @Column(name = "max_video_age", nullable = true)
    public Long getMaxVideoAge() {
        return maxVideoAge;
    }

    public void setMaxVideoAge(Long maxVideoAge) {
        this.maxVideoAge = maxVideoAge;
    }

    @Basic
    @Column(name = "username", nullable = true, length = 50)
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Basic
    @Column(name = "password", nullable = true, length = 50)
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Basic
    @Column(name = "created", nullable = true)
    public Timestamp getCreated() {
        return created;
    }

    public void setCreated(Timestamp created) {
        this.created = created;
    }

    @Basic
    @Column(name = "updated", nullable = true)
    public Timestamp getUpdated() {
        return updated;
    }

    public void setUpdated(Timestamp updated) {
        this.updated = updated;
    }

    @Basic
    @Column(name = "model_specific_field", nullable = true, length = 60)
    public String getModelSpecificField() {
        return modelSpecificField;
    }

    public void setModelSpecificField(String modelSpecificField) {
        this.modelSpecificField = modelSpecificField;
    }

    @Basic
    @Column(name = "use_dvrns", nullable = true)
    public Short getUseDvrns() {
        return useDvrns;
    }

    public void setUseDvrns(Short useDvrns) {
        this.useDvrns = useDvrns;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Dvr dvr = (Dvr) o;

        if (dvrId != dvr.dvrId) {
            return false;
        }
        if (url != null ? !url.equals(dvr.url) : dvr.url != null) {
            return false;
        }
        if (name != null ? !name.equals(dvr.name) : dvr.name != null) {
            return false;
        }
        if (dvrTypeId != null ? !dvrTypeId.equals(dvr.dvrTypeId) : dvr.dvrTypeId != null) {
            return false;
        }
        if (accountId != null ? !accountId.equals(dvr.accountId) : dvr.accountId != null) {
            return false;
        }
        if (playbackOffset != null ? !playbackOffset.equals(dvr.playbackOffset) : dvr.playbackOffset != null) {
            return false;
        }
        if (timeZone != null ? !timeZone.equals(dvr.timeZone) : dvr.timeZone != null) {
            return false;
        }
        if (maxVideoAge != null ? !maxVideoAge.equals(dvr.maxVideoAge) : dvr.maxVideoAge != null) {
            return false;
        }
        if (username != null ? !username.equals(dvr.username) : dvr.username != null) {
            return false;
        }
        if (password != null ? !password.equals(dvr.password) : dvr.password != null) {
            return false;
        }
        if (created != null ? !created.equals(dvr.created) : dvr.created != null) {
            return false;
        }
        if (updated != null ? !updated.equals(dvr.updated) : dvr.updated != null) {
            return false;
        }
        if (modelSpecificField != null ? !modelSpecificField.equals(dvr.modelSpecificField) : dvr.modelSpecificField != null) {
            return false;
        }
        return useDvrns != null ? useDvrns.equals(dvr.useDvrns) : dvr.useDvrns == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (dvrId ^ (dvrId >>> 32));
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (dvrTypeId != null ? dvrTypeId.hashCode() : 0);
        result = 31 * result + (accountId != null ? accountId.hashCode() : 0);
        result = 31 * result + (playbackOffset != null ? playbackOffset.hashCode() : 0);
        result = 31 * result + (timeZone != null ? timeZone.hashCode() : 0);
        result = 31 * result + (maxVideoAge != null ? maxVideoAge.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (created != null ? created.hashCode() : 0);
        result = 31 * result + (updated != null ? updated.hashCode() : 0);
        result = 31 * result + (modelSpecificField != null ? modelSpecificField.hashCode() : 0);
        result = 31 * result + (useDvrns != null ? useDvrns.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "dvrByDvrId")
    public Collection<Camera> getCamerasByDvrId() {
        return camerasByDvrId;
    }

    public void setCamerasByDvrId(Collection<Camera> camerasByDvrId) {
        this.camerasByDvrId = camerasByDvrId;
    }

    @ManyToOne
    @JoinColumn(name = "dvr_type_id", referencedColumnName = "dvr_type_id", insertable = false, updatable = false)
    public DvrType getDvrTypeByDvrTypeId() {
        return dvrTypeByDvrTypeId;
    }

    public void setDvrTypeByDvrTypeId(DvrType dvrTypeByDvrTypeId) {
        this.dvrTypeByDvrTypeId = dvrTypeByDvrTypeId;
    }

    @ManyToOne
    @JoinColumn(name = "account_id", referencedColumnName = "account_id", insertable = false, updatable = false)
    public Account getAccountByAccountId() {
        return accountByAccountId;
    }

    public void setAccountByAccountId(Account accountByAccountId) {
        this.accountByAccountId = accountByAccountId;
    }
}
