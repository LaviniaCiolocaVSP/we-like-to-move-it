package com.brivo.panel.server.reference.dao.hardware;

import com.brivo.panel.server.reference.model.hardware.SaltoLock;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Salto Lock Extractor
 *
 * @author brandon
 */
@Component
public class SaltoLockExtractor implements ResultSetExtractor<List<SaltoLock>> {
    @Override
    public List<SaltoLock> extractData(ResultSet rs) throws SQLException,
            DataAccessException {
        List<SaltoLock> list = new ArrayList<>();

        while (rs.next()) {
            Long deviceId = rs.getLong("deviceOID");
            Integer lockId = rs.getInt("lockID");
            Integer passthrough = rs.getInt("passthrough");

            Integer enablePrivacyModeInt = rs.getInt("enablePrivacyMode");
            Boolean enablePrivacyMode = false;
            if (enablePrivacyModeInt != null && enablePrivacyModeInt == 1) {
                enablePrivacyMode = true;
            }

            Integer cacheCredentialTimeLimit = rs.getInt("lockCacheCredentialTimeLimit");

            SaltoLock lock = new SaltoLock();

            lock.setDeviceId(deviceId);
            lock.setLockId(lockId);
            lock.setPassthroughDelay(passthrough);
            lock.setPrivacyModeOn(enablePrivacyMode);
            lock.setCredentialCachePeriod(cacheCredentialTimeLimit);

            long unlockScheduleId = 0;
            long lockdown = rs.getLong("lockedDown");
            if (rs.wasNull()) {
                unlockScheduleId = rs.getLong("scheduleID");
            } else if (lockdown > 0) {
                lock.setPrivacyModeOn(false);
            }
            lock.setUnlockScheduleId(unlockScheduleId);

            list.add(lock);
        }

        return list;
    }
}
