package com.brivo.panel.server.reference.integration;

import io.restassured.RestAssured;
import io.restassured.filter.log.LogDetail;
import io.restassured.parsing.Parser;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

class RestAssuredConfig implements BeforeAllCallback {

    @Override
    public void beforeAll(final ExtensionContext context) {
        RestAssured.enableLoggingOfRequestAndResponseIfValidationFails(LogDetail.ALL);
        RestAssured.port = 8888;
        RestAssured.defaultParser = Parser.JSON;
    }
}
