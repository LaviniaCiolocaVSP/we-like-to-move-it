package com.brivo.panel.server.reference.unit;

import com.brivo.panel.server.reference.AbstractRPSTests;
import com.brivo.panel.server.reference.domain.entities.Brain;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.Device;
import com.brivo.panel.server.reference.domain.entities.DeviceType;
import com.brivo.panel.server.reference.domain.entities.DeviceTypes;
import com.brivo.panel.server.reference.domain.repository.DeviceRepository;
import com.brivo.panel.server.reference.dto.ui.UiDoorDTO;
import com.brivo.panel.server.reference.service.UiDeviceService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class DeviceServiceTests extends AbstractRPSTests {

    @Mock
    private DeviceRepository deviceRepository;

    @InjectMocks
    private UiDeviceService deviceService;

    @Test
    @DisplayName("Given there are doors for an existing account, when reading the devices, then the doors are correctly retrieved")
    void givenThereAreDoorsForAnExistingAccount_whenReadingTheDevices_thenTheyAreCorrectlyRetrieved() {
        // arrange
        final Device device = mock(Device.class);
        when(device.getDeleted()).thenReturn((short) 0);

        final Brain brainByBrainId = mock(Brain.class);
        when(device.getBrainByBrainId()).thenReturn(brainByBrainId);

        final DeviceType deviceType = mock(DeviceType.class);
        when(device.getDeviceTypeByDeviceTypeId()).thenReturn(deviceType);
        when(deviceType.getDeviceTypeId()).thenReturn(DeviceTypes.DOOR_KEYPAD.getDeviceTypeID());

        final BrivoObject brivoObject = mock(BrivoObject.class);
        when(brivoObject.getObjectId()).thenReturn(VALID_OBJECT_ID);
        when(device.getObjectByBrivoObjectId()).thenReturn(brivoObject);

        final List<Device> devices = Collections.singletonList(device);
        when(deviceRepository.findByAccountId(VALID_ACCOUNT_ID)).thenReturn(Optional.of(devices));

        // act
        final Collection<UiDoorDTO> uiDoorDtosForAccount = deviceService.getUiDoorDtosForAccount(VALID_ACCOUNT_ID);

        // assert
        assertNotNull(uiDoorDtosForAccount, "The devices list cannot be null");
        assertEquals(uiDoorDtosForAccount.size(), devices.size());
    }

    @Test
    @DisplayName("Given there are no doors for an existing account, when reading the devices, then the response contains no doors")
    void givenThereAreNoDoorsForAnExistingAccount_whenReadingTheDevices_thenTheyAreCorrectlyConverted() {
        final Device device = mock(Device.class);
        when(device.getDeleted()).thenReturn((short) 1);

        final DeviceType deviceType = mock(DeviceType.class);
        when(device.getDeviceTypeByDeviceTypeId()).thenReturn(deviceType);
        when(deviceType.getDeviceTypeId()).thenReturn(DeviceTypes.ELEVATOR.getDeviceTypeID());

        final List<Device> devices = Collections.singletonList(device);
        when(deviceRepository.findByAccountId(VALID_ACCOUNT_ID)).thenReturn(Optional.of(devices));

        final Collection<UiDoorDTO> uiDoorDtosForAccount = deviceService.getUiDoorDtosForAccount(VALID_ACCOUNT_ID);

        assertNotNull(uiDoorDtosForAccount, "The devices list cannot be null");
        assertEquals(uiDoorDtosForAccount.size(), 0);
    }

    @Test
    @DisplayName("Given an in-existing account is queried, when reading the devices, then an empty list is returned")
    void givenAnAccountDoesNotExist_whenReadingTheDevices_thenTheyAreCorrectlyConverted() {
        when(deviceRepository.findByAccountId(INVALID_ACCOUNT_ID)).thenReturn(null);

        final Collection<UiDoorDTO> uiDoorDtosForAccount = deviceService.getUiDoorDtosForAccount(VALID_ACCOUNT_ID);

        assertNotNull(uiDoorDtosForAccount, "The devices list should not be null");
        assertEquals(uiDoorDtosForAccount.size(), 0);
    }
}
