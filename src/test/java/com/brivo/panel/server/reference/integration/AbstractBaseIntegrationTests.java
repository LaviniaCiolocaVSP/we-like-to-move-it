package com.brivo.panel.server.reference.integration;

import com.brivo.panel.server.reference.AbstractRPSTests;
import com.brivo.panel.server.reference.PanelReferenceServerApplication;
import com.brivo.panel.server.reference.RunProfile;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@SpringBootTest(
        classes = PanelReferenceServerApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT
)
@ActiveProfiles(RunProfile.INTEGRATION_TESTING)
@ExtendWith({
        RestAssuredConfig.class,
        SpringExtension.class
})
abstract class AbstractBaseIntegrationTests extends AbstractRPSTests {

    @LocalServerPort
    protected int port;
}
