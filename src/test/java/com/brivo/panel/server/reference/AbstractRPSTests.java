package com.brivo.panel.server.reference;

public abstract class AbstractRPSTests {

    protected static final long VALID_ACCOUNT_ID = 82836;

    protected static final long INVALID_ACCOUNT_ID = 127;

    protected static final long VALID_OBJECT_ID = 123;
}
