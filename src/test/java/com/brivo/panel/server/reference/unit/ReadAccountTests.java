package com.brivo.panel.server.reference.unit;

import com.brivo.panel.server.reference.AbstractRPSTests;
import com.brivo.panel.server.reference.domain.brivo.BrivoAccountRepository;
import com.brivo.panel.server.reference.domain.brivo.BrivoObjectPropertyRepository;
import com.brivo.panel.server.reference.domain.entities.Account;
import com.brivo.panel.server.reference.domain.entities.AccountType;
import com.brivo.panel.server.reference.domain.entities.BrivoObject;
import com.brivo.panel.server.reference.domain.entities.BrivoObjectTypes;
import com.brivo.panel.server.reference.domain.repository.AccountRepository;
import com.brivo.panel.server.reference.domain.repository.ObjectPropertyRepository;
import com.brivo.panel.server.reference.dto.AccountDTO;
import com.brivo.panel.server.reference.dto.ui.UiAccountSummaryDTO;
import com.brivo.panel.server.reference.service.account.ReadAccount;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
class ReadAccountTests extends AbstractRPSTests {

    private static final String ACCOUNT_NAME = "Brivo account";

    @Mock
    private AccountRepository accountRepository;

    @Mock
    private BrivoAccountRepository brivoAccountRepository;

    @Mock
    private BrivoObjectPropertyRepository brivoObjectPropertyRepository;

    @Mock
    private ObjectPropertyRepository objectPropertyRepository;

    @InjectMocks
    private ReadAccount readAccount;

    @Test
    @DisplayName("Given a current account exists, when reading the account summary, then the account details are " +
            "correctly returned")
    void givenACurrentAccountExists_whenReadingTheAccountSummary_thenTheAccountDetailsAreCorrectlyReturned() {
        final UiAccountSummaryDTO accountSummaryDTO = mock(UiAccountSummaryDTO.class);
        when(accountSummaryDTO.getAccountId()).thenReturn(VALID_ACCOUNT_ID);
        when(accountSummaryDTO.getName()).thenReturn(ACCOUNT_NAME);

        final Page<UiAccountSummaryDTO> accountsPage = new PageImpl<>(Collections.singletonList(accountSummaryDTO));
        when(accountRepository.getCurrentAccount(any(Pageable.class))).thenReturn(accountsPage);

        final UiAccountSummaryDTO accountSummary = readAccount.getCurrentAccount();

        assertNotNull(accountSummary, "The account cannot be null");
        assertEquals((long) accountSummary.getAccountId(), VALID_ACCOUNT_ID);
        assertEquals(accountSummary.getName(), ACCOUNT_NAME);
    }

    @Test
    @DisplayName("Given a current account does not exist, when reading the account summary, then a null response is returned")
    void givenThereIsNoCurrentAccount_whenReadingTheAccount_thenANullAccountIsReturned() {
        when(accountRepository.getCurrentAccount(any(Pageable.class))).thenReturn(Page.empty());

        final UiAccountSummaryDTO accountSummary = readAccount.getCurrentAccount();

        assertNull(accountSummary, "The account should be null");
    }

    @Test
    @DisplayName("Given a current account exists, when reading the account, then the account details are correctly returned")
    void givenACurrentAccountExists_whenReadingTheAccount_thenTheAccountDetailsAreCorrectlyReturned() {
        // arrange
        final Account account = mock(Account.class);
        when(account.getAccountId()).thenReturn(VALID_ACCOUNT_ID);
        when(account.getName()).thenReturn(ACCOUNT_NAME);
        when(account.getAccountTypeByAccountTypeId()).thenReturn(new AccountType());
        when(account.getObjectByObjectId()).thenReturn(new BrivoObject(BrivoObjectTypes.ACCOUNT));

        when(accountRepository.findById(VALID_ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(objectPropertyRepository.findByAccountId(VALID_ACCOUNT_ID)).thenReturn(Optional.of(Collections.emptyList()));

        // act
        final AccountDTO accountDTO = readAccount.getCurrentAccount(VALID_ACCOUNT_ID);

        // assert
        assertNotNull(accountDTO, "The account cannot be null");
        assertEquals((long) accountDTO.getId(), VALID_ACCOUNT_ID);
        assertEquals(accountDTO.getName(), ACCOUNT_NAME);
        verify(accountRepository, times(1)).findById(VALID_ACCOUNT_ID);
    }

    @Test
    @DisplayName("Given a current account doesn't exist, when reading the account, then an exception is thrown")
    void givenACurrentAccountDoesntExist_whenReadingTheAccount_thenAnExceptionIsThrown() {
        when(accountRepository.findById(INVALID_ACCOUNT_ID)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> readAccount.getCurrentAccount(INVALID_ACCOUNT_ID));
        verify(accountRepository, times(1)).findById(INVALID_ACCOUNT_ID);
    }

    @Test
    @DisplayName("Given a Brivo account exists, when reading the account, then the account details are correctly returned")
    void givenABrivoAccountExists_whenReadingTheAccount_thenTheAccountDetailsAreCorrectlyReturned() {
        // arrange
        final Account account = mock(Account.class);
        when(account.getAccountId()).thenReturn(VALID_ACCOUNT_ID);
        when(account.getName()).thenReturn(ACCOUNT_NAME);
        when(account.getAccountTypeByAccountTypeId()).thenReturn(new AccountType());
        when(account.getObjectByObjectId()).thenReturn(new BrivoObject(BrivoObjectTypes.ACCOUNT));

        when(brivoAccountRepository.findById(VALID_ACCOUNT_ID)).thenReturn(Optional.of(account));
        when(brivoObjectPropertyRepository.findByAccountId(VALID_ACCOUNT_ID)).thenReturn(Optional.of(Collections.emptyList()));

        // act
        final AccountDTO accountDTO = readAccount.getBrivoAccount(VALID_ACCOUNT_ID);

        // setup
        assertNotNull(accountDTO, "The account cannot be null");
        assertEquals((long) accountDTO.getId(), VALID_ACCOUNT_ID);
        assertEquals(accountDTO.getName(), ACCOUNT_NAME);
        verify(brivoAccountRepository, times(1)).findById(VALID_ACCOUNT_ID);
    }

    @Test
    @DisplayName("Given a Brivo account doesn't exist, when reading the account, then an exception is thrown")
    void givenABrivoAccountDoesntExist_whenReadingTheAccount_thenAnExceptionIsThrown() {
        when(brivoAccountRepository.findById(INVALID_ACCOUNT_ID)).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> readAccount.getBrivoAccount(INVALID_ACCOUNT_ID));
        verify(brivoAccountRepository, times(1)).findById(INVALID_ACCOUNT_ID);
    }
}
