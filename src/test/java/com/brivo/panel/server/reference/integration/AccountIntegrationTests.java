package com.brivo.panel.server.reference.integration;

import com.brivo.panel.server.reference.PanelReferenceServerApplication;
import com.brivo.panel.server.reference.domain.brivo.BrivoAccountRepository;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.core.Is.is;

@SpringBootTest(
        classes = PanelReferenceServerApplication.class,
        webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
@ExtendWith({
        RestAssuredConfig.class,
        SpringExtension.class
})
class AccountIntegrationTests extends AbstractBaseIntegrationTests {

    private static final String ACCOUNT_ENDPOINT = "/account";

    @Autowired
    private BrivoAccountRepository brivoAccountRepository;

    @Test
    @DisplayName("Given the current account exists, when requesting the account summary, then the account details are " +
            "correctly returned")
    void givenTheCurrentAccountExists_whenRequestingTheCurrentAccountSummary_ThenThereIsOnlyOneAccountInTheResponse() {
        given().port(port)
               .contentType(ContentType.JSON)
        .when()
               .get(ACCOUNT_ENDPOINT + "/current")
        .then()
               .statusCode(HttpStatus.OK.value())
               .body("$", hasKey("accountId"))
               .body("$", hasKey("name"))
        ;
    }

    @Test
    @DisplayName("Given accounts exist in the DB, when requesting the accounts, then their number is correctly returned")
    void givenAccountsExist_whenRequestingTheAccountsSummary_ThenTheyAreCorrectlyReturned() {
        final int accountsNumber = (int) brivoAccountRepository.count();

        given().port(port)
               .contentType(ContentType.JSON)
        .when()
               .get(ACCOUNT_ENDPOINT)
        .then()
               .statusCode(HttpStatus.OK.value())
               .body("$.size", is(accountsNumber))
        ;
    }
}
