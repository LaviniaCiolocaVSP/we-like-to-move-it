# [Brivo Reference Panel Server](https://bitbucket.org/brivoinc/panel-server-reference)  #

The Reference Panel Server is a standalone server which aims to be used as a reference both by the Brivo Cloud and the 
Panel devices.

### Start the application
gradle bootRun