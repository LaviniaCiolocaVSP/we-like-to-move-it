BEGIN TRANSACTION;

delete from brivo20.device_schedule;
delete from brivo20.object_permission;

delete from brivo20.schedule_exception_data;
delete from brivo20.schedule_data;
delete from brivo20.schedule_holiday_map;
delete from brivo20.schedule;
delete from brivo20.holiday;

delete from brivo20.credential_field_value;
delete from brivo20.credential_field;
delete from brivo20.access_credential;
delete from brivo20.users;

delete from brivo20.board_data;
delete from brivo20.board_property;
delete from brivo20.board;
delete from brivo20.door_data;
delete from brivo20.door_data_antipassback;
delete from brivo20.device_property;
delete from brivo20.prog_device_puts;
delete from brivo20.paneldata_hardware_extra_fields_values;
delete from brivo20.paneldata_hardware_tags_extra_fields;
delete from brivo20.device;
delete from brivo20.address;

delete from brivo20.security_group_member;
delete from brivo20.security_group_antipassback;
delete from brivo20.security_group;

delete from brivo20.prog_device_data;
delete from brivo20.swap_history;
delete from brivo20.brain_connection_status;
delete from brivo20.brain_state;
delete from brivo20.prog_io_points;
delete from brivo20.rs485_settings;
delete from brivo20.elevator_floor_map;
delete from brivo20.brain_build_version;
delete from brivo20.brain_ip_address;
delete from brivo20.brain;

delete from brivo20.report_configuration;
delete from brivo20.feature_override;
delete from brivo20.oauth_application;
delete from brivo20.application;
delete from brivo20.account;

delete from brivo20.brain_lock;
delete from brivo20.video_provider_property_value;
delete from brivo20.video_provider;
delete from brivo20.object_property;
delete from brivo20.object;

COMMIT;
