ALTER TABLE brivo20.account DROP CONSTRAINT brand_account_fk;

-------

--this is the actual state of the trigger on security_group table, no alterations yet
create or replace function brivo20.security_group_biufer_f() returns trigger language plpgsql as $$
BEGIN
  IF TG_OP = 'INSERT' THEN
    IF COALESCE(NEW.object_id, 0) = 0 or COALESCE(NEW.security_group_id, 0) = 0 THEN
      SELECT nextval('object_id_seq'), nextval('security_group_id_seq'), CURRENT_TIMESTAMP, CURRENT_TIMESTAMP
      INTO NEW.object_id, NEW.security_group_id, NEW.created, NEW.updated;

      INSERT INTO object ( object_id, object_type_id )
      VALUES ( NEW.object_id, 4 );
    END IF;

    IF NEW.security_group_type_id != 0 THEN
      INSERT INTO security_group_member ( security_group_id, object_id )
        SELECT security_group_id, NEW.object_id
        FROM security_group
        WHERE account_id = NEW.account_id
        AND security_group_type_id = 0;
     END IF;

   ELSE
    SELECT CURRENT_TIMESTAMP
    INTO NEW.updated;
  END IF;

  RETURN NEW;
END; $$

-------