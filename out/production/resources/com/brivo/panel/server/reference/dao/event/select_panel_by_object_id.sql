SELECT brain_id,
       b.object_id,
       account_id,
       'Control Panel ' || b.name AS device_name,
       o.object_type_id
FROM brain b,
     object o
WHERE b.object_id = :panelObjectId
  AND o.object_id = b.object_id