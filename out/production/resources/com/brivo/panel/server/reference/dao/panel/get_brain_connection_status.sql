SELECT
  bcs.brain_id brainId,
  bcs.message_queue_name messageQueueName,
  bcs.connect_time connectTime,
  bcs.ip_address ipAddress
FROM brain_connection_status bcs
  JOIN brain bn ON bcs.brain_id = bn.brain_id
WHERE bn.object_id = :panelObjId