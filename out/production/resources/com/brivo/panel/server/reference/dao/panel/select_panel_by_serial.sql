SELECT brain_id,
	object_id,
	account_id,
	network_id,
	electronic_serial_number,
	firmware_version,
	time_zone,
	panel_changed,
	persons_changed,
	schedules_changed,
	brain_type_id,
	is_registered,
	log_period
FROM brain
WHERE electronic_serial_number = :panelSerialNumber