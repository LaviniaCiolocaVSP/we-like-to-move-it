INSERT INTO unknown_card_log
(account_id,
 door_name,
 hex_value,
 number_bits,
 occurred,
 device_oid)
SELECT d.account_id,
       d.name,
       :credentialValue,
       :credentialBits,
       :occurred,
       :objectID
FROM device d
WHERE d.object_id = :objectID