SELECT DISTINCT b.object_id
FROM device d
JOIN object_permission op ON op.object_id = d.object_id
JOIN security_group sg ON sg.object_id = op.actor_object_id
JOIN brain b on d.brain_id = b.brain_id
where sg.object_id in (:groupObjectIds)
and security_group_type_id = 2