SELECT d.object_id AS deviceOID,
    pdp.board_number AS boardNumber,
    pdp.point_address AS pointAddress,
    pdp.argument AS argument,
    d.two_factor_schedule_id AS twoFactorScheduleID,
    d.two_factor_interval AS twoFactorInterval,
    d.card_required_schedule_id AS cardRequiredScheduleID
FROM brain b
JOIN device d ON b.brain_id = d.brain_id
JOIN prog_device_puts pdp ON d.object_id = pdp.device_oid
WHERE b.electronic_serial_number = :panelSerialNumber
AND d.deleted != 1
AND d.device_type_id = 8