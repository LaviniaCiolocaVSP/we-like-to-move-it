SELECT d.object_id AS deviceOID,
    pdp.board_number AS boardNumber,
    pdp.point_address AS pointAddress,
    ds.schedule_id AS scheduleID,
    pdd.notify_flag AS notifyFlag,
    pdd.notify_diseng AS notifyDisengage
FROM device d,
    prog_device_puts pdp,
    device_schedule ds,
    prog_device_data pdd,
    brain b,
    board bd
WHERE b.brain_id = d.brain_id
AND d.object_id = pdp.device_oid
AND d.device_id = ds.device_id
AND d.object_id = pdd.device_oid
AND pdp.panel_oid = bd.panel_oid
AND bd.board_number = pdp.board_number
AND d.deleted != 1 
AND b.electronic_serial_number = :panelSerialNumber
AND d.device_type_id = 5
/* additional criteria for switches on door boards */
AND bd.board_type in (1,12,13)
AND pdp.point_address in (1,2,6,7,14,15,19,20)
/* union and repeat the common portion */
UNION
SELECT d.object_id AS deviceOID,
    pdp.board_number AS boardNumber,
    pdp.point_address AS pointAddress,
    ds.schedule_id AS scheduleID,
    pdd.notify_flag AS notifyFlag,
    pdd.notify_diseng AS notifyDisengage
FROM device d,
    prog_device_puts pdp,
    device_schedule ds,
    prog_device_data pdd,
    brain b,
    board bd
WHERE b.brain_id = d.brain_id
AND d.object_id = pdp.device_oid
AND d.device_id = ds.device_id
AND d.object_id = pdd.device_oid
AND pdp.panel_oid = bd.panel_oid
AND bd.board_number = pdp.board_number
AND d.deleted != 1 
AND b.electronic_serial_number = :panelSerialNumber
AND d.device_type_id = 5
/* additional criteria for switches on i/o boards */
AND bd.board_type = 2
/* points 1 through 8, inclusive, are inputs on an i/o board; > 8 are outputs which we can ignore */
AND pdp.point_address BETWEEN 1 AND 8
UNION
SELECT d.object_id AS deviceOID,
    pdp.board_number AS boardNumber,
    pdp.point_address AS pointAddress,
    ds.schedule_id AS scheduleID,
    pdd.notify_flag AS notifyFlag,
    pdd.notify_diseng AS notifyDisengage
FROM device d,
    prog_device_puts pdp,
    device_schedule ds,
    prog_device_data pdd,
    brain b,
    board bd
WHERE b.brain_id = d.brain_id
AND d.object_id = pdp.device_oid
AND d.device_id = ds.device_id
AND d.object_id = pdd.device_oid
AND pdp.panel_oid = bd.panel_oid
AND bd.board_number = pdp.board_number
AND d.deleted != 1 
AND b.electronic_serial_number = :panelSerialNumber
AND d.device_type_id = 5
/* additional criteria for switches on i/o boards */
AND bd.board_type = 3
/* points 1 through 2, inclusive, are inputs on an i/o board; > 2 are outputs which we can ignore */
AND pdp.point_address BETWEEN 1 AND 2