SELECT 
  b.object_id
FROM brain b
JOIN brain_type bt ON b.brain_type_id = bt.brain_type_id
WHERE bt.firmware_protocol = 6000
AND b.account_id = :accountId