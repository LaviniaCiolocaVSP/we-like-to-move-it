SELECT COUNT(*) AS count
FROM object_permission
WHERE object_id = :deviceObjectId
  AND actor_object_id IN (
  SELECT sg.object_id
  FROM security_group sg,
       security_group_member sgm
  WHERE sgm.object_id = :userObjectId
    AND sg.security_group_id = sgm.security_group_id
    AND sg.disabled = 0
    AND sg.security_group_type_id != 0
)
