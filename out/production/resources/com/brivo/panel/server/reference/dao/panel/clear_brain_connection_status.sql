UPDATE brain_connection_status bcs
SET message_queue_name = null, 
	connect_time = null 
WHERE bcs.brain_id = (
	SELECT brain_id 
	FROM brain bn 
	WHERE bn.electronic_serial_number = :panelSerial
)