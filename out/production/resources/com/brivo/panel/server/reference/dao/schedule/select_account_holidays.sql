SELECT h.holiday_id AS holidayId,
  h.start_date AS startDate,
  h.stop_date AS stopDate
FROM holiday h
WHERE h.account_id = :accountId
  