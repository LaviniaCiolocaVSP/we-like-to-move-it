SELECT sed.is_enable_block AS isEnableBlock,
    sed.repeat_type AS repeatType,
    sed.repeat_ordinal AS repeatOrdinal,
    sed.repeat_index AS repeatIndex,
    sed.start_min AS startTime,
    sed.end_min AS endTime,
    sed.schedule_id AS scheduleId
FROM schedule_exception_data sed
WHERE sed.schedule_id = :scheduleId
ORDER BY start_min, end_min