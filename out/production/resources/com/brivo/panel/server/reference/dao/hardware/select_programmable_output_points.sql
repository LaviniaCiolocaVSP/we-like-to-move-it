SELECT pip.board_number AS boardNumber,
    pip.point_address AS pointAddress,
    pdp.behavior AS behavior,
    pdp.argument AS argument,
    b.board_type AS boardType
FROM prog_device_puts pdp
JOIN board b ON (b.panel_oid = pdp.panel_oid 
    AND b.board_number = pdp.board_number)
JOIN prog_io_points pip ON pdp.panel_oid = pip.panel_oid
WHERE pdp.device_oid = :deviceOID
AND pdp.board_number = pip.board_number
AND pdp.point_address = pip.point_address
AND pip.type = 2