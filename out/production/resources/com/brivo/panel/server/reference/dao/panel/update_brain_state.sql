UPDATE brain_state bs
SET panel_checked = :panelStateTime,
	ip_address = :ipAddress,
	firmware_version = :firmwareVersion
WHERE bs.object_id = :panelObjId