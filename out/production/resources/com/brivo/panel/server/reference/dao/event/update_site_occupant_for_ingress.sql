UPDATE site_occupant
SET site_oid       = :siteId,
    is_present     = 1,
    entered        = :entered,
    entry_door_oid = :entryDoorId,
    exited         = null,
    exit_door_oid  = null
WHERE user_oid = :userId
