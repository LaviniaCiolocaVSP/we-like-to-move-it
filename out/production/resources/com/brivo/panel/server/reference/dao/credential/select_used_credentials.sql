SELECT DISTINCT ac.owner_object_id AS ownerOID,
    ac.access_credential_type_id AS accessCredentialTypeID,
    act.name AS accessCredentialTypeName,
    ac.enable_on AS enableOn,
    ac.expires AS expires,
    ac.credential AS credential,
    ac.access_credential_id AS accessCredentialID,
    act.num_bits AS numBits,
    ac.num_bits as numBitsBackup,
    ac.created AS created,
    extract(epoch FROM (ac.created-act.new_card_engine_date)) as createdWith255Magic
FROM access_credential ac
JOIN access_credential_type act ON act.access_credential_type_id = ac.access_credential_type_id
WHERE owner_object_id IN (
    SELECT DISTINCT ac.owner_object_id
    FROM access_credential ac
    JOIN security_group_member sgm ON ac.owner_object_id = sgm.object_id
    JOIN users u ON sgm.object_id = u.object_id
    JOIN security_group sg ON sgm.security_group_id = sg.security_group_id
    JOIN object_permission op ON sg.object_id = op.actor_object_id
    JOIN device d ON op.object_id = d.object_id
    JOIN brain b ON d.brain_id = b.brain_id
    JOIN Cassandra_Credential_Usage ccu ON
    ccu.DeviceId = d.object_id and ccu.CredentialId = ac.access_credential_id and ccu.UserId = ac.owner_object_id
    WHERE ac.disabled = 0
  	AND ac.enable_on is not null
  	AND u.deactivated IS NULL
  	AND u.disabled = 0
   	AND sg.disabled = 0
   	AND d.deleted != 1
 	AND b.electronic_serial_number = :panelSerialNumber
)      
AND ac.disabled != 1
ORDER BY ac.owner_object_id
LIMIT :maxNumCredentials