SELECT DISTINCT
    b.object_id
FROM 
    device d
JOIN
    brain b on d.brain_id = b.brain_id
WHERE 
    d.deleted = 0
AND 
    d.object_id in(:deviceObjectIds)