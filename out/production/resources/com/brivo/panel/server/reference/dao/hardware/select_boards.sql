SELECT b.board_number AS boardNumber,
    b.board_type AS boardType,
    b.object_id AS boardId,
    pip.point_address AS pointAddress,
    pip.eol AS eol,
    pip.state AS state
FROM board b
JOIN prog_io_points pip ON (b.panel_oid = pip.panel_oid
	AND b.board_number = pip.board_number)
WHERE b.panel_oid = (
    SELECT object_id
    FROM brain
    WHERE electronic_serial_number = :panelSerialNumber
)
AND pip.type != 3
ORDER BY b.board_number