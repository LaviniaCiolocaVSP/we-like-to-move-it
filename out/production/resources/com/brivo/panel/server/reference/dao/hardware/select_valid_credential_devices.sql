SELECT d.object_id AS deviceOID,
    pdp.board_number AS boardNumber,
    pdp.point_address AS pointAddress,
    pdd.notify_flag AS notifyFlag,
    d.two_factor_schedule_id AS twoFactorScheduleID,
    d.two_factor_interval AS twoFactorInterval,
    d.card_required_schedule_id AS cardRequiredScheduleID
FROM brain b
JOIN device d ON b.brain_id = d.brain_id
JOIN prog_device_puts pdp ON d.object_id = pdp.device_oid
JOIN prog_device_data pdd ON d.object_id = pdd.device_oid
WHERE b.electronic_serial_number = :panelSerialNumber
AND d.deleted != 1
AND d.device_type_id = 4
AND pdp.point_address IN (
    SELECT ipt.point_address
    FROM board b
    JOIN io_point_template ipt ON b.board_type = ipt.board_type
    WHERE b.panel_oid = (
        SELECT object_id
        FROM brain
        WHERE electronic_serial_number = :panelSerialNumber
    )
    AND ipt.type = 3
)