SELECT bn.account_id      AS account_id,
       bd.object_id       AS object_id,
       ''                 AS full_name,
       bd.object_id       AS device_oid,
       obj.object_type_id AS object_type_id,
       bd.board_location  AS device_name,
       sg.object_id       AS site_oid,
       sg.name            AS security_group_name,
       0                  AS device_type_id,
       ''                 AS engage_msg,
       ''                 AS disengage_msg
FROM board bd
       JOIN brain bn ON bn.object_id = bd.panel_oid
       JOIN object obj ON obj.object_id = bd.object_id
       JOIN security_group sg ON sg.security_group_id = :securityGroupId
WHERE bd.object_id = :boardObjectId