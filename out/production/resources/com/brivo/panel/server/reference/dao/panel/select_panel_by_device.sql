SELECT b.brain_id,
	b.object_id,
	b.account_id,
	b.network_id,
	b.electronic_serial_number,
	b.firmware_version,
	b.time_zone,
	b.panel_changed,
	b.persons_changed,
	b.schedules_changed,
	b.brain_type_id,
	b.is_registered,
	b.log_period
FROM brain b, 
	device d
WHERE b.brain_id = d.brain_id
AND d.deleted = 0
AND d.object_id = :deviceObjectId