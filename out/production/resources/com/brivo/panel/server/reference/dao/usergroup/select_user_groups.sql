SELECT sg.object_id as group_object_id,
			 sga.reset_time,
			 COALESCE(sga.immunity, 0) AS immunity,
			 sgm.object_id as user_object_id
FROM security_group sg
			 JOIN brain b ON sg.account_id = b.account_id
			 LEFT OUTER JOIN security_group_antipassback sga ON sg.security_group_id = sga.security_group_id
			 LEFT OUTER JOIN security_group_member sgm ON sg.security_group_id = sgm.security_group_id
WHERE b.object_id = :panelObjectId
	AND sg.security_group_type_id = 2
	AND sg.disabled = 0
	AND sg.deleted = 0
ORDER BY sg.object_id
