UPDATE 
    brain
SET 
   panel_changed = CURRENT_TIMESTAMP
WHERE
    object_id in (:brainIds)