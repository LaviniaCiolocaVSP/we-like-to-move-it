BEGIN TRANSACTION;

--LIKE ACS, do not delete from object_permission and security_group_member
--DELETE FROM brivo20.object_permission
--WHERE actor_object_id = :groupObjectId;

--DELETE FROM brivo20.security_group_member
--WHERE object_id = :groupObjectId;

UPDATE brivo20.security_group SET disabled = 1
where object_id = :groupObjectId;

UPDATE schedule s
SET enabling_group_id = 0, enabling_grace_period = 0
WHERE s.enabling_group_id = :groupObjectId;

COMMIT;