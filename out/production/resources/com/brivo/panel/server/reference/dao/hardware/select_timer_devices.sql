SELECT d.object_id AS deviceOID,
    ds.schedule_id AS scheduleID,
    pdd.notify_flag AS notifyFlag,
    pdd.notify_diseng AS notifyDisengage
FROM brain b
JOIN device d ON b.brain_id = d.brain_id
JOIN device_schedule ds ON d.device_id = ds.device_id
JOIN prog_device_data pdd ON d.object_id = pdd.device_oid
WHERE b.electronic_serial_number = :panelSerialNumber
AND d.deleted != 1
AND d.device_type_id = 3