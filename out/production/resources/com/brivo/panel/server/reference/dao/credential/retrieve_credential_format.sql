SELECT access_credential_type_id,
  name,
  description,
  num_bits,
  credential_encoding_id,
  supported_by_4000
FROM access_credential_type act
WHERE act.access_credential_type_id = :access_credential_type_id
AND act.access_credential_type_id >= 100
AND act.access_credential_type_id != 101