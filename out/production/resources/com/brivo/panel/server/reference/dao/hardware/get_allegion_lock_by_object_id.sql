select 
	d.name as name,
    d.object_id as deviceId,
    dd.node_num as lockId
from
    device d 
join 
    door_data dd on d.object_id = dd.device_oid
where 
    d.deleted = 0
and
    d.device_type_id = 11
and 
d.object_id = :deviceOid