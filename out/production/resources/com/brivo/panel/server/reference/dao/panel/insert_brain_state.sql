INSERT INTO brain_state 
(
	object_id, 
	panel_checked, 
	firmware_version, 
	ip_address
)
VALUES 
(
	:panelObjId, 
	:panelStateTime, 
	:firmwareVersion, 
	:ipAddress
)