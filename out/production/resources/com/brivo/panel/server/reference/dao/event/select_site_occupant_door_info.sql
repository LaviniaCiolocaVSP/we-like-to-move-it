SELECT d.object_id  AS deviceObjectId,
       d.is_ingress,
       d.is_egress,
       sg.object_id AS siteObjectId
FROM device d
       JOIN security_group_member sgm ON sgm.object_id = d.object_id
       JOIN security_group sg ON (sg.security_group_id = sgm.security_group_id
  AND sg.security_group_type_id = 1
  AND sg.deleted = 0)
WHERE d.object_id = :deviceObjectId
  AND d.deleted = 0
