select 
    dd.osdp_pd_address as pdAddress,
    dd.BOARD_NUM as boardAddress,
    dd.node_num as nodeNum
from 
    door_data dd 
join
    device d
on 
    d.object_id = dd.DEVICE_OID 
where 
    d.brain_id = :brainId
and 
  d.deleted = 0