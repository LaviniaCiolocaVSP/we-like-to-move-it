BEGIN TRANSACTION;

DELETE FROM brivo20.access_credential
WHERE owner_object_id = :userObjectId;

DELETE FROM brivo20.security_group_member
WHERE object_id = :userObjectId;

DELETE FROM brivo20.users
where object_id = :userObjectId;

DELETE FROM brivo20.object
where object_id = :userObjectId;

COMMIT;