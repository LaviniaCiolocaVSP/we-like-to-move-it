BEGIN TRANSACTION;

delete from brivo20.credential_field_value
where access_credential_id = :accessCredentialId;

DELETE FROM brivo20.access_credential
WHERE access_credential_id = :accessCredentialId;

COMMIT;