BEGIN TRANSACTION;

DELETE FROM brivo20.schedule_holiday_map
WHERE holiday_oid = :holidayId;

DELETE FROM brivo20.holiday
where holiday_id = :holidayId;

COMMIT;