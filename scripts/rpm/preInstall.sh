#!/bin/bash
#
APP_NAME=@applicationName@
CONFIG_FILE=/etc/brivo/@applicationName@.properties
DATE=`date +"%y-%m-%d.%H.%M"`
BACKUP_CONFIG="/etc/brivo/@applicationName@.properties.$DATE"
START_SCRIPT="/usr/local/bin/@applicationName@.sh"
WORK_LOCATION="/usr/local/brivo/$APP_NAME"
DEPLOY_LOCATION="/usr/local/brivo"



mkdir -p /etc/brivo
mkdir -p /var/log/brivo/$APP_NAME
mkdir -p /usr/local/brivo
mkdir -p /usr/local/bin

echo "Created direcories"

# check if user exists. If not, create it.
id -u @appUser@  &>/dev/null || useradd @appUser@

chown -R @appUser@ /var/log/brivo/$APP_NAME
chown -R @appUser@ /etc/brivo
chown -R @appUser@ /usr/local/brivo
chown -R @appUser@ /usr/local/bin

echo "Assigned ownership of directories to @appUser@"

if [[ -e $CONFIG_FILE || -e $START_SCRIPT ]]; then
  echo "A previous version is installed. Shutting it down if application is running"
  service $APP_NAME stop
  cp $CONFIG_FILE $BACKUP_CONFIG 2>/dev/null
  echo "Made a backup of $CONFIG_FILE_BACKUP"
 if $START_SCRIPT status | grep 'is running' >/dev/null 2>&1
  then
    $START_SCRIPT  stop
  fi

  if [ -f $WORK_LOCATION ]
  then
    rm -rf $WORK_LOCATION
    echo "Removed $WORK_LOCATION"
  fi
fi

# Destroy previous backup
echo "Destroying the old backup war directory"
rm -rf $DEPLOY_LOCATION/$APP_NAME-backup


# Copy previous war to a safe location before it gets removed by the new rpm
echo "Making backup war directory"
mkdir -p $DEPLOY_LOCATION/$APP_NAME-backup
echo "Copying old war file to a backup directory"
cp $DEPLOY_LOCATION/$APP_NAME-*.war $DEPLOY_LOCATION/$APP_NAME-backup 2>/dev/null

echo "Done with pre-install script"