#!/bin/sh
APP_NAME=@applicationName@
APP_CONFIG="/etc/brivo/$APP_NAME.properties"
JVM_CONFIG="/etc/brivo/$APP_NAME.env"
RUN_AS_USER=@appUser@
DEBUG_OPTS="-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=4143,suspend=n"

CURRENT_USER=`whoami`
IS_CURRENT_USER=0

if [ $CURRENT_USER == $RUN_AS_USER ]; then
IS_CURRENT_USER=1
fi

echo "Running $APP_NAME as $RUN_AS_USER"

if [ ! -f $APP_CONFIG ]
then
	echo "Application config not found at $APP_CONFIG... Please make sure that the application confguration exists"
	exit
fi

if [ ! -f $JVM_CONFIG ]
then
	echo "Application jvm config not found at $JVM_CONFIG... Continuing sans custom jvm env settings"
	JVM_ARGS="-Dcom.sun.management.jmxremote.ssl=false -Dcom.sun.management.jmxremote.port=7200 -Djava.rmi.server.hostname=localhost -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote"
else
	JVM_ARGS=$(<$JVM_CONFIG)
fi

PIDFile="/var/log/brivo/$APP_NAME/$APP_NAME.pid"
SPRING_OPTS="--spring.pidfile=$PIDFile --spring.cloud.bootstrap.location=$APP_CONFIG"

function check_if_pid_file_exists {
  if [ ! -f $PIDFile ]
  then
    echo "PID file not found: $PIDFile"
  exit 1
  fi
}

function check_if_process_is_running {
  if ps -p $(print_process) > /dev/null
  then
    return 0
  else
    return 1
 fi
}



function print_process {
  echo $(<"$PIDFile")
}

case "$1" in
  status)
    check_if_pid_file_exists
    if check_if_process_is_running
    then
      echo $(print_process)" is running"
    else
      echo "Process not running: $(print_process)"
    fi
    ;;
  stop)
    check_if_pid_file_exists
    if ! check_if_process_is_running
    then
      echo "Process $(print_process) already stopped"
      exit 0
    fi
    PID=$(print_process)
    kill -TERM $PID
    echo -ne "Waiting for process to stop"

    NOT_KILLED=1
    for i in {1..20}; do
      if [ -f "$PIDFile" ]
      then
        sleep 1
      else
        NOT_KILLED=0
      fi
    done
    if [ $NOT_KILLED = 1 ]
    then
      echo "Cannot kill process $(print_process)"
      exit 1
    fi
    echo "Process stopped"
    ;;
  start)
    if [ -f $PIDFile ] && check_if_process_is_running
    then
      echo "Process $(print_process) already running"
      exit 1
    fi
    if [ $IS_CURRENT_USER -eq 0 ]; then
      su - $RUN_AS_USER -c "nohup java $JVM_ARGS -jar /usr/local/brivo/$APP_NAME.war $SPRING_OPTS >/var/log/brivo/$APP_NAME/$APP_NAME.out 2>&1 &"
    else
      nohup java $JVM_ARGS -jar /usr/local/brivo/$APP_NAME.war $SPRING_OPTS >/var/log/brivo/$APP_NAME/$APP_NAME.out 2>&1 &
    fi
    echo "Process started"
    ;;
  debug)
    if [ -f $PIDFile ] && check_if_process_is_running
    then
      echo "Process $(print_process) already running"
      exit 1
    fi

    if [ $IS_CURRENT_USER -eq 0 ]; then
      su - $RUN_AS_USER -c  "nohup java $DEBUG_OPTS $JVM_ARGS -jar /usr/local/brivo/$APP_NAME.war $SPRING_OPTS >/var/log/brivo/$APP_NAME/$APP_NAME.out 2>&1 &"
    else
      nohup java $DEBUG_OPTS $JVM_ARGS -jar /usr/local/brivo/$APP_NAME.war $SPRING_OPTS >/var/log/brivo/$APP_NAME/$APP_NAME.out 2>&1 &
    fi

    echo "Process started"
    ;;
  restart)
    $0 stop
    if [ $? = 1 ]
    then
      exit 1
    fi
      $0 start
    ;;
  *)

  echo "Usage: $0 {start|stop|restart|debug|status}"
  exit 1
esac
exit 0

